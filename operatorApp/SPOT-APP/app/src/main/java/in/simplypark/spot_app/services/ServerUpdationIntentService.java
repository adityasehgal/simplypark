package in.simplypark.spot_app.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import hirondelle.date4j.DateTime;
import in.simplypark.spot_app.R;
import in.simplypark.spot_app.alarm.AlarmHandler;
import in.simplypark.spot_app.db_tables.ReusableTokensTbl;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.db_tables.LostPassTbl;
import in.simplypark.spot_app.db_tables.PaymentInfoTbl;
import in.simplypark.spot_app.db_tables.PendingExits;
import in.simplypark.spot_app.db_tables.SimplyParkBookingTbl;
import in.simplypark.spot_app.db_tables.SimplyParkTransactionTbl;
import in.simplypark.spot_app.db_tables.TransactionTbl;
import in.simplypark.spot_app.utils.Cache;
import in.simplypark.spot_app.utils.MiscUtils;
import in.simplypark.spot_app.utils.PendingActions;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static in.simplypark.spot_app.config.Constants.SYNC_ONLINE_OP_APP_BOOKING_IS_STAFF_PASS;
import static in.simplypark.spot_app.utils.MiscUtils.checkForConnectivity;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 */
public class ServerUpdationIntentService extends IntentService {

    // TODO: Rename parameters
    private static final String EXTRA_PARAM1 = "in.simplypark.spot_app.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "in.simplypark.spot_app.extra.PARAM2";


    public ServerUpdationIntentService() {

        super("ServerUpdationIntentService");

    }

    /**
     * Starts this service to perform action DeRegister with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionDeRegister(Context context, String param1, String param2) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_DEREGISTER);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Register with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionRegister(Context context) {

        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_REGISTER);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Entry with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionLogEntry(Context context,long entryTime,String carReg,
                                         String parkingId, boolean isParkAndRide,int vehicleType,
                                           String token){
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_ENTRY);
        intent.putExtra(Constants.BUNDLE_PARAM_CAR_REG, carReg);
        intent.putExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, entryTime);
        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_ID, parkingId);
        intent.putExtra(Constants.BUNDLE_PARAM_IS_PARK_AND_RIDE, isParkAndRide);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, vehicleType);
        intent.putExtra(Constants.BUNDLE_PARAM_TOKEN,token);
        context.startService(intent);
    }


    /**
     * Starts this service to perform action Exit with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionLogExit(Context context, String parkingId, long exitTime,
                                          double parkingFees,int vehicleType,String token) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_EXIT);
        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_ID, parkingId);
        intent.putExtra(Constants.BUNDLE_PARAM_EXIT_TIME, exitTime);
        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_FEES, parkingFees);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, vehicleType);
        intent.putExtra(Constants.BUNDLE_PARAM_TOKEN,token);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Exit with the given parameters. If
     * the service is already performing a task this action will be queued.
     * This function is used for prepaid parkings as it will also send
     * car reg number as an additional parameter
     *
     * @see IntentService
     */

    public static void startActionLogExit(Context context, String parkingId,
                                          long entryTime,long exitTime,
                                          double parkingFees,int vehicleType,String vehicleReg,
                                          String token) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_EXIT);
        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_ID, parkingId);
        intent.putExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, entryTime);
        intent.putExtra(Constants.BUNDLE_PARAM_EXIT_TIME, exitTime);
        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_FEES, parkingFees);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, vehicleType);
        intent.putExtra(Constants.BUNDLE_PARAM_CAR_REG,vehicleReg);
        intent.putExtra(Constants.BUNDLE_PARAM_TOKEN,token);
        context.startService(intent);
    }


    /**
     * Starts this service to perform action SimplyPark Entry with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionSimplyParkLogEntry(Context context, String bookingId,
                                                     int bookingType,
                                                     String parkingId, long entryTime,
                                                     String vehicleReg, int vehicleType,
                                                     boolean isParkAndRide) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_SP_ENTRY);

        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_ID, bookingId);
        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_ID, parkingId);
        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_TYPE, bookingType);
        intent.putExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, entryTime);
        intent.putExtra(Constants.BUNDLE_PARAM_CAR_REG, vehicleReg);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, vehicleType);
        intent.putExtra(Constants.BUNDLE_PARAM_IS_PARK_AND_RIDE, isParkAndRide);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action SimplyPark Exit with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionSimplyParkLogExit(Context context, String bookingId,
                                                    String parkingId, long exitTime, int parkingFees,
                                                    int vehicleType, String vehicleReg) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);

        intent.setAction(Constants.ACTION_SP_EXIT);
        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_ID, bookingId);
        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_ID, parkingId);
        intent.putExtra(Constants.BUNDLE_PARAM_EXIT_TIME, exitTime);
        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_FEES, parkingFees);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, vehicleType);
        intent.putExtra(Constants.BUNDLE_PARAM_CAR_REG, vehicleReg);

        context.startService(intent);
    }


    /**
     * Starts this service to perform action Refresh Token with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionRefreshToken(Context context) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_REFRESH_TOKEN);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Refresh Token with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionGetCurrentOccupancy(Context context) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_GET_CURRENT_OCCUPANCY);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action on recieving an entry notifcation
     * from another app. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionHandleEntryNotificationFromAnotherApp(Context context,
                                                             String parkingId,String reg,
                                                             boolean isParkAndRide,
                                                             long entryTime,
                                                             int vehicleType,
                                                             int syncId,
                                                             int operatorId,String operatorDesc,
                                                                        String token) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_ENTRY_NOTIFICATION_FROM_ANOTHER_APP);

        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_ID, parkingId);
        intent.putExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, entryTime);
        intent.putExtra(Constants.BUNDLE_PARAM_CAR_REG, reg);
        intent.putExtra(Constants.BUNDLE_PARAM_IS_PARK_AND_RIDE, isParkAndRide);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, vehicleType);
        intent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);
        intent.putExtra(Constants.BUNDLE_PARAM_OPERATOR_DESC, operatorDesc);
        intent.putExtra(Constants.BUNDLE_PARAM_OPERATOR_ID,operatorId);
        intent.putExtra(Constants.BUNDLE_PARAM_TOKEN,token);

        context.startService(intent);
    }


    /**
     * Starts this service to perform action on recieving an exit notifcation
     * from another app. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */


    public static void startActionHandleExitNotificationFromAnotherApp(Context context,
                                                                       String parkingId,
                                                                       double parkingFees,
                                                                       long entryTime, long exitTime,
                                                                       int vehicleType,
                                                                       String vehicleReg,
                                                                       int syncId,
                                                                       int operatorId,
                                                                       String operatorDesc,
                                                                       String token){
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_EXIT_NOTIFICATION_FROM_ANOTHER_APP);

        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_ID, parkingId);
        intent.putExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, entryTime);
        intent.putExtra(Constants.BUNDLE_PARAM_EXIT_TIME, exitTime);
        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_FEES, parkingFees);
        intent.putExtra(Constants.BUNDLE_PARAM_CAR_REG, vehicleReg);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, vehicleType);
        intent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);
        intent.putExtra(Constants.BUNDLE_PARAM_OPERATOR_DESC, operatorDesc);
        intent.putExtra(Constants.BUNDLE_PARAM_OPERATOR_ID,operatorId);
        intent.putExtra(Constants.BUNDLE_PARAM_TOKEN,token);



        context.startService(intent);

    }

    /**
     * Starts this service to perform action on recieving a cancel notifcation
     * from another app. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */


    public static void startActionHandleCancelNotificationFromAnotherApp(Context context,
                                                                         String parkingId,
                                                                         int operatorId,int syncId,
                                                                String vehicleReg,int vehicleType,
                                                                         long entryDateTime,
                                                                         String token){

        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_CANCEL_NOTIFICATION_FROM_ANOTHER_APP);

        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_ID, parkingId);
        intent.putExtra(Constants.BUNDLE_PARAM_OPERATOR_ID, operatorId);
        intent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_REG, vehicleReg);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, vehicleType);
        intent.putExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, entryDateTime);
        intent.putExtra(Constants.BUNDLE_PARAM_TOKEN,token);


        context.startService(intent);


    }

    /**
     * Starts this service to perform action on recieving a cancel notifcation
     * from another app. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */


    public static void startActionHandleUpdateNotificationFromAnotherApp(Context context,
                                                                         String parkingId,
                                                                         int operatorId,
                                                                          int parkingStatus,
                                                                          int parkingFees,
                                                                          int syncId,
                                                              String vehicleReg,int vehicleType,
                                                                         long entryDateTime,
                                                                         String token){

        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_UPDATE_NOTIFICATION_FROM_ANOTHER_APP);

        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_ID, parkingId);
        intent.putExtra(Constants.BUNDLE_PARAM_OPERATOR_ID, operatorId);
        intent.putExtra(Constants.BUNDLE_PARAM_UPDATED_PARKING_STATUS, parkingStatus);
        intent.putExtra(Constants.BUNDLE_PARAM_UPDATED_PARKING_FEES, parkingFees);
        intent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_REG, vehicleReg);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, vehicleType);
        intent.putExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, entryDateTime);
        intent.putExtra(Constants.BUNDLE_PARAM_TOKEN,token);


        context.startService(intent);


    }

    /**
     * Starts this service to perform action on recieving a cancel notifcation
     * from another app. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */


    public static void startActionHandleBookingUpdateFromServer(Context context, String serialNumber,
                                                                String bookingId, int bookingType,
                                                                String customer,
                                                                String startTime, String endTime,
                                                                String bookingTime,
                                                                int numBookedSlots, boolean isPassForStaff, int status,
                                                                int syncId, int spaceOwnerId) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_BOOKING_UPDATE_FROM_SERVER);

        intent.putExtra(Constants.BUNDLE_PARAM_SERIAL_NUMBER, serialNumber);
        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_ID, bookingId);
        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_TYPE, bookingType);
        intent.putExtra(Constants.BUNDLE_PARAM_CUSTOMER_DESC, customer);
        intent.putExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, startTime);
        intent.putExtra(Constants.BUNDLE_PARAM_EXIT_TIME, endTime);
        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_TIME, bookingTime);
        intent.putExtra(Constants.BUNDLE_PARAM_NUM_BOOKED_SLOTS, numBookedSlots);
        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_STATUS, status);
        intent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);
        intent.putExtra(Constants.BUNDLE_PARAM_SPACE_OWNER_ID, spaceOwnerId);
        intent.putExtra(Constants.BUNDLE_PARAM_IS_STAFF_PASS, isPassForStaff);


        context.startService(intent);


    }

    /**
     * Starts this service to perform action on recieving a booking update which was
     * created in the operator APP.
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */


    public static void startActionHandleOpAppBookingUpdate(Context context,
                                                           String name, String email, String mobile,
                                                           String bookingId, int bookingStatus,
                                                           String bookingStartDateTime,
                                                           String bookingEndDateTime, String passIssueDate,
                                                           String encodedPassCode, String serialNumber,
                                                           String vehicleReg, int vehicleType,
                                                           int numBookedSlots, int parkingFees,
                                                           int discount,
                                                           int spaceOwnerId, int syncId,
                                                           boolean isRenewal, String parentBookingId,
                                                           int paymentMode, String invoiceId,
                                                           String paymentId, int operatorId, boolean isPassForStaff) {

        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_OPAPP_BOOKING_UPDATE_FROM_SERVER);

        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_NAME, name);
        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_EMAIL, email);
        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_MOBILE, mobile);
        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_BOOKING_ID, bookingId);
        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_BOOKING_STATUS, bookingStatus);
        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_BOOKING_START, bookingStartDateTime);
        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_BOOKING_END, bookingEndDateTime);
        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_PASS_ISSUE, passIssueDate);
        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_ENCODE_PASS_CODE, encodedPassCode);
        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_SERIAL_NUM, serialNumber);
        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_VEHICLE_REG, vehicleReg);
        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_VEHICLE_TYPE, vehicleType);
        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_NUM_BOOKED_SLOTS, numBookedSlots);
        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_PARKING_FEES, parkingFees);
        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_DISCOUNT, discount);
        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_SPACE_OWNER_ID, spaceOwnerId);
        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_SYNC_ID, syncId);
        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_IS_RENEWAL, isRenewal);
        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_PARENT_BOOKING_ID, parentBookingId);
        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_PAYMENT_MODE, paymentMode);
        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_INVOICE_ID, invoiceId);
        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_PAYMENT_ID, paymentId);
        intent.putExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_OPERATOR_ID, operatorId);
        intent.putExtra(Constants.BUNDLE_PARAM_IS_STAFF_PASS, isPassForStaff);

        context.startService(intent);

    }


    /**
     * Starts this service to perform action on recieving an entry notifcation
     * from another app. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionHandleSpEntryNotificationFromAnotherApp(Context context,
                                                                          String bookingId, String parkingId,
                                                                          String vehicleReg, int vehicleType,
                                                                          int syncId, long entryTime,
                                                                          int operatorId, String operatorDesc,
                                                                          boolean isParkAndRide) {

        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_SP_ENTRY_NOTIFICATION_FROM_ANOTHER_APP);

        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_ID, bookingId);
        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_ID, parkingId);
        intent.putExtra(Constants.BUNDLE_PARAM_CAR_REG, vehicleReg);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, vehicleType);
        intent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);
        intent.putExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, entryTime);
        intent.putExtra(Constants.BUNDLE_PARAM_OPERATOR_ID, operatorId);
        intent.putExtra(Constants.BUNDLE_PARAM_OPERATOR_DESC, operatorDesc);
        intent.putExtra(Constants.BUNDLE_PARAM_IS_PARK_AND_RIDE, isParkAndRide);


        context.startService(intent);
    }

    /**
     * Starts this service to perform action on recieving an exit notifcation
     * from another app. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */


    public static void startActionHandleSpExitNotificationFromAnotherApp(Context context,
                                                                         String bookingId, String parkingId,
                                                                         int parkingFees, long exitTime,
                                                                         int syncId, int vehicleType,
                                                                         int operatorId, String operatorDesc) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_SP_EXIT_NOTIFICATION_FROM_ANOTHER_APP);

        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_ID, bookingId);
        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_ID, parkingId);
        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_FEES, parkingFees);
        intent.putExtra(Constants.BUNDLE_PARAM_EXIT_TIME, exitTime);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, vehicleType);
        intent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);
        intent.putExtra(Constants.BUNDLE_PARAM_OPERATOR_ID, operatorId);
        intent.putExtra(Constants.BUNDLE_PARAM_OPERATOR_DESC, operatorDesc);


        context.startService(intent);

    }

    /**
     * Starts this service to perform action on recieving a cancel notifcation
     * from another app. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */


    public static void startActionHandleSpCancelNotificationFromAnotherApp(Context context,
                                                                           String parkingId,
                                                                           int operatorId,
                                                                           int syncId,
                                                                           String vehicleReg, int vehicleType,
                                                                           long entryTime,
                                                                           String bookingId, int bookingType) {

        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_SP_CANCEL_NOTIFICATION_FROM_ANOTHER_APP);

        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_ID, parkingId);
        intent.putExtra(Constants.BUNDLE_PARAM_OPERATOR_ID, operatorId);
        intent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_REG, vehicleReg);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, vehicleType);
        intent.putExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, entryTime);
        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_ID, bookingId);
        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_TYPE, bookingType);


        context.startService(intent);


    }

    /**
     * Starts this service to perform action on recieving a cancel notifcation
     * from another app. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */


    public static void startActionHandleSpUpdateNotificationFromAnotherApp(Context context,
                                                                           String parkingId,
                                                                           int status,
                                                                           int fees,
                                                                           int syncId,
                                                                           int operatorId,
                                                                           String vehicleReg, int vehicleType,
                                                                           long entryTime, long exitTime,
                                                                           String bookingId, int bookingType) {

        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_SP_UPDATE_NOTIFICATION_FROM_ANOTHER_APP);

        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_ID, parkingId);
        intent.putExtra(Constants.BUNDLE_PARAM_OPERATOR_ID, operatorId);
        intent.putExtra(Constants.BUNDLE_PARAM_UPDATED_PARKING_STATUS, status);
        intent.putExtra(Constants.BUNDLE_PARAM_UPDATED_PARKING_FEES, fees);
        intent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_REG, vehicleReg);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, vehicleType);
        intent.putExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, entryTime);
        intent.putExtra(Constants.BUNDLE_PARAM_EXIT_TIME, exitTime);
        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_ID, bookingId);
        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_TYPE, bookingType);


        context.startService(intent);


    }

    /**
     * Starts this service to perform action on recieving an exit notifcation
     * from another app. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */


    public static void startActionTriggerPendingActions(Context context) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_TRIGGER_PENDING_ACTIONS);


        context.startService(intent);

    }

    /**
     * Starts this service to perform action on end of day actions. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionEndOfDay(Context context) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_END_OF_DAY);

        context.startService(intent);

    }

    /**
     * Starts this service to perform action to get Space Operating Hours. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionGetOperatingHours(Context context) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_SPACE_OPERATING_HOURS);

        context.startService(intent);
    }

    /**
     * Starts this service to perform action to pull updates from the server. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionGetUpdatesFromServer(Context context, int currentSyncId) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_GET_UPDATES_FROM_SERVER);
        intent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, currentSyncId);

        context.startService(intent);
    }

    /**
     * Starts this service to perform action to push updates to the server. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionPushUpdatesToServer(Context context) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_PUSH_UPDATES_TO_SERVER);

        context.startService(intent);
    }

    /**
     * Starts this service to perform action to get the current sync id  from the server. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionGetSyncIdFromServer(Context context) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_GET_SYNC_ID_FROM_SERVER);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action to get the current sync id  from the server. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionResetSyncWithServer(Context context) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_RESET_SYNC_WITH_SERVER);
        context.startService(intent);
    }


    /**
     * Starts this service to perform actions after connectivity to the server is regainedr. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionSyncWithServer(Context context) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_SYNC_WITH_SERVER);
        context.startService(intent);
    }


    /**
     * Starts this service to perform actions after a transaction is cancelled
     *
     * @see IntentService
     */

    public static void startActionTransactionCancel(Context context, String parkingId,
                                                    long entryTime,
                                                    String vehicleReg,
                                                    int vehicleType) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_CANCEL_TRANSACTION);
        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_ID, parkingId);
        intent.putExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, entryTime);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_REG, vehicleReg);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, vehicleType);

        context.startService(intent);
    }

    /**
     * Starts this service to perform actions after a transaction is updated
     *
     * @see IntentService
     */

    public static void startActionTransactionUpdate(Context context, String parkingId,
                                                    int updatedParkingStatus, int updatedParkingFees,
                                                    long entryTime, String vehicleReg, int vehicleType) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_UPDATE_TRANSACTION);
        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_ID, parkingId);
        intent.putExtra(Constants.BUNDLE_PARAM_UPDATED_PARKING_FEES, updatedParkingFees);
        intent.putExtra(Constants.BUNDLE_PARAM_UPDATED_PARKING_STATUS, updatedParkingStatus);
        intent.putExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, entryTime);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_REG, vehicleReg);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, vehicleType);
        context.startService(intent);
    }

    /**
     * Starts this service to perform actions after a transaction is cancelled
     *
     * @see IntentService
     */

    public static void startActionSpTransactionCancel(Context context, String parkingId, long entryTime,
                                                      String vehicleReg, int vehicleType,
                                                      String bookingId, int bookingType) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_CANCEL_SP_TRANSACTION);
        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_ID, parkingId);
        intent.putExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, entryTime);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_REG, vehicleReg);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, vehicleType);
        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_ID, bookingId);
        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_TYPE, bookingType);
        context.startService(intent);
    }

    /**
     * Starts this service to perform actions after a transaction is updated
     *
     * @see IntentService
     */

    public static void startActionSpTransactionUpdate(Context context, String parkingId,
                                                      int updatedParkingStatus,
                                                      int updatedParkingFees,
                                                      long entryTime, long exitTime,
                                                      String vehicleReg, int vehicleType,
                                                      String bookingId, int bookingType) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_UPDATE_SP_TRANSACTION);
        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_ID, parkingId);
        intent.putExtra(Constants.BUNDLE_PARAM_UPDATED_PARKING_FEES, updatedParkingFees);
        intent.putExtra(Constants.BUNDLE_PARAM_UPDATED_PARKING_STATUS, updatedParkingStatus);
        intent.putExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, entryTime);
        intent.putExtra(Constants.BUNDLE_PARAM_EXIT_TIME, entryTime);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_REG, vehicleReg);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, vehicleType);
        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_ID, bookingId);
        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_TYPE, bookingType);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Refresh Token with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionCalculateOccupancyFromDb(Context context) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_CALCULATE_OCCUPANCY_FROM_DB);
        context.startService(intent);
    }

    public static void startActionStartPushTimerIfRequired(Context context) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_START_PUSH_TIMER_IF_REQUIRED);
        context.startService(intent);

    }

    /**
     * Starts this service to perform action cancellation of Monthly pass with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionCancelMonthlyPass(Context context, String bookingId) {


        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_CANCEL_MONTHLY_PASS);

        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_ID, bookingId);

        context.startService(intent);
    }


    /**
     * Starts this service to perform action is Space Bike Only with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionIsSpaceBikeOnly(Context context) {


        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_IS_SPACE_BIKE_ONLY);

        context.startService(intent);
    }

    public static void startActionLogMonthlyPassGenerated(Context context,
                                                          SimplyParkBookingTbl booking) {
        startActionLogMonthlyPassGenerated(context, booking, "", "");

    }

    /**
     * Starts this service to perform action log monthly Pass creation with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionLogMonthlyPassGenerated(Context context,
                                                          SimplyParkBookingTbl booking,
                                                          String invoiceId, String paymentId) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_MONTHLY_PASS_GENERATED);

        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_ID, booking.getBookingID());
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_ISSUED_TO, booking.getCustomerDesc());
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_EMAIL, booking.getEmail());
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_TEL, booking.getMobile());
        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_STATUS, booking.getBookingStatus());
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_VALID_FROM, booking.getBookingStartTime());
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_VALID_TILL, booking.getBookingEndTime());
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_ISSUE_DATE, booking.getPassIssueDate());
        intent.putExtra(Constants.BUNDLE_PARAM_ENCODE_PASS_CODE, booking.getEncodedPassCode());
        intent.putExtra(Constants.BUNDLE_PARAM_SERIAL_NUMBER, booking.getSerialNumber());
        intent.putExtra(Constants.BUNDLE_PARAM_CAR_REG, booking.getVehicleReg());
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, booking.getVehicleType());
        intent.putExtra(Constants.BUNDLE_PARAM_NUM_BOOKED_SLOTS, booking.getNumBookedSlots());
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_PER_MONTH_FEES, booking.getParkingFees());
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_DISCOUNT, 0);
        intent.putExtra(Constants.BUNDLE_PARAM_IS_RENEWAL, booking.getIsRenewalBooking());
        intent.putExtra(Constants.BUNDLE_PARAM_PARENT_BOOKING_ID, booking.getParentBookingID());
        intent.putExtra(Constants.BUNDLE_PARAM_PAYMENT_MODE, booking.getPaymentMode());
        intent.putExtra(Constants.BUNDLE_PARAM_INVOICE_ID, invoiceId);
        intent.putExtra(Constants.BUNDLE_PARAM_PAYMENT_ID, paymentId);
        intent.putExtra(Constants.BUNDLE_PARAM_IS_STAFF_PASS, booking.getIsIsStaffPass());


        context.startService(intent);


    }


    /**
     * Starts this service to perform action to reset occupancy count with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionResetOccupancyCount(Context context) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_RESET_OCCUPANCY_COUNT);

        context.startService(intent);

    }


    /**
     * Starts this service to perform action to determine if canellation/updation is allowed.If
     * the service is already performing a task this action
     * will be queued.
     *
     * @see IntentService
     */

    public static void startActionIsCancellationUpdationEnabled(Context context) {
        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_IS_CANCELLATION_UPDATION_ALLOWED);

        context.startService(intent);

    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (Constants.ACTION_REGISTER.equals(action)) {
                handleActionRegister();
            } else if (Constants.ACTION_DEREGISTER.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                handleActionDeRegister(param1, param2);
            } else if (Constants.ACTION_ENTRY.equals(action)) {
                final String carReg = intent.getStringExtra(Constants.BUNDLE_PARAM_CAR_REG);
                final long entryTime = intent.getLongExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, 0);
                final String parkingId = intent.getStringExtra(Constants.BUNDLE_PARAM_PARKING_ID);
                final boolean isParkAndRide =
                        intent.getBooleanExtra(Constants.BUNDLE_PARAM_IS_PARK_AND_RIDE, false);
                final int vehicleType = intent.getIntExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                        Constants.VEHICLE_TYPE_CAR);
                final String token = intent.getStringExtra(Constants.BUNDLE_PARAM_TOKEN);
                handleActionEntry(carReg, entryTime, parkingId, isParkAndRide, vehicleType,token);
            } else if (Constants.ACTION_EXIT.equals(action)) {
                final String parkingId = intent.getStringExtra(Constants.BUNDLE_PARAM_PARKING_ID);
                final long exitTime = intent.getLongExtra(Constants.BUNDLE_PARAM_EXIT_TIME, 0);
                final long entryTime = intent.getLongExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, 0);
                final double parkingFees = intent.getDoubleExtra(Constants.BUNDLE_PARAM_PARKING_FEES,
                        0.00);
                final int vehicleType = intent.getIntExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                        Constants.VEHICLE_TYPE_CAR);

                final String vehicleReg = intent.getStringExtra(Constants.BUNDLE_PARAM_CAR_REG);
                final String token = intent.getStringExtra(Constants.BUNDLE_PARAM_TOKEN);


                handleActionExit(parkingId, entryTime,exitTime, parkingFees, vehicleType,vehicleReg,
                                 token);
            } else if (Constants.ACTION_REFRESH_TOKEN.equals(action)) {
                handleActionRefreshToken();
            } else if (Constants.ACTION_GET_CURRENT_OCCUPANCY.equals(action)) {
                handleActionGetCurrentOccupancy();
            } else if (Constants.ACTION_ENTRY_NOTIFICATION_FROM_ANOTHER_APP.equals(action)) {
                final String parkingId = intent.getStringExtra(Constants.BUNDLE_PARAM_PARKING_ID);
                final long entryTime = intent.getLongExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, 0);
                final boolean isParkAndRide =
                        intent.getBooleanExtra(Constants.BUNDLE_PARAM_IS_PARK_AND_RIDE, false);
                final String reg = intent.getStringExtra(Constants.BUNDLE_PARAM_CAR_REG);
                final int vehicleType = intent.getIntExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                        Constants.VEHICLE_TYPE_CAR);
                final int syncId = intent.getIntExtra(Constants.BUNDLE_PARAM_SYNC_ID, -1);
                final String operatorDesc = intent.getStringExtra(
                        Constants.BUNDLE_PARAM_OPERATOR_DESC);
                final int operatorId = intent.getIntExtra(Constants.BUNDLE_PARAM_OPERATOR_ID,
                        Constants.INVALID_OPERATOR_ID);
                final String token = intent.getStringExtra(Constants.BUNDLE_PARAM_TOKEN);


                handleActionEntryNotificationFromAnotherApp(parkingId, entryTime, reg, isParkAndRide,
                        vehicleType, syncId, operatorId,operatorDesc,token);

            } else if (Constants.ACTION_EXIT_NOTIFICATION_FROM_ANOTHER_APP.equals(action)) {
                final String parkingId = intent.getStringExtra(Constants.BUNDLE_PARAM_PARKING_ID);
                final long exitTime = intent.getLongExtra(Constants.BUNDLE_PARAM_EXIT_TIME, 0);
                final long entryTime = intent.getLongExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, 0);
                final double parkingFees = intent.getDoubleExtra(Constants.BUNDLE_PARAM_PARKING_FEES,
                        0.00);
                final int vehicleType = intent.getIntExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                        Constants.VEHICLE_TYPE_CAR);
                final int syncId = intent.getIntExtra(Constants.BUNDLE_PARAM_SYNC_ID, -1);
                final String operatorDesc = intent.getStringExtra(
                        Constants.BUNDLE_PARAM_OPERATOR_DESC);
                final String vehicleReg = intent.getStringExtra(Constants.BUNDLE_PARAM_CAR_REG);
                final int operatorId = intent.getIntExtra(Constants.BUNDLE_PARAM_OPERATOR_ID,
                        Constants.INVALID_OPERATOR_ID);

                final String token = intent.getStringExtra(Constants.BUNDLE_PARAM_TOKEN);


                handleActionExitNotificationFromAnotherApp(parkingId, entryTime,exitTime, parkingFees,
                        vehicleReg, vehicleType, syncId, operatorId,operatorDesc,token);

            } else if (Constants.ACTION_TRIGGER_PENDING_ACTIONS.equals(action)) {
                handleActionTriggerPendingActions();
            } else if (Constants.ACTION_END_OF_DAY.equals(action)) {
                handleActionEndOfDay();
            } else if (Constants.ACTION_SPACE_OPERATING_HOURS.equals(action)) {
                handleActionGetSpaceOperatingHours();
            } else if (Constants.ACTION_GET_UPDATES_FROM_SERVER.equals(action)) {
                final int syncId = intent.getIntExtra(Constants.BUNDLE_PARAM_SYNC_ID, 0);
                handleActionGetUpdatesFromServer(syncId);
            } else if (Constants.ACTION_RESET_SYNC_WITH_SERVER.equals(action)) {
                handleActionGetUpdatesFromServer(Constants.RESET_START_SYNC_ID);
            } else if (Constants.ACTION_GET_SYNC_ID_FROM_SERVER.equals(action)) {
                handleActionGetSyncIdFromServer();
            } else if (Constants.ACTION_PUSH_UPDATES_TO_SERVER.equals(action)) {
                handleActionPushUpdatesToServer();
            } else if (Constants.ACTION_SYNC_WITH_SERVER.equals(action)) {
                handleActionSyncWithServer();
            } else if (Constants.ACTION_CANCEL_TRANSACTION.equals(action)) {
                final String parkingId = intent.getStringExtra(Constants.BUNDLE_PARAM_PARKING_ID);
                final long entryTime = intent.getLongExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, 0);
                final String vehicleReg = intent.getStringExtra(Constants.BUNDLE_PARAM_VEHICLE_REG);
                final int vehicleType = intent.getIntExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                        Constants.VEHICLE_TYPE_INVALID);
                handleActionCancelTransaction(parkingId, entryTime, vehicleReg, vehicleType);
            } else if (Constants.ACTION_UPDATE_TRANSACTION.equals(action)) {
                final String parkingId = intent.getStringExtra(Constants.BUNDLE_PARAM_PARKING_ID);
                final int parkingStatus = intent.getIntExtra(
                        Constants.BUNDLE_PARAM_UPDATED_PARKING_STATUS,
                        Constants.PARKING_STATUS_ENTERED);
                final int parkingFees = intent.getIntExtra(
                        Constants.BUNDLE_PARAM_UPDATED_PARKING_FEES, 0);
                final long entryTime = intent.getLongExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, 0);
                final String vehicleReg = intent.getStringExtra(Constants.BUNDLE_PARAM_VEHICLE_REG);
                final int vehicleType = intent.getIntExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                        Constants.VEHICLE_TYPE_INVALID);

                handleActionUpdateTransaction(parkingId, parkingStatus, parkingFees, entryTime,
                        vehicleReg, vehicleType);
            } else if (Constants.ACTION_CANCEL_NOTIFICATION_FROM_ANOTHER_APP.equals(action)) {
                final String parkingId = intent.getStringExtra(Constants.BUNDLE_PARAM_PARKING_ID);
                final int operatorId = intent.getIntExtra(Constants.BUNDLE_PARAM_OPERATOR_ID, 0);
                final int syncId = intent.getIntExtra(Constants.BUNDLE_PARAM_SYNC_ID,
                        Constants.INVALID_SYNC_ID);
                final String vehicleReg = intent.getStringExtra(Constants.BUNDLE_PARAM_VEHICLE_REG);
                final int vehicleType = intent.getIntExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                                                           Constants.VEHICLE_TYPE_INVALID);
                final long entryTime = intent.getLongExtra(Constants.BUNDLE_PARAM_ENTRY_TIME,0);
                final String token = intent.getStringExtra(Constants.BUNDLE_PARAM_TOKEN);


                handleActionCancelTransactionFromAnotherApp(parkingId, operatorId, syncId,
                                                     vehicleReg,vehicleType,entryTime,token);

            } else if (Constants.ACTION_UPDATE_NOTIFICATION_FROM_ANOTHER_APP.equals(action)) {
                final String parkingId = intent.getStringExtra(Constants.BUNDLE_PARAM_PARKING_ID);
                final int operatorId = intent.getIntExtra(Constants.BUNDLE_PARAM_OPERATOR_ID, 0);
                final int parkingStatus = intent.getIntExtra(
                        Constants.BUNDLE_PARAM_UPDATED_PARKING_STATUS,
                        Constants.PARKING_STATUS_ENTERED);
                final int parkingFees = intent.getIntExtra(
                        Constants.BUNDLE_PARAM_UPDATED_PARKING_FEES,
                        0);
                final int syncId = intent.getIntExtra(Constants.BUNDLE_PARAM_SYNC_ID,
                        Constants.INVALID_SYNC_ID);
                final String vehicleReg = intent.getStringExtra(Constants.BUNDLE_PARAM_VEHICLE_REG);
                final int vehicleType = intent.getIntExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                        Constants.VEHICLE_TYPE_INVALID);
                final long entryTime = intent.getLongExtra(Constants.BUNDLE_PARAM_ENTRY_TIME,0);
                final String token = intent.getStringExtra(Constants.BUNDLE_PARAM_TOKEN);


                handleActionUpdateTransactionFromAnotherApp(parkingId, operatorId, parkingStatus,
                        parkingFees, syncId,vehicleReg,vehicleType,entryTime,token);

            } else if (Constants.ACTION_CALCULATE_OCCUPANCY_FROM_DB.equals(action)) {
                handleActionCalculateOccupancyFromDb();
            } else if (Constants.ACTION_START_PUSH_TIMER_IF_REQUIRED.equals(action)) {
                handleActionStartPushTimerIfRequired();

            } else if (Constants.ACTION_SP_ENTRY.equals(action)) {
                final String vehicleReg = intent.getStringExtra(Constants.BUNDLE_PARAM_CAR_REG);
                final long entryTime = intent.getLongExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, 0);
                final String parkingId = intent.getStringExtra(Constants.BUNDLE_PARAM_PARKING_ID);
                final String bookingId = intent.getStringExtra(Constants.BUNDLE_PARAM_BOOKING_ID);
                final boolean isParkAndRide =
                        intent.getBooleanExtra(Constants.BUNDLE_PARAM_IS_PARK_AND_RIDE, false);
                final int vehicleType = intent.getIntExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                        Constants.VEHICLE_TYPE_CAR);
                final int bookingType = intent.getIntExtra(Constants.BUNDLE_PARAM_BOOKING_TYPE,
                        Constants.BOOKING_TYPE_HOURLY);
                handleActionSpEntry(bookingId, bookingType, parkingId, entryTime,
                        vehicleReg, vehicleType, isParkAndRide);
            } else if (Constants.ACTION_SP_EXIT.equals(action)) {

                final String bookingId = intent.getStringExtra(Constants.BUNDLE_PARAM_BOOKING_ID);
                final String parkingId = intent.getStringExtra(Constants.BUNDLE_PARAM_PARKING_ID);
                final long exitTime = intent.getLongExtra(Constants.BUNDLE_PARAM_EXIT_TIME, 0);
                final int parkingFees = intent.getIntExtra(Constants.BUNDLE_PARAM_PARKING_FEES, 0);
                final int vehicleType = intent.getIntExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                        Constants.VEHICLE_TYPE_CAR);
                final String vehicleReg = intent.getStringExtra(Constants.BUNDLE_PARAM_CAR_REG);

                handleActionSpExit(bookingId, parkingId, exitTime, parkingFees, vehicleType, vehicleReg);
            } else if (Constants.ACTION_CANCEL_SP_TRANSACTION.equals(action)) {
                final String parkingId = intent.getStringExtra(Constants.BUNDLE_PARAM_PARKING_ID);
                final long entryTime = intent.getLongExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, 0);
                final String vehicleReg = intent.getStringExtra(Constants.BUNDLE_PARAM_VEHICLE_REG);
                final int vehicleType = intent.getIntExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                        Constants.VEHICLE_TYPE_INVALID);
                final String bookingId = intent.getStringExtra(Constants.BUNDLE_PARAM_BOOKING_ID);
                final int bookingType = intent.getIntExtra(Constants.BUNDLE_PARAM_BOOKING_TYPE,
                        Constants.BOOKING_TYPE_NA);
                handleActionCancelSpTransaction(parkingId, entryTime, vehicleReg, vehicleType,
                        bookingId, bookingType);

            } else if (Constants.ACTION_UPDATE_SP_TRANSACTION.equals(action)) {
                final String parkingId = intent.getStringExtra(Constants.BUNDLE_PARAM_PARKING_ID);
                final int parkingStatus = intent.getIntExtra(
                        Constants.BUNDLE_PARAM_UPDATED_PARKING_STATUS,
                        Constants.PARKING_STATUS_ENTERED);
                final int parkingFees = intent.getIntExtra(
                        Constants.BUNDLE_PARAM_UPDATED_PARKING_FEES, 0);
                final long entryTime = intent.getLongExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, 0);
                final long exitTime = intent.getLongExtra(Constants.BUNDLE_PARAM_EXIT_TIME, 0);
                final String vehicleReg = intent.getStringExtra(Constants.BUNDLE_PARAM_VEHICLE_REG);
                final int vehicleType = intent.getIntExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                        Constants.VEHICLE_TYPE_INVALID);
                final String bookingId = intent.getStringExtra(Constants.BUNDLE_PARAM_BOOKING_ID);
                final int bookingType = intent.getIntExtra(Constants.BUNDLE_PARAM_BOOKING_TYPE,
                        Constants.BOOKING_TYPE_NA);


                handleActionUpdateSpTransaction(parkingId, parkingStatus, parkingFees,
                        entryTime, exitTime, vehicleReg, vehicleType, bookingId, bookingType);
            } else if (Constants.ACTION_SP_ENTRY_NOTIFICATION_FROM_ANOTHER_APP.equals(action)) {
                final String bookingId = intent.getStringExtra(Constants.BUNDLE_PARAM_BOOKING_ID);
                final String parkingId = intent.getStringExtra(Constants.BUNDLE_PARAM_PARKING_ID);
                final String vehicleReg = intent.getStringExtra(Constants.BUNDLE_PARAM_CAR_REG);
                final int vehicleType = intent.getIntExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                        Constants.VEHICLE_TYPE_CAR);
                final int syncId = intent.getIntExtra(Constants.BUNDLE_PARAM_SYNC_ID, -1);
                final long entryTime = intent.getLongExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, 0);
                final String operatorDesc = intent.getStringExtra(
                        Constants.BUNDLE_PARAM_OPERATOR_DESC);
                final boolean isParkAndRide =
                        intent.getBooleanExtra(Constants.BUNDLE_PARAM_IS_PARK_AND_RIDE, false);
                final int operatorId = intent.getIntExtra(Constants.BUNDLE_PARAM_OPERATOR_ID,
                        Constants.INVALID_OPERATOR_ID);

                handleActionSpEntryNotificationFromAnotherApp(bookingId, parkingId, vehicleReg,
                        vehicleType, syncId, entryTime,
                        operatorId, operatorDesc, isParkAndRide);

            } else if (Constants.ACTION_SP_EXIT_NOTIFICATION_FROM_ANOTHER_APP.equals(action)) {

                final String bookingId = intent.getStringExtra(Constants.BUNDLE_PARAM_BOOKING_ID);
                final String parkingId = intent.getStringExtra(Constants.BUNDLE_PARAM_PARKING_ID);
                final int parkingFees = intent.getIntExtra(Constants.BUNDLE_PARAM_PARKING_FEES, 0);
                final long exitTime = intent.getLongExtra(Constants.BUNDLE_PARAM_EXIT_TIME, 0);
                final int syncId = intent.getIntExtra(Constants.BUNDLE_PARAM_SYNC_ID, -1);
                final int vehicleType = intent.getIntExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                        Constants.VEHICLE_TYPE_CAR);
                final String operatorDesc = intent.getStringExtra(
                        Constants.BUNDLE_PARAM_OPERATOR_DESC);
                final int operatorId = intent.getIntExtra(Constants.BUNDLE_PARAM_OPERATOR_ID,
                        Constants.INVALID_OPERATOR_ID);

                handleActionSpExitNotificationFromAnotherApp(bookingId, parkingId, parkingFees,
                        exitTime, syncId, vehicleType,
                        operatorId, operatorDesc);


            } else if (Constants.ACTION_SP_CANCEL_NOTIFICATION_FROM_ANOTHER_APP.equals(action)) {
                final String parkingId = intent.getStringExtra(Constants.BUNDLE_PARAM_PARKING_ID);
                final int operatorId = intent.getIntExtra(Constants.BUNDLE_PARAM_OPERATOR_ID, 0);
                final int syncId = intent.getIntExtra(Constants.BUNDLE_PARAM_SYNC_ID,
                        Constants.INVALID_SYNC_ID);

                final String vehicleReg = intent.getStringExtra(Constants.BUNDLE_PARAM_VEHICLE_REG);
                final int vehicleType = intent.getIntExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                        Constants.VEHICLE_TYPE_INVALID);
                final long entryTime = intent.getLongExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, 0);
                final String bookingId = intent.getStringExtra(Constants.BUNDLE_PARAM_BOOKING_ID);
                final int bookingType = intent.getIntExtra(Constants.BUNDLE_PARAM_BOOKING_TYPE,
                        Constants.BOOKING_TYPE_NA);


                handleActionSpCancelTransactionFromAnotherApp(parkingId, operatorId, syncId,
                        vehicleReg, vehicleType, entryTime, bookingId, bookingType);


            } else if (Constants.ACTION_SP_UPDATE_NOTIFICATION_FROM_ANOTHER_APP.equals(action)) {
                final String parkingId = intent.getStringExtra(Constants.BUNDLE_PARAM_PARKING_ID);
                final int operatorId = intent.getIntExtra(Constants.BUNDLE_PARAM_OPERATOR_ID, 0);
                final int status = intent.getIntExtra(
                        Constants.BUNDLE_PARAM_UPDATED_PARKING_STATUS,
                        Constants.PARKING_STATUS_ENTERED);
                final int fees = intent.getIntExtra(
                        Constants.BUNDLE_PARAM_UPDATED_PARKING_FEES,
                        0);
                final int syncId = intent.getIntExtra(Constants.BUNDLE_PARAM_SYNC_ID,
                        Constants.INVALID_SYNC_ID);
                final String vehicleReg = intent.getStringExtra(Constants.BUNDLE_PARAM_VEHICLE_REG);
                final int vehicleType = intent.getIntExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                        Constants.VEHICLE_TYPE_INVALID);
                final long entryTime = intent.getLongExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, 0);
                final long exitTime = intent.getLongExtra(Constants.BUNDLE_PARAM_EXIT_TIME, 0);
                final String bookingId = intent.getStringExtra(Constants.BUNDLE_PARAM_BOOKING_ID);
                final int bookingType = intent.getIntExtra(Constants.BUNDLE_PARAM_BOOKING_TYPE,
                        Constants.BOOKING_TYPE_NA);

                handleActionSpUpdateTransactionFromAnotherApp(parkingId, status, fees, syncId, operatorId,
                        vehicleReg, vehicleType, entryTime, exitTime, bookingId, bookingType);


            } else if (Constants.ACTION_BOOKING_UPDATE_FROM_SERVER.equals(action)) {

                String serialNumber = intent.getStringExtra(Constants.BUNDLE_PARAM_SERIAL_NUMBER);
                String bookingId = intent.getStringExtra(Constants.BUNDLE_PARAM_BOOKING_ID);
                String customer = intent.getStringExtra(Constants.BUNDLE_PARAM_CUSTOMER_DESC);
                String startTime = intent.getStringExtra(Constants.BUNDLE_PARAM_ENTRY_TIME);
                String endTime = intent.getStringExtra(Constants.BUNDLE_PARAM_EXIT_TIME);
                String bookingTime = intent.getStringExtra(Constants.BUNDLE_PARAM_BOOKING_TIME);
                int bookingType = intent.getIntExtra(Constants.BUNDLE_PARAM_BOOKING_TYPE, 0);
                int numBookedSlots = intent.getIntExtra(Constants.BUNDLE_PARAM_NUM_BOOKED_SLOTS, 0);
                int bookingStatus = intent.getIntExtra(Constants.BUNDLE_PARAM_BOOKING_STATUS, 0);
                int syncId = intent.getIntExtra(Constants.BUNDLE_PARAM_SYNC_ID, 0);
                int spaceOwnerId = intent.getIntExtra(Constants.BUNDLE_PARAM_SPACE_OWNER_ID, 0);


                handleActionBookingUpdateFromServer(serialNumber, bookingId, customer, bookingType,
                        startTime, endTime, bookingTime, numBookedSlots,
                        bookingStatus,
                        syncId, spaceOwnerId);

            } else if (Constants.ACTION_CANCEL_MONTHLY_PASS.equals(action)) {
                String bookingId = intent.getStringExtra(Constants.BUNDLE_PARAM_BOOKING_ID);

                handleActionCancelMonthlyPass(bookingId);
            } else if (Constants.ACTION_MONTHLY_PASS_GENERATED.equals(action)) {


                final String bookingId = intent.getStringExtra(Constants.BUNDLE_PARAM_BOOKING_ID);
                final String customer = intent.getStringExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_ISSUED_TO);
                final String email = intent.getStringExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_EMAIL);
                final String mobile = intent.getStringExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_TEL);
                final int bookingStatus = (int) intent.getLongExtra(Constants.BUNDLE_PARAM_BOOKING_STATUS,
                        Constants.BOOKING_STATUS_APPROVED);
                final long startTime = intent.getLongExtra(
                        Constants.BUNDLE_PARAM_MONTHLY_PASS_VALID_FROM, 0);
                final long endTime = intent.getLongExtra(
                        Constants.BUNDLE_PARAM_MONTHLY_PASS_VALID_TILL, 0);

                final long passIssueDate = intent.getLongExtra(
                        Constants.BUNDLE_PARAM_MONTHLY_PASS_ISSUE_DATE, 0);

                final String encodedPassCode = intent.getStringExtra(Constants.BUNDLE_PARAM_ENCODE_PASS_CODE);
                final String serialNumber = intent.getStringExtra(Constants.BUNDLE_PARAM_SERIAL_NUMBER);
                final String vehicleReg = intent.getStringExtra(Constants.BUNDLE_PARAM_CAR_REG);
                final int vehicleType = intent.getIntExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                        Constants.VEHICLE_TYPE_CAR);
                final int numBookedSlots = intent.getIntExtra(
                        Constants.BUNDLE_PARAM_NUM_BOOKED_SLOTS, 1);

                final int parkingFees = intent.getIntExtra(
                        Constants.BUNDLE_PARAM_MONTHLY_PASS_PER_MONTH_FEES, 0);

                final int discount = intent.getIntExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_DISCOUNT, 0);
                final boolean isRenewal = intent.getBooleanExtra(Constants.BUNDLE_PARAM_IS_RENEWAL,
                        false);
                final String parentBookingId = intent.getStringExtra(Constants.BUNDLE_PARAM_PARENT_BOOKING_ID);

                final int paymentMode = intent.getIntExtra(Constants.BUNDLE_PARAM_PAYMENT_MODE,
                        Constants.PAYMENT_MODE_CASH);
                final String invoiceId = intent.getStringExtra(Constants.BUNDLE_PARAM_INVOICE_ID);
                final String paymentId = intent.getStringExtra(Constants.BUNDLE_PARAM_PAYMENT_ID);
                final boolean isStaffPass = intent.getBooleanExtra(Constants.BUNDLE_PARAM_IS_STAFF_PASS, false);

                handleActionMonthlyPassGenerated(bookingId, customer, email, mobile, bookingStatus,
                        startTime, endTime, passIssueDate, encodedPassCode, serialNumber,
                        vehicleReg, vehicleType, numBookedSlots, parkingFees,
                        discount, isRenewal, parentBookingId, isStaffPass, paymentMode, invoiceId, paymentId);

            } else if (Constants.ACTION_OPAPP_BOOKING_UPDATE_FROM_SERVER.equals(action)) {
                SimplyParkBookingTbl booking = new SimplyParkBookingTbl();

                booking.setCustomerDesc(intent.getStringExtra(
                        Constants.BUNDLE_PARAM_OP_APP_UPDATE_NAME));

                booking.setEmail(intent.getStringExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_EMAIL));
                booking.setMobile(intent.getStringExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_MOBILE));
                booking.setBookingID(intent.getStringExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_BOOKING_ID));
                booking.setBookingStatus(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_OP_APP_UPDATE_BOOKING_STATUS, Constants.BOOKING_STATUS_APPROVED));

                DateTime startTime = new DateTime(intent.getStringExtra(
                        Constants.BUNDLE_PARAM_OP_APP_UPDATE_BOOKING_START));

                booking.setBookingStartTime(startTime.getMilliseconds(Constants.TIME_ZONE));

                DateTime endTime = new DateTime(intent.getStringExtra(
                        Constants.BUNDLE_PARAM_OP_APP_UPDATE_BOOKING_END));

                booking.setBookingEndTime(endTime.getMilliseconds(Constants.TIME_ZONE));

                DateTime passIssueDate = new DateTime(intent.getStringExtra(
                        Constants.BUNDLE_PARAM_OP_APP_UPDATE_PASS_ISSUE));

                booking.setPassIssueDate(passIssueDate.getMilliseconds(Constants.TIME_ZONE));


                booking.setEncodedPassCode(intent.getStringExtra(
                        Constants.BUNDLE_PARAM_OP_APP_UPDATE_ENCODE_PASS_CODE));

                booking.setSerialNumber(intent.getStringExtra(
                        Constants.BUNDLE_PARAM_OP_APP_UPDATE_SERIAL_NUM));

                booking.setVehicleReg(intent.getStringExtra(
                        Constants.BUNDLE_PARAM_OP_APP_UPDATE_VEHICLE_REG));

                booking.setVehicleType(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_OP_APP_UPDATE_VEHICLE_TYPE, Constants.VEHICLE_TYPE_CAR));

                booking.setNumBookedSlots(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_OP_APP_UPDATE_NUM_BOOKED_SLOTS, 1));
                booking.setParkingFees(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_OP_APP_UPDATE_PARKING_FEES, 0));
                booking.setParentBookingID(intent.getStringExtra(
                        Constants.BUNDLE_PARAM_OP_APP_UPDATE_PARENT_BOOKING_ID));
                booking.setIsRenewalBooking(intent.getBooleanExtra(
                        Constants.BUNDLE_PARAM_OP_APP_UPDATE_IS_RENEWAL, false));

                booking.setPaymentMode(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_OP_APP_UPDATE_PAYMENT_MODE,
                        Constants.PAYMENT_MODE_CASH));

                String invoiceId = "";
                String paymentId = "";

                if (booking.getPaymentMode() == Constants.PAYMENT_MODE_CARD) {
                    invoiceId = intent.getStringExtra(
                            Constants.BUNDLE_PARAM_OP_APP_UPDATE_INVOICE_ID);
                    paymentId = intent.getStringExtra(
                            Constants.BUNDLE_PARAM_OP_APP_UPDATE_PAYMENT_ID);
                }

                booking.setOperatorId(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_OP_APP_UPDATE_OPERATOR_ID,
                        Constants.INVALID_OPERATOR_ID));
                booking.setIsStaffPass(intent.getBooleanExtra(Constants.BUNDLE_PARAM_IS_STAFF_PASS, false));

                int unUsed = intent.getIntExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_DISCOUNT, 0);
                int spaceOwnerId = intent.getIntExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_SPACE_OWNER_ID, 0);
                int syncId = intent.getIntExtra(Constants.BUNDLE_PARAM_OP_APP_UPDATE_SYNC_ID,
                        Constants.INVALID_SYNC_ID);


                handleActionOpAppBookingUpdate(booking, spaceOwnerId, syncId, invoiceId, paymentId);
            } else if (Constants.ACTION_IS_SPACE_BIKE_ONLY.equals(action)) {
                handleActionIsSpaceBikeOnly();
            } else if (Constants.ACTION_RESET_OCCUPANCY_COUNT.equals(action)) {
                handleActionResetOccupancyCount();
            } else if (Constants.ACTION_IS_CANCELLATION_UPDATION_ALLOWED.equals(action)) {
                handleActionIsCancellationUpdationAllowed();
            }else if (Constants.ACTION_LOST_MONTHLY_PASS_REISSUE.equals(action)) {

                final String bookingId = intent.getStringExtra(Constants.BUNDLE_PARAM_BOOKING_ID);
                final long newPassIssueDate = intent.getLongExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_ISSUE_DATE, 0);
                final long oldPassIssueDate = intent.getLongExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_OLD_ISSUE_DATE, 0);

                final String newEncodedPassCode = intent.getStringExtra(Constants.BUNDLE_PARAM_ENCODE_PASS_CODE);
                final String oldEncodedPassCode = intent.getStringExtra(Constants.BUNDLE_PARAM_OLD_ENCODE_PASS_CODE);
                final String newSerialNumber = intent.getStringExtra(Constants.BUNDLE_PARAM_SERIAL_NUMBER);
                final String oldSerialNumber = intent.getStringExtra(Constants.BUNDLE_PARAM_OLD_SERIAL_NUMBER);

                final int paymentMode = intent.getIntExtra(Constants.BUNDLE_PARAM_PAYMENT_MODE,Constants.PAYMENT_MODE_CASH);
                final int lostMonthlyPassFees = intent.getIntExtra(Constants.BUNDLE_PARAM_LOST_MONTHLY_PASS_FEE, 0);

                handleActionModifyMonthlyPass(bookingId, oldEncodedPassCode, newEncodedPassCode, oldPassIssueDate, newPassIssueDate,
                        oldSerialNumber, newSerialNumber, paymentMode, lostMonthlyPassFees);

            }else if (Constants.ACTION_OPAPP_BOOKING_MODIFICATION_NOTIFICATION_FROM_ANOTHER_APP.equals(action)){
                final String bookingId = intent.getStringExtra(Constants.BUNDLE_PARAM_BOOKING_ID);
                final long newPassIssueDate = intent.getLongExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_ISSUE_DATE, 0);
                final long oldPassIssueDate = intent.getLongExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_OLD_ISSUE_DATE, 0);

                final String newEncodedPassCode = intent.getStringExtra(Constants.BUNDLE_PARAM_ENCODE_PASS_CODE);
                final String oldEncodedPassCode = intent.getStringExtra(Constants.BUNDLE_PARAM_OLD_ENCODE_PASS_CODE);
                final String newSerialNumber = intent.getStringExtra(Constants.BUNDLE_PARAM_SERIAL_NUMBER);
                final String oldSerialNumber = intent.getStringExtra(Constants.BUNDLE_PARAM_OLD_SERIAL_NUMBER);


                final int paymentMode = intent.getIntExtra(Constants.BUNDLE_PARAM_PAYMENT_MODE,Constants.PAYMENT_MODE_CASH);
                final int lostMonthlyPassFees = intent.getIntExtra(Constants.BUNDLE_PARAM_LOST_MONTHLY_PASS_FEE, 0);
                final int operatorId = intent.getIntExtra(Constants.BUNDLE_PARAM_OPERATOR_ID,0);
                final String operatorDesc = intent.getStringExtra(Constants.BUNDLE_PARAM_OPERATOR_DESC);


                handleActionModifyMonthlyPassFromAnotherApp(operatorId,operatorDesc,bookingId, oldEncodedPassCode, newEncodedPassCode, oldPassIssueDate, newPassIssueDate,
                        oldSerialNumber, newSerialNumber, lostMonthlyPassFees, paymentMode);

            }
        }
    }

    /**
     * Handle action Register in the provided background thread with the provided
     * parameters.
     */
    private void handleActionRegister() {

        if (checkForConnectivity(getApplicationContext()) == false) {

            PendingActions obj = new PendingActions(Constants.TRIGGER_ACTION_REGISTER);
            obj.save();

            sendResult(Constants.TRIGGER_ACTION_REGISTER, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

            return;
        }

        try {
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

            Cache.setInCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_APP_REGISTERATION_TOKEN, token,
                    Constants.TYPE_STRING);

            OkHttpClient client = new OkHttpClient();
            Request request = buildRegisterRequest(token);

            Response response = client.newCall(request).execute();

            int result = Constants.OK;
            int receivedSyncId = 0;

            if (response.isSuccessful()) {

                String responseData = response.body().string();
                JSONObject json = new JSONObject(responseData);

                //Log.v("ResponseData", responseData);
                Integer occupied = json.getInt("numOccupied");
                receivedSyncId = json.optInt("syncId", Constants.INVALID_SYNC_ID);

                Context context = getApplicationContext();

                /*
                Cache.setInCache(context,
                        Constants.CACHE_PREFIX_PARKED_CARS_COUNT_QUERY,
                        json.getInt("numParkedCars"),
                        Constants.TYPE_INT);

                Cache.setInCache(context,
                        Constants.CACHE_PREFIX_PARKED_BIKES_COUNT_QUERY,
                        json.getInt("numParkedBikes"),
                        Constants.TYPE_INT);


                 Cache.setInCache(context,
                        Constants.CACHE_PREFIX_EARNINGS_PER_DAY_QUERY,
                        json.getInt("earnings"),
                        Constants.TYPE_INT);


                Cache.setInCache(context,
                                 Constants.CACHE_PREFIX_CURRENT_OCCUPANCY_QUERY,
                                 occupied,Constants.TYPE_INT);
                */

                Integer gcmIdAtServer = json.getInt("id");

                Cache.setInCache(context,
                        Constants.CACHE_PREFIX_ISCONFIGURED_QUERY,
                        true, Constants.TYPE_BOOL);


                Cache.setInCache(context,
                        Constants.CACHE_PREFIX_GCM_ID_AT_SERVER_QUERY,
                        gcmIdAtServer, Constants.TYPE_INT);


            } else {
                sendResult(Constants.TRIGGER_ACTION_REGISTER, Constants.NOK,
                        response.message());
                response.body().close();

                return;

            }

            response.body().close();

            sendResult(Constants.TRIGGER_ACTION_REGISTER, result, receivedSyncId);

        } catch (JSONException ioe) {
            MiscUtils.errorMsgs.add("handleActionRegister, JSON Exception:: " + ioe);
            sendResult(Constants.TRIGGER_ACTION_REGISTER, Constants.NOK,
                    Constants.ERROR_INVALID_RESPONSE);
        } catch (java.io.IOException ioe) {
            MiscUtils.errorMsgs.add("handleActionRegister, IO Exception:: " + ioe);

            PendingActions obj = new PendingActions(Constants.TRIGGER_ACTION_REGISTER);
            obj.save();

            sendResult(Constants.TRIGGER_ACTION_REGISTER, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

        }

    }

    /**
     * Handle action DeRegister in the provided background thread with the provided
     * parameters.
     */
    private void handleActionDeRegister(String param1, String param2) {
        // TODO: Handle action DeRegister
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Handle action Entry in the provided background thread with the provided
     * parameters.
     */
    private void handleActionEntry(String carReg, long entryTime,
                                   String parkingId, boolean isParkAndRide,
                                   int vehicleType,String token) {

        if (checkForConnectivity(getApplicationContext()) == false) {
            //set the state of the entry object to NOT in sync

            setTransactionStateOutOfSync(parkingId);
            sendResult(Constants.TRIGGER_ACTION_ENTRY, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

            return;
        }

        int result = Constants.OK;
        try{
        OkHttpClient client = new OkHttpClient();
        Request      request = buildLogEntryRequest(carReg, entryTime,
                parkingId, isParkAndRide, vehicleType,token);


            Response response = client.newCall(request).execute();
            int occupied = -1;
            int numParkedCars = -1;
            int numParkedBikes = -1;
            int serverSyncId = 0;


            if (response.isSuccessful()) {
                String responseData = response.body().string();
                JSONObject json = new JSONObject(responseData);

                //Log.v("ResponseData", responseData);
                int status = json.optInt("status", Constants.NOK);

                if (status == Constants.OK) {
                    occupied = json.getInt("numOccupied");
                    numParkedCars = json.getInt("numParkedCars");
                    numParkedBikes = json.getInt("numParkedBikes");
                    serverSyncId = json.getInt("syncId");

                /* Issue 96 fix. Sync iD should only be updated if the received syncID == expected */
                    Context context = getApplicationContext();
                    int syncIdAtClient = MiscUtils.getSyncId(context);

                    if ((syncIdAtClient + 1) == serverSyncId) {
                        //Moving the syncID increment after receiveing a successful response
                        MiscUtils.updateSyncId(getApplicationContext(), 1);
                    }
                    //issue109: setting the transaction state as IN sync
                    setTransactionStateInSync(parkingId);
                } else {
                    String reason = json.optString("reason");
                    MiscUtils.errorMsgs.add("handleActionEntry, Failed with Reason :: " + reason);
                    setTransactionStateOutOfSync(parkingId);
                    sendResult(Constants.TRIGGER_ACTION_ENTRY, Constants.NOK,
                            reason);
                    response.body().close();

                    return;

                }


            } else {

                String reason = response.message();
                MiscUtils.errorMsgs.add("handleActionEntry, Failed with Reason :: " + reason);
                setTransactionStateOutOfSync(parkingId);
                sendResult(Constants.TRIGGER_ACTION_ENTRY, Constants.NOK,
                        reason);
                response.body().close();

                return;

            }

            Intent localIntent = prepareResult(Constants.TRIGGER_ACTION_ENTRY, result);
            localIntent.putExtra(Constants.BUNDLE_PARAM_CURRENT_OCCUPANCY, occupied);
            localIntent.putExtra(Constants.BUNDLE_PARAM_CURRENT_PARKED_CARS, numParkedCars);
            localIntent.putExtra(Constants.BUNDLE_PARAM_CURRENT_PARKED_BIKES, numParkedBikes);
            localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, serverSyncId);


            sendResult(localIntent);

            response.body().close();
        } catch (JSONException ioe) {
            MiscUtils.errorMsgs.add("handleActionEntry, JSON Exception:: " + ioe);
            setTransactionStateOutOfSync(parkingId);
            sendResult(Constants.TRIGGER_ACTION_ENTRY, Constants.NOK,
                    Constants.ERROR_INVALID_RESPONSE);

        } catch (java.io.IOException ioe) {
            MiscUtils.errorMsgs.add("handleActionEntry, IO Exception:: " + ioe);
            setTransactionStateOutOfSync(parkingId);
            sendResult(Constants.TRIGGER_ACTION_ENTRY, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

        }


    }

    /**
     * Handle action Exit in the provided background thread with the provided
     * parameters.
     */
    private void handleActionExit(String parkingId, long entryTime,long exitTime, double parkingFees,
                                  int vehicleType,String vehicleReg,String token) {

        if (checkForConnectivity(getApplicationContext()) == false) {
            //set the state of the entry object to NOT in sync

            setTransactionStateOutOfSync(parkingId);
            sendResult(Constants.TRIGGER_ACTION_EXIT, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

            return;
        }

        int result = Constants.OK;
        try {
            OkHttpClient client = new OkHttpClient();
            Request      request = buildLogExitRequest(parkingId, entryTime,exitTime, parkingFees,
                    vehicleType, vehicleReg,token);




            Response response = client.newCall(request).execute();
            int occupied = -1;
            int numParkedCars = -1;
            int numParkedBikes = -1;
            int serverSyncId = 0;


            if (response.isSuccessful()) {
                String responseData = response.body().string();
                JSONObject json = new JSONObject(responseData);

                int status = json.getInt("status");

                if (status == Constants.OK) {

                    //Log.v("ResponseData", responseData);
                    occupied = json.getInt("numOccupied");
                    numParkedCars = json.getInt("numParkedCars");
                    numParkedBikes = json.getInt("numParkedBikes");
                    serverSyncId = json.getInt("syncId");

                    /* Issue 96 fix. Sync iD should only be updated if the received syncID == expected */
                    Context context = getApplicationContext();
                    int syncIdAtClient = MiscUtils.getSyncId(context);

                    if ((syncIdAtClient + 1) == serverSyncId) {
                        //Moving the syncID increment after receiveing a successful response
                        MiscUtils.updateSyncId(getApplicationContext(), 1);
                    }
                    //issue109: setting the transaction state as IN sync
                    setTransactionStateInSync(parkingId);

                } else {
                    String reason = json.getString("reason");

                    MiscUtils.errorMsgs.add("handleActionExit, Failed with Reason " + reason);
                    setTransactionStateOutOfSync(parkingId);

                    sendResult(Constants.TRIGGER_ACTION_EXIT, Constants.NOK,
                            reason);
                    response.body().close();

                    return;

                }

            } else {

                String reason = response.message();
                MiscUtils.errorMsgs.add("handleActionExit, Failed with Reason " + reason);
                setTransactionStateOutOfSync(parkingId);

                sendResult(Constants.TRIGGER_ACTION_EXIT, Constants.NOK,
                        reason);
                response.body().close();

                return;

            }

            Intent localIntent = prepareResult(Constants.TRIGGER_ACTION_EXIT, result);
            localIntent.putExtra(Constants.BUNDLE_PARAM_CURRENT_OCCUPANCY, occupied);
            localIntent.putExtra(Constants.BUNDLE_PARAM_CURRENT_PARKED_CARS, numParkedCars);
            localIntent.putExtra(Constants.BUNDLE_PARAM_CURRENT_PARKED_BIKES, numParkedBikes);
            localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, serverSyncId);


            sendResult(localIntent);

        } catch (JSONException ioe) {
            MiscUtils.errorMsgs.add("handleActionExit, JSON Exception:: " + ioe);

            setTransactionStateOutOfSync(parkingId);

            sendResult(Constants.TRIGGER_ACTION_EXIT, Constants.NOK,
                    Constants.ERROR_INVALID_RESPONSE);
        } catch (java.io.IOException ioe) {
            MiscUtils.errorMsgs.add("handleActionExit, IO Exception:: " + ioe);

            setTransactionStateOutOfSync(parkingId);

            sendResult(Constants.TRIGGER_ACTION_EXIT, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);


        }
    }

    /**
     * Handle action Entry in the provided background thread with the provided
     * parameters.
     */
    private void handleActionSpEntry(String bookingId, int bookingType,
                                     String parkingId, long entryTime,
                                     String vehicleReg, int vehicleType,
                                     boolean isParkAndRide) {

        if (checkForConnectivity(getApplicationContext()) == false) {
            //set the state of the entry object to NOT in sync

            setSpTransactionStateOutOfSync(parkingId);
            sendResult(Constants.TRIGGER_ACTION_SP_ENTRY, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

            return;
        }

        int result = Constants.OK;
        try {
            OkHttpClient client = new OkHttpClient();
            Request request = buildLogSpEntryRequest(bookingId, bookingType,
                    parkingId, entryTime, vehicleReg, vehicleType,
                    isParkAndRide);


            Response response = client.newCall(request).execute();
            int occupied = -1;
            int numParkedCars = -1;
            int numParkedBikes = -1;
            int serverSyncId = 0;


            if (response.isSuccessful()) {
                String responseData = response.body().string();
                JSONObject json = new JSONObject(responseData);

                //Log.v("ResponseData", responseData);
                int status = json.optInt("status", Constants.NOK);

                if (status == Constants.OK) {
                    occupied = json.getInt("numOccupied");
                    numParkedCars = json.getInt("numParkedCars");
                    numParkedBikes = json.getInt("numParkedBikes");
                    serverSyncId = json.getInt("syncId");

                    /* Issue 96 fix. Sync iD should only be updated if the received syncID == expected */
                    Context context = getApplicationContext();
                    int syncIdAtClient = MiscUtils.getSyncId(context);

                    if ((syncIdAtClient + 1) == serverSyncId) {
                        //Moving the syncID increment after receiveing a successful response
                        MiscUtils.updateSyncId(getApplicationContext(), 1);
                    }

                    //issue109: setting the transaction state as IN sync
                    setSpTransactionStateInSync(parkingId);
                } else {
                    String reason = json.optString("reason");
                    MiscUtils.errorMsgs.add("handleActionSpEntry, Failed with Reason :: " + reason);
                    setSpTransactionStateOutOfSync(parkingId);
                    sendResult(Constants.TRIGGER_ACTION_SP_ENTRY, Constants.NOK,
                            reason);
                    response.body().close();

                    return;

                }


            } else {

                String reason = response.message();
                MiscUtils.errorMsgs.add("handleActionSpEntry, Failed with Reason :: " + reason);
                setSpTransactionStateOutOfSync(parkingId);
                sendResult(Constants.TRIGGER_ACTION_SP_ENTRY, Constants.NOK,
                        reason);
                response.body().close();

                return;

            }

            Intent localIntent = prepareResult(Constants.TRIGGER_ACTION_SP_ENTRY, result);
            localIntent.putExtra(Constants.BUNDLE_PARAM_CURRENT_OCCUPANCY, occupied);
            localIntent.putExtra(Constants.BUNDLE_PARAM_CURRENT_PARKED_CARS, numParkedCars);
            localIntent.putExtra(Constants.BUNDLE_PARAM_CURRENT_PARKED_BIKES, numParkedBikes);
            localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, serverSyncId);


            sendResult(localIntent);

            response.body().close();
        } catch (JSONException ioe) {
            MiscUtils.errorMsgs.add("handleActionSpEntry, JSON Exception:: " + ioe);
            setSpTransactionStateOutOfSync(parkingId);
            sendResult(Constants.TRIGGER_ACTION_SP_ENTRY, Constants.NOK,
                    Constants.ERROR_INVALID_RESPONSE);

        } catch (java.io.IOException ioe) {
            MiscUtils.errorMsgs.add("handleActionSpEntry, IO Exception:: " + ioe);
            setSpTransactionStateOutOfSync(parkingId);
            sendResult(Constants.TRIGGER_ACTION_SP_ENTRY, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

        }


    }


    /**
     * Handle action Exit in the provided background thread with the provided
     * parameters.
     */
    private void handleActionSpExit(String bookingId, String parkingId, long exitTime, int parkingFees,
                                    int vehicleType, String vehicleReg) {

        if (checkForConnectivity(getApplicationContext()) == false) {
            //set the state of the entry object to NOT in sync

            setSpTransactionStateOutOfSync(parkingId);
            sendResult(Constants.TRIGGER_ACTION_SP_EXIT, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

            return;
        }

        int result = Constants.OK;
        try {
            OkHttpClient client = new OkHttpClient();
            Request request = buildLogSpExitRequest(bookingId, parkingId, exitTime, parkingFees,
                    vehicleType, vehicleReg);


            Response response = client.newCall(request).execute();
            int occupied = -1;
            int numParkedCars = -1;
            int numParkedBikes = -1;
            int serverSyncId = 0;


            if (response.isSuccessful()) {
                String responseData = response.body().string();
                JSONObject json = new JSONObject(responseData);

                int status = json.getInt("status");

                if (status == Constants.OK) {

                    //Log.v("ResponseData", responseData);
                    occupied = json.getInt("numOccupied");
                    numParkedCars = json.getInt("numParkedCars");
                    numParkedBikes = json.getInt("numParkedBikes");
                    serverSyncId = json.getInt("syncId");

                    /* Issue 96 fix. Sync iD should only be updated if the received syncID == expected */
                    Context context = getApplicationContext();
                    int syncIdAtClient = MiscUtils.getSyncId(context);

                    if ((syncIdAtClient + 1) == serverSyncId) {
                        //Moving the syncID increment after receiveing a successful response
                        MiscUtils.updateSyncId(getApplicationContext(), 1);
                    }
                    //issue109: setting the transaction state as IN sync
                    setSpTransactionStateInSync(parkingId);

                } else {
                    String reason = json.getString("reason");

                    MiscUtils.errorMsgs.add("handleActionSpExit, Failed with Reason " + reason);
                    setSpTransactionStateOutOfSync(parkingId);

                    sendResult(Constants.TRIGGER_ACTION_SP_EXIT, Constants.NOK,
                            reason);
                    response.body().close();

                    return;

                }

            } else {

                String reason = response.message();
                MiscUtils.errorMsgs.add("handleActionSpExit, Failed with Reason " + reason);
                setSpTransactionStateOutOfSync(parkingId);

                sendResult(Constants.TRIGGER_ACTION_SP_EXIT, Constants.NOK,
                        reason);
                response.body().close();

                return;

            }

            Intent localIntent = prepareResult(Constants.TRIGGER_ACTION_SP_EXIT, result);
            localIntent.putExtra(Constants.BUNDLE_PARAM_CURRENT_OCCUPANCY, occupied);
            localIntent.putExtra(Constants.BUNDLE_PARAM_CURRENT_PARKED_CARS, numParkedCars);
            localIntent.putExtra(Constants.BUNDLE_PARAM_CURRENT_PARKED_BIKES, numParkedBikes);
            localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, serverSyncId);


            sendResult(localIntent);

        } catch (JSONException ioe) {
            MiscUtils.errorMsgs.add("handleActionSpExit, JSON Exception:: " + ioe);

            setSpTransactionStateOutOfSync(parkingId);

            sendResult(Constants.TRIGGER_ACTION_SP_EXIT, Constants.NOK,
                    Constants.ERROR_INVALID_RESPONSE);
        } catch (java.io.IOException ioe) {
            MiscUtils.errorMsgs.add("handleActionExit, IO Exception:: " + ioe);

            setSpTransactionStateOutOfSync(parkingId);

            sendResult(Constants.TRIGGER_ACTION_SP_EXIT, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);


        }
    }


    /**
     * Handle action RefreshToken in the provided background thread with the provided
     * parameters.
     */
    private void handleActionRefreshToken() {

        if (checkForConnectivity(getApplicationContext()) == false) {
            //set the state of the entry object to NOT in sync

            sendResult(Constants.TRIGGER_ACTION_REFRESH_TOKEN, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);
            PendingActions obj = new PendingActions(Constants.TRIGGER_ACTION_REFRESH_TOKEN);
            obj.save();

            return;
        }
        try {
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);


            Context context = getApplicationContext();

            String oldToken = (String) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_APP_REGISTERATION_TOKEN,
                    Constants.TYPE_STRING);

            Cache.setInCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_APP_REGISTERATION_TOKEN, token,
                    Constants.TYPE_STRING);

            OkHttpClient client = new OkHttpClient();
            Request request = buildRefreshTokenRequest(oldToken, token);

            Response response = client.newCall(request).execute();

            int result = Constants.OK;

            if (!response.isSuccessful()) {

                PendingActions obj = new PendingActions(Constants.TRIGGER_ACTION_REFRESH_TOKEN);
                obj.save();

                sendResult(Constants.TRIGGER_ACTION_REFRESH_TOKEN, Constants.NOK,
                        response.message());
                response.body().close();

                return;

            }

            sendResult(Constants.TRIGGER_ACTION_REFRESH_TOKEN, result);

        } catch (JSONException ioe) {
            MiscUtils.errorMsgs.add("handleActionRefreshToken, JSON Exception:: " + ioe);
            sendResult(Constants.TRIGGER_ACTION_REFRESH_TOKEN, Constants.NOK,
                    Constants.ERROR_INVALID_RESPONSE);
        } catch (java.io.IOException ioe) {
            MiscUtils.errorMsgs.add("handleActionRefreshToken, IO Exception:: " + ioe);
            sendResult(Constants.TRIGGER_ACTION_REFRESH_TOKEN, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

            PendingActions obj = new PendingActions(Constants.TRIGGER_ACTION_REFRESH_TOKEN);
            obj.save();

        }
    }

    /**
     * Handle action GetCurrentOccupancy in the provided background thread with the provided
     * parameters.
     */
    private void handleActionGetCurrentOccupancy() {

        int result = Constants.OK;

        try {

            OkHttpClient client = new OkHttpClient();
            Request request = buildGetCurrentOccupancyRequest();

            Response response = client.newCall(request).execute();

            Integer occupied = -1;
            Integer numParkedCars = -1;
            Integer numParkedBikes = -1;

            if (response.isSuccessful()) {

                String responseData = response.body().string();
                JSONObject json = new JSONObject(responseData);

                occupied = json.getInt("numOccupied");
                numParkedCars = json.getInt("numParkedCars");
                numParkedBikes = json.getInt("numParkedBikes");


                //TODO: we dont send numParkedMiniBus and numParkedBus currently.
                //This primitive is NOT used
                MiscUtils.setOccupancyCounters(getApplicationContext(),
                        numParkedCars, numParkedBikes, 0, 0);

            } else {
                result = Constants.NOK;
            }

            response.body().close();

            Intent localIntent = prepareResult(Constants.TRIGGER_ACTION_GET_CURRENT_OCCUPANCY,
                    result);

            localIntent.putExtra(Constants.BUNDLE_PARAM_CURRENT_OCCUPANCY, occupied);
            localIntent.putExtra(Constants.BUNDLE_PARAM_CURRENT_PARKED_CARS, numParkedCars);
            localIntent.putExtra(Constants.BUNDLE_PARAM_CURRENT_PARKED_BIKES, numParkedBikes);

            sendResult(localIntent);

        } catch (JSONException ioe) {

            MiscUtils.errorMsgs.add("handleActionGetCurrentOccupancy, JSON Exception:: " + ioe);
            sendResult(Constants.TRIGGER_ACTION_GET_CURRENT_OCCUPANCY, Constants.NOK,
                    Constants.ERROR_INVALID_RESPONSE);
        } catch (java.io.IOException ioe) {
            MiscUtils.errorMsgs.add("handleActionGetCurrentOccupancy, IO Exception:: " + ioe);
            sendResult(Constants.TRIGGER_ACTION_GET_CURRENT_OCCUPANCY, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

            PendingActions obj = new PendingActions(Constants.TRIGGER_ACTION_GET_CURRENT_OCCUPANCY);
            obj.save();


        }

    }

    /**
     * Handle action handleActionEntryNotificationFromAnotherApp in the provided background
     * thread with the provided
     * parameters.
     */
    private void handleActionEntryNotificationFromAnotherApp(String parkingId,long entryTime,
                                                             String reg,boolean isParkAndRide,
                                                             int vehicleType,int syncId,
                                                             int operatorId,String operatorDesc,
                                                             String token) {

        //check if we have fresh information than server
        //scenario : car A enters through app 1. app 1 updates the server. The server
        //sends a notification update to app 2. However, app2 is offline.
        //Car A exits through app 2. app 2 comes back online. GCM delivers the update
        //to app 2. However, app2 has fresh information than the server

        //first try to find if a transaction with this parkingId exists

        List<TransactionTbl> transactionLst = TransactionTbl.find(TransactionTbl.class,
                "m_parking_id = ?", parkingId);
        Intent localIntent = prepareResult(Constants.TRIGGER_ACTION_ASYNC_GCM_UPDATE_ENTRY,
                Constants.OK);

        if (transactionLst.isEmpty()) {
            //create new row in the transaction table
            TransactionTbl entryTransaction = new TransactionTbl();

            entryTransaction.setParkingId(parkingId);
            entryTransaction.setEntryTime(entryTime);
            entryTransaction.setCarRegNumber(reg);
            entryTransaction.setIsParkAndRide(isParkAndRide);
            entryTransaction.setParkingStatus(Constants.PARKING_STATUS_ENTERED);
            entryTransaction.setVehicleType(vehicleType);
            entryTransaction.setOperatorId(operatorId);
            entryTransaction.setIsSyncedWithServer(true);
            entryTransaction.setToken(token);

            entryTransaction.save();

            if(!token.isEmpty()){
                ReusableTokensTbl.markTokenAsBusy(token);
            }

            //long openingTime = MiscUtils.getLongOpeningTimeForToday(getApplicationContext());
            //long closingTime = MiscUtils.getLongClosingTimeForToday(getApplicationContext());

            /*if(entryTransaction.getEntryTime() >= openingTime &&
                    entryTransaction.getEntryTime() <= closingTime) {*/
            MiscUtils.incrementOccupancyCount(getApplicationContext(),
                    entryTransaction.getVehicleType());

            localIntent.putExtra(Constants.BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW, true);
            localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);
            localIntent.putExtra(Constants.BUNDLE_PARAM_OPERATOR_DESC, operatorDesc);


        } else {
            //TODO: maybe we need to start the PUSH updates to server procedure
            //This can happen if car 1234 entered through Mobile 1 (which was offline)
            //and exited via parking id through Mobile 2. When Mobile 1 comes online,
            //it sends the GCM update about entry.We can ignore it but in this example
            //scenario need to fill the car reg number

            TransactionTbl existingTransaction = transactionLst.get(0);

            if (existingTransaction.getCarRegNumber().isEmpty()) {
                existingTransaction.setCarRegNumber(reg);
                existingTransaction.save();
            } else if (reg.isEmpty()) {
                //issue109: force the app to send an update
                existingTransaction.setIsSyncedWithServer(false);
                existingTransaction.save();
            }


            //handleActionCalculateOccupancyFromDb();
            localIntent.putExtra(Constants.BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW, true);
            localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);
        }

         /* Issue 96 fix. Sync iD should only be updated if the received syncID == expected */
        Context context = getApplicationContext();
        int syncIdAtClient = MiscUtils.getSyncId(context);

        if ((syncIdAtClient + 1) == syncId) {
            //Moving the syncID increment after receiveing a successful response
            MiscUtils.updateSyncId(context, 1);
        }

        sendResult(localIntent);
    }

    /**
     * Handle action handleActionExitNotificationFromAnotherApp in the provided background
     * thread with the provided
     * parameters.
     */
    private void handleActionExitNotificationFromAnotherApp(String parkingId,
                                                            long entryTime, long exitTime,
                                                            double parkingFees,
                                                            String vehicleReg,int vehicleType,
                                                            int syncId,int operatorId,
                                                            String operatorDesc,
                                                            String token) {

        //first try to find if a transaction with this parkingId exists

        List<TransactionTbl> transactions = TransactionTbl.find(TransactionTbl.class,
                "m_parking_id = ?", parkingId);

        Intent localIntent = prepareResult(Constants.TRIGGER_ACTION_ASYNC_GCM_UPDATE_EXIT,
                Constants.OK);

        long openingTime = MiscUtils.getLongOpeningTimeForToday(getApplicationContext());
        long closingTime = MiscUtils.getLongClosingTimeForToday(getApplicationContext());


        if (!transactions.isEmpty()) {
            TransactionTbl existingTransaction = transactions.get(0);
            boolean isStale = false;

            if (existingTransaction.getParkingStatus() == Constants.PARKING_STATUS_EXITED) {
                isStale = true;
            }

            existingTransaction.setParkingStatus(Constants.PARKING_STATUS_EXITED);
            existingTransaction.setExitTime(exitTime);
            existingTransaction.setParkingFees(parkingFees);

            if (!isStale) {
                existingTransaction.setOperatorId(operatorId);
            }

            //issue109
            if (!existingTransaction.getCarRegNumber().isEmpty() && vehicleReg.isEmpty()) {
                existingTransaction.setIsSyncedWithServer(false);
            }

            existingTransaction.save();

            String existingTransToken = existingTransaction.getToken();

            if(!existingTransToken.isEmpty()){
                ReusableTokensTbl.markTokenAsFree(existingTransToken);
            }


            if (!isStale) {
                if (existingTransaction.getEntryTime() >= openingTime &&
                        existingTransaction.getExitTime() <= closingTime) {

                    MiscUtils.incrementTodaysEarnings(getApplicationContext(),
                            (int) Math.ceil(parkingFees));
                }

                MiscUtils.decrementOccupancyCount(getApplicationContext(),
                        existingTransaction.getVehicleType());
            }

            localIntent.putExtra(Constants.BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW, true);

        } else {
            //create new row in the transaction table
            TransactionTbl exitTransaction = new TransactionTbl();

            exitTransaction.setParkingId(parkingId);
            exitTransaction.setEntryTime(entryTime);
            exitTransaction.setExitTime(exitTime);
            exitTransaction.setParkingStatus(Constants.PARKING_STATUS_EXITED);
            exitTransaction.setParkingFees(parkingFees);
            exitTransaction.setVehicleType(vehicleType);
            exitTransaction.setCarRegNumber(vehicleReg); //currently only provided in prepaid parking
            exitTransaction.setIsSyncedWithServer(true);
            exitTransaction.setToken(token);

            if ((Boolean) Cache.getFromCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY, Constants.TYPE_BOOL)) {
                exitTransaction.setEntryTime(exitTransaction.getExitTime());
            }


            exitTransaction.save();

            if(!token.isEmpty()){
                ReusableTokensTbl.markTokenAsFree(token);
            }

            boolean isParkingPrepaid = (Boolean)Cache.getFromCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY,Constants.TYPE_BOOL);

            if (isParkingPrepaid) {
                MiscUtils.incrementVehiclesParkedCount(getApplicationContext(),
                        exitTransaction.getVehicleType());

                localIntent.putExtra(Constants.BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW, true);
            } else {
                localIntent.putExtra(Constants.BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW, false);

            }


        }


        localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);
        localIntent.putExtra(Constants.BUNDLE_PARAM_OPERATOR_DESC, operatorDesc);

        /* Issue 96 fix. Sync iD should only be updated if the received syncID == expected */
        Context context = getApplicationContext();
        int syncIdAtClient = MiscUtils.getSyncId(context);

        if ((syncIdAtClient + 1) == syncId) {
            //Moving the syncID increment after receiveing a successful response
            MiscUtils.updateSyncId(context, 1);
        }

        sendResult(localIntent);


    }

    /**
     * Handle action handleActionCancelTransactionFromAnoterApp to handle a cancel transaction
     * received from another app
     */


    void handleActionCancelTransactionFromAnotherApp(String parkingId, int operatorId, int syncId,
                                                     String vehicleReg, int vehicleType,
                                                     long entryDateTime,String token){

        //first try to find if a transaction with this parkingId exists

        List<TransactionTbl> transactions = TransactionTbl.find(TransactionTbl.class,
                "m_parking_id = ?", parkingId);

        Intent localIntent = prepareResult(Constants.TRIGGER_ACTION_CANCEL_TRANSACTION_FROM_ANOTHER_APP,
                Constants.OK);


        if (!transactions.isEmpty()) {
            TransactionTbl existingTransaction = transactions.get(0);

            if (existingTransaction.getParkingStatus()
                    != Constants.PARKING_STATUS_TRANSACTION_CANCELLED) {
                existingTransaction.setParkingStatus(Constants.PARKING_STATUS_TRANSACTION_CANCELLED);
                existingTransaction.setOperatorId(operatorId);

                if (existingTransaction.getCarRegNumber().isEmpty() && !vehicleReg.isEmpty()) {
                    existingTransaction.setCarRegNumber(vehicleReg);
                }

                existingTransaction.save();
                String transactionToken = existingTransaction.getToken();

                if(token != transactionToken){
                    //error scnearion
                }

                if(!transactionToken.isEmpty()){
                    ReusableTokensTbl.markTokenAsFree(transactionToken);
                }

                boolean isParkingPrepaid = (Boolean) Cache.getFromCache(getApplicationContext(),
                        Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY, Constants.TYPE_BOOL);

                if (isParkingPrepaid) {
                    MiscUtils.decrementVehiclesParkedCount(getApplicationContext(),
                            existingTransaction.getVehicleType());
                } else {
                    MiscUtils.decrementOccupancyCount(getApplicationContext(),
                            existingTransaction.getVehicleType());
                }
                localIntent.putExtra(Constants.BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW, true);


            } else {

                if (existingTransaction.getCarRegNumber().isEmpty() && !vehicleReg.isEmpty()) {
                    existingTransaction.setCarRegNumber(vehicleReg);
                }
                existingTransaction.save();

                localIntent.putExtra(Constants.BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW, false);

            }


        } else {
            //create a new transactions
            TransactionTbl transaction = new TransactionTbl();
            transaction.setParkingStatus(Constants.PARKING_STATUS_TRANSACTION_CANCELLED);
            transaction.setOperatorId(operatorId);
            transaction.setParkingId(parkingId);
            transaction.setEntryTime(entryDateTime);
            transaction.setVehicleType(vehicleType);
            transaction.setCarRegNumber(vehicleReg);
            //issue109
            transaction.setIsSyncedWithServer(true);
            transaction.setToken(token);

            transaction.save();

            if(!token.isEmpty()){
                ReusableTokensTbl.markTokenAsFree(token);
            }
            localIntent.putExtra(Constants.BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW, false);

        }

        /* Issue 96 fix. Sync iD should only be updated if the received syncID == expected */
        Context context = getApplicationContext();
        int syncIdAtClient = MiscUtils.getSyncId(context);

        if ((syncIdAtClient + 1) == syncId) {
            //Moving the syncID increment after receiveing a successful response
            MiscUtils.updateSyncId(context, 1);
        }


        localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);

        sendResult(localIntent);


    }


    /**
     * Handle action handleActionUpdateTransactionFromAnoterApp to handle a cancel transaction
     * received from another app
     */

    void handleActionUpdateTransactionFromAnotherApp(String parkingId, int operatorId,
                                                     int parkingStatus,
                                                     int parkingFees, int syncId,
                                                     String vehicleReg,int vehicleType,
                                                     long entryDateTime,String token){

        //first try to find if a transaction with this parkingId exists

        List<TransactionTbl> transactions = TransactionTbl.find(TransactionTbl.class,
                "m_parking_id = ?", parkingId);

        Intent localIntent = prepareResult(Constants.TRIGGER_ACTION_UPDATE_TRANSACTION_FROM_ANOTHER_APP,
                Constants.OK);


        if (!transactions.isEmpty()) {
            TransactionTbl existingTransaction = transactions.get(0);

            int existingParkingStatus = existingTransaction.getParkingStatus();

            if (existingParkingStatus != parkingStatus) {

                long openingTime = MiscUtils.getLongOpeningTimeForToday(getApplicationContext());
                long closingTime = MiscUtils.getLongClosingTimeForToday(getApplicationContext());

                if (existingTransaction.getEntryTime() >= openingTime &&
                        existingTransaction.getExitTime() <= closingTime) {
                    MiscUtils.decrementTodaysEarnings(getApplicationContext(),
                            (int) Math.ceil(existingTransaction.getParkingFees()));
                }


                existingTransaction.setParkingStatus(parkingStatus);
                existingTransaction.setParkingFees(parkingFees);
                existingTransaction.setOperatorId(operatorId);


                MiscUtils.incrementOccupancyCount(getApplicationContext(),
                        existingTransaction.getVehicleType());

                localIntent.putExtra(Constants.BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW, true);
                localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);

                //Issue109: if received vehicle reg is empty and this app has the vehicle reg,
                //send an update
                if (vehicleReg.isEmpty() && !existingTransaction.getCarRegNumber().isEmpty()) {
                    existingTransaction.setIsSyncedWithServer(false);
                }

                existingTransaction.save();

                String existingTransToken = existingTransaction.getToken();

                if(!existingTransToken.isEmpty()){
                    ReusableTokensTbl.markTokenAsBusy(existingTransToken);
                }

            }else{
                //Issue109: if received vehicle reg is empty and this app has the vehicle reg,
                //send an update
                if (vehicleReg.isEmpty() && !existingTransaction.getCarRegNumber().isEmpty()) {
                    existingTransaction.setIsSyncedWithServer(false);
                }

                localIntent.putExtra(Constants.BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW, false);
                localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);

            }

        } else {
            //create a new transactions
            TransactionTbl transaction = new TransactionTbl();
            transaction.setParkingStatus(parkingStatus);
            transaction.setParkingFees(parkingFees);
            transaction.setOperatorId(operatorId);
            transaction.setParkingId(parkingId);
            transaction.setEntryTime(entryDateTime);
            transaction.setVehicleType(vehicleType);
            transaction.setCarRegNumber(vehicleReg);
            transaction.setIsSyncedWithServer(true);
            transaction.setToken(token);

            transaction.save();

            if(!token.isEmpty()){
                ReusableTokensTbl.markTokenAsBusy(token);
            }

            localIntent.putExtra(Constants.BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW, false);
            localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);

        }

        /* Issue 96 fix. Sync iD should only be updated if the received syncID == expected */
        Context context = getApplicationContext();
        int syncIdAtClient = MiscUtils.getSyncId(context);

        if ((syncIdAtClient + 1) == syncId) {
            //Moving the syncID increment after receiveing a successful response
            MiscUtils.updateSyncId(context, 1);
        }

        sendResult(localIntent);


    }


    /**
     * Handle action handleActionSpEntryNotificationFromAnotherApp in the provided background
     * thread with the provided
     * parameters.
     */
    private void handleActionSpEntryNotificationFromAnotherApp(String bookingId, String parkingId,
                                                               String vehicleReg, int vehicleType,
                                                               int syncId, long entryTime,
                                                               int operatorId, String operatorDesc,
                                                               boolean isParkAndRide) {
        //check if we have fresh information than server
        //scenario : car A enters through app 1. app 1 updates the server. The server
        //sends a notification update to app 2. However, app2 is offline.
        //Car A exits through app 2. app 2 comes back online. GCM delivers the update
        //to app 2. However, app2 has fresh information than the server

        //first try to find if a transaction with this parkingId exists

        List<SimplyParkBookingTbl> bookingList = SimplyParkBookingTbl.find(
                SimplyParkBookingTbl.class, "m_booking_id = ?", bookingId);


        if (bookingList.isEmpty()) {
            //invalid update received or a transaction update is received
            //before a booking update is received.
            sendResult(Constants.TRIGGER_ACTION_ASYNC_GCM_UPDATE_SP_ENTRY, Constants.NOK,
                    Constants.ERROR_NO_SP_BOOKING_AVAILABLE);
        }

        List<SimplyParkTransactionTbl> transactionLst = SimplyParkTransactionTbl.find(
                SimplyParkTransactionTbl.class,
                "m_parking_id = ?", parkingId);

        Intent localIntent = prepareResult(Constants.TRIGGER_ACTION_ASYNC_GCM_UPDATE_SP_ENTRY,
                Constants.OK);

        if (transactionLst.isEmpty()) {
            SimplyParkBookingTbl booking = bookingList.get(0);


            //create new row in the transaction table
            SimplyParkTransactionTbl entryTransaction = new SimplyParkTransactionTbl();

            entryTransaction.setBookingId(bookingId);
            entryTransaction.setParkingId(parkingId);
            entryTransaction.setVehicleReg(vehicleReg);
            entryTransaction.setVehicleType(vehicleType);
            entryTransaction.setEntryTime(entryTime);
            entryTransaction.setIsParkAndRide(isParkAndRide);
            entryTransaction.setOperatorId(operatorId);

            boolean addEntry = true;


            //an exit happend through this app. However, the app had no entry information.
            //booking numSlotsInUse was reduced but the server was not synced that the pass
            //has also exited.
            List<PendingExits> pendingExits = booking.getPendingExits();
            if (!pendingExits.isEmpty()) {

                for (ListIterator<PendingExits> iter = pendingExits.listIterator();
                     iter.hasNext(); ) {
                    PendingExits exit = iter.next();
                    if (exit.getVehicleReg().equals(entryTransaction.getVehicleReg())) {
                        entryTransaction.setStatus(Constants.PARKING_STATUS_EXITED);
                        entryTransaction.setExitTime(exit.getExitDateTime());
                        entryTransaction.setIsSyncedWithServer(false);
                        entryTransaction.save();
                        addEntry = false;


                        //Intent service to update the server
                        ServerUpdationIntentService.startActionSimplyParkLogExit(getApplicationContext(),
                                entryTransaction.getBookingId(),
                                entryTransaction.getParkingId(),
                                entryTransaction.getExitTime(),
                                entryTransaction.getParkingFees(),
                                entryTransaction.getVehicleType(),
                                entryTransaction.getVehicleReg());

                        exit.delete();
                        break;
                    }
                }
            }


            if (addEntry) {
                entryTransaction.setStatus(Constants.PARKING_STATUS_ENTERED);
                entryTransaction.setIsSyncedWithServer(true);

                entryTransaction.save();

                MiscUtils.incrementOccupancyCount(getApplicationContext(),
                        entryTransaction.getVehicleType());

                booking.incrementSlotsInUse(getApplicationContext(), vehicleType);
                booking.save();
            }


            localIntent.putExtra(Constants.BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW, true);
            localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);
            localIntent.putExtra(Constants.BUNDLE_PARAM_OPERATOR_DESC, operatorDesc);


        } else {
            //TODO: maybe we need to start the PUSH updates to server procedure
            //This can happen if car 1234 entered through Mobile 1 (which was offline)
            //and exited via parking id through Mobile 2. When Mobile 1 comes online,
            //it sends the GCM update about entry.We can ignore it but in this example
            //scenario need to fill the car reg number

            SimplyParkTransactionTbl existingTransaction = transactionLst.get(0);
            SimplyParkBookingTbl booking = bookingList.get(0);


            if (existingTransaction.getVehicleReg().isEmpty()) {
                existingTransaction.setVehicleReg(vehicleReg);

            }


            /* This code is NO longer required
            //case where a Car enters through App A. A is offline and does not update B.
            //car exits through B but it did not have booking ID information.
            //Then A comes online and sends an update
            if(existingTransaction.getBookingId().isEmpty()){
                existingTransaction.setBookingId(booking.getBookingID());


                if(existingTransaction.getStatus() == Constants.PARKING_STATUS_EXITED){
                    booking.decrementSlotsInUse(getApplicationContext(),
                            existingTransaction.getVehicleType());

                }
            }
            */

            existingTransaction.save();
            booking.save();


            handleActionCalculateOccupancyFromDb();
            localIntent.putExtra(Constants.BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW, true);
            localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);
        }

        /* Issue 96 fix. Sync iD should only be updated if the received syncID == expected */
        Context context = getApplicationContext();
        int syncIdAtClient = MiscUtils.getSyncId(context);

        if ((syncIdAtClient + 1) == syncId) {
            //Moving the syncID increment after receiveing a successful response
            MiscUtils.updateSyncId(context, 1);
        }
        //think of what to do with this sync id. as the update could be a stale one

        sendResult(localIntent);
    }

    /**
     * Handle action handleActionSpExitNotificationFromAnotherApp in the provided background
     * thread with the provided
     * parameters.
     */
    private void handleActionSpExitNotificationFromAnotherApp(String bookingId, String parkingId,
                                                              int parkingFees, long exitTime,
                                                              int syncId, int vehicleType,
                                                              int operatorId, String operatorDesc) {

        //first try to find if a transaction with this parkingId exists

        List<SimplyParkTransactionTbl> transactions = SimplyParkTransactionTbl.find(
                SimplyParkTransactionTbl.class,
                "m_parking_id = ?", parkingId);

        List<SimplyParkBookingTbl> bookingList = SimplyParkBookingTbl.find(
                SimplyParkBookingTbl.class, "m_booking_id = ?", bookingId);


        if (bookingList.isEmpty()) {
            //invalid update received or a transaction update is received
            //before a booking update is received.
            sendResult(Constants.TRIGGER_ACTION_ASYNC_GCM_UPDATE_SP_ENTRY, Constants.NOK,
                    Constants.ERROR_NO_SP_BOOKING_AVAILABLE);
        }


        Intent localIntent = prepareResult(Constants.TRIGGER_ACTION_ASYNC_GCM_UPDATE_SP_EXIT,
                Constants.OK);

        long openingTime = MiscUtils.getLongOpeningTimeForToday(getApplicationContext());
        long closingTime = MiscUtils.getLongClosingTimeForToday(getApplicationContext());


        if (!transactions.isEmpty()) {
            SimplyParkTransactionTbl existingTransaction = transactions.get(0);
            boolean isStale = false;

            if (existingTransaction.getStatus() == Constants.PARKING_STATUS_EXITED) {
                isStale = true;
            }

            existingTransaction.setStatus(Constants.PARKING_STATUS_EXITED);
            existingTransaction.setExitTime(exitTime);
            existingTransaction.setParkingFees(parkingFees);
            existingTransaction.setOperatorId(operatorId);

            existingTransaction.save();


            if (!isStale) {
                MiscUtils.decrementOccupancyCount(getApplicationContext(),
                        existingTransaction.getVehicleType());

                SimplyParkBookingTbl booking = bookingList.get(0);

                booking.decrementSlotsInUse(getApplicationContext(), vehicleType);
                booking.save();
            }

            localIntent.putExtra(Constants.BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW, true);

        } else {
            //create new row in the transaction table
            SimplyParkTransactionTbl exitTransaction = new SimplyParkTransactionTbl();

            exitTransaction.setParkingId(parkingId);
            exitTransaction.setExitTime(exitTime);
            exitTransaction.setStatus(Constants.PARKING_STATUS_EXITED);
            exitTransaction.setParkingFees(parkingFees);
            exitTransaction.setVehicleType(vehicleType);
            exitTransaction.setBookingId(bookingId);
            exitTransaction.setOperatorId(operatorId);
            exitTransaction.setIsSyncedWithServer(true);

            exitTransaction.save();


            localIntent.putExtra(Constants.BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW, false);


        }


        localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);
        localIntent.putExtra(Constants.BUNDLE_PARAM_OPERATOR_DESC, operatorDesc);


        /* Issue 96 fix. Sync iD should only be updated if the received syncID == expected */
        Context context = getApplicationContext();
        int syncIdAtClient = MiscUtils.getSyncId(context);

        if ((syncIdAtClient + 1) == syncId) {
            //Moving the syncID increment after receiveing a successful response
            MiscUtils.updateSyncId(context, 1);
        }

        sendResult(localIntent);


    }


    /**
     * Handle action handleActionSpCancelTransactionFromAnoterApp to handle a cancel transaction
     * received from another app
     */


    void handleActionSpCancelTransactionFromAnotherApp(String parkingId, int operatorId, int syncId,
                                                       String vehicleReg, int vehicleType, long entryTime,
                                                       String bookingId, int bookingType) {

        //first try to find if a transaction with this parkingId exists

        List<SimplyParkTransactionTbl> transactions = SimplyParkTransactionTbl.find(
                SimplyParkTransactionTbl.class,
                "m_parking_id = ?", parkingId);

        Intent localIntent = prepareResult(
                Constants.TRIGGER_ACTION_SP_CANCEL_TRANSACTION_FROM_ANOTHER_APP,
                Constants.OK);


        if (!transactions.isEmpty()) {
            SimplyParkTransactionTbl existingTransaction = transactions.get(0);

            if (existingTransaction.getVehicleReg().isEmpty() && !vehicleReg.isEmpty()) {
                existingTransaction.setVehicleReg(vehicleReg);
            }

            if (existingTransaction.getStatus()
                    != Constants.PARKING_STATUS_TRANSACTION_CANCELLED) {
                existingTransaction.setStatus(Constants.PARKING_STATUS_TRANSACTION_CANCELLED);
                existingTransaction.setOperatorId(operatorId);
                MiscUtils.decrementOccupancyCount(getApplicationContext(),
                        existingTransaction.getVehicleType());
                localIntent.putExtra(Constants.BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW, true);
            } else {
                localIntent.putExtra(Constants.BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW, false);


            }

            existingTransaction.save();


        } else {
            //create a new transction
            SimplyParkTransactionTbl transaction = new SimplyParkTransactionTbl();
            transaction.setIsSyncedWithServer(true);
            transaction.setParkingId(parkingId);
            transaction.setBookingId(bookingId);
            transaction.setEntryTime(entryTime);
            transaction.setOperatorId(operatorId);
            transaction.setVehicleReg(vehicleReg);
            transaction.setVehicleType(vehicleType);
            transaction.setStatus(Constants.PARKING_STATUS_TRANSACTION_CANCELLED);

            transaction.save();

        }


        localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);

         /* Issue 96 fix. Sync iD should only be updated if the received syncID == expected */
        Context context = getApplicationContext();
        int syncIdAtClient = MiscUtils.getSyncId(context);

        if ((syncIdAtClient + 1) == syncId) {
            //Moving the syncID increment after receiveing a successful response
            MiscUtils.updateSyncId(context, 1);
        }

        sendResult(localIntent);


    }

    /**
     * Handle action handleActionUpdateTransactionFromAnoterApp to handle a cancel transaction
     * received from another app
     */

    void handleActionSpUpdateTransactionFromAnotherApp(String parkingId, int parkingStatus,
                                                       int parkingFees, int syncId, int operatorId,
                                                       String vehicleReg, int vehicleType, long entryTime, long exitTime,
                                                       String bookingId, int bookingType) {


        //first try to find if a transaction with this parkingId exists

        List<SimplyParkTransactionTbl> transactionsList = SimplyParkTransactionTbl.find(
                SimplyParkTransactionTbl.class,
                "m_parking_id = ?", parkingId);

        Intent localIntent = prepareResult(
                Constants.TRIGGER_ACTION_SP_UPDATE_TRANSACTION_FROM_ANOTHER_APP,
                Constants.OK);


        if (!transactionsList.isEmpty()) {
            SimplyParkTransactionTbl existingTransaction = transactionsList.get(0);

            int existingParkingStatus = existingTransaction.getStatus();

            if (existingTransaction.getVehicleReg().isEmpty() && !vehicleReg.isEmpty()) {
                existingTransaction.setVehicleReg(vehicleReg);
            }

            if (existingParkingStatus != parkingStatus) {

                existingTransaction.setStatus(parkingStatus);
                existingTransaction.setParkingFees(parkingFees);
                existingTransaction.setOperatorId(operatorId);


                MiscUtils.incrementOccupancyCount(getApplicationContext(),
                        existingTransaction.getVehicleType());

                localIntent.putExtra(Constants.BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW, true);
                localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);

            } else {
                localIntent.putExtra(Constants.BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW, false);
                localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);

            }

            existingTransaction.save();


        } else {
            //create a new transction
            SimplyParkTransactionTbl transaction = new SimplyParkTransactionTbl();
            transaction.setIsSyncedWithServer(true);
            transaction.setParkingId(parkingId);
            transaction.setBookingId(bookingId);
            transaction.setEntryTime(entryTime);
            transaction.setOperatorId(operatorId);
            transaction.setVehicleReg(vehicleReg);
            transaction.setVehicleType(vehicleType);
            transaction.setStatus(parkingStatus);
            transaction.setParkingFees(parkingFees);

            transaction.save();

        }

        /* Issue 96 fix. Sync iD should only be updated if the received syncID == expected */
        Context context = getApplicationContext();
        int syncIdAtClient = MiscUtils.getSyncId(context);

        if ((syncIdAtClient + 1) == syncId) {
            //Moving the syncID increment after receiveing a successful response
            MiscUtils.updateSyncId(context, 1);
        }

        sendResult(localIntent);


    }

    /**
     * Handle action handleActionCancelMonthlyPass to handle a cancel monthly pass
     */

    private void handleActionCancelMonthlyPass(String bookingId) {
        List<SimplyParkBookingTbl> bookingList = SimplyParkBookingTbl.find(
                SimplyParkBookingTbl.class, "m_booking_id = ?", bookingId);

        if (bookingList.isEmpty()) {
            //nothing to do
            return;
        }

        SimplyParkBookingTbl booking = bookingList.get(0);

        booking.setBookingStatus(Constants.BOOKING_STATUS_CANCELLED);
        booking.save();

        handleActionEndOfDay(); //to update the UI in case the number of active passes change
    }

    private void handleActionMonthlyPassGenerated(String bookingId, String customer, String email,
                                                  String mobile, int bookingStatus,
                                                  long startTime, long endTime, long passIssueDate,
                                                  String encodedPassCode, String serialNumber,
                                                  String vehicleReg, int vehicleType,
                                                  int numBookedSlots, int parkingFees, int discount,
                                                  boolean isRenewal, String parentBookingId,
                                                  boolean isStaffPass, int paymentMode, String invoiceId,
                                                  String paymentId) {

        if (checkForConnectivity(getApplicationContext()) == false) {
            //set the state of the entry object to NOT in sync

            setMonthlyPassGenerationOutOfSync(bookingId);
            sendResult(Constants.TRIGGER_ACTION_MONTHLY_PASS_GENERATED, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

            return;

        }

        int result = Constants.OK;
        try {
            OkHttpClient client = new OkHttpClient();
            Request request = buildCreateBookingRequest(bookingId, customer, email, mobile,
                    bookingStatus, startTime, endTime, passIssueDate,
                    encodedPassCode, serialNumber, vehicleReg, vehicleType,
                    numBookedSlots, parkingFees, discount,
                    isRenewal, parentBookingId, isStaffPass, paymentMode, invoiceId,
                    paymentId);


            Response response = client.newCall(request).execute();

            int serverSyncId = 0;


            if (response.isSuccessful()) {
                String responseData = response.body().string();
                JSONObject json = new JSONObject(responseData);

                int status = json.getInt("status");

                if (status == Constants.OK) {

                    //Log.v("ResponseData", responseData);

                    serverSyncId = json.getInt("syncId");

                    /* Issue 96 fix. Sync iD should only be updated if the received syncID == expected */
                    Context context = getApplicationContext();
                    int syncIdAtClient = MiscUtils.getSyncId(context);

                    if ((syncIdAtClient + 1) == serverSyncId) {
                        //Moving the syncID increment after receiveing a successful response
                        MiscUtils.updateSyncId(getApplicationContext(), 1);
                    }

                    //issue109: setting the booking state as synced
                    setMonthlyPassGenerationInSync(bookingId);
                } else {
                    String reason = json.getString("reason");

                    MiscUtils.errorMsgs.add("handleActionMonthlyPassGenerated, " +
                            "Failed with Reason " + reason);
                    setMonthlyPassGenerationOutOfSync(bookingId);

                    sendResult(Constants.TRIGGER_ACTION_MONTHLY_PASS_GENERATED, Constants.NOK,
                            reason);
                    response.body().close();

                    return;

                }

            } else {

                String reason = response.message();
                MiscUtils.errorMsgs.add("handleActionMonthlyPassGenerated, Failed with Reason " + reason);
                setMonthlyPassGenerationOutOfSync(bookingId);

                sendResult(Constants.TRIGGER_ACTION_MONTHLY_PASS_GENERATED, Constants.NOK,
                        reason);
                response.body().close();

                return;

            }

            Intent localIntent = prepareResult(Constants.TRIGGER_ACTION_MONTHLY_PASS_GENERATED, result);
            localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, serverSyncId);


            sendResult(localIntent);

        } catch (JSONException ioe) {
            MiscUtils.errorMsgs.add("handleActionMonthlyPassGenerated, JSON Exception:: " + ioe);

            setMonthlyPassGenerationOutOfSync(bookingId);

            sendResult(Constants.TRIGGER_ACTION_MONTHLY_PASS_GENERATED, Constants.NOK,
                    Constants.ERROR_INVALID_RESPONSE);
        } catch (java.io.IOException ioe) {
            MiscUtils.errorMsgs.add("handleActionMonthlyPassGenerated, IO Exception:: " + ioe);

            setMonthlyPassGenerationOutOfSync(bookingId);

            sendResult(Constants.TRIGGER_ACTION_MONTHLY_PASS_GENERATED, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);


        }


    }


    /**
     * Handle action handleActionBookingUpdateFromServer to trigger action when bookingUpdate is
     * received from the server
     */


    private void handleActionOpAppBookingUpdate(SimplyParkBookingTbl bookingParams, int spaceOwnerId,
                                                int syncId, String invoiceId, String paymentId) {


        Intent localIntent = prepareResult(
                Constants.TRIGGER_ACTION_OPAPP_BOOKING_UPDATE_RECEIVED,
                Constants.OK);

        localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);


        Cache.setInCache(getApplicationContext(), Constants.CACHE_PREFIX_SPACE_OWNER_ID_QUERY,
                spaceOwnerId, Constants.TYPE_INT);

        List<SimplyParkBookingTbl> bookingList = SimplyParkBookingTbl.find(
                SimplyParkBookingTbl.class,
                "m_booking_id = ?", bookingParams.getBookingID());

        SimplyParkBookingTbl booking;
        boolean isNew = false;
        int oldBookingStatus = Constants.BOOKING_STATUS_WAITING;

        if (!bookingList.isEmpty()) {
            booking = bookingList.get(0);
            booking.setSlotsInUse(0);
            oldBookingStatus = booking.getBookingStatus();
        } else {
            booking = new SimplyParkBookingTbl();
            //issue109: a newly received booking is already synced
            booking.setIsSyncedWithServer(true);
            isNew = true;
        }

        booking.setBookingID(bookingParams.getBookingID());
        booking.setBookingStatus(bookingParams.getBookingStatus());
        booking.setBookingType(bookingParams.getBookingType());
        booking.setCustomerDesc(bookingParams.getCustomerDesc());
        booking.setSerialNumber(bookingParams.getSerialNumber());

        booking.setBookingStartTime(bookingParams.getBookingStartTime());
        booking.setBookingEndTime(bookingParams.getBookingEndTime());
        booking.setPassIssueDate(bookingParams.getPassIssueDate());
        booking.setNumBookedSlots(bookingParams.getNumBookedSlots());
        booking.setParkingFees(bookingParams.getParkingFees());
        booking.setOperatorId(bookingParams.getOperatorId());

        booking.setIsSyncedWithServer(true);

        boolean isSlotOccupiedAssumed = (Boolean) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_IS_SLOT_OCCUPIED_ASSUMED_QUERY, Constants.TYPE_BOOL);

        if (isSlotOccupiedAssumed) {
            booking.setIsSlotOccupiedAssumed(true);
            handleActionEndOfDay();
        } else {
            booking.setIsSlotOccupiedAssumed(false);
        }

        booking.setIsRenewalBooking(bookingParams.getIsRenewalBooking());
        booking.setParentBookingID(bookingParams.getParentBookingID());

        booking.setPaymentMode(bookingParams.getPaymentMode());

        booking.save();

        if (booking.getPaymentMode() == Constants.PAYMENT_MODE_CARD) {

            PaymentInfoTbl paymentInfo = booking.getPaymentInfo();

            if (paymentInfo == null) {
                paymentInfo = new PaymentInfoTbl();
            }

            paymentInfo.setBookingId(booking.getBookingID());
            paymentInfo.setInvoiceId(invoiceId);
            paymentInfo.setPaymentId(paymentId);
            paymentInfo.save();
        }

        if (booking.getIsRenewalBooking()) {
            List<SimplyParkBookingTbl> parentBookingList = SimplyParkBookingTbl.find(
                    SimplyParkBookingTbl.class, "m_booking_id = ?", booking.getParentBookingID());

            if (!parentBookingList.isEmpty()) {
                SimplyParkBookingTbl parentBooking = parentBookingList.get(0);
                long nextRenewalDate = parentBooking.getNextRenewalDateTime();
                long toDateTime = booking.getBookingEndTime();

                if (nextRenewalDate < toDateTime) {
                    parentBooking.setNextRenewalDateTime(toDateTime);
                    parentBooking.save();
                }
            }
        }

        DateTime today = DateTime.today(Constants.TIME_ZONE);
        long todayInMs = today.getMilliseconds(Constants.TIME_ZONE);

        long startDateTime = booking.getBookingStartTime();
        long endDateTime = booking.getBookingEndTime();
        boolean isActiveToday = false;
        if (todayInMs >= startDateTime && todayInMs <= endDateTime) {
            isActiveToday = true;
        }


        int updatedStatus = booking.getBookingStatus();

        if (isNew) {
            if (updatedStatus != Constants.BOOKING_STATUS_CANCELLED ||
                    updatedStatus != Constants.BOOKING_STATUS_EXPIRED) {
                MiscUtils.incrementMonthlyPassCounters(getApplicationContext(), isActiveToday);
            } else {
                //passing false as its a new booking therefore this was never added to the active
                //passes
                MiscUtils.decrementMonthlyPassCounters(getApplicationContext(), false);
            }
        } else {
            if (oldBookingStatus != updatedStatus) {
                //Currently we only send an update when a new booking is created. Therefore,
                //this scenario is currently not possible
            }
        }

         /* Issue 96 fix. Sync iD should only be updated if the received syncID == expected */
        Context context = getApplicationContext();
        int syncIdAtClient = MiscUtils.getSyncId(context);

        if ((syncIdAtClient + 1) == syncId) {
            //Moving the syncID increment after receiveing a successful response
            MiscUtils.updateSyncId(context, 1);
        }


        sendResult(localIntent);

    }

    /**
     * Handle action handleActionBookingUpdateFromServer to trigger action when bookingUpdate is
     * received from the server
     */


    private void handleActionBookingUpdateFromServer(String serialNumber, String bookingId,
                                                     String customer, int bookingType,
                                                     String startTime, String endTime,
                                                     String bookingTime,
                                                     int numBookedSlots, int bookingStatus,
                                                     int syncId, int spaceOwnerId) {


        Intent localIntent = prepareResult(
                Constants.TRIGGER_ACTION_BOOKING_UPDATE_FROM_SERVER,
                Constants.OK);

        localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);


        Cache.setInCache(getApplicationContext(), Constants.CACHE_PREFIX_SPACE_OWNER_ID_QUERY,
                spaceOwnerId, Constants.TYPE_INT);

        List<SimplyParkBookingTbl> bookingList = SimplyParkBookingTbl.find(
                SimplyParkBookingTbl.class,
                "m_booking_id = ?", bookingId);

        SimplyParkBookingTbl booking;

        if (!bookingList.isEmpty()) {
            booking = bookingList.get(0);
            booking.setSlotsInUse(0);
        } else {
            booking = new SimplyParkBookingTbl();
            //Issue109:
            booking.setIsSyncedWithServer(true);
        }

        booking.setBookingID(bookingId);
        booking.setBookingStatus(bookingStatus);
        booking.setBookingType(bookingType);
        booking.setCustomerDesc(customer);
        booking.setSerialNumber(serialNumber);

        DateTime startDate = new DateTime(startTime);
        booking.setBookingStartTime(startDate.getMilliseconds(Constants.TIME_ZONE));

        DateTime endDate = new DateTime(endTime);
        booking.setBookingEndTime(endDate.getMilliseconds(Constants.TIME_ZONE));

        DateTime passIssueDate = new DateTime(bookingTime);
        booking.setPassIssueDate(passIssueDate.getMilliseconds(Constants.TIME_ZONE));


        booking.setNumBookedSlots(numBookedSlots);


        booking.save();

        /* Issue 96 fix. Sync iD should only be updated if the received syncID == expected */
        Context context = getApplicationContext();
        int syncIdAtClient = MiscUtils.getSyncId(context);

        if ((syncIdAtClient + 1) == syncId) {
            //Moving the syncID increment after receiveing a successful response
            MiscUtils.updateSyncId(context, 1);
        }


        sendResult(localIntent);

    }


    /**
     * Handle action handleActionTriggerPendingActions to trigger all pending actions
     * This is used when we lost internet connectivity and now have regained it
     */

    private void handleActionTriggerPendingActions() {
        List<PendingActions> actionsList = PendingActions.listAll(PendingActions.class);
        Context context = getApplicationContext();

        if (!actionsList.isEmpty()) {


            for (ListIterator<PendingActions> iterator = actionsList.listIterator();
                 iterator.hasNext(); ) {
                PendingActions obj = iterator.next();

                switch (obj.getActionName()) {
                    case Constants.TRIGGER_ACTION_REGISTER:
                        startActionRegister(context);
                        break;

                    case Constants.TRIGGER_ACTION_DEREGISTER:
                        //TODO:implement ACTION DEREGISTER
                        startActionDeRegister(context, EXTRA_PARAM1, EXTRA_PARAM2);
                        break;

                    case Constants.TRIGGER_ACTION_REFRESH_TOKEN:
                        startActionRefreshToken(context);
                        break;
                    case Constants.TRIGGER_ACTION_GET_SPACE_OPERATING_HOURS:
                        startActionGetOperatingHours(context);

                }


                PendingActions.deleteAll(PendingActions.class);

            }
        }


    }

    /**
     * Handle action handleActionEndOfDay to trigger a count of monthly
     * passes which will expire today and become active today
     */
    private void handleActionEndOfDay() {

        List<SimplyParkBookingTbl> bookingList = SimplyParkBookingTbl.find(
                SimplyParkBookingTbl.class, "m_is_slot_occupied_assumed = ? AND m_booking_status != ?",
                Integer.toString(1), Integer.toString(Constants.BOOKING_STATUS_EXPIRED));

        if (bookingList.isEmpty()) {
            //nothing to do
            return;
        }

        Intent localIntent = prepareResult(
                Constants.TRIGGER_ACTION_END_OF_DAY,
                Constants.OK);

        DateTime today = DateTime.today(Constants.TIME_ZONE);
        long todayInMs = today.getMilliseconds(Constants.TIME_ZONE);
        int activeToday = 0;
        int deactiveToday = 0;


        for (Iterator<SimplyParkBookingTbl> iter = bookingList.listIterator(); iter.hasNext(); ) {
            SimplyParkBookingTbl booking = iter.next();

            long startDateTime = booking.getBookingStartTime();
            long endDateTime = booking.getBookingEndTime();

            if (todayInMs >= startDateTime && todayInMs <= endDateTime) {
                if (booking.getBookingStatus() != Constants.BOOKING_STATUS_EXPIRED &&
                        booking.getBookingStatus() != Constants.BOOKING_STATUS_CANCELLED) {
                    activeToday++;
                    if (booking.getIsSlotOccupiedAssumed()) {
                        int bookedSlots = booking.getNumBookedSlots();
                        booking.setSlotsInUse(bookedSlots);
                    }
                }

            } else if (todayInMs > endDateTime) {
                deactiveToday++;
                booking.setBookingStatus(Constants.BOOKING_STATUS_EXPIRED);
                booking.setSlotsInUse(0);
            }

            booking.save();

        }

        Context context = getApplicationContext();
        MiscUtils.setActiveMonthlyPassesForToday(context, activeToday);
        MiscUtils.setInActiveMonthlyPasses(context, deactiveToday);


        boolean isParkingPrepaid = (Boolean) Cache.getFromCache(context,
                Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY,
                Constants.TYPE_BOOL);

        if (isParkingPrepaid) {
            List<PendingActions> resetOccupancy = PendingActions.find(PendingActions.class,
                    "m_action_name = ?",
                    Integer.toString(Constants.TRIGGER_ACTION_RESET_OCCUPANCY_COUNT)
            );

            if (!resetOccupancy.isEmpty()) {
                MiscUtils.resetOccupancyCount(context);
                PendingActions.deleteInTx(resetOccupancy);
            }
        }

        sendResult(localIntent);

    }


    /**
     * Handle action handleActionGetSpaceOperatingHours to trigger record exit of
     * all cars/bikes currently in the parking lot
     */
    private void handleActionGetSpaceOperatingHours() {

        int result = Constants.OK;
        String reason = "";

        if (checkForConnectivity(getApplicationContext()) == false) {
            //set the state of the entry object to NOT in sync

            sendResult(Constants.TRIGGER_ACTION_GET_SPACE_OPERATING_HOURS, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

            PendingActions obj = new PendingActions(
                    Constants.TRIGGER_ACTION_GET_SPACE_OPERATING_HOURS);
            obj.save();

            return;
        }

        try {

            OkHttpClient client = new OkHttpClient();
            Request request = buildGetSpaceOperatingHoursRequest();

            Response response = client.newCall(request).execute();

            String fromDateTime;
            String toDateTime;
            boolean is24HoursOpen = false;


            if (response.isSuccessful()) {

                String responseData = response.body().string();
                JSONObject json = new JSONObject(responseData);

                result = json.getInt("status");
                reason = json.getString("reason");

                if (result == Constants.OK) {

                    fromDateTime = json.getString("openingDateTime");
                    toDateTime = json.getString("closingDateTime");
                    is24HoursOpen = json.getBoolean("is24HoursOpen");

                    Context context = getApplicationContext();

                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_IS_OPEN_24_HOURS,
                            is24HoursOpen, Constants.TYPE_BOOL);

                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_OPENING_DATE_TIME_QUERY,
                            fromDateTime, Constants.TYPE_STRING);

                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_CLOSING_DATE_TIME_QUERY,
                            toDateTime, Constants.TYPE_STRING);

                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_OPENING_TIME_QUERY,
                            MiscUtils.getOpeningTimeFromDateTime(fromDateTime), Constants.TYPE_STRING);

                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_CLOSING_TIME_QUERY,
                            MiscUtils.getOpeningTimeFromDateTime(toDateTime), Constants.TYPE_STRING);

                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_OPERATING_HOURS_SET_QUERY,
                            true, Constants.TYPE_BOOL);
                } else {

                    PendingActions obj = new PendingActions(
                            Constants.TRIGGER_ACTION_GET_SPACE_OPERATING_HOURS);
                    obj.save();

                    sendResult(Constants.TRIGGER_ACTION_GET_SPACE_OPERATING_HOURS, Constants.NOK,
                            reason);
                    response.body().close();

                    return;

                }


            } else {

                PendingActions obj = new PendingActions(
                        Constants.TRIGGER_ACTION_GET_SPACE_OPERATING_HOURS);
                obj.save();

                sendResult(Constants.TRIGGER_ACTION_GET_SPACE_OPERATING_HOURS, Constants.NOK,
                        response.message());
                response.body().close();

                return;

            }

            response.body().close();

            Intent localIntent = prepareResult(Constants.TRIGGER_ACTION_GET_SPACE_OPERATING_HOURS,
                    result);

            sendResult(Constants.TRIGGER_ACTION_GET_SPACE_OPERATING_HOURS, result,
                    reason);

        } catch (JSONException ioe) {

            MiscUtils.errorMsgs.add("handleActionGetSpaceOperatingHours, JSON Exception:: " + ioe);
            sendResult(Constants.TRIGGER_ACTION_GET_SPACE_OPERATING_HOURS, Constants.NOK,
                    Constants.ERROR_INVALID_RESPONSE);
        } catch (java.io.IOException ioe) {
            MiscUtils.errorMsgs.add("handleActionGetSpaceOperatingHours, IO Exception:: " + ioe);
            sendResult(Constants.TRIGGER_ACTION_GET_SPACE_OPERATING_HOURS, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

            PendingActions obj = new PendingActions(
                    Constants.TRIGGER_ACTION_GET_SPACE_OPERATING_HOURS);
            obj.save();


        }

    }

    /**
     * Handle handleActionGetSyncIdFromServer to trigger getting the sync
     * id from server
     */

    void handleActionGetSyncIdFromServer() {

        if (checkForConnectivity(getApplicationContext()) == false) {
            //set the state of the entry object to NOT in sync

            sendResult(Constants.TRIGGER_ACTION_GET_SYNC_ID_FROM_SERVER, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

            //don't add to PendingAction as GetSyncID is the first thing we do after
            //connecting back

            return;
        }


        int result = Constants.OK;
        String reason = "";

        try {

            OkHttpClient client = new OkHttpClient();
            Request request = buildGetSyncIdFromServer();

            Response response = client.newCall(request).execute();
            int receivedSyncId = 0;


            if (response.isSuccessful()) {
                String responseData = response.body().string();
                JSONObject json = new JSONObject(responseData);

                receivedSyncId = json.optInt("syncId", Constants.INVALID_SYNC_ID);

                Intent localIntent = prepareResult(
                        Constants.TRIGGER_ACTION_GET_SYNC_ID_FROM_SERVER,
                        result);

                localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, receivedSyncId);

                response.body().close();

                sendResult(localIntent);


            } else {

                PendingActions obj = new PendingActions(
                        Constants.TRIGGER_ACTION_GET_SYNC_ID_FROM_SERVER);
                obj.save();

                sendResult(Constants.TRIGGER_ACTION_GET_SYNC_ID_FROM_SERVER, Constants.NOK,
                        response.message());
                response.body().close();

                return;

            }


        } catch (JSONException ioe) {

            MiscUtils.errorMsgs.add("handleActionGetSyncIdFromServer, JSON Exception:: " + ioe);
            sendResult(Constants.TRIGGER_ACTION_GET_SYNC_ID_FROM_SERVER, Constants.NOK,
                    Constants.ERROR_INVALID_RESPONSE);
        } catch (java.io.IOException ioe) {
            MiscUtils.errorMsgs.add("handleActionGetSyncIdFromServer, IO Exception:: " + ioe);
            sendResult(Constants.TRIGGER_ACTION_GET_SYNC_ID_FROM_SERVER, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

            PendingActions obj = new PendingActions(
                    Constants.TRIGGER_ACTION_GET_SYNC_ID_FROM_SERVER);
            obj.save();


        }


    }

    /**
     * Handle handleActionPushUpdatesToServer to trigger pushing the updates
     * to the server
     */

    void handleActionPushUpdatesToServer() {
        Context context = getApplicationContext();

        if (checkForConnectivity(context) == false) {
            //set the state of the entry object to NOT in sync
            startPushTransactionsTimer();
            sendResult(Constants.TRIGGER_ACTION_PUSH_UPDATES_TO_SERVER, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);


            return;
        }


        int result = Constants.OK;
        String reason = "";

        try {

            OkHttpClient client = new OkHttpClient();
            List<TransactionTbl> transactionTblList = getPendingTransactions(
                    Constants.OFFLINE_TRANSACTIONS_PUSH_LIMIT);
            List<SimplyParkTransactionTbl> spTransactionTbList = getPendingSpTransactions();
            List<SimplyParkBookingTbl> opAppBookingsList = getPendingOpAppBookings();
            List<LostPassTbl> lostPassList = getPendingLostPass();
            Request request = buildPushUpdatesToServer(transactionTblList, spTransactionTbList,
                    opAppBookingsList, lostPassList);

            if (transactionTblList.size() == Constants.OFFLINE_TRANSACTIONS_PUSH_LIMIT) {
                //Toast.makeText(getApplicationContext(), "More than 64 transactions",Toast.LENGTH_SHORT).show();
                //maybe there were more
                startActionPushUpdatesToServer(context);

            }


            if (request != null) {

                Response response = client.newCall(request).execute();
                int receivedSyncId = 0;
                //Toast.makeText(getApplicationContext(), response.toString(),Toast.LENGTH_SHORT).show();
                if (response.isSuccessful()) {
                    String responseData = response.body().string();
                    JSONObject json = new JSONObject(responseData);
                    //Toast.makeText(getApplicationContext(), responseData,Toast.LENGTH_SHORT).show();
                    // Toast.makeText(getApplicationContext(),"Valid response received",Toast.LENGTH_SHORT).show();

                    int updateSize = transactionTblList.size() + spTransactionTbList.size() +
                            opAppBookingsList.size();
                    result = json.optInt("status", Constants.NOK);

                        /*Issue 96 Fix. Compare the received sync Id with the expected sync ID.
                          If the received syncID != expeceted Sync ID, a pull is required.
                          Consider an Entry phone and Exit phone. Both the server, Entry & Exit phones
                          currently have sync id 10. Both phones are offline.
                          While offline, Entry phone has done 3 entries and Exit has done 3 exits.
                          Both come online at roughly the same time and both request a pull.
                          After PULL entry & exit both push their transactions. Both are expecting
                          the sync ID to be 13. Server processes entry phone first, adds the entries
                          and retuns the sync ID as 13. Then the server processes exit phone, add
                          the entries and returns the sync ID as 16. At Exit phone, the result is OK,
                          so the exit phone updates its sync ID as 13, correctly figuring that it
                          is behind 3 entries from server. However, crucially, it has incorrectly
                          thought that the entries it is missing is 14,15 and 16. This will cause an
                          issue where the sync ID is matched at both entry and exit phones while exit
                          phone will miss some transactions
                         */

                    int currentSyncId = MiscUtils.getSyncId(context);
                    if (result == Constants.OK) {
                        receivedSyncId = json.optInt("syncId", Constants.INVALID_SYNC_ID);

                        if ((currentSyncId + updateSize) == receivedSyncId) {
                            //setup next expected sync ID
                            MiscUtils.updateSyncId(getApplicationContext(), updateSize);
                        }

                        updatePendingTransactionState(transactionTblList);
                        updatePendingSpTransactionState(spTransactionTbList);
                        updatePendingOpAppBookingsState(opAppBookingsList);
                        updatePendingModifiedPasses(lostPassList);

                    } else if (result == Constants.PARTIAL_OK) {

                        // Toast.makeText(getApplicationContext(),"Partial OK response received",Toast.LENGTH_SHORT).show();


                        receivedSyncId = json.optInt("syncId", Constants.INVALID_SYNC_ID);
                        String resultCode = json.optString("offlineResultCode", "");
                        String onlineResultCode = json.optString("onlineResultCode", "");
                        String opAppBookingResultCode = json.optString("opAppBookingResultCode", "");
                        String opModifiedPassResultCode = json.optString("opAppBookingModificationResultCode", ""); //Changes to be done

                        int numPushedUpdates = updatePendingTransactionState(transactionTblList,
                                resultCode);

                        numPushedUpdates += updatePendingSpTransactionState(spTransactionTbList,
                                onlineResultCode);

                        numPushedUpdates += updatePendingOpAppBookingsState(opAppBookingsList,
                                opAppBookingResultCode);

                        numPushedUpdates += updatePendingModifiedPasses(lostPassList,
                                opModifiedPassResultCode);

                        if ((currentSyncId + numPushedUpdates) == receivedSyncId) {
                            MiscUtils.updateSyncId(getApplicationContext(), numPushedUpdates);
                        }


                    } else {
                        //Toast.makeText(getApplicationContext(),"Response is Failed",Toast.LENGTH_SHORT).show();
                        response.body().close();
                        reason = json.optString("reason", "");
                        sendResult(Constants.TRIGGER_ACTION_PUSH_UPDATES_TO_SERVER, result,
                                reason);
                        startPushTransactionsTimer();
                        return;

                    }
                } else {
                    // Toast.makeText(getApplicationContext(),"Response is NOT successful",Toast.LENGTH_SHORT).show();
                    response.body().close();
                    result = Constants.NOK;
                    sendResult(Constants.TRIGGER_ACTION_PUSH_UPDATES_TO_SERVER, result,
                            response.message());

                    startPushTransactionsTimer();
                    return;
                }

                response.body().close();


                if (result == Constants.OK) {
                    sendResult(Constants.TRIGGER_ACTION_PUSH_UPDATES_TO_SERVER,
                            result, receivedSyncId);
                } else if (result == Constants.PARTIAL_OK) {
                    Intent localIntent = prepareResult(Constants.TRIGGER_ACTION_PUSH_UPDATES_TO_SERVER,
                            result);
                    localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, receivedSyncId);
                    localIntent.putExtra(Constants.BUNDLE_PARAM_IS_PARTIAL_SUCCESS, true);

                    sendResult(localIntent);
                }

            } else {
                //we reach here if PUSH to server was required but there were no records to push
                sendResult(Constants.TRIGGER_ACTION_PUSH_UPDATES_TO_SERVER, Constants.OK);
            }
        } catch (JSONException ioe) {

            MiscUtils.errorMsgs.add("handleActionPushUpdatesToServer, JSON Exception:: " + ioe);
            startPushTransactionsTimer();
            sendResult(Constants.TRIGGER_ACTION_PUSH_UPDATES_TO_SERVER, Constants.NOK,
                    Constants.ERROR_INVALID_RESPONSE);
        } catch (java.io.IOException ioe) {
            MiscUtils.errorMsgs.add("handleActionPushUpdatesToServer, IO Exception:: " + ioe);
            startPushTransactionsTimer();
            sendResult(Constants.TRIGGER_ACTION_PUSH_UPDATES_TO_SERVER, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);


        }

    }

    /**
     * Handle action handleActionGetUpdatesFromServer to trigger the sync procedure
     * from the server
     */

    void handleActionGetUpdatesFromServer(int syncId) {


        int result = Constants.OK;
        String reason = "";

        if (checkForConnectivity(getApplicationContext()) == false) {

            sendResult(Constants.TRIGGER_ACTION_PULL_UPDATES_FROM_SERVER, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

            return;
        }


        try {
            Context context = getApplicationContext();

            OkHttpClient client = new OkHttpClient();
            Request request = buildGetUpdateFromServerRequest(syncId);

            Response response = client.newCall(request).execute();


            if (response.isSuccessful()) {

                String responseData = response.body().string();
                JSONObject json = new JSONObject(responseData);

                result = json.getInt("status");
                reason = json.optString("reason", "");

                if (result == Constants.OK) {
                    int numUpdates = json.optInt("numUpdates", 0);

                    JSONArray updates = json.getJSONArray("update");


                    long openingTime = MiscUtils.getLongOpeningTimeForToday(
                            getApplicationContext());

                    long closingTime = MiscUtils.getLongClosingTimeForToday(
                            getApplicationContext());

                    int numParkedBikes = MiscUtils.getBikesParkedCount(getApplicationContext());
                    int numParkedCars = MiscUtils.getCarsParkedCount(getApplicationContext());

                    boolean updatePassesCount = false;
                    for (int i = 0; i < numUpdates; i++) {
                        JSONObject row = updates.getJSONObject(i);

                        String updateType = row.keys().next();

                        if (updateType.equals(Constants.SYNC_OFFLINE_ENTRY_EXIT_UPDATE)) {
                            JSONObject serverTransaction = (JSONObject) row.get(
                                    Constants.SYNC_OFFLINE_ENTRY_EXIT_UPDATE);
                            handleOfflineEntryExitUpdate(serverTransaction, openingTime,
                                    closingTime);

                        } else if (updateType.equals(Constants.SYNC_ONLINE_BOOKING_UPDATE)) {
                            JSONObject serverTransaction = (JSONObject) row.get(
                                    Constants.SYNC_ONLINE_BOOKING_UPDATE);
                            handleSimplyParkBookingUpdate(serverTransaction);

                        } else if (updateType.equals(Constants.SYNC_ONLINE_OP_APP_BOOKING_UPDATE)) {
                            JSONObject opAppBooking = (JSONObject) row.get(
                                    Constants.SYNC_ONLINE_OP_APP_BOOKING_UPDATE);


                            updatePassesCount |= handleOpAppBookingUpdate(opAppBooking);

                        } else if (updateType.equals(Constants.SYNC_ONLINE_ENTRY_EXIT_UPDATE)) {
                            JSONObject serverTransaction = (JSONObject) row.get(
                                    Constants.SYNC_ONLINE_ENTRY_EXIT_UPDATE);
                            handleOnlineEntryExitUpdate(serverTransaction, openingTime,
                                    closingTime);

                        }else if (updateType.equals(Constants.SYNC_ONLINE_OP_APP_LOST_PASS_UPDATE)) {
                            /***@Chandresh: here the value for modify is supposed to be added.**/
                            JSONObject lostPassUpdate = (JSONObject) row.get(
                                    Constants.SYNC_ONLINE_OP_APP_LOST_PASS_UPDATE);

                            handleOpAppLostPassUpdate(lostPassUpdate);

                        }


                    }
                    //to update UI of the big circle if any monthly passes update is received
                    if (updatePassesCount) {
                        handleActionEndOfDay();
                    }

                    //store and/or update the operators list
                    JSONArray operators = json.optJSONArray("operators");

                    if (operators != null) {
                        int length = operators.length();

                        for (int idx = 0; idx < length; idx++) {
                            Cache.setInCache(context, Constants.CACHE_PREFIX_OPERATORID_DESC_QUERY + idx,
                                    operators.getString(idx), Constants.TYPE_STRING);
                        }
                    }


                    int receivedSyncId = json.optInt("syncId", Constants.INVALID_SYNC_ID);


                    MiscUtils.setSyncId(getApplicationContext(), receivedSyncId);


                } else if (result == Constants.PARTIAL_OK) {
                    //no updates to sync. Set the sync id as the
                    //received syncId
                    int receivedSyncId = json.optInt("syncId", Constants.INVALID_SYNC_ID);
                    MiscUtils.setSyncId(getApplicationContext(), receivedSyncId);

                    response.body().close();

                    sendResult(Constants.TRIGGER_ACTION_PULL_UPDATES_FROM_SERVER,
                            Constants.OK);
                    return;


                } else {
                    reason = json.optString("reason", "");
                    MiscUtils.errorMsgs.add("handleActionGetUpdatesFromServer,Failed:: " +
                            reason);
                    response.body().close();
                    sendResult(Constants.TRIGGER_ACTION_PULL_UPDATES_FROM_SERVER, result,
                            reason);
                    return;

                }

                response.body().close();

                sendResult(Constants.TRIGGER_ACTION_PULL_UPDATES_FROM_SERVER, result);


            } else {

                MiscUtils.errorMsgs.add("handleActionGetUpdatesFromServer,Failed:: " +
                        response.message());
                response.body().close();
                sendResult(Constants.TRIGGER_ACTION_PULL_UPDATES_FROM_SERVER, result,
                        response.message());


            }
        } catch (JSONException ioe) {

            MiscUtils.errorMsgs.add("handleActionGetUpdatesFromServer,JSON Exception:: " + ioe);
            sendResult(Constants.TRIGGER_ACTION_PULL_UPDATES_FROM_SERVER, Constants.NOK,
                    Constants.ERROR_INVALID_RESPONSE);
        } catch (java.io.IOException ioe) {
            MiscUtils.errorMsgs.add("handleActionGetUpdatesFromServer, IO Exception:: " + ioe);
            sendResult(Constants.TRIGGER_ACTION_PULL_UPDATES_FROM_SERVER, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);
        }


    }


    void handleActionSyncWithServer() {
        //pull from server updates
        int syncId = MiscUtils.getSyncId(getApplicationContext());

        Cache.setInCache(getApplicationContext(),
                Constants.CACHE_PREFIX_SYNC_STATE_QUERY,
                Constants.SYNC_STATE_BOTH_PUSH_AND_PULL_IN_PROGRESS, Constants.TYPE_INT);

        handleActionGetUpdatesFromServer(syncId);


    }

    /**
     * Handle action handleActionCancelTransaction to update the server about a failed
     * transaction
     */

    void handleActionCancelTransaction(String parkingId, long entryTime, String vehicleReg,
                                       int vehicleType) {
        int result = Constants.OK;
        String reason = "";

        if (checkForConnectivity(getApplicationContext()) == false) {

            setTransactionStateOutOfSync(parkingId);
            sendResult(Constants.TRIGGER_ACTION_CANCEL_TRANSACTION, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

            return;
        }


        try {

            OkHttpClient client = new OkHttpClient();
            Request request = buildCancelTransactionRequest(parkingId, entryTime, vehicleReg, vehicleType);


            Response response = client.newCall(request).execute();


            if (response.isSuccessful()) {
                String responseData = response.body().string();
                JSONObject json = new JSONObject(responseData);
                int receivedSyncId = 0;


                result = json.optInt("status", Constants.NOK);
                reason = json.optString("reason");

                if (result == Constants.OK) {


                    receivedSyncId = json.optInt("syncId", Constants.INVALID_SYNC_ID);
                    /* Issue 96 fix. Sync iD should only be updated if the received syncID == expected */
                    Context context = getApplicationContext();
                    int syncIdAtClient = MiscUtils.getSyncId(context);

                    if ((syncIdAtClient + 1) == receivedSyncId) {
                        //Moving the syncID increment after receiveing a successful response
                        MiscUtils.updateSyncId(getApplicationContext(), 1);
                    }

                    //issue109: setting the transaction state as IN sync
                    setTransactionStateInSync(parkingId);

                    Intent localIntent = prepareResult(Constants.TRIGGER_ACTION_CANCEL_TRANSACTION,
                            Constants.OK);
                    localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, receivedSyncId);
                    sendResult(localIntent);
                } else {
                    MiscUtils.errorMsgs.add("handleActionCancelTransaction,Failed with reason:: " +
                            reason);
                    setTransactionStateOutOfSync(parkingId);
                    sendResult(Constants.TRIGGER_ACTION_CANCEL_TRANSACTION, Constants.NOK);
                }
                response.body().close();

            } else {
                MiscUtils.errorMsgs.add("handleActionCancelTransaction,Failed with reason:: " +
                        response.message());

                setTransactionStateOutOfSync(parkingId);
                sendResult(Constants.TRIGGER_ACTION_CANCEL_TRANSACTION, Constants.NOK);
                response.body().close();
            }

        } catch (JSONException ioe) {
            setTransactionStateOutOfSync(parkingId);
            MiscUtils.errorMsgs.add("handleActionCancelTransaction,JSON Exception:: " + ioe);
            sendResult(Constants.TRIGGER_ACTION_CANCEL_TRANSACTION, Constants.NOK,
                    Constants.ERROR_INVALID_RESPONSE);
        } catch (java.io.IOException ioe) {
            setTransactionStateOutOfSync(parkingId);
            MiscUtils.errorMsgs.add("handleActionCancelTransaction, IO Exception:: " + ioe);
            sendResult(Constants.TRIGGER_ACTION_CANCEL_TRANSACTION, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);
        }
    }

    /**
     * Handle action handleActionUpdateTransaction to update the server about a failed
     * transaction
     */

    void handleActionUpdateTransaction(String parkingId, int updatedParkingStatus, int updatedParkingFees,
                                       long entryTime, String vehicleReg, int vehicleType) {
        int result = Constants.OK;
        String reason = "";

        if (checkForConnectivity(getApplicationContext()) == false) {

            setTransactionStateOutOfSync(parkingId);
            sendResult(Constants.TRIGGER_ACTION_UPDATE_TRANSACTION, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

            return;
        }


        try {

            OkHttpClient client = new OkHttpClient();
            Request request = buildUpdateTransactionRequest(parkingId, updatedParkingStatus,
                    updatedParkingFees, entryTime, vehicleReg, vehicleType);


            Response response = client.newCall(request).execute();


            if (response.isSuccessful()) {
                String responseData = response.body().string();
                JSONObject json = new JSONObject(responseData);
                int receivedSyncId = 0;


                result = json.optInt("status", Constants.NOK);
                reason = json.optString("reason");

                if (result == Constants.OK) {


                    receivedSyncId = json.optInt("syncId", Constants.INVALID_SYNC_ID);

                    /* Issue 96 fix. Sync iD should only be updated if the received syncID == expected */
                    Context context = getApplicationContext();
                    int syncIdAtClient = MiscUtils.getSyncId(context);

                    if ((syncIdAtClient + 1) == receivedSyncId) {
                        //Moving the syncID increment after receiveing a successful response
                        MiscUtils.updateSyncId(getApplicationContext(), 1);
                    }

                    //issue109: setting the transaction state as IN sync
                    setTransactionStateInSync(parkingId);

                    Intent localIntent = prepareResult(Constants.TRIGGER_ACTION_UPDATE_TRANSACTION,
                            Constants.OK);
                    localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, receivedSyncId);
                    sendResult(localIntent);
                } else {
                    MiscUtils.errorMsgs.add("handleActionUpdateTransaction,Failed " +
                            reason);
                    setTransactionStateOutOfSync(parkingId);
                    sendResult(Constants.TRIGGER_ACTION_UPDATE_TRANSACTION, Constants.NOK);
                }

            } else {
                MiscUtils.errorMsgs.add("handleActionUpdateTransaction,Failed " +
                        response.message());
                setTransactionStateOutOfSync(parkingId);
                sendResult(Constants.TRIGGER_ACTION_UPDATE_TRANSACTION, Constants.NOK);
            }

            response.body().close();

        } catch (JSONException ioe) {
            setTransactionStateOutOfSync(parkingId);
            MiscUtils.errorMsgs.add("handleActionUpdateTransaction,JSON Exception:: " + ioe);
            sendResult(Constants.TRIGGER_ACTION_UPDATE_TRANSACTION, Constants.NOK,
                    Constants.ERROR_INVALID_RESPONSE);
        } catch (java.io.IOException ioe) {
            setTransactionStateOutOfSync(parkingId);
            MiscUtils.errorMsgs.add("handleActionUpdateTransaction, IO Exception:: " + ioe);
            sendResult(Constants.TRIGGER_ACTION_UPDATE_TRANSACTION, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);
        }
    }

    /**
     * Handle action handleActionCancelSpTransaction to update the server about a failed
     * transaction
     */

    void handleActionCancelSpTransaction(String parkingId, long entryTime, String vehicleReg,
                                         int vehicleType, String bookingId, int bookingType) {
        int result = Constants.OK;
        String reason = "";

        if (checkForConnectivity(getApplicationContext()) == false) {

            setSpTransactionStateOutOfSync(parkingId);
            sendResult(Constants.TRIGGER_ACTION_SP_CANCEL_TRANSACTION, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

            return;
        }


        try {

            OkHttpClient client = new OkHttpClient();
            Request request = buildCancelSpTransactionRequest(parkingId, entryTime,
                    vehicleReg, vehicleType,
                    bookingId, bookingType);


            Response response = client.newCall(request).execute();


            if (response.isSuccessful()) {
                String responseData = response.body().string();
                JSONObject json = new JSONObject(responseData);
                int receivedSyncId = 0;


                result = json.optInt("status", Constants.NOK);
                reason = json.optString("reason");

                if (result == Constants.OK) {

                    receivedSyncId = json.optInt("syncId", Constants.INVALID_SYNC_ID);

                    /* Issue 96 fix. Sync iD should only be updated if the received syncID == expected */
                    Context context = getApplicationContext();
                    int syncIdAtClient = MiscUtils.getSyncId(context);

                    if ((syncIdAtClient + 1) == receivedSyncId) {
                        //Moving the syncID increment after receiveing a successful response
                        MiscUtils.updateSyncId(getApplicationContext(), 1);
                    }

                    //issue109: setting the transaction state as IN sync
                    setSpTransactionStateInSync(parkingId);

                    Intent localIntent = prepareResult(Constants.TRIGGER_ACTION_SP_CANCEL_TRANSACTION,
                            Constants.OK);
                    localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, receivedSyncId);
                    sendResult(localIntent);
                } else {
                    MiscUtils.errorMsgs.add("handleActionCancelSpTransaction,Failed with reason:: " +
                            reason);
                    setSpTransactionStateOutOfSync(parkingId);
                    sendResult(Constants.TRIGGER_ACTION_SP_CANCEL_TRANSACTION, Constants.NOK);
                }
                response.body().close();

            } else {
                MiscUtils.errorMsgs.add("handleActionCancelSpTransaction,Failed with reason:: " +
                        response.message());

                setSpTransactionStateOutOfSync(parkingId);
                sendResult(Constants.TRIGGER_ACTION_SP_CANCEL_TRANSACTION, Constants.NOK);
                response.body().close();
            }

        } catch (JSONException ioe) {
            setSpTransactionStateOutOfSync(parkingId);
            MiscUtils.errorMsgs.add("handleActionCancelSpTransaction,JSON Exception:: " + ioe);
            sendResult(Constants.TRIGGER_ACTION_SP_CANCEL_TRANSACTION, Constants.NOK,
                    Constants.ERROR_INVALID_RESPONSE);
        } catch (java.io.IOException ioe) {
            setSpTransactionStateOutOfSync(parkingId);
            MiscUtils.errorMsgs.add("handleActionCancelSpTransaction, IO Exception:: " + ioe);
            sendResult(Constants.TRIGGER_ACTION_SP_CANCEL_TRANSACTION, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);
        }
    }

    /**
     * Handle action handleActionUpdateTransaction to update the server about a failed
     * transaction
     */

    void handleActionUpdateSpTransaction(String parkingId, int updatedParkingStatus,
                                         int updatedParkingFees,
                                         long entryTime, long exitTime,
                                         String vehicleReg, int vehicleType,
                                         String bookingId, int bookingType) {
        int result = Constants.OK;
        String reason = "";

        if (checkForConnectivity(getApplicationContext()) == false) {

            setSpTransactionStateOutOfSync(parkingId);
            sendResult(Constants.TRIGGER_ACTION_SP_UPDATE_TRANSACTION, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

            return;
        }


        try {

            OkHttpClient client = new OkHttpClient();
            Request request = buildUpdateSpTransactionRequest(parkingId, updatedParkingStatus,
                    updatedParkingFees, entryTime, exitTime, vehicleReg, vehicleType, bookingId, bookingType);


            Response response = client.newCall(request).execute();


            if (response.isSuccessful()) {
                String responseData = response.body().string();
                JSONObject json = new JSONObject(responseData);
                int receivedSyncId = 0;


                result = json.optInt("status", Constants.NOK);
                reason = json.optString("reason");

                if (result == Constants.OK) {

                    receivedSyncId = json.optInt("syncId", Constants.INVALID_SYNC_ID);
                    /* Issue 96 fix. Sync iD should only be updated if the received syncID == expected */
                    Context context = getApplicationContext();
                    int syncIdAtClient = MiscUtils.getSyncId(context);

                    if ((syncIdAtClient + 1) == receivedSyncId) {
                        //Moving the syncID increment after receiveing a successful response
                        MiscUtils.updateSyncId(getApplicationContext(), 1);
                    }

                    //issue109: setting the transaction state as IN sync
                    setSpTransactionStateInSync(parkingId);

                    Intent localIntent = prepareResult(Constants.TRIGGER_ACTION_SP_UPDATE_TRANSACTION,
                            Constants.OK);
                    localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, receivedSyncId);
                    sendResult(localIntent);
                } else {
                    MiscUtils.errorMsgs.add("handleActionUpdateSpTransaction,Failed " +
                            reason);
                    setSpTransactionStateOutOfSync(parkingId);
                    sendResult(Constants.TRIGGER_ACTION_SP_UPDATE_TRANSACTION, Constants.NOK);
                }

            } else {
                MiscUtils.errorMsgs.add("handleActionUpdateSpTransaction,Failed " +
                        response.message());
                setSpTransactionStateOutOfSync(parkingId);
                sendResult(Constants.TRIGGER_ACTION_SP_UPDATE_TRANSACTION, Constants.NOK);
            }

            response.body().close();

        } catch (JSONException ioe) {
            setSpTransactionStateOutOfSync(parkingId);
            MiscUtils.errorMsgs.add("handleActionUpdateSpTransaction,JSON Exception:: " + ioe);
            sendResult(Constants.TRIGGER_ACTION_SP_UPDATE_TRANSACTION, Constants.NOK,
                    Constants.ERROR_INVALID_RESPONSE);
        } catch (java.io.IOException ioe) {
            setSpTransactionStateOutOfSync(parkingId);
            MiscUtils.errorMsgs.add("handleActionUpdateSpTransaction, IO Exception:: " + ioe);
            sendResult(Constants.TRIGGER_ACTION_SP_UPDATE_TRANSACTION, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);
        }
    }


    /**
     * Handle action handleActionIsSpaceBikeOnly to determine if the space is bike only
     */

    void handleActionIsSpaceBikeOnly() {

        int result = Constants.OK;
        String reason = "";

        if (checkForConnectivity(getApplicationContext()) == false) {

            sendResult(Constants.TRIGGER_ACTION_IS_SPACE_BIKE_ONLY, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

            return;
        }


        try {

            OkHttpClient client = new OkHttpClient();

            Request request = buildIsSpaceBikeOnlyRequest();

            if (request != null) {

                Response response = client.newCall(request).execute();
                int status = Constants.OK;

                if (response.isSuccessful()) {
                    String responseData = response.body().string();
                    JSONObject json = new JSONObject(responseData);

                    status = json.optInt("status", Constants.INVALID_SYNC_ID);

                    if (status == Constants.OK) {
                        boolean isBikeOnly = json.optBoolean("isBikeOnly", false);

                        Cache.setInCache(getApplicationContext(),
                                Constants.CACHE_PREFIX_IS_SPACE_BIKE_ONLY_QUERY,
                                isBikeOnly, Constants.TYPE_BOOL);

                        Cache.setInCache(getApplicationContext(),
                                Constants.CACHE_PREFIX_SPACE_VEHICLE_TYPE_QUERY,
                                true, Constants.TYPE_BOOL);

                    } else {
                        sendResult(Constants.TRIGGER_ACTION_IS_SPACE_BIKE_ONLY, Constants.NOK,
                                Constants.ERROR_INVALID_RESPONSE);
                    }
                }
            }
        } catch (JSONException ioe) {

            MiscUtils.errorMsgs.add("handleActionIsSpaceBikeOnly, JSON Exception:: " + ioe);
            sendResult(Constants.TRIGGER_ACTION_IS_SPACE_BIKE_ONLY, Constants.NOK,
                    Constants.ERROR_INVALID_RESPONSE);
        } catch (java.io.IOException ioe) {
            MiscUtils.errorMsgs.add("handleActionIsSpaceBikeOnly, IO Exception:: " + ioe);
            sendResult(Constants.TRIGGER_ACTION_IS_SPACE_BIKE_ONLY, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);


        }


    }


    /**
     * Handle action handleActionIsSpaceBikeOnly to determine if the space is bike only
     */

    void handleActionResetOccupancyCount() {

        int result = Constants.OK;


        Context context = getApplicationContext();
        MiscUtils.resetOccupancyCount(context);

    }

    /**
     * Handle action handleActionIsCancellationUpdationAllowed to
     * determine if Cancellation/Updation of Transactions is allowed
     */


    void handleActionIsCancellationUpdationAllowed() {
        int result = Constants.OK;
        String reason = "";

        if (checkForConnectivity(getApplicationContext()) == false) {

            return;
        }


        try {

            OkHttpClient client = new OkHttpClient();
            Request request = buildIsCancelUpdationEnabled();


            Response response = client.newCall(request).execute();


            if (response.isSuccessful()) {
                String responseData = response.body().string();
                JSONObject json = new JSONObject(responseData);
                int receivedSyncId = 0;


                result = json.optInt("status", Constants.NOK);


                if (result == Constants.OK) {

                    boolean isAllowed = json.optBoolean("isCancelUpdateAllowed", false);
                    Context context = getApplicationContext();

                    Cache.setInCache(context, Constants.CACHE_PREFIX_IS_CANCELLATION_UPDATION_ALLOWED,
                            isAllowed, Constants.TYPE_BOOL);

                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_IS_CANCELLATION_UPDATION_FEATURE_AVAIL_KNOWN,
                            true, Constants.TYPE_BOOL);

                } else {
                    //nothing to do here..we will try again
                }
                response.body().close();

            } else {

                response.body().close();
            }

        } catch (JSONException ioe) {
            MiscUtils.errorMsgs.add("handleActionIsCancellationUpdationAllowed,JSON Exception:: "
                    + ioe);
        } catch (java.io.IOException ioe) {
            MiscUtils.errorMsgs.add("handleActionIsCancellationUpdationAllowed, IO Exception:: "
                    + ioe);
        }

    }

    /**
     * Handle action handleActionCalculateOccupancyFromDb to calculate the occupancy count
     * based on the db
     */

    private void handleActionCalculateOccupancyFromDb() {

        if ((Boolean) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY, Constants.TYPE_BOOL)) {
            String carStatus[] = {String.valueOf(Constants.PARKING_STATUS_EXITED),
                    String.valueOf(Constants.VEHICLE_TYPE_CAR)};

            String bikeStatus[] = {String.valueOf(Constants.PARKING_STATUS_EXITED),
                    String.valueOf(Constants.VEHICLE_TYPE_BIKE)};

            String miniBusStatus[] = {String.valueOf(Constants.PARKING_STATUS_EXITED),
                    String.valueOf(Constants.VEHICLE_TYPE_MINIBUS)};

            String busStatus[] = {String.valueOf(Constants.PARKING_STATUS_EXITED),
                    String.valueOf(Constants.VEHICLE_TYPE_BUS)};

            long prepaidCarsParkedInDb = 0;
            long prepaidBikesParkedInDb = 0;
            long prepaidMiniBusParkedInDb = 0;
            long prepaidBusParkedInDb = 0;

            prepaidCarsParkedInDb = TransactionTbl.count(TransactionTbl.class,
                    "m_status = ? AND m_vehicle_type = ?", carStatus);

            prepaidCarsParkedInDb += SimplyParkTransactionTbl.count(SimplyParkTransactionTbl.class,
                    "m_status = ? AND m_vehicle_type = ?", carStatus);

            prepaidBikesParkedInDb = TransactionTbl.count(TransactionTbl.class,
                    "m_status = ? AND m_vehicle_type = ?", bikeStatus);

            prepaidBikesParkedInDb += SimplyParkTransactionTbl.count(SimplyParkTransactionTbl.class,
                    "m_status = ? AND m_vehicle_type = ?", bikeStatus);

            prepaidMiniBusParkedInDb = TransactionTbl.count(TransactionTbl.class,
                    "m_status = ? AND m_vehicle_type = ?", miniBusStatus);

            prepaidMiniBusParkedInDb += SimplyParkTransactionTbl.count(SimplyParkTransactionTbl.class,
                    "m_status = ? AND m_vehicle_type = ?", miniBusStatus);

            prepaidBusParkedInDb = TransactionTbl.count(TransactionTbl.class,
                    "m_status = ? AND m_vehicle_type = ?", busStatus);

            prepaidBusParkedInDb += SimplyParkTransactionTbl.count(SimplyParkTransactionTbl.class,
                    "m_status = ? AND m_vehicle_type = ?", busStatus);

            MiscUtils.setPrepaidCarsParkedCount(getApplicationContext(), (int) prepaidCarsParkedInDb);
            MiscUtils.setPrepaidBikesParkedCount(getApplicationContext(), (int) prepaidBikesParkedInDb);
            MiscUtils.setPrepaidMiniBusParkedCount(getApplicationContext(), (int) prepaidMiniBusParkedInDb);
            MiscUtils.setPrepaidBusParkedCount(getApplicationContext(), (int) prepaidBusParkedInDb);


        } else {
            String carStatus[] = {String.valueOf(Constants.PARKING_STATUS_ENTERED),
                    String.valueOf(Constants.VEHICLE_TYPE_CAR)};

            String bikeStatus[] = {String.valueOf(Constants.PARKING_STATUS_ENTERED),
                    String.valueOf(Constants.VEHICLE_TYPE_BIKE)};

            String miniBusStatus[] = {String.valueOf(Constants.PARKING_STATUS_ENTERED),
                    String.valueOf(Constants.VEHICLE_TYPE_MINIBUS)};

            String busStatus[] = {String.valueOf(Constants.PARKING_STATUS_ENTERED),
                    String.valueOf(Constants.VEHICLE_TYPE_BUS)};

            long carsParkedInDb = 0;
            long bikesParkedInDb = 0;
            long miniBusParkedInDb = 0;
            long busParkedInDb = 0;

            carsParkedInDb = TransactionTbl.count(TransactionTbl.class,
                    "m_status = ? AND m_vehicle_type = ?", carStatus);

            carsParkedInDb += SimplyParkTransactionTbl.count(SimplyParkTransactionTbl.class,
                    "m_status = ? AND m_vehicle_type = ?", carStatus);

            bikesParkedInDb = TransactionTbl.count(TransactionTbl.class,
                    "m_status = ? AND m_vehicle_type = ?", bikeStatus);

            bikesParkedInDb += SimplyParkTransactionTbl.count(SimplyParkTransactionTbl.class,
                    "m_status = ? AND m_vehicle_type = ?", bikeStatus);

            miniBusParkedInDb = TransactionTbl.count(TransactionTbl.class,
                    "m_status = ? AND m_vehicle_type = ?", miniBusStatus);

            miniBusParkedInDb += SimplyParkTransactionTbl.count(SimplyParkTransactionTbl.class,
                    "m_status = ? AND m_vehicle_type = ?", miniBusStatus);

            busParkedInDb = TransactionTbl.count(TransactionTbl.class,
                    "m_status = ? AND m_vehicle_type = ?", busStatus);

            busParkedInDb += SimplyParkTransactionTbl.count(SimplyParkTransactionTbl.class,
                    "m_status = ? AND m_vehicle_type = ?", busStatus);

            Context context = getApplicationContext();

            MiscUtils.setCarsParkedCount(context, (int) carsParkedInDb);
            MiscUtils.setBikesParkedCount(context, (int) bikesParkedInDb);
            MiscUtils.setMiniBusParkedCount(context, (int) miniBusParkedInDb);
            MiscUtils.setBusParkedCount(context, (int) busParkedInDb);


            MiscUtils.setOccupancyCounters(getApplicationContext(),
                    (int) carsParkedInDb, (int) bikesParkedInDb, (int) miniBusParkedInDb,
                    (int) busParkedInDb);
        }


        Intent localIntent = prepareResult(
                Constants.TRIGGER_ACTION_CALCULATE_OCCUPANCY_COUNT_FROM_DB,
                Constants.OK);
        //localIntent.putExtra(Constants.BUNDLE_PARAM_CURRENT_PARKED_CARS, carsParkedInDb);
        //localIntent.putExtra(Constants.BUNDLE_PARAM_CURRENT_PARKED_BIKES, bikesParkedInDb);


        sendResult(localIntent);


    }

    /**
     * Handle action handleActionStartPushTimerIfRequired to start the push timer if required.
     * This is used in MainActivity onCreate. Useful in situations when the mobile is switched
     * back on
     */

    void handleActionStartPushTimerIfRequired() {

        List<TransactionTbl> transactionTblList = getPendingTransactions();
        List<SimplyParkTransactionTbl> spTransactionTbList = getPendingSpTransactions();
        List<SimplyParkBookingTbl> bookingList = getPendingOpAppBookings();
        List<LostPassTbl> lostPassList = getPendingLostPass();

        if (!transactionTblList.isEmpty() || !spTransactionTbList.isEmpty()
                || !bookingList.isEmpty()|| !lostPassList.isEmpty()) {
            AlarmHandler.startAlarm(getApplicationContext(),
                    Constants.TIMER_ACTION_PUSH, Constants.TIMER_ACTION_PUSH_DURATION_IN_SECONDS,
                    false);
        }

    }


    /*********************************************************************************************
     * REQUEST BUILDER FUNCTIONS
     ********************************************************************************************/

    private Request buildIsSpaceBikeOnlyRequest() {
        String spaceId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT)).toString();

        String url = Constants.IS_SPACE_BIKE_ONLY + spaceId + ".json";

        return (new Request.Builder()
                .url(url)
                .build());


    }

    private Request buildCreateBookingRequest(String bookingId, String customer, String email,
                                              String mobile,
                                              int bookingStatus, long startTime, long endTime,
                                              long passIssueDate,
                                              String encodedPassCode, String serialNumber,
                                              String vehicleReg, int vehicleType,
                                              int numBookedSlots, int parkingFees, int discount,
                                              boolean isRenewal, String parentBookingId, boolean isStaffPass,
                                              int paymentMode, String invoiceId, String paymentId)

            throws JSONException {
        String spaceId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT)).toString();

        int operatorId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_OPERATOR_QUERY,
                Constants.TYPE_INT));

        int gcmId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_GCM_ID_AT_SERVER_QUERY,
                Constants.TYPE_INT));


        String url = Constants.CREATE_BOOKING + spaceId + ".json";

        JSONObject jsonDataObj = new JSONObject();

        jsonDataObj.put("operatorId", operatorId);
        jsonDataObj.put("gcmId", gcmId);
        jsonDataObj.put("bookingId", bookingId);
        jsonDataObj.put("name", customer);
        jsonDataObj.put("email", email);
        jsonDataObj.put("mobile", mobile);
        jsonDataObj.put("bookingStatus", bookingStatus);
        jsonDataObj.put("startDateTime", DateTime.forInstant(startTime, Constants.TIME_ZONE).
                format("YYYY-MM-DD hh:mm:ss"));
        jsonDataObj.put("endDateTime", DateTime.forInstant(endTime, Constants.TIME_ZONE).
                format("YYYY-MM-DD hh:mm:ss"));

        jsonDataObj.put("passIssueDate", DateTime.forInstant(passIssueDate, Constants.TIME_ZONE).
                format("YYYY-MM-DD hh:mm:ss"));

        jsonDataObj.put("encodedPassCode", encodedPassCode);
        jsonDataObj.put("serialNumber", serialNumber);
        jsonDataObj.put("vehicleReg", vehicleReg);
        jsonDataObj.put("vehicleType", vehicleType);
        jsonDataObj.put("numBookedSlots", numBookedSlots);
        jsonDataObj.put("parkingFees", parkingFees);
        jsonDataObj.put("discount", discount);
        jsonDataObj.put("isRenewal", isRenewal);
        if (parentBookingId != "") {
            jsonDataObj.put("parentBookingId", parentBookingId);
        }
        jsonDataObj.put("isPassForStaff", isStaffPass);
        jsonDataObj.put("paymentMode", paymentMode);
        jsonDataObj.put("invoiceId", invoiceId);
        jsonDataObj.put("paymentId", paymentId);
        jsonDataObj.put("operatorId", operatorId);


        RequestBody body = RequestBody.create(Constants.JSON, jsonDataObj.toString());

        return (new Request.Builder()
                .url(url)
                .post(body)
                .build());
    }


    private Request buildUpdateTransactionRequest(String parkingId, int updatedParkingStatus,
                                                  int updatedParkingFees,
                                                  long entryTime,
                                                  String vehicleReg, int vehicleType) throws JSONException {
        String spaceId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT)).toString();

        int operatorId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_OPERATOR_QUERY,
                Constants.TYPE_INT));

        int gcmId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_GCM_ID_AT_SERVER_QUERY,
                Constants.TYPE_INT));


        String url = Constants.UPDATE_TRANSACTION + spaceId + ".json";


        JSONObject jsonDataObj = new JSONObject();

        jsonDataObj.put("operator_id", operatorId);
        jsonDataObj.put("parking_id", parkingId);
        jsonDataObj.put("parking_status", updatedParkingStatus);
        jsonDataObj.put("parking_fees", updatedParkingFees);
        jsonDataObj.put("gcm_id", gcmId);

        jsonDataObj.put("entry_date_time", DateTime.forInstant(entryTime, Constants.TIME_ZONE).
                format("YYYY-MM-DD hh:mm:ss"));
        jsonDataObj.put("entry_date_time_stamp", entryTime);
        jsonDataObj.put("vehicle_reg", vehicleReg);
        jsonDataObj.put("vehicle_type", vehicleType);


        RequestBody body = RequestBody.create(Constants.JSON, jsonDataObj.toString());

        return (new Request.Builder()
                .url(url)
                .post(body)
                .build());


    }

    private Request buildIsCancelUpdationEnabled() throws JSONException {
        String spaceId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT)).toString();

        String url = Constants.IS_CANCEL_UPDATE_ENABLED + spaceId + ".json";

        return (new Request.Builder()
                .url(url)
                .build());


    }

    private Request buildCancelTransactionRequest(String parkingId, long entryTime,
                                                  String vehicleReg, int vehicleType)
            throws JSONException {
        String spaceId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT)).toString();

        int operatorId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_OPERATOR_QUERY,
                Constants.TYPE_INT));

        int gcmId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_GCM_ID_AT_SERVER_QUERY,
                Constants.TYPE_INT));


        String url = Constants.CANCEL_TRANSACTION + spaceId + ".json";


        JSONObject jsonDataObj = new JSONObject();

        jsonDataObj.put("operator_id", operatorId);
        jsonDataObj.put("parking_id", parkingId);
        jsonDataObj.put("gcm_id", gcmId);
        jsonDataObj.put("entry_date_time", DateTime.forInstant(entryTime, Constants.TIME_ZONE).
                format("YYYY-MM-DD hh:mm:ss"));
        jsonDataObj.put("entry_date_time_stamp", entryTime);
        jsonDataObj.put("vehicle_reg", vehicleReg);
        jsonDataObj.put("vehicle_type", vehicleType);


        RequestBody body = RequestBody.create(Constants.JSON, jsonDataObj.toString());

        return (new Request.Builder()
                .url(url)
                .post(body)
                .build());


    }

    private Request buildCancelSpTransactionRequest(String parkingId, long entryTime,
                                                    String vehicleReg, int vehicleType,
                                                    String bookingId, int bookingType)
            throws JSONException {
        String spaceId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT)).toString();

        int operatorId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_OPERATOR_QUERY,
                Constants.TYPE_INT));

        int gcmId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_GCM_ID_AT_SERVER_QUERY,
                Constants.TYPE_INT));


        String url = Constants.CANCEL_SP_TRANSACTION + spaceId + ".json";


        JSONObject jsonDataObj = new JSONObject();

        jsonDataObj.put("operatorId", operatorId);
        jsonDataObj.put("parkingId", parkingId);
        jsonDataObj.put("entry_date_time", DateTime.forInstant(entryTime, Constants.TIME_ZONE).
                format("YYYY-MM-DD hh:mm:ss"));
        jsonDataObj.put("entry_date_time_stamp", entryTime);
        jsonDataObj.put("vehicle_reg", vehicleReg);
        jsonDataObj.put("vehicle_type", vehicleType);
        jsonDataObj.put("booking_id", bookingType);
        jsonDataObj.put("booking_type", bookingType);
        jsonDataObj.put("gcmId", gcmId);


        RequestBody body = RequestBody.create(Constants.JSON, jsonDataObj.toString());

        return (new Request.Builder()
                .url(url)
                .post(body)
                .build());


    }

    private Request buildUpdateSpTransactionRequest(String parkingId, int updatedParkingStatus,
                                                    int updatedParkingFees, long entryTime, long exitTime,
                                                    String vehicleReg, int vehicleType,
                                                    String bookingId, int bookingType) throws JSONException {
        String spaceId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT)).toString();

        int operatorId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_OPERATOR_QUERY,
                Constants.TYPE_INT));

        int gcmId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_GCM_ID_AT_SERVER_QUERY,
                Constants.TYPE_INT));


        String url = Constants.UPDATE_SP_TRANSACTION + spaceId + ".json";


        JSONObject jsonDataObj = new JSONObject();

        jsonDataObj.put("operatorId", operatorId);
        jsonDataObj.put("parkingId", parkingId);
        jsonDataObj.put("parkingStatus", updatedParkingStatus);
        jsonDataObj.put("parkingFees", updatedParkingFees);
        jsonDataObj.put("entry_date_time", DateTime.forInstant(entryTime, Constants.TIME_ZONE).
                format("YYYY-MM-DD hh:mm:ss"));
        jsonDataObj.put("entry_date_time_stamp", entryTime);
        jsonDataObj.put("exit_date_time", DateTime.forInstant(exitTime, Constants.TIME_ZONE).
                format("YYYY-MM-DD hh:mm:ss"));
        jsonDataObj.put("exit_date_time_stamp", exitTime);
        jsonDataObj.put("vehicle_reg", vehicleReg);
        jsonDataObj.put("vehicle_type", vehicleType);
        jsonDataObj.put("booking_id", bookingType);
        jsonDataObj.put("booking_type", bookingType);

        jsonDataObj.put("gcmId", gcmId);


        RequestBody body = RequestBody.create(Constants.JSON, jsonDataObj.toString());

        return (new Request.Builder()
                .url(url)
                .post(body)
                .build());


    }


    private Request buildGetSpaceOperatingHoursRequest() throws JSONException {

        String spaceId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT)).toString();

        String url = Constants.SPACE_OPERATING_HOURS_URL + spaceId + ".json";

        return (new Request.Builder()
                .url(url)
                .build());

    }

    private Request buildGetCurrentOccupancyRequest() throws JSONException {

        String spaceId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT)).toString();

        String url = Constants.CURRENT_OCCUPANCY_URL + spaceId + ".json";

        return (new Request.Builder()
                .url(url)
                .build());

    }

    private Request buildRegisterRequest(String token) throws JSONException {
        String spaceId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT)).toString();

        int operatorId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_OPERATOR_QUERY,
                Constants.TYPE_INT));


        int syncId = MiscUtils.getSyncId(getApplicationContext());

        String desc = (String) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_OPERATOR_DESC_QUERY,
                Constants.TYPE_STRING);

        String url = Constants.REGISTER_URL + spaceId + ".json";


        JSONObject jsonDataObj = new JSONObject();
        jsonDataObj.put("token", token);
        jsonDataObj.put("operator_id", operatorId);
        jsonDataObj.put("operator_desc", desc);
        jsonDataObj.put("sync_id", syncId);


        RequestBody body = RequestBody.create(Constants.JSON, jsonDataObj.toString());

        return (new Request.Builder()
                .url(url)
                .post(body)
                .build());

    }

    private Request buildLogEntryRequest(String carReg,long entryTime,String parkingId,
                                          boolean isParkAndRide,int vehicleType,String token)
                    throws JSONException {
        String spaceId = ((Integer)Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT)).toString();

        int operatorId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_OPERATOR_QUERY,
                Constants.TYPE_INT));

        int gcmIdAtServer = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_GCM_ID_AT_SERVER_QUERY,
                Constants.TYPE_INT));


        String url = Constants.LOG_ENTRY_URL + spaceId + ".json";

        DateTime dt = (DateTime.forInstant(entryTime, Constants.TIME_ZONE));
        String entryTimeStr = dt.format("YYYY-MM-DD hh:mm:ss");

        JSONObject jsonDataObj = new JSONObject();
        jsonDataObj.put("gcmIdAtServer",gcmIdAtServer);
        jsonDataObj.put("operatorId",operatorId);
        jsonDataObj.put("carReg",carReg);
        jsonDataObj.put("entryTime",entryTimeStr);
        jsonDataObj.put("entryTimeStamp",entryTime);
        jsonDataObj.put("parkingId",parkingId);
        jsonDataObj.put("isParkAndRide",isParkAndRide);
        jsonDataObj.put("vehicleType",vehicleType);
        if(!token.isEmpty()) {
            jsonDataObj.put("reusableToken", token);
        }


        RequestBody body = RequestBody.create(Constants.JSON, jsonDataObj.toString());

        return (new Request.Builder()
                .url(url)
                .post(body)
                .build());

    }

    private Request buildLogSpEntryRequest(String bookingId, int bookingType,
                                           String parkingId, long entryTime,
                                           String vehicleReg, int vehicleType,
                                           boolean isParkAndRide) throws JSONException {

        String spaceId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT)).toString();

        int operatorId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_OPERATOR_QUERY,
                Constants.TYPE_INT));

        int gcmIdAtServer = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_GCM_ID_AT_SERVER_QUERY,
                Constants.TYPE_INT));


        String url = Constants.SP_LOG_ENTRY_URL + spaceId + ".json";

        DateTime dt = (DateTime.forInstant(entryTime, Constants.TIME_ZONE));
        String entryTimeStr = dt.format("YYYY-MM-DD hh:mm:ss");

        JSONObject jsonDataObj = new JSONObject();
        jsonDataObj.put("gcmIdAtServer", gcmIdAtServer);
        jsonDataObj.put("operatorId", operatorId);
        jsonDataObj.put("parkingId", parkingId);
        jsonDataObj.put("bookingId", bookingId);
        jsonDataObj.put("entryTime", entryTimeStr);
        jsonDataObj.put("entryTimeStamp", entryTime);
        jsonDataObj.put("vehicleReg", vehicleReg);
        jsonDataObj.put("vehicleType", vehicleType);
        jsonDataObj.put("isParkAndRide", isParkAndRide);
        jsonDataObj.put("bookingType", bookingType);


        RequestBody body = RequestBody.create(Constants.JSON, jsonDataObj.toString());

        return (new Request.Builder()
                .url(url)
                .post(body)
                .build());

    }


    private Request buildLogExitRequest(String parkingId,long entryTime,long exitTime,double parkingFees,
                                        int vehicleType,String vehicleReg,String token)
                                          throws JSONException {
        String spaceId = ((Integer)Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT)).toString();

        int operatorId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_OPERATOR_QUERY,
                Constants.TYPE_INT));

        int gcmIdAtServer = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_GCM_ID_AT_SERVER_QUERY,
                Constants.TYPE_INT));


        String url = Constants.LOG_EXIT_URL + spaceId + "/" + parkingId + ".json";

        DateTime exDt = (DateTime.forInstant(exitTime, Constants.TIME_ZONE));
        String exitTimeStr = exDt.format("YYYY-MM-DD hh:mm:ss");

        DateTime enDt = (DateTime.forInstant(entryTime, Constants.TIME_ZONE));
        String entryTimeStr = enDt.format("YYYY-MM-DD hh:mm:ss");

        JSONObject jsonDataObj = new JSONObject();
        jsonDataObj.put("gcmIdAtServer",gcmIdAtServer);
        jsonDataObj.put("operatorId",operatorId);
        jsonDataObj.put("entryTime",entryTimeStr);
        jsonDataObj.put("entryTimeStamp",entryTime);
        jsonDataObj.put("exitTime",exitTimeStr);
        jsonDataObj.put("exitTimeStamp",exitTime);
        jsonDataObj.put("parkingFees",parkingFees);
        jsonDataObj.put("vehicleType",vehicleType);
        if(!token.isEmpty()){
            jsonDataObj.put("reusableToken",token);
        }

        //used in prepaid parking
        if (vehicleReg != null) {
            jsonDataObj.put("vehicleReg", vehicleReg);
        }


        RequestBody body = RequestBody.create(Constants.JSON, jsonDataObj.toString());

        return (new Request.Builder()
                .url(url)
                .post(body)
                .build());

    }

    private Request buildLogSpExitRequest(String bookingId, String parkingId, long exitTime,
                                          int parkingFees, int vehicleType, String vehicleReg)
            throws JSONException {
        String spaceId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT)).toString();

        int operatorId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_OPERATOR_QUERY,
                Constants.TYPE_INT));

        int gcmIdAtServer = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_GCM_ID_AT_SERVER_QUERY,
                Constants.TYPE_INT));


        String url = Constants.SP_LOG_EXIT_URL + spaceId + "/" + parkingId + ".json";

        DateTime dt = (DateTime.forInstant(exitTime, Constants.TIME_ZONE));
        String exitTimeStr = dt.format("YYYY-MM-DD hh:mm:ss");

        JSONObject jsonDataObj = new JSONObject();
        jsonDataObj.put("gcmIdAtServer", gcmIdAtServer);
        jsonDataObj.put("operatorId", operatorId);
        jsonDataObj.put("exitTime", exitTimeStr);
        jsonDataObj.put("exitTimeStamp", exitTime);
        jsonDataObj.put("parkingFees", parkingFees);
        jsonDataObj.put("vehicleType", vehicleType);
        jsonDataObj.put("vehicleReg", vehicleReg);
        jsonDataObj.put("bookingId", bookingId);


        RequestBody body = RequestBody.create(Constants.JSON, jsonDataObj.toString());

        return (new Request.Builder()
                .url(url)
                .post(body)
                .build());

    }

    private Request buildRefreshTokenRequest(String oldToken, String newToken)
            throws JSONException {
        String spaceId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT)).toString();

        int operatorId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_OPERATOR_QUERY,
                Constants.TYPE_INT));

        int gcmIdAtServer = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_GCM_ID_AT_SERVER_QUERY,
                Constants.TYPE_INT));

        String url = Constants.REFRESH_TOKEN_URL + spaceId + "/" + gcmIdAtServer + ".json";


        JSONObject jsonDataObj = new JSONObject();
        jsonDataObj.put("operatorId", operatorId);
        jsonDataObj.put("oldToken", oldToken);
        jsonDataObj.put("newToken", newToken);


        RequestBody body = RequestBody.create(Constants.JSON, jsonDataObj.toString());


        return (new Request.Builder()
                .url(url)
                .post(body)
                .build());


    }

    private Request buildGetSyncIdFromServer() throws JSONException {

        String spaceId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT)).toString();

        String url = Constants.GET_SYNC_ID + spaceId + ".json";


        return (new Request.Builder()
                .url(url)
                .build());


    }

    private Request buildGetUpdateFromServerRequest(int syncId) throws JSONException {

        String spaceId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT)).toString();

        String url = Constants.SYNC_WITH_SERVER + spaceId + "/" + syncId + ".json";


        return (new Request.Builder()
                .url(url)
                .build());


    }

    private List<TransactionTbl> getPendingTransactions() {

        int isSyncedWithServer = 0;

        List<TransactionTbl> pendingTransactionsList = TransactionTbl.find(
                TransactionTbl.class, "m_is_synced_with_server = ?",
                Integer.toString(isSyncedWithServer));

        return pendingTransactionsList;

    }

    private List<TransactionTbl> getPendingTransactions(int limit) {

        int isSyncedWithServer = 0;
        String[] whereArgs = {Integer.toString(isSyncedWithServer)};

        String limitStr = Integer.toString(limit);

        List<TransactionTbl> pendingTransactionsList = TransactionTbl.find(
                TransactionTbl.class, "m_is_synced_with_server = ?", whereArgs, null, null, limitStr);


        return pendingTransactionsList;

    }

    private List<SimplyParkTransactionTbl> getPendingSpTransactions() {

        int isSyncedWithServer = 0;

        List<SimplyParkTransactionTbl> pendingTransactionsList = SimplyParkTransactionTbl.find(
                SimplyParkTransactionTbl.class, "m_is_synced_with_server = ?",
                Integer.toString(isSyncedWithServer));

        return pendingTransactionsList;

    }

    private List<SimplyParkBookingTbl> getPendingOpAppBookings() {
        int isSyncedWithServer = 0;

        List<SimplyParkBookingTbl> pendingOpAppBookings = SimplyParkBookingTbl.find(
                SimplyParkBookingTbl.class, "m_is_synced_with_server = ?",
                Integer.toString(isSyncedWithServer));

        return pendingOpAppBookings;
    }
    private List<LostPassTbl> getPendingLostPass(){
        int isSyncedWithServer = 0;

        List<LostPassTbl> pendingLostPasses = LostPassTbl.find(
                LostPassTbl.class, "m_is_synced_with_server = ?",
                Integer.toString(isSyncedWithServer));

        return pendingLostPasses;

    }
    private void updatePendingTransactionState(List<TransactionTbl> transactionTblList) {
        for (int i = 0; i < transactionTblList.size(); i++) {
            TransactionTbl transaction = transactionTblList.get(i);
            transaction.setIsSyncedWithServer(true);
            transaction.save();
        }
    }

    private void updatePendingSpTransactionState(List<SimplyParkTransactionTbl> transactionTblList) {
        for (int i = 0; i < transactionTblList.size(); i++) {
            SimplyParkTransactionTbl transaction = transactionTblList.get(i);
            transaction.setIsSyncedWithServer(true);
            transaction.save();
        }
    }

    private void updatePendingOpAppBookingsState(List<SimplyParkBookingTbl> bookingList) {
        for (int i = 0; i < bookingList.size(); i++) {
            SimplyParkBookingTbl booking = bookingList.get(i);
            booking.setIsSyncedWithServer(true);
            booking.save();
        }
    }

    private void updatePendingModifiedPasses(List<LostPassTbl> lostPassList){
        for (LostPassTbl lostPass: lostPassList) {
            lostPass.setIsSyncedWithServer(true);
            lostPass.save();
        }
    }

    private int updatePendingModifiedPasses(List<LostPassTbl> lostPassList,String resultCode){

        int incrementSyncIdBy = 0;
        boolean startPushTimer = false;
        int i = 0;
        for (LostPassTbl lostPass:lostPassList) {

            char result = resultCode.charAt(i);

            if (result != Constants.SYNC_PUSH_FAILED) {
                lostPass.setIsSyncedWithServer(true);
                lostPass.save();

                //only increment the count if the push was not stale
                if (result == Constants.SYNC_PUSH_SUCCESSFUL) {
                    incrementSyncIdBy++;
                }
            } else {
                //failed. set a boolean to start Push Timer
                startPushTimer = true;
            }
            i++;
        }

        if (startPushTimer) {
            startPushTransactionsTimer();
        }

        return incrementSyncIdBy;

    }

    //returns by how much sync id should be incremented
    private int updatePendingTransactionState(List<TransactionTbl> transactionTblList,
                                              String resultCode) {

        int incrementSyncIdBy = 0;
        boolean startPushTimer = false;

        for (int i = 0; i < transactionTblList.size(); i++) {

            char result = resultCode.charAt(i);

            if (result != Constants.SYNC_PUSH_FAILED) {
                TransactionTbl transaction = transactionTblList.get(i);
                transaction.setIsSyncedWithServer(true);
                transaction.save();

                //only increment the count if the push was not stale
                if (result == Constants.SYNC_PUSH_SUCCESSFUL) {
                    incrementSyncIdBy++;
                }
            } else {
                //failed. set a boolean to start Push Timer
                startPushTimer = true;
            }
        }

        if (startPushTimer) {
            startPushTransactionsTimer();
        }

        return incrementSyncIdBy;
    }

    //returns by how much sync id should be incremented
    private int updatePendingSpTransactionState(List<SimplyParkTransactionTbl> transactionTblList,
                                                String resultCode) {

        int incrementSyncIdBy = 0;
        boolean startPushTimer = false;

        for (int i = 0; i < transactionTblList.size(); i++) {

            char result = resultCode.charAt(i);

            if (result != Constants.SYNC_PUSH_FAILED) {
                SimplyParkTransactionTbl transaction = transactionTblList.get(i);
                transaction.setIsSyncedWithServer(true);
                transaction.save();

                //only increment the count if the push was not stale
                if (result == Constants.SYNC_PUSH_SUCCESSFUL) {
                    incrementSyncIdBy++;
                }
            } else {
                //failed. set a boolean to start Push Timer
                startPushTimer = true;
            }
        }

        if (startPushTimer) {
            startPushTransactionsTimer();
        }

        return incrementSyncIdBy;
    }

    //returns by how much sync id should be incremented
    private int updatePendingOpAppBookingsState(List<SimplyParkBookingTbl> bookingList,
                                                String resultCode) {

        int incrementSyncIdBy = 0;
        boolean startPushTimer = false;

        for (int i = 0; i < bookingList.size(); i++) {

            char result = resultCode.charAt(i);

            if (result != Constants.SYNC_PUSH_FAILED) {
                SimplyParkBookingTbl booking = bookingList.get(i);
                booking.setIsSyncedWithServer(true);
                booking.save();

                //only increment the count if the push was not stale
                if (result == Constants.SYNC_PUSH_SUCCESSFUL) {
                    incrementSyncIdBy++;
                }
            } else {
                //failed. set a boolean to start Push Timer
                startPushTimer = true;
            }
        }

        if (startPushTimer) {
            startPushTransactionsTimer();
        }

        return incrementSyncIdBy;
    }

    private Request buildPushUpdatesToServer(List<TransactionTbl> pendingTransactionsList,
                                             List<SimplyParkTransactionTbl> pendingSpTransactionsList,
                                             List<SimplyParkBookingTbl> pendingOpAppBookingsList,
                                             List<LostPassTbl> pendingLostPassList)
            throws JSONException {
        String spaceId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT)).toString();

        String url = Constants.SYNC_WITH_SERVER + spaceId + ".json";


        if (pendingTransactionsList.isEmpty() && pendingSpTransactionsList.isEmpty() &&
                pendingOpAppBookingsList.isEmpty() && pendingLostPassList.isEmpty()) {
            return null;
        } else {

            JSONObject jsonDataObj = new JSONObject();
            int pendingTransactionSize = pendingTransactionsList.size();
            int pendingSpTransactionSize = pendingSpTransactionsList.size();
            int pendingOpAppBookingsSize = pendingOpAppBookingsList.size();
            int pendingLostPassReissueSize = pendingLostPassList.size();
            int updateSize = pendingTransactionSize + pendingSpTransactionSize +
                    pendingOpAppBookingsSize+pendingLostPassReissueSize;
            jsonDataObj.put("numUpdates", updateSize);
            int operatorId = ((Integer) Cache.getFromCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_OPERATOR_QUERY,
                    Constants.TYPE_INT));

            int gcmIdAtServer = ((Integer) Cache.getFromCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_GCM_ID_AT_SERVER_QUERY,
                    Constants.TYPE_INT));
            jsonDataObj.put("operatorId", operatorId);
            jsonDataObj.put("gcmId", gcmIdAtServer);

            JSONArray transactionArray = new JSONArray();

            for (int i = 0; i < pendingTransactionSize; i++) {
                TransactionTbl transaction = pendingTransactionsList.get(i);
                JSONObject update = new JSONObject();
                update.put(Constants.SYNC_PUSH_MSG_TYPE_KEY,
                        Constants.SYNC_PUSH_MSG_TYPE_OFFLINE_UPDATE);
                update.put(Constants.SYNC_PARK_AND_RIDE, transaction.getIsParkAndRide());
                update.put(Constants.SYNC_PARKING_ID, transaction.getParkingId());
                update.put(Constants.SYNC_REG_NUMBER, transaction.getCarRegNumber());
                update.put(Constants.SYNC_VEHICLE_TYPE, transaction.getVehicleType());
                update.put(Constants.SYNC_PARKING_FEES, transaction.getParkingFees());
                update.put(Constants.SYNC_STATUS, transaction.getParkingStatus());
                update.put(Constants.SYNC_IS_UPDATED, transaction.getIsUpdated());

                DateTime dt = (DateTime.forInstant(transaction.getEntryTime(), Constants.TIME_ZONE));
                String entryTimeStr = dt.format("YYYY-MM-DD hh:mm:ss");
                update.put(Constants.SYNC_ENTRY_TIME, entryTimeStr);

                DateTime eDt = (DateTime.forInstant(transaction.getExitTime(), Constants.TIME_ZONE));
                String exitTimeStr = eDt.format("YYYY-MM-DD hh:mm:ss");
                update.put(Constants.SYNC_EXIT_TIME, exitTimeStr);

                update.put(Constants.SYNC_PARKING_FEES, transaction.getParkingFees());
                update.put(Constants.SYNC_REUSABLE_TOKEN, transaction.getToken());

                transactionArray.put(update);

            }

            for (int i = 0; i < pendingSpTransactionSize; i++) {
                SimplyParkTransactionTbl transaction = pendingSpTransactionsList.get(i);
                List<SimplyParkBookingTbl> bookingList = SimplyParkBookingTbl.find(
                        SimplyParkBookingTbl.class, "m_booking_id = ?",
                        transaction.getBookingId());


                JSONObject update = new JSONObject();
                update.put(Constants.SYNC_PUSH_MSG_TYPE_KEY,
                        Constants.SYNC_PUSH_MSG_TYPE_ONLINE_UPDATE);
                update.put(Constants.SYNC_ONLINE_ENTRY_EXIT_IS_PARK_AND_RIDE,
                        transaction.getIsParkAndRide());
                update.put(Constants.SYNC_ONLINE_ENTRY_EXIT_PARKING_ID,
                        transaction.getParkingId());
                update.put(Constants.SYNC_ONLINE_ENTRY_EXIT_VEHICLE_REG,
                        transaction.getVehicleReg());
                update.put(Constants.SYNC_ONLINE_ENTRY_EXIT_VEHICLE_TYPE,
                        transaction.getVehicleType());
                update.put(Constants.SYNC_ONLINE_ENTRY_EXIT_PARKING_FEES,
                        transaction.getParkingFees());
                update.put(Constants.SYNC_ONLINE_ENTRY_EXIT_STATUS,
                        transaction.getStatus());
                update.put(Constants.SYNC_ONLINE_ENTRY_EXIT_IS_UPDATED,
                        transaction.getIsUpdated());
                update.put(Constants.SYNC_ONLINE_ENTRY_EXIT_BOOKING_ID,
                        transaction.getBookingId());

                if (bookingList.isEmpty()) {
                    update.put(Constants.SYNC_ONLINE_ENTRY_EXIT_BOOKING_TYPE,
                            Constants.BOOKING_TYPE_MONTHLY);

                } else {
                    SimplyParkBookingTbl booking = bookingList.get(0);
                    update.put(Constants.SYNC_ONLINE_ENTRY_EXIT_BOOKING_TYPE,
                            booking.getBookingType());
                }

                DateTime dt = (DateTime.forInstant(transaction.getEntryTime(), Constants.TIME_ZONE));
                String entryTimeStr = dt.format("YYYY-MM-DD hh:mm:ss");
                update.put(Constants.SYNC_ONLINE_ENTRY_EXIT_ENTRY_TIME, entryTimeStr);

                DateTime eDt = (DateTime.forInstant(transaction.getExitTime(), Constants.TIME_ZONE));
                String exitTimeStr = eDt.format("YYYY-MM-DD hh:mm:ss");
                update.put(Constants.SYNC_ONLINE_ENTRY_EXIT_EXIT_TIME, exitTimeStr);

                update.put(Constants.SYNC_ONLINE_ENTRY_EXIT_PARKING_FEES, transaction.getParkingFees());

                transactionArray.put(update);

            }
            Log.i("ServerUpdationService", "SimplyParkBookingTbl Length:- " + pendingOpAppBookingsSize);
            for (int i = 0; i < pendingOpAppBookingsSize; i++) {
                SimplyParkBookingTbl booking = pendingOpAppBookingsList.get(i);


                JSONObject update = new JSONObject();
                update.put(Constants.SYNC_PUSH_MSG_TYPE_KEY,
                        Constants.SYNC_PUSH_MSG_TYPE_OP_APP_BOOKING_UPDATE);

                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_ID, booking.getBookingID());
                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_NAME, booking.getCustomerDesc());
                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_EMAIL, booking.getEmail());
                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_MOBILE, booking.getMobile());
                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_STATUS, booking.getBookingStatus());
                update.put(SYNC_ONLINE_OP_APP_BOOKING_IS_STAFF_PASS, booking.getIsIsStaffPass());
                Log.i("ServerUpdationService", "SimplyParkBookingTbl :- " + booking.getCustomerDesc() + ", IsStaffPass" + booking.getIsIsStaffPass());

                DateTime dt = (DateTime.forInstant(booking.getBookingStartTime(), Constants.TIME_ZONE));
                String entryTimeStr = dt.format("YYYY-MM-DD hh:mm:ss");
                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_START_DATE, entryTimeStr);

                DateTime eDt = (DateTime.forInstant(booking.getBookingEndTime(), Constants.TIME_ZONE));
                String exitTimeStr = eDt.format("YYYY-MM-DD hh:mm:ss");

                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_END_DATE, exitTimeStr);

                DateTime issueDate = (DateTime.forInstant(booking.getPassIssueDate(), Constants.TIME_ZONE));
                String issueDateStr = issueDate.format("YYYY-MM-DD hh:mm:ss");
                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_BOOKING_DATE, issueDateStr);

                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_ENCODE_PASS_CODE,
                        booking.getEncodedPassCode());
                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_SERIAL_NUM, booking.getSerialNumber());
                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_VEHICLE_REG, booking.getVehicleReg());
                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_VEHICLE_TYPE, booking.getVehicleType());
                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_NUM_SLOTS, booking.getNumBookedSlots());
                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_PARKING_FEES, booking.getParkingFees());
                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_DISCOUNT, 0);
                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_IS_RENEWAL, booking.getIsRenewalBooking());
                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_PARENT_BOOKING_ID,
                        booking.getParentBookingID());

                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_PAYMENT_MODE, booking.getPaymentMode());
                if (booking.getPaymentMode() == Constants.PAYMENT_MODE_CARD) {
                    PaymentInfoTbl paymentInfo = booking.getPaymentInfo();
                    if (paymentInfo != null) {
                        update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_INVOICE_ID,
                                paymentInfo.getInvoiceId());
                        update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_PAYMENT_ID,
                                paymentInfo.getPaymentId());

                    }
                }


                transactionArray.put(update);

            }


            Log.i("ServerUpdationService", "LostPassTbl Length:- " + pendingLostPassList);
            for (LostPassTbl lostPass : pendingLostPassList) {
                Log.i("ServerUpdationService", "LostPassTbl :- " + lostPass.toString());

                JSONObject update = new JSONObject();
                update.put(Constants.SYNC_PUSH_MSG_TYPE_KEY,
                        Constants.SYNC_PUSH_MSG_TYPE_OP_APP_BOOKING_MODIFIED);


                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_ID, lostPass.getBookingId());

                DateTime dt = (DateTime.forInstant(lostPass.getNewPassIssueDate(), Constants.TIME_ZONE));
                String newIssueTimeStr = dt.format("YYYY-MM-DD hh:mm:ss");
                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_NEW_ISSUE_DATE, newIssueTimeStr);

                DateTime oDt = (DateTime.forInstant(lostPass.getOldPassIssueDate(), Constants.TIME_ZONE));
                String oldIssueTimeStr = oDt.format("YYYY-MM-DD hh:mm:ss");

                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_OLD_ISSUE_DATE, oldIssueTimeStr);


                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_OLD_ENCODE_PASS_CODE,
                        lostPass.getOldEncodedPassCode());
                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_NEW_ENCODE_PASS_CODE,
                        lostPass.getNewEncodedPassCode());
                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_OLD_SERIAL_NUM, lostPass.getOldSerialNumber());
                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_NEW_SERIAL_NUM, lostPass.getNewSerialNumber());

                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_LOST_PASS_FEES, lostPass.getLostPassIssueFees());

                update.put(Constants.SYNC_ONLINE_OP_APP_BOOKING_PAYMENT_MODE, lostPass.getPaymentMode());

                transactionArray.put(update);

            }

            jsonDataObj.put("updates", transactionArray);


            RequestBody body = RequestBody.create(Constants.JSON, jsonDataObj.toString());


            return (new Request.Builder()
                    .url(url)
                    .post(body)
                    .build());

        }

    }


    private Intent prepareResult(int triggerActionName, int result) {
            /*
          * Creates a new Intent containing a Uri object
          * BROADCAST_ACTION is a custom Intent action
          */
        Intent localIntent =
                new Intent(Constants.SERVER_UPDATION_BROADCAST_RESULTS_ACTION);

        localIntent.putExtra(Constants.SERVER_UPDATION_BROADCAST_RESULTS_STATUS, result);

        localIntent.putExtra(Constants.TRIGGER_ACTION_NAME, triggerActionName);


        return localIntent;
    }

    private Intent prepareResult(int triggerActionName, int result, int syncId) {
            /*
          * Creates a new Intent containing a Uri object
          * BROADCAST_ACTION is a custom Intent action
          */
        Intent localIntent =
                new Intent(Constants.SERVER_UPDATION_BROADCAST_RESULTS_ACTION);

        localIntent.putExtra(Constants.SERVER_UPDATION_BROADCAST_RESULTS_STATUS, result);

        localIntent.putExtra(Constants.TRIGGER_ACTION_NAME, triggerActionName);
        localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, syncId);


        return localIntent;
    }

    private void sendResult(int triggerActionName, int result, int sync_id) {
        Intent intent = prepareResult(triggerActionName, result, sync_id);

        // Broadcasts the Intent to receivers in this app.
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

    }

    private void sendResult(Intent intent) {
        // Broadcasts the Intent to receivers in this app.
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

    }

    private void sendResult(int triggerActionName, int result) {
        sendResult(triggerActionName, result, "");

    }


    private void sendResult(int triggerActionName, int result, String error) {
            /*
          * Creates a new Intent containing a Uri object
          * BROADCAST_ACTION is a custom Intent action
          */
        Intent localIntent =
                new Intent(Constants.SERVER_UPDATION_BROADCAST_RESULTS_ACTION);

        localIntent.putExtra(Constants.SERVER_UPDATION_BROADCAST_RESULTS_STATUS, result);

        localIntent.putExtra(Constants.TRIGGER_ACTION_NAME, triggerActionName);


        localIntent.putExtra(Constants.ERROR, error);


        // Broadcasts the Intent to receivers in this app.
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);

    }


    /***********************************************************************************************
     * Misc util functions
     ********************************************************************************************/

    private void setTransactionStateOutOfSync(String parkingId) {

        List<TransactionTbl> transactionTblList = TransactionTbl.find(TransactionTbl.class,
                "m_parking_id = ?", parkingId);

        if (transactionTblList.isEmpty()) {
            //nothing to do here
            return;
        } else {
            TransactionTbl transaction = transactionTblList.get(0);

            transaction.setIsSyncedWithServer(false);

            transaction.save();

            startPushTransactionsTimer();


        }

    }

    private void setTransactionStateInSync(String parkingId) {

        List<TransactionTbl> transactionTblList = TransactionTbl.find(TransactionTbl.class,
                "m_parking_id = ?", parkingId);

        if (transactionTblList.isEmpty()) {
            //nothing to do here
            return;
        } else {
            TransactionTbl transaction = transactionTblList.get(0);

            transaction.setIsSyncedWithServer(true);

            transaction.save();


        }

    }

    private void setSpTransactionStateOutOfSync(String parkingId) {

        List<SimplyParkTransactionTbl> transactionTblList = SimplyParkTransactionTbl.find(
                SimplyParkTransactionTbl.class,
                "m_parking_id = ?", parkingId);

        if (transactionTblList.isEmpty()) {
            //nothing to do here
            return;
        } else {
            SimplyParkTransactionTbl transaction = transactionTblList.get(0);

            transaction.setIsSyncedWithServer(false);

            transaction.save();

            startPushTransactionsTimer();


        }

    }

    private void setSpTransactionStateInSync(String parkingId) {

        List<SimplyParkTransactionTbl> transactionTblList = SimplyParkTransactionTbl.find(
                SimplyParkTransactionTbl.class,
                "m_parking_id = ?", parkingId);

        if (transactionTblList.isEmpty()) {
            //nothing to do here
            return;
        } else {
            SimplyParkTransactionTbl transaction = transactionTblList.get(0);

            transaction.setIsSyncedWithServer(true);

            transaction.save();

        }

    }


    private void setMonthlyPassGenerationOutOfSync(String bookingId) {

        List<SimplyParkBookingTbl> bookingList = SimplyParkBookingTbl.find(
                SimplyParkBookingTbl.class,
                "m_booking_id = ?", bookingId);

        if (bookingList.isEmpty()) {
            //nothing to do here
            return;
        } else {
            SimplyParkBookingTbl booking = bookingList.get(0);

            booking.setIsSyncedWithServer(false);

            booking.save();

            startPushTransactionsTimer();

        }
    }

    private void setMonthlyPassGenerationInSync(String bookingId) {

        List<SimplyParkBookingTbl> bookingList = SimplyParkBookingTbl.find(
                SimplyParkBookingTbl.class,
                "m_booking_id = ?", bookingId);

        if (bookingList.isEmpty()) {
            //nothing to do here
            return;
        } else {
            SimplyParkBookingTbl booking = bookingList.get(0);

            booking.setIsSyncedWithServer(true);

            booking.save();

        }

    }

    private void startPushTransactionsTimer() {
        AlarmHandler.startAlarm(getApplicationContext(),
                Constants.TIMER_ACTION_PUSH,
                Constants.TIMER_ACTION_PUSH_DURATION_IN_SECONDS, false);

    }

    private void handleOfflineEntryExitUpdate(JSONObject serverTransaction,
                                              long openingTime, long closingTime) {
        String parkingId = serverTransaction.optString(Constants.SYNC_PARKING_ID);
        String reg = serverTransaction.optString(Constants.SYNC_REG_NUMBER);
        String entryTime = serverTransaction.optString(Constants.SYNC_ENTRY_TIME);
        String exitTime = serverTransaction.optString(Constants.SYNC_EXIT_TIME);
        int status = serverTransaction.optInt(Constants.SYNC_STATUS, 0);
        double parkingFees = serverTransaction.optDouble(Constants.SYNC_PARKING_FEES,
                0.00);
        int vehicleType = serverTransaction.optInt(Constants.SYNC_VEHICLE_TYPE,
                Constants.VEHICLE_TYPE_CAR);

        boolean isParkAndRide = serverTransaction.optBoolean(
                Constants.SYNC_PARK_AND_RIDE,
                false);

        boolean isUpdated = serverTransaction.optBoolean(Constants.SYNC_IS_UPDATED,
                false);

        int operatorId = serverTransaction.optInt(Constants.SYNC_OPERATOR_ID);


        List<TransactionTbl> transactionTblList = TransactionTbl.find(
                TransactionTbl.class, "m_parking_id = ?", parkingId);

        TransactionTbl transaction;
        boolean isNew = false;

        int existingStatus = Constants.PARKING_STATUS_NONE;
        int existingParkingFees = 0;

        if (transactionTblList.isEmpty()) {
            transaction = new TransactionTbl();
            transaction.setIsSyncedWithServer(true);
            isNew = true;

        } else {
            transaction = transactionTblList.get(0);

            //check if server is sending us stale information
            //this can happen if Car A entered through app 1. server
            //was NOT updated. However, Car A exited through app 2 via parkingID. Server
            //was not updated. App 2 now comes back online. Since, we
            //do pull before push, we need to makre sure server information
            //is fresh than ours
            //TODO: please check how this impacts prepaid parking

            if ((transaction.getParkingStatus() ==
                    Constants.PARKING_STATUS_EXITED && !isUpdated)
                    ||
                    transaction.getParkingStatus() ==
                            Constants.PARKING_STATUS_TRANSACTION_CANCELLED) {

                if (transaction.getCarRegNumber().isEmpty()) {
                    transaction.setCarRegNumber(reg);

                    if (!entryTime.equals(Constants.SYNC_NO_EXIT_TIME)) {

                        transaction.setEntryTime(
                                new DateTime(entryTime).
                                        getMilliseconds(Constants.TIME_ZONE));
                    }

                    transaction.save();
                }

                if (transaction.getIsSyncedWithServer()) {
                    //server is sending me stale information even though i am synced with server.
                    //set the sync state as false and try again
                    transaction.setIsSyncedWithServer(false);
                    transaction.save();
                }

                return;
            }

            //a redundant update is received
            if (transaction.getParkingStatus() == status /*&& !isUpdated*/) {
                if (transaction.getCarRegNumber().isEmpty() && !reg.isEmpty()) {
                    transaction.setCarRegNumber(reg);
                    transaction.save();
                }
                return;
            }

            existingStatus = transaction.getParkingStatus();
            existingParkingFees = (int) Math.ceil(transaction.getParkingFees());


        }

        if (entryTime.equals(Constants.SYNC_NO_EXIT_TIME)) {
            if (isNew) {
                transaction.setEntryTime(0);
            }
        } else {
            transaction.setEntryTime(
                    new DateTime(entryTime).getMilliseconds(Constants.TIME_ZONE));
        }
        if (exitTime.equals(Constants.SYNC_NO_EXIT_TIME)) {
            transaction.setExitTime(0);
        } else {
            transaction.setExitTime(
                    new DateTime(exitTime).getMilliseconds(Constants.TIME_ZONE));

        }

        transaction.setParkingStatus(status);
        transaction.setParkingFees(parkingFees);
        if (!reg.isEmpty()) {
            transaction.setCarRegNumber(reg);
        } else {
            //if reg received from server is empty and the current app has reg information
            //then push this update again
            if (!transaction.getCarRegNumber().isEmpty()) {
                transaction.setIsSyncedWithServer(false);
            }
        }
        transaction.setIsParkAndRide(isParkAndRide);
        transaction.setVehicleType(vehicleType);
        transaction.setParkingId(parkingId);

        boolean isUpdatedAtServer = transaction.getIsUpdated();

        if (isUpdatedAtServer || isNew) {
            transaction.setIsUpdated(true);
        }

        boolean isParkingPrepaid = (Boolean) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY, Constants.TYPE_BOOL);


        if (isParkingPrepaid) {
            transaction.setEntryTime(transaction.getExitTime());

        }

        transaction.setOperatorId(operatorId);

        transaction.save();


        switch (transaction.getParkingStatus()) {
            case Constants.PARKING_STATUS_ENTERED: {
                //either this is a new entry or an exited entry updated
                //to entered state by cancelling exit. Both cases
                //count is incremented
                MiscUtils.incrementOccupancyCount(getApplicationContext(),
                        transaction.getVehicleType());


                //this transaction was cancelled
                if (existingStatus == Constants.PARKING_STATUS_EXITED) {
                    if (transaction.getEntryTime() >= openingTime &&
                            transaction.getExitTime() <= closingTime) {
                        MiscUtils.decrementTodaysEarnings(getApplicationContext(),
                                existingParkingFees);
                    }

                }
            }
            break;

            case Constants.PARKING_STATUS_EXITED: {
                if (transaction.getEntryTime() >= openingTime &&
                        transaction.getExitTime() <= closingTime) {
                    MiscUtils.incrementTodaysEarnings(getApplicationContext(),
                            (int) Math.ceil(transaction.getParkingFees()));

                }

                if (!isNew && existingStatus == Constants.PARKING_STATUS_ENTERED) {
                    MiscUtils.decrementOccupancyCount(getApplicationContext(),
                            transaction.getVehicleType());
                }

                if (isNew && isParkingPrepaid) {
                    MiscUtils.incrementVehiclesParkedCount(getApplicationContext(),
                            transaction.getVehicleType());
                }


            }
            break;

            case Constants.PARKING_STATUS_TRANSACTION_CANCELLED: {
                if (!isNew) {
                    if (isParkingPrepaid) {
                        MiscUtils.decrementVehiclesParkedCount(getApplicationContext(),
                                transaction.getVehicleType());

                    } else {
                        MiscUtils.decrementOccupancyCount(getApplicationContext(),
                                transaction.getVehicleType());
                    }
                }

            }
            break;
        }

    }

    private boolean handleOpAppBookingUpdate(JSONObject opAppBooking) {
        boolean updatePassesCount = false;
        String bookingId = opAppBooking.optString(Constants.SYNC_ONLINE_OP_APP_BOOKING_ID);
        String encodedPassCode = opAppBooking.optString(Constants.SYNC_ONLINE_OP_APP_BOOKING_ENCODE_PASS_CODE);

        String serialNum = opAppBooking.optString(Constants.SYNC_ONLINE_OP_APP_BOOKING_SERIAL_NUM);
        String customerFirstName = opAppBooking.optString(
                Constants.SYNC_ONLINE_OP_APP_BOOKING_CUSTOMER_FIRST_NAME);
        String customerLastName = opAppBooking.optString(
                Constants.SYNC_ONLINE_OP_APP_BOOKING_CUSTOMER_LAST_NAME);

        int bookingType = opAppBooking.optInt(Constants.SYNC_ONLINE_OP_APP_BOOKING_TYPE,
                Constants.BOOKING_TYPE_NA);
        String startDateStr = opAppBooking.optString(Constants.SYNC_ONLINE_OP_APP_BOOKING_START_DATE);
        String endDateStr = opAppBooking.optString(Constants.SYNC_ONLINE_OP_APP_BOOKING_END_DATE);
        String bookingDateStr =
                opAppBooking.optString(Constants.SYNC_ONLINE_OP_APP_BOOKING_BOOKING_DATE);
        int numSlots = opAppBooking.optInt(Constants.SYNC_ONLINE_OP_APP_BOOKING_NUM_SLOTS, 0);
        int status = opAppBooking.optInt(Constants.SYNC_ONLINE_OP_APP_BOOKING_STATUS,
                Constants.BOOKING_STATUS_WAITING);

        int spaceOwnerId = opAppBooking.optInt(Constants.SYNC_ONLINE_OP_APP_BOOKING_SPACE_OWNER_ID,
                0);

        String email = opAppBooking.optString(Constants.SYNC_ONLINE_OP_APP_BOOKING_EMAIL);
        String mobile = opAppBooking.optString(Constants.SYNC_ONLINE_OP_APP_BOOKING_MOBILE);
        String vehicleReg = opAppBooking.optString(Constants.SYNC_ONLINE_OP_APP_BOOKING_VEHICLE_REG);
        int vehicleType = opAppBooking.optInt(Constants.SYNC_ONLINE_OP_APP_BOOKING_VEHICLE_TYPE,
                Constants.VEHICLE_TYPE_CAR);

        int parkingFees = opAppBooking.optInt(Constants.SYNC_ONLINE_OP_APP_BOOKING_PARKING_FEES, 0);
        boolean isRenewal = opAppBooking.optBoolean(Constants.SYNC_ONLINE_OP_APP_BOOKING_IS_RENEWAL, false);

        String parentBookingId = opAppBooking.optString(Constants.SYNC_ONLINE_OP_APP_BOOKING_PARENT_BOOKING_ID);

        int paymentMode = opAppBooking.optInt(Constants.SYNC_ONLINE_OP_APP_BOOKING_PAYMENT_MODE,
                Constants.PAYMENT_MODE_CASH);

        int operatorId = opAppBooking.optInt(Constants.SYNC_ONLINE_OP_APP_BOOKING_OPERATOR_ID,
                Constants.INVALID_OPERATOR_ID);

        String invoiceId = opAppBooking.optString(Constants.SYNC_ONLINE_OP_APP_BOOKING_INVOICE_ID);
        String paymentId = opAppBooking.optString(Constants.SYNC_ONLINE_OP_APP_BOOKING_PAYMENT_ID);
        boolean isPassForStaff = opAppBooking.optBoolean(SYNC_ONLINE_OP_APP_BOOKING_IS_STAFF_PASS, false);
        Cache.setInCache(getApplicationContext(), Constants.CACHE_PREFIX_SPACE_OWNER_ID_QUERY,
                spaceOwnerId, Constants.TYPE_INT);


        List<SimplyParkBookingTbl> bookingList = SimplyParkBookingTbl.find(
                SimplyParkBookingTbl.class,
                "m_booking_id = ?", bookingId);

        SimplyParkBookingTbl booking;
        boolean isNew = false;
        int oldBookingStatus = Constants.BOOKING_STATUS_WAITING;

        if (!bookingList.isEmpty()) {
            booking = bookingList.get(0);
            booking.setSlotsInUse(0);
            oldBookingStatus = booking.getBookingStatus();
            //if a booking exits, dont change its sync state with server
        } else {
            booking = new SimplyParkBookingTbl();
            booking.setIsSyncedWithServer(true);
            isNew = true;
        }

        booking.setIsRenewalBooking(isRenewal);
        booking.setOperatorId(operatorId);
        if (booking.getIsRenewalBooking()) {
            booking.setParentBookingID(parentBookingId);
        }

        booking.setBookingID(bookingId);
        booking.setEncodedPassCode(encodedPassCode);
        booking.setBookingStatus(status);
        booking.setBookingType(bookingType);
        booking.setCustomerDesc(customerFirstName + " " + customerLastName);
        booking.setSerialNumber(serialNum);

        DateTime startDate = new DateTime(startDateStr);
        booking.setBookingStartTime(startDate.getMilliseconds(Constants.TIME_ZONE));

        DateTime endDate = new DateTime(endDateStr);
        booking.setBookingEndTime(endDate.getMilliseconds(Constants.TIME_ZONE));

        DateTime passIssueDate = new DateTime(bookingDateStr);
        booking.setPassIssueDate(passIssueDate.getMilliseconds(Constants.TIME_ZONE));

        booking.setNumBookedSlots(numSlots);

        booking.setCustomerDesc(customerFirstName + " " + customerLastName);
        booking.setEmail(email);
        booking.setMobile(mobile);
        booking.setParkingFees(parkingFees);
        booking.setVehicleReg(vehicleReg);
        booking.setVehicleType(vehicleType);

        boolean isSlotOccupiedAssumed = (Boolean) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_IS_SLOT_OCCUPIED_ASSUMED_QUERY,
                Constants.TYPE_BOOL);

        booking.setIsSlotOccupiedAssumed(isSlotOccupiedAssumed);

        booking.setPaymentMode(paymentMode);
        booking.setIsStaffPass(isPassForStaff);
        booking.save();

        if (booking.getPaymentMode() == Constants.PAYMENT_MODE_CARD) {
            PaymentInfoTbl paymentInfo = booking.getPaymentInfo();

            if (paymentInfo == null) {
                paymentInfo = new PaymentInfoTbl();
            }

            paymentInfo.setBookingId(booking.getBookingID());
            paymentInfo.setInvoiceId(invoiceId);
            paymentInfo.setPaymentId(paymentId);
            paymentInfo.save();
        }

        if (booking.getIsRenewalBooking()) {
            List<SimplyParkBookingTbl> parentBookingList = SimplyParkBookingTbl.find(
                    SimplyParkBookingTbl.class, "m_booking_id = ?", booking.getParentBookingID());

            if (!parentBookingList.isEmpty()) {
                SimplyParkBookingTbl parentBooking = parentBookingList.get(0);
                long nextRenewalDate = parentBooking.getNextRenewalDateTime();
                long toDateTime = booking.getBookingEndTime();

                if (nextRenewalDate < toDateTime) {
                    parentBooking.setNextRenewalDateTime(toDateTime);
                    parentBooking.save();
                }
            }
        }

        DateTime today = DateTime.today(Constants.TIME_ZONE);
        long todayInMs = today.getMilliseconds(Constants.TIME_ZONE);

        long startDateTime = booking.getBookingStartTime();
        long endDateTime = booking.getBookingEndTime();
        boolean isActiveToday = false;
        if (todayInMs >= startDateTime && todayInMs <= endDateTime) {
            isActiveToday = true;
        }

        int updatedStatus = booking.getBookingStatus();

        if (isNew) {
            if (updatedStatus != Constants.BOOKING_STATUS_CANCELLED ||
                    updatedStatus != Constants.BOOKING_STATUS_EXPIRED) {
                MiscUtils.incrementMonthlyPassCounters(getApplicationContext(), isActiveToday);
            } else {
                //passing false as its a new booking therefore this was never added to the active
                //passes
                MiscUtils.decrementMonthlyPassCounters(getApplicationContext(), false);
            }
        } else {
            if (oldBookingStatus != updatedStatus) {
                //Currently we only send an update when a new booking is created. Therefore,
                //this scenario is currently not possible
            }
        }


        return updatePassesCount;

    }


    private void handleOpAppLostPassUpdate(JSONObject lostPassBooking) {
        boolean updatePassesCount = false;
        String bookingId = lostPassBooking.optString(Constants.SYNC_ONLINE_OP_APP_GENERATED_BOOKING_ID);
        String oldEncodedPassCode = lostPassBooking.optString(Constants.SYNC_ONLINE_OP_APP_BOOKING_OLD_ENCODE_PASS_CODE);
        String newEncodedPassCode = lostPassBooking.optString(Constants.SYNC_ONLINE_OP_APP_BOOKING_NEW_ENCODE_PASS_CODE);

        String oldBookingIssueDate = lostPassBooking.optString(Constants.SYNC_ONLINE_OP_APP_BOOKING_OLD_ISSUE_DATE);
        String newBookingIssueDate = lostPassBooking.optString(Constants.SYNC_ONLINE_OP_APP_BOOKING_NEW_ISSUE_DATE);

        String oldSerialNo = lostPassBooking.optString(Constants.SYNC_ONLINE_OP_APP_BOOKING_OLD_SERIAL_NUM);
        String newSerialNo = lostPassBooking.optString(Constants.SYNC_ONLINE_OP_APP_BOOKING_NEW_SERIAL_NUM);

        int lostPassFees = lostPassBooking.optInt(Constants.SYNC_ONLINE_OP_APP_BOOKING_LOST_PASS_FEES, 0);


        int paymentMode = lostPassBooking.optInt(Constants.SYNC_ONLINE_OP_APP_BOOKING_PAYMENT_MODE,
                Constants.PAYMENT_MODE_CASH);

        int operatorId = lostPassBooking.optInt(Constants.SYNC_ONLINE_OP_APP_BOOKING_OPERATOR_ID,
                Constants.INVALID_OPERATOR_ID);


        List<SimplyParkBookingTbl> bookingList = SimplyParkBookingTbl.find(
                SimplyParkBookingTbl.class,"m_booking_id = ?",bookingId);

        SimplyParkBookingTbl booking;


        if(bookingId.isEmpty()){
            return;
        }else{
            booking = bookingList.get(0);
        }
        List<LostPassTbl> lostPassList = LostPassTbl.find(
                LostPassTbl.class,
                "m_booking_id = ? and m_old_encoded_pass_code = ?",
                bookingId, oldEncodedPassCode);

        LostPassTbl lostPass;
        boolean isNew = false;
        int oldBookingStatus = Constants.BOOKING_STATUS_WAITING;

        if (!lostPassList.isEmpty()) {
            lostPass = lostPassList.get(0);
        } else {
            lostPass = new LostPassTbl();
            lostPass.setIsSyncedWithServer(true);
            isNew = true;
        }

        lostPass.setOperatorId(operatorId);

        lostPass.setBookingId(bookingId);
        lostPass.setNewEncodedPassCode(newEncodedPassCode);
        lostPass.setOldEncodedPassCode(oldEncodedPassCode);
        lostPass.setNewSerialNumber(newSerialNo);
        lostPass.setOldSerialNumber(oldSerialNo);

        DateTime oldBookingIssueDate_DATETIME = new DateTime(oldBookingIssueDate);
        lostPass.setOldPassIssueDate(oldBookingIssueDate_DATETIME.getMilliseconds(Constants.TIME_ZONE));

        DateTime newBookingIssueDate_DATETIME = new DateTime(newBookingIssueDate);
        lostPass.setNewPassIssueDate(newBookingIssueDate_DATETIME.getMilliseconds(Constants.TIME_ZONE));


        lostPass.setPaymentMode(paymentMode);
        lostPass.setLostPassIssueFees(lostPassFees);
        lostPass.save();

        booking.setEncodedPassCode(newEncodedPassCode);
        booking.save();



    }


    private void handleSimplyParkBookingUpdate(JSONObject serverTransaction) {
        String bookingId = serverTransaction.optString(Constants.SYNC_ONLINE_BOOKING_ENCODE_PASS_CODE);
        String serialNum = serverTransaction.optString(Constants.SYNC_ONLINE_BOOKING_SERIAL_NUM);
        boolean isPassForStaff = serverTransaction.optBoolean(SYNC_ONLINE_OP_APP_BOOKING_IS_STAFF_PASS, false);
        String customerFirstName = serverTransaction.optString(
                Constants.SYNC_ONLINE_BOOKING_CUSTOMER_FIRST_NAME);
        String customerLastName = serverTransaction.optString(
                Constants.SYNC_ONLINE_BOOKING_CUSTOMER_LAST_NAME);

        int bookingType = serverTransaction.optInt(Constants.SYNC_ONLINE_BOOKING_TYPE,
                Constants.BOOKING_TYPE_NA);

        String startDateStr = serverTransaction.optString(Constants.SYNC_ONLINE_BOOKING_START_DATE);
        String endDateStr = serverTransaction.optString(Constants.SYNC_ONLINE_BOOKING_END_DATE);
        String bookingDateStr =
                serverTransaction.optString(Constants.SYNC_ONLINE_BOOKING_BOOKING_DATE);
        int numSlots = serverTransaction.optInt(Constants.SYNC_ONLINE_BOOKING_NUM_SLOTS, 0);
        int status = serverTransaction.optInt(Constants.SYNC_ONLINE_BOOKING_STATUS,
                Constants.BOOKING_STATUS_WAITING);

        int spaceOwnerId = serverTransaction.optInt(Constants.SYNC_ONLINE_BOOKING_SPACE_OWNER_ID,
                0);

        Cache.setInCache(getApplicationContext(), Constants.CACHE_PREFIX_SPACE_OWNER_ID_QUERY,
                spaceOwnerId, Constants.TYPE_INT);

        List<SimplyParkBookingTbl> bookingList = SimplyParkBookingTbl.find(
                SimplyParkBookingTbl.class,
                "m_booking_id = ?", bookingId);

        SimplyParkBookingTbl booking;

        if (!bookingList.isEmpty()) {
            booking = bookingList.get(0);
            booking.setSlotsInUse(0);
        } else {
            booking = new SimplyParkBookingTbl();
        }


        booking.setBookingID(bookingId);
        booking.setBookingStatus(status);
        booking.setBookingType(bookingType);
        booking.setCustomerDesc(customerFirstName + " " + customerLastName);
        booking.setSerialNumber(serialNum);
        booking.setIsSyncedWithServer(true);
        booking.setIsStaffPass(isPassForStaff);
        DateTime startDate = new DateTime(startDateStr);
        booking.setBookingStartTime(startDate.getMilliseconds(Constants.TIME_ZONE));

        DateTime endDate = new DateTime(endDateStr);
        booking.setBookingEndTime(endDate.getMilliseconds(Constants.TIME_ZONE));

        DateTime passIssueDate = new DateTime(bookingDateStr);
        booking.setPassIssueDate(passIssueDate.getMilliseconds(Constants.TIME_ZONE));

        booking.setNumBookedSlots(numSlots);

        boolean isSlotOccupiedAssumed = (Boolean) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_IS_SLOT_OCCUPIED_ASSUMED_QUERY,
                Constants.TYPE_BOOL);

        booking.setIsSlotOccupiedAssumed(isSlotOccupiedAssumed);

        booking.save();

    }

    private void handleOnlineEntryExitUpdate(JSONObject serverTransaction,
                                             long openingTime, long closingTime) {
        String bookingId = serverTransaction.optString(Constants.SYNC_ONLINE_ENTRY_EXIT_BOOKING_ID);
        String parkingId = serverTransaction.optString(Constants.SYNC_ONLINE_ENTRY_EXIT_PARKING_ID);
        String vehicleReg = serverTransaction.optString(Constants.SYNC_ONLINE_ENTRY_EXIT_VEHICLE_REG);
        int vehicleType = serverTransaction.optInt(Constants.SYNC_ONLINE_ENTRY_EXIT_VEHICLE_TYPE,
                Constants.VEHICLE_TYPE_CAR);
        String entryTime = serverTransaction.optString(Constants.SYNC_ONLINE_ENTRY_EXIT_ENTRY_TIME);
        String exitTime = serverTransaction.optString(Constants.SYNC_ONLINE_ENTRY_EXIT_EXIT_TIME);
        int status = serverTransaction.optInt(Constants.SYNC_ONLINE_ENTRY_EXIT_STATUS,
                Constants.PARKING_STATUS_NONE);
        int parkingFees = serverTransaction.optInt(Constants.SYNC_ONLINE_ENTRY_EXIT_PARKING_FEES, 0);


        boolean isParkAndRide = serverTransaction.optBoolean(
                Constants.SYNC_ONLINE_ENTRY_EXIT_IS_PARK_AND_RIDE, false);

        boolean isUpdated = serverTransaction.optBoolean(Constants.SYNC_ONLINE_ENTRY_EXIT_IS_UPDATED,
                false);

        int operatorId = serverTransaction.optInt(Constants.SYNC_ONLINE_ENTRY_EXIT_OPERATOR_ID,
                Constants.INVALID_OPERATOR_ID);


        List<SimplyParkTransactionTbl> transactionTblList = SimplyParkTransactionTbl.find(
                SimplyParkTransactionTbl.class, "m_parking_id = ?", parkingId);

        SimplyParkTransactionTbl transaction;
        boolean isNew = false;

        int existingStatus = Constants.PARKING_STATUS_NONE;
        int existingParkingFees = 0;

        List<SimplyParkBookingTbl> bookingList = SimplyParkBookingTbl.find(
                SimplyParkBookingTbl.class, "m_booking_id = ?", bookingId);

        if (bookingList.isEmpty()) {
            MiscUtils.errorMsgs.add("handleOnlineEntryExitUpdate: No booking Found for " + bookingId
                    + "Rejecting Update received from Server");
            return;
        }

        SimplyParkBookingTbl booking = bookingList.get(0);


        if (transactionTblList.isEmpty()) {
            transaction = new SimplyParkTransactionTbl();
            transaction.setBookingId(bookingId);
            transaction.setIsSyncedWithServer(true);
            isNew = true;

        } else {
            transaction = transactionTblList.get(0);
            //check if server is sending us stale information
            //this can happen if Car A entered through app 1. server
            //was updated. However, Car A exited through app 2. Server
            //was not updated. App 2 now comes back online. Since, we
            //do pull before push, we need to makre sure server information
            //is fresh than ours
            //TODO: please check how this impacts prepaid parking

            if ((transaction.getStatus() ==
                    Constants.PARKING_STATUS_EXITED && !isUpdated)
                    ||
                    transaction.getStatus() ==
                            Constants.PARKING_STATUS_TRANSACTION_CANCELLED) {

                if (transaction.getVehicleReg().isEmpty()) {
                    transaction.setVehicleReg(vehicleReg);

                    if (!entryTime.equals(Constants.SYNC_NO_EXIT_TIME)) {

                        transaction.setEntryTime(
                                new DateTime(entryTime).
                                        getMilliseconds(Constants.TIME_ZONE));
                    }

                    transaction.save();
                }

                if (transaction.getBookingId().isEmpty()) {
                    transaction.setBookingId(booking.getBookingID());
                    transaction.save();

                    booking.decrementSlotsInUse(getApplicationContext(),
                            transaction.getVehicleType());
                    booking.save();
                }

                if (transaction.getIsSyncedWithServer()) {
                    //server is sending me stale information even though i am synced with server.
                    //set the sync state as false and try again
                    transaction.setIsSyncedWithServer(false);
                }


                return;
            }

            //a redundant update is received
            if (transaction.getStatus() == status /*&& !isUpdated*/) {
                return;
            }

            existingStatus = transaction.getStatus();
            existingParkingFees = (int) Math.ceil(transaction.getParkingFees());


        }

        if (entryTime.equals(Constants.SYNC_NO_EXIT_TIME)) {
            if (isNew) {
                transaction.setEntryTime(0);
            }
        } else {
            transaction.setEntryTime(
                    new DateTime(entryTime).getMilliseconds(Constants.TIME_ZONE));
        }
        if (exitTime.equals(Constants.SYNC_NO_EXIT_TIME)) {
            transaction.setExitTime(0);
        } else {
            transaction.setExitTime(
                    new DateTime(exitTime).getMilliseconds(Constants.TIME_ZONE));

        }

        transaction.setStatus(status);
        transaction.setParkingFees(parkingFees);
        transaction.setOperatorId(operatorId);
        if (!vehicleReg.isEmpty()) {
            transaction.setVehicleReg(vehicleReg);
        } else {
            //if reg received from server is empty and the current app has reg information
            //then push this update again
            if (!transaction.getVehicleReg().isEmpty()) {
                transaction.setIsSyncedWithServer(false);
            }
        }
        transaction.setIsParkAndRide(isParkAndRide);
        transaction.setVehicleType(vehicleType);
        transaction.setParkingId(parkingId);

        boolean isUpdatedAtServer = transaction.getIsUpdated();

        if (isUpdatedAtServer || isNew) {
            transaction.setIsUpdated(true);
        }

        transaction.save();

        //an exit happend through this app. However, the app had no entry information.
        //booking numSlotsInUse was reduced but the server was not synced that the pass
        //has also exited.
        List<PendingExits> pendingExits = booking.getPendingExits();
        if (!pendingExits.isEmpty()) {
            for (ListIterator<PendingExits> iter = pendingExits.listIterator();
                 iter.hasNext(); ) {
                PendingExits exit = iter.next();
                if (exit.getVehicleReg().equals(transaction.getVehicleReg())) {
                    transaction.setStatus(Constants.PARKING_STATUS_EXITED);
                    transaction.setExitTime(exit.getExitDateTime());
                    transaction.save();


                    //Intent service to update the server
                    ServerUpdationIntentService.startActionSimplyParkLogExit(getApplicationContext(),
                            transaction.getBookingId(),
                            transaction.getParkingId(),
                            transaction.getExitTime(),
                            transaction.getParkingFees(),
                            transaction.getVehicleType(),
                            transaction.getVehicleReg());

                    exit.delete();
                }
            }

        } else {


            switch (transaction.getStatus()) {
                case Constants.PARKING_STATUS_ENTERED: {
                    //either this is a new entry or an exited entry updated
                    //to entered state by cancelling exit. Both cases
                    //count is incremented
                    MiscUtils.incrementOccupancyCount(getApplicationContext(),
                            transaction.getVehicleType());

                    booking.incrementSlotsInUse(getApplicationContext(), transaction.getVehicleType());

                }
                break;

                case Constants.PARKING_STATUS_EXITED: {

                    if (!isNew && existingStatus == Constants.PARKING_STATUS_ENTERED) {
                        MiscUtils.decrementOccupancyCount(getApplicationContext(),
                                transaction.getVehicleType());
                        booking.decrementSlotsInUse(getApplicationContext(),
                                transaction.getVehicleType());


                    }
                }
                break;

                case Constants.PARKING_STATUS_TRANSACTION_CANCELLED: {
                    if (!isNew) {
                        MiscUtils.decrementOccupancyCount(getApplicationContext(),
                                transaction.getVehicleType());

                        booking.decrementSlotsInUse(getApplicationContext(),
                                transaction.getVehicleType());


                    }

                }
                break;
            }
            booking.save();
        }
    }


    private void setLostMonthlyPassReIssueOutOfSync(String bookingId, String passCode) {

        List<LostPassTbl> bookingList = LostPassTbl.find(
                LostPassTbl.class,
                "m_booking_id = ? AND m_new_encoded_pass_code = ?", bookingId,passCode);

        if (bookingList.isEmpty()) {
            //nothing to do here
            return;
        } else {
            LostPassTbl booking = bookingList.get(0);

            booking.setIsSyncedWithServer(false);

            booking.save();

            startPushTransactionsTimer();

        }
    }

    private void setLostMonthlyPassReIssueInSync(String bookingId, String passCode) {

        List<LostPassTbl> bookingList = LostPassTbl.find(
                LostPassTbl.class,
                "m_booking_id = ? AND m_new_encoded_pass_code = ?", bookingId,passCode);

        if (bookingList.isEmpty()) {
            //nothing to do here
            return;
        } else {
            LostPassTbl booking = bookingList.get(0);

            booking.setIsSyncedWithServer(true);

            booking.save();

        }

    }


    public static void startActionLostMonthlPass(Context context, LostPassTbl lostPass) {
        startActionLogMonthlyPassReGenerated(context, lostPass, "", "");
    }
    public void handleActionModifyMonthlyPass(String bookingID, String old_encoded_pass_code, String new_encoded_pass_code,
                                                       Long old_pass_issue_date, Long new_pass_issue_date, String old_serial_number,
                                                       String new_serial_number, int payment_mode, int lost_pass_fees){


        if (checkForConnectivity(getApplicationContext()) == false) {
            //set the state of the entry object to NOT in sync

            setLostMonthlyPassReIssueOutOfSync(bookingID,new_encoded_pass_code);
            sendResult(Constants.TRIGGER_ACTION_LOST_MONTHLY_PASS, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

            return;

        }
        int result = Constants.OK;
        try {
            OkHttpClient client = new OkHttpClient();
            Request request = buildModifyBookingRequest(bookingID, old_encoded_pass_code, new_encoded_pass_code,
                    old_pass_issue_date, new_pass_issue_date,old_serial_number,
                    new_serial_number, payment_mode, lost_pass_fees);


            Response response = client.newCall(request).execute();

            int serverSyncId = 0;

            /*
             *   If the response from the server is 2 [i.e. the booking does not exist at server],
             *   then the booking has to be searched in the app and its sync_with_server_flag needs to be set to false.
             *   This will ensure that booking & its modification both will reach the server.
            */
            if (response.isSuccessful()) {
                String responseData = response.body().string();
                JSONObject json = new JSONObject(responseData);
                Toast.makeText(this,responseData,Toast.LENGTH_SHORT).show();
                int status = json.getInt("status");

                if (status == Constants.OK) {

                    serverSyncId = json.getInt("syncId");

                    Context context = getApplicationContext();
                    int syncIdAtClient = MiscUtils.getSyncId(context);

                    if ((syncIdAtClient + 1) == serverSyncId) {
                        //Moving the syncID increment after receiveing a successful response
                        MiscUtils.updateSyncId(getApplicationContext(), 1);
                    }

                    setLostMonthlyPassReIssueInSync(bookingID,new_encoded_pass_code);


                }else{
                    /*The substatus will be used to check in case the result status is not OK*/
                    int substatus = json.getInt("substatus");

                    if(substatus == Constants.OK){
                        setMonthlyPassGenerationOutOfSync(bookingID);
                        setLostMonthlyPassReIssueOutOfSync(bookingID,new_encoded_pass_code);
                    }
                }
            } else {

                String reason = response.message();
                MiscUtils.errorMsgs.add("handleActionModifyMonthlyPass, Failed with Reason " + reason);
                setLostMonthlyPassReIssueOutOfSync(bookingID,new_encoded_pass_code);

                sendResult(Constants.TRIGGER_ACTION_LOST_MONTHLY_PASS, Constants.NOK,
                        reason);
                response.body().close();

                return;

            }

            Intent localIntent = prepareResult(Constants.TRIGGER_ACTION_LOST_MONTHLY_PASS, result);
            localIntent.putExtra(Constants.BUNDLE_PARAM_SYNC_ID, serverSyncId);


            sendResult(localIntent);

        } catch (JSONException ioe) {
            MiscUtils.errorMsgs.add("handleActionModifyMonthlyPass, JSON Exception:: " + ioe);

            setLostMonthlyPassReIssueOutOfSync(bookingID,new_encoded_pass_code);

            sendResult(Constants.TRIGGER_ACTION_LOST_MONTHLY_PASS, Constants.NOK,
                    Constants.ERROR_INVALID_RESPONSE);
        } catch (java.io.IOException ioe) {
            MiscUtils.errorMsgs.add("handleActionModifyMonthlyPass, IO Exception:: " + ioe);


            setLostMonthlyPassReIssueOutOfSync(bookingID,new_encoded_pass_code);

            sendResult(Constants.TRIGGER_ACTION_LOST_MONTHLY_PASS, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);


        }

    }

    /**
     * Starts this service to perform action log lost monthly Pass Re Issue with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionLogMonthlyPassReGenerated(Context context,
                                                          LostPassTbl lostPass,
                                                          String invoiceId, String paymentId) {


        Intent intent = new Intent(context, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_LOST_MONTHLY_PASS_REISSUE);
        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_ID, lostPass.getBookingId());
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_ISSUE_DATE, lostPass.getNewPassIssueDate());
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_OLD_ISSUE_DATE, lostPass.getOldPassIssueDate());
        intent.putExtra(Constants.BUNDLE_PARAM_ENCODE_PASS_CODE, lostPass.getNewEncodedPassCode());
        intent.putExtra(Constants.BUNDLE_PARAM_OLD_ENCODE_PASS_CODE, lostPass.getOldEncodedPassCode());
        intent.putExtra(Constants.BUNDLE_PARAM_SERIAL_NUMBER, lostPass.getNewSerialNumber());
        intent.putExtra(Constants.BUNDLE_PARAM_OLD_SERIAL_NUMBER, lostPass.getOldSerialNumber());
        intent.putExtra(Constants.BUNDLE_PARAM_PAYMENT_MODE, lostPass.getPaymentMode());
        intent.putExtra(Constants.BUNDLE_PARAM_LOST_MONTHLY_PASS_FEE, lostPass.getLostPassIssueFees());


        context.startService(intent);


    }

    private Request buildModifyBookingRequest(String bookingID, String old_encoded_pass_code, String new_encoded_pass_code,
                                              Long old_pass_issue_date, Long new_pass_issue_date, String old_serial_number,
                                              String new_serial_number, int payment_mode, int lost_pass_fees)throws JSONException {
        String spaceId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT)).toString();

        int operatorId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_OPERATOR_QUERY,
                Constants.TYPE_INT));

        int gcmId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_GCM_ID_AT_SERVER_QUERY,
                Constants.TYPE_INT));


        String url = Constants.MODIFY_BOOKING + spaceId + ".json";

        JSONObject jsonDataObj = new JSONObject();

        jsonDataObj.put("operator_id", operatorId);
        jsonDataObj.put("gcm_id", gcmId);
        jsonDataObj.put("booking_id", bookingID);
        jsonDataObj.put("old_encoded_pass_code", old_encoded_pass_code);
        jsonDataObj.put("new_encoded_pass_code", new_encoded_pass_code);

        jsonDataObj.put("old_pass_issue_date", DateTime.forInstant(old_pass_issue_date, Constants.TIME_ZONE).
                format("YYYY-MM-DD hh:mm:ss"));
        jsonDataObj.put("new_pass_issue_date", DateTime.forInstant(new_pass_issue_date, Constants.TIME_ZONE).
                format("YYYY-MM-DD hh:mm:ss"));

        jsonDataObj.put("old_pass_issue_date_timestamp", old_pass_issue_date);
        jsonDataObj.put("new_pass_issue_date_timestamp", new_pass_issue_date);
        jsonDataObj.put("old_serial_number", old_serial_number);
        jsonDataObj.put("new_serial_number", new_serial_number);
        jsonDataObj.put("payment_mode", payment_mode);
        jsonDataObj.put("lost_pass_fees", lost_pass_fees);


        RequestBody body = RequestBody.create(Constants.JSON, jsonDataObj.toString());

        return (new Request.Builder()
                .url(url)
                .post(body)
                .build());

    }

    public static void startActionHandleLostPassNotificationFromAnotherApp(Context applicationContext, int operatorId,
                                                                           String operatorDesc, String bookingId, String oldEncodedPassCode,
                                                                           String newEncodedPassCode, Long oldPassIssueDate,
                                                                           Long newPassIssueDate, String oldSerialNumber,
                                                                           String newSerialNumber, int paymentMode, int lostPassFees) {
        Intent intent = new Intent(applicationContext, ServerUpdationIntentService.class);
        intent.setAction(Constants.ACTION_OPAPP_BOOKING_MODIFICATION_NOTIFICATION_FROM_ANOTHER_APP);
        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_ID, bookingId);
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_ISSUE_DATE, newPassIssueDate);
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_OLD_ISSUE_DATE, oldPassIssueDate);
        intent.putExtra(Constants.BUNDLE_PARAM_ENCODE_PASS_CODE, newEncodedPassCode);
        intent.putExtra(Constants.BUNDLE_PARAM_OLD_ENCODE_PASS_CODE, oldEncodedPassCode);
        intent.putExtra(Constants.BUNDLE_PARAM_SERIAL_NUMBER, newSerialNumber);
        intent.putExtra(Constants.BUNDLE_PARAM_OLD_SERIAL_NUMBER, oldSerialNumber);
        intent.putExtra(Constants.BUNDLE_PARAM_PAYMENT_MODE, paymentMode);
        intent.putExtra(Constants.BUNDLE_PARAM_LOST_MONTHLY_PASS_FEE, lostPassFees);
        intent.putExtra(Constants.BUNDLE_PARAM_OPERATOR_ID,operatorId);
        intent.putExtra(Constants.BUNDLE_PARAM_OPERATOR_DESC,operatorDesc);
        applicationContext.startService(intent);
    }

    private void handleActionModifyMonthlyPassFromAnotherApp(int operatorId,
                                                             String operatorDesc, String bookingId, String oldEncodedPassCode,
                                                             String newEncodedPassCode, long oldPassIssueDate, long newPassIssueDate,
                                                             String oldSerialNumber, String newSerialNumber, int lostMonthlyPassFees,
                                                             int paymentMode) {
        List<SimplyParkBookingTbl> bookingTblList = SimplyParkBookingTbl.find(
                SimplyParkBookingTbl.class, "m_booking_id = ? AND m_encoded_pass_code = ?",
                bookingId, newEncodedPassCode);

        SimplyParkBookingTbl booking;

        if(bookingTblList.isEmpty()){
            return;
        }else{
            booking = bookingTblList.get(0);
        }

        LostPassTbl lostPass = new LostPassTbl();
        lostPass.setOperatorId(operatorId);
        lostPass.setBookingId(bookingId);
        lostPass.setOldEncodedPassCode(oldEncodedPassCode);
        lostPass.setNewEncodedPassCode(newEncodedPassCode);
        lostPass.setNewPassIssueDate(newPassIssueDate);
        lostPass.setOldPassIssueDate(oldPassIssueDate);
        lostPass.setOldSerialNumber(oldSerialNumber);
        lostPass.setNewSerialNumber(newSerialNumber);
        lostPass.setPaymentMode(paymentMode);
        lostPass.setLostPassIssueFees(lostMonthlyPassFees);
        lostPass.save();

        booking.setEncodedPassCode(newEncodedPassCode);
        booking.save();


    }

}


