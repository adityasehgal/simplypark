package in.simplypark.spot_app.utils;

/**
 * Created by root on 11/10/16.
 */
public abstract class Formatter {
    public abstract String font_Emphasized_On();
    public abstract String font_Emphasized_Off();
    public abstract String font_Double_Height_Width_On();
    public abstract String font_Double_Height_Width_Off();
    public abstract String font_Double_Height_On();
    public abstract String font_Double_Height_Off();

    public abstract String horizontal_Tab();
    public abstract String barcode_Code_128_Alpha_Numerics(String value);

    public abstract String font_Courier_10(String value);
    public abstract String font_Courier_19(String value);
    public abstract String font_Courier_20(String value);
    public abstract String font_Courier_24(String value);
    public abstract String font_Courier_25(String value);
    public abstract String font_Courier_27(String value);
    public abstract String font_Courier_29(String value);
    public abstract String font_Courier_32(String value);
    public abstract String font_Courier_34(String value);
    public abstract String font_Courier_38(String value);
    public abstract String font_Courier_42(String value);
    public abstract String font_Courier_48(String value);
    public abstract String font_SansSerif_8(String value);
    public abstract String font_SansSerif_32(String value);
    public abstract String font_SansSerif_34(String value);
    public abstract String font_SansSerif_38(String value);
    public abstract String carriage_Return();


}





