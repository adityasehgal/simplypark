package in.simplypark.spot_app.utils;

import java.util.UUID;

import in.simplypark.spot_app.printer.PrinterHandle;

/**
 * Created by root on 07/10/16.
 */
public class AnalogicsPrinterHandle extends PrinterHandle {
    private UUID mUUID;


    public UUID getUUID(){
        return mUUID;
    }
    public AnalogicsPrinterHandle() {
        mUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    }



}
