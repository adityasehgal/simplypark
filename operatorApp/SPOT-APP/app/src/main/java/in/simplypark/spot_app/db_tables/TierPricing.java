package in.simplypark.spot_app.db_tables;

import com.orm.SugarRecord;

/**
 * Created by aditya on 04/05/16.
 */
public class TierPricing extends SugarRecord {
    int    hours;
    double price;
    int    spaceId;

    public TierPricing(){
        hours = 0;
        price = 0.00;
        spaceId = 0;
    }



    public void setTierPricing(int spaceId,int hours, double price){
        this.spaceId = spaceId;
        this.hours   = hours;
        this.price   = price;

    }


}
