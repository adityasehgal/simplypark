package in.simplypark.spot_app.utils;

import in.simplypark.spot_app.listelements.Element;

/**
 * Created by aditya on 24/05/16.
 * Format for the ticket is Header, Body, Footer, Sponsered Message
 */
public class Format {


    Element[][] mHeader;
    Element[][] mBody;
    Element[][] mFooter;
    Element[][] mSponseredMsg;


    public void setHeader(Element[][] header){
        mHeader = header;
    }

    public Element[][] getHeader(){return mHeader;}

   public void setBody(Element[][] body){
        mBody = body;
    }

    public Element[][] getBody(){return mBody;}

    public void setFooter(Element[][] footer){
        mFooter = footer;
    }

    public Element[][] getFooter(){return mFooter;}
}



