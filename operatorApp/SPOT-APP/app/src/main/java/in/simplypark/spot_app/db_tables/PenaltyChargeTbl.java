package in.simplypark.spot_app.db_tables;

import com.orm.SugarRecord;

import in.simplypark.spot_app.config.Constants;

/**
 * Created by root on 07/12/16.
 */

public class PenaltyChargeTbl extends SugarRecord {
    private int mChargeCondition;
    private int mHourlyCharge;
    private int mVehicleType;

    public PenaltyChargeTbl(){
        mChargeCondition = Constants.PENALTLY_CHARGE_CONDITION_NIGHT_STAY;
        mHourlyCharge = 0;
        mVehicleType = Constants.VEHICLE_TYPE_INVALID;
    }

    public int getChargeCondition() {
        return mChargeCondition;
    }

    public void setChargeCondition(int mChargeCondition) {
        this.mChargeCondition = mChargeCondition;
    }

    public int getHourlyCharge() {
        return mHourlyCharge;
    }

    public void setHourlyCharge(int mHourlyCharge) {
        this.mHourlyCharge = mHourlyCharge;
    }

    public int getVehicleType() {
        return mVehicleType;
    }

    public void setVehicleType(int mVehicleType) {
        this.mVehicleType = mVehicleType;
    }
}
