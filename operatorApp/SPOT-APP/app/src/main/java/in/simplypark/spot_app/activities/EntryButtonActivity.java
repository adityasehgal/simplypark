package in.simplypark.spot_app.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hirondelle.date4j.DateTime;
import in.simplypark.spot_app.R;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.db_tables.ReusableTokensTbl;
import in.simplypark.spot_app.db_tables.SimplyParkTransactionTbl;
import in.simplypark.spot_app.db_tables.TransactionTbl;
import in.simplypark.spot_app.printer.PrinterService;
import in.simplypark.spot_app.services.ServerUpdationIntentService;
import in.simplypark.spot_app.utils.Cache;
import in.simplypark.spot_app.utils.MiscUtils;
import in.simplypark.spot_app.utils.ParkingId;

public class EntryButtonActivity extends Activity {

    private logEntryTask   mLogEntryTask;
    private scanQrCodeTask mScanQrCodeTask;


    IntentFilter     mStatusIntentFilter;
    ResponseReceiver mResponseReceiver;

    private View mProgressView;
    private View mLogEntryFormiew;
    private EditText mCarRegView;
    private Button mSimplyParkScanButton;
    private Button mPrintButton;
    private Button mPrintTokenScanButton;
    private boolean mIsReusableTokenUsed = false;

    private String mVehicleReg;
    private int    mVehicleType;
    private int    mDefaultCheckedVehicleType = Constants.VEHICLE_TYPE_CAR;
//    private int    mDefaultUnCheckedVehicleType = Constants.VEHICLE_TYPE_BIKE;

    private InputMethodManager imm;

    private boolean mIsSpaceBikeOnly;
    private String mReusablePrintToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        if (android.os.Build.VERSION.SDK_INT>=11){
            setFinishOnTouchOutside(false);
        }
        setContentView(R.layout.activity_entry_button);

        mLogEntryFormiew = findViewById(R.id.log_entry_view);
        mProgressView = findViewById(R.id.log_entry_progress);
        mCarRegView   = (EditText)findViewById(R.id.car_reg);
        mCarRegView.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);

        mPrintButton = (Button)findViewById(R.id.print_ticket);
        mPrintTokenScanButton = (Button)findViewById(R.id.scan_token_pass);



//        mCarRegView.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        mSimplyParkScanButton = (Button)findViewById(R.id.scan_simplypark_pass);
        showProgress(false);

        Context context = getApplicationContext();

        mIsReusableTokenUsed = (Boolean)Cache.getFromCache(context,
                Constants.CACHE_PREFIX_IS_REUSABLE_TOKEN_USED,Constants.TYPE_BOOL);

        if(mIsReusableTokenUsed){
            mPrintButton.setVisibility(View.GONE);
            mSimplyParkScanButton.setVisibility(View.GONE);
            mPrintTokenScanButton.setVisibility(View.VISIBLE);
        }

        if((Boolean) Cache.getFromCache(context,
                Constants.CACHE_PREFIX_IS_PARK_AND_RIDE_AVAIL_QUERY,
                Constants.TYPE_BOOL) == true){

            Switch parkAndRide = (Switch)findViewById(R.id.checkbox_park_and_ride);
            parkAndRide.setVisibility(View.VISIBLE);
        }

        if((Boolean)Cache.getFromCache(context,
                Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY,
                Constants.TYPE_BOOL)){
            mSimplyParkScanButton.setVisibility(View.GONE);
        }

        Spinner vehicleType = (Spinner) findViewById(R.id.vehicle_type);



        if((Boolean)Cache.getFromCache(context,
                Constants.CACHE_PREFIX_IS_CAR_PRICING_AVAILABLE_QUERY,Constants.TYPE_BOOL)){
            List<String> existingItems = retrieveAllItems(vehicleType);
            existingItems.add(getString(R.string.car));
            addItemsToSpinner(existingItems,vehicleType);
            vehicleType.setSelection(getIndex(vehicleType,getString(R.string.car)));
            mDefaultCheckedVehicleType = Constants.VEHICLE_TYPE_CAR;
        }

        if((Boolean)Cache.getFromCache(context,
                Constants.CACHE_PREFIX_IS_SPACE_BIKE_ONLY_QUERY,
                        Constants.TYPE_BOOL)){
            List<String> existingItems = retrieveAllItems(vehicleType);
            existingItems.add(getString(R.string.bike));
            addItemsToSpinner(existingItems,vehicleType);
            vehicleType.setSelection(getIndex(vehicleType,getString(R.string.bike)));
            mDefaultCheckedVehicleType = Constants.VEHICLE_TYPE_BIKE;

            mIsSpaceBikeOnly = true;

        }else if((Boolean)Cache.getFromCache(context,
                              Constants.CACHE_PREFIX_IS_BIKE_PRICING_AVAILABLE_QUERY,
                              Constants.TYPE_BOOL)){
            List<String> existingItems = retrieveAllItems(vehicleType);
            existingItems.add(getString(R.string.bike));
            addItemsToSpinner(existingItems,vehicleType);
        }

        if((Boolean)Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_IS_MINIBUS_PRICING_AVAILABLE_QUERY,
                Constants.TYPE_BOOL)){
            List<String> existingItems = retrieveAllItems(vehicleType);
            existingItems.add(getString(R.string.minibus));
            addItemsToSpinner(existingItems,vehicleType);
        }

        if((Boolean)Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_IS_BUS_PRICING_AVAILABLE_QUERY,
                Constants.TYPE_BOOL)){
            List<String> existingItems = retrieveAllItems(vehicleType);
            existingItems.add(getString(R.string.bus));
            addItemsToSpinner(existingItems,vehicleType);
        }

        /*
        * This is added to support the requirement of changing the default vehicle type for different sites...
        *
        */
        String vehicleTypeStr = (String)Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_DEFAULT_VEHICLE_TYPE,
                Constants.TYPE_STRING);
        if(vehicleTypeStr!=null){
            vehicleType.setSelection(getIndex(vehicleType,vehicleTypeStr));
        }

        vehicleType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                // On selecting a spinner item
                String vehicle_type_str = adapter.getItemAtPosition(position).toString();
                mDefaultCheckedVehicleType = Constants.VEHICLE_TYPE_MAP.get(vehicle_type_str);
                // Showing selected spinner item
//                Toast.makeText(getApplicationContext(),
//                        "Selected Vehicle : " + vehicle_type_str+ ", mDefaultCheckedVehicleType : "+mDefaultCheckedVehicleType, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        mCarRegView.requestFocus();
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.showSoftInputFromInputMethod(mCarRegView.getWindowToken(), InputMethodManager.SHOW_IMPLICIT);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    @Override
    public void onPause(){
        super.onPause();
        View v = findViewById(R.id.car_reg);
        InputMethodManager imm = (InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
    // add items into spinner dynamically
    public void addItemsToSpinner(List<String> itemList, Spinner spinner) {
              ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, itemList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }
    // get the position of an item by value
    private int getIndex(Spinner spinner, String myString)
    {
        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                index = i;
                break;
            }
        }
        return index;
    }

    public List<String> retrieveAllItems(Spinner spinner) {
        Adapter adapter = spinner.getAdapter();
        int n = adapter.getCount();
        List<String> items = new ArrayList<String>(n);
        for (int i = 0; i < n; i++) {
            String item = (String) adapter.getItem(i);
            items.add(item);
        }
        return items;
    }
    //Cancel Button
    public void handleCancelButton(View view){
        finish();
    }

    //print Button
        public void handlePrintButton(View view) {



            EditText carRegView = (EditText) findViewById(R.id.car_reg);
            EditText prefixRegView = (EditText) findViewById(R.id.car_reg_prefix);
            Switch isParkAndRide = (Switch) findViewById(R.id.checkbox_park_and_ride);
            Spinner vehicleType = (Spinner) findViewById(R.id.vehicle_type);

            // Reset errors.
            carRegView.setError(null);

            if(!mReusablePrintToken.isEmpty()){
                //check if the token is valid
                int retValue = ReusableTokensTbl.isTokenValid(mReusablePrintToken);

                switch(retValue){
                    case Constants.TOKEN_INVALID:{
                        carRegView.setError("Invalid Print Token");
                        carRegView.requestFocus();
                        mReusablePrintToken = "";
                        return;
                    }

                    case Constants.TOKEN_BUSY:{
                        carRegView.setError("Token is Already in Use");
                        carRegView.requestFocus();
                        mReusablePrintToken = "";
                        return;
                    }
                }
            }




            if (carRegView.length() == 0) {
                carRegView.setError("This field is required");
                carRegView.requestFocus();
                return;
            } else if (carRegView.length() < Constants.REG_MIN_LEN) {
                carRegView.setError("Invalid Vehicle number.");
                carRegView.requestFocus();
                return;


            }


            // Store values at the time of the login attempt.
            String car_reg = "";
            if (prefixRegView.length() == 0) {
                car_reg = carRegView.getText().toString().toUpperCase();
            } else {
                car_reg = prefixRegView.getText().toString().toUpperCase() +
                        carRegView.getText().toString();
            }

            //Check for a valid car registeration
            if (TextUtils.isEmpty(car_reg)) {
                carRegView.setError("This field is required");
                carRegView.requestFocus();
                return;
            }

            // The filter's action is SERVER_UPDATION_BROADCAST_RESULTS ACTION
            mStatusIntentFilter = new IntentFilter(
                    Constants.PRINT_ENTRY_RECEIPT_BROADCAST_RESULTS_ACTION);

            mResponseReceiver = new ResponseReceiver();

            LocalBroadcastManager.getInstance(this).registerReceiver(
                    mResponseReceiver,
                    mStatusIntentFilter);


            //save the transaction to db
            TransactionTbl transaction = new TransactionTbl();
            Date entryTimeObj = new Date();

            byte[] parkingIdInBytes = ParkingId.generateParkingId((int) Cache.getFromCache(
                    getApplicationContext(),
                    Constants.CACHE_PREFIX_OPERATOR_QUERY,
                    Constants.TYPE_INT));

            String asciiParkingId;
            try {
                asciiParkingId = new String(parkingIdInBytes, "ASCII");
            } catch (java.io.UnsupportedEncodingException ie) {
                Log.d("Parking", "Could not Encode the parking String Correctly");
                carRegView.setError("Unknown Error. Please contact SimplyPark.in");
                carRegView.requestFocus();
                return;
            }

            transaction.setParkingId(asciiParkingId);
            transaction.setCarRegNumber(car_reg);
            transaction.setEntryTime(entryTimeObj.getTime());
            transaction.setIsParkAndRide(isParkAndRide.isChecked());
            transaction.setToken(mReusablePrintToken);


            boolean isParkingPrepaid = (Boolean) Cache.getFromCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY,
                    Constants.TYPE_BOOL);


            if (vehicleType.isSelected()) {
                transaction.setVehicleType(mDefaultCheckedVehicleType);
            } else {
                transaction.setVehicleType(mDefaultCheckedVehicleType);
            }


            if (isParkingPrepaid) {

                switch (transaction.getVehicleType()) {
                    case Constants.VEHICLE_TYPE_CAR: {
                        int parkingFees = (Integer) Cache.getFromCache(getApplicationContext(),
                                Constants.CACHE_PREFIX_CAR_PREPAID_FEES_QUERY,
                                Constants.TYPE_INT);

                        transaction.setParkingFees(parkingFees);
                    }
                    break;

                    case Constants.VEHICLE_TYPE_BIKE: {
                        int parkingFees = (Integer) Cache.getFromCache(getApplicationContext(),
                                Constants.CACHE_PREFIX_BIKE_PREPAID_FEES_QUERY,
                                Constants.TYPE_INT);

                        transaction.setParkingFees(parkingFees);

                    }
                    break;
                    case Constants.VEHICLE_TYPE_MINIBUS: {
                        int parkingFees = (Integer) Cache.getFromCache(getApplicationContext(),
                                Constants.CACHE_PREFIX_MINIBUS_PREPAID_FEES_QUERY,
                                Constants.TYPE_INT);

                        transaction.setParkingFees(parkingFees);

                    }
                    break;
                    case Constants.VEHICLE_TYPE_BUS: {
                        int parkingFees = (Integer) Cache.getFromCache(getApplicationContext(),
                                Constants.CACHE_PREFIX_BUS_PREPAID_FEES_QUERY,
                                Constants.TYPE_INT);

                        transaction.setParkingFees(parkingFees);

                    }
                    break;
                }

                transaction.setParkingStatus(Constants.PARKING_STATUS_EXITED);
                //transaction.setParkingStatus(Constants.PARKING_STATUS_EXIT_NOT_RECORDED);
                transaction.setExitTime(transaction.getEntryTime());

            } else {

                transaction.setParkingStatus(Constants.PARKING_STATUS_ENTERED);
            }


            //this tasks writes to db in background, and then starts
            //intent services for receipt printing and server updation
            mLogEntryTask = new logEntryTask(transaction, parkingIdInBytes);
            mLogEntryTask.execute((Void) null);
            showProgress(true);

        }



    public void handleScanSimplyParkButton(View view) {
        EditText carRegView = (EditText) findViewById(R.id.car_reg);
        EditText prefixRegView = (EditText) findViewById(R.id.car_reg_prefix);
        Switch isParkAndRide = (Switch) findViewById(R.id.checkbox_park_and_ride);
        Spinner vehicleType = (Spinner) findViewById(R.id.vehicle_type);

        if (vehicleType.isSelected()) {
            mVehicleType = mDefaultCheckedVehicleType;
        } else {
            mVehicleType = mDefaultCheckedVehicleType;

        }


        //Scan pass button do not require the car number to be entered. A passholder will
        //now scan his pass at entry and scan his pass at exit. Car entry will be required for
        // those cases where number of slots > 0
        /*
        // Reset errors.
        carRegView.setError(null);


        if(view.getId() == R.id.scan_token_pass && carRegView.length() == 0){
            carRegView.setError("This field is required");
            carRegView.requestFocus();
            return;
        }
        */

        int length = carRegView.length();
        if (length > 0 && length < Constants.REG_MIN_LEN) {
            carRegView.setError("Invalid Vehicle number.");
            carRegView.requestFocus();
            return;
        }


        mVehicleReg = "";
        if (length > 0){
            if (prefixRegView.length() == 0) {
                mVehicleReg = carRegView.getText().toString().toUpperCase();
            } else {
                mVehicleReg = prefixRegView.getText().toString().toUpperCase() +
                        carRegView.getText().toString();
            }
        }

        /*
        //Check for a valid car registeration
        if (TextUtils.isEmpty(mVehicleReg)) {
            carRegView.setError("This field is required");
            carRegView.requestFocus();
            return;
        }
        */


        //this tasks writes to db in background, and then starts
        //intent services for receipt printing and server updation
        mScanQrCodeTask = new scanQrCodeTask();
        mScanQrCodeTask.execute((Void) null);
        showProgress(true);


    }


    @Override
    /**
     * Reads data scanned by user and returned by QR Droid
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (Constants.SIMPLYPARK_BOOKING_SCANNING_REQUEST_CODE == requestCode &&
                null != data && data.getExtras() != null) {
            //Read result from QR Droid (it's stored in la.droid.qr.result)
            String result = data.getExtras().getString(Constants.SIMPLYPARK_BOOKING_SCANNING_RESULT);

            if (resultCode != RESULT_OK || null == result || result.length() == 0) {
                Log.d("QRCODE RESULTS", "failed");
                MiscUtils.displayNotification(getApplicationContext(),
                        "Failed to Scan Monthly Pass",
                        false);

                return;

            }
            //Just set result to EditText to be able to view it
            Log.d("QRCODE RESULTS", "result");

            if(mIsReusableTokenUsed && result.startsWith(Constants.REUSABLE_TOKEN_PREFIX)){
                mReusablePrintToken = result;
                handlePrintButton(null);

            }else {

                if (mVehicleType == Constants.VEHICLE_TYPE_BIKE) {
                    mCarRegView.setError("Bikes are NOT allowed to be parked with this Pass.");
                    mCarRegView.requestFocus();
                }else {

                    Intent intent = new Intent(this, SimplyParkButtonActivity.class);
                    intent.putExtra(Constants.SIMPLYPARK_BOOKING_SCANNING_RESULT, result);
                    intent.putExtra(Constants.BUNDLE_PARAM_CAR_REG, mVehicleReg);
                    intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, mVehicleType);
                    startActivityForResult(intent, Constants.SP_ENTRY_REQUEST_CODE);
                }
            }

        } else if (Constants.SP_ENTRY_REQUEST_CODE == requestCode) {

                if(resultCode == RESULT_OK) {
                    Intent intent = EntryButtonActivity.this.getIntent();
                    EntryButtonActivity.this.setResult(resultCode, intent);
                    finish();
                }else{

                }

        }
    }


    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLogEntryFormiew.setVisibility(show ? View.GONE : View.VISIBLE);
            mLogEntryFormiew.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLogEntryFormiew.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLogEntryFormiew.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous saveToTransaction task used to authenticate
     * the user.
     */
    public class logEntryTask extends AsyncTask<Void, Void, Boolean> {

        TransactionTbl mDbObj;
        byte[]         mParkingIdInBytes;
        String         mFailureCause;

        logEntryTask(TransactionTbl objToSave,byte[] parkingIdInBytes){
           mDbObj = objToSave;
           mParkingIdInBytes = parkingIdInBytes;
           mFailureCause = new String();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            String carReg = mDbObj.getCarRegNumber();
            List<TransactionTbl> transLst = TransactionTbl.find(TransactionTbl.class,
                    "m_car_reg_number = ? and m_status = ?",
                    carReg,
                    Integer.toString(Constants.PARKING_STATUS_ENTERED));
            List<SimplyParkTransactionTbl> transactionList =
                    SimplyParkTransactionTbl.find(SimplyParkTransactionTbl.class,
                            "m_vehicle_reg = ? AND m_status = ?",
                            carReg,Integer.toString(Constants.PARKING_STATUS_ENTERED));

            if(transLst.isEmpty() && transactionList.isEmpty()) {
                mDbObj.save();
                String token = mDbObj.getToken();
                if(!token.isEmpty()){
                    ReusableTokensTbl.markTokenAsBusy(token);
                }
                return true;
            }else{
                mFailureCause = "Vehicle("+carReg+") is already inside. Kindly re-check" +
                                " vehicle number";
                return false;
            }

        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mLogEntryTask = null;

            int resCode = RESULT_OK;

            if (success) {
                Context context = getApplicationContext();
                if(mDbObj.getParkingStatus() == Constants.PARKING_STATUS_EXITED){
                    MiscUtils.incrementVehiclesParkedCount(context,mDbObj.getVehicleType());

                }else {
                    MiscUtils.incrementOccupancyCount(context, mDbObj.getVehicleType());
                }



                //print parking receipt
                long entryTime   = mDbObj.getEntryTime();
                String regNumber = mDbObj.getCarRegNumber();
                String parkingId = mDbObj.getParkingId();
                boolean isParkAndRide = mDbObj.getIsParkAndRide();
                DateTime dateTime = DateTime.forInstant(entryTime, Constants.TIME_ZONE);
                String entryTimeStr =  dateTime.format("DD/MM/YY hh:mm");
                int    vehicleType  = mDbObj.getVehicleType();

                if(mDbObj.getParkingStatus() == Constants.PARKING_STATUS_EXITED){ //prepaid parking
                    Integer fees = (int)mDbObj.getParkingFees();
                    PrinterService.startActionPrintExitTicket(context,
                            entryTimeStr, "",Integer.toString(fees),mDbObj.getNightChargeDescription(),
                            mDbObj.getCarRegNumber(),
                            "",vehicleType);

                    //update server
                    ServerUpdationIntentService.startActionLogExit(context,parkingId,
                                                        entryTime,entryTime,
                                                        mDbObj.getParkingFees(),vehicleType,
                                                        regNumber,mDbObj.getToken());
                }else {

                    /*PrinterService.startActionPrintEntryTicket(context,
                            entryTimeStr, vehicleType+parkingId + regNumber, isParkAndRide, regNumber,
                            vehicleType);*/

                    if(mReusablePrintToken.isEmpty()) {
                        PrinterService.startActionPrintEntryTicket(context,
                                entryTimeStr, vehicleType + parkingId, isParkAndRide, regNumber,
                                vehicleType);
                    }


                    //update server
                    ServerUpdationIntentService.startActionLogEntry(context,
                            entryTime,
                            regNumber,
                            parkingId,
                            isParkAndRide, vehicleType,mDbObj.getToken());
                }

                Intent intent = EntryButtonActivity.this.getIntent();
                EntryButtonActivity.this.setResult(resCode,intent);
                finish();

            } else {
                showProgress(false);
                mCarRegView.setError(mFailureCause);
                mCarRegView.requestFocus();
                resCode = RESULT_CANCELED;
            }



        }

        @Override
        protected void onCancelled() {
            mLogEntryTask = null;

        }


    }

    /**
     * Represents an asynchronous saveToTransaction task used to authenticate
     * the user.
     */
    public class scanQrCodeTask extends AsyncTask<Void, Void, Boolean> {


        String         mFailureCause;

        scanQrCodeTask(){

        }

        @Override
        protected Boolean doInBackground(Void... params) {

            if(!mVehicleReg.isEmpty()) {

                if (mVehicleType == Constants.VEHICLE_TYPE_BIKE) {
                    if(!mIsReusableTokenUsed) {
                        mFailureCause = "Bikes are NOT allowed to be parked with this Pass.";
                        mVehicleType = 0;
                        return false;
                    }
                }
                List<SimplyParkTransactionTbl> transactionList =
                        SimplyParkTransactionTbl.find(SimplyParkTransactionTbl.class,
                                "m_vehicle_reg = ? AND m_status = ?",
                                mVehicleReg, Integer.toString(Constants.PARKING_STATUS_ENTERED));

                List<TransactionTbl> offlineTransactionList = TransactionTbl.find(TransactionTbl.class,
                        "m_car_reg_number = ? and m_status = ?",
                        mVehicleReg,
                        Integer.toString(Constants.PARKING_STATUS_ENTERED));

                if (offlineTransactionList.isEmpty() && transactionList.isEmpty()) {
                    return true;
                } else {
                    mFailureCause = "Vehicle(" + mVehicleReg + ") is already inside. Kindly re-check" +
                            " vehicle number";
                    mVehicleReg = "";
                    mVehicleType = Constants.VEHICLE_TYPE_CAR;
                    return false;
                }
            }else{
                return true; //scan the pass and if the number of slots > 1, come back for car reg
                             //number if not entered
            }





        }

        @Override
        protected void onPostExecute(final Boolean success) {

            showProgress(false);

            if(success){
                //Create a new Intent to scan QR Code
                Intent scanActivity = new Intent(Constants.SIMPLYPARK_BOOKING_QR_CODE_SCAN);

                try {
                    startActivityForResult(scanActivity,
                                           Constants.SIMPLYPARK_BOOKING_SCANNING_REQUEST_CODE);

                } catch (ActivityNotFoundException activity) {
                    String errorMsg = "Appropriate App(QrCode Private) is NOT installed";
                    Log.e("QrCodeScanning", errorMsg);
                    Toast toast = Toast.makeText(getApplicationContext(),
                                                 errorMsg,
                                                 Toast.LENGTH_SHORT);
                    toast.show();
                }


            } else {
                showProgress(false);
                mCarRegView.setError(mFailureCause);
                mCarRegView.requestFocus();


            }



        }

        @Override
        protected void onCancelled() {
            mScanQrCodeTask = null;

        }


    }



    /**********************************************************************************************
     *
     *    Sub Class to Handle Results of the background activites
     *
     *********************************************************************************************/

    // Broadcast receiver for receiving status updates from the IntentService
    private class ResponseReceiver extends BroadcastReceiver {
        // Called when the BroadcastReceiver gets an Intent it's registered to receive

        public void onReceive(Context context, Intent intent) {

            // Enter relevant functionality for when the last widget is recieved
            final String action = intent.getAction();

            Log.d("EntryButton-INSIDE", action);
            if (action.equals(Constants.PRINT_ENTRY_RECEIPT_BROADCAST_RESULTS_ACTION))
            {
                showProgress(false);
            }

        }
    }

}
