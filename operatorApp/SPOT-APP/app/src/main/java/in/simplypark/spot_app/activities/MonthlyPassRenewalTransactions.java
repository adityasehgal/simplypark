package in.simplypark.spot_app.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.R;
import in.simplypark.spot_app.adapters.CustomRenewalTransactionsAdapter;
import in.simplypark.spot_app.db_tables.SimplyParkBookingTbl;

public class MonthlyPassRenewalTransactions extends AppCompatActivity {

    private CustomRenewalTransactionsAdapter mCustomListAdapter;
    private ListView mRenewalTransactionsView;
    private ArrayList<SimplyParkBookingTbl> mElements;
    private View mProgressView;
    private GetRenewalsTask mRenewalTransactionsTask;
    private View mParentView;
    private ArrayList<SimplyParkBookingTbl> mTransactionsList;
    private TextView mCustomerInfoView;

    private EditText mSearchView;

    String mParentBookingId = "";
    String mCustomerName = "";
    String mVehicleReg = "";
    int   mVehicleType = Constants.VEHICLE_TYPE_CAR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setTitle("Passes Issued");
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_monthly_pass_renewal_transactions);
        Toolbar toolbar = (Toolbar) findViewById(R.id.amp_toolbar);
        setSupportActionBar(toolbar);


        Bundle passedParameters = getIntent().getExtras();

        if(!passedParameters.isEmpty()) {
            mCustomerName = passedParameters.getString(Constants.BUNDLE_PARAM_MONTHLY_PASS_ISSUED_TO);
            mVehicleReg = passedParameters.getString(Constants.BUNDLE_PARAM_MONTHLY_PASS_VEHICLE_REG);
            mVehicleType =
                    passedParameters.getInt(Constants.BUNDLE_PARAM_MONTHLY_PASS_VEHICLE_TYPE,
                                            Constants.VEHICLE_TYPE_CAR);

            mParentBookingId = passedParameters.getString(Constants.BUNDLE_PARAM_MONTHLY_PASS_BOOKING_ID);


        }

        mElements = new ArrayList<SimplyParkBookingTbl>();


        mParentView = findViewById(R.id.crt_transactions_view_id);
        mRenewalTransactionsView = (ListView) findViewById(R.id.crt_transactions_list_id);
        mCustomListAdapter = new CustomRenewalTransactionsAdapter(MonthlyPassRenewalTransactions.this,
                mElements);
        mRenewalTransactionsView.setAdapter(mCustomListAdapter);
        mProgressView = findViewById(R.id.crt_progress_id);
        mCustomerInfoView = (TextView)findViewById(R.id.crt_info_id);
        String customerInfo = mCustomerName + "\n" + "Vehicle: " + mVehicleReg;
        if(mVehicleType == Constants.VEHICLE_TYPE_CAR){
            customerInfo += " (Car)";
        }else if(mVehicleType == Constants.VEHICLE_TYPE_BIKE){
            customerInfo += " (Bike)";
        }else if(mVehicleType == Constants.VEHICLE_TYPE_MINIBUS){
            customerInfo += " (MiniBus)";
        }else if(mVehicleType == Constants.VEHICLE_TYPE_BUS){
            customerInfo += " (Bus)";
        }

        mCustomerInfoView.setText(customerInfo);


        mSearchView = (EditText) findViewById(R.id.crt_search_id);

        showProgress(true);

        mRenewalTransactionsTask = new GetRenewalsTask();
        mRenewalTransactionsTask.execute((Void) null);

        mSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mCustomListAdapter.getFilter().filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mParentView.setVisibility(show ? View.GONE : View.VISIBLE);
            mParentView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mParentView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mParentView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    /**
     * Represents an asynchronous transaction history task
     * the user.
     */
    public class GetRenewalsTask extends AsyncTask<Void, Void, Boolean> {


        public GetRenewalsTask() {

        }


        @Override
        protected Boolean doInBackground(Void... params) {

           //we store the first booking as a normal booking ID without any parent booking ID.
           //all renewals then have parent booking ID as the booking ID of the first booking.

            mTransactionsList = (ArrayList) SimplyParkBookingTbl.find(SimplyParkBookingTbl.class,
                    "m_booking_id = ? OR m_parent_booking_id = ?",mParentBookingId,mParentBookingId);

            return true;

        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mRenewalTransactionsTask = null;
            showProgress(false);

            if (success) {
                if (mTransactionsList.size() != 0) {
                    mCustomListAdapter.addAll(mTransactionsList);
                } else {
                    Toast.makeText(MonthlyPassRenewalTransactions.this, "No Entries Found",
                                   Toast.LENGTH_LONG).show();

                }

                mCustomListAdapter.notifyDataSetChanged();


            }
        }

        @Override
        protected void onCancelled() {
            mRenewalTransactionsTask = null;
            showProgress(false);
        }

    }
}
