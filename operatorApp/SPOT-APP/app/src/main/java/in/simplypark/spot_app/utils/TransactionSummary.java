package in.simplypark.spot_app.utils;

import in.simplypark.spot_app.config.Constants;

/**
 * Created by aditya on 10/06/16.
 */
public class TransactionSummary{
    private long mDate;
    private int  mEarningsForToday;
    private int  mCarEarnings;
    private int  mBikeEarnings;
    private int  mPassEarnings;
    private int  mLostPassEarnings;

    private long mAverageCarStayTime;
    private long mAverageBikeStayTime;
    private long mAveragePassCarStayTime;
    private long mAveragePassBikeStayTime;
    private int  mNumCarsParked;
    private int  mNumPassCarsParked;
    private int  mNumBikesParked;
    private int  mNumPassBikesParked;

    private int     mIncompleteCarExits;
    private int     mIncompletePassCarExits;
    private int     mIncompleteBikeExits;
    private int     mIncompletePassBikeExits;
    private int     mNumBikesInside;
    private int     mNumPassBikesInside;
    private int     mNumCarsInside;
    private int     mNumPassCarsInside;

    private int mNumLostPassReIssued;
    private int mNumPassesIssued;
    private int mNumCarPassesIssued;
    private int mNumBikePassesIssued;

    private int[] mPerOperatorEarning = new int[Constants.MAX_OPERATORS];
    private int[] mPerOperatorPassesIssuedEarning = new int[Constants.MAX_OPERATORS];
    private int[] mPerOperatorLostPassesEarning = new int[Constants.MAX_OPERATORS];

    private String mPerOperatorEarningPrintString = "";
    private String mPerOperatorPassEarningPrintString = "";
    private String mPerOperatorLostPassEarningPrintString = "";


    /*My variable*/

    /**
     * Created by csharma on 04-12-2016.
     */

        private int  mMiniBusEarnings;
        private int  mBusEarnings;


        private long mAverageMiniBusStayTime;
        private long mAverageBusStayTime;
        private long mAverageMiniBusPassStayTime;
        private long mAverageBusPassStayTime;


        private int  mNumMiniBusParked;
        private int  mNumPassMiniBusParked;
        private int  mNumBusesParked;
        private int  mNumPassBusesParked;


        private int  mIncompleteMiniBusExits;
        private int  mIncompleteMiniBusPassExits;
        private int  mIncompleteBusExits;
        private int  mIncompleteBusPassExits;


        private int  mNumMiniBusesInside;
        private  int mNumMiniBusesPassesInside;


        private int  mNumBusesInside;
        private int  mNumBusesPassesInside;

        private int  nNumMiniBusPassesIssued;
        private int  mBusPassesIssued;

        public int getMiniBusEarnings() {
            return mMiniBusEarnings;
        }

        public void setMiniBusEarnings(int mMiniBusEarnings) {
            this.mMiniBusEarnings = mMiniBusEarnings;
        }

        public int getBusEarnings() {
            return mBusEarnings;
        }

        public void setBusEarnings(int mBusEarnings) {
            this.mBusEarnings = mBusEarnings;
        }

        public long getAverageMiniBusStayTime() {
            return mAverageMiniBusStayTime;
        }

        public void setAverageMiniBusStayTime(long mAverageMiniBusStayTime) {
            this.mAverageMiniBusStayTime = mAverageMiniBusStayTime;
        }

        public long getAverageBusStayTime() {
            return mAverageBusStayTime;
        }

        public void setAverageBusStayTime(long mAverageBusStayTime) {
            this.mAverageBusStayTime = mAverageBusStayTime;
        }

        public long getAveragePassMiniBusStayTime() {
            return mAverageMiniBusPassStayTime;
        }

        public void setAveragePassMiniBusStayTime(long mAverageMiniBusPassStayTime) {
            this.mAverageMiniBusPassStayTime = mAverageMiniBusPassStayTime;
        }

        public long getAveragePassBusStayTime() {
            return mAverageBusPassStayTime;
        }

        public void setAveragePassBusStayTime(long mAverageBusPassStayTime) {
            this.mAverageBusPassStayTime = mAverageBusPassStayTime;
        }

        public int getNumMiniBusParked() {
            return mNumMiniBusParked;
        }

        public void setNumMiniBusParked(int mNumMiniBusParked) {
            this.mNumMiniBusParked = mNumMiniBusParked;
        }

        public int getNumBusesParked() {
            return mNumBusesParked;
        }

        public void setNumBusesParked(int mNumBusesParked) {
            this.mNumBusesParked = mNumBusesParked;
        }

        public int getIncompleteMiniBusExits() {
            return mIncompleteMiniBusExits;
        }

        public void setIncompleteMiniBusExits(int mIncompleteMiniBusExits) {
            this.mIncompleteMiniBusExits = mIncompleteMiniBusExits;
        }

        public int getIncompletePassMiniBusExits() {
            return mIncompleteMiniBusPassExits;
        }

        public void setIncompletePassMiniBusExits(int mIncompleteMiniBusPassExits) {
            this.mIncompleteMiniBusPassExits = mIncompleteMiniBusPassExits;
        }

        public int getIncompleteBusExits() {
            return mIncompleteBusExits;
        }

        public void setIncompleteBusExits(int mIncompleteBusExits) {
            this.mIncompleteBusExits = mIncompleteBusExits;
        }

        public int getIncompletePassBusExits() {
            return mIncompleteBusPassExits;
        }

        public void setIncompletePassBusExits(int mIncompleteBusPassExits) {
            this.mIncompleteBusPassExits = mIncompleteBusPassExits;
        }

        public int getNumMiniBusesInside() {
            return mNumMiniBusesInside;
        }

        public void setNumMiniBusesInside(int mNumMiniBusesInside) {
            this.mNumMiniBusesInside = mNumMiniBusesInside;
        }

        public int getNumPassMiniBusesInside() {
            return mNumMiniBusesPassesInside;
        }

        public void setNumPassMiniBusesInside(int mNumMiniBusesPassesInside) {
            this.mNumMiniBusesPassesInside = mNumMiniBusesPassesInside;
        }

        public int getNumBusesInside() {
            return mNumBusesInside;
        }

        public void setNumBusesInside(int mNumBusesInside) {
            this.mNumBusesInside = mNumBusesInside;
        }

        public int getNumPassBusesInside() {
            return mNumBusesPassesInside;
        }

        public void setNumPassBusesInside(int mNumBusesPassesInside) {
            this.mNumBusesPassesInside = mNumBusesPassesInside;
        }

        public int getNumMiniBusPassesIssued() {
            return nNumMiniBusPassesIssued;
        }


        public void setNumMiniBusPassesIssued(int nNumMiniBusPassesIssued) {
            this.nNumMiniBusPassesIssued = nNumMiniBusPassesIssued;
        }

        public void setNumPassMiniBusParked(int numPassMiniBusParked){
            mNumPassMiniBusParked = numPassMiniBusParked;
        }

        public int getNumPassMiniBusParked(){ return mNumPassMiniBusParked;}

        public void setNumPassBusesParked(int numPassBusesParked){
            mNumPassBusesParked = numPassBusesParked;
        }

        public int getNumPassBusesParked(){ return mNumPassBusesParked;}


    public int getNumBusPassesIssued() {
            return mBusPassesIssued;
        }

        public void setNumBusesPassesIssued(int mBusPassesIssued) {
            this.mBusPassesIssued = mBusPassesIssued;
        }


    /*My variables end*/


    public TransactionSummary(){
        mDate = 0;
        mEarningsForToday = 0;
        mCarEarnings = 0;
        mBikeEarnings = 0;
        mAverageCarStayTime = 0;
        mAverageBikeStayTime = 0;
        mAveragePassCarStayTime = 0;
        mAveragePassBikeStayTime = 0;
        mNumCarsParked = 0;
        mNumPassCarsParked = 0;
        mNumBikesParked = 0;
        mNumPassBikesParked = 0;
        mNumPassMiniBusParked = 0;
        mNumPassBusesParked = 0;

        mIncompleteCarExits = 0;
        mIncompletePassCarExits = 0;
        mIncompleteBikeExits = 0;
        mIncompletePassBikeExits = 0;
        mNumBikesInside = 0;
        mNumPassBikesInside = 0;
        mNumCarsInside = 0;
        mNumPassCarsInside = 0;


    }

    public TransactionSummary(long date,int earnings, int carEarnings,int bikeEarnings,
                       long averageCarStayTime,long averageBikeStayTime,
                       int numCarsParked, int numBikesParked,
                       int numInCompleteCarExits, int numInCompleteBikeExits,
                       int numCarsInside, int numBikesInside){
        mDate                 = date;
        mEarningsForToday     = earnings;
        mCarEarnings          = carEarnings;
        mBikeEarnings         = bikeEarnings;
        mAverageCarStayTime   = averageCarStayTime;
        mAverageBikeStayTime  = averageBikeStayTime;
        mNumCarsParked        = numCarsParked;
        mNumBikesParked       = numBikesParked;
        mIncompleteBikeExits  = numInCompleteBikeExits;
        mIncompleteCarExits   = numInCompleteCarExits;
        mNumBikesInside       = numBikesInside;
        mNumCarsInside        = numCarsInside;


    }

    public void setDate(long date){ mDate = date;}
    public long getDate() { return mDate;}

    //void setEarnings(int earnings){ mEarningsForToday = earnings;}
    public int getEarnings(){ return mBikeEarnings + mCarEarnings + mPassEarnings +mLostPassEarnings; }

    public void setCarEarnings(int carEarnings){ mCarEarnings = carEarnings;}
    public int getCarEarnings(){ return mCarEarnings;}

    public void setBikeEarnings(int bikeEarnings){ mBikeEarnings = bikeEarnings;}
    public int  getBikeEarnings(){ return mBikeEarnings;}

    public void setPassEarnings(int passEarnings) { mPassEarnings = passEarnings;}
    public int  getPassEarnings() { return mPassEarnings;}

    public void setLostPassEarnings(int lostPassEarnings) { mLostPassEarnings = lostPassEarnings;}
    public int  getLostPassEarnings() { return mLostPassEarnings;}


    public void setAverageCarStayTime(long averageStayTime){ mAverageCarStayTime = averageStayTime;}
    public long  getAverageCarStayTime(){ return mAverageCarStayTime;}

    public void setAverageBikeStayTime(long averageStayTime){ mAverageBikeStayTime = averageStayTime;}
    public long  getAverageBikeStayTime(){ return mAverageBikeStayTime;}

    public void setAveragePassCarStayTime(long averagePassStayTime){
         mAveragePassCarStayTime = averagePassStayTime;
    }
    public long  getAveragePassCarStayTime(){ return mAveragePassCarStayTime;}

    public void setAveragePassBikeStayTime(long averagePassStayTime){
        mAverageBikeStayTime = averagePassStayTime;
    }
    public long  getAveragePassBikeStayTime(){ return mAveragePassBikeStayTime;}


    public void setNumCarsParked(int numCarsParked){ mNumCarsParked = numCarsParked; }
    public int getNumCarsParked(){ return mNumCarsParked;}

    public void setNumBikesParked(int numBikesParked) { mNumBikesParked = numBikesParked; }
    public int  getNumBikesParked() { return mNumBikesParked; }

    public int getTotalVehiclesParked() { return (mNumBikesParked + mNumCarsParked + mNumPassBikesParked +
                                           mNumPassCarsParked + mNumMiniBusParked +
                                           mNumBusesParked);}


    public void setIncompleteCarExits(int inCompleteCarExits) {
        mIncompleteCarExits = inCompleteCarExits;
    }

    public int getIncompleteCarExits() { return mIncompleteCarExits;}

    public void setIncompleteBikeExits(int inCompleteBikeExits) {
        mIncompleteBikeExits = inCompleteBikeExits;
    }

    public int getIncompleteBikeExits() { return mIncompleteBikeExits;}

    public int getIncompleteExits() { return mIncompleteBikeExits + mIncompleteCarExits +
                                             mIncompletePassBikeExits + mIncompletePassCarExits;}


    public void setNumBikesInside(int numBikes) { mNumBikesInside = numBikes;}
    public int getNumBikesInside() { return mNumBikesInside;}

    public void setNumCarsInside(int numCars) { mNumCarsInside = numCars; }
    public int  getNumCarsInside(){ return mNumCarsInside; }
    public int  getNumVehiclesInside() { return mNumCarsInside + mNumBikesInside + mNumPassCarsInside +
                                         mNumPassBikesInside; }

    //monthly pass related stats
    public void setNumPassCarsParked(int numPassCarsParked){ mNumPassCarsParked = numPassCarsParked;}
    public int getNumPassCarsParked(){ return mNumPassCarsParked;}

    public void setNumPassBikesParked(int numPassBikesParked){ mNumPassBikesParked = numPassBikesParked;}
    public int getNumPassBikesParked(){ return mNumPassBikesParked;}

    public void setIncompletePassCarExits(int inCompletePassCarExits) {
        mIncompletePassCarExits = inCompletePassCarExits;
    }

    public int getIncompletePassCarExits() { return mIncompletePassCarExits;}

    public void setIncompletePassBikeExits(int inCompletePassBikeExits) {
        mIncompletePassBikeExits = inCompletePassBikeExits;
    }

    public int getIncompletePassBikeExits() { return mIncompletePassBikeExits;}

    public void setNumPassBikesInside(int numPassBikes) { mNumPassBikesInside = numPassBikes;}
    public int getNumPassBikesInside() { return mNumPassBikesInside;}

    public void setNumPassCarsInside(int numPassCars) { mNumPassCarsInside = numPassCars;}
    public int getNumPassCarsInside() { return mNumPassCarsInside;}

    public void setNumPassesIssued(int numPassesIssued) {mNumPassesIssued = numPassesIssued;}
    public int getNumPassesIssued(){ return mNumPassesIssued;}

    public void setNumLostPassesIssued(int numLostPassesReIssued) {mNumLostPassReIssued = numLostPassesReIssued;}
    public int getNumLostPassesIssued(){ return mNumLostPassReIssued;}


    public void setNumCarsPassesIssued(int numCarPassesIssued) { mNumCarPassesIssued = numCarPassesIssued;}
    public int getNumCarPassesIssued(){ return mNumCarPassesIssued;}

    public void setNumBikesPassesIssued(int numBikesPassesIssued) {
        mNumBikePassesIssued = numBikesPassesIssued;
    }
    public int getNumBikesPassesIssued(){ return mNumBikePassesIssued;}

    public void addToOperatorEarnings(int operatorId, int earnings){
        if(operatorId < Constants.MAX_OPERATORS){
            int currentEarnings = mPerOperatorEarning[operatorId];
            mPerOperatorEarning[operatorId] = currentEarnings + earnings;
        }
    }

    public void addToOperatorPassIssuedEarnings(int operatorId, int earnings){
        if(operatorId < Constants.MAX_OPERATORS){
            int currentEarnings = mPerOperatorPassesIssuedEarning[operatorId];
            mPerOperatorPassesIssuedEarning[operatorId] = currentEarnings + earnings;
        }
    }

    public void addToOperatorLostPassEarnings(int operatorId, int earnings){
        if(operatorId < Constants.MAX_OPERATORS){
            int currentEarnings = mPerOperatorLostPassesEarning[operatorId];
            mPerOperatorLostPassesEarning[operatorId] = currentEarnings + earnings;
        }
    }

    public int[] getPerOperatorLostPassesEarning(){return mPerOperatorLostPassesEarning;};

    public int[] getPerOperatorEarning(){
        return mPerOperatorEarning;
    }

    public int[] getPerOperatorPassesIssuedEarning(){
        return mPerOperatorPassesIssuedEarning;
    }

    public String getPerOperatorPassEarningPrintString() {
        return mPerOperatorPassEarningPrintString;
    }

    public void setPerOperatorPassEarningPrintString(String mPerOperatorPassEarningPrintString) {
        this.mPerOperatorPassEarningPrintString = mPerOperatorPassEarningPrintString;
    }
    public void setPerOperatorLostPassEarningPrintString(String mPerOperatorLostPassEarningPrintString) {
        this.mPerOperatorLostPassEarningPrintString = mPerOperatorLostPassEarningPrintString;
    }


    public String getPerOperatorEarningPrintString() {
        return mPerOperatorEarningPrintString;
    }

    public void setPerOperatorEarningPrintString(String mPerOperatorEarningPrintString) {
        this.mPerOperatorEarningPrintString = mPerOperatorEarningPrintString;
    }


}
