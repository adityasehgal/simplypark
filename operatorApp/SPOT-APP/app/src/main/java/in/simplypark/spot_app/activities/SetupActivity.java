package in.simplypark.spot_app.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import in.simplypark.spot_app.Manifest;
import in.simplypark.spot_app.utils.Cache;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.printer.Printer;
import in.simplypark.spot_app.R;

public class SetupActivity extends AppCompatActivity {

    /**
     * Keep track of the setup task to ensure we can cancel it if requested.
     */
    private ConnectTask mConnectTask= null;
    private View mTopLevelView = null;
    private View mProgressView = null;
    private View mPrinterSizeView = null;
    private TextView mMacAddressView = null;
    private String mFailureReason = null;
    private Button   mConnectButton = null;
    private ArrayAdapter<String> mArrayAdapter;
    private BluetoothDiscoveryResult mDiscoveryResult;
    private IntentFilter             mIntentFilter;
    private ListView                 mListDevices;
    private boolean mIsPrinterRescanProcessOngoing = false;
    private EditText mOperatorDescView = null;
    private Spinner mAppType = null;

    private BluetoothAdapter  mBlueToothAdapter = null;

    private Printer mPrinterHandle;

    private String mMacAddress = null;
    private String mPrinterName;

    @Override
    protected void onDestroy(){
        super.onDestroy();



    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle("Setup");

        setContentView(R.layout.activity_setup);

        mTopLevelView = findViewById(R.id.topLevelView);
        mProgressView = findViewById(R.id.setup_progress);
        mMacAddressView = (TextView)findViewById(R.id.mac_address);
        mConnectButton    = (Button)findViewById(R.id.connect_to_printer);
        mPrinterSizeView  = (View)findViewById(R.id.printer_size);

        mListDevices = (ListView) findViewById(R.id.devicesfound);
        mArrayAdapter = new ArrayAdapter<String>(SetupActivity.this,
                                                 android.R.layout.simple_list_item_1);
        mListDevices.setAdapter(mArrayAdapter);
        mArrayAdapter.notifyDataSetChanged();


        mListDevices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                String selection = (String) (mListDevices
                        .getItemAtPosition(position));

                String[] split = selection.split("\n");

                /*mMacAddress = selection.substring(getString(R.string.printer_name).length() + 1,
                        selection.length());*/

                mPrinterName = split[0];
                mMacAddress  = split[1];
                mPrinterHandle = Printer.getPrinter(mPrinterName);

                if (!selection.equalsIgnoreCase(getString(R.string.no_results_found))) {
                    mMacAddressView.setText(selection);
                    mConnectButton.setText(getString(R.string.connect_to_bluetooth_button));
                }
            }

        });
        mAppType = (Spinner) findViewById(R.id.as_app_type);

        mAppType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                // On selecting a spinner item
                String appType = adapter.getItemAtPosition(position).toString();

                /*String[] appTypes = getApplicationContext().getResources().getStringArray(
                                                                          R.array.app_types);*/

                switch(position){
                    case Constants.APP_TYPE_ENTRY_EXIT_ENABLED:
                        Cache.setInCache(getApplicationContext(),Constants.CACHE_PREFIX_APP_TYPE,
                                Constants.APP_TYPE_ENTRY_EXIT_ENABLED,Constants.TYPE_INT);
                        break;
                    case Constants.APP_TYPE_ONLY_ENTRY_ENABLED:
                        Cache.setInCache(getApplicationContext(),Constants.CACHE_PREFIX_APP_TYPE,
                                Constants.APP_TYPE_ONLY_ENTRY_ENABLED,Constants.TYPE_INT);
                        break;
                    case Constants.APP_TYPE_ONLY_EXIT_ENABLED:
                        Cache.setInCache(getApplicationContext(),Constants.CACHE_PREFIX_APP_TYPE,
                                Constants.APP_TYPE_ONLY_EXIT_ENABLED,Constants.TYPE_INT);
                        break;
                }



            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });




        Cache.setInCache(getApplicationContext(),
                Constants.CACHE_PREFIX_PRINTER_SIZE,Constants.PRINTER_SIZE_2_INCH,Constants.TYPE_INT);


        mOperatorDescView = (EditText)findViewById(R.id.operator_desc);
        Bundle passedParams = getIntent().getExtras();

        if(passedParams != null) {
            mIsPrinterRescanProcessOngoing = passedParams.getBoolean(
                    Constants.BUNDLE_PARAM_PRINTER_RESCAN_PROCESS_ONGOING);

        }

        if(mIsPrinterRescanProcessOngoing){
            String operatorDesc = (String)Cache.getFromCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_OPERATOR_DESC_QUERY,Constants.TYPE_STRING);
            mOperatorDescView.setText(operatorDesc);
            mOperatorDescView.setEnabled(false);
        }


    }
    public void onCheckboxClicked(View view){
        //CheckBox check = (CheckBox)view;
        Switch check = (Switch)view;


        if(check.isChecked()){
            //mMacAddressView.setVisibility(View.VISIBLE);
            mConnectButton.setText(R.string.connect);

        }else{
            //mMacAddressView.setVisibility(View.GONE);
            mConnectButton.setText(R.string.dashboard);

        }


    }

    public void onPrinterSizeCheckboxClicked(View view){

    }

    public int setupBluetooth(){

        mBlueToothAdapter = BluetoothAdapter.getDefaultAdapter();

        //check if device supports bluetooth
        if(mBlueToothAdapter == null){
            showSetupError("Device does not support Bluetooth.");
            return Constants.SETUP_WITH_NO_BT_SUPPORT;
        }

        //check if bluetooth is enabled

        if(!mBlueToothAdapter.isEnabled()){
            //blueetooth not enabled. Request user to enable it
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent,Constants.REQUEST_FOR_BT);
            return Constants.REQUEST_FOR_BT;
        }

        return findPrinter();
    }


    public int findPrinter(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);
        }

        if(mBlueToothAdapter.startDiscovery()){
            mIntentFilter = new IntentFilter();
            mIntentFilter.addAction(BluetoothDevice.ACTION_FOUND);
            mIntentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
            mDiscoveryResult = new BluetoothDiscoveryResult();

            showProgress(true);
            // Register the BroadcastReceiver
            registerReceiver(mDiscoveryResult, mIntentFilter);

        }else{
            showSetupError("Bluetooth discovery failed. Please try after sometime");
        }




        return Constants.SETUP_WITH_BT_SUPPORT;
    }

    @Override
    /**
     * Reads data scanned by user and returned by QR Droid
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_FOR_BT) {

            if(resultCode == RESULT_OK){

            }else{
                showSetupError("Bluetooth not enabled. ");
                mConnectButton.setVisibility(View.GONE);
                mConnectButton.setText(R.string.dashboard);
                Switch check = (Switch)findViewById(R.id.checkbox_bt_printer_available);
                check.setChecked(false);
            }
        }
    }


    public void showSetupError(String errorMsg){
        Toast.makeText(getApplicationContext(), errorMsg,
                Toast.LENGTH_LONG).show();

    }



    public void handleConnectOrDashboardButton(View view) {

        Button button = (Button) view;

        String label = button.getText().toString();


        if (!mIsPrinterRescanProcessOngoing){
            String operatorDesc = mOperatorDescView.getText().toString();

        EditText descView = (EditText) findViewById(R.id.operator_desc);

        if (descView.getText().toString().isEmpty()) {
            descView.setError("This field is required");
            descView.requestFocus();
            return;
        }

        Cache.setInCache(getApplicationContext(), Constants.CACHE_PREFIX_OPERATOR_DESC_QUERY,
                operatorDesc, Constants.TYPE_STRING);

        int operatorId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_OPERATOR_QUERY,
                Constants.TYPE_INT));

        Cache.setInCache(getApplicationContext(), Constants.CACHE_PREFIX_OPERATORID_DESC_QUERY + operatorId,
                operatorDesc, Constants.TYPE_STRING);
    }




        if( label.equalsIgnoreCase(getString(R.string.connect))){
                   showProgress(true);

            mConnectButton.setEnabled(false);
            mConnectButton.setText("Connecting....");

            showProgress(false);

            if(mPrinterHandle.connect(mMacAddress)) {

                mPrinterHandle.setState(Constants.PRINTER_STATE_CONNECTED);

                Context context = getApplicationContext();

                Cache.setInCache(context,
                        Constants.CACHE_PREFIX_IS_PRINTER_CONFIGURED,true,Constants.TYPE_BOOL);

                Intent intent = new Intent(SetupActivity.this, PrinterSetup.class);
                //set the macAddress in Cache


                Cache.setInCache(context,
                        Constants.CACHE_PREFIX_BT_MACADDRESS_QUERY,
                        mMacAddress,Constants.TYPE_STRING);

                Bundle b = new Bundle();
                b.putInt(Constants.BUNDLE_PARAM_PRINTER_SIZE,
                        (Integer) Cache.getFromCache(getApplicationContext(),
                                Constants.CACHE_PREFIX_PRINTER_SIZE, Constants.TYPE_INT));
                startActivity(intent);
                finish();
            }else{
                showSetupError("Could not connect. Please try again later");
                mConnectButton.setEnabled(true);
                mConnectButton.setText(getString(R.string.scan));
            }

        }else{ //dashboards
            Switch checkbox = (Switch)findViewById(R.id.checkbox_bt_printer_available);

            Context context = getApplicationContext();

            if(checkbox.isChecked()){
                mArrayAdapter.clear();
                setupBluetooth();
            }else{
                Cache.setInCache(context, Constants.CACHE_PREFIX_IS_PRINTER_CONFIGURED,
                        false, Constants.TYPE_BOOL);

                showProgress(true);

                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);

                finish();
            }


        }



    }

    public void handleListItemClicked(ListView parentView, View view, int position, long id) {


    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.two_inch:
                if (checked){
                    Cache.setInCache(getApplicationContext(),Constants.CACHE_PREFIX_PRINTER_SIZE,
                            Constants.PRINTER_SIZE_2_INCH,Constants.TYPE_INT);
                }

                break;
            case R.id.three_inch:
                if (checked){
                    Cache.setInCache(getApplicationContext(),Constants.CACHE_PREFIX_PRINTER_SIZE,
                            Constants.PRINTER_SIZE_3_INCH,Constants.TYPE_INT);

                }

                break;
        }
    }


    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mTopLevelView.setVisibility(show ? View.GONE : View.VISIBLE);
            mTopLevelView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mTopLevelView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mTopLevelView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    public class ConnectTask extends AsyncTask<Void, Void, Boolean> {

        String mMacAddress;

        ConnectTask(String macAddress) {
            Log.d("SETUP---", "ConnectTask");
            mMacAddress = macAddress;
        }


        @Override
        protected Boolean doInBackground(Void... params) {

            Log.d("SETUP---", "Do in Background");

            if(mPrinterHandle.connect(mMacAddress)) {
                return true;
            }else {
                return false;
            }

        }

        @Override
        protected void onPostExecute(final Boolean success) {
            showProgress(false);

            if(success){
                 Intent intent = new Intent(SetupActivity.this, PrinterSetup.class);
                //pass the payment due
                Bundle b = new Bundle();
                b.putInt(Constants.BUNDLE_PARAM_PRINTER_SIZE,
                        (Integer) Cache.getFromCache(getApplicationContext(),
                                Constants.CACHE_PREFIX_PRINTER_SIZE,Constants.TYPE_INT));
                b.putBoolean(Constants.BUNDLE_PARAM_PRINTER_RESCAN_PROCESS_ONGOING,
                        mIsPrinterRescanProcessOngoing);

                intent.putExtras(b);
                startActivity(intent);
                //showSetupError("Connected");

            }else{
                showSetupError(mFailureReason);
            }

        }
    }

    /**********************************************************************************************
     * Sub Class to Handle Results of the background activites
     *********************************************************************************************/

    // Broadcast receiver for receiving status updates from the IntentService
    private class BluetoothDiscoveryResult extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                String name = device.getName();

                if(name != null && ((name.equalsIgnoreCase(getString(R.string.printer_name)))
                                ||  (name.equalsIgnoreCase(getString(R.string.mate_printer_name)))
                                ||  (name.startsWith(getString(R.string.analogics_printer_prefix)))
                                ||  (name.startsWith(getString(R.string.analogics_printer_prefix_2)))

                )){
                    // Add the name and address to an array adapter to show in a ListView
                    mArrayAdapter.add(name + "\n" + device.getAddress());
                }
            }else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)){
                 showProgress(false);
                 mMacAddressView.setVisibility(View.VISIBLE);
                 mPrinterSizeView.setVisibility(View.VISIBLE);

                 if(mListDevices.getAdapter().getCount() == 0){
                     mArrayAdapter.add(getString(R.string.no_results_found));
                 }

                unregisterReceiver(mDiscoveryResult);


            }
        }

    }

}
