package in.simplypark.spot_app.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import hirondelle.date4j.DateTime;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.db_tables.ReusableTokensTbl;
import in.simplypark.spot_app.printer.PrinterService;
import in.simplypark.spot_app.R;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.db_tables.PendingExits;
import in.simplypark.spot_app.db_tables.SimplyParkBookingTbl;
import in.simplypark.spot_app.db_tables.SimplyParkTransactionTbl;
import in.simplypark.spot_app.db_tables.TransactionTbl;
import in.simplypark.spot_app.printer.PrinterService;
import in.simplypark.spot_app.services.ServerUpdationIntentService;
import in.simplypark.spot_app.utils.Cache;
import in.simplypark.spot_app.utils.MiscUtils;

public class ExitButtonSubActivity extends Activity {

    private double mParkingFees;
    private String mParkingId;
    private long   mEntryTime;
    private long   mExitTime;
    private String mVehicleReg;
    private int    mVehicleType;
    private String mBookingId;
    private boolean mIsSimplyParkTransaction;
    private boolean mIsNightChargeApplied;
    private int     mNumNights;
    private int     mNightCharge;
    private boolean mIsPenaltyChargeApplied = false;
    private boolean mIsExitThroughPass = false;
    private int     mPenaltyCharge = 0;
    private int     mNumPenaltyChargeHours = 0;
    private String mDurationDescription;
    private View   mExitSubEntryMainView;
    private View   mProgressView;
    TransactionTbl mTransactionObj;
    SimplyParkBookingTbl mBooking;
    private String mNightChargeString = "";
    private String mPenaltyChargeString = "";
    private int mLostTicketCharge = 0;

    private Button   mPrintButtonView;
    private boolean mIsReusablePrintTokenUsed = false;

    private boolean mIsNew;

    private logExitTask mLogExitTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        if (android.os.Build.VERSION.SDK_INT >= 11) {
            setFinishOnTouchOutside(false);
        }

        Bundle passedParams = getIntent().getExtras();
        mParkingFees = 0.00;
        mParkingId = "";
        mExitTime = 0;
        mDurationDescription = "";
        mIsSimplyParkTransaction = false;





        if(passedParams != null){

            mIsExitThroughPass = passedParams.getBoolean(Constants.BUNDLE_PARAM_IS_EXIT_THROUGH_PASS,false);
            if(!mIsExitThroughPass) {
                mParkingFees = passedParams.getDouble(Constants.BUNDLE_PARAM_PAYMENT_DUE);
                mParkingId = passedParams.getString(Constants.BUNDLE_PARAM_PARKING_ID);
                mEntryTime = passedParams.getLong(Constants.BUNDLE_PARAM_ENTRY_TIME);
                mExitTime = passedParams.getLong(Constants.BUNDLE_PARAM_EXIT_TIME);
                mVehicleReg  = passedParams.getString(Constants.BUNDLE_PARAM_VEHICLE_REG);
                mVehicleType = passedParams.getInt(Constants.BUNDLE_PARAM_VEHICLE_TYPE);
                mBookingId   = passedParams.getString(Constants.BUNDLE_PARAM_BOOKING_ID);
                mDurationDescription = passedParams.getString(
                        Constants.BUNDLE_PARAM_DURATION_DESCRIPTION);
                mIsNew = passedParams.getBoolean(Constants.BUNDLE_PARAM_IS_NEW_RECORD);
                mIsSimplyParkTransaction = passedParams.getBoolean(
                        Constants.BUNDLE_PARAM_IS_TRANSACTION_SP);
                mIsNightChargeApplied = passedParams.getBoolean(
                        Constants.BUNDLE_PARAM_IS_NIGHT_CHARGE_APPLIED);
                mNumNights = passedParams.getInt(Constants.BUNDLE_PARAM_NUM_NIGHTS);
                mNightCharge = passedParams.getInt(Constants.BUNDLE_PARAM_EXTRA_NIGHT_CHARGE);

                mIsPenaltyChargeApplied = passedParams.getBoolean(
                        Constants.BUNDLE_PARAM_IS_PENALTY_CHARGE_APPLIED);
                mPenaltyCharge = passedParams.getInt(Constants.BUNDLE_PARAM_PENALTY_CHARGE);
                mNumPenaltyChargeHours = passedParams.getInt(Constants.BUNDLE_PARAM_NUM_PENALTY_HOURS);
                mLostTicketCharge = passedParams.getInt(Constants.BUNDLE_PARAM_LOST_TICKET_CHARGE);
            }else{
                mParkingFees = passedParams.getDouble(Constants.BUNDLE_PARAM_PAYMENT_DUE);
                mEntryTime = passedParams.getLong(Constants.BUNDLE_PARAM_ENTRY_TIME);
                mExitTime = passedParams.getLong(Constants.BUNDLE_PARAM_EXIT_TIME);
                mBookingId = passedParams.getString(Constants.BUNDLE_PARAM_BOOKING_ID);
                mVehicleType = passedParams.getInt(Constants.BUNDLE_PARAM_VEHICLE_TYPE);
                mIsPenaltyChargeApplied =passedParams.getBoolean(Constants.BUNDLE_PARAM_IS_PENALTY_CHARGE_APPLIED);
                mPenaltyCharge = passedParams.getInt(Constants.BUNDLE_PARAM_PENALTY_CHARGE);
                mNumPenaltyChargeHours =passedParams.getInt(Constants.BUNDLE_PARAM_NUM_PENALTY_HOURS);
                mIsNew = passedParams.getBoolean(Constants.BUNDLE_PARAM_IS_NEW_RECORD);

            }
        }

        setContentView(R.layout.activity_exit_button_sub);

        mPrintButtonView = (Button)findViewById(R.id.print_receipt);

        if((Boolean)Cache.getFromCache(getApplicationContext(),
                                       Constants.CACHE_PREFIX_IS_REUSABLE_TOKEN_USED,
                                       Constants.TYPE_BOOL)){
            mPrintButtonView.setText(R.string.transaction_details);
            mIsReusablePrintTokenUsed = true;
        }

        mExitSubEntryMainView = (View)findViewById(R.id.exitSubActivityMainView);
        mProgressView  = (View)findViewById(R.id.log_exit_progress);
        RelativeLayout mVisitorDetailsView =(RelativeLayout) findViewById(R.id.VisitorDetailsView);
        TextView paymentTextBox = (TextView)findViewById(R.id.payment_id);
        TextView nightChargesBox = (TextView)findViewById(R.id.night_charge_id);

        if(mIsExitThroughPass) {
        /*Calling the getPassDetails Assync Task for displaying the pass details if any*/
            new getPassDetails(mBookingId,mVisitorDetailsView).execute();
        }
        //paymentTextBox.setTypeface(paymentTextBox.getTypeface(), Typeface.BOLD);
        //DecimalFormat formatter = new DecimalFormat("0.##");
        //formatter.setMinimumFractionDigits(2);
        //paymentTextBox.setText(formatter.format(mParkingFees));

        if(!mIsSimplyParkTransaction) {
            if(((int) Math.ceil(mParkingFees))>0) {
                paymentTextBox.setText(Integer.toString((int) Math.ceil(mParkingFees)));
            }else {
                paymentTextBox.setTextSize(25);
                paymentTextBox.setText(R.string.no_dues_test);
                nightChargesBox.setVisibility(View.GONE);
            }
            if (mIsNightChargeApplied) {
                mNightChargeString = "Includes extra night charges of " + mNightCharge +
                        " for " + mNumNights + " nights";
                nightChargesBox.setText(mNightChargeString);

            }else if(mIsPenaltyChargeApplied || mLostTicketCharge > 0) {
                if(mIsPenaltyChargeApplied ) {
                    mPenaltyChargeString = "Includes penalty charges of Rs." + mPenaltyCharge +
                            " for " + mNumPenaltyChargeHours + " hours";
                }

                if(mLostTicketCharge > 0){
                    mPenaltyChargeString += (mIsPenaltyChargeApplied)?" and ":" ";
                    mPenaltyChargeString += "Lost Ticket charge: Rs. " + mLostTicketCharge;
                }

                nightChargesBox.setText(mPenaltyChargeString);
            }else{
                nightChargesBox.setVisibility(View.GONE);
            }
        }else {
            paymentTextBox.setTextSize(25);
            paymentTextBox.setText("No Payment Due");

            mPrintButtonView.setVisibility(View.GONE);
            nightChargesBox.setVisibility(View.GONE);

        }


    }

    public void handleDoneButton(View view){
        if(!mIsExitThroughPass) {
            mLogExitTask = new logExitTask(mParkingId, mExitTime, mParkingFees, false, mDurationDescription);
            mLogExitTask.execute((Void) null);
        }else{
            new logPassExitTask(mBookingId,mExitTime,mParkingFees,false,mDurationDescription,mVehicleType).execute();
        }
        showProgress(true);

    }

    public void handlePrintReceiptButton(View view){

        if(mIsReusablePrintTokenUsed){
            Intent detailsIntent = new Intent(ExitButtonSubActivity.this,
                    OfflineTransactionDetail.class);
            detailsIntent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, mVehicleType);
            detailsIntent.putExtra(Constants.BUNDLE_PARAM_IS_NEW_RECORD, mIsNew);
            detailsIntent.putExtra(Constants.BUNDLE_PARAM_PARKING_ID,mParkingId);

            detailsIntent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_REG,mVehicleReg);
            detailsIntent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, mVehicleType);

            String entryTime = DateTime.forInstant(mEntryTime,
                                                  Constants.TIME_ZONE).format("YYYY-MM-DD hh:mm");
            detailsIntent.putExtra(Constants.BUNDLE_PARAM_ENTRY_TIME,entryTime);

            String exitTime = DateTime.forInstant(mExitTime,
                                                  Constants.TIME_ZONE).format("YYYY-MM-DD hh:mm");
            detailsIntent.putExtra(Constants.BUNDLE_PARAM_EXIT_TIME,exitTime);

            detailsIntent.putExtra(Constants.BUNDLE_PARAM_DURATION_DESCRIPTION,
                    mDurationDescription);

            detailsIntent.putExtra(Constants.BUNDLE_PARAM_PARKING_FEES, mParkingFees);

            startActivity(detailsIntent);
        }else {
            mLogExitTask = new logExitTask(mParkingId, mExitTime, mParkingFees, true,
                                           mDurationDescription);
            mLogExitTask.execute((Void) null);
            showProgress(true);
        }

    }




    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mExitSubEntryMainView.setVisibility(show ? View.GONE : View.VISIBLE);
            mExitSubEntryMainView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mExitSubEntryMainView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mExitSubEntryMainView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous saveToTransaction task used to authenticate
     * the user.
     */
    public class logExitTask extends AsyncTask<Void, Void, Boolean> {


        String mParkingId;
        long mExitTime;
        double mParkingFees;
        boolean mPrintReceipt;
        String mDurationDescription;




        logExitTask(String parkingId, long exitTime, double parkingFees, boolean printReceipt,
                    String durationDescription) {
            mParkingId   = parkingId;
            mExitTime    =  exitTime;
            mParkingFees = parkingFees;
            mPrintReceipt = printReceipt;
            mDurationDescription = durationDescription;

        }

        @Override
        protected Boolean doInBackground(Void... params) {

            int operatorId = (Integer) Cache.getFromCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_OPERATOR_QUERY,Constants.TYPE_INT);

            if(!mIsSimplyParkTransaction) {

               if(mIsNew){
                  mTransactionObj = new TransactionTbl();

               }else{
                   List<TransactionTbl> mTransactionObjList =
                        TransactionTbl.find(TransactionTbl.class, "m_parking_id = ?",
                                mParkingId);

                   if (mTransactionObjList.isEmpty()) {
                       return false;
                   }
                   mTransactionObj = mTransactionObjList.get(0);
               }

                int parkingFees = (int) Math.ceil(mParkingFees);

                mTransactionObj.setParkingId(mParkingId);
                mTransactionObj.setEntryTime(mEntryTime);
                mTransactionObj.setExitTime(mExitTime);
                mTransactionObj.setCarRegNumber(mVehicleReg);
                mTransactionObj.setVehicleType(mVehicleType);
                mTransactionObj.setParkingFees(Math.ceil(parkingFees));
                mTransactionObj.setParkingStatus(Constants.PARKING_STATUS_EXITED);
                //Issue109: setting the defauly sync state as false
                mTransactionObj.setIsSyncedWithServer(false);
                mTransactionObj.setNightChargeDescription(mNightChargeString);
                mTransactionObj.setPenaltyChargeDescription(mPenaltyChargeString);
                mTransactionObj.setOperatorId(operatorId);

                DateTime eDt = (DateTime.forInstant(mTransactionObj.getEntryTime(), Constants.TIME_ZONE));
                String entryTimeStr = eDt.format("DD-MM-YYYY hh:mm");

                DateTime exDt = (DateTime.forInstant(mExitTime, Constants.TIME_ZONE));
                String exitTimeStr = exDt.format("DD-MM-YYYY hh:mm");

                mTransactionObj.save();

                String token = mTransactionObj.getToken();

                if(!token.isEmpty()){
                    ReusableTokensTbl.markTokenAsFree(token);
                }

                Context context = getApplicationContext();


                if (mPrintReceipt && !mIsReusablePrintTokenUsed) {
                    //TODO: can night and penalty charge co-exist together? currently we assume
                    //either night charge is charged or penalty charge.
                    String chargesString = "";
                    if(!mNightChargeString.isEmpty()){
                        chargesString = mNightChargeString;
                    }else if (!mPenaltyChargeString.isEmpty()){
                        chargesString = mPenaltyChargeString;
                    }

                    //bluetooth printing
                    PrinterService.startActionPrintExitTicket(context, entryTimeStr, exitTimeStr,
                            Integer.toString(parkingFees),chargesString,
                            mTransactionObj.getCarRegNumber(), mDurationDescription,
                            mTransactionObj.getVehicleType());
                }

                //Intent service to update the server
                ServerUpdationIntentService.startActionLogExit(getApplicationContext(),
                        mParkingId, mTransactionObj.getEntryTime(),mExitTime, parkingFees,
                        mTransactionObj.getVehicleType(),mTransactionObj.getCarRegNumber(),
                        mTransactionObj.getToken());

                mVehicleType = mTransactionObj.getVehicleType();

            }else{
                SimplyParkTransactionTbl spTransaction = null;
                SimplyParkBookingTbl booking = null;

                if(mIsNew){
                    spTransaction = new SimplyParkTransactionTbl();
                    spTransaction.setBookingId(mBookingId);

                }else {
                    List<SimplyParkTransactionTbl> transactionList = SimplyParkTransactionTbl.find(
                            SimplyParkTransactionTbl.class, "m_parking_id = ?", mParkingId);


                    if (transactionList.isEmpty()) {
                        return false;
                    }

                    spTransaction = transactionList.get(0);

                }


                if(!mBookingId.isEmpty()) {
                    List<SimplyParkBookingTbl> bookingList = SimplyParkBookingTbl.find(
                            SimplyParkBookingTbl.class, "m_booking_id = ?",
                            mBookingId);

                    if (bookingList.isEmpty()) {
                        return false;
                    }

                    booking = bookingList.get(0);

                    booking.decrementSlotsInUse(getApplicationContext(),
                            spTransaction.getVehicleType());

                    booking.save();
                }

                spTransaction.setEntryTime(mEntryTime);
                spTransaction.setExitTime(mExitTime);
                //Issue109: setting the transactions as NOT synced by default
                spTransaction.setIsSyncedWithServer(false);
                spTransaction.setStatus(Constants.PARKING_STATUS_EXITED);
                spTransaction.setParkingFees((int) Math.ceil(mParkingFees));
                spTransaction.setParkingId(mParkingId);
                spTransaction.setOperatorId(operatorId);
                spTransaction.setVehicleReg(mVehicleReg);
                spTransaction.setVehicleType(mVehicleType);


                spTransaction.save();

                //Intent service to update the server
                ServerUpdationIntentService.startActionSimplyParkLogExit(getApplicationContext(),
                                                                    spTransaction.getBookingId(),
                                                                    spTransaction.getParkingId(),
                                                                    spTransaction.getExitTime(),
                                                                    spTransaction.getParkingFees(),
                                                                    spTransaction.getVehicleType(),
                                                                    spTransaction.getVehicleReg());

                mVehicleType = spTransaction.getVehicleType();



            }

            return true;

        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mLogExitTask = null;

            int resCode = RESULT_OK;
            Intent intent = ExitButtonSubActivity.this.getIntent();

            if (success) {

                intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, mVehicleType);
                intent.putExtra(Constants.BUNDLE_PARAM_IS_NEW_RECORD, mIsNew);
                ExitButtonSubActivity.this.setResult(resCode, intent);
                finish();

            } else {
                //TODO: figure out the failure case handling
                resCode = RESULT_CANCELED;
                ExitButtonSubActivity.this.setResult(resCode, intent);
                finish();

            }


        }

        @Override
        protected void onCancelled() {
            mLogExitTask = null;

        }
    }

    public class getPassDetails extends AsyncTask<Void, Void, Boolean> {
        String mBookingId;
        View mVisitorDetailsView;
        getPassDetails(String bookingId, View   visitorDetailsView){
            this.mBookingId = bookingId;
            this.mVisitorDetailsView = visitorDetailsView;
        }

        @Override
        protected Boolean doInBackground(Void... params){
            String[] queryParams = {mBookingId, mBookingId};
            List<SimplyParkBookingTbl> bookingList =
                    SimplyParkBookingTbl.find(SimplyParkBookingTbl.class,
                            "m_booking_id = ? OR m_encoded_pass_code = ?", queryParams);
            if(bookingList.isEmpty()){
                return false;
            }
            mBooking = bookingList.get(0);
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success){
            if(success){
                mVisitorDetailsView.setVisibility(View.VISIBLE);
                TextView visitorName = (TextView) findViewById(R.id.VisitorName);
                TextView regNoId = (TextView) findViewById(R.id.RegNoId);
                TextView passValidity = (TextView) findViewById(R.id.passValidity);
                visitorName.setText(mBooking.getCustomerDesc());
                regNoId.setText(mBooking.getVehicleReg());
                String entryDateTime =  DateTime.forInstant(mBooking.getBookingStartTime(),
                        Constants.TIME_ZONE).format("DD-MM-YY");

                String exitDateTime =  DateTime.forInstant(mBooking.getBookingEndTime(),
                        Constants.TIME_ZONE).format("DD-MM-YY");
                String passValidityString = entryDateTime+" to "+exitDateTime;
                passValidity.setText(passValidityString);
            }else{
                mVisitorDetailsView.setVisibility(View.GONE);
            }
        }
    }

    /**
     * Represents an asynchronous saveToTransaction task used to authenticate
     * the user.
     */
    public class logPassExitTask extends AsyncTask<Void, Void, Boolean> {

        String mBookingId;
        long mExitTime;
        double mParkingFees;
        boolean mPrintReceipt;
        String mDurationDescription;
        int    mVehicleType;
        SimplyParkTransactionTbl mTransaction;


        logPassExitTask(String bookingId, long exitTime, double parkingFees, boolean printReceipt,
                    String durationDescription, int vehicleType) {
            mBookingId   = bookingId;
            mExitTime    =  exitTime;
            mParkingFees = parkingFees;
            mPrintReceipt = printReceipt;
            mDurationDescription = durationDescription;
            mVehicleType = vehicleType;

        }

        @Override
        protected Boolean doInBackground(Void... params) {

            int operatorId = (Integer) Cache.getFromCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_OPERATOR_QUERY,Constants.TYPE_INT);
            boolean isValid = false;

            if(mBooking!=null) {
                String vehicleReg = mBooking.getVehicleReg();
                String[] qParams = {mBookingId, vehicleReg,
                        Integer.toString(Constants.PARKING_STATUS_ENTERED)};
                mVehicleType = mBooking.getVehicleType();
                List<SimplyParkTransactionTbl> transList =
                        SimplyParkTransactionTbl.find(SimplyParkTransactionTbl.class,
                                "m_booking_id = ? and m_vehicle_reg = ? and m_status = ?",
                                qParams);
                if(transList.isEmpty()) {
                    //the booking is found but this app has no entry. We are going to assume
                    //that the entry happened through some other gate and the app isnt synced
                    //yet. However, we have no parking ID to sync the server. Therefore, we
                    //are going to save a pending exit with car reg and exit datetime and
                    //update the server when the entry datetime syncs with the app
                    isValid = false;
                    PendingExits pendingExit = new PendingExits();
                    pendingExit.save(mBookingId,mBooking.getVehicleReg(),mExitTime);
                    mBooking.decrementSlotsInUse(getApplicationContext(),mBooking.getVehicleType());
                    mBooking.save();

                }else{
                    mTransaction = transList.get(0);
                    mTransaction.setIsSyncedWithServer(false);
                    mTransaction.setExitTime(mExitTime);
                    mTransaction.setStatus(Constants.PARKING_STATUS_EXITED);
                    mTransaction.setStatus(Constants.PARKING_STATUS_EXITED);
                    mTransaction.setParkingFees((int) Math.ceil(mParkingFees));
                    mTransaction.setOperatorId(operatorId);

                    mTransaction.save();

                    mBooking.decrementSlotsInUse(getApplicationContext(),mBooking.getVehicleType());
                    mBooking.save();

                    isValid = true;
                }

            }

            if(isValid) {

                Context context = getApplicationContext();
                if (mPrintReceipt) {
                    //TODO: can night and penalty charge co-exist together? currently we assume
                    //either night charge is charged or penalty charge.
                    String chargesString = "";
                    if(!mNightChargeString.isEmpty()){
                        chargesString = mNightChargeString;
                    }else if (!mPenaltyChargeString.isEmpty()){
                        chargesString = mPenaltyChargeString;
                    }

                    DateTime eDt = (DateTime.forInstant(mTransaction.getEntryTime(), Constants.TIME_ZONE));
                    String entryTimeStr = eDt.format("DD-MM-YYYY hh:mm");

                    DateTime exitTimeDt = (DateTime.forInstant(mExitTime, Constants.TIME_ZONE));
                    String exitTimeStr = exitTimeDt.format("DD-MM-YYYY hh:mm");


                    //bluetooth printing
                    PrinterService.startActionPrintExitTicket(context, entryTimeStr, exitTimeStr,
                            Double.toString(mParkingFees),chargesString,
                            mTransactionObj.getCarRegNumber(), mDurationDescription,
                            mTransactionObj.getVehicleType());
                }
                //Intent service to update the server
                ServerUpdationIntentService.startActionSimplyParkLogExit(getApplicationContext(),
                        mTransaction.getBookingId(),
                        mTransaction.getParkingId(),
                        mTransaction.getExitTime(),
                        mTransaction.getParkingFees(),
                        mTransaction.getVehicleType(),
                        mTransaction.getVehicleReg());

                mVehicleType = mTransaction.getVehicleType();

            }

            return true;

        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mLogExitTask = null;

            int resCode = RESULT_OK;
            Intent intent = ExitButtonSubActivity.this.getIntent();

            if (success) {
                intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                        mVehicleType);

                intent.putExtra(Constants.BUNDLE_PARAM_IS_NEW_RECORD,
                        mIsNew);

            } else {
                //TODO: figure out the failure case handling
                resCode = RESULT_CANCELED;
            }

            ExitButtonSubActivity.this.setResult(resCode, intent);
            finish();
        }

        @Override
        protected void onCancelled() {
            mLogExitTask = null;

        }
    }

}
