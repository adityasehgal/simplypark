package in.simplypark.spot_app.utils;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import in.simplypark.spot_app.activities.MainActivity;
import in.simplypark.spot_app.activities.SplashScreen;
import in.simplypark.spot_app.alarm.AlarmHandler;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.services.ServerUpdationIntentService;

public class SystemNotificationsReceiver extends BroadcastReceiver {
    public SystemNotificationsReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // Enter relevant functionality for when the last widget is recieved
        final String action = intent.getAction();
        //Log.d(EntryExit.class.getSimpleName(), "action: " + intent.getAction());

        try {

           if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {


                if (isConnected(context)) {
                    MainActivity instance = MainActivity.getInstance();
                    if(instance != null) {
                        instance.handleConnectivityChangeEvent(Constants.INTERNET_CONNECTIVITY_UP);
                    }
                    //finish pending actions
                    ServerUpdationIntentService.startActionTriggerPendingActions(context);
                    //get sync id and compare if we need to push or pull from server
                    ServerUpdationIntentService.startActionSyncWithServer(context);
                } else {
                    MainActivity instance = MainActivity.getInstance();
                    if(instance != null) {
                        instance.handleConnectivityChangeEvent(Constants.INTERNET_CONNECTIVITY_DOWN);
                    }

                }
           }else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
               final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                       BluetoothAdapter.ERROR);

               MainActivity instance = MainActivity.getInstance();

               if(instance != null) {

                   switch (state) {
                       case BluetoothAdapter.STATE_OFF:
                          instance.handleBluetoothChangeEvent(Constants.BLUETOOTH_CONNECTIVITY_DOWN);
                          // Printer.getPrinter().disconnect();

                           break;
                       case BluetoothAdapter.STATE_ON:
                           instance.handleBluetoothChangeEvent(Constants.BLUETOOTH_CONNECTIVITY_UP);
                           break;

                   }
               }
           }else if (action.equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)){
               final int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE,
                                                    BluetoothDevice.ERROR);

               MainActivity instance = MainActivity.getInstance();

               if(instance != null) {

                   switch (state) {
                       case BluetoothDevice.BOND_BONDED:
                           instance.handleBluetoothChangeEvent(Constants.BLUETOOTH_CONNECTIVITY_UP);

                           break;
                       case BluetoothDevice.BOND_NONE:
                           instance.handleBluetoothChangeEvent(Constants.BLUETOOTH_CONNECTIVITY_DOWN);
                           break;
                   }
               }
           }else if(action.equals("android.intent.action.BATTERY_LOW") ||
                    action.equals("android.intent.action.BATTERY_OKAY")){
               MainActivity instance = MainActivity.getInstance();

               if(instance != null){
                   instance.handleBatteryEvent();
               }
           }else if(action.equals(Constants.TIMER_ACTION)){

               final int timerAction = intent.getIntExtra(Constants.BUNDLE_PARAM_TIMER_ACTION,
                                                         Constants.TIMER_INVALID_ACTION);

               switch (timerAction){
                   case Constants.TIMER_ACTION_PUSH:
                       AlarmHandler.setTimerExpired(timerAction);
                       Cache.setInCache(context,
                                        Constants.CACHE_PREFIX_TIMER_ACITON_PUSH_START_TIME_QUERY,
                               (long)0,Constants.TYPE_LONG);

                       Cache.setInCache(context,
                               Constants.CACHE_PREFIX_TIMER_ACITON_PUSH_END_TIME_QUERY,
                               (long) 0, Constants.TYPE_LONG);
                       ServerUpdationIntentService.startActionPushUpdatesToServer(context);
                       break;

                   case Constants.TIMER_ACTION_END_OF_DAY:
                       //this is a repeating alarm. so dont set it as expired
                       ServerUpdationIntentService.startActionEndOfDay(context);
               }


           }else if(action.equals("android.intent.action.BOOT_COMPLETED") ||
                   action.equals("android.intent.action.QUICKBOOT_POWERON")){
               boolean isParkingPrepaid = (Boolean)Cache.getFromCache(context,
                       Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY,
                       Constants.TYPE_BOOL);


               boolean isSpaceConfigured = (Boolean) Cache.getFromCache(context,
                       Constants.CACHE_PREFIX_LOGIN_QUERY,
                       Constants.TYPE_BOOL);

               if(isSpaceConfigured) {

                   String openingDateTime = (String)Cache.getFromCache(context,
                           Constants.CACHE_PREFIX_OPENING_DATE_TIME_QUERY,
                           Constants.TYPE_STRING);

                   String closingDateTime = (String)Cache.getFromCache(context,
                           Constants.CACHE_PREFIX_CLOSING_DATE_TIME_QUERY,
                           Constants.TYPE_STRING);

                   /*AlarmHandler.startDayEndTimer(context,Constants.TIMER_ACTION,openingDateTime,
                                                closingDateTime);*/
               }

               Intent startupIntent = new Intent(context, SplashScreen.class);
               context.startActivity(startupIntent);
           }else if(action.equals(Intent.ACTION_DATE_CHANGED)) {
               boolean isParkingPrepaid = (Boolean)Cache.getFromCache(context,
                                        Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY,
                                        Constants.TYPE_BOOL);

               if(isParkingPrepaid){
                   ServerUpdationIntentService.startActionResetOccupancyCount(context);
                   //MiscUtils.resetOccupancyCount(context);
               }
           }

        }catch(Exception e){
                Log.e("ConnectivityChage", "Exception" + e);
        }


    }

    /**
     * @description static function to determine if an internet connection exists
     * @param context
     * @return boolean
     */
    public static boolean isConnected(Context context){
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        return isConnected;
    }


    /**
     * Check for Bluetooth.
     * @return True if Bluetooth is available.
     */
    public static boolean isBluetoothAvailable() {
        final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        return (bluetoothAdapter != null && bluetoothAdapter.isEnabled());
    }
}
