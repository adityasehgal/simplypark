package in.simplypark.spot_app.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.simplypark.spot_app.R;
import in.simplypark.spot_app.db_tables.TransactionTbl;

/**
 * Created by csharma on 20-12-2016.
 */

public class AutoCompleteRegDropDownAdapter extends ArrayAdapter<TransactionTbl> {
    Context mActivity;
    List<TransactionTbl> mElements;
    boolean isRegNo;
    List<TransactionTbl> filteredTransactions = new ArrayList<>();
    AutoCompleteTextView mTicketIdView;
    public AutoCompleteRegDropDownAdapter(Context activity, int resource, List<TransactionTbl> transactionTblList, boolean isRegNo, AutoCompleteTextView mTicketIdView) {
        super(activity, resource, transactionTblList);
        mActivity = activity;
        mElements = transactionTblList;
        this.isRegNo = isRegNo;
        this.mTicketIdView = mTicketIdView;
    }

    @Override
    public int getCount() {
        return filteredTransactions.size();
    }

    @Override
    public Filter getFilter() {
        return new RegNoDropDownFilter(this, mElements, isRegNo);
    }

@Override
public void notifyDataSetInvalidated(){
    super.notifyDataSetInvalidated();
    this.mTicketIdView.setError("Invalid vehicle number.");
}
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mActivity); //.getLayoutInflater();
        Log.i("AUTOCOMPLETEADAPTER","getView Called "+position);


        View rowView = inflater.inflate(R.layout.autocomplete_text_item, parent, false);

        TextView regNumber = (TextView)rowView.findViewById(R.id.car_reg_id);

        TransactionTbl element = filteredTransactions.get(position);
//        Toast.makeText(getContext(),"Reg Number "+element.getCarRegNumber(),Toast.LENGTH_SHORT);

        if(isRegNo) {
            regNumber.setText(element.getCarRegNumber());
            rowView.setTag(element.getCarRegNumber());
        }else {
            regNumber.setText(element.getParkingId());
            rowView.setTag(element.getParkingId());
        }
        return rowView;
    }
    public List<TransactionTbl> getAllElements(){
        return filteredTransactions;
    }

}