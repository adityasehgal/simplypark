package in.simplypark.spot_app.logcollection;

import com.orm.SugarApp;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.HttpSender;

import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.R;
import in.simplypark.spot_app.utils.MiscUtils;

/**
 * Created by csharma on 09-12-2016.
 */
@ReportsCrashes(
        formUri = Constants.APP_CRASH_LOG_REPORT,
//        reportType = HttpSender.Type.JSON,
        httpMethod = HttpSender.Method.POST,
//        formUriBasicAuthLogin = "GENERATED_USERNAME_WITH_WRITE_PERMISSIONS",
//        formUriBasicAuthPassword = "GENERATED_PASSWORD",

        customReportContent = {
                ReportField.APP_VERSION_CODE,
                ReportField.APP_VERSION_NAME,
                ReportField.ANDROID_VERSION,
                ReportField.PACKAGE_NAME,
                ReportField.REPORT_ID,
                ReportField.BUILD,
                ReportField.FILE_PATH,
                ReportField.STACK_TRACE,
                ReportField.LOGCAT,
                /*To add the CUSTOM_DATA, such as space_id, operator_id*/
                ReportField.CUSTOM_DATA
        },
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.toast_crash
)
public class SpotLogs extends SugarApp {
    /* The purpose of the SpotLogs is to Report the stacktraces, if any to the server.    */
        @Override
        public void onCreate() {
            // The following line triggers the initialization of ACRA
            super.onCreate();
            ACRA.init(this);
            ACRA.getErrorReporter().putCustomData("SPACE_ID", String.valueOf(MiscUtils.getSpaceId(getApplicationContext())));
            ACRA.getErrorReporter().putCustomData("OPERATOR_ID", String.valueOf(MiscUtils.getOperatorId(getApplicationContext())));
        }

}



