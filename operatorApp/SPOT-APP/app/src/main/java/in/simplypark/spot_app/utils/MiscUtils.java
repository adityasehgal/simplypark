package in.simplypark.spot_app.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import hirondelle.date4j.DateTime;
import in.simplypark.spot_app.config.Constants;

/**
 * Created by root on 13/06/16.
 */
public class MiscUtils {

    public static List<String> errorMsgs = new ArrayList<String>();

    private static final ReentrantLock mSyncIdLock   = new ReentrantLock();
    private static final ReentrantLock mOccupancyLock = new ReentrantLock();
    private static final ReentrantLock mCacheMonthlyPassLock = new ReentrantLock();
    private static final ReentrantLock mCacheCustomFormatLock = new ReentrantLock();


    public static void displayNotification(Context context, String notification,
                                                          boolean isDurationLong){
        int duration = Toast.LENGTH_SHORT;

        if(isDurationLong){
            duration = Toast.LENGTH_LONG;
        }

        Toast toast = Toast.makeText(context, notification, duration);
        toast.show();

    }




    public static String getOpeningTimeFromDateTime(String openingDateTime){
        String openingTimeSubStr = openingDateTime.substring(openingDateTime.length() - 8,
                (openingDateTime.length() - 8) + 5);

        return openingTimeSubStr;

    }

    public static String getClosingTimeFromDateTime(String closingDateTime){
        String closingTimeSubStr = closingDateTime.substring(closingDateTime.length() - 8,
                (closingDateTime.length() - 8) + 5);

        return closingTimeSubStr;
    }

    public static long getLongOpeningTimeForToday(Context context){
        DateTime now = DateTime.today(Constants.TIME_ZONE);
        String today = now.format("YYYY-MM-DD");
        String openingTime = "00:00";

        boolean isOpen24Hours = (Boolean) Cache.getFromCache(context,
                                  Constants.CACHE_PREFIX_IS_OPEN_24_HOURS,Constants.TYPE_BOOL);

        if(!isOpen24Hours) {
            openingTime = (String) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_OPENING_TIME_QUERY,
                    Constants.TYPE_STRING);
        }

        today += " " + openingTime;


        return new DateTime(today).getMilliseconds(Constants.TIME_ZONE);
    }

    public static long getLongClosingTimeForToday(Context context){
        DateTime now = DateTime.today(Constants.TIME_ZONE);

        String openingTime = "00:00";
        String closingTime = "00:00";

        boolean isOpen24Hours = (Boolean)Cache.getFromCache(context,
                Constants.CACHE_PREFIX_IS_OPEN_24_HOURS,Constants.TYPE_BOOL);


        if(!isOpen24Hours) {
            openingTime = (String) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_OPENING_TIME_QUERY,
                    Constants.TYPE_STRING);

            closingTime = (String) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_CLOSING_TIME_QUERY,
                    Constants.TYPE_STRING);

            int openingHour = Integer.parseInt(openingTime.substring(0, 2));
            int closingHour = Integer.parseInt(closingTime.substring(0, 2));

            if (closingHour <= openingHour) {
                now = now.plusDays(1);
            }
        }else{
            now = now.plusDays(1);
        }

        String today = now.format("YYYY-MM-DD");

        today += " " + closingTime;

        return new DateTime(today).getMilliseconds(Constants.TIME_ZONE);
    }

    public static void decrementOccupancyCount(Context context,int vehicleType) {
        mOccupancyLock.lock();

        try {
            int numOccupied = (Integer) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_CURRENT_OCCUPANCY_QUERY,
                    Constants.TYPE_INT);

            if(numOccupied > 0) {

                switch(vehicleType){
                    case Constants.VEHICLE_TYPE_CAR:{
                        Cache.setInCache(context,
                                Constants.CACHE_PREFIX_CURRENT_OCCUPANCY_QUERY,
                                --numOccupied, Constants.TYPE_INT);

                        int numCars = (Integer) Cache.getFromCache(context,
                                Constants.CACHE_PREFIX_PARKED_CARS_COUNT_QUERY,
                                Constants.TYPE_INT);

                        Cache.setInCache(context,
                                Constants.CACHE_PREFIX_PARKED_CARS_COUNT_QUERY,
                                --numCars, Constants.TYPE_INT);

                    }
                    break;

                    case Constants.VEHICLE_TYPE_BIKE:{
                        int slotToBikeRatio = (Integer) Cache.getFromCache(context,
                                Constants.CACHE_PREFIX_BIKES_TO_SLOT_RATIO,
                                Constants.TYPE_INT);
                        int numBikes = (Integer) Cache.getFromCache(context,
                                Constants.CACHE_PREFIX_PARKED_BIKES_COUNT_QUERY,
                                Constants.TYPE_INT);

                        Cache.setInCache(context,
                                Constants.CACHE_PREFIX_PARKED_BIKES_COUNT_QUERY,
                                --numBikes, Constants.TYPE_INT);

                        if (numBikes % slotToBikeRatio == 0) {
                            Cache.setInCache(context, Constants.CACHE_PREFIX_CURRENT_OCCUPANCY_QUERY,
                                    --numOccupied, Constants.TYPE_INT);

                        }

                    }
                    break;

                    case Constants.VEHICLE_TYPE_MINIBUS:{
                        Cache.setInCache(context,
                                Constants.CACHE_PREFIX_CURRENT_OCCUPANCY_QUERY,
                                --numOccupied, Constants.TYPE_INT);

                        int numMiniBus = (Integer) Cache.getFromCache(context,
                                Constants.CACHE_PREFIX_PARKED_MINIBUS_COUNT_QUERY,
                                Constants.TYPE_INT);

                        Cache.setInCache(context,
                                Constants.CACHE_PREFIX_PARKED_MINIBUS_COUNT_QUERY,
                                --numMiniBus, Constants.TYPE_INT);


                    }
                    break;

                    case Constants.VEHICLE_TYPE_BUS:{
                        Cache.setInCache(context,
                                Constants.CACHE_PREFIX_CURRENT_OCCUPANCY_QUERY,
                                --numOccupied, Constants.TYPE_INT);

                        int numBus = (Integer) Cache.getFromCache(context,
                                Constants.CACHE_PREFIX_PARKED_BUS_COUNT_QUERY,
                                Constants.TYPE_INT);

                        Cache.setInCache(context,
                                Constants.CACHE_PREFIX_PARKED_BUS_COUNT_QUERY,
                                --numBus, Constants.TYPE_INT);


                    }
                    break;

                }
            }
        }finally{
            mOccupancyLock.unlock();
        }
    }

    public static void decrementVehiclesParkedCount(Context context, int vehicleType){
        mOccupancyLock.lock();

        try {
            int numParked = (Integer) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_CURRENT_PARKED_QUERY,
                    Constants.TYPE_INT);
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_CURRENT_OCCUPANCY_QUERY,
                    --numParked, Constants.TYPE_INT);

            switch(vehicleType){
                case Constants.VEHICLE_TYPE_CAR:{
                    int numCars = (Integer) Cache.getFromCache(context,
                            Constants.CACHE_PREFIX_PREPAID_CARS_COUNT_QUERY,
                            Constants.TYPE_INT);

                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_PREPAID_CARS_COUNT_QUERY,
                            --numCars, Constants.TYPE_INT);

                }
                break;

                case Constants.VEHICLE_TYPE_BIKE:{
                    int numBikes = (Integer) Cache.getFromCache(context,
                            Constants.CACHE_PREFIX_PREPAID_BIKES_COUNT_QUERY,
                            Constants.TYPE_INT);

                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_PREPAID_BIKES_COUNT_QUERY,
                            --numBikes, Constants.TYPE_INT);

                }
                break;

                case Constants.VEHICLE_TYPE_MINIBUS:{
                    int numMiniBus = (Integer) Cache.getFromCache(context,
                            Constants.CACHE_PREFIX_PREPAID_MINIBUS_COUNT_QUERY,
                            Constants.TYPE_INT);

                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_PREPAID_MINIBUS_COUNT_QUERY,
                            --numMiniBus, Constants.TYPE_INT);

                }
                break;

                case Constants.VEHICLE_TYPE_BUS:{
                    int numBus = (Integer) Cache.getFromCache(context,
                            Constants.CACHE_PREFIX_PREPAID_BUS_COUNT_QUERY,
                            Constants.TYPE_INT);

                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_PREPAID_BUS_COUNT_QUERY,
                            --numBus, Constants.TYPE_INT);


                }
                break;
            }


            if (vehicleType == Constants.VEHICLE_TYPE_CAR) {

                int numCars = (Integer) Cache.getFromCache(context,
                        Constants.CACHE_PREFIX_PREPAID_CARS_COUNT_QUERY,
                        Constants.TYPE_INT);

                Cache.setInCache(context,
                        Constants.CACHE_PREFIX_PREPAID_CARS_COUNT_QUERY,
                        --numCars, Constants.TYPE_INT);

            } else {

                int numBikes = (Integer) Cache.getFromCache(context,
                        Constants.CACHE_PREFIX_PREPAID_BIKES_COUNT_QUERY,
                        Constants.TYPE_INT);

                Cache.setInCache(context,
                        Constants.CACHE_PREFIX_PREPAID_BIKES_COUNT_QUERY,
                        --numBikes, Constants.TYPE_INT);


            }
        }finally{
            mOccupancyLock.unlock();
        }

    }


    public static void incrementOccupancyCount(Context context,int vehicleType){

        mOccupancyLock.lock();

        try {
            int numOccupied = (Integer) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_CURRENT_OCCUPANCY_QUERY,
                    Constants.TYPE_INT);

            switch(vehicleType){
                case Constants.VEHICLE_TYPE_CAR:{
                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_CURRENT_OCCUPANCY_QUERY,
                            ++numOccupied, Constants.TYPE_INT);

                    int numCars = (Integer) Cache.getFromCache(context,
                            Constants.CACHE_PREFIX_PARKED_CARS_COUNT_QUERY,
                            Constants.TYPE_INT);

                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_PARKED_CARS_COUNT_QUERY,
                            ++numCars, Constants.TYPE_INT);

                }
                break;

                case Constants.VEHICLE_TYPE_BIKE:{
                    int slotToBikeRatio = (Integer) Cache.getFromCache(context,
                            Constants.CACHE_PREFIX_BIKES_TO_SLOT_RATIO,
                            Constants.TYPE_INT);
                    int numBikes = (Integer) Cache.getFromCache(context,
                            Constants.CACHE_PREFIX_PARKED_BIKES_COUNT_QUERY,
                            Constants.TYPE_INT);

                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_PARKED_BIKES_COUNT_QUERY,
                            ++numBikes, Constants.TYPE_INT);

                    if (numBikes % slotToBikeRatio == 1) {
                        Cache.setInCache(context, Constants.CACHE_PREFIX_CURRENT_OCCUPANCY_QUERY,
                                ++numOccupied, Constants.TYPE_INT);

                    }

                }
                break;

                case Constants.VEHICLE_TYPE_MINIBUS:{
                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_CURRENT_OCCUPANCY_QUERY,
                            ++numOccupied, Constants.TYPE_INT);

                    int numMiniBus = (Integer) Cache.getFromCache(context,
                            Constants.CACHE_PREFIX_PARKED_CARS_COUNT_QUERY,
                            Constants.TYPE_INT);

                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_PARKED_CARS_COUNT_QUERY,
                            ++numMiniBus, Constants.TYPE_INT);

                }
                break;

                case Constants.VEHICLE_TYPE_BUS:{
                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_CURRENT_OCCUPANCY_QUERY,
                            ++numOccupied, Constants.TYPE_INT);

                    int numBus = (Integer) Cache.getFromCache(context,
                            Constants.CACHE_PREFIX_PARKED_CARS_COUNT_QUERY,
                            Constants.TYPE_INT);

                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_PARKED_CARS_COUNT_QUERY,
                            ++numBus, Constants.TYPE_INT);

                }
                break;
            }


        }finally{
            mOccupancyLock.unlock();
        }


    }

    public static void incrementVehiclesParkedCount(Context context, int vehicleType){
        mOccupancyLock.lock();

        try {
            int numParked = (Integer) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_CURRENT_PARKED_QUERY,
                    Constants.TYPE_INT);
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_CURRENT_OCCUPANCY_QUERY,
                    ++numParked, Constants.TYPE_INT);

            switch(vehicleType){
                case Constants.VEHICLE_TYPE_CAR:{
                    int numCars = (Integer) Cache.getFromCache(context,
                            Constants.CACHE_PREFIX_PREPAID_CARS_COUNT_QUERY,
                            Constants.TYPE_INT);

                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_PREPAID_CARS_COUNT_QUERY,
                            ++numCars, Constants.TYPE_INT);

                }
                break;

                case Constants.VEHICLE_TYPE_BIKE:{
                    int numBikes = (Integer) Cache.getFromCache(context,
                            Constants.CACHE_PREFIX_PREPAID_BIKES_COUNT_QUERY,
                            Constants.TYPE_INT);

                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_PREPAID_BIKES_COUNT_QUERY,
                            ++numBikes, Constants.TYPE_INT);


                }
                break;

                case Constants.VEHICLE_TYPE_MINIBUS:{
                    int numMiniBus = (Integer) Cache.getFromCache(context,
                            Constants.CACHE_PREFIX_PREPAID_MINIBUS_COUNT_QUERY,
                            Constants.TYPE_INT);

                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_PREPAID_MINIBUS_COUNT_QUERY,
                            ++numMiniBus, Constants.TYPE_INT);
                }
                break;

                case Constants.VEHICLE_TYPE_BUS:{
                    int numBus = (Integer) Cache.getFromCache(context,
                            Constants.CACHE_PREFIX_PREPAID_BUS_COUNT_QUERY,
                            Constants.TYPE_INT);

                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_PREPAID_BUS_COUNT_QUERY,
                            ++numBus, Constants.TYPE_INT);
                }
                break;
            }

        }finally{
            mOccupancyLock.unlock();
        }


    }

    public static void resetOccupancyCount(Context context){

        mOccupancyLock.lock();

        try {

            Cache.setInCache(context,
                        Constants.CACHE_PREFIX_CURRENT_OCCUPANCY_QUERY,
                        0, Constants.TYPE_INT);


            Cache.setInCache(context,
                        Constants.CACHE_PREFIX_PREPAID_CARS_COUNT_QUERY,
                        0, Constants.TYPE_INT);

            Cache.setInCache(context,
                        Constants.CACHE_PREFIX_PREPAID_BIKES_COUNT_QUERY,
                        0, Constants.TYPE_INT);

            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_PREPAID_MINIBUS_COUNT_QUERY,
                    0, Constants.TYPE_INT);

            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_PREPAID_BUS_COUNT_QUERY,
                    0, Constants.TYPE_INT);


        }finally{
            mOccupancyLock.unlock();
        }


    }

    public static void setOccupancyCounters(Context context,int numParkedCars,
                                            int numParkedBikes,int numParkedMiniBus,
                                            int numParkedBus ){

        mOccupancyLock.lock();

        try {

            int slotToBikeRatio = (Integer) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_BIKES_TO_SLOT_RATIO,
                    Constants.TYPE_INT);

            int occupancyCount = numParkedCars + numParkedMiniBus + numParkedBus;

            occupancyCount += numParkedBikes / slotToBikeRatio;

            if(numParkedBikes % slotToBikeRatio != 0){
                occupancyCount++;
            }

            Cache.setInCache(context,
                        Constants.CACHE_PREFIX_CURRENT_OCCUPANCY_QUERY,
                        occupancyCount, Constants.TYPE_INT);

            Cache.setInCache(context,
                        Constants.CACHE_PREFIX_PARKED_CARS_COUNT_QUERY,
                        numParkedCars, Constants.TYPE_INT);

            Cache.setInCache(context,
                        Constants.CACHE_PREFIX_PARKED_BIKES_COUNT_QUERY,
                        numParkedBikes, Constants.TYPE_INT);

            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_PARKED_MINIBUS_COUNT_QUERY,
                    numParkedMiniBus, Constants.TYPE_INT);

            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_PARKED_BUS_COUNT_QUERY,
                    numParkedBus, Constants.TYPE_INT);


        }finally{
            mOccupancyLock.unlock();
        }


    }



    public static int getCarsParkedCount(Context context){

        int carsParked = 0;

        mOccupancyLock.lock();

        try {

            carsParked = (Integer)Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_PARKED_CARS_COUNT_QUERY,
                    Constants.TYPE_INT);


        }finally{
            mOccupancyLock.unlock();
        }

        return carsParked;


    }

    public static void setCarsParkedCount(Context context, int numParkedCars){



        mOccupancyLock.lock();

        try {

            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_PARKED_CARS_COUNT_QUERY,
                    numParkedCars,Constants.TYPE_INT);


        }finally{
            mOccupancyLock.unlock();
        }
    }
    public static void setPrepaidCarsParkedCount(Context context, int numPrepaidCars){
        mOccupancyLock.lock();

        try {

            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_PREPAID_CARS_COUNT_QUERY,
                    numPrepaidCars,Constants.TYPE_INT);


        }finally{
            mOccupancyLock.unlock();
        }

    }

    public static void setBikesParkedCount(Context context, int numParkedBikes){



        mOccupancyLock.lock();

        try {

            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_PARKED_BIKES_COUNT_QUERY,
                    numParkedBikes, Constants.TYPE_INT);


        }finally{
            mOccupancyLock.unlock();
        }
    }

    public static void setPrepaidBikesParkedCount(Context context, int numPrepaidBikes){
        mOccupancyLock.lock();

        try {

            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_PREPAID_BIKES_COUNT_QUERY,
                    numPrepaidBikes, Constants.TYPE_INT);


        }finally{
            mOccupancyLock.unlock();
        }

    }

    public static int getBikesParkedCount(Context context){

        int bikesParked = 0;

        mOccupancyLock.lock();

        try {

            bikesParked = (Integer)Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_PARKED_BIKES_COUNT_QUERY,
                    Constants.TYPE_INT);


        }finally{
            mOccupancyLock.unlock();
        }

        return bikesParked;


    }

    /* MiniBus & Bus related functions - addition */

    public static int getBusParkedCount(Context context){

        int busParked = 0;

        mOccupancyLock.lock();

        try {

            busParked = (Integer)Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_PARKED_BUS_COUNT_QUERY,
                    Constants.TYPE_INT);


        }finally{
            mOccupancyLock.unlock();
        }

        return busParked;


    }

    public static void setBusParkedCount(Context context, int numParkedBus){


        mOccupancyLock.lock();

        try {

            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_PARKED_BUS_COUNT_QUERY,
                    numParkedBus,Constants.TYPE_INT);


        }finally{
            mOccupancyLock.unlock();
        }
    }
    public static void setPrepaidBusParkedCount(Context context, int numPrepaidBus){
        mOccupancyLock.lock();

        try {

            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_PREPAID_BUS_COUNT_QUERY,
                    numPrepaidBus,Constants.TYPE_INT);

        }finally{
            mOccupancyLock.unlock();
        }

    }

    public static int getMiniBusParkedCount(Context context){

        int miniBusParked = 0;

        mOccupancyLock.lock();

        try {

            miniBusParked = (Integer)Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_PARKED_MINIBUS_COUNT_QUERY,
                    Constants.TYPE_INT);


        }finally{
            mOccupancyLock.unlock();
        }

        return miniBusParked;


    }

    public static void setMiniBusParkedCount(Context context, int numParkedMiniBus){


        mOccupancyLock.lock();

        try {

            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_PARKED_MINIBUS_COUNT_QUERY,
                    numParkedMiniBus,Constants.TYPE_INT);


        }finally{
            mOccupancyLock.unlock();
        }
    }
    public static void setPrepaidMiniBusParkedCount(Context context, int numPrepaidMiniBus){
        mOccupancyLock.lock();

        try {

            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_PREPAID_MINIBUS_COUNT_QUERY,
                    numPrepaidMiniBus,Constants.TYPE_INT);

        }finally{
            mOccupancyLock.unlock();
        }

    }



    //currently only called for prepaid parking
    public static void resetOccupancyView(Context context) {
        mOccupancyLock.lock();

        try {

            boolean isParkingPrepaid = (Boolean) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY, Constants.TYPE_BOOL);


            if (isParkingPrepaid) {
                Cache.setInCache(context,Constants.CACHE_PREFIX_PREPAID_CARS_COUNT_QUERY,0,
                                 Constants.TYPE_INT);
                Cache.setInCache(context,Constants.CACHE_PREFIX_PREPAID_BIKES_COUNT_QUERY,0,
                        Constants.TYPE_INT);
                Cache.setInCache(context,Constants.CACHE_PREFIX_PREPAID_MINIBUS_COUNT_QUERY,0,
                        Constants.TYPE_INT);
                Cache.setInCache(context,Constants.CACHE_PREFIX_PREPAID_BUS_COUNT_QUERY,0,
                        Constants.TYPE_INT);



            }
        }finally{
            mOccupancyLock.unlock();
        }
    }

    public static void updateOccupancyView(Context context, TextView occupancyView){

        mOccupancyLock.lock();

        try {

            boolean isParkingPrepaid = (Boolean)Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY,Constants.TYPE_BOOL);
            String numSlotsString = "";

            if(isParkingPrepaid){
                String vehiclesParked = "";
                int totalPrepaidVehicles = (Integer) Cache.getFromCache(context,
                                  Constants.CACHE_PREFIX_PREPAID_CARS_COUNT_QUERY,
                                  Constants.TYPE_INT);
                totalPrepaidVehicles +=(Integer)Cache.getFromCache(context,
                        Constants.CACHE_PREFIX_PREPAID_MINIBUS_COUNT_QUERY,
                        Constants.TYPE_INT);

                totalPrepaidVehicles +=(Integer)Cache.getFromCache(context,
                        Constants.CACHE_PREFIX_PREPAID_BUS_COUNT_QUERY,
                        Constants.TYPE_INT);


                totalPrepaidVehicles +=(Integer)Cache.getFromCache(context,
                        Constants.CACHE_PREFIX_PREPAID_BIKES_COUNT_QUERY,
                        Constants.TYPE_INT);

                boolean isMonthlyPassesGenerated = (Boolean) Cache.getFromCache(context,
                        Constants.CACHE_PREFIX_IS_MONTHLY_PASS_GENERATED,
                        Constants.TYPE_BOOL);


                if (isMonthlyPassesGenerated) {
                    int passesActiveToday = (Integer) Cache.getFromCache(context,
                            Constants.CACHE_PREFIX_MONTHLY_PASSES_ACTIVE_TODAY,
                            Constants.TYPE_INT);


                    numSlotsString = "vehicles parked + \n" + passesActiveToday + " active passes";

                } else {
                    numSlotsString = "vehicles parked";
                }

                vehiclesParked  = Integer.toString(totalPrepaidVehicles);

                //now create the spanning text
                SpannableString current = new SpannableString(vehiclesParked + "\n");

                SpannableString total = new SpannableString(numSlotsString);

                if (vehiclesParked.length() > 3) {
                    current.setSpan(new RelativeSizeSpan(1.5f), 0, vehiclesParked.length(), 0);
                } else {
                    current.setSpan(new RelativeSizeSpan(2f), 0, vehiclesParked.length(), 0);
                }
                total.setSpan(new RelativeSizeSpan(.2f), 0, numSlotsString.length(), 0);

                CharSequence finalText = TextUtils.concat(current, total);
                //CharSequence finalText = TextUtils.concat(occupancy, " ", total);


                occupancyView.setText(finalText);




            }else {

                String occupancy = "";

                int totalParkedVehicles = (Integer) Cache.getFromCache(context,
                        Constants.CACHE_PREFIX_PARKED_CARS_COUNT_QUERY,
                        Constants.TYPE_INT);

                totalParkedVehicles += (Integer) Cache.getFromCache(context,
                        Constants.CACHE_PREFIX_PARKED_MINIBUS_COUNT_QUERY,
                        Constants.TYPE_INT);

                totalParkedVehicles += (Integer) Cache.getFromCache(context,
                        Constants.CACHE_PREFIX_PARKED_BUS_COUNT_QUERY,
                        Constants.TYPE_INT);

                totalParkedVehicles += (Integer) Cache.getFromCache(context,
                        Constants.CACHE_PREFIX_PARKED_BIKES_COUNT_QUERY,
                        Constants.TYPE_INT);

                int currentOccupancy = (Integer) Cache.getFromCache(context,
                        Constants.CACHE_PREFIX_CURRENT_OCCUPANCY_QUERY,
                        Constants.TYPE_INT);

                if (currentOccupancy != totalParkedVehicles) {
                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_CURRENT_OCCUPANCY_QUERY,
                            totalParkedVehicles, Constants.TYPE_INT);
                }

                boolean isSlotOccupancyAssumed = (Boolean)Cache.getFromCache(context,
                        Constants.CACHE_PREFIX_IS_SLOT_OCCUPIED_ASSUMED_QUERY,Constants.TYPE_BOOL);


                numSlotsString = "vehicles inside";

                if (isSlotOccupancyAssumed) {
                    boolean isMonthlyPassesGenerated = (Boolean) Cache.getFromCache(context,
                            Constants.CACHE_PREFIX_IS_MONTHLY_PASS_GENERATED,
                            Constants.TYPE_BOOL);

                    if (isMonthlyPassesGenerated) {

                        int passesActiveToday = (Integer) Cache.getFromCache(context,
                                Constants.CACHE_PREFIX_MONTHLY_PASSES_ACTIVE_TODAY,
                                Constants.TYPE_INT);


                        numSlotsString = "vehicles inside + \n" + passesActiveToday + " active passes";
                    }
                }


                occupancy = Integer.toString(totalParkedVehicles);


                //now create the spanning text
                SpannableString current = new SpannableString(occupancy + "\n");

                SpannableString total = new SpannableString(numSlotsString);

                if (occupancy.length() > 2) {
                    current.setSpan(new RelativeSizeSpan(1.5f), 0, occupancy.length(), 0);
                } else {
                    current.setSpan(new RelativeSizeSpan(2f), 0, occupancy.length(), 0);
                }
                total.setSpan(new RelativeSizeSpan(.2f), 0, numSlotsString.length(), 0);

                CharSequence finalText = TextUtils.concat(current, total);
                //CharSequence finalText = TextUtils.concat(occupancy, " ", total);


                occupancyView.setText(finalText);
            }
        }finally{
            mOccupancyLock.unlock();
        }
    }







    public static void incrementTodaysEarnings(Context context, int parkingFees){
         int fees = (Integer)Cache.getFromCache(context,
                 Constants.CACHE_PREFIX_EARNINGS_PER_DAY_QUERY,
                 Constants.TYPE_INT);

        fees += parkingFees;

        Cache.setInCache(context,
                Constants.CACHE_PREFIX_EARNINGS_PER_DAY_QUERY,
                fees, Constants.TYPE_INT);
    }

    public static void decrementTodaysEarnings(Context context, int parkingFees){
        int fees = (Integer)Cache.getFromCache(context,
                Constants.CACHE_PREFIX_EARNINGS_PER_DAY_QUERY,
                Constants.TYPE_INT);

        fees -= parkingFees;

        Cache.setInCache(context,
                Constants.CACHE_PREFIX_EARNINGS_PER_DAY_QUERY,
                fees, Constants.TYPE_INT);
    }

    public static int getSyncId(Context context){
        int syncId = Constants.INVALID_SYNC_ID;
        mSyncIdLock.lock();
        try {
            syncId = (Integer) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_SYNC_ID_QUERY,
                    Constants.TYPE_INT);
        }finally{
            mSyncIdLock.unlock();
        }

        return syncId;
    }

    public static void lockSyncId(){
        mSyncIdLock.lock();
    }

    public static void unlockSyncId(){
        mSyncIdLock.unlock();
    }

    public static void setSyncId(Context context, int syncId){
        mSyncIdLock.lock();
        try {
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_SYNC_ID_QUERY, syncId, Constants.TYPE_INT);
        }finally{
            mSyncIdLock.unlock();
        }
    }

    public static void incrementSyncId(Context context){
        int syncId = Constants.INVALID_SYNC_ID;

        mSyncIdLock.lock();
        try {
            syncId = (Integer) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_SYNC_ID_QUERY,
                    Constants.TYPE_INT);

            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_SYNC_ID_QUERY, ++syncId, Constants.TYPE_INT);
        }finally{
            mSyncIdLock.unlock();
        }

    }

    public static void decrementSyncId(Context context){
        int syncId = Constants.INVALID_SYNC_ID;

        mSyncIdLock.lock();

        try {
            syncId = (Integer) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_SYNC_ID_QUERY,
                    Constants.TYPE_INT);

            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_SYNC_ID_QUERY, --syncId, Constants.TYPE_INT);
        }finally{
            mSyncIdLock.unlock();
        }

    }



    public static void updateSyncId(Context context, int updateBy){
        int syncId = Constants.INVALID_SYNC_ID;

        mSyncIdLock.lock();

        try {
            syncId = (Integer) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_SYNC_ID_QUERY,
                    Constants.TYPE_INT);

            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_SYNC_ID_QUERY, syncId + updateBy, Constants.TYPE_INT);
        }finally{
            mSyncIdLock.unlock();
        }


    }

    public static String decodeBookingId(Context context,String encodedBookingId){
        int mNumberIndexStart = 0;
        int mAlphabetIndexStart = 10;

        byte itr = (byte) encodedBookingId.charAt(0);
        int customerNumber;

        if (itr >= '0' && itr <= '9') {
            customerNumber =  (itr - '0' + mNumberIndexStart);
        } else {
            customerNumber =  (itr - 'a' + mAlphabetIndexStart);
        }

        if(encodedBookingId.length() == Constants.MAX_PARK_ID_SUFFIX_LEN){
            itr = (byte)encodedBookingId.charAt(1);
            if (itr >= '0' && itr <= '9') {
                customerNumber += (itr - '0' + mNumberIndexStart);
            } else {
                customerNumber += (itr - 'a' + mAlphabetIndexStart);
            }

        }

        customerNumber += 1;



        String spaceId = ((Integer)Cache.getFromCache(context,
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT)).toString();

        String ownerId = ((Integer)Cache.getFromCache(context,
                Constants.CACHE_PREFIX_SPACE_OWNER_ID_QUERY,
                Constants.TYPE_INT)).toString();

        return "SP_"+ spaceId + "_" + ownerId + "_" + customerNumber;

    }


    public static void incrementMonthlyPassCounters(Context context, boolean isActiveToday){
        mCacheMonthlyPassLock.lock();

        try {
            Cache.setInCache(context, Constants.CACHE_PREFIX_IS_MONTHLY_PASS_GENERATED,
                    true, Constants.TYPE_BOOL);

            int numTotalPasses = (Integer) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_TOTAL_MONTHLY_PASSES,
                    Constants.TYPE_INT);

            Cache.setInCache(context, Constants.CACHE_PREFIX_TOTAL_MONTHLY_PASSES, ++numTotalPasses,
                    Constants.TYPE_INT);

            int passesGeneratedToday = (Integer) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_MONTHLY_PASSES_ADDED_TODAY,
                    Constants.TYPE_INT);
            Cache.setInCache(context, Constants.CACHE_PREFIX_MONTHLY_PASSES_ADDED_TODAY,
                    ++passesGeneratedToday, Constants.TYPE_INT);

            if(isActiveToday){
                int activeToday = (Integer) Cache.getFromCache(context,
                        Constants.CACHE_PREFIX_MONTHLY_PASSES_ACTIVE_TODAY,
                        Constants.TYPE_INT);

                Cache.setInCache(context,Constants.CACHE_PREFIX_MONTHLY_PASSES_ACTIVE_TODAY,
                        ++activeToday,Constants.TYPE_INT);
            }

        }finally {
            mCacheMonthlyPassLock.unlock();
        }

    }

    public static void decrementMonthlyPassCounters(Context context, boolean isDeActiveToday){
        mCacheMonthlyPassLock.lock();

        try {

            int numTotalPasses = (Integer) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_TOTAL_MONTHLY_PASSES,
                    Constants.TYPE_INT);

            Cache.setInCache(context, Constants.CACHE_PREFIX_TOTAL_MONTHLY_PASSES, --numTotalPasses,
                    Constants.TYPE_INT);

            if(numTotalPasses == 0){
                Cache.setInCache(context, Constants.CACHE_PREFIX_IS_MONTHLY_PASS_GENERATED,
                        false, Constants.TYPE_BOOL);

            }

            if(isDeActiveToday){
                int activeToday = (Integer) Cache.getFromCache(context,
                        Constants.CACHE_PREFIX_MONTHLY_PASSES_ACTIVE_TODAY,
                        Constants.TYPE_INT);

                Cache.setInCache(context,Constants.CACHE_PREFIX_MONTHLY_PASSES_ACTIVE_TODAY,
                        --activeToday,Constants.TYPE_INT);
            }

        }finally {
            mCacheMonthlyPassLock.unlock();
        }

    }

    public static void resetActiveMonthlyPassesForToday(Context context){
        mCacheMonthlyPassLock.lock();

        try {
            Cache.setInCache(context,Constants.CACHE_PREFIX_MONTHLY_PASSES_ACTIVE_TODAY,0,
                             Constants.TYPE_INT);
        }finally{
            mCacheMonthlyPassLock.unlock();

        }


    }

    public static void setActiveMonthlyPassesForToday(Context context,int numActivePasses){
        mCacheMonthlyPassLock.lock();

        try {
            Cache.setInCache(context,Constants.CACHE_PREFIX_MONTHLY_PASSES_ACTIVE_TODAY,
                             numActivePasses,
                             Constants.TYPE_INT);
        }finally{
            mCacheMonthlyPassLock.unlock();

        }


    }


    public static void setInActiveMonthlyPasses(Context context, int passesExpired){
        mCacheMonthlyPassLock.lock();

        try {

            int numTotalPasses = (Integer) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_TOTAL_MONTHLY_PASSES,
                    Constants.TYPE_INT);

            numTotalPasses = numTotalPasses - passesExpired;

            Cache.setInCache(context, Constants.CACHE_PREFIX_TOTAL_MONTHLY_PASSES, numTotalPasses,
                    Constants.TYPE_INT);

            if(numTotalPasses == 0){
                Cache.setInCache(context, Constants.CACHE_PREFIX_IS_MONTHLY_PASS_GENERATED,
                        false, Constants.TYPE_BOOL);

            }
        }finally {
            mCacheMonthlyPassLock.unlock();
        }


    }

    public static void setCustomFormats(Context context,String[] customStrings){
        mCacheCustomFormatLock.lock();

        try {

            for(int idx=0 ; idx < customStrings.length; idx++){
                if(customStrings[idx] == null || customStrings[idx].isEmpty()){
                    continue;
                }
                switch(idx){
                    case Constants.CUSTOM_SPACE_ADDRESS_IDX:{
                        Cache.setInCache(context,Constants.CACHE_PREFIX_CUSTOM_SPACE_ADDRESS,
                                   customStrings[idx],Constants.TYPE_STRING);
                    }
                    break;
                    case Constants.CUSTOM_SPACE_HEADING_IDX:{
                        Cache.setInCache(context,Constants.CACHE_PREFIX_CUSTOM_SPACE_HEADING,
                                customStrings[idx],Constants.TYPE_STRING);
                    }
                    break;
                    case Constants.CUSTOM_AGENCY_NAME_IDX:{
                        Cache.setInCache(context,Constants.CACHE_PREFIX_CUSTOM_AGENCY_NAME,
                                customStrings[idx],Constants.TYPE_STRING);
                    }
                    break;
                    case Constants.CUSTOM_SPACE_OWNER_MOBILE_IDX:{
                        Cache.setInCache(context,Constants.CACHE_PREFIX_CUSTOM_SPACE_OWNER_MOBILE,
                                customStrings[idx],Constants.TYPE_STRING);
                    }
                    break;
                    case Constants.CUSTOM_OPERATING_HOURS_IDX:{
                        Cache.setInCache(context,Constants.CACHE_PREFIX_CUSTOM_OPERATING_HOURS,
                                customStrings[idx],Constants.TYPE_STRING);
                    }
                    break;
                    case Constants.CUSTOM_PARKING_RATES_IDX:{
                        Cache.setInCache(context,Constants.CACHE_PREFIX_CUSTOM_PARKING_RATES,
                                customStrings[idx],Constants.TYPE_STRING);
                    }
                    break;
                    case Constants.CUSTOM_BIKE_PARKING_RATES_IDX:{
                        Cache.setInCache(context,Constants.CACHE_PREFIX_CUSTOM_BIKE_PARKING_RATES,
                                customStrings[idx],Constants.TYPE_STRING);
                    }
                    break;
                    case Constants.CUSTOM_NIGHT_CHARGES_IDX:{
                        Cache.setInCache(context,Constants.CACHE_PREFIX_CUSTOM_NIGHT_CHARGES,
                                customStrings[idx],Constants.TYPE_STRING);
                    }
                    break;
                    case Constants.CUSTOM_BIKE_NIGHT_CHARGES_IDX:{
                        Cache.setInCache(context,Constants.CACHE_PREFIX_CUSTOM_BIKE_NIGHT_CHARGES,
                                customStrings[idx],Constants.TYPE_STRING);
                    }
                    break;
                    case Constants.CUSTOM_LOST_TICKET_IDX:{
                        Cache.setInCache(context,Constants.CACHE_PREFIX_CUSTOM_LOST_TICKET,
                                customStrings[idx],Constants.TYPE_STRING);
                    }
                    break;
                    case Constants.CUSTOM_URL_IDX:{
                        Cache.setInCache(context,Constants.CACHE_PREFIX_CUSTOM_URL,
                                customStrings[idx],Constants.TYPE_STRING);
                    }
                    break;
                    case Constants.CUSTOM_DESC_IDX:{
                        Cache.setInCache(context,Constants.CACHE_PREFIX_CUSTOM_DESC,
                                customStrings[idx],Constants.TYPE_STRING);
                    }
                    break;
                    case Constants.CUSTOM_AD_TEXT_IDX:{
                        Cache.setInCache(context,Constants.CACHE_PREFIX_CUSTOM_AD_TEXT,
                                customStrings[idx],Constants.TYPE_STRING);
                    }
                    break;
                    case Constants.CUSTOM_SPARK_AD_IDX:{
                        Cache.setInCache(context,Constants.CACHE_PREFIX_CUSTOM_EXIT_SIMPLYPARK_AD_TEXT,
                                customStrings[idx],Constants.TYPE_STRING);
                    }
                    break;


                }
            }


        }finally {
            mCacheCustomFormatLock.unlock();

        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public static void showProgress(Context context, final boolean show,final View progressView,
                                    final View viewToHide) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = context.getResources().getInteger(
                    android.R.integer.config_shortAnimTime);

            viewToHide.setVisibility(show ? View.GONE : View.VISIBLE);
            viewToHide.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    viewToHide.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            viewToHide.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public static boolean checkForConnectivity(Context context){
        if(SystemNotificationsReceiver.isConnected(context)){
            return true;
        }else{
            return false;
        }
    }
/* Space Id is required at many places in the app, therefore the below function is to support all such cases*/
    public static  int getSpaceId(Context context){
        return (int)Cache.getFromCache(context,
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT);
    }

/* Operator Id is required at many places in the app, therefore the below function is to support all such cases*/

    public static int getOperatorId(Context context){
        return (int)Cache.getFromCache(context,
                Constants.CACHE_PREFIX_OPERATOR_QUERY,Constants.TYPE_INT);

    }

    public static int getmUserId(Context context){
        return (int)Cache.getFromCache(context,
                Constants.CACHE_PREFIX_SPACE_OWNER_ID_QUERY,
                Constants.TYPE_INT);
    }





}
