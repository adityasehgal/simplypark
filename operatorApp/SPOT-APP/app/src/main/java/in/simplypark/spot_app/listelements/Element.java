package in.simplypark.spot_app.listelements;


import in.simplypark.spot_app.config.Constants;

/**
 * Created by aditya on 26/05/16.
 */
public class Element {
        private int    mKey;
        private int    mType;
        private int    mAlign;
        private int    mSize;
        private int    mFont;
        private int    mFontAttribute;
        private int    mStyle;



        public Element(int key,int type,int align, int size){
            mKey = key;mType = type; mAlign = align; mSize = size;
            mFont = Constants.PRINTER_FONT_BASE;
            mFontAttribute = Constants.PRINTER_WIDTH_HEIGHT_NORMAL;
            mStyle = Constants.PRINTER_STYLE_NORMAL;
        }


        public Element(int key,int type,int align, int size,int style){
            mKey = key;mType = type; mAlign = align; mSize = size;
            mFont = Constants.PRINTER_FONT_BASE;
            mFontAttribute = Constants.PRINTER_WIDTH_HEIGHT_NORMAL;

            mStyle = style;

        }


        public Element(int key, int type,int align, int size, int font,boolean isWidthDouble,
                boolean isHeightDouble){
            mKey = key; mType = type; mAlign = align; mSize = size; mFont = font;
            mFontAttribute = (isHeightDouble && isWidthDouble)?
                             Constants.PRINTER_WIDTH_HEIGHT_DOUBLE:
                             isHeightDouble?Constants.PRINTER_HEIGHT_DOUBLE:
                                     Constants.PRINTER_WIDTH_HEIGHT_NORMAL;
            mStyle = Constants.PRINTER_STYLE_NORMAL;
            mFont = Constants.PRINTER_FONT_BASE;

        }

        public Element(int key, int type,int align, int size,boolean isHeightWidthDouble){

            mKey = key; mType = type; mAlign = align; mSize = size;
            mFont = Constants.PRINTER_FONT_BASE;
            mFontAttribute = (isHeightWidthDouble)?Constants.PRINTER_WIDTH_HEIGHT_DOUBLE:
                Constants.PRINTER_WIDTH_HEIGHT_NORMAL;
            mStyle = Constants.PRINTER_STYLE_NORMAL;

        }


        public int getKey(){return mKey;}
        public int getType(){return mType;}
        public int getAlignment(){return mAlign; }
        public int getSize(){return mSize;}
        public int getFont(){return mFont;}
        public int getFontAttribute(){return mFontAttribute;}
        public int getStyle(){ return mStyle;}


}
