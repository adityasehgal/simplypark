package in.simplypark.spot_app.utils;

/**
 * Created by root on 11/10/16.
 */
public class Mate2InchFormatter extends Formatter {
    private int MAX_BITS_IN_BYTE = 8;
    private int ESCAPE = 27;
    private int EXCLAMATION = 33;
    private int CHARACTER_E = 69;
    private int HORIZONTAL_TAB = 9;

    private int CHARACTER_FONT_A_SELECTED = 0;
    private int CHARACTER_FONT_B_SELECTED = 1;
    private int RESERVED = 0;
    private int OFF = 0;
    private int EMPHASIS_MODE_SELECTED = 8;
    private int DOUBLE_HEIGHT_MODE_SELECTED = 16;
    private int DOUBLE_WIDTH_MODE_SELECTED = 32;
    private int UNDERLINE_MODE_SELECTED = 128;
    private int FONT_BIT_POS = 0;
    private int EMPHASIS_BIT_POS = 3;
    private int DOUBLE_HEIGHT_BIT_POS=4;
    private int DOUBLE_WIDTH_BIT_POS=5;
    private int UNDERLINE_BIT_POS=6;


    Mate2InchFormatter(){

    }
    public String font_Emphasized_On(){
        byte[] fEOn = new byte[]{(byte)ESCAPE, (byte)EXCLAMATION, (byte)EMPHASIS_MODE_SELECTED};
        String s = new String(fEOn);
        return s;
    }
    public  String font_Emphasized_Off(){
        byte[] fEOn = new byte[]{(byte)ESCAPE, (byte)EXCLAMATION, (byte)OFF};
        String s = new String(fEOn);
        s += "\n";
        return s;
    }
    public  String font_Double_Height_Width_On() {
        byte[] fDHWOn = new byte[]{(byte)ESCAPE, (byte)EXCLAMATION,
                                   (byte)(DOUBLE_WIDTH_MODE_SELECTED | DOUBLE_HEIGHT_MODE_SELECTED) };
        String s = new String(fDHWOn);
        return s;
    }

    public  String font_Double_Height_Width_Off(){
        byte[] fDHWOn = new byte[]{(byte)ESCAPE, (byte)EXCLAMATION, (byte) OFF };
        String s = new String(fDHWOn);
        s += "\n";
        return s;
    }
    public  String font_Double_Height_On() {
        byte[] fDHWOn = new byte[]{(byte)ESCAPE, (byte)EXCLAMATION,
                (byte) DOUBLE_HEIGHT_MODE_SELECTED };
        String s = new String(fDHWOn);
        return s;
    }
    public  String font_Double_Height_Off(){
        byte[] fDHWOn = new byte[]{(byte)ESCAPE, (byte)EXCLAMATION, (byte) OFF };
        String s = new String(fDHWOn);
        s += "\n";
        return s;
    }

    public  String horizontal_Tab(){
        byte[] hTab = new byte[]{(byte)HORIZONTAL_TAB};
        String s = new String(hTab);
        return s;
    }

    public  String barcode_Code_128_Alpha_Numerics(String value){
        int len = value.length() + 1;
        //System.out.println(len);
        String strHexNumber = Integer.toHexString(len);
        byte[] var10000 = new byte[]{Byte.valueOf(String.valueOf(len)).byteValue()};
        //System.out.println(strHexNumber);
        byte[] rf1 = new byte[]{(byte)27, (byte)90, (byte)50, Byte.parseByte(strHexNumber, 16),
                (byte)80, Byte.parseByte("C", 16)};
        String s = new String(rf1) + value;
        //System.out.println("sdfsdfsd::" + s);
        return s;

    }

    public String font_Courier_10(String value) {
        byte[] rf1 = new byte[]{(byte)ESCAPE, (byte)EXCLAMATION, (byte)CHARACTER_FONT_A_SELECTED};
        String s = new String(rf1);
        return s + value;
    }

    public String font_Courier_19(String value) {
        byte[] rf1 = new byte[]{(byte)ESCAPE, (byte)EXCLAMATION, (byte)CHARACTER_FONT_A_SELECTED};
        String s = new String(rf1);
        return s + value;
    }

    public String font_Courier_20(String value) {
        byte[] rf1 = new byte[]{(byte)ESCAPE, (byte)EXCLAMATION, (byte)CHARACTER_FONT_A_SELECTED};
        String s = new String(rf1);
        return s + value;
    }

    public String font_Courier_24(String value) {
        byte[] rf1 = new byte[]{(byte)ESCAPE, (byte)EXCLAMATION, (byte)CHARACTER_FONT_A_SELECTED};
        String s = new String(rf1);
        return s + value;
    }

    public String font_Courier_25(String value) {
        byte[] rf1 = new byte[]{(byte)ESCAPE, (byte)EXCLAMATION, (byte)CHARACTER_FONT_A_SELECTED};
        String s = new String(rf1);
        return s + value;
    }

    public String font_Courier_27(String value) {
        byte[] rf1 = new byte[]{(byte)ESCAPE, (byte)EXCLAMATION, (byte)CHARACTER_FONT_A_SELECTED};
        String s = new String(rf1);
        return s + value;
    }

    public String font_Courier_29(String value) {
        byte[] rf1 = new byte[]{(byte)ESCAPE, (byte)EXCLAMATION, (byte)CHARACTER_FONT_A_SELECTED};
        String s = new String(rf1);
        return s + value;
    }

    public String font_Courier_32(String value) {
        byte[] rf1 = new byte[]{(byte)ESCAPE, (byte)EXCLAMATION, (byte)CHARACTER_FONT_A_SELECTED};
        String s = new String(rf1);
        return s + value;
    }

    public String font_Courier_34(String value) {
        byte[] rf1 = new byte[]{(byte)ESCAPE, (byte)EXCLAMATION, (byte)CHARACTER_FONT_A_SELECTED};
        String s = new String(rf1);
        return s + value;
    }

    public String font_Courier_38(String value) {
        byte[] rf1 = new byte[]{(byte)ESCAPE, (byte)EXCLAMATION, (byte)CHARACTER_FONT_A_SELECTED};
        String s = new String(rf1);
        return s + value;
    }

    public String font_Courier_42(String value) {
        byte[] rf1 = new byte[]{(byte)ESCAPE, (byte)EXCLAMATION, (byte)CHARACTER_FONT_A_SELECTED};
        String s = new String(rf1);
        return s + value;
    }

    public String font_Courier_48(String value) {
        byte[] rf1 = new byte[]{(byte)ESCAPE, (byte)EXCLAMATION, (byte)CHARACTER_FONT_A_SELECTED};
        String s = new String(rf1);
        return s + value;
    }

    public String font_SansSerif_32(String value) {
        byte[] rf1 = new byte[]{(byte)ESCAPE, (byte)EXCLAMATION, (byte)CHARACTER_FONT_B_SELECTED};
        String s = new String(rf1);
        return s + value;
    }

    public String font_SansSerif_34(String value) {
        byte[] rf1 = new byte[]{(byte)ESCAPE, (byte)EXCLAMATION, (byte)CHARACTER_FONT_B_SELECTED};
        String s = new String(rf1);
        return s + value;
    }

    public String font_SansSerif_38(String value) {
        byte[] rf1 = new byte[]{(byte)ESCAPE, (byte)EXCLAMATION, (byte)CHARACTER_FONT_B_SELECTED};
        String s = new String(rf1);
        return s + value;
    }

    public String font_SansSerif_8(String value) {
        byte[] rf1 = new byte[]{(byte)ESCAPE, (byte)EXCLAMATION, (byte)CHARACTER_FONT_B_SELECTED};
        String s = new String(rf1);
        return s + value;
    }

    public String carriage_Return() {
        String s = new String("\n");
        return s;
    }
}
