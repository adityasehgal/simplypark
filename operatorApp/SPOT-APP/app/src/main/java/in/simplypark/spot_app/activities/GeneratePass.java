package in.simplypark.spot_app.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import hirondelle.date4j.DateTime;
import in.simplypark.spot_app.R;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.db_tables.PricingPerVehicleTypeTbl;
import in.simplypark.spot_app.db_tables.SimplyParkBookingTbl;
import in.simplypark.spot_app.db_tables.SpotDb;
import in.simplypark.spot_app.printer.PrinterService;
import in.simplypark.spot_app.services.PaymentGatewayIntentService;
import in.simplypark.spot_app.services.ServerUpdationIntentService;
import in.simplypark.spot_app.utils.Cache;
import in.simplypark.spot_app.utils.DateUtil;
import in.simplypark.spot_app.utils.MiscUtils;

import static in.simplypark.spot_app.config.Constants.BUNDLE_PARAM_STAFF_MONTHLY_PASS;
import static in.simplypark.spot_app.config.Constants.VEHICLE_TYPE_CAR;
import static java.lang.Long.getLong;

/**
 * A login screen that offers login via email/password.
 */
public class GeneratePass extends AppCompatActivity implements PaymentResultListener {


    /**
     * Keep track of the generate pass task to ensure we can cancel it if requested.
     */
    private SpaceDetailsTask mSpaceDetailsTask = null;
    private GeneratePassTask  mGeneratePassTask = null;
    private SpotDb mSpotDb = null;

    IntentFilter mStatusIntentFilter;
    PaymentGatewayResponse mResponseReceiver;



    private View mProgressView;
    private View mScrollView;
    private Context mContext;
    private boolean mBookingCreated;

    private DatePickerDialog mFromDatePicker;
    private DatePickerDialog mToDatePicker;
    private String mDate4jFormattedFromDate;
    private String mDate4jFormattedToDate;
    private SimpleDateFormat mDateFormatter;
    private SimpleDateFormat  mDate4jStyleFormatter;
    private EditText mFromDateView;
    private EditText mToDateView;
    private EditText mLostPassFee;
    private View  mErrorView = null; //the view on which validate function failed
    private View mLostPassInputLayout;

    private EditText   mNameView;
    private EditText   mEmailView;
    private EditText   mMobileView;
    private EditText   mVehicleView;
    private Spinner    mVehicleTypeView;
    private EditText   mMonthlyRateView;
    private Button     mPrintPassButtonView;
    private Button     mCancelButtonView;
    private RelativeLayout mStaffDiscountView;
    private Switch mStaffDiscountSwitch;

    private EditText   mPassCodeView;
    private Button     mPassActivationButton;
    private boolean    mIsPassActivationRequired = false;
    private String     mEncodedPassCode;
    private Button     mScanPassButton;
    private boolean    mIsPaymentModeCash = true;




    //form fields
    boolean mIsLostPass;
    String mName;
    String mEmail;
    String mMobile;
    String mFromDate;
    String mToDate;
    String mVehicleReg;
    int    mVehicleType = VEHICLE_TYPE_CAR;
    int    mParkingFees;
    boolean    mStaffDiscountSwitch_Val;
    String mParkingFeesStr;
    int mCarStaffDiscount = 0;
    int    mBikeMonthlyRate = 0;
    int mBikeStaffDiscount = 0;
    int    mMonthlyRate = 0;
    int    mMiniBusMonthlyRate = 0;
    int mMiniBusStaffDiscount = 0;
    int    mBusMonthlyRate = 0;
    int mBusStaffDiscount = 0;
    String mDefaultEmail;
    String mParentBookingId = ""; //used for renewal
    boolean mIsRenewal = false;





    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();

    String randomString( int len ){
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_pass);
        setupActionBar();

        mContext = getApplicationContext();

        // The filter's action is SERVER_UPDATION_BROADCAST_RESULTS ACTION
        mStatusIntentFilter = new IntentFilter(
                Constants.SERVER_UPDATION_PAYMENT_GATEWAY_RESULTS_ACTION);

        mResponseReceiver = new PaymentGatewayResponse();


        mScrollView = findViewById(R.id.agp_scroll_view_id);
        mProgressView = findViewById(R.id.agp_progress_id);
        mNameView = (EditText)findViewById(R.id.agp_name_id);
        mEmailView  = (EditText)findViewById(R.id.agp_email_id);
        mMobileView  = (EditText)findViewById(R.id.agp_mobile_id);
        mVehicleView  = (EditText)findViewById(R.id.agp_vehicle_id);
        mVehicleTypeView  = (Spinner)findViewById(R.id.agp_vehicle_type_id);
        mStaffDiscountView = (RelativeLayout) findViewById(R.id.discount_option_view);
        mStaffDiscountSwitch = (Switch)  findViewById(R.id.staff_discount);
        mStaffDiscountSwitch_Val = mStaffDiscountSwitch.isChecked();
        mVehicleTypeView.setSelection(getIndex(mVehicleTypeView,getString(R.string.car)));


        mMonthlyRateView  = (EditText)findViewById(R.id.agp_monthly_rate_id);
        mPrintPassButtonView  = (Button)findViewById(R.id.agp_print_pass_id);
        mCancelButtonView  = (Button)findViewById(R.id.agp_cancel_id);
        mFromDateView = (EditText)findViewById(R.id.agp_from_date_id);
        mToDateView   = (EditText)findViewById(R.id.agp_to_date_id);
        mLostPassFee   = (EditText)findViewById(R.id.agp_lost_monthly_pass_fee_id);

        mLostPassInputLayout = (View)findViewById(R.id.agp_text_input_lost_pass_fee_id);


        Bundle passedParameters = getIntent().getExtras();
        mDateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);//this is to show to user
        mDate4jStyleFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);//this is to pass to code


        if((Boolean) Cache.getFromCache(mContext,
                Constants.CACHE_PREFIX_IS_CAR_PRICING_AVAILABLE_QUERY,
                Constants.TYPE_BOOL)){
            List<String> existingItems = retrieveAllItems(mVehicleTypeView);
            existingItems.add(getString(R.string.car));
            addItemsToSpinner(existingItems,mVehicleTypeView);

        }

        if((Boolean)Cache.getFromCache(mContext,
                Constants.CACHE_PREFIX_IS_BIKE_PRICING_AVAILABLE_QUERY,
                Constants.TYPE_BOOL)){
            List<String> existingItems = retrieveAllItems(mVehicleTypeView);
            existingItems.add(getString(R.string.bike));
            addItemsToSpinner(existingItems,mVehicleTypeView);
        }

        if((Boolean)Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_IS_MINIBUS_PRICING_AVAILABLE_QUERY,
                Constants.TYPE_BOOL)){
            List<String> existingItems = retrieveAllItems(mVehicleTypeView);
            existingItems.add(getString(R.string.minibus));
            addItemsToSpinner(existingItems,mVehicleTypeView);
        }

        if((Boolean)Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_IS_BUS_PRICING_AVAILABLE_QUERY,
                Constants.TYPE_BOOL)){
            List<String> existingItems = retrieveAllItems(mVehicleTypeView);
            existingItems.add(getString(R.string.bus));
            addItemsToSpinner(existingItems,mVehicleTypeView);
        }

        /*
        * This is added to support the requirement of changing the default vehicle type for different sites...
        *
        */
        String vehicleTypeStr = (String)Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_DEFAULT_VEHICLE_TYPE,
                Constants.TYPE_STRING);
        if(vehicleTypeStr!=null){
            mVehicleTypeView.setSelection(getIndex(mVehicleTypeView,vehicleTypeStr));
        }


        mVehicleTypeView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                // On selecting a spinner item
                String vehicle_type_str = adapter.getItemAtPosition(position).toString();
                mVehicleType = Constants.VEHICLE_TYPE_MAP.get(vehicle_type_str);
                switch (mVehicleType){
                    case VEHICLE_TYPE_CAR:{
                        handleDiscountOnSelect(mCarStaffDiscount,mMonthlyRate);
                    }
                    break;
                    case Constants.VEHICLE_TYPE_BIKE:{
                        handleDiscountOnSelect(mBikeStaffDiscount,mBikeMonthlyRate);
                    }
                    break;
                    case Constants.VEHICLE_TYPE_MINIBUS:{
                        handleDiscountOnSelect(mMiniBusStaffDiscount,mMiniBusMonthlyRate);
                    }
                    break;

                    case Constants.VEHICLE_TYPE_BUS:{
                        handleDiscountOnSelect(mBusStaffDiscount,mBusMonthlyRate);
                    }
                    break;
                }



            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });




        mMonthlyRate = (Integer) Cache.getFromCache(mContext,
                Constants.CACHE_PREFIX_MONTHLY_RATE_QUERY + VEHICLE_TYPE_CAR,
                Constants.TYPE_INT);
        mCarStaffDiscount = (Integer) Cache.getFromCache(mContext,
                Constants.CACHE_PREFIX_STAFF_DISCOUNT_RATE_QUERY + VEHICLE_TYPE_CAR,
                Constants.TYPE_INT);

        mBikeMonthlyRate = (Integer) Cache.getFromCache(mContext,
                Constants.CACHE_PREFIX_MONTHLY_RATE_QUERY + Constants.VEHICLE_TYPE_BIKE,
                Constants.TYPE_INT);

        mBikeStaffDiscount = (Integer) Cache.getFromCache(mContext,
                Constants.CACHE_PREFIX_STAFF_DISCOUNT_RATE_QUERY + Constants.VEHICLE_TYPE_BIKE,
                Constants.TYPE_INT);


        mMiniBusMonthlyRate = (Integer)Cache.getFromCache(mContext,
                Constants.CACHE_PREFIX_MONTHLY_RATE_QUERY + Constants.VEHICLE_TYPE_MINIBUS,
                Constants.TYPE_INT);

        mMiniBusStaffDiscount = (Integer) Cache.getFromCache(mContext,
                Constants.CACHE_PREFIX_STAFF_DISCOUNT_RATE_QUERY + Constants.VEHICLE_TYPE_MINIBUS,
                Constants.TYPE_INT);


        mBusMonthlyRate = (Integer)Cache.getFromCache(mContext,
                Constants.CACHE_PREFIX_MONTHLY_RATE_QUERY + Constants.VEHICLE_TYPE_BUS,
                Constants.TYPE_INT);

        mBusStaffDiscount = (Integer) Cache.getFromCache(mContext,
                Constants.CACHE_PREFIX_STAFF_DISCOUNT_RATE_QUERY + Constants.VEHICLE_TYPE_BUS,
                Constants.TYPE_INT);


        if(passedParameters != null && !passedParameters.isEmpty()) {
            mIsLostPass = passedParameters.getBoolean(Constants.BUNDLE_PARAM_LOST_MONTHLY_PASS);

            mNameView.setText(passedParameters.getString(Constants.BUNDLE_PARAM_MONTHLY_PASS_ISSUED_TO));
            mDefaultEmail = passedParameters.getString(Constants.BUNDLE_PARAM_MONTHLY_PASS_EMAIL);

            mMobileView.setText(passedParameters.getString(Constants.BUNDLE_PARAM_MONTHLY_PASS_TEL));
            mMobileView.setEnabled(false);mMobileView.setInputType(InputType.TYPE_NULL);

            mVehicleView.setText(passedParameters.getString(Constants.BUNDLE_PARAM_MONTHLY_PASS_VEHICLE_REG));
            mVehicleView.setEnabled(false); mVehicleView.setInputType(InputType.TYPE_NULL);

            int vehicleType =
                    passedParameters.getInt(Constants.BUNDLE_PARAM_MONTHLY_PASS_VEHICLE_TYPE,
                                                  VEHICLE_TYPE_CAR);

            int fees = getmMonthlyRate(vehicleType);//passedParameters.getInt(Constants.BUNDLE_PARAM_MONTHLY_PASS_PER_MONTH_FEES,0);
            mMonthlyRateView.setText(Integer.toString(fees));

            Long passValidFromLong = passedParameters.getLong(Constants.BUNDLE_PARAM_MONTHLY_PASS_VALID_FROM);
            Long passValidTillLong = passedParameters.getLong(Constants.BUNDLE_PARAM_MONTHLY_PASS_VALID_TILL);



            if(vehicleType == VEHICLE_TYPE_CAR){
                mVehicleTypeView.setSelection(getIndex(mVehicleTypeView, getString(R.string.car)));
//                mMonthlyRate = fees;
            }else if(vehicleType == Constants.VEHICLE_TYPE_BIKE){
                mVehicleTypeView.setSelection(getIndex(mVehicleTypeView, getString(R.string.bike)));
//                mBikeMonthlyRate = fees;
            }else if(vehicleType == Constants.VEHICLE_TYPE_MINIBUS){
                mVehicleTypeView.setSelection(getIndex(mVehicleTypeView, getString(R.string.minibus)));
//                mMiniBusMonthlyRate = fees;
            }else if(vehicleType == Constants.VEHICLE_TYPE_BUS){
                mVehicleTypeView.setSelection(getIndex(mVehicleTypeView, getString(R.string.bus)));
//                mBusMonthlyRate = fees;
            }
            if(passedParameters.getBoolean(BUNDLE_PARAM_STAFF_MONTHLY_PASS,false)){
                mCarStaffDiscount = (Integer) Cache.getFromCache(mContext,
                        Constants.CACHE_PREFIX_STAFF_DISCOUNT_RATE_QUERY + vehicleType,
                        Constants.TYPE_INT);
                mMonthlyRate = (Integer) Cache.getFromCache(mContext,
                        Constants.CACHE_PREFIX_MONTHLY_RATE_QUERY + vehicleType,
                        Constants.TYPE_INT);

                mStaffDiscountSwitch.setChecked(true);

                handleDiscountOnSelect(mCarStaffDiscount,mMonthlyRate);
            }


            mVehicleTypeView.setEnabled(false);




            if(mIsLostPass){
                mLostPassInputLayout.setVisibility(View.VISIBLE);
                mFromDateView.setText(mDateFormatter.format(passValidFromLong));
                mToDateView.setText(mDateFormatter.format(passValidTillLong));
                mDate4jFormattedFromDate = mDate4jStyleFormatter.format(passValidFromLong);
                mDate4jFormattedToDate = mDate4jStyleFormatter.format(passValidTillLong);

            }
            long nextRenewalDate = getLong(Constants.BUNDLE_PARAM_MONTHLY_PASS_NEXT_RENEWAL_DATETIME,0);
            long to = getLong(Constants.BUNDLE_PARAM_MONTHLY_PASS_VALID_TILL,0);

            if(passValidTillLong == 0){
                setDateTimeField(passValidFromLong);
            }else{
                setDateTimeField(passValidTillLong);
            }


            mParentBookingId = passedParameters.getString(Constants.BUNDLE_PARAM_MONTHLY_PASS_BOOKING_ID);



        }else {


            mDefaultEmail = (String) Cache.getFromCache(mContext,
                    Constants.CACHE_PREFIX_SPACE_OWNER_EMAIL_QUERY,
                    Constants.TYPE_STRING);

            //bus and minibus pricing if available will always be set
            if (mMonthlyRate == 0 || mBikeMonthlyRate == 0 || mDefaultEmail == "") {
                showProgress(true);
                mSpaceDetailsTask = new SpaceDetailsTask();
                mSpaceDetailsTask.execute((Void) null);
            } else {
                mMonthlyRateView.setText(Integer.toString(mMonthlyRate));
            }

            setDateTimeField(DateTime.today(Constants.TIME_ZONE).
                                   getMilliseconds(Constants.TIME_ZONE));
        }

        mIsRenewal = (mParentBookingId.isEmpty())?false:true;

        mPassCodeView = (EditText)findViewById(R.id.agp_encoded_pass_code_id);
        mPassActivationButton = (Button)findViewById(R.id.agp_scan_pass_id);

        if(!((Boolean)Cache.getFromCache(mContext,Constants.CACHE_PREFIX_IS_PASS_ACTIVATION_REQUIRED,
                                       Constants.TYPE_BOOL))){
            mPassCodeView.setVisibility(View.GONE);
            mPassActivationButton.setVisibility(View.GONE);
            mPrintPassButtonView.setText(R.string.print_pass);
        }else{
            mIsPassActivationRequired = true;
            mPrintPassButtonView.setText(R.string.issue_pass);
            mPassCodeView.setFocusable(false);
            if(mIsRenewal){
                //if its renewal, don't give the operator the option to scan a pass.
                mPassCodeView.setText(
                        passedParameters.getString(Constants.BUNDLE_PARAM_ENCODE_PASS_CODE));
                mPassActivationButton.setVisibility(View.INVISIBLE);
            }

        }


    }

    @Override
    protected void onStart(){
        super.onStart();

        LocalBroadcastManager.getInstance(this).registerReceiver(
                mResponseReceiver,
                mStatusIntentFilter);

    }

    @Override
    protected void onStop(){
        super.onStop();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mResponseReceiver);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                deleteBookingId();
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch(keyCode){
            case KeyEvent.KEYCODE_BACK:
                // do something here
                deleteBookingId();
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    // add items into spinner dynamically
    public void addItemsToSpinner(List<String> itemList, Spinner spinner) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, itemList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }
    // get the position of an item by value
    private int getIndex(Spinner spinner, String myString)
    {
        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                index = i;
                break;
            }
        }
        return index;
    }

    public List<String> retrieveAllItems(Spinner spinner) {
        Adapter adapter = spinner.getAdapter();
        int n = adapter.getCount();
        List<String> items = new ArrayList<String>(n);
        for (int i = 0; i < n; i++) {
            String item = (String) adapter.getItem(i);
            items.add(item);
        }
        return items;
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // Show the Up button in the action bar.
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
    }


    public void handleFromDateOnClick(View view){
        mFromDatePicker.show();

    }

    public void handleToDateOnClick(View view){
        if(mFromDateView.getText().toString().length() != 0) {
            DateTime temp = new DateTime(mDate4jFormattedFromDate);
            DateTime newTo = temp.plus(0,1,0,0,0,0,0,DateTime.DayOverflow.LastDay);
            //mToDatePicker.getDatePicker().setMinDate(newTo.getMilliseconds(Constants.TIME_ZONE));
            mToDatePicker.updateDate(newTo.getYear(), newTo.getMonth() - 1, newTo.getDay());

        }
        mToDatePicker.show();

    }


    public void handleScanPassButton(View view){
        //Create a new Intent to scan QR Code
        Intent scanActivity = new Intent(Constants.ACTIVATE_MONTHLY_PASS_QR_CODE_SCAN);

        try {
            startActivityForResult(scanActivity,
                    Constants.ACTIVATE_MONTHLY_PASS_SCANNING_REQUEST_CODE);

        } catch (ActivityNotFoundException activity) {
            String errorMsg = "Appropriate App(QrCode Private) is NOT installed. Please enter the" +
                              "pass scan value manually";
            MiscUtils.displayNotification(mContext,errorMsg,true);
        }

    }

    @Override
    /**
     * Reads data scanned by user and returned by QR Droid
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (Constants.ACTIVATE_MONTHLY_PASS_SCANNING_REQUEST_CODE == requestCode &&
                null != data && data.getExtras() != null) {
            //Read result from QR Droid (it's stored in la.droid.qr.result)
            String result = data.getExtras().getString(Constants.ACTIVATE_MONTHLY_PASS_SCANNING_RESULT);

            if (resultCode != RESULT_OK || null == result || result.length() == 0) {
                String errorMsg = "Appropriate App(QrCode Private) is NOT installed. Please enter the" +
                        "pass scan value manually";
                mPassCodeView.setError(errorMsg);
                mPassCodeView.requestFocus();

            }else{
                Toast.makeText(mContext,"Pass Scan successful",Toast.LENGTH_SHORT);
                mPassCodeView.setText(result);
            }


        }
    }


    private void resetFields(){
        mNameView.setText("");
        mEmailView.setText("");
        mMobileView.setText("");
        mFromDateView.setText("");
        mToDateView.setText("");
        mVehicleView.setText("");
        mMonthlyRateView.setText("");
        mPassCodeView.setText("");

        mNameView.requestFocus();
    }

    public void handlePrintPassButton(View view){

        mName     = mNameView.getText().toString();
        mEmail    = mEmailView.getText().toString();
        if(mEmail.isEmpty()){
            if(!mDefaultEmail.isEmpty()){
                mEmail = mDefaultEmail;
            }else{
                mEmail = "monthlyuser@simplypark.in"; //parking operators are not filling email
                                                      //This is causing lot of fake users on the
                                                      //the server
            }

        }
        mMobile   = mMobileView.getText().toString();
        mFromDate = mFromDateView.getText().toString();
        mToDate   = mToDateView.getText().toString();
        mVehicleReg = mVehicleView.getText().toString();
        mParkingFeesStr = mMonthlyRateView.getText().toString();


        if(mIsPassActivationRequired){
            mEncodedPassCode = mPassCodeView.getText().toString();
        }

        if(validate()){

            showProgress(true);
            mGeneratePassTask = new GeneratePassTask();
            mGeneratePassTask.execute((Void) null);

        }else{
            if(mErrorView != null) {
                mErrorView.requestFocus();
            }
        }


    }

    public void handleCancelButton(View view){
        deleteBookingId();
        finish();
    }

    @Override
    public void onBackPressed(){
        // do something here and don't write super.onBackPressed()
        super.onBackPressed();
        deleteBookingId();
    }


    private void deleteBookingId(){
        String bookingId = (String)Cache.getFromCache(mContext,
                Constants.CACHE_PREFIX_PASS_BOOKING_ID,Constants.TYPE_STRING);

        if(!bookingId.isEmpty()){
            //reset
            Cache.setInCache(mContext,Constants.CACHE_PREFIX_INVOICE_ID,"",Constants.TYPE_STRING);
            Cache.setInCache(mContext, Constants.CACHE_PREFIX_PASS_BOOKING_ID, "", Constants.TYPE_STRING);
            PaymentGatewayIntentService.startActionFailurePaymentResult(mContext,bookingId);
        }

    }

    private void setDateTimeField(long startDateTime) {

        //DateTime today = DateTime.today(Constants.TIME_ZONE);
        DateTime today = DateTime.forInstant(startDateTime, Constants.TIME_ZONE);
        mFromDatePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                //Calendar newDate = Calendar.getInstance();
                //newDate.set(year, monthOfYear, dayOfMonth);
                DateTime newDate = new DateTime(year, monthOfYear + 1, dayOfMonth,23,59,0,0);
                long newDateInMs = newDate.getMilliseconds(Constants.TIME_ZONE);
                mFromDateView.setText(mDateFormatter.format(newDateInMs));

                mDate4jFormattedFromDate = mDate4jStyleFormatter.format(newDateInMs);


                DateTime toMinDate = newDate.plus(0,1,0,0,0,0,0,DateTime.DayOverflow.LastDay);
                //mToDatePicker.getDatePicker().setMinDate(toMinDate.getMilliseconds(Constants.TIME_ZONE));
                //mToDatePicker.updateDate(toMinDate.getYear(), toMinDate.getMonth() - 1, toMinDate.getDay());

                if(mToDateView.getText().toString().length() != 0){
                    mToDateView.setText(null);
                }
            }

        },today.getYear(), today.getMonth() -1,today.getDay());


        //allow 3 months in advance

        DateTime fromMaxDate = today.plus(0,3,0,0,0,0,0,DateTime.DayOverflow.LastDay);
        //mFromDatePicker.getDatePicker().setMinDate(today.getMilliseconds(Constants.TIME_ZONE));
        mFromDatePicker.getDatePicker().setMaxDate(fromMaxDate.getMilliseconds(Constants.TIME_ZONE));


        mToDatePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                DateTime newDate = new DateTime(year, monthOfYear + 1, dayOfMonth,23,59,0,0);
                long newDateInMs = newDate.getMilliseconds(Constants.TIME_ZONE);
                mToDateView.setText(mDateFormatter.format(newDateInMs));
                mDate4jFormattedToDate = mDate4jStyleFormatter.format(newDateInMs);

            }



        },today.getYear(),today.getMonth() -1,today.getDay());

        DateTime toMaxDate = today.plus(1,1,0,0,0,0,0,DateTime.DayOverflow.LastDay);
        DateTime toMinDate = today.plus(0,1,0,0,0,0,0,DateTime.DayOverflow.LastDay);

        //mToDatePicker.getDatePicker().setMinDate(toMinDate.getMilliseconds(Constants.TIME_ZONE));
        mToDatePicker.getDatePicker().setMaxDate(toMaxDate.getMilliseconds(Constants.TIME_ZONE));

    }
    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private boolean validate() {

        // Reset errors.
        mNameView.setError(null);
        mEmailView.setError(null);
        mMobileView.setError(null);
        mVehicleView.setError(null);
        mMonthlyRateView.setError(null);

        if(mIsPassActivationRequired){
            mPassCodeView.setError(null);
        }


        mErrorView = null;



        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(mName)) {
            mNameView.setError(getString(R.string.error_field_required));
            mErrorView = mNameView;
            return false;
        }

        // Check for a valid email address.
        /*if (TextUtils.isEmpty(mEmail)) {
            mEmailView.setError(getString(R.string.error_field_required));
            mErrorView = mEmailView;
            return false;
        } else*/ if (!TextUtils.isEmpty(mEmail) && !isEmailValid(mEmail)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            mErrorView = mEmailView;
            return false;
        }

        //check for a valid mobile
        if(TextUtils.isEmpty(mMobile)){
            mMobileView.setError("This field is required");
            mErrorView = mMobileView;
            return false;
        }else if(!isMobileNumberValid(mMobile)){
            mMobileView.setError("This mobile number is invalid");
            mErrorView = mMobileView;
            return false;
        }

        //check for a valid vehicle Reg
        if(TextUtils.isEmpty(mVehicleReg)){
            mVehicleView.setError("This field is required");
            mErrorView = mVehicleView;
            return false;
        }else if(!isVehicleRegValid(mVehicleReg)){
            mVehicleView.setError("Please enter atleast 4 digits for Vehicle Number");
            mErrorView = mVehicleView;
            return false;
        }

        //check for vlaid parkign fees
        if(TextUtils.isEmpty(mParkingFeesStr)){
            mMonthlyRateView.setError("This field is required");
            mErrorView = mMonthlyRateView;
            return false;
        }else if(!isPriceValid(mParkingFeesStr)){
            mVehicleView.setError("Invaid Parking Fees entered");
            mErrorView = mMonthlyRateView;
            return false;
        }

        if(mFromDate.isEmpty()){
            mFromDateView.setError("This field is required");
            mErrorView = mFromDateView;
            return false;
        }

        if(mToDate.isEmpty()){
            mToDateView.setError("This field is required");
            mErrorView = mToDateView;
            return false;
        }

        if(!areDatesValid(mDate4jFormattedFromDate, mDate4jFormattedToDate)){
            mFromDateView.setError("Invalid Dates");
            mErrorView = mFromDateView;
            return false;
        }

        if(mIsPassActivationRequired && mEncodedPassCode.isEmpty()){
            //mPassCodeView.setError("Please activate the pass first. ");
            //mMonthlyRateView.setError("Please activate the pass first.");
            //mErrorView = mMonthlyRateView;
            MiscUtils.displayNotification(mContext, "Please activate the pass first.", true);
            return false;
        }



        return true;
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isVehicleRegValid(String vehicleReg){

        return (vehicleReg.length() < 4)?false:true;
    }

    private boolean areDatesValid(String fromDate, String toDate){
        DateTime from = new DateTime(fromDate);
        DateTime to   = new DateTime(toDate);
        DateTime now  = DateTime.today(Constants.TIME_ZONE);

        if(to.lt(from)){
            return false;
        }

      /*  if(from.lt(now) || to.lt(now)){
            return false;
        } */

        return true;
    }


    private boolean isMobileNumberValid(String mobile) {
        String mobileRegex = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";
        return mobile.matches(mobileRegex);

    }

    private boolean isPriceValid(String price){
        boolean isParsed = true;
        try{
            mParkingFees = Integer.parseInt(price);
        }catch(NumberFormatException e){
            isParsed = false;
            mParkingFees = 0;
        }finally {
            return isParsed;
        }

    }

    private String createBookingId(){

        int operatorId = (Integer)Cache.getFromCache(mContext,
                                                     Constants.CACHE_PREFIX_OPERATOR_QUERY,
                                                     Constants.TYPE_INT);

        String bookingId = "app_"+operatorId+randomString(Constants.BOOKING_ID_LENGTH);

        return bookingId;
    }

    public void handlePaymentModeRadioButton(View view){
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        String buttonText = "";

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_cash:
                if (checked){
                    if(mIsPassActivationRequired){
                        buttonText = "Issue Pass";
                    }else{
                        buttonText = "Print Pass";
                    }
                    mIsPaymentModeCash = true;
                }
              break;
            case R.id.radio_card:
                if (checked){
                    buttonText = "Take Payment";
                    mIsPaymentModeCash = false;
                }
                break;
        }

        mPrintPassButtonView.setText(buttonText);
    }





    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mScrollView.setVisibility(show ? View.GONE : View.VISIBLE);
            mScrollView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mScrollView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mScrollView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    /**
     * Represents an asynchronous task to get the monthly rates for the space
     * the user.
     */
    public class SpaceDetailsTask extends AsyncTask<Void, Void, Boolean> {

        PricingPerVehicleTypeTbl mCarPricingObj;
        PricingPerVehicleTypeTbl mBikePricingObj;
        PricingPerVehicleTypeTbl mMiniBusPricingObj;
        PricingPerVehicleTypeTbl mBusPricingObj;

        SpaceDetailsTask() {

        }

        @Override
        protected Boolean doInBackground(Void... params) {

            int spaceId = (Integer)Cache.getFromCache(mContext,
                                             Constants.CACHE_PREFIX_SPACEID_QUERY,
                                             Constants.TYPE_INT);
            List<SpotDb> spaces = SpotDb.find(SpotDb.class, "m_space_id = ?",
                                               Integer.toString(spaceId));

            if(spaces.isEmpty()){
                return false;
            }else {
                mSpotDb = spaces.get(0);
                mCarPricingObj = mSpotDb.getPricing(VEHICLE_TYPE_CAR);
                mBikePricingObj = mSpotDb.getPricing(Constants.VEHICLE_TYPE_BIKE);
                mMiniBusPricingObj = mSpotDb.getPricing(Constants.VEHICLE_TYPE_MINIBUS);
                mBusPricingObj = mSpotDb.getPricing(Constants.VEHICLE_TYPE_BUS);

                return true;
            }

        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mSpaceDetailsTask = null;
            showProgress(false);

            if (success) {
                if(mCarPricingObj != null) {
                    mMonthlyRate = (int) Math.ceil(mCarPricingObj.getMonthlyPrice());
                    Cache.setInCache(mContext, Constants.CACHE_PREFIX_MONTHLY_RATE_QUERY +
                                               VEHICLE_TYPE_CAR, mMonthlyRate,
                                               Constants.TYPE_INT);
                }

                if(mBikePricingObj != null) {
                    mBikeMonthlyRate = (int) Math.ceil(mBikePricingObj.getMonthlyPrice());
                    Cache.setInCache(mContext, Constants.CACHE_PREFIX_MONTHLY_RATE_QUERY +
                                    Constants.VEHICLE_TYPE_BIKE, mBikeMonthlyRate,
                                    Constants.TYPE_INT);

                }

                if(mMiniBusPricingObj != null){
                    mMiniBusMonthlyRate = (int) Math.ceil(mMiniBusPricingObj.getMonthlyPrice());
                    Cache.setInCache(mContext, Constants.CACHE_PREFIX_MONTHLY_RATE_QUERY +
                                    Constants.VEHICLE_TYPE_MINIBUS, mMiniBusMonthlyRate,
                                    Constants.TYPE_INT);
                }

                if(mBusPricingObj != null){
                    mBusMonthlyRate = (int) Math.ceil(mBusPricingObj.getMonthlyPrice());
                    Cache.setInCache(mContext, Constants.CACHE_PREFIX_MONTHLY_RATE_QUERY +
                                    Constants.VEHICLE_TYPE_BUS, mBusMonthlyRate,
                                    Constants.TYPE_INT);
                }


                mMonthlyRateView.setText(Integer.toString(mMonthlyRate));


                Cache.setInCache(mContext,Constants.CACHE_PREFIX_SPACE_OWNER_EMAIL_QUERY,
                        mSpotDb.getOperatorEmail(),Constants.TYPE_STRING);

                //email is no longer mandatory
                //mEmailView.setText(mSpotDb.getOperatorEmail());



            }else {
             mSpotDb = null;
            }
        }

        @Override
        protected void onCancelled() {
            mSpaceDetailsTask = null;
            mSpotDb = null;
            showProgress(false);
        }
    }



    /**
     * Represents an asynchronous task to get the monthly rates for the space
     * the user.
     */
    public class GeneratePassTask extends AsyncTask<Void, Void, Boolean> {

        long fromDate,toDate;
        boolean isPassActiveFromToday = false;
        String mDisplayMsg = "Printing Monthly Pass";



        GeneratePassTask() {

            Boolean is24Hours = (Boolean)Cache.getFromCache(mContext,
                                                            Constants.CACHE_PREFIX_IS_OPEN_24_HOURS,
                                                            Constants.TYPE_BOOL);
            String fromDateStr = mDate4jFormattedFromDate;
            String toDateStr   = mDate4jFormattedToDate;

            fromDateStr += " " + Constants.FULL_DAY_OPENING_TIME;
            toDateStr   += " " + Constants.FULL_DAY_CLOSING_TIME;

            DateTime now = DateTime.now(Constants.TIME_ZONE);
            DateTime fromDateObj = new DateTime(fromDateStr);

            try {
                long nowInMs = now.getMilliseconds(Constants.TIME_ZONE);
                long fromInMs = fromDateObj.getMilliseconds(Constants.TIME_ZONE);

                if(nowInMs <= fromInMs){
                    isPassActiveFromToday = true;
                }
            }catch(Exception e){
                //Log.e("Exception", "Exception Generated " + e);
                MiscUtils.displayNotification(mContext, "An error occurred", true);
                showProgress(false);
            }

            fromDate = fromDateObj.getMilliseconds(Constants.TIME_ZONE);
            toDate   = new DateTime(toDateStr).getMilliseconds(Constants.TIME_ZONE);

        }

        @Override
        protected Boolean doInBackground(Void... params) {


            String[] queryParams = {mEncodedPassCode,
                                    Integer.toString(Constants.BOOKING_STATUS_APPROVED)};

            if(mIsPassActivationRequired && !mIsRenewal){
                List<SimplyParkBookingTbl> bookingList =
                        SimplyParkBookingTbl.find(SimplyParkBookingTbl.class,
                                "m_encoded_pass_code = ? AND m_booking_status = ?",
                                queryParams);

                if(!bookingList.isEmpty()){

                    mDisplayMsg = "Pass is already issued. Please use a different pass";
                    return false;
                }
            }

            SimplyParkBookingTbl booking = new SimplyParkBookingTbl();

            booking.setBookingID(createBookingId());
            booking.setBookingStatus(Constants.BOOKING_STATUS_APPROVED);
            booking.setBookingType(Constants.BOOKING_TYPE_MONTHLY);
            booking.setBookingStartTime(fromDate);
            booking.setBookingEndTime(toDate);
            booking.setOperatorId((Integer)Cache.getFromCache(mContext,Constants.CACHE_PREFIX_OPERATOR_QUERY,
                                  Constants.TYPE_INT));
            if(mIsPassActivationRequired){
                booking.setEncodedPassCode(mEncodedPassCode);
            }else {
                booking.setEncodedPassCode(booking.getBookingID());
                booking.setSerialNumber(booking.getBookingID());
            }
            booking.setCustomerDesc(mName);
            booking.setNumBookedSlots(1);
            booking.setVehicleReg(mVehicleReg);
            booking.setVehicleType(mVehicleType);
            booking.setMobile(mMobile);
            booking.setEmail(mEmail);
            if(mStaffDiscountSwitch_Val)
                booking.setIsStaffPass(true);
            if(mIsPaymentModeCash){
                booking.setPaymentMode(Constants.PAYMENT_MODE_CASH);
            }else{
                booking.setPaymentMode(Constants.PAYMENT_MODE_CARD);
            }

            DateTime today = DateTime.now(Constants.TIME_ZONE);
            booking.setPassIssueDate(today.getMilliseconds(Constants.TIME_ZONE));
            boolean isSlotOccupiedAssumed = (Boolean)Cache.getFromCache(mContext,
                    Constants.CACHE_PREFIX_IS_SLOT_OCCUPIED_ASSUMED_QUERY,Constants.TYPE_BOOL);

            booking.setIsSlotOccupiedAssumed(isSlotOccupiedAssumed);

            if(isSlotOccupiedAssumed && isPassActiveFromToday){
                booking.setSlotsInUse(1);
            }

            booking.setParkingFees(mParkingFees);

            //next installment date
            DateTime fromDate = DateTime.forInstant(booking.getBookingStartTime(),
                                                    Constants.TIME_ZONE);
            String nextInstallmentDateStr = DateUtil.addMonthToDate(fromDate);
            booking.setNextInstallmentTime(new DateTime(nextInstallmentDateStr).
                    getMilliseconds(Constants.TIME_ZONE));
            if(mIsRenewal){
                booking.setIsRenewalBooking(true);
                booking.setParentBookingID(mParentBookingId);

                List<SimplyParkBookingTbl> parentBookingList = SimplyParkBookingTbl.find(
                        SimplyParkBookingTbl.class, "m_booking_id = ?",mParentBookingId);

                if(!mParentBookingId.isEmpty()){
                    SimplyParkBookingTbl parentBooking = parentBookingList.get(0);
                    long nextRenewalDateTime = parentBooking.getNextRenewalDateTime();
                    long bookingEndDateTime  = booking.getBookingEndTime();
                    if(nextRenewalDateTime < bookingEndDateTime){
                        parentBooking.setNextRenewalDateTime(bookingEndDateTime);
                        parentBooking.save();
                    }
                }
            }else{
                booking.setNextRenewalDateTime(booking.getBookingEndTime());
            }

            booking.save();



            //print the monthly pass
            /*PrinterService.startActionPrintGeneratedMonthlyPass(mContext,
                    mVehicleReg, mVehicleType, mName, mEmail, mMobile,
                    mDate4jFormattedFromDate,mDate4jFormattedToDate,
                    booking.getBookingID(),booking.getParkingFees());*/

            if(mIsPaymentModeCash) {

                if (!mIsPassActivationRequired) {
                    PrinterService.startActionPrintGeneratedMonthlyPass(mContext,
                            booking.getVehicleReg(), booking.getVehicleType(), booking.getCustomerDesc(),
                            booking.getEmail(), booking.getMobile(),
                            mDate4jFormattedFromDate, mDate4jFormattedToDate,
                            booking.getBookingID(), booking.getParkingFees());
                }
                //send an update to the simplypark server
                ServerUpdationIntentService.startActionLogMonthlyPassGenerated(mContext,
                        booking);

            }else{

                Cache.setInCache(mContext, Constants.CACHE_PREFIX_PASS_BOOKING_ID,
                        booking.getBookingID(), Constants.TYPE_STRING);
                PaymentGatewayIntentService.startActionCreateInvoice(mContext,booking);


            }


            //print the receipt
            /*PrinterService.startActionPrintMonthlyPassReceipt(mContext,
                    booking.getParkingFees(),booking.getBookingID(),nextInstallmentDateStr);*/






            return true;



        }

        @Override
        protected void onPostExecute(final Boolean success) {

            if(mIsPaymentModeCash) {
                showProgress(false);
                if (success) {
                    if (!mIsPassActivationRequired) {
                        MiscUtils.displayNotification(mContext, mDisplayMsg, false);
                    }

                    resetFields();

                    //update generated monthly pass related cache entries
                    MiscUtils.incrementMonthlyPassCounters(mContext,
                            isPassActiveFromToday);
                    showDialog();
                } else {
                    if(mDisplayMsg.isEmpty()) {
                        MiscUtils.displayNotification(mContext, "An error occurred", true);
                    }else{
                        MiscUtils.displayNotification(mContext, mDisplayMsg, true);
                    }

                }
            }else {
                if (!success) {
                    showProgress(false);
                    MiscUtils.displayNotification(mContext, mDisplayMsg, false);

                }
            }

        }

        @Override
        protected void onCancelled() {

        }
    }
    private void showDialog(){
        AlertDialog alertDialog = new AlertDialog.Builder(GeneratePass.this).create();
        alertDialog.setTitle("Pass Successfully Created");
        //alertDialog.setMessage("Pass Successfully Created");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    /**********************************************************************************************
     * Sub Class to Handle Results of the background activites
     *********************************************************************************************/

    // Broadcast receiver for receiving status updates from the IntentService
    public class PaymentGatewayResponse extends BroadcastReceiver {

        public PaymentGatewayResponse() {

        }

        // Called when the BroadcastReceiver gets an Intent it's registered to receive
        @Override
        public void onReceive(Context context, Intent intent) {
            // Enter relevant functionality for when the last widget is recieved
            final String action = intent.getAction();
            Bundle extras = getIntent().getExtras();
            final int triggerRequest = intent.getIntExtra(Constants.TRIGGER_ACTION_NAME,
                    Constants.TRIGGER_ACTION_NONE);
            final int status = intent.getIntExtra(
                                Constants.SERVER_UPDATION_PAYMENT_GATEWAY_RESULTS_STATUS,
                                Constants.NOK);

            if (action.equals(Constants.SERVER_UPDATION_PAYMENT_GATEWAY_RESULTS_ACTION)) {
                if (status == Constants.OK) {
                    switch (triggerRequest) {
                        case Constants.TRIGGER_ACTION_CREATE_INVOICE:{
                            final String invoiceId = intent.getStringExtra(
                                                      Constants.BUNDLE_PARAM_INVOICE_ID);

                            final String bookingId = intent.getStringExtra(
                                                    Constants.BUNDLE_PARAM_BOOKING_ID);
                            final int amount = intent.getIntExtra(Constants.BUNDLE_PARAM_PARKING_FEES,0);
                            final String amountStr = Integer.toString(amount * 100);

                            startPaymentProcess(invoiceId,bookingId,amountStr);

                        }
                        break;

                        case Constants.TRIGGER_ACTION_COLLECT_PAYMENT_SUCCESSFUL_RESULT:{
                            showProgress(false);
                            SimplyParkBookingTbl booking = new SimplyParkBookingTbl();


                            booking.setVehicleReg(intent.getStringExtra(
                                    Constants.BUNDLE_PARAM_CAR_REG));
                            booking.setVehicleType(intent.getIntExtra(
                                    Constants.BUNDLE_PARAM_VEHICLE_TYPE, VEHICLE_TYPE_CAR));
                            booking.setCustomerDesc(intent.getStringExtra(
                                    Constants.BUNDLE_PARAM_CUSTOMER_DESC));
                            booking.setEmail(intent.getStringExtra(
                                    Constants.BUNDLE_PARAM_MONTHLY_PASS_EMAIL));
                            booking.setMobile(intent.getStringExtra(
                                    Constants.BUNDLE_PARAM_MONTHLY_PASS_TEL));
                            booking.setBookingStartTime(intent.getLongExtra(
                                    Constants.BUNDLE_PARAM_MONTHLY_PASS_VALID_FROM, 0));

                            booking.setBookingEndTime(intent.getLongExtra(
                                    Constants.BUNDLE_PARAM_MONTHLY_PASS_VALID_TILL, 0));

                            booking.setBookingID(intent.getStringExtra(
                                    Constants.BUNDLE_PARAM_BOOKING_ID));

                            booking.setParkingFees(intent.getIntExtra(
                                    Constants.BUNDLE_PARAM_PARKING_FEES, 0));

                            booking.setBookingStatus(intent.getIntExtra(
                                    Constants.BUNDLE_PARAM_BOOKING_STATUS,
                                    Constants.BOOKING_STATUS_APPROVED));
                            booking.setPassIssueDate(intent.getLongExtra(
                                    Constants.BUNDLE_PARAM_MONTHLY_PASS_ISSUE_DATE, 0));

                            booking.setEncodedPassCode(intent.getStringExtra(
                                    Constants.BUNDLE_PARAM_ENCODE_PASS_CODE));
                            booking.setSerialNumber(intent.getStringExtra(
                                    Constants.BUNDLE_PARAM_SERIAL_NUMBER));
                            booking.setNumBookedSlots(intent.getIntExtra(
                                    Constants.BUNDLE_PARAM_NUM_BOOKED_SLOTS, 1));
                            booking.setIsRenewalBooking(intent.getBooleanExtra(
                                    Constants.BUNDLE_PARAM_IS_RENEWAL, false));
                            booking.setParentBookingID(intent.getStringExtra(
                                    Constants.BUNDLE_PARAM_PARENT_BOOKING_ID));

                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                            String fromDate =  dateFormat.format(booking.getBookingStartTime());
                            String toDate = dateFormat.format(booking.getBookingEndTime());

                            if(!((Boolean)Cache.getFromCache(mContext,
                                    Constants.CACHE_PREFIX_IS_PASS_ACTIVATION_REQUIRED,
                                    Constants.TYPE_BOOL))){
                                PrinterService.startActionPrintGeneratedMonthlyPass(mContext,
                                        booking.getVehicleReg(), booking.getVehicleType(),
                                        booking.getCustomerDesc(),booking.getEmail(),
                                        booking.getMobile(),fromDate,toDate,booking.getBookingID(),
                                        booking.getParkingFees());

                            }

                            booking.setPaymentMode(Constants.PAYMENT_MODE_CARD);
                            String invoiceId = intent.getStringExtra(Constants.BUNDLE_PARAM_INVOICE_ID);
                            String paymentId = intent.getStringExtra(Constants.BUNDLE_PARAM_PAYMENT_ID);

                            //send an update to the simplypark server
                            ServerUpdationIntentService.startActionLogMonthlyPassGenerated(mContext,
                                    booking,invoiceId,paymentId);

                            resetFields();
                            boolean isPassActiveFromToday = false;

                            String fromDateStr = fromDate + " " + Constants.FULL_DAY_OPENING_TIME;


                            DateTime now = DateTime.now(Constants.TIME_ZONE);
                            DateTime fromDateObj = new DateTime(fromDateStr);

                            long nowInMs = now.getMilliseconds(Constants.TIME_ZONE);
                            long fromInMs = fromDateObj.getMilliseconds(Constants.TIME_ZONE);

                            if(nowInMs <= fromInMs){
                                isPassActiveFromToday = true;
                            }

                            //update generated monthly pass related cache entries
                            MiscUtils.incrementMonthlyPassCounters(mContext,
                                    isPassActiveFromToday);
                            showDialog();

                        }
                        break;
                        case Constants.TRIGGER_ACTION_COLLECT_PAYMENT_UNSUCCESSFUL_RESULT: {
                            showProgress(false);
                            MiscUtils.displayNotification(mContext,
                                    "Online Payment failed. Customer's card is NOT charged. Please" +
                                            " use other payment methods",true);

                        }
                        break;

                    }
                }else{
                    showProgress(false);
                    switch(triggerRequest){
                        case Constants.TRIGGER_ACTION_CREATE_INVOICE:
                            MiscUtils.displayNotification(mContext,"Online Payment not available",
                                    true);
                            deleteBookingId();
                            break;
                        case Constants.TRIGGER_ACTION_COLLECT_PAYMENT_SUCCESSFUL_RESULT:
                            MiscUtils.displayNotification(mContext,
                                    "Something went wrong. Please contact SimplyPark.in",true);
                            deleteBookingId();
                            break;
                    }
                }
            }

        }
    }




    /************************************************************************
     *   Razor Pay overriden functions
     *
     *
     * @param razorpayPaymentID
     ***********************************************************************/

    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {

        String invoiceId = (String)Cache.getFromCache(mContext,
                Constants.CACHE_PREFIX_INVOICE_ID,Constants.TYPE_STRING);
        String bookingId = (String)Cache.getFromCache(mContext,
                Constants.CACHE_PREFIX_PASS_BOOKING_ID,Constants.TYPE_STRING);

        //reset
        Cache.setInCache(mContext,Constants.CACHE_PREFIX_INVOICE_ID,"",Constants.TYPE_STRING);
        Cache.setInCache(mContext,Constants.CACHE_PREFIX_PASS_BOOKING_ID,"",Constants.TYPE_STRING);

        PaymentGatewayIntentService.startActionSuccessfulPaymentResult(mContext,
                bookingId, invoiceId, razorpayPaymentID);


    }

    @Override
    public void onPaymentError(int code, String response) {
       deleteBookingId();
       //showProgress(false);
    }

    private void startPaymentProcess(String invoiceId,String bookingId,String amount){
        Checkout checkout =  new Checkout();
        checkout.setImage(R.drawable.logo_normal);

        try{
            JSONObject options = new JSONObject();
            String spaceName = (String)Cache.getFromCache(mContext,
                                                          Constants.CACHE_PREFIX_SPACE_NAME_QUERY,
                                                          Constants.TYPE_STRING);
            options.put("name",spaceName);
            options.put("currency","INR");
            options.put("amount",amount);
            options.put("invoice_id",invoiceId);
            options.put("ecod", true);

            Cache.setInCache(mContext, Constants.CACHE_PREFIX_INVOICE_ID,
                    invoiceId, Constants.TYPE_STRING);


            checkout.open(GeneratePass.this,options);


        }catch(Exception e){
            showProgress(false);
            MiscUtils.displayNotification(mContext,"Online Payment not available",
                    true);

        }

    }

    public void handleVehicleStaffDiscountSwitch(View v) {
            int rate = getmMonthlyRate(mVehicleType);//Integer.valueOf(mMonthlyRateView.getText().toString());
            switch (mVehicleType) {
                case VEHICLE_TYPE_CAR: {
                    handleDiscountOnSelect(mCarStaffDiscount, rate);
                }
                break;
                case Constants.VEHICLE_TYPE_BIKE: {
                    handleDiscountOnSelect(mBikeStaffDiscount, rate);
                }
                break;
                case Constants.VEHICLE_TYPE_MINIBUS: {
                    handleDiscountOnSelect(mMiniBusStaffDiscount, rate);
                }
                break;
                case Constants.VEHICLE_TYPE_BUS: {
                    handleDiscountOnSelect(mBusStaffDiscount, rate);
                }
                break;
            }
    }

    private void handleDiscountOnSelect(int discount, int rate){
//           Toast.makeText(getApplicationContext(),""+discount+" "+rate,Toast.LENGTH_SHORT).show();
          Float discountedRate = Float.valueOf(rate);
           boolean mMonthlyRateView_hide = false;
            mStaffDiscountSwitch_Val = mStaffDiscountSwitch.isChecked();
           if (discount == 100) {
               mStaffDiscountView.setVisibility(View.VISIBLE);
               mMonthlyRateView_hide = true;
           } else if (discount > 0) {
               mStaffDiscountView.setVisibility(View.VISIBLE);
               mMonthlyRateView.getText();
           } else {
               mStaffDiscountView.setVisibility(View.GONE);
           }
           if(mStaffDiscountSwitch_Val) {
               if(mMonthlyRateView_hide){
                   mMonthlyRateView.setVisibility(View.INVISIBLE);
               }
               discountedRate = rate*(1-(Float.valueOf(discount)/100));
           }else{
               mMonthlyRateView.setVisibility(View.VISIBLE);
           }
            int discountedRateInt = Math.round(discountedRate);
        mMonthlyRateView.setText(Integer.toString(discountedRateInt));
       }
    private int getmMonthlyRate(int VEHICLE_TYPE) {
            return (Integer) Cache.getFromCache(mContext,
                Constants.CACHE_PREFIX_MONTHLY_RATE_QUERY + VEHICLE_TYPE,
                Constants.TYPE_INT);
    }
    private int getmDiscount(int VEHICLE_TYPE) {
            return  (Integer) Cache.getFromCache(mContext,
            Constants.CACHE_PREFIX_STAFF_DISCOUNT_RATE_QUERY + VEHICLE_TYPE,
            Constants.TYPE_INT);

    }
}

