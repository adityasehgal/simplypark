package in.simplypark.spot_app.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import in.simplypark.spot_app.R;
import in.simplypark.spot_app.activities.GeneratePass;
import in.simplypark.spot_app.activities.LostPassActivity;
import in.simplypark.spot_app.activities.MonthlyPassRenewalTransactions;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.printer.PrinterService;
import in.simplypark.spot_app.listelements.ReportElements;
import in.simplypark.spot_app.listelements.MonthlyPassesReportElements;

/**
 * Created by root on 06/08/16.
 */
public class CustomMonthlyPassesListAdapter extends ArrayAdapter<String> implements Filterable{
    private final Activity mContext;
    private ArrayList<MonthlyPassesReportElements> mElements;
    private ArrayList<MonthlyPassesReportElements> mOriginalElements;
    private Filter mFilter;

    public CustomMonthlyPassesListAdapter(Activity context,
                                          ArrayList<MonthlyPassesReportElements> elements){
        super(context, R.layout.report_list);

        mContext = context;
        mElements = new ArrayList<MonthlyPassesReportElements>(elements);
        mOriginalElements = new ArrayList<MonthlyPassesReportElements>(elements);

    }

    public void add(MonthlyPassesReportElements row){
        super.add("item");
        mElements.add(row);
        mOriginalElements.add(row);


    }

    public void addAll(ArrayList<MonthlyPassesReportElements> elements){
        String []item = new String[elements.size()];
        java.util.Arrays.fill(item, "item1");

        super.addAll(item);
        mElements.addAll(elements);
        mOriginalElements.addAll(elements);

    }

    @Override
    public int getCount() {
        return mElements.size();
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent){
        LayoutInflater inflater = mContext.getLayoutInflater();
        final View rowView = inflater.inflate(R.layout.monthly_passes_booking_report_list, null, true);



        TextView customerNameView  = (TextView)rowView.findViewById(R.id.ltbrl_customer_name_id);
        TextView bookingIdView = (TextView)rowView.findViewById(R.id.ltbr_booking_id);
        TextView bookingDurationView = (TextView)rowView.findViewById(R.id.ltbr_booking_duration_id);
        final TextView bookingStatusView = (TextView)rowView.findViewById(R.id.ltbr_booking_status_id);
        TextView numSlotsBookedView = (TextView)rowView.findViewById(R.id.ltbr_slots_booked_id);
        TextView numSlotsUsedView = (TextView)rowView.findViewById(R.id.ltbr_slots_used_id);
        TextView numCarsParkedView = (TextView)rowView.findViewById(R.id.ltbr_num_cars_parked_id);
        TextView numBikesParkedView = (TextView)rowView.findViewById(R.id.ltbr_num_bikes_parked);
        TextView perMonthFeesView = (TextView)rowView.findViewById(R.id.ltbr_per_month_fees_id);
        TextView issueDateView = (TextView)rowView.findViewById(R.id.ltbr_issue_date_id);
        TextView validDateView = (TextView)rowView.findViewById(R.id.ltbr_valid_date_id);

        View vehicleRegTableRow = rowView.findViewById(R.id.ltbr_vehicle_reg_row_id);
        TextView vehicleRegView = (TextView)rowView.findViewById(R.id.ltbr_vehicle_reg_id);

        View staffPassView = rowView.findViewById(R.id.is_staff_pass_view);

        View syncWithServerView = rowView.findViewById(R.id.ltbr_blue_end_line_id);

        final MonthlyPassesReportElements row = mElements.get(position);

        customerNameView.setText(row.getCustomer());
        bookingIdView.setText(row.getBookingId());
        bookingDurationView.setText(row.getBookingStartDateTime() + " to " +
                                    row.getBookingEndDateTime());

        numSlotsBookedView.setText(Integer.toString(row.getNumBookedSlots()));
        numSlotsUsedView.setText(Integer.toString(row.getNumSlotsInUse()));
        numCarsParkedView.setText(Integer.toString(row.getNumCarsParked()));
        numBikesParkedView.setText(Integer.toString(row.getNumBikesParked()));
        perMonthFeesView.setText("INR " + Integer.toString(row.getPerMonthFees()));



        if(row.getIsStaffPass()) {
            staffPassView.setVisibility(View.VISIBLE);
        }
        if(row.getIsSyncedWithServer()){
            syncWithServerView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.simplypark_blue3));
        }else{
            syncWithServerView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.red));
        }

        String issueDate = row.getBookingIssueDateTime();
        String validDate = row.getBookingValidityDateTime();

        issueDateView.setText(issueDate);
        validDateView.setText(validDate);


        final ImageButton renewalButton = (ImageButton)rowView.findViewById(R.id.ltr_renew_pass_id);
        final ImageButton printButton  = (ImageButton)rowView.findViewById(R.id.ltbr_print_again_id);
        final ImageButton transactionsButton = (ImageButton)rowView.findViewById(R.id.ltr_list_transactions_id);
        final Button modifyButton = (Button)rowView.findViewById(R.id.ltbr_lost_pass_id);

        boolean isDisabled = false;

        if(row.getNumBookedSlots() == 1){
            vehicleRegTableRow.setVisibility(View.VISIBLE);
            renewalButton.setVisibility(View.VISIBLE);
            printButton.setVisibility(View.INVISIBLE);
            transactionsButton.setVisibility(View.VISIBLE);

            String vehicleDetails = row.getVehicleReg();
            if(row.getVehicleType() == Constants.VEHICLE_TYPE_BIKE){
                vehicleDetails += " (Bike)";
            }else{
                vehicleDetails += " (Car)";
            }
            vehicleRegView.setText(vehicleDetails);

            transactionsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(), MonthlyPassRenewalTransactions.class);
                    Bundle parameters = new Bundle();

                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_BOOKING_ID,row.getBookingId());
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_ISSUED_TO,row.getCustomer());
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_VEHICLE_REG,
                            row.getVehicleReg());
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_VEHICLE_TYPE,
                            row.getVehicleType());

                    intent.putExtras(parameters);
                    getContext().startActivity(intent);
                }
            });


            renewalButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(), GeneratePass.class);
                    Bundle parameters = new Bundle();

                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_BOOKING_ID,row.getBookingId());
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_ISSUED_TO,row.getCustomer());
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_EMAIL,row.getEmail());
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_TEL,row.getMobile());
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_VEHICLE_REG,
                                    row.getVehicleReg());
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_VEHICLE_TYPE,
                                    row.getVehicleType());
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_PER_MONTH_FEES,
                                    row.getPerMonthFees());
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_NEXT_RENEWAL_DATETIME,
                                    row.getNextRenewalDateTime());
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_VALID_TILL,
                                    row.getBookingEndDateTimeLong());
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_VALID_FROM,
                            row.getBookingStartDateTimeLong());
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_VALID_TILL,
                            row.getBookingEndDateTimeLong());

                    intent.putExtra(Constants.BUNDLE_PARAM_ENCODE_PASS_CODE,row.getEncodedPassCode());
                    intent.putExtra(Constants.BUNDLE_PARAM_STAFF_MONTHLY_PASS,row.getIsStaffPass());

                    intent.putExtras(parameters);
                    getContext().startActivity(intent);
                }
            });

            printButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   try {
                       //print the monthly pass
                       PrinterService.startActionPrintGeneratedMonthlyPass(mContext,
                               row.getVehicleReg(), row.getVehicleType(), row.getCustomer(),
                               row.getEmail(), row.getMobile(),
                               row.getBookingStartDateTime(), row.getBookingEndDateTime(),
                               row.getBookingId(), row.getPerMonthFees());
                   }catch(Exception e){
                       Log.d("Printer","Exception is " + e);
                   }

                }
            });

            modifyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(), LostPassActivity.class);
                    Bundle parameters = new Bundle();

                    intent.putExtra(Constants.BUNDLE_PARAM_LOST_MONTHLY_PASS,true);
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_BOOKING_ID,row.getBookingId());
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_ISSUED_TO,row.getCustomer());
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_EMAIL,row.getEmail());
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_TEL,row.getMobile());
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_VEHICLE_REG,
                            row.getVehicleReg());
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_VEHICLE_TYPE,
                            row.getVehicleType());
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_PER_MONTH_FEES,
                            row.getPerMonthFees());
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_VALID_FROM,
                            row.getBookingStartDateTimeLong());
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_VALID_TILL,
                            row.getBookingEndDateTimeLong());

                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_NEXT_RENEWAL_DATETIME,
                            row.getNextRenewalDateTime());
                    intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_VALID_TILL,
                            row.getBookingEndDateTimeLong());
                    intent.putExtra(Constants.BUNDLE_PARAM_ENCODE_PASS_CODE,row.getEncodedPassCode());
                    intent.putExtra(Constants.BUNDLE_PARAM_STAFF_MONTHLY_PASS,row.getIsStaffPass());

                    intent.putExtras(parameters);
                    getContext().startActivity(intent);
                }
            });
        }else{

            int status = row.getStatus();

            if(status != Constants.BOOKING_STATUS_CANCELLED  &&
                    status != Constants.BOOKING_STATUS_EXPIRED){
                bookingStatusView.setText("Active");
            }else{
                if(status == Constants.BOOKING_STATUS_CANCELLED){
                    bookingStatusView.setText("Cancelled");
                }else{
                    bookingStatusView.setText("Expired");
                }
                isDisabled = true;
            }


            printButton.setVisibility(View.VISIBLE);
            renewalButton.setVisibility(View.INVISIBLE);
            transactionsButton.setVisibility(View.INVISIBLE);


        }


        if(isDisabled){
            rowView.setBackgroundResource(R.drawable.layer_card_background_disabled);
            rowView.invalidate();
            printButton.setVisibility(View.GONE);
        }




        return rowView;


    }

    public MonthlyPassesReportElements get(int position){
        return mElements.get(position);
    }

    @Override
    public Filter getFilter()
    {
        if (mFilter == null)
            mFilter = new CustomMonthlyPassesListAdapterFilter();

        return mFilter;
    }

    private class CustomMonthlyPassesListAdapterFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            String prefix = constraint.toString().toLowerCase();

            if (prefix == null || prefix.length() == 0)
            {
                results.values = mOriginalElements;
                results.count  = mOriginalElements.size();
            }else{

                final ArrayList<MonthlyPassesReportElements> list = mOriginalElements;
                int count = list.size();
                final ArrayList<MonthlyPassesReportElements> filteredList =
                                         new ArrayList<MonthlyPassesReportElements>(count);

                for (int i=0; i<count; i++)
                {
                    final MonthlyPassesReportElements element = list.get(i);
                    final String vehicleReg = element.getVehicleReg().toLowerCase();
                    final String name = element.getCustomer().toLowerCase();
                    final String email = element.getEmail().toLowerCase();
                    final String bookingId = element.getBookingId().toLowerCase();


                    if (vehicleReg.toLowerCase().contains(prefix))
                    {
                        filteredList.add(element);
                    }else if (name.startsWith(prefix)){
                        filteredList.add(element);
                    }else if (email.startsWith(prefix)){
                        filteredList.add(element);
                    }
                }

                results.values = filteredList;
                results.count  = filteredList.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            int count = results.count;

            if(count > 0) {
                mElements = (ArrayList<MonthlyPassesReportElements>)(results.values);
                notifyDataSetChanged();
            }else{
                mElements.clear();
                //notifyDataSetInvalidated();
                notifyDataSetChanged();
            }
        }

    }
}




