package in.simplypark.spot_app.adapters;

import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;

import in.simplypark.spot_app.db_tables.TransactionTbl;

/**
 * Created by csharma on 20-12-2016.
 */

public class RegNoDropDownFilter extends Filter {
    AutoCompleteRegDropDownAdapter adapter;
    List<TransactionTbl> originalList;
    List<TransactionTbl> filteredList;
    boolean isParkingId;
    public RegNoDropDownFilter(AutoCompleteRegDropDownAdapter adapter, List<TransactionTbl> originalList, boolean isParkingId) {
        super();
        this.adapter = adapter;
        this.originalList = originalList;
        this.filteredList = new ArrayList<>();
        this.isParkingId = isParkingId;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        filteredList.clear();
        final FilterResults results = new FilterResults();

        if (constraint == null || constraint.length() == 0) {
            filteredList.addAll(originalList);
        } else {
            final String filterPattern = constraint.toString().toLowerCase().trim();

            // Your filtering logic goes in here
            for (final TransactionTbl transaction : originalList) {
                if(isParkingId) {
                    if (transaction.getCarRegNumber().toLowerCase().contains(filterPattern)) {
                        filteredList.add(transaction);
                    }
                }else {
                    if (transaction.getParkingId().toLowerCase().contains(filterPattern)) {
                        filteredList.add(transaction);
                    }
                }
            }
        }
        results.values = filteredList;
        results.count = filteredList.size();
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.filteredTransactions.clear();
        adapter.filteredTransactions.addAll((List) results.values);
        if (results != null && results.count > 0) {
            adapter.notifyDataSetChanged();
        }
        else {
            adapter.notifyDataSetInvalidated();
        }
    }


}
