package in.simplypark.spot_app.db_tables;

import com.orm.SugarRecord;

import in.simplypark.spot_app.config.Constants;

/**
 * Created by root on 07/12/16.
 */

public class NightChargeTbl extends SugarRecord {
    private int mStartHour;
    private int mEndHour;
    private int mDailyCharge;
    private int mMonthlyCharge;
    private int mVehicleType;

    public int getStartHour() {
        return mStartHour;
    }

    public void setStartHour(int mStartHour) {
        this.mStartHour = mStartHour;
    }

    public int getEndHour() {
        return mEndHour;
    }

    public void setEndHour(int mEndHour) {
        this.mEndHour = mEndHour;
    }

    public int getDailyCharge() {
        return mDailyCharge;
    }

    public void setDailyCharge(int mDailyCharge) {
        this.mDailyCharge = mDailyCharge;
    }

    public int getMonthlyCharge() {
        return mMonthlyCharge;
    }

    public void setMonthlyCharge(int mMonthlyCharge) {
        this.mMonthlyCharge = mMonthlyCharge;
    }

    public int getVehicleType() {
        return mVehicleType;
    }

    public void setVehicleType(int mVehicleType) {
        this.mVehicleType = mVehicleType;
    }

    public NightChargeTbl(){
        mStartHour = 0;
        mEndHour = 0;
        mDailyCharge = 0;
        mMonthlyCharge = 0;
        mVehicleType = Constants.VEHICLE_TYPE_INVALID;

    }
}
