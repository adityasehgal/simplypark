package in.simplypark.spot_app.listelements;

import java.util.Comparator;

/**
 * Created by root on 06/08/16.
 */
public class MonthlyPassesReportElements {
    private String mCustomer;
    private String mEmail;
    private String mMobile;
    private String mBookingId;
    private String mBookingStartDateTime;
    private Long   mBookingStartDateTimeLong;
    private String mBookingEndDateTime;
    private long   mBookingEndDateTimeLong;
    private long   mNextRenewalDateTime;
    private int    mNumBookedSlots;
    private int    mNumSlotsInUse;
    private int    mNumCarsParked;
    private int    mNumBikesParked;
    private int    mStatus;
    private int    mPerMonthFees;
    private String mVehicleReg;
    private int    mVehicleType;
    private boolean mIsSyncedWithServer;
    private String  mEncodedPassCode;
    private String  mBookingValidityDateTime;
    private String  mBookingIssueDateTime;
    private long    mBookingIssueDateTimeLong;
    private boolean mIsStaffPass;

    public MonthlyPassesReportElements(){
       mEncodedPassCode = "";
    }

    public String getBookingValidityDateTime() {
        return mBookingValidityDateTime;
    }

    public void setBookingValidityDateTime(String mBookingValidityDateTime) {
        this.mBookingValidityDateTime = mBookingValidityDateTime;
    }

    public String getBookingIssueDateTime() {
        return mBookingIssueDateTime;
    }

    public void setBookingIssueDateTime(String mBookingIssueDateTime) {
        this.mBookingIssueDateTime = mBookingIssueDateTime;
    }

    public long getBookingIssueDateTimeLong() {
        return mBookingIssueDateTimeLong;
    }

    public void setBookingIssueDateTimeLong(long bookingIssueDateTime){
        this.mBookingIssueDateTimeLong = bookingIssueDateTime;

    }



    public void setCustomer(String customer){ mCustomer = customer;}
    public String getCustomer(){ return mCustomer;}

    public void setBookingId(String bookingId){ mBookingId = bookingId;}
    public String getBookingId(){ return mBookingId;}

    public void setBookingStartDateTime(String bookingStartDateTime){
        mBookingStartDateTime = bookingStartDateTime;
    }
    public String getBookingStartDateTime(){ return mBookingStartDateTime;}

    public void setNextRenewalDateTime(long nextRenewalDateTime){
        mNextRenewalDateTime = nextRenewalDateTime;
    }
    public long getNextRenewalDateTime(){
        return mNextRenewalDateTime;
    }

    public void setBookingEndDateTime(String bookingEndDateTime){
        mBookingEndDateTime = bookingEndDateTime;
    }
    public String getBookingEndDateTime(){ return mBookingEndDateTime;}

    public void setBookingEndDateTimeLong(long bookingEndDateTimeLong){
        mBookingEndDateTimeLong = bookingEndDateTimeLong;
    }
    public long getBookingEndDateTimeLong(){
        return mBookingEndDateTimeLong;
    }



    public void setNumBookedSlots(int numBookedSlots){ mNumBookedSlots = numBookedSlots;}
    public int getNumBookedSlots(){ return mNumBookedSlots;}

    public void setNumSlotsInUse(int numSlotsInUse){ mNumSlotsInUse = numSlotsInUse;}
    public int getNumSlotsInUse(){ return mNumSlotsInUse;}

    public void setNumCarsParked(int numCarsParked){ mNumCarsParked = numCarsParked;}
    public int getNumCarsParked(){ return mNumCarsParked;}

    public void setNumBikesParked(int numBikesParked){ mNumBikesParked = numBikesParked;}
    public int getNumBikesParked(){ return mNumBikesParked;}

    public void    setStatus(int status){ mStatus = status;}
    public int getStatus(){ return mStatus;}

    public void setPerMonthFees(int perMonthFees){ mPerMonthFees = perMonthFees;}
    public int getPerMonthFees() { return mPerMonthFees;}

    public void setVehicleReg(String vehicleReg) { mVehicleReg = vehicleReg;}

    public boolean getIsStaffPass() {
        return mIsStaffPass;
    }

    public void setIsStaffPass(boolean mIsStaffPass) {
        this.mIsStaffPass = mIsStaffPass;
    }

    public String getVehicleReg(){ return mVehicleReg;}

    public void setVehicleType(int vehicleType) { mVehicleType = vehicleType;}
    public int getVehicleType(){ return mVehicleType;}

    public void setEmail(String email){ mEmail = email;}
    public String getEmail() { return mEmail;}

    public void setMobile(String mobile){ mMobile = mobile;}
    public String getMobile(){ return mMobile;}

    public void setIsSyncedWithServer(boolean isSyncedWithServer){ mIsSyncedWithServer = isSyncedWithServer;}
    public boolean getIsSyncedWithServer(){ return mIsSyncedWithServer;}

    public void setEncodedPassCode(String encodedPassCode){
        mEncodedPassCode = encodedPassCode;
    }
    public String getEncodedPassCode(){ return mEncodedPassCode;}

    public Long getBookingStartDateTimeLong() {
        return mBookingStartDateTimeLong;
    }

    public void setBookingStartDateTimeLong(Long mBookingStartDateTimeLong) {
        this.mBookingStartDateTimeLong = mBookingStartDateTimeLong;
    }

    public static final Comparator<MonthlyPassesReportElements> ISSUE_TIME_ORDER =
            new Comparator<MonthlyPassesReportElements>() {
                public int compare(MonthlyPassesReportElements e1, MonthlyPassesReportElements e2) {
                    return (int)(e1.getBookingIssueDateTimeLong() - e2.getBookingIssueDateTimeLong());
                }
            };
}
