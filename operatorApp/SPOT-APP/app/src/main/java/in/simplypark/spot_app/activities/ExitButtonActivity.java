package in.simplypark.spot_app.activities;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import hirondelle.date4j.DateTime;
import in.simplypark.spot_app.R;
import in.simplypark.spot_app.adapters.AutoCompleteRegDropDownAdapter;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.db_tables.NightChargeTbl;
import in.simplypark.spot_app.db_tables.PenaltyChargeTbl;
import in.simplypark.spot_app.db_tables.PendingExits;
import in.simplypark.spot_app.db_tables.PricingPerVehicleTypeTbl;
import in.simplypark.spot_app.db_tables.SimplyParkBookingTbl;
import in.simplypark.spot_app.db_tables.SimplyParkTransactionTbl;
import in.simplypark.spot_app.db_tables.SpotDb;
import in.simplypark.spot_app.db_tables.TransactionTbl;
import in.simplypark.spot_app.utils.Cache;
import in.simplypark.spot_app.utils.DateUtil;
import in.simplypark.spot_app.utils.MiscUtils;
import in.simplypark.spot_app.utils.ParkingId;

import static in.simplypark.spot_app.config.Constants.BUNDLE_PARAM_IS_NIGHT_CHARGE_APPLIED;

public class ExitButtonActivity extends Activity {

    private logExitTask mLogExitTask;
    private logPassExitTask mLogPassExitTask;
    private AutoCompleteTextView mTicketIdView;
    private TextView mOrView;
    private AutoCompleteTextView mParkingIdView;
    private Button mScanReceiptButton;
    private EditText mTicketIdPrefixView;
    private boolean mIsScanWithParkingIdRequired = true;
    private Switch mIsTicketLostView;
    private boolean mIsTicketLost = false;
    private Context mContext = null;



    private InputMethodManager imm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        if (android.os.Build.VERSION.SDK_INT >= 11) {
            setFinishOnTouchOutside(false);
        }

        setContentView(R.layout.activity_exit_button);
        mContext = getApplicationContext();
        mOrView = (TextView) findViewById(R.id.or_id);
        mParkingIdView = (AutoCompleteTextView) findViewById(R.id.parking_id);
        mScanReceiptButton = (Button) findViewById(R.id.scan_receipt);
        mTicketIdView = (AutoCompleteTextView) findViewById(R.id.ticket_id);
        mTicketIdPrefixView = (EditText) findViewById(R.id.ticket_id_prefix);
        mTicketIdView.setThreshold(1);
        mTicketIdView.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        new LoadAutoCompleteList().execute();
        mTicketIdView.setDropDownBackgroundResource(R.color.white);
        mTicketIdView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> av, View view, int index,
                                    long arg3) {
                String fetchedId = view.getTag().toString();
                mTicketIdView.setText(fetchedId);
            }
        });

        mIsTicketLostView = (Switch)findViewById(R.id.aeb_lost_ticket_id);
        if(!(Boolean)Cache.getFromCache(getApplicationContext(),
                                       Constants.CACHE_PREFIX_IS_LOST_TICKET_CHARGE_AVAILABLE,
                                       Constants.TYPE_BOOL)){
            mIsTicketLostView.setVisibility(View.GONE);
        }

        mTicketIdView.requestFocus();
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

        if((Boolean)Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_IS_REUSABLE_TOKEN_USED,Constants.TYPE_BOOL)){
            mOrView.setVisibility(View.GONE);
            mParkingIdView.setVisibility(View.GONE);
        }
    }

    //lost ticket button
    public void handleLostTicketButton(View view){
        if(mIsTicketLostView.isChecked()){
            mIsTicketLost = true;
        }else{
            mIsTicketLost = false;
        }

    }

    //Cancel Button
    public void handleCancelButton(View view) {
        finish();
    }

    //payment Button
    public void handleCalculateButton(View view) {


        // Reset errors.
        mTicketIdView.setError(null);


        if (mTicketIdView.length() == 0 && mIsScanWithParkingIdRequired == false) {
            mTicketIdView.setError("This field is required");
            mTicketIdView.requestFocus();
            return;
        }

        boolean queryByCarReg = true;

        String ticketId = "";
        if (mTicketIdView.length() != 0) {
            if (mTicketIdView.length() < Constants.REG_MIN_LEN) {
                mTicketIdView.setError("Invalid Vehicle Number. Please try again");
                mTicketIdView.requestFocus();
                return;
            }
            if (mTicketIdPrefixView.length() == 0) {
                ticketId = mTicketIdView.getText().toString().toUpperCase();
            } else {
                ticketId = mTicketIdPrefixView.getText().toString().toUpperCase() +
                        mTicketIdView.getText().toString();
            }
        } else if (mIsScanWithParkingIdRequired == true || mParkingIdView.length() != 0) {
              if (mParkingIdView.length() != Constants.PARKING_ID_REG_MIN_LEN_WITH_VEHICLE_TYPE
                    && mParkingIdView.length() != Constants.PARKING_ID_MIN_LEN_WITH_CUSTOMER_ID) {
                mParkingIdView.setError("Invalid Parking ID. Minimum Length is " +
                        Constants.PARKING_ID_REG_MIN_LEN_WITH_VEHICLE_TYPE);
                mParkingIdView.requestFocus();
                return;
            }
            ticketId = mParkingIdView.getText().toString().toLowerCase();
            queryByCarReg = false;

        }else {
            mTicketIdView.setError("Please provide either the vehicle number or the parking ID");
            mTicketIdView.requestFocus();
            return;
        }


        if (queryByCarReg == true) {
            //search if a transaction occurs
            //this tasks writes to db in background, and then starts
            //intent services for receipt printing and server updation
            mLogExitTask = new logExitTask(ticketId, Constants.QUERY_RECEIPT_BY_CAR_REG);
            mLogExitTask.execute((Void) null);

        } else {
            //search if a transaction occurs
            //this tasks writes to db in background, and then starts
            //intent services for receipt printing and server updation
            mLogExitTask = new logExitTask(ticketId, Constants.QUERY_RECEIPT_BY_PARKING_ID);
            mLogExitTask.execute((Void) null);

        }


    }

    @Override
    public void onPause(){
        super.onPause();

        InputMethodManager imm = (InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mTicketIdView.getWindowToken(), 0);
    }
    //scan pass button
    public void handleScanSimplyParkButton(View view) {
        //Create a new Intent to scan QR Code
        Intent scanActivity = new Intent(Constants.SIMPLYPARK_BOOKING_QR_CODE_SCAN);

        try {
            startActivityForResult(scanActivity,
                    Constants.EXIT_MONTHLY_PASS_READ_REQUEST);

        } catch (ActivityNotFoundException activity) {
            String errorMsg = "Appropriate App(QrCode Private) is NOT installed";
            Log.e("QrCodeScanning", errorMsg);
            Toast toast = Toast.makeText(getApplicationContext(),
                    errorMsg,
                    Toast.LENGTH_SHORT);
            toast.show();
        }

    }


    public void handleScanReceiptButton(View view) {
        //Create a new Intent to scan QR Code
        Intent scanActivity = new Intent(Constants.SIMPLYPARK_BOOKING_QR_CODE_SCAN);

        try {
            startActivityForResult(scanActivity, Constants.EXIT_BARCODE_READ_REQUEST);

        } catch (ActivityNotFoundException activity) {
            Log.e("QrCodeScanning", "Appropriate APP(QrCode Private) is NOT installed");
        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == Constants.EXIT_REQUEST_CODE) { //result of the subexit activity
            if (resultCode == RESULT_OK) {
                Context context = getApplicationContext();
                int vehicleType = data.getIntExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                        Constants.VEHICLE_TYPE_CAR);

                boolean isNew = data.getBooleanExtra(Constants.BUNDLE_PARAM_IS_NEW_RECORD, false);

                if (!isNew) {
                    //pre-emptively update the cache and the view.
                    MiscUtils.decrementOccupancyCount(context, vehicleType);
                }
            }

            Intent intent = ExitButtonActivity.this.getIntent();
            ExitButtonActivity.this.setResult(resultCode, intent);
            finish();
        }else if (requestCode == Constants.EXIT_MONTHLY_PASS_READ_REQUEST) {

            if (resultCode == RESULT_OK && data != null && data.getExtras() != null) {

                String scanResult = data.getExtras().getString(
                          Constants.SIMPLYPARK_BOOKING_SCANNING_RESULT);
                if(scanResult.startsWith(Constants.REUSABLE_TOKEN_PREFIX)){
                    mLogExitTask = new logExitTask(scanResult, Constants.QUERY_RECEIPT_BY_TOKEN);
                    mLogExitTask.execute((Void) null);

                }else {
                    String bookingId = scanResult;

                    mLogPassExitTask = new logPassExitTask(bookingId);
                    mLogPassExitTask.execute((Void) null);
                }

            } else {
                mParkingIdView.setError("Could not read the pass. Please try again.");
                mParkingIdView.requestFocus();
            }

        } else if (requestCode == Constants.EXIT_BARCODE_READ_REQUEST) {
            if (data != null && data.getExtras() != null) {
                //Read result from QR Droid (it's stored in la.droid.qr.result)
                String result = data.getExtras().getString(
                        Constants.SIMPLYPARK_BOOKING_SCANNING_RESULT);

                if (resultCode != RESULT_OK || null == result || result.length() == 0) {
                    Log.d("QRCODE RESULTS", "failed");
                    mParkingIdView.setError("Could not read the barcode. Please enter manually");
                    mParkingIdView.requestFocus();

                }
                //Just set result to EditText to be able to view it
                mParkingIdView.setText(result);
            }

        }
    }


    /**
     * Represents an asynchronous logExitTask task used to log the Exit
     */
    public class logExitTask extends AsyncTask<Void, Void, Boolean> {


        String mParkingId;
        String mCarReg;
        String mBookingId;
        String mToken;
        List<SpotDb> mSpaceObjList;
        double mParkingFees;
        int mSpaceId;
        int mVehicleType = Constants.VEHICLE_TYPE_CAR;

        boolean mIsQueryByParkingId;
        boolean mIsQueryByToken;

        long mExitTime;
        long mEntryTime;
        String mFailureCause;
        String mDurationDescription;
        boolean mIsNew;
        boolean mIsSimplyParkTransaction = false;
        boolean mIsNightChargeApplied = false;
        boolean mIsPenaltyChargeApplied = false;
        boolean mIsLostTicketChargeApplied = false;
        int mNumNights = 0;
        int mNightCharge = 0;
        int mPenaltyCharge = 0;
        int mNumPenaltyHours = 0;
        int mLostTicketCharge = 0;

        

        logExitTask(String queryBy, int carRegOrParkingId) {
            if (carRegOrParkingId == Constants.QUERY_RECEIPT_BY_PARKING_ID) {
                mVehicleType = queryBy.charAt(0) - '0';
                mParkingId = queryBy.substring(1);
                //mCarReg    = queryBy.substring(Constants.PARKING_ID_MAX_LEN + 1);
                //mParkingId = queryBy;

                mIsQueryByParkingId = true;
                mIsQueryByToken = false;
            } else if (carRegOrParkingId == Constants.QUERY_RECEIPT_BY_CAR_REG){
                mCarReg = queryBy;
                mParkingId = "";
                mIsQueryByParkingId = false;
                mIsQueryByToken = false;
            }else{
                mIsQueryByToken = true;
                mToken = queryBy;

            }

            mFailureCause = new String();
            mDurationDescription = new String();
            mIsNew = false;

        }

        @Override
        protected Boolean doInBackground(Void... params) {

            List<TransactionTbl> dbObjList;
            List<SimplyParkTransactionTbl> spTransList = null;

            mSpaceId = (Integer) Cache.getFromCache(mContext,
                    Constants.CACHE_PREFIX_SPACEID_QUERY,
                    Constants.TYPE_INT);


            if(mIsQueryByToken){
                dbObjList = TransactionTbl.find(TransactionTbl.class, "m_token = ? and " +
                                "m_status = ?", mToken,
                        Integer.toString(Constants.PARKING_STATUS_ENTERED));
            }else {
                if (!mIsQueryByParkingId) {
                    dbObjList = TransactionTbl.find(TransactionTbl.class, "m_car_reg_number = ? and " +
                                    "m_status = ?", mCarReg,
                            Integer.toString(Constants.PARKING_STATUS_ENTERED));
                } else {
                    //Query by parking Id but check if the parking id belongs to this space
                    switch (mSpaceId) {
                        case Constants.NDMC_HACK_BIKE_SPACE_ID:
                            if (mVehicleType != Constants.VEHICLE_TYPE_BIKE) {
                                mFailureCause = "Invalid ParkingID. Please enter a bike specific " +
                                        "Parking ID";
                                return false;
                            }
                            break;

                        case Constants.NDMC_HACK_CAR_SPACE_ID:
                            if (mVehicleType != Constants.VEHICLE_TYPE_CAR) {
                                mFailureCause = "Invalid ParkingID. Please enter a car specific " +
                                        "Parking ID";
                                return false;
                            }
                            break;
                    }
                    dbObjList = TransactionTbl.find(TransactionTbl.class, "m_parking_id = ?", mParkingId);

                }
            }


            if (dbObjList.isEmpty()) {
                if(mIsQueryByToken){
                    mFailureCause = "No Entry found. Kindly re-check the Token or exit with car number";
                    return false;

                }else {
                    if (!mIsQueryByParkingId) {
                        spTransList = SimplyParkTransactionTbl.find(SimplyParkTransactionTbl.class,
                                "m_vehicle_reg = ? and m_status = ?",
                                mCarReg,
                                Integer.toString(Constants.PARKING_STATUS_ENTERED));
                        if (spTransList.isEmpty()) {
                            Log.e("ExitButtonActivity",
                                    "Query by registeration number but NO such parking found");

                            mFailureCause = "No Entry found. Kindly re-check the vehicle number " +
                                    "or enter Parking ID from the receipt";
                            mIsScanWithParkingIdRequired = true;
                            return false;
                        }

                        mIsSimplyParkTransaction = true;

                    } else {

                        //if(mParkingId.indexOf(Constants.PARKING_ID_PARKING_FEES_PREPAID_SUFFIX) != -1){
                        if (mParkingId.length() > Constants.MAX_PARK_ID_LEN) {
                            spTransList = SimplyParkTransactionTbl.find(SimplyParkTransactionTbl.class,
                                    "m_parking_id = ? and m_status = ?",
                                    mParkingId,
                                    Integer.toString(Constants.PARKING_STATUS_ENTERED));

                            mIsSimplyParkTransaction = true;


                        }

                    }
                }

            }else{



                if(mIsQueryByParkingId){
                    TransactionTbl transaction = dbObjList.get(0);
                    int parkingStatus = transaction.getParkingStatus();

                    switch(parkingStatus){
                        case Constants.PARKING_STATUS_EXITED:{
                            mFailureCause = "Vehicle is already exited. Kindly re-check the ParkingID " +
                                    "from the receipt";
                            return false;
                        }


                        case Constants.PARKING_STATUS_TRANSACTION_CANCELLED:{
                            mFailureCause = "Vehicle transaction is cancelled. Kindly re-check the" +
                                    "Parking ID from the receipt";
                            return false;

                        }

                        case Constants.PARKING_STATUS_ENTERED:{

                        }
                        break;

                        default:{
                            mFailureCause = "No transaction found. Kindly re-check the" +
                                    "Parking ID from the receipt";

                            return false;

                        }

                    }

                }
            }




            mSpaceObjList = SpotDb.find(SpotDb.class, "m_space_id = ?", Integer.toString(mSpaceId));

            if (mSpaceObjList.isEmpty()) {
                Log.e("ExitButtonActivity", "Database/Cache corrupted. Could not find any entry" +
                        "in database for spaceId" + Integer.toString(mSpaceId));

                mFailureCause = "Database/Cache corrupted. Kindly contact SimplyPark.in";

                return false;
            }

            if (!mIsSimplyParkTransaction) {

                Date entryTime, exitTime;

                if (dbObjList.isEmpty()) {
                    exitTime = new Date();
                    entryTime = ParkingId.getEntryTime(mParkingId);
                    if (entryTime == null) {
                        mFailureCause = "Invalid Parking ID entered.";
                        return false;

                    }
                    mExitTime  = exitTime.getTime();
                    mEntryTime = entryTime.getTime(); 
                    mIsNew = true;

                } else {
                    TransactionTbl transaction = dbObjList.get(0);

                    if (transaction.getParkingStatus() != Constants.PARKING_STATUS_ENTERED) {
                        mFailureCause = "No Entry for this vehicle Found. " +
                                "Kindly re-check vehicle number";
                        return false;
                    }
                    exitTime     = new Date();
                    mExitTime    = exitTime.getTime();
                    mEntryTime   = transaction.getEntryTime();
                    mParkingId   = transaction.getParkingId();
                    mVehicleType = transaction.getVehicleType();
                    mCarReg      = transaction.getCarRegNumber();

                }

                mParkingFees = calculateParkingFees(mEntryTime, mExitTime,mVehicleType,true,mIsTicketLost);

                if (mParkingFees == Constants.ERROR_INVALID_CLOCK) {
                    mFailureCause = "Invalid Parking ID entered - Invalid Clock";
                    return false;
                }
            } else {

                mExitTime = DateTime.now(Constants.TIME_ZONE).getMilliseconds(Constants.TIME_ZONE);
                SimplyParkTransactionTbl spTransaction = null;

                if (spTransList.size() > 0) {
                    spTransaction = spTransList.get(0);
                    mBookingId = spTransaction.getBookingId();

                    //find the parent booking
                    List<SimplyParkBookingTbl> bookingList = SimplyParkBookingTbl.find(
                            SimplyParkBookingTbl.class, "m_booking_id = ?", mBookingId);

                    if (bookingList.isEmpty()) {
                        mFailureCause = "Could not find the booking ID for the entered Vehicle.";
                        return false;
                    }

                    mEntryTime   = spTransaction.getEntryTime();
                    mParkingId   = spTransaction.getParkingId();
                    mVehicleType = spTransaction.getVehicleType(); 
                    mIsNew = false;

                } else {
                    String parkingID = mParkingId.substring(0, Constants.PARKING_ID_LEN);
                    String bookingId = MiscUtils.decodeBookingId(getApplicationContext(),
                            mParkingId.substring(Constants.PARKING_ID_LEN,
                                    mParkingId.length()));

                    List<SimplyParkBookingTbl> bookingList = SimplyParkBookingTbl.find(
                            SimplyParkBookingTbl.class, "m_booking_id = ?", bookingId);

                    if (bookingList.isEmpty()) {
                        mFailureCause = "Could not find the booking ID for the entered Vehicle.";
                        return false;
                    }


                    Date entryTime = ParkingId.getEntryTime(parkingID);
                    if (entryTime == null) {
                        mFailureCause = "Invalid Parking ID entered2.";
                        return false;

                    }

                    mEntryTime = entryTime.getTime();
                    mBookingId = bookingId;
                    mIsNew = true;

                }

                mParkingFees = calculateParkingFees(mEntryTime,mExitTime, mVehicleType,false,
                                                    mIsTicketLost);


            }


            return true;

        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mLogExitTask = null;


            if (success) {
                Intent intent = new Intent(ExitButtonActivity.this, ExitButtonSubActivity.class);


                //pass the payment due
                Bundle b = new Bundle();
                b.putBoolean(Constants.BUNDLE_PARAM_IS_EXIT_THROUGH_PASS, false);
                b.putBoolean(Constants.BUNDLE_PARAM_IS_TRANSACTION_SP, mIsSimplyParkTransaction);
                b.putDouble(Constants.BUNDLE_PARAM_PAYMENT_DUE, mParkingFees);
                b.putLong(Constants.BUNDLE_PARAM_ENTRY_TIME,mEntryTime);
                b.putLong(Constants.BUNDLE_PARAM_EXIT_TIME, mExitTime);
                b.putString(Constants.BUNDLE_PARAM_VEHICLE_REG,mCarReg);
                b.putInt(Constants.BUNDLE_PARAM_VEHICLE_TYPE,mVehicleType);
                b.putString(Constants.BUNDLE_PARAM_PARKING_ID, mParkingId);
                b.putString(Constants.BUNDLE_PARAM_DURATION_DESCRIPTION, mDurationDescription);
                b.putBoolean(Constants.BUNDLE_PARAM_IS_NEW_RECORD, mIsNew);
                b.putBoolean(BUNDLE_PARAM_IS_NIGHT_CHARGE_APPLIED, mIsNightChargeApplied);
                b.putInt(Constants.BUNDLE_PARAM_NUM_NIGHTS, mNumNights);
                b.putInt(Constants.BUNDLE_PARAM_EXTRA_NIGHT_CHARGE, mNightCharge);
                b.putBoolean(Constants.BUNDLE_PARAM_IS_PENALTY_CHARGE_APPLIED, mIsPenaltyChargeApplied);
                b.putInt(Constants.BUNDLE_PARAM_PENALTY_CHARGE, mPenaltyCharge);
                b.putInt(Constants.BUNDLE_PARAM_NUM_PENALTY_HOURS, mNumPenaltyHours);
                b.putInt(Constants.BUNDLE_PARAM_LOST_TICKET_CHARGE,mLostTicketCharge);

                

                intent.putExtras(b);

                startActivityForResult(intent, Constants.EXIT_REQUEST_CODE);


            } else {
                if (mIsScanWithParkingIdRequired) {
                    mOrView.setVisibility(View.VISIBLE);
                    mParkingIdView.setVisibility(View.VISIBLE);
                    //mScanReceiptButton.setVisibility(View.VISIBLE);
                    mTicketIdView.setText("");
                    mTicketIdPrefixView.setText("");
                }
                mTicketIdView.setError(mFailureCause);
                mTicketIdView.requestFocus();

            }
        }

        @Override
        protected void onCancelled() {
            mLogExitTask = null;
        }

        private double calculateParkingFeesWithPenalty(PricingPerVehicleTypeTbl pricing ,
                                                       long entryTime, long exitTime,
                                                       int vehicleType,
                                                       boolean isTicketLost){
            DateTime entryDateTime = DateTime.forInstant(entryTime, Constants.TIME_ZONE);
            DateTime exitDateTime = DateTime.forInstant(exitTime, Constants.TIME_ZONE);


            double fees = 0.00;

            Context context = getApplicationContext();
            int penaltyHours;
            //we assume penalty of this type (stayting beyond space close timings) is NOT charged
            //on 24 hours space
            //1. calculate From Date's end of day
            String closingTimeStr = (String)Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_CLOSING_TIME_QUERY,
                    Constants.TYPE_STRING);

            String openingTimeStr = (String)Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_OPENING_TIME_QUERY,
                    Constants.TYPE_STRING);

            String[] closingTime = closingTimeStr.split(":");
            String[] openingTime = openingTimeStr.split(":");

            int closingHour = Integer.parseInt(closingTime[0]);
            int openingHour = Integer.parseInt(openingTime[0]);

            DateTime tempDateObj = null;
            if(openingHour >= closingHour){
                tempDateObj = entryDateTime.plusDays(1);
            }else{
                tempDateObj = entryDateTime;
            }

            DateTime closingTimeObj = new DateTime(tempDateObj.getYear(),tempDateObj.getMonth(),
                    tempDateObj.getDay(),closingHour,
                    Integer.parseInt(closingTime[1]),0,0);

            //2. Take DateDifference between step 1 and end time
            DateUtil difference = new DateUtil();
            DateUtil.Interval interval = difference.getDateDifference(closingTimeObj,
                    exitDateTime);

            //3. Convert the difference into hours
            PenaltyChargeTbl penaltyPrices = pricing.getPenalty();
            if(penaltyPrices != null) {
                if (interval.status != Constants.DATEDIFF_ERROR_FROM_TIME_GREATER_THAN_TO_TIME) {
                    mNumPenaltyHours = interval.days * Constants.NUM_HOURS_IN_A_DAY +
                            interval.hours + (interval.minutes > 1 ? 1 : 0);
                    //4. Charge penalty
                    mPenaltyCharge = mNumPenaltyHours * penaltyPrices.getHourlyCharge();
                    fees += mPenaltyCharge;

                    mIsPenaltyChargeApplied = true;
                }
            }


            //4. Charge tier pricing for the non penalty hours

            if(mIsPenaltyChargeApplied) {
                interval = difference.getDateDifference(entryDateTime, closingTimeObj);
            }else{
                interval = difference.getDateDifference(entryDateTime,exitDateTime);
            }

            fees += calculateParkingFees(pricing,interval,vehicleType,isTicketLost);


            return fees;

        }

        private double calculateParkingFees(PricingPerVehicleTypeTbl pricing,
                                            DateUtil.Interval difference,int vehicleType,
                                            boolean isTicketLost){

            double fees = 0.00;
            int status = difference.getIntervalStatus();

            if (status == Constants.DATEDIFF_HOURLY_BOOKING) {
                int minutes = difference.getMinutes();
                int hours = difference.getHours();

                if (hours == 0 && minutes == 0) {
                    hours = 1;
                } else {
                    hours += minutes > 0 ? 1 : 0;
                }

                //Hourly Booking
                if (pricing.isPricingTiered()) {
                    String key = Constants.CACHE_PREFIX_PRICE_AT_HOUR_QUERY + vehicleType +
                            Integer.toString(hours);
                    fees = (Double) Cache.getFromCache(getApplicationContext(),key,
                            Constants.TYPE_DOUBLE);

                } else {
                    fees = pricing.getHourlyPrice() * hours;
                }

                mDurationDescription = difference.describe();

            } else if (status == Constants.DATEDIFF_DAILY_BOOKING) {
                //
                int numDays = difference.getDays();
                double dailyPrice = pricing.getDailyPrice();

                if (dailyPrice > 0) {
                    fees = pricing.getDailyPrice() * numDays;
                }

                int minutes = difference.getMinutes();
                int hours = difference.getHours();

                if (hours == 0 && minutes == 0) {
                    hours = 1;
                } else {
                    hours += minutes > 0 ? 1 : 0;
                }

                if (pricing.isPricingTiered()) {
                    String key = Constants.CACHE_PREFIX_PRICE_AT_HOUR_QUERY + vehicleType +
                            Integer.toString(hours);
                    fees += (Double) Cache.getFromCache(getApplicationContext(),key,
                            Constants.TYPE_DOUBLE);

                    if (dailyPrice <= 0) {
                        key = Constants.CACHE_PREFIX_PRICE_AT_HOUR_QUERY + vehicleType +
                                Integer.toString(Constants.NUM_HOURS_IN_A_DAY - 1);
                        fees += numDays * (Double) Cache.getFromCache(getApplicationContext(),
                                key,Constants.TYPE_DOUBLE);
                    }
                } else {
                    fees += pricing.getHourlyPrice() * hours;

                    if (dailyPrice <= 0) {
                        fees += numDays * Constants.NUM_HOURS_IN_A_DAY * pricing.getHourlyPrice();
                    }
                }

                mDurationDescription = difference.describe();

            } else {
                //mTicketIdView.setError(getString(R.string.error_clock_not_correct));
                //mTicketIdView.requestFocus();
                return Constants.ERROR_INVALID_CLOCK;

            }


            if(pricing.isNightChargeAvailable()) {
                int nights = difference.getNights();

                if (nights > 0) {
                    NightChargeTbl nightCharge = pricing.getNightCharges();
                    mNightCharge = nightCharge.getDailyCharge() * nights;
                    fees += mNightCharge;
                    mIsNightChargeApplied = true;
                    mNumNights = nights;
                }
            }

            if(isTicketLost && pricing.getLostTicketCharge() > 0.00){
                mLostTicketCharge = (int)pricing.getLostTicketCharge();
                fees += mLostTicketCharge;
                mIsLostTicketChargeApplied = true;


            }

            return fees;

        }


        private double calculateParkingFees(long entryTime, long exitTime, int vehicleType,
                                            boolean calculatePenalty,
                                            boolean isTicketLost) {


            DateTime entryDateTime = DateTime.forInstant(entryTime, Constants.TIME_ZONE);
            DateTime exitDateTime = DateTime.forInstant(exitTime, Constants.TIME_ZONE);

            //DateTime entryDateTime = new DateTime("2016-09-08 05:33");
            //DateTime exitDateTime  = new DateTime("2016-09-08 14:17");
            SpotDb dbObj = mSpaceObjList.get(0);
            PricingPerVehicleTypeTbl pricing = dbObj.getPricing(vehicleType);

            double fees = 0.00;

            if(calculatePenalty){
                fees = calculateParkingFeesWithPenalty(pricing,entryTime,exitTime,vehicleType,
                                                        isTicketLost);
            }else {

                DateUtil dateDifference = new DateUtil();
                DateUtil.Interval differenceInterval = dateDifference.getDateDifference(entryDateTime,
                        exitDateTime);

                fees = calculateParkingFees(pricing,differenceInterval,vehicleType,isTicketLost);
            }


            return fees;
        }



    }

    /**
     * Represents an asynchronous logExitTask task used to log the Exit
     */
    public class logPassExitTask extends AsyncTask<Void, Void, Boolean> {
        String mBookingId;
        String mFailureCause;
        SimplyParkTransactionTbl mTransaction;
        SimplyParkBookingTbl mBooking;
        List<SpotDb> mSpaceObjList;
        Date entryTime;
        Date exitTime;
        long mStartDateTime;
        long mEndDateTime;
        boolean mIsPenaltyApplied;
        double mParkingFees = 0;
        int mNumPenaltyHours = 0;
        int mLostPassCharge = 0;
        int mPenaltyCharge = 0;
        logPassExitTask(String bookingId) {
            mBookingId = bookingId;
            mFailureCause = "";
            mTransaction = null;
            mBooking = null;
            mStartDateTime = 0;
            mEndDateTime = 0;
            exitTime = new Date();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            int mSpaceId = (Integer) Cache.getFromCache(mContext,
                    Constants.CACHE_PREFIX_SPACEID_QUERY,
                    Constants.TYPE_INT);

            mSpaceObjList = SpotDb.find(SpotDb.class, "m_space_id = ?", Integer.toString(mSpaceId));


            String[] queryParams = {mBookingId, mBookingId};
            List<SimplyParkBookingTbl> bookingList =
                    SimplyParkBookingTbl.find(SimplyParkBookingTbl.class,
                            "m_booking_id = ? OR m_encoded_pass_code = ?", queryParams);

            if (bookingList.isEmpty()) {
                mFailureCause = "Invalid Booking Pass.Exit Cannot be recorded.";
                return false;
            } else {


                long now = DateTime.now(Constants.TIME_ZONE).getMilliseconds(Constants.TIME_ZONE);
                boolean isValid = false;
                for (ListIterator<SimplyParkBookingTbl> iter = bookingList.listIterator();
                     iter.hasNext(); ) {
                    mBooking = iter.next();
                    mBookingId = mBooking.getBookingID(); //the qr scan result is encoded qr code
                                                          //or the booking ID

                    if(mBooking.getNumBookedSlots() > 1){ //bulkbooking
                        mFailureCause = "Please exit through Vehicle Number/Parking Id";
                        return false;
                    }else{
                        mStartDateTime = mBooking.getBookingStartTime();
                        mEndDateTime = mBooking.getBookingEndTime();


                        if(!(mStartDateTime <= now && mEndDateTime >= now)){
                            continue;
                        }
                        String vehicleReg = mBooking.getVehicleReg();
                        String[] qParams = {mBookingId,vehicleReg,
                                           Integer.toString(Constants.PARKING_STATUS_ENTERED)};
//                        List<SimplyParkTransactionTbl> allTrans = SimplyParkTransactionTbl.listAll(SimplyParkTransactionTbl.class);
                        List<SimplyParkTransactionTbl> transList =
                                SimplyParkTransactionTbl.find(SimplyParkTransactionTbl.class,
                                        "m_booking_id = ? and m_vehicle_reg = ? and m_status = ?",
                                        qParams);
                        SimplyParkTransactionTbl transaction = null;

                        if(transList.isEmpty()) {
                            //the booking is found but this app has no entry. We are going to assume
                            //that the entry happened through some other gate and the app isnt synced
                            //yet. However, we have no parking ID to sync the server. Therefore, we
                            //are going to save a pending exit with car reg and exit datetime and
                            //update the server when the entry datetime syncs with the app
                            isValid = true;
                            break;

                        }else{
                            mTransaction = transList.get(0);
                            isValid = true;
                            break;
                        }

                    }
                }

                if(isValid){
                    if(mTransaction != null) {
                        entryTime = new Date(mTransaction.getEntryTime());
                        exitTime = new Date(now);
                        SpotDb dbObj = mSpaceObjList.get(0);
                        PricingPerVehicleTypeTbl pricing = dbObj.getPricing(mTransaction.getVehicleType());

                        String penaltyOnPassKey = Constants.CACHE_PREFIX_IS_PENALTY_ON_MONTHLY_PASS_AVAILABLE_QUERY;
                        mIsPenaltyApplied=(boolean)Cache.getFromCache(getApplicationContext(),penaltyOnPassKey,Constants.TYPE_BOOL);
                        if(mIsPenaltyApplied) {
                            mParkingFees = calculateParkingPenulty(pricing,entryTime.getTime(), exitTime.getTime(),
                                    mTransaction.getVehicleType(), true);
                        }
                        if(mParkingFees>0) {
                            mTransaction.setExitTime(now);
                            mTransaction.setStatus(Constants.PARKING_STATUS_EXITED);
                            mTransaction.save();

                            mBooking.decrementSlotsInUse(getApplicationContext(), mBooking.getVehicleType());
                            mBooking.save();
                        }
                    }else{
                        PendingExits pendingExit = new PendingExits();
                        pendingExit.save(mBookingId,mBooking.getVehicleReg(),now);
                    }

                    return true;
                }else{
                    if(mFailureCause.isEmpty()) {
                        mFailureCause = "No valid Booking found for this pass";
                    }
                    return false;
                }

            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mLogExitTask = null;


            if (success) {

                Intent intent = new Intent(ExitButtonActivity.this, ExitButtonSubActivity.class);


                //pass the payment due
                Bundle b = new Bundle();
                b.putBoolean(Constants.BUNDLE_PARAM_IS_EXIT_THROUGH_PASS, true);
                b.putDouble(Constants.BUNDLE_PARAM_PAYMENT_DUE, mParkingFees);
                b.putLong(Constants.BUNDLE_PARAM_EXIT_TIME, exitTime.getTime());
                b.putString(Constants.BUNDLE_PARAM_BOOKING_ID, mBookingId);
                b.putString(Constants.BUNDLE_PARAM_VEHICLE_TYPE, mBooking.getVehicleReg());
                b.putBoolean(Constants.BUNDLE_PARAM_IS_PENALTY_CHARGE_APPLIED, mIsPenaltyApplied);
                b.putInt(Constants.BUNDLE_PARAM_PENALTY_CHARGE, mPenaltyCharge);
                b.putInt(Constants.BUNDLE_PARAM_NUM_PENALTY_HOURS, mNumPenaltyHours);


                intent.putExtras(b);
                startActivityForResult(intent, Constants.EXIT_REQUEST_CODE);


//                Context context = getApplicationContext();
//
//                if(mTransaction != null) {
//                    //Intent service to update the server
//                    ServerUpdationIntentService.startActionSimplyParkLogExit(context,
//                            mTransaction.getBookingId(),
//                            mTransaction.getParkingId(),
//                            mTransaction.getExitTime(),
//                            mTransaction.getParkingFees(),
//                            mTransaction.getVehicleType(),
//                            mTransaction.getVehicleReg());
//
//                }
//                MiscUtils.displayNotification(context, "Exit Recorded", true);
//
//                //pre-emptively update the cache and the view.
//                MiscUtils.decrementOccupancyCount(context, mBooking.getVehicleType());
//
//                int resultCode = RESULT_OK;
//                Intent intent = ExitButtonActivity.this.getIntent();
//                ExitButtonActivity.this.setResult(resultCode, intent);
//                finish();
            } else {
                mTicketIdView.setError(mFailureCause);
                mTicketIdView.requestFocus();

            }

        }

        @Override
        protected void onCancelled() {
            mLogPassExitTask = null;
        }

        protected double calculateParkingPenulty(PricingPerVehicleTypeTbl pricing, Long entryTimeLong, Long exitTimeLong, int VehicleType, boolean isPassLost){
            DateTime entryDateTime = DateTime.forInstant(entryTimeLong, Constants.TIME_ZONE);
            DateTime exitDateTime = DateTime.forInstant(exitTimeLong, Constants.TIME_ZONE);

            double fees = 0.00;

            Context context = getApplicationContext();
            int penaltyHours;
            //we assume penalty of this type (stayting beyond space close timings) is NOT charged
            //on 24 hours space
            //1. calculate From Date's end of day
            String closingTimeStr = (String)Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_CLOSING_TIME_QUERY,
                    Constants.TYPE_STRING);

            String openingTimeStr = (String)Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_OPENING_TIME_QUERY,
                    Constants.TYPE_STRING);

            String[] closingTime = closingTimeStr.split(":");
            String[] openingTime = openingTimeStr.split(":");

            int closingHour = Integer.parseInt(closingTime[0]);
            int openingHour = Integer.parseInt(openingTime[0]);

            DateTime tempDateObj = null;
            if(openingHour >= closingHour){
                tempDateObj = entryDateTime.plusDays(1);
            }else{
                tempDateObj = entryDateTime;
            }

            DateTime closingTimeObj = new DateTime(tempDateObj.getYear(),tempDateObj.getMonth(),
                    tempDateObj.getDay(),closingHour,
                    Integer.parseInt(closingTime[1]),0,0);

            //2. Take DateDifference between step 1 and end time
            DateUtil difference = new DateUtil();
            DateUtil.Interval interval = difference.getDateDifference(closingTimeObj,
                    exitDateTime);

            //3. Convert the difference into hours
            if(interval.status != Constants.DATEDIFF_ERROR_FROM_TIME_GREATER_THAN_TO_TIME){
                mNumPenaltyHours = interval.days * Constants.NUM_HOURS_IN_A_DAY +
                        interval.hours + (interval.minutes > 1?1:0);
                //4. Charge penalty
                PenaltyChargeTbl penaltyPrices = pricing.getPenalty();
                mPenaltyCharge = mNumPenaltyHours * penaltyPrices.getHourlyCharge();
                fees += mPenaltyCharge;


            }

            return fees;

        }


    }
    public class LoadAutoCompleteList extends AsyncTask<Void, Void, Boolean> {
        List<TransactionTbl> activeTransactions;
        String[] mParkingIds;
        String[] mRegNumbers;
        public LoadAutoCompleteList(){

        }
        @Override
        protected Boolean doInBackground(Void... params) {
            activeTransactions = TransactionTbl.find(TransactionTbl.class,"m_status = ?",Integer.toString(Constants.PARKING_STATUS_ENTERED));
            List<SimplyParkTransactionTbl> activeSimplyParkTransactions = SimplyParkTransactionTbl.find(
                    SimplyParkTransactionTbl.class,"m_status = ?",
                    Integer.toString(Constants.PARKING_STATUS_ENTERED));
            int transactionSize = activeTransactions.size();
            int simplyParkTransactionSize = activeSimplyParkTransactions.size();

            mRegNumbers = new String[transactionSize + simplyParkTransactionSize];
            mParkingIds = new String[transactionSize + simplyParkTransactionSize];
            int idx = 0;
            // Print out the values to the log
            for(int i = 0; i < transactionSize; i++)
            {
                mRegNumbers[idx] = activeTransactions.get(i).getCarRegNumber();
                mParkingIds[idx] = activeTransactions.get(i).getParkingId();
                idx++;
            }

            // Print out the values to the log
            for(int i = 0; i < simplyParkTransactionSize; i++)
            {
                mRegNumbers[idx] = activeSimplyParkTransactions.get(i).getVehicleReg();
                mParkingIds[idx] = activeSimplyParkTransactions.get(i).getParkingId();
                idx++;
            }
            return true;
        }
        @Override
        protected void onPostExecute(final Boolean success) {
            AutoCompleteRegDropDownAdapter adapter = new AutoCompleteRegDropDownAdapter(getApplicationContext(),R.layout.autocomplete_text_item,activeTransactions,true, mTicketIdView);

            mTicketIdView.setAdapter(adapter);


//            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.autocomplete_text_item, mRegNumbers);
//            mTicketIdView.setAdapter(adapter);
//            AutoCompleteRegDropDownAdapter adapter2 = new AutoCompleteRegDropDownAdapter(getApplicationContext(),R.layout.autocomplete_text_item,activeTransactions,false);
//            ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getApplicationContext(), R.layout.autocomplete_text_item, mParkingIds);
//            mParkingIdView.setAdapter(adapter2);

        }
        @Override
        protected void onCancelled() {

        }
    }
    }
