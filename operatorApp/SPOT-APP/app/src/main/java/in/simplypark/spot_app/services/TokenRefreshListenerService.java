package in.simplypark.spot_app.services;

import com.google.android.gms.iid.InstanceIDListenerService;

public class TokenRefreshListenerService extends InstanceIDListenerService {
    public TokenRefreshListenerService() {
    }

    @Override
    public void onTokenRefresh() {
        // Fetch updated Instance ID token and notify our app's server of any changes
        // (if applicable).
            ServerUpdationIntentService.startActionRefreshToken(getApplicationContext());
        }


    }
