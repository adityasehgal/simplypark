package in.simplypark.spot_app.db_tables;

import com.orm.SugarRecord;

import in.simplypark.spot_app.config.Constants;

/**
 * Created by csharma on 07-01-2017.
 */

public class LostPassTbl extends SugarRecord {



    private int  mOperatorId;
    private String  mBookingId;
    private String  mOldEncodedPassCode;
    private String  mNewEncodedPassCode;
    private String  mOldSerialNumber;
    private String  mNewSerialNumber;
    private Long mOldPassIssueDate;
    private Long mNewPassIssueDate;
    private int mLostPassIssueFees;
    private int mPaymentMode;
    boolean mIsSyncedWithServer;

    public int getOperatorId() {
        return mOperatorId;
    }

    public void setOperatorId(int mOperatorId) {
        this.mOperatorId = mOperatorId;
    }

    public String getBookingId() {
        return mBookingId;
    }

    public void setBookingId(String mBookingId) {
        this.mBookingId = mBookingId;
    }

    public String getOldEncodedPassCode() {
        return mOldEncodedPassCode;
    }

    public void setOldEncodedPassCode(String mOldEncodedPassCode) {
        this.mOldEncodedPassCode = mOldEncodedPassCode;
    }

    public String getNewEncodedPassCode() {
        return mNewEncodedPassCode;
    }

    public void setNewEncodedPassCode(String mNewEncodedPassCode) {
        this.mNewEncodedPassCode = mNewEncodedPassCode;
    }

    public String getOldSerialNumber() {
        return mOldSerialNumber;
    }

    public void setOldSerialNumber(String mOldSerialNumber) {
        this.mOldSerialNumber = mOldSerialNumber;
    }

    public String getNewSerialNumber() {
        return mNewSerialNumber;
    }

    public void setNewSerialNumber(String mNewSerialNumber) {
        this.mNewSerialNumber = mNewSerialNumber;
    }

    public Long getOldPassIssueDate() {
        return mOldPassIssueDate;
    }

    public void setOldPassIssueDate(Long mOldPassIssueDate) {
        this.mOldPassIssueDate = mOldPassIssueDate;
    }

    public Long getNewPassIssueDate() {
        return mNewPassIssueDate;
    }

    public void setNewPassIssueDate(Long mNewPassIssueDate) {
        this.mNewPassIssueDate = mNewPassIssueDate;
    }

    public int getLostPassIssueFees() {
        return mLostPassIssueFees;
    }

    public void setLostPassIssueFees(int mLostPassIssueFees) {
        this.mLostPassIssueFees = mLostPassIssueFees;
    }

    public LostPassTbl()
    {
        this.mPaymentMode = Constants.PAYMENT_MODE_CASH;
        this.mIsSyncedWithServer = false;
    }

    public int getPaymentMode() {

        return mPaymentMode;
    }

    public void setPaymentMode(int mPaymentMode) {
        this.mPaymentMode = mPaymentMode;
    }
    public void setIsSyncedWithServer(boolean isSynced){ mIsSyncedWithServer = isSynced;}
    public boolean getIsSyncedWithServer(){ return mIsSyncedWithServer;}

    @Override
    public String toString() {
        return "LostPassTbl{" +
                "mOperatorId=" + mOperatorId +
                ", mBookingId='" + mBookingId + '\'' +
                ", mOldEncodedPassCode='" + mOldEncodedPassCode + '\'' +
                ", mNewEncodedPassCode='" + mNewEncodedPassCode + '\'' +
                ", mOldSerialNumber='" + mOldSerialNumber + '\'' +
                ", mNewSerialNumber='" + mNewSerialNumber + '\'' +
                ", mOldPassIssueDate=" + mOldPassIssueDate +
                ", mNewPassIssueDate=" + mNewPassIssueDate +
                ", mLostPassIssueFees=" + mLostPassIssueFees +
                ", mPaymentMode=" + mPaymentMode +
                ", mIsSyncedWithServer=" + mIsSyncedWithServer +
                '}';
    }
}
