package in.simplypark.spot_app.utils;

import android.content.Context;

import in.simplypark.spot_app.config.Constants;

/**
 * Created by csharma on 28-12-2016.
 */

public class CacheParams{
    public int getSpaceId() {
        return spaceId;
    }

    public void setSpaceId(int spaceId) {
        this.spaceId = spaceId;
    }

    public int getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }

    public int getSlots() {
        return slots;
    }

    public void setSlots(int slots) {
        this.slots = slots;
    }

    public String getSpaceName() {
        return spaceName;
    }

    public void setSpaceName(String spaceName) {
        this.spaceName = spaceName;
    }

    public boolean isParkAndRideAvailable() {
        return isParkAndRideAvailable;
    }

    public void setParkAndRideAvailable(boolean parkAndRideAvailable) {
        isParkAndRideAvailable = parkAndRideAvailable;
    }


    public int getBikePricingRatio() {
        return bikePricingRatio;
    }

    public void setBikePricingRatio(int bikePricingRatio) {
        this.bikePricingRatio = bikePricingRatio;
    }

    public boolean isMultiDayParkingPossible() {
        return isMultiDayParkingPossible;
    }

    public void setMultiDayParkingPossible(boolean multiDayParkingPossible) {
        isMultiDayParkingPossible = multiDayParkingPossible;
    }

    public String getOpeningDateTime() {
        return openingDateTime;
    }

    public void setOpeningDateTime(String openingDateTime) {
        this.openingDateTime = openingDateTime;
    }

    public String getClosingDateTime() {
        return closingDateTime;
    }

    public void setClosingDateTime(String closingDateTime) {
        this.closingDateTime = closingDateTime;
    }

    public boolean isOpen24Hours() {
        return open24Hours;
    }

    public void setOpen24Hours(boolean open24Hours) {
        this.open24Hours = open24Hours;
    }

    public boolean isParkingPrepaid() {
        return isParkingPrepaid;
    }

    public void setParkingPrepaid(boolean parkingPrepaid) {
        isParkingPrepaid = parkingPrepaid;
    }

    public int getNumHoursPrepaid() {
        return numHoursPrepaid;
    }

    public void setNumHoursPrepaid(int numHoursPrepaid) {
        this.numHoursPrepaid = numHoursPrepaid;
    }

    public double getNightCharge() {
        return nightCharge;
    }

    public void setNightCharge(double nightCharge) {
        this.nightCharge = nightCharge;
    }

    public boolean isSlotOccupiedAssumed() {
        return isSlotOccupiedAssumed;
    }

    public void setSlotOccupiedAssumed(boolean slotOccupiedAssumed) {
        isSlotOccupiedAssumed = slotOccupiedAssumed;
    }

    public int getLostTicketCharge() {
        return lostTicketCharge;
    }

    public void setLostTicketCharge(int lostTicketCharge) {
        this.lostTicketCharge = lostTicketCharge;
    }

    public String getSpaceOwnerName() {
        return spaceOwnerName;
    }

    public void setSpaceOwnerName(String spaceOwnerName) {
        this.spaceOwnerName = spaceOwnerName;
    }

    public String getSpaceOwnerMobile() {
        return spaceOwnerMobile;
    }

    public void setSpaceOwnerMobile(String spaceOwnerMobile) {
        this.spaceOwnerMobile = spaceOwnerMobile;
    }

    public int getNumBikesInSingleSlot() {
        return numBikesInSingleSlot;
    }

    public void setNumBikesInSingleSlot(int numBikesInSingleSlot) {
        this.numBikesInSingleSlot = numBikesInSingleSlot;
    }

    public boolean isSpaceBikeOnly() {
        return isSpaceBikeOnly;
    }

    public void setSpaceBikeOnly(boolean spaceBikeOnly) {
        isSpaceBikeOnly = spaceBikeOnly;
    }

    public boolean isGateWiseReportsRequired() {
        return isGateWiseReportsRequired;
    }

    public void setGateWiseReportsRequired(boolean gateWiseReportsRequired) {
        isGateWiseReportsRequired = gateWiseReportsRequired;
    }

    public String getDefaultVehicleType() {
        return defaultVehicleType;
    }

    public void setDefaultVehicleType(String defaultVehicleType) {
        this.defaultVehicleType = defaultVehicleType;
    }

    public void setIsReusablePrintTokenUsed(boolean isPrintTokenUsed){
        isReusablePrintTokenUsed = isPrintTokenUsed;
    }

    public boolean isCancelUpdateAllowed() {
        return isCancelUpdateAllowed;
    }

    public void setCancelUpdateAllowed(boolean isCancelUpdateAllowed) {
        this.isCancelUpdateAllowed = isCancelUpdateAllowed;
    }



    public boolean isReusablePrintTokenUsed(){
        return isReusablePrintTokenUsed;
    }

    private int spaceId;
    private int operatorId;
    private int slots;
    private String spaceName;
    private boolean isParkAndRideAvailable;
    private int bikePricingRatio;
    private int numBikesInSingleSlot;
    private boolean isMultiDayParkingPossible;
    private String openingDateTime;
    private String closingDateTime;
    private boolean open24Hours;
    private boolean isParkingPrepaid;
    int numHoursPrepaid;
    private Double nightCharge;
    private boolean isSlotOccupiedAssumed;
    private int lostTicketCharge;
    private String spaceOwnerName;
    private String spaceOwnerMobile;
    private boolean isSpaceBikeOnly;
    private boolean isGateWiseReportsRequired;
    private String defaultVehicleType;
    private boolean isReusablePrintTokenUsed;
    private boolean isCancelUpdateAllowed;


    public CacheParams(){

    }
    public void setCache(Context context) {

         Cache.setInCache(context,
                 Constants.CACHE_PREFIX_IS_CANCELLATION_UPDATION_ALLOWED,
                 this.isCancelUpdateAllowed(),Constants.TYPE_BOOL);

        //is Logged In
        Cache.setInCache(context,
                Constants.CACHE_PREFIX_LOGIN_QUERY,
                true,
                Constants.TYPE_BOOL);

        //spaceId
        Cache.setInCache(context,
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                this.getSpaceId(),
                Constants.TYPE_INT);

        //operatorId
        Cache.setInCache(context,
                Constants.CACHE_PREFIX_OPERATOR_QUERY,
                this.getOperatorId(),
                Constants.TYPE_INT);

        //slots
        Cache.setInCache(context,
                Constants.CACHE_PREFIX_NUM_SLOTS_QUERY,
                this.getSlots(),
                Constants.TYPE_INT);

        //spaceName
        Cache.setInCache(context,
                Constants.CACHE_PREFIX_SPACE_NAME_QUERY,
                this.getSpaceName(),
                Constants.TYPE_STRING);

        //is Park And Ride available
        Cache.setInCache(context,
                Constants.CACHE_PREFIX_IS_PARK_AND_RIDE_AVAIL_QUERY,
                this.isParkAndRideAvailable(),
                Constants.TYPE_BOOL);

        //set bikes to slot ratio
        Cache.setInCache(context,
                Constants.CACHE_PREFIX_BIKES_TO_SLOT_RATIO,
                this.getNumBikesInSingleSlot(),
                Constants.TYPE_INT);
/*
            //bike pricing ratio
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_BIKE_PRICING_RATIO,
                    params.getBikePricingRatio(),Constants.TYPE_INT);
*/
        //openingDateTime
        Cache.setInCache(context,
                Constants.CACHE_PREFIX_OPENING_DATE_TIME_QUERY,
                this.getOpeningDateTime(),Constants.TYPE_STRING);

        //closingDateTime
        Cache.setInCache(context,
                Constants.CACHE_PREFIX_CLOSING_DATE_TIME_QUERY,
                this.getClosingDateTime(),Constants.TYPE_STRING);

        //store the opening and closing time (just time) seperately
        //to help us store summaries

        Cache.setInCache(context,
                Constants.CACHE_PREFIX_CLOSING_TIME_QUERY,
                MiscUtils.getClosingTimeFromDateTime(this.getClosingDateTime()),
                Constants.TYPE_STRING);

        Cache.setInCache(context,
                Constants.CACHE_PREFIX_OPENING_TIME_QUERY,
                MiscUtils.getOpeningTimeFromDateTime(this.getOpeningDateTime()),
                Constants.TYPE_STRING);

        Cache.setInCache(context,
                Constants.CACHE_PREFIX_OPERATING_HOURS_SET_QUERY,
                true,Constants.TYPE_BOOL);



        //is Slot occupied assumed
        Cache.setInCache(context,
                Constants.CACHE_PREFIX_IS_SLOT_OCCUPIED_ASSUMED_QUERY,
                this.isSlotOccupiedAssumed(),Constants.TYPE_BOOL);

        //is open 24 hours
        Cache.setInCache(context,
                Constants.CACHE_PREFIX_IS_OPEN_24_HOURS,
                this.isOpen24Hours(), Constants.TYPE_BOOL);
        //multiday parking
        Cache.setInCache(context,
                Constants.CACHE_PREFIX_IS_MULTI_DAY_PARKING_POSSIBLE_QUERY,
                this.isMultiDayParkingPossible(),Constants.TYPE_BOOL);

        //isParkingPrepaid
        Cache.setInCache(context,
                Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY,
                this.isParkingPrepaid(),Constants.TYPE_BOOL);

        //set numHours
        Cache.setInCache(context,
                Constants.CACHE_PREFIX_NUM_PREPAID_HOURS_QUERY,
                this.getNumHoursPrepaid(),Constants.TYPE_INT);


        //lostTicket charge
        Cache.setInCache(context,
                Constants.CACHE_PREFIX_LOST_TICKET_CHARGE,this.getLostTicketCharge(),Constants.TYPE_INT);

        //spaceOwner Name
        Cache.setInCache(context,
                Constants.CACHE_PREFIX_SPACE_OWNER_NAME_QUERY,this.getSpaceOwnerName(),
                Constants.TYPE_STRING);

        //spaceOwner Mobile
        Cache.setInCache(context,
                Constants.CACHE_PREFIX_SPACE_OWNER_MOBILE_QUERY,this.getSpaceOwnerMobile(),
                Constants.TYPE_STRING);

        Cache.setInCache(context,
                Constants.CACHE_PREFIX_IS_SPACE_BIKE_ONLY_QUERY,
                this.isSpaceBikeOnly(),Constants.TYPE_BOOL);

        Cache.setInCache(context,
                Constants.CACHE_PREFIX_SPACE_VEHICLE_TYPE_QUERY,
                true,Constants.TYPE_BOOL);

        Cache.setInCache(context,
                Constants.CACHE_PREFIX_IS_GATEWISE_REPORT_REQUIRED_QUERY,
                this.isGateWiseReportsRequired(),Constants.TYPE_BOOL);

        Cache.setInCache(context,
                Constants.CACHE_PREFIX_DEFAULT_VEHICLE_TYPE,
                this.getDefaultVehicleType(),Constants.TYPE_STRING);

        //reusable print token
        Cache.setInCache(context,
                Constants.CACHE_PREFIX_IS_REUSABLE_TOKEN_USED,this.isReusablePrintTokenUsed(),
                Constants.TYPE_BOOL);


    }

}