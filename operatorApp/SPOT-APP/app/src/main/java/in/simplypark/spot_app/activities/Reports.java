package in.simplypark.spot_app.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import hirondelle.date4j.DateTime;
import in.simplypark.spot_app.db_tables.ReusableTokensTbl;
import in.simplypark.spot_app.db_tables.LostPassTbl;
import in.simplypark.spot_app.utils.Cache;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.utils.DateUtil;
import in.simplypark.spot_app.utils.MiscUtils;
import in.simplypark.spot_app.R;
import in.simplypark.spot_app.listelements.ReportElements;
import in.simplypark.spot_app.adapters.CustomListAdapter;
import in.simplypark.spot_app.db_tables.SimplyParkBookingTbl;
import in.simplypark.spot_app.db_tables.SimplyParkTransactionTbl;
import in.simplypark.spot_app.db_tables.TransactionTbl;
import in.simplypark.spot_app.printer.PrinterService;
import in.simplypark.spot_app.services.ServerUpdationIntentService;

public class Reports extends AppCompatActivity {

    private CustomListAdapter mCustomListAdapter;
    private ListView mListTransactions;
    private ArrayList<ReportElements> mElements;
    private View mProgressView;
    private TransactionHistoryTask mTransactionsHistoryTask;
    private CancelTransactionTask  mCancelTransactionTask;
    private CancelSpTransactionTask  mCancelSpTransactionTask;
    private View mSummaryAndListView;
    private List<TransactionTbl> mTransactionsList;
    private List<SimplyParkTransactionTbl> mSimplyParkTransactionsList;
    List<LostPassTbl> mLostPassList;
    private EditText mSearchView;

    private int mDailyEarning;
    private int mVehiclesInside;
    private int mParkedBikes;
    private int mParkedCars;
    private int mBikesInside;
    private int mCarsInside;
    private long mFrom,mTo;
    private Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_reports);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mContext = getApplicationContext();

        mElements = new ArrayList<ReportElements>();

        mSummaryAndListView = findViewById(R.id.cr_summary_and_list_view);
        mListTransactions = (ListView) findViewById(R.id.transactions_history);
        mCustomListAdapter = new CustomListAdapter(Reports.this, mElements);

        mListTransactions.setAdapter(mCustomListAdapter);
        mProgressView = findViewById(R.id.cr_login_progress);
        mFrom = 0;
        mTo = 0;
        Bundle passedParams = getIntent().getExtras();


        if (passedParams != null) {
            mFrom = passedParams.getLong(Constants.BUNDLE_PARAM_REPORT_DATETIME_FROM);
            mTo = passedParams.getLong(Constants.BUNDLE_PARAM_REPORT_DATETIME_TO);

        }

        showProgress(true);



        mListTransactions.setLongClickable(true);


        mListTransactions.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                final int mPosition = position;


                ReportElements element = mCustomListAdapter.get((int) id);
                final int positionInArrayList = element.getPosition();
                final boolean isSimplyParkTransaction = element.getIsSimplyParkTransaction();
                String textToDisplay = "";

                if (!isSimplyParkTransaction) {
                    TransactionTbl transaction = mTransactionsList.get(element.getPosition());

                    if (transaction.getParkingStatus() == Constants.PARKING_STATUS_ENTERED) {
                        textToDisplay = "Cancel Entry?";
                    }else if (transaction.getParkingStatus() == Constants.PARKING_STATUS_EXITED){
                        textToDisplay = "Cancel Exit?";
                    }
                } else {

                    SimplyParkTransactionTbl transaction = mSimplyParkTransactionsList.get(
                            element.getPosition());

                    if (transaction.getStatus() == Constants.PARKING_STATUS_ENTERED) {
                        textToDisplay = "Cancel Entry?";
                    } else if (transaction.getStatus() == Constants.PARKING_STATUS_EXITED){
                        textToDisplay = "Cancel Exit?";
                    }
                }

                //Toast.makeText(getApplicationContext(), " " + position , Toast.LENGTH_LONG).show();
                android.support.v7.app.AlertDialog.Builder builder =
                        new android.support.v7.app.AlertDialog.Builder(Reports.this);
                // Add the buttons
                builder.setPositiveButton("Re-Print",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                handlePrintAgainButton(isSimplyParkTransaction, positionInArrayList);
                            }
                        });
                if((Boolean) Cache.getFromCache(mContext,
                                           Constants.CACHE_PREFIX_IS_CANCELLATION_UPDATION_ALLOWED,
                                           Constants.TYPE_BOOL) && !textToDisplay.isEmpty()) {
                    builder.setNegativeButton(textToDisplay,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    handleCancelTransactionButton(isSimplyParkTransaction,
                                                                  mPosition, positionInArrayList);

                                }
                            });
                }

                android.support.v7.app.AlertDialog dialog = builder.create();
                dialog.show();


                return true;

            }
        });

        mSearchView = (EditText)findViewById(R.id.cr_search_id);
        mSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mCustomListAdapter.getFilter().filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mTransactionsHistoryTask = new TransactionHistoryTask(mFrom, mTo);

        mTransactionsHistoryTask.execute((Void) null);

    }

    public void handleEmailButton(View view){

    }

    public void handleCancelTransactionButton(boolean isSimplyParkTransaction,
                                              int positionInList,
                                              int positionInArrayList){
        if(!isSimplyParkTransaction){
            handleOfflineCancelTransactionButton(positionInList,positionInArrayList);
        }else{
            handleSpCancelTransaction(positionInList,positionInArrayList);
        }
    }

    public void handleSpCancelTransaction(int positionInList,int positionInArrayList){
        SimplyParkTransactionTbl transaction = mSimplyParkTransactionsList.get(positionInArrayList);
        showProgress(true);
        mCancelSpTransactionTask = new CancelSpTransactionTask(positionInArrayList,
                                                     positionInList,transaction.getParkingId());
        mCancelSpTransactionTask.execute((Void)null);


    }

    public void handleOfflineCancelTransactionButton(int positionInList,int positionInArrayList){
        TransactionTbl transaction = mTransactionsList.get(positionInArrayList);
        showProgress(true);
        mCancelTransactionTask = new CancelTransactionTask(positionInArrayList,positionInList,
                                                           transaction.getParkingId());
        mCancelTransactionTask.execute((Void)null);


    }

    public void handlePrintAgainButton(boolean isSimplyParkTransaction, int index){
        if(!isSimplyParkTransaction){
            handleOfflinePrintAgainButton(index);
        }else{
            handleSpPrintAgainButton(index);
        }
    }

    public void handleSpPrintAgainButton(int index) {
        SimplyParkTransactionTbl transaction = mSimplyParkTransactionsList.get(index);
        Context context = getApplicationContext();
        int parkingStatus = transaction.getStatus();

        //if(parkingStatus == Constants.PARKING_STATUS_ENTERED ) {
            DateTime entryDateTime = DateTime.forInstant(transaction.getEntryTime(),
                        Constants.TIME_ZONE);
                //print parking receipt
            long entryTime   = transaction.getEntryTime();
            String regNumber = transaction.getVehicleReg();
            String parkingId = transaction.getParkingId();
            boolean isParkAndRide = transaction.getIsParkAndRide();
            DateTime dateTime = DateTime.forInstant(entryTime, Constants.TIME_ZONE);
            String entryTimeStr =  dateTime.format("DD/MM/YY hh:mm");
            int    vehicleType  = transaction.getVehicleType();

            PrinterService.startActionPrintEntryTicket(context,
                        entryTimeStr,
                        vehicleType+parkingId,
                        isParkAndRide,
                        regNumber,
                        vehicleType);

        //}

    }

    public void handleOfflinePrintAgainButton(int index){
        TransactionTbl transaction = mTransactionsList.get(index);
        Context context = getApplicationContext();
        int parkingStatus = transaction.getParkingStatus();

        boolean isParkingPrepaid = (Boolean) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY,
                Constants.TYPE_BOOL);


        if(parkingStatus == Constants.PARKING_STATUS_ENTERED){
            DateTime entryDateTime = DateTime.forInstant(transaction.getEntryTime(),
                                                         Constants.TIME_ZONE);
            //print parking receipt
            long entryTime   = transaction.getEntryTime();
            String regNumber = transaction.getCarRegNumber();
            String parkingId = transaction.getParkingId();
            boolean isParkAndRide = transaction.getIsParkAndRide();
            DateTime dateTime = DateTime.forInstant(entryTime, Constants.TIME_ZONE);
            String entryTimeStr =  dateTime.format("DD/MM/YYYY hh:mm");
            int    vehicleType  = transaction.getVehicleType();

            PrinterService.startActionPrintEntryTicket(context,
                                                       entryTimeStr,
                                                       vehicleType+parkingId,
                                                       isParkAndRide,
                                                       regNumber,
                                                       vehicleType);

        }else if(parkingStatus == Constants.PARKING_STATUS_EXITED){
            DateTime eDt = (DateTime.forInstant(transaction.getEntryTime(), Constants.TIME_ZONE));
            String entryTimeStr =  eDt.format("DD-MM-YYYY hh:mm");

            DateTime exDt = (DateTime.forInstant(transaction.getExitTime(), Constants.TIME_ZONE));
            String exitTimeStr =  exDt.format("DD-MM-YYYY hh:mm");

            DateTime entryDateTime = DateTime.forInstant(transaction.getEntryTime(),
                                                         Constants.TIME_ZONE);
            DateTime exitDateTime  = DateTime.forInstant(transaction.getExitTime(),
                    Constants.TIME_ZONE);

            DateUtil dateDifference = new DateUtil();
            DateUtil.Interval differenceInterval = dateDifference.getDateDifference(entryDateTime,
                    exitDateTime);

            String durationDescription = differenceInterval.describe();
            int parkingFees = (int)Math.ceil(transaction.getParkingFees());

            String extraChargeDescription = "";
            if(!transaction.getNightChargeDescription().isEmpty()){
                extraChargeDescription = transaction.getNightChargeDescription();
            }else if (!transaction.getPenaltyChargeDescription().isEmpty()){
                extraChargeDescription = transaction.getPenaltyChargeDescription();
            }

            if(isParkingPrepaid){
                PrinterService.startActionPrintExitTicket(context,entryTimeStr, "",
                        Integer.toString(parkingFees),
                        extraChargeDescription,transaction.getCarRegNumber(),
                        "",transaction.getVehicleType());

            }else {
                PrinterService.startActionPrintExitTicket(context, entryTimeStr, exitTimeStr,
                        Integer.toString(parkingFees), extraChargeDescription,
                        transaction.getCarRegNumber(), durationDescription,
                        transaction.getVehicleType());
            }

        }else if(parkingStatus == Constants.PARKING_STATUS_EXIT_NOT_RECORDED) {

            String extraChargeDescription = "";
            if(!transaction.getNightChargeDescription().isEmpty()){
                extraChargeDescription = transaction.getNightChargeDescription();
            }else if (!transaction.getPenaltyChargeDescription().isEmpty()){
                extraChargeDescription = transaction.getPenaltyChargeDescription();
            }



            if (isParkingPrepaid) {
                Integer fees = (int) transaction.getParkingFees();
                DateTime eDt = (DateTime.forInstant(transaction.getEntryTime(), Constants.TIME_ZONE));
                String entryTimeStr = eDt.format("DD-MM-YYYY hh:mm");

                PrinterService.startActionPrintExitTicket(context,
                        entryTimeStr, "", Integer.toString(fees), extraChargeDescription,
                        transaction.getCarRegNumber(), "", transaction.getVehicleType());

            }else{
                DateTime entryDateTime = DateTime.forInstant(transaction.getEntryTime(),
                        Constants.TIME_ZONE);
                //print parking receipt
                long entryTime   = transaction.getEntryTime();
                String regNumber = transaction.getCarRegNumber();
                String parkingId = transaction.getParkingId();
                boolean isParkAndRide = transaction.getIsParkAndRide();
                DateTime dateTime = DateTime.forInstant(entryTime, Constants.TIME_ZONE);
                String entryTimeStr =  dateTime.format("DD/MM/YYYY hh:mm");
                int    vehicleType  = transaction.getVehicleType();

                PrinterService.startActionPrintEntryTicket(context,
                        entryTimeStr,
                        vehicleType+parkingId,
                        isParkAndRide,
                        regNumber,
                        vehicleType);

            }
        }


    }



    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mSummaryAndListView.setVisibility(show ? View.GONE : View.VISIBLE);
            mSummaryAndListView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mSummaryAndListView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mSummaryAndListView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous transaction cancel taskl
     */

    public class CancelTransactionTask extends AsyncTask<Void,Void,Boolean> {
        String mParkingId;
        int    mPosition;
        int    mPositionInArrayList;
        boolean mUpdate= false;
        String mEntryTimeStr;

        CancelTransactionTask(int positionInArrayList,int position,String parkingId) {
            mParkingId = parkingId;
            mPosition  = position;
            mPositionInArrayList = positionInArrayList;
            mEntryTimeStr = "";

        }

        @Override
        protected Boolean doInBackground(Void... params) {
            List<TransactionTbl> transactionTblList = TransactionTbl.find(TransactionTbl.class,
                    "m_parking_id = ?", mParkingId);

            long openingTime = MiscUtils.getLongOpeningTimeForToday(
                    getApplicationContext());

            long closingTime = MiscUtils.getLongClosingTimeForToday(
                    getApplicationContext());

            if(transactionTblList.isEmpty()){
                return false;
            }else{
                TransactionTbl transaction = transactionTblList.get(0);

                int parkingStatus = transaction.getParkingStatus();
                Context context = getApplicationContext();

                if(parkingStatus == Constants.PARKING_STATUS_ENTERED){
                    transaction.setParkingStatus(Constants.PARKING_STATUS_TRANSACTION_CANCELLED);
                    transaction.setIsUpdated(true);
                    //issue109: setting the default sync state as false
                    transaction.setIsSyncedWithServer(false);
                    transaction.save();

                    String token = transaction.getToken();

                    if(!token.isEmpty()){
                        ReusableTokensTbl.markTokenAsFree(token);
                    }

                    MiscUtils.decrementOccupancyCount(getApplicationContext(),
                                                          transaction.getVehicleType());

                    ServerUpdationIntentService.startActionTransactionCancel(context,mParkingId,
                                          transaction.getEntryTime(),transaction.getCarRegNumber(),
                                          transaction.getVehicleType());

                }else if(parkingStatus == Constants.PARKING_STATUS_EXITED){
                    boolean isParkingPrepaid = (Boolean)Cache.getFromCache(context,
                            Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY,Constants.TYPE_BOOL);
                    mDailyEarning -= transaction.getParkingFees();


                    transaction.setIsUpdated(true);
                    //issue109: setting the default sync state as false
                    transaction.setIsSyncedWithServer(false);

                    DateTime entryDt = (DateTime.forInstant(transaction.getEntryTime(),
                            Constants.TIME_ZONE));
                    mEntryTimeStr = entryDt.format("hh:mm");

                    transaction.setParkingFees(0);
                    transaction.setExitTime(0);
                    if(isParkingPrepaid){
                        transaction.setParkingStatus(Constants.PARKING_STATUS_TRANSACTION_CANCELLED);
                        transaction.save();
                        MiscUtils.decrementVehiclesParkedCount(getApplicationContext(),
                                transaction.getVehicleType());
                        ServerUpdationIntentService.startActionTransactionCancel(context,mParkingId,
                                      transaction.getEntryTime(),transaction.getCarRegNumber(),
                                      transaction.getVehicleType());

                        String token = transaction.getToken();

                        if(!token.isEmpty()){
                            ReusableTokensTbl.markTokenAsFree(token);
                        }

                    }else {
                        transaction.setParkingStatus(Constants.PARKING_STATUS_ENTERED);
                        transaction.save();
                        MiscUtils.incrementOccupancyCount(getApplicationContext(),
                                transaction.getVehicleType());

                        ServerUpdationIntentService.startActionTransactionUpdate(context,mParkingId,
                                Constants.PARKING_STATUS_ENTERED,
                                0,transaction.getEntryTime(),transaction.getCarRegNumber(),
                                transaction.getVehicleType());
                        mUpdate = true;

                        String token = transaction.getToken();

                        if(!token.isEmpty()){
                            ReusableTokensTbl.markTokenAsBusy(token);
                        }
                    }




                }else if(parkingStatus == Constants.PARKING_STATUS_EXIT_NOT_RECORDED){

                    boolean isParkingPrepaid = (Boolean)Cache.getFromCache(getApplicationContext(),
                            Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY,
                            Constants.TYPE_BOOL);

                    if(isParkingPrepaid){
                       mDailyEarning -= transaction.getParkingFees();
                       transaction.setParkingStatus(Constants.PARKING_STATUS_TRANSACTION_CANCELLED);
                       transaction.setIsUpdated(true);
                        //issue109: setting the default sync state as false
                       transaction.setIsSyncedWithServer(false);


                        transaction.save();

                       MiscUtils.decrementOccupancyCount(getApplicationContext(),
                                   transaction.getVehicleType());



                        ServerUpdationIntentService.startActionTransactionCancel(context,mParkingId,
                                transaction.getEntryTime(),transaction.getCarRegNumber(),
                                    transaction.getVehicleType());

                    }



                }


            }

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {

            showProgress(false);

            if(success){

                setTitle("Earnings: Rs." + mDailyEarning);
                View rowView = mListTransactions.getChildAt(mPosition -
                                                   mListTransactions.getFirstVisiblePosition());


                if(rowView == null) {
                    return;
                }


                TextView  heading = (TextView)rowView.findViewById(R.id.item);

                TextView  subHeading = (TextView)rowView.findViewById(R.id.textView1);
                TextView  earnings = (TextView)rowView.findViewById(R.id.earning_per_parking);

                TransactionTbl modifiedTransaction = mTransactionsList.get(mPositionInArrayList);


                if(!mUpdate) {
                    heading.setPaintFlags(heading.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    subHeading.setPaintFlags(heading.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    earnings.setPaintFlags(heading.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                    if(modifiedTransaction != null){
                        modifiedTransaction.setParkingStatus(Constants.PARKING_STATUS_TRANSACTION_CANCELLED);
                    }

                }else{
                    earnings.setText("-");
                    subHeading.setText(mEntryTimeStr);
                    if(modifiedTransaction != null){
                        modifiedTransaction.setParkingStatus(Constants.PARKING_STATUS_ENTERED);
                    }

                }

                //mCustomListAdapter.notifyDataSetChanged();




            }else{
                Toast.makeText(getApplicationContext(), "Error cancelling. Please try again" ,
                                                 Toast.LENGTH_LONG).show();
            }








        }
    }

    /**
     * Represents an asynchronous transaction cancel taskl
     */

    public class CancelSpTransactionTask extends AsyncTask<Void,Void,Boolean> {
        String mParkingId;
        int    mPosition;
        int    mPositionInArrayList;
        boolean mUpdate= false;
        String mEntryTimeStr;

        CancelSpTransactionTask(int positionInArrayList,int position,String parkingId) {
            mParkingId = parkingId;
            mPosition  = position;
            mEntryTimeStr = "";
            mPositionInArrayList = positionInArrayList;

        }

        @Override
        protected Boolean doInBackground(Void... params) {
            List<SimplyParkTransactionTbl> transactionTblList =
                                          SimplyParkTransactionTbl.find(SimplyParkTransactionTbl.class,
                                                                       "m_parking_id = ?", mParkingId);



            long openingTime = MiscUtils.getLongOpeningTimeForToday(
                    getApplicationContext());

            long closingTime = MiscUtils.getLongClosingTimeForToday(
                    getApplicationContext());

            if(transactionTblList.isEmpty()){
                return false;
            }else{
                SimplyParkTransactionTbl transaction = transactionTblList.get(0);

                List<SimplyParkBookingTbl> bookingList = SimplyParkBookingTbl.find(
                        SimplyParkBookingTbl.class,"m_booking_id = ?",transaction.getBookingId());

                if(bookingList.isEmpty()){
                    return false;
                }

                SimplyParkBookingTbl booking = bookingList.get(0);


                int parkingStatus = transaction.getStatus();

                if(parkingStatus == Constants.PARKING_STATUS_ENTERED){
                    transaction.setStatus(Constants.PARKING_STATUS_TRANSACTION_CANCELLED);
                    transaction.setIsUpdated(true);
                    //issue109: setting the default sync state as false
                    transaction.setIsSyncedWithServer(false); //resetting to true as another attempt
                    // will be made to sync (if the value
                    // was false)
                    transaction.save();

                    MiscUtils.decrementOccupancyCount(getApplicationContext(),
                            transaction.getVehicleType());

                    booking.decrementSlotsInUse(getApplicationContext(),
                                                transaction.getVehicleType());

                    booking.save();

                    ServerUpdationIntentService.startActionSpTransactionCancel(getApplicationContext(),
                            mParkingId,transaction.getEntryTime(),transaction.getVehicleReg(),
                            transaction.getVehicleType(),transaction.getBookingId(),
                            booking.getBookingType());


                }else if(parkingStatus == Constants.PARKING_STATUS_EXITED){
                    mDailyEarning -= transaction.getParkingFees();
                    transaction.setStatus(Constants.PARKING_STATUS_ENTERED);
                    transaction.setIsUpdated(true);
                    //issue109: setting the default sync state as false
                    transaction.setIsSyncedWithServer(false);


                    DateTime entryDt = (DateTime.forInstant(transaction.getEntryTime(),
                            Constants.TIME_ZONE));
                    mEntryTimeStr = entryDt.format("hh:mm");

                    transaction.setParkingFees(0);
                    transaction.setExitTime(0);
                    transaction.save();

                    MiscUtils.incrementOccupancyCount(getApplicationContext(),
                            transaction.getVehicleType());

                    booking.incrementSlotsInUse(getApplicationContext(),
                            transaction.getVehicleType());

                    booking.save();

                    ServerUpdationIntentService.startActionSpTransactionUpdate(getApplicationContext(),
                            mParkingId,
                            Constants.PARKING_STATUS_ENTERED,
                            0,transaction.getEntryTime(),transaction.getExitTime(),
                             transaction.getVehicleReg(),
                            transaction.getVehicleType(),transaction.getBookingId(),
                            booking.getBookingType());
                    mUpdate = true;

                }else if(parkingStatus == Constants.PARKING_STATUS_EXIT_NOT_RECORDED){

                    boolean isParkingPrepaid = (Boolean)Cache.getFromCache(getApplicationContext(),
                            Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY,
                            Constants.TYPE_BOOL);

                    if(isParkingPrepaid){
                        mDailyEarning -= transaction.getParkingFees();
                        transaction.setStatus(Constants.PARKING_STATUS_TRANSACTION_CANCELLED);
                        transaction.setIsUpdated(true);
                        //issue109: setting the default sync state as false
                        transaction.setIsSyncedWithServer(false);


                        transaction.save();

                        MiscUtils.decrementOccupancyCount(getApplicationContext(),
                                transaction.getVehicleType());

                        booking.decrementSlotsInUse(getApplicationContext(),
                                transaction.getVehicleType());

                        booking.save();




                        ServerUpdationIntentService.startActionSpTransactionCancel(getApplicationContext(),
                                mParkingId,transaction.getEntryTime(),transaction.getVehicleReg(),
                                transaction.getVehicleType(),transaction.getBookingId(),
                                booking.getBookingType());
                    }



                }






            }

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {

            showProgress(false);

            if(success){

                //setTitle("Earnings: Rs." + mDailyEarning);
                View rowView = mListTransactions.getChildAt(mPosition -
                        mListTransactions.getFirstVisiblePosition());


                if(rowView == null) {
                    return;
                }

                TextView  heading = (TextView)rowView.findViewById(R.id.item);

                TextView  subHeading = (TextView)rowView.findViewById(R.id.textView1);
                TextView  earnings = (TextView)rowView.findViewById(R.id.earning_per_parking);

                SimplyParkTransactionTbl modifiedTransaction = mSimplyParkTransactionsList.get(mPositionInArrayList);
                if(!mUpdate) {
                    heading.setPaintFlags(heading.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    subHeading.setPaintFlags(heading.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    earnings.setPaintFlags(heading.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                    if(modifiedTransaction != null){
                        modifiedTransaction.setStatus(Constants.PARKING_STATUS_TRANSACTION_CANCELLED);
                    }
                }else{
                    earnings.setText("-");
                    subHeading.setText(mEntryTimeStr);

                    if(modifiedTransaction != null){
                        modifiedTransaction.setStatus(Constants.PARKING_STATUS_ENTERED);
                    }
                }

                //mCustomListAdapter.notifyDataSetChanged();

            }else{
                Toast.makeText(getApplicationContext(), "Error cancelling. Please try again" ,
                        Toast.LENGTH_LONG).show();
            }








        }
    }
    /**
     * Represents an asynchronous transaction history task
     * the user.
     */
    public class TransactionHistoryTask extends AsyncTask<Void, Void, Boolean> {


        private String mFromStr, mToStr;
        private long mFrom, mTo;



        TransactionHistoryTask(long from, long to) {
           mTo = to;
           mFrom = from;
           mDailyEarning   = 0;
           mVehiclesInside = 0;
           mParkedBikes = 0;
           mParkedCars  = 0;
           mBikesInside = 0;
           mCarsInside = 0;

           mFromStr = DateTime.forInstant(from,Constants.TIME_ZONE).
                                                     format("D-MMM hh:mm a", Locale.US);

           mToStr = DateTime.forInstant(to,Constants.TIME_ZONE).
                    format("D-MMM hh:mm a", Locale.US);

        }


        @Override
        protected Boolean doInBackground(Void... params) {

            mTransactionsList = TransactionTbl.find(TransactionTbl.class,
                    "(m_exit_time >= ? AND m_exit_time <= ?) OR (m_entry_time >= ? AND m_entry_time <= ?)",
                    Long.toString(mFrom),Long.toString(mTo),
                    Long.toString(mFrom), Long.toString(mTo));

            mSimplyParkTransactionsList = SimplyParkTransactionTbl.find(
                                             SimplyParkTransactionTbl.class,
                    "(m_exit_time >= ? AND m_exit_time <= ?) OR (m_entry_time >= ? AND m_entry_time <= ?)",
                    Long.toString(mFrom),Long.toString(mTo),
                    Long.toString(mFrom), Long.toString(mTo));


            mLostPassList = LostPassTbl.find(LostPassTbl.class,
                                            "m_new_pass_issue_date >=? AND m_new_pass_issue_date <=?",
                                             Long.toString(mFrom),
                                             Long.toString(mTo));


            boolean isParkingPrepaid = (Boolean)Cache.getFromCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY,
                    Constants.TYPE_BOOL);

            addOfflineTransactions(isParkingPrepaid);

            addSimplyParkTransactions();
            if(!mLostPassList.isEmpty()){
                addLostPassTransactions();
            }

            //sort based on entry time
            Collections.sort(mElements,ReportElements.ENTRY_TIME_ORDER);

            return true;

        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mTransactionsHistoryTask = null;
            showProgress(false);

            if (success) {

              if(mElements.size() > 0) {
                  mCustomListAdapter.addAll(mElements);

                  String summaryText = "Vehicles Inside: " + mBikesInside + " (Bikes), " +
                          mCarsInside + "(Cars)";

                  Snackbar.make(mSummaryAndListView,
                          summaryText,
                          Snackbar.LENGTH_LONG).show();

                  setTitle("Earnings: Rs." + mDailyEarning);
                  mListTransactions.setLongClickable(true);
              }else{
                  ReportElements row = new ReportElements();

                  row.setHeading("No entry found");
                  row.setSubHeading("");
                  row.setParkingFess("");
                  row.setIcon(R.drawable.search_results);

                  mCustomListAdapter.add(row);
                  mListTransactions.setLongClickable(false);
              }

                mCustomListAdapter.notifyDataSetChanged();

                TextView dateView = (TextView)findViewById(R.id.cr_earning_date);
                dateView.setText("Earnings for "+mFromStr + " to " + mToStr);

            }
        }

        @Override
        protected void onCancelled() {
            mTransactionsHistoryTask = null;
            showProgress(false);
        }


        private boolean addOfflineTransactions(boolean isParkingPrepaid){
            int position = -1;
            for(Iterator<TransactionTbl> iterator = mTransactionsList.iterator();
                iterator.hasNext(); ){
                TransactionTbl transaction = iterator.next();
                boolean addEarnings = false;
                long exitTime = transaction.getExitTime();

                if(exitTime >= mFrom && exitTime <= mTo){
                    addEarnings = true;
                }



                ReportElements row = new ReportElements();

                if (transaction.getVehicleType() == Constants.VEHICLE_TYPE_CAR) {
                    row.setIcon(R.drawable.car_black_24);
                    //mParkedCars++;

                } else if (transaction.getVehicleType() == Constants.VEHICLE_TYPE_BIKE){
                    row.setIcon(R.drawable.motorcycle_black_24);
                    //mParkedBikes++;
                }else if(transaction.getVehicleType() == Constants.VEHICLE_TYPE_MINIBUS){
                    row.setIcon(R.drawable.minibus_black_24);
                    //mParkedMiniBuses++;
                }else if(transaction.getVehicleType() == Constants.VEHICLE_TYPE_BUS){
                    row.setIcon(R.drawable.bus_black_24);
                    //mParkedBuses++;
                }

                String reg = transaction.getCarRegNumber();
                if(reg.isEmpty()){
                    String parkingId = transaction.getVehicleType() +
                            transaction.getParkingId();
                    row.setHeading(parkingId);
                }else {
                    row.setHeading(transaction.getCarRegNumber());
                }

                String subHeading = "In  :";
                DateTime entryDt = (DateTime.forInstant(transaction.getEntryTime(),
                        Constants.TIME_ZONE));
                String entryTimeStr = entryDt.format("hh:mm");

                int parkingStatus = transaction.getParkingStatus();

                if(parkingStatus ==  Constants.PARKING_STATUS_EXIT_NOT_RECORDED) {

                    subHeading += entryTimeStr + ", ";
                    subHeading += "Out :N/A";

                    if(isParkingPrepaid){
                        int earning = (int)Math.ceil(transaction.getParkingFees());

                        mDailyEarning += earning;

                        String parkingFees = "Rs. " + Integer.toString(earning);


                        row.setParkingFess(parkingFees);
                    }

                }else if(parkingStatus == Constants.PARKING_STATUS_ENTERED){
                    subHeading += entryTimeStr;
                    row.setParkingFess("-");

                    mVehiclesInside++;

                    if (transaction.getVehicleType() == Constants.VEHICLE_TYPE_CAR) {
                        mCarsInside++;
                    } else {
                        mBikesInside++;
                    }

                }else if(parkingStatus == Constants.PARKING_STATUS_TRANSACTION_CANCELLED){
                    subHeading += entryTimeStr + ", ";
                    String exitTimeStr = "-";
                    String parkingFees = "-";

                    if(transaction.getExitTime() != 0){
                        DateTime exitDt = (DateTime.forInstant(transaction.getExitTime(),
                                Constants.TIME_ZONE));
                        exitTimeStr = exitDt.format("hh:mm");

                        subHeading += "Out :" + exitTimeStr;
                        int earning = (int)Math.ceil(transaction.getParkingFees());

                        parkingFees = "Rs. " + Integer.toString(earning);

                        row.setParkingFess(parkingFees);
                    }



                }
                else {

                    DateTime entryTime = DateTime.forInstant(transaction.getEntryTime(),
                                                              Constants.TIME_ZONE);

                    DateTime exitDt = (DateTime.forInstant(transaction.getExitTime(),
                            Constants.TIME_ZONE));

                    String exitTimeStr = "";

                    if(isParkingPrepaid) {
                        entryTimeStr = entryDt.format("DD-MMM hh:mm", Locale.US);
                        subHeading += entryTimeStr + ", ";
                        subHeading += "Out : N/A";
                    }else {


                        if (exitDt.isSameDayAs(entryTime)) {
                            exitTimeStr = exitDt.format("hh:mm");
                            subHeading += entryTimeStr + ", ";
                            subHeading += "Out : " + exitTimeStr;
                        } else {
                            entryTimeStr = entryDt.format("DD-MMM hh:mm", Locale.US);
                            exitTimeStr = exitDt.format("DD-MMM hh:mm", Locale.US);
                            subHeading = "In    : ";
                            subHeading += entryTimeStr;
                            subHeading += "\nOut : " + exitTimeStr;
                        }
                    }

                    if(addEarnings) {

                        int earning = (int) Math.ceil(transaction.getParkingFees());
                        mDailyEarning += earning;
                        String parkingFees = "Rs. " + Integer.toString(earning);
                        row.setParkingFess(parkingFees);
                    }else{
                        row.setParkingFess("-");
                    }

                }

                row.setSubHeading(subHeading);
                row.setParkingStatus(parkingStatus);
                row.setSyncedWithServerState(transaction.getIsSyncedWithServer());
                row.setTime(transaction.getEntryTime());
                row.setIsSimplyParkTransaction(false);
                row.setPosition(++position);


                mElements.add(row);

            }

            return true;

        }


        private boolean addSimplyParkTransactions(){
            int position = -1;
            for(Iterator<SimplyParkTransactionTbl> iterator = mSimplyParkTransactionsList.iterator();
                iterator.hasNext(); ){
                SimplyParkTransactionTbl transaction = iterator.next();

                List<SimplyParkBookingTbl> bookingList = SimplyParkBookingTbl.find(
                                      SimplyParkBookingTbl.class,"m_booking_id = ?",
                                      transaction.getBookingId());



                ReportElements row = new ReportElements();

                if (transaction.getVehicleType() == Constants.VEHICLE_TYPE_CAR) {
                    row.setIcon(R.drawable.car_black_24);
                    //mParkedCars++;

                } else {
                    row.setIcon(R.drawable.motorcycle_black_24);
                    //mParkedBikes++;

                }

                String reg = transaction.getVehicleReg();
                if(reg.isEmpty()){
                    String parkingId = transaction.getVehicleType() +
                            transaction.getParkingId();
                    row.setHeading(parkingId);
                }else {
                    row.setHeading(transaction.getVehicleReg());
                }

                String subHeading = "In  :";
                DateTime entryDt = (DateTime.forInstant(transaction.getEntryTime(),
                        Constants.TIME_ZONE));
                String entryTimeStr = entryDt.format("hh:mm");

                int parkingStatus = transaction.getStatus();

                if(parkingStatus ==  Constants.PARKING_STATUS_EXIT_NOT_RECORDED) {

                    subHeading += entryTimeStr + ", ";
                    subHeading += "Out :N/A";



                }else if(parkingStatus == Constants.PARKING_STATUS_ENTERED){
                    subHeading += entryTimeStr;
                    row.setParkingFess("-");

                    mVehiclesInside++;

                    if (transaction.getVehicleType() == Constants.VEHICLE_TYPE_CAR) {
                        mCarsInside++;
                    } else {
                        mBikesInside++;
                    }

                }else if(parkingStatus == Constants.PARKING_STATUS_TRANSACTION_CANCELLED){
                    subHeading += entryTimeStr + ", ";
                    String exitTimeStr = "-";
                    String parkingFees = "-";

                    if(transaction.getExitTime() != 0){
                        DateTime exitDt = (DateTime.forInstant(transaction.getExitTime(),
                                Constants.TIME_ZONE));
                        exitTimeStr = exitDt.format("hh:mm");

                        subHeading += "Out :" + exitTimeStr;
                        row.setParkingFess(parkingFees);
                    }
                }
                else {

                    DateTime exitDt = (DateTime.forInstant(transaction.getExitTime(),
                            Constants.TIME_ZONE));
                    String exitTimeStr = exitDt.format("hh:mm");

                    subHeading += entryTimeStr + ", ";

                    subHeading += "Out :" + exitTimeStr;

                    row.setParkingFess("-");

                }

                row.setSubHeading(subHeading);
                row.setParkingStatus(parkingStatus);
                row.setSyncedWithServerState(transaction.getIsSyncedWithServer());
                row.setIsSimplyParkTransaction(true);

                if(!bookingList.isEmpty()){
                    SimplyParkBookingTbl booking = bookingList.get(0);
                    row.setCustomer(booking.getCustomerDesc());
                }


                row.setTime(transaction.getEntryTime());
                row.setPosition(++position);

                mElements.add(row);

            }

            return true;

        }

        private boolean addLostPassTransactions(){
            int position = -1;
            for(LostPassTbl lostPass: mLostPassList){
                List<SimplyParkBookingTbl> bookingList = SimplyParkBookingTbl.find(
                        SimplyParkBookingTbl.class,"m_booking_id = ?",
                        lostPass.getBookingId());
                SimplyParkBookingTbl booking = bookingList.get(0);
                ReportElements row = new ReportElements();

                if (booking.getVehicleType() == Constants.VEHICLE_TYPE_CAR) {
                    row.setIcon(R.drawable.car_black_24);
                } else {
                    row.setIcon(R.drawable.motorcycle_black_24);
                }


                row.setHeading(booking.getVehicleReg());


                String subHeading = "Pass Lost by " + booking.getCustomerDesc() + "\n";
                subHeading += "New Pass Issued at: ";
                DateTime newPassIssueDt = (DateTime.forInstant(lostPass.getNewPassIssueDate(),
                        Constants.TIME_ZONE));
                String newPassIssueTimeStr = newPassIssueDt.format("hh:mm");
                subHeading += newPassIssueTimeStr;

                int lostPassFees = lostPass.getLostPassIssueFees();
                row.setParkingFess(String.valueOf(lostPassFees));

                row.setSubHeading(subHeading);
                row.setSyncedWithServerState(lostPass.getIsSyncedWithServer());
                row.setIsSimplyParkTransaction(true);
                //row.setCustomer(booking.getCustomerDesc());

                row.setPosition(++position);

                mDailyEarning += lostPassFees;

                mElements.add(row);

            }
                return true;
            }
        }

}
