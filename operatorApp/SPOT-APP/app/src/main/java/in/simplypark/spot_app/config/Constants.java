package in.simplypark.spot_app.config;

import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import okhttp3.MediaType;

/**
 * Created by aditya on 06/05/16.
 */
public class Constants {

    public static final String APP_VERSION = "9.3.5";

    //Server Address related constants
     public static final String BASE_URL = "https://www.simplypark.in";
    //public static final String BASE_URL = "http://10.42.0.1/simplypark";
    //public static final String BASE_URL = "http://192.168.0.24/simplypark";
    //public static final String BASE_URL = "http://172.17.21.7/simplypark";
    //public static final String BASE_URL = "http://5.68.244.68/simplypark";
    //public static final String BASE_URL = "http://192.168.0.102:81/simplypark";
//    public static final String BASE_URL = "http://192.168.1.102:81/simplypark";
//    public static final String BASE_URL = "http://172.17.200.139:81/simplypark";
    //public static final String BASE_URL = "http://172.20.10.6/simplypark";

    public static final String LOGIN_URL = BASE_URL + "/homes/operatorLogin";
    public static final String SPACE_LIST_URL = BASE_URL + "/spaces/opAppGetSpaces/";
    public static final String SPACE_INFO_URL = BASE_URL + "/spaces/operatorSpaceDetails/";
    public static final String REGISTER_URL = BASE_URL + "/spaces/registerApp/";
    public static final String CURRENT_OCCUPANCY_URL = BASE_URL + "/spaces/getCurrentOccupancy/";
    public static final String SPACE_OPERATING_HOURS_URL = BASE_URL + "/spaces/getOperatingHours/";
    public static final String LOG_ENTRY_URL = BASE_URL + "/spaces/logOfflineEntry/";
    public static final String LOG_EXIT_URL = BASE_URL + "/spaces/logOfflineExit/";
    public static final String REFRESH_TOKEN_URL = BASE_URL + "/spaces/refreshAppToken/";
    public static final String SYNC_WITH_SERVER = BASE_URL + "/spaces/sync/";
    public static final String GET_SYNC_ID = BASE_URL + "/spaces/getSyncId/";
    public static final String CANCEL_TRANSACTION = BASE_URL + "/spaces/cancelTransaction/";
    public static final String UPDATE_TRANSACTION = BASE_URL + "/spaces/updateTransaction/";
    public static final String GET_SPACES = BASE_URL + "/spaces/getSpaces";

    public static final String SP_LOG_ENTRY_URL = BASE_URL + "/spaces/logOnlineEntry/";
    public static final String SP_LOG_EXIT_URL = BASE_URL + "/spaces/logOnlineExit/";
    public static final String CANCEL_SP_TRANSACTION = BASE_URL + "/spaces/cancelOnlineTransaction/";
    public static final String UPDATE_SP_TRANSACTION = BASE_URL + "/spaces/updateOnlineTransaction/";

    public static final String CREATE_BOOKING = BASE_URL + "/bookings/createBooking/";
    public static final String MODIFY_BOOKING = BASE_URL + "/bookings/modifyBooking/";
    public static final String IS_SPACE_BIKE_ONLY = BASE_URL +  "/spaces/isSpaceBikeOnly/";
    public static final String IS_CANCEL_UPDATE_ENABLED = BASE_URL + "/spaces/isCancelUpdateEnabled/";

    public static final String RAZORPAY_BASE_URL = "https://api.razorpay.com/v1";
    public static final String CREATE_INVOICE = BASE_URL + "/bookings/createInvoice/";
    public static final String APP_CRASH_LOG_REPORT = BASE_URL+"/report.php";

    public static final String ANALOGICS_PRINTER_NAME = "ANTHERMAL";



    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    //Date related constants
    public static final TimeZone TIME_ZONE = TimeZone.getTimeZone("Asia/Calcutta");
    public static final int      DATEDIFF_MINUTES_IN_HOUR = 60;
    public static final int      DATEDIFF_OK = 0;
    public static final int      DATEDIFF_ERROR_FROM_TIME_GREATER_THAN_TO_TIME = 1;
    public static final int      DATEDIFF_HOURLY_BOOKING = 2;
    public static final int      DATEDIFF_DAILY_BOOKING = 3;
    public static final int      DATEDIFF_LONG_TERM_BOOKING = 4;

    //public bluetooth setup contants
    public static final int      SETUP_WITH_NO_BT_SUPPORT = 0;
    public static final int      SETUP_WITH_BT_SUPPORT = 1;
    public static final int      REQUEST_FOR_BT = 2;
    public static final int      PRINTER_STATE_DISCONNECTED = 0;
    public static final int      PRINTER_STATE_CONNECTED = 1;

    //printer size related constnats
    public static final int      PRINTER_SIZE_2_INCH = 1;
    public static final int      PRINTER_SIZE_3_INCH = 2;

    //Defines CONNECTIVITY CHANGE OPTIONS
    public static final int   INTERNET_CONNECTIVITY_DOWN = 1;
    public static final int   INTERNET_CONNECTIVITY_UP = 2;
    public static final int   BLUETOOTH_CONNECTIVITY_DOWN = 1;
    public static final int   BLUETOOTH_CONNECTIVITY_UP = 2;

    //battery status constants
    public static final int BATTERY_STATUS_100 = 100;
    public static final int BATTERY_STATUS_80 = 80;
    public static final int BATTERY_STATUS_60 = 60;
    public static final int BATTERY_STATUS_40 = 40;
    public static final int BATTERY_STATUS_20 = 20;


    public static final int BATTERY_STATUS_LOW = 1;
    public static final int BATTERY_STATUS_OKAY = 2;


    public static final int OK = 1;
    public static final int PARTIAL_OK = 2;
    public static final int ALREADY_IN_PROGRESS = 3;
    public static final int NOK = 0;

    // Defines a custom Intent action
    public static final String SERVER_UPDATION_BROADCAST_RESULTS_ACTION =
            "in.simplypark.spot_app.SERVER_UPDATION_BROADCAST_RESULTS";
    public static final String SERVER_UPDATION_BROADCAST_RESULTS_STATUS =
            "in.simplypark.spot_app.SERVER_UPDATION_BROADCAST_RESULTS_STATUS";

    public static final String SERVER_UPDATION_PAYMENT_GATEWAY_RESULTS_ACTION =
            "in.simplypark.spot_app.SERVER_UPDATION_PAYMENT_GATEWAY_RESULTS";

    public static final String SERVER_UPDATION_PAYMENT_GATEWAY_RESULTS_STATUS =
            "in.simplypark.spot_app.SERVER_UPDATION_PAYMENT_GATEWAY_RESULTS_STATUS";

    public static final String GCM_ASYNC_NOTIFICATIONS =
            "in.simplypark.spot_app.GCM_ASYNC_NOTIFICATIONS";

    public static final int GCM_ACTION_NONE = 0;
    public static final int GCM_ACTION_ENTRY = 1;
    public static final int GCM_ACTION_EXIT = 2;
    public static final int GCM_ACTION_SYNC = 3;
    public static final int GCM_ACTION_CANCEL = 4;
    public static final int GCM_ACTION_UPDATE = 5;
    public static final int GCM_ACTION_BOOKING_UPDATE = 6;
    public static final int GCM_ACTION_SP_ENTRY = 7;
    public static final int GCM_ACTION_SP_EXIT = 8;
    public static final int GCM_ACTION_SP_CANCEL= 9;
    public static final int GCM_ACTION_SP_UPDATE= 10;
    public static final int GCM_ACTION_OPAPP_BOOKING_UPDATE = 11;
    public static final int GCM_ACTION_OPAPP_BOOKING_MODIFICATION=12;
    // Defines a custom Intent action for Entry Printing
    public static final String PRINT_ENTRY_RECEIPT_BROADCAST_RESULTS_ACTION =
            "in.simplypark.spot_app.PRINT_ENTRY_RECEIPT_BROADCAST_RESULTS";
    public static final String PRINT_ENTRY_RECEIPT_BROADCAST_RESULTS_STATUS =
            "in.simplypark.spot_app.PRINT_ENTRY_RECEIPT_BROADCAST_RESULTS_STATUS";

    // Defines a custom Intent action for Exit Printing
    public static final String PRINT_EXIT_RECEIPT_BROADCAST_RESULTS_ACTION =
            "in.simplypark.spot_app.PRINT_EXIT_RECEIPT_BROADCAST_RESULTS";
    public static final String PRINT_EXIT_RECEIPT_BROADCAST_RESULTS_STATUS =
            "in.simplypark.spot_app.PRINT_EXIT_RECEIPT_BROADCAST_RESULTS_STATUS";


    //server updation actions
    //these constants are used by the service
    public static final String ACTION_NONE = "in.simplypark.spot_app.action.NONE";
    public static final String ACTION_REGISTER   = "in.simplypark.spot_app.action.REGISTER";
    public static final String ACTION_DEREGISTER = "in.simplypark.spot_app.action.DEREGISTER";
    public static final String ACTION_ENTRY      = "in.simplypark.spot_app.action.ENTRY";
    public static final String ACTION_SP_ENTRY      = "in.simplypark.spot_app.action.SP_ENTRY";
    public static final String ACTION_EXIT      = "in.simplypark.spot_app.action.EXIT";
    public static final String ACTION_SP_EXIT      = "in.simplypark.spot_app.action.SP_EXIT";
    public static final String ACTION_REFRESH_TOKEN  = "in.simplypark.spot_app.action.REFRESH_TOKEN";
    public static final String ACTION_GET_CURRENT_OCCUPANCY  =
                                              "in.simplypark.spot_app.action.GET_CURRENT_OCCUPANCY";
    public static final String ACTION_ENTRY_NOTIFICATION_FROM_ANOTHER_APP =
                                "in.simplypark.spot_app.action.ENTRY_NOTIFICATION_FROM_ANOTHER_APP";

    public static final String ACTION_SP_ENTRY_NOTIFICATION_FROM_ANOTHER_APP =
            "in.simplypark.spot_app.action.SP_ENTRY_NOTIFICATION_FROM_ANOTHER_APP";


    public static final String ACTION_EXIT_NOTIFICATION_FROM_ANOTHER_APP =
            "in.simplypark.spot_app.action.EXIT_NOTIFICATION_FROM_ANOTHER_APP";

    public static final String ACTION_SP_EXIT_NOTIFICATION_FROM_ANOTHER_APP =
            "in.simplypark.spot_app.action.SP_EXIT_NOTIFICATION_FROM_ANOTHER_APP";


    public static final String ACTION_CANCEL_NOTIFICATION_FROM_ANOTHER_APP =
            "in.simplypark.spot_app.action.CANCEL_NOTIFICATION_FROM_ANOTHER_APP";

    public static final String ACTION_SP_CANCEL_NOTIFICATION_FROM_ANOTHER_APP =
            "in.simplypark.spot_app.action.SP_CANCEL_NOTIFICATION_FROM_ANOTHER_APP";

    public static final String ACTION_UPDATE_NOTIFICATION_FROM_ANOTHER_APP =
            "in.simplypark.spot_app.action.UPDATE_NOTIFICATION_FROM_ANOTHER_APP";

    public static final String ACTION_SP_UPDATE_NOTIFICATION_FROM_ANOTHER_APP =
            "in.simplypark.spot_app.action.SP_UPDATE_NOTIFICATION_FROM_ANOTHER_APP";

    public static final String ACTION_OPAPP_BOOKING_MODIFICATION_NOTIFICATION_FROM_ANOTHER_APP =
            "in.simplypark.spot_app.action.OPAPP_BOOKING_MODIFICATION_NOTIFICATION_FROM_ANOTHER_APP";


    public static final String ACTION_BOOKING_UPDATE_FROM_SERVER =
            "in.simplypark.spot_app.action.BOOKING_UPDATE_FROM_SERVER";

    public static final String ACTION_OPAPP_BOOKING_UPDATE_FROM_SERVER =
            "in.simplypark.spot_app.action.OPAPP_BOOKING_UPDATE_FROM_SERVER";


    public static final String ACTION_TRIGGER_PENDING_ACTIONS =
            "in.simplypark.spot_app.action.TRIGGER_PENDING_ACTIONS";

    public static final String ACTION_PRINT_ENTRY_TICKET =
                                                 "in.simplypark.spot_app.action.PRINT_ENTRY_TICKET";

    public static final String ACTION_PRINT_EXIT_TICKET =
            "in.simplypark.spot_app.action.PRINT_EXIT_TICKET";

    public static final String ACTION_PRINT_REPORT =
            "in.simplypark.spot_app.action.PRINT_REPORT";

    public static final String ACTION_MONTHLY_PASS_GENERATED =
            "in.simplypark.spot_app.action.MONTHLY_PASS_GENERATED";
    public static final String ACTION_LOST_MONTHLY_PASS_REISSUE =
            "in.simplypark.spot_app.action.LOST_MONTHLY_PASS_REISSUE";
    public static final String ACTION_CREATE_INVOICE =
            "in.simplypark.spot_app.action.CREATE_INVOICE";

    public static final String ACTION_COLLECT_PAYMENT_SUCCESSFUL_RESULT =
            "in.simplypark.spot_app.action.ACTION_COLLECT_PAYMENT_SUCCESSFUL_RESULT";

    public static final String ACTION_COLLECT_PAYMENT_FAILURE_RESULT =
            "in.simplypark.spot_app.action.ACTION_COLLECT_PAYMENT_FAILURE_RESULT";

    public static final String ACTION_RESET_OCCUPANCY_COUNT =
            "in.simplypark.spot_app.action.RESET_OCCUPANCY_COUNT";

    public static final String ACTION_PRINT_GENERATED_MONTHLY_PASS =
            "in.simplypark.spot_app.action.PRINT_GENERATED_MONTHLY_PASS";

    public static final String ACTION_CANCEL_MONTHLY_PASS =
            "in.simplypark.spot_app.action.CANCEL_MONTHLY_PASS";

    public static final String ACTION_PRINT_MONTHLY_PASS_RECEIPT =
            "in.simplypark.spot_app.action.PRINT_MONTHLY_PASS_RECEIPT";


    public static final String ACTION_CONNECT_TO_PRINTER =
            "in.simplypark.spot_app.action.CONNECT_TO_PRINTER";

    public static final String ACTION_DISCONNECT_FROM_PRINTER =
            "in.simplypark.spot_app.action.DISCONNECT_FROM_PRINTER";

    public static final String ACTION_END_OF_DAY =
            "in.simplypark.spot_app.action.END_OF_DAY";

    public static final String ACTION_SPACE_OPERATING_HOURS =
            "in.simplypark.spot_app.action.SPACE_OPERATING_HOURS";

    public static final String ACTION_GET_UPDATES_FROM_SERVER =
            "in.simplypark.spot_app.action.GET_UPDATES_FROM_SERVER";

    public static final String ACTION_PUSH_UPDATES_TO_SERVER =
            "in.simplypark.spot_app.action.PUSH_UPDATES_TO_SERVER";

    public static final String ACTION_GET_SYNC_ID_FROM_SERVER =
            "in.simplypark.spot_app.action.GET_SYNC_ID_FROM_SERVER";

    public static final String ACTION_RESET_SYNC_WITH_SERVER =
            "in.simplypark.spot_app.action.RESET_SYNC_WITH_SERVER";

    public static final String ACTION_SYNC_WITH_SERVER =
            "in.simplypark.spot_app.action.SYNC_WITH_SERVER";

    public static final String ACTION_CANCEL_TRANSACTION =
            "in.simplypark.spot_app.action.CANCEL_TRANSACTION";

    public static final String ACTION_CANCEL_SP_TRANSACTION =
            "in.simplypark.spot_app.action.CANCEL_SP_TRANSACTION";

    public static final String ACTION_UPDATE_TRANSACTION =
            "in.simplypark.spot_app.action.UPDATE_TRANSACTION";

    public static final String ACTION_UPDATE_SP_TRANSACTION =
            "in.simplypark.spot_app.action.UPDATE_SP_TRANSACTION";

    public static  final String ACTION_CALCULATE_OCCUPANCY_FROM_DB =
            "in.simplypark.spot_app.action.CALCULATE_OCCUPANCY_FROM_DB";

    public static  final String ACTION_START_PUSH_TIMER_IF_REQUIRED =
            "in.simplypark.spot_app.action.START_PUSH_TIMER_IF_REQUIRED";

    public static  final String ACTION_IS_SPACE_BIKE_ONLY =
            "in.simplypark.spot_app.action.START_IS_SPACE_BIKE_ONLY";

    public static final String ACTION_IS_CANCELLATION_UPDATION_ALLOWED =
            "in.simplypark.spot_app.action.IS_CANCELLATION_UPDATION_ALLOWED";




    public static final String TIMER_ACTION = "in.simplypark.spot_app.TIMER_EXPIRY";
    public static final String TRIGGER_ACTION_NAME = "in.simplypark.spot_app.trigger.ACTION";
    public static final String ERROR = "in.simplypark.spot_all.ERROR";

    //while passing the response, we also pass the action which triggered this.
    public static final int TRIGGER_ACTION_NONE = 0;
    public static final int TRIGGER_ACTION_REGISTER = 1;
    public static final int TRIGGER_ACTION_DEREGISTER = 2;
    public static final int TRIGGER_ACTION_ENTRY = 3;
    public static final int TRIGGER_ACTION_EXIT = 4;
    public static final int TRIGGER_ACTION_REFRESH_TOKEN = 5;
    public static final int TRIGGER_ACTION_GET_CURRENT_OCCUPANCY = 6;
    public static final int TRIGGER_ACTION_ASYNC_GCM_UPDATE = 7; //this is currently unused
    public static final int TRIGGER_ACTION_ASYNC_GCM_UPDATE_ENTRY = 8;
    public static final int TRIGGER_ACTION_ASYNC_GCM_UPDATE_EXIT  = 9;
    public static final int TRIGGER_ACTION_GET_SPACE_OPERATING_HOURS = 10;
    public static final int TRIGGER_ACTION_PULL_UPDATES_FROM_SERVER = 11;
    public static final int TRIGGER_ACTION_GET_SYNC_ID_FROM_SERVER = 12;
    public static final int TRIGGER_ACTION_PUSH_UPDATES_TO_SERVER = 13;
    public static final int TRIGGER_ACTION_CANCEL_TRANSACTION = 14;
    public static final int TRIGGER_ACTION_UPDATE_TRANSACTION = 15;
    public static final int TRIGGER_ACTION_CANCEL_TRANSACTION_FROM_ANOTHER_APP = 16;
    public static final int TRIGGER_ACTION_UPDATE_TRANSACTION_FROM_ANOTHER_APP = 17;
    public static final int TRIGGER_ACTION_CALCULATE_OCCUPANCY_COUNT_FROM_DB = 18;

    public static final int TRIGGER_ACTION_SP_ENTRY = 19;
    public static final int TRIGGER_ACTION_SP_EXIT = 20;
    public static final int TRIGGER_ACTION_SP_CANCEL_TRANSACTION = 21;
    public static final int TRIGGER_ACTION_SP_UPDATE_TRANSACTION = 22;
    public static final int TRIGGER_ACTION_ASYNC_GCM_UPDATE_SP_ENTRY = 23;
    public static final int TRIGGER_ACTION_ASYNC_GCM_UPDATE_SP_EXIT  = 24;
    public static final int TRIGGER_ACTION_SP_CANCEL_TRANSACTION_FROM_ANOTHER_APP = 25;
    public static final int TRIGGER_ACTION_SP_UPDATE_TRANSACTION_FROM_ANOTHER_APP = 26;
    public static final int TRIGGER_ACTION_BOOKING_UPDATE_FROM_SERVER = 27;
    public static final int TRIGGER_ACTION_END_OF_DAY = 28;
    public static final int TRIGGER_ACTION_MONTHLY_PASS_GENERATED = 29;
    public static final int TRIGGER_ACTION_OPAPP_BOOKING_UPDATE_RECEIVED = 30;


    public static final int TRIGGER_ACTION_IS_SPACE_BIKE_ONLY = 31;
    public static final int TRIGGER_ACTION_RESET_OCCUPANCY_COUNT = 32;
    public static final int TRIGGER_ACTION_CREATE_INVOICE = 33;
    public static final int TRIGGER_ACTION_COLLECT_PAYMENT_SUCCESSFUL_RESULT = 34;
    public static final int TRIGGER_ACTION_COLLECT_PAYMENT_UNSUCCESSFUL_RESULT = 35;
    public static final int TRIGGER_ACTION_LOST_MONTHLY_PASS = 36;


    public static final int INVALID_SYNC_ID = -1;




    //cache (implemented as Shared Preferences)


    public static final int TYPE_INT = 0;
    public static final int TYPE_BOOL = 1;
    public static final int TYPE_STRING = 2;
    public static final int TYPE_DOUBLE = 3;
    public static final int TYPE_LONG = 4;

    public static final String CACHE_NAME = "in.simplypark.spot_app";
    public static final String CACHE_PREFIX_KEY = "cache";
    public static final String CACHE_PREFIX_EMAIL_QUERY = "cacheEmail";
    public static final String CACHE_PREFIX_LOGIN_QUERY = "cacheLogin";
    public static final String CACHE_PREFIX_ISCONFIGURED_QUERY = "cacheIsConfigured";
    public static final String CACHE_PREFIX_APP_REGISTERATION_TOKEN = "cacheRegToken";
    public static final String CACHE_PREFIX_OPERATOR_QUERY = "cacheOperator";
    public static final String CACHE_PREFIX_OPERATOR_DESC_QUERY = "cacheOperatorDesc";
    public static final String CACHE_PREFIX_SPACEID_QUERY = "cacheSpaceId";
    public static final String CACHE_PREFIX_SPACE_NAME_QUERY = "cacheSpaceName";
    public static final String CACHE_PREFIX_IS_PRINTER_CONFIGURED = "cacheIsPrinterCfged";
    public static final String CACHE_PREFIX_IS_TEST_PAGE_PRINTED = "cacheIsTestPagePrinted";
    public static final String CACHE_PREFIX_ISBT_SUPPORTED_QUERY = "cachePrinterAdded";
    public static final String CACHE_PREFIX_BT_MACADDRESS_QUERY = "cacheMacAddress";
    public static final String CACHE_PREFIX_CURRENT_OCCUPANCY_QUERY = "cacheOccupancy";
    public static final String CACHE_PREFIX_CURRENT_PARKED_QUERY = "cacheCurrentParkedQuery";
    public static final String CACHE_PREFIX_NUM_SLOTS_QUERY = "cacheNumSlots";
    public static final String CACHE_PREFIX_GCM_ID_AT_SERVER_QUERY = "cacheGCMIdAtServer";
    public static final String CACHE_PREFIX_PRICE_AT_HOUR_QUERY = "cachePriceAtHour_";
    public static final String CACHE_PREFIX_PRINTER_SIZE = "cachePrinterSize";
    public static final String CACHE_PREFIX_PRINT_PRICING_QUERY = "cachePrinterPricing";
    public static final String CACHE_PREFIX_PRINT_BIKE_PRICING_QUERY = "cachePrinterBikePricing";
    public static final String CACHE_PREFIX_IS_PARK_AND_RIDE_AVAIL_QUERY = "cacheParkAndRide";
    public static final String CACHE_PREFIX_BIKES_TO_SLOT_RATIO = "cacheBikesToSlotRatio";
    public static final String CACHE_PREFIX_BIKE_PRICING_RATIO = "cacheBikePricingRatio";
    public static final String CACHE_PREFIX_PARKED_CARS_COUNT_QUERY = "cacheGetParkedCarsCount";
    public static final String CACHE_PREFIX_PREPAID_CARS_COUNT_QUERY = "cacheGetPrepaidCarsCount";
    public static final String CACHE_PREFIX_PARKED_BIKES_COUNT_QUERY = "cacheGetParkedBikesCount";
    public static final String CACHE_PREFIX_PREPAID_BIKES_COUNT_QUERY = "cacheGetPrepaidBikesCount";
    public static final String CACHE_PREFIX_PARKED_MINIBUS_COUNT_QUERY = "cacheGetParkedMiniBusCount";
    public static final String CACHE_PREFIX_PREPAID_MINIBUS_COUNT_QUERY = "cacheGetPrepaidMiniBusCount";
    public static final String CACHE_PREFIX_PARKED_BUS_COUNT_QUERY = "cacheGetParkedBusCount";
    public static final String CACHE_PREFIX_PREPAID_BUS_COUNT_QUERY = "cacheGetPrepaidBusCount";
    public static final String CACHE_PREFIX_EARNINGS_PER_DAY_QUERY = "cacheEarningsPerDay";
    public static final String CACHE_PREFIX_UPDATE_OCCUPANCY_VIEW_QUERY = "cacheUpdateOccupancyView";
    public static final String CACHE_PREFIX_OPENING_DATE_TIME_QUERY = "cacheOpeningDateTimeQuery";
    public static final String CACHE_PREFIX_CLOSING_DATE_TIME_QUERY = "cacheClosingDateTimeQuery";
    public static final String CACHE_PREFIX_CLOSING_TIME_QUERY = "cacheClosingTimeQuery";
    public static final String CACHE_PREFIX_OPENING_TIME_QUERY = "cahceOpeningTimeQuery";
    public static final String CACHE_PREFIX_OPERATING_HOURS_SET_QUERY = "cacheOperatingHourSet";
    public static final String CACHE_PREFIX_IS_OPEN_24_HOURS = "cacheIsOpen24HoursQuery";
    public static final String CACHE_PREFIX_IS_MULTI_DAY_PARKING_POSSIBLE_QUERY =
                             "cacheIsMultiDayPakringPossibleQuery";
    public static final String CACHE_PREFIX_IS_PRICING_TIERED = "cachePrefixIsPricingTiered";


    public static final String CACHE_PREFIX_LOST_TICKET_CHARGE = "cacheLostTicketCharge";


    public static final String CACHE_PREFIX_IS_PARKING_PREPAID_QUERY = "cachePrefixIsParkingPrepid";
    public static final String CACHE_PREFIX_NUM_PREPAID_HOURS_QUERY = "cachePrefixNumPrepaidHours";
    public static final String CACHE_PREFIX_CAR_PREPAID_FEES_QUERY = "cachePrefixCarPrepaidFees";
    public static final String CACHE_PREFIX_BIKE_PREPAID_FEES_QUERY = "cachePrefixBikePrepaidFees";
    public static final String CACHE_PREFIX_BUS_PREPAID_FEES_QUERY =  "cachePrefixBusPrepaidFees";
    public static final String CACHE_PREFIX_MINIBUS_PREPAID_FEES_QUERY =  "cachePrefixMiniBusPrepaidFees";
    public static final String CACHE_PREFIX_SYNC_ID_QUERY = "cachePrefixSyncIdQuery";
    public static final String CACHE_PREFIX_NIGHT_CHARGE_QUERY = "cachePrefixNightChargeQuery";
    public static final String CACHE_PREFIX_IS_SLOT_OCCUPIED_ASSUMED_QUERY =
                                                                 "cachePrefixIsSlotOccupiedQuery";
    public static final String CACHE_PREFIX_SPACE_OWNER_NAME_QUERY = "cachePrefixAgencyNameQuery";
    public static final String CACHE_PREFIX_SPACE_OWNER_MOBILE_QUERY = "cachePrefixOwnerMobileQuery";
    public static final String CACHE_PREFIX_NUM_UPDATES_SEND_QUERY =
                                                      "cachePrefixNumUpdatesSentQuery";
    /*public static final String CACHE_PREFIX_NEXT_EXPECTED_SYNC_ID_QUERY =
                                                    "cachePrefixNextExpectedSyncIdQuery";*/

    public static final String CACHE_PREFIX_SYNC_STATE_QUERY =
                                                    "cachePrefixSyncStateQuery";

    public static final String CACHE_PREFIX_TIMER_ACITON_PUSH_START_TIME_QUERY =
                                          "cachePrefixActionPushStartTime";

    public static final String CACHE_PREFIX_TIMER_ACITON_PUSH_END_TIME_QUERY =
                                            "cachePrefixActionPushEndTime";

    public static final String CACHE_PREFIX_MONTHLY_RATE_QUERY = "cachePrefixGetMonthlyRate";
    public static final String CACHE_PREFIX_BIKE_MONTHLY_RATE_QUERY = "cachePrefixGetBikeMonthlyRate";
    public static final String CACHE_PREFIX_SPACE_OWNER_EMAIL_QUERY = "cachePrefixSpaceOwnerEmail";
    public static final String CACHE_PREFIX_YEARLY_RATE_QUERY  = "cachePrefixGetYearlyRate";
    public static final String CACHE_PREFIX_SPACE_OWNER_ID_QUERY =
            "cachePrefixSpaceOwnerIdQuery";
    public static final String CACHE_PREFIX_STAFF_DISCOUNT_RATE_QUERY = "cachePrefixStaffDiscountRate";
    public static final String CACHE_PREFIX_IS_MONTHLY_PASS_GENERATED =
                                                             "cachePrefixIsMonthlyPassGenerated";

    public static final String CACHE_PREFIX_TOTAL_MONTHLY_PASSES =
                                                             "cachePrefixTotalMonthlyPass";

    public static final String CACHE_PREFIX_MONTHLY_PASSES_ADDED_TODAY =
                                                         "cachePrefixMonthlyPassAddedToday";

    public static final String CACHE_PREFIX_MONTHLY_PASSES_ACTIVE_TODAY =
                                                         "cachePreixMonthlyPassesActiveToday";

    public static final String CACHE_PREFIX_LAST_PRINT_COMMAND = "cachePrefixLastPrintCmdQuery";
    public static final String CACHE_PREFIX_LAST_PRINT_COMMAND_SIZE = "cachePrefixLastPrintCmdSizeQuery";
    public static final String CACHE_PREFIX_PRINT_EXCEPTION = "cachePrefixPrintExcepton";
    public static final String CACHE_PREFIX_FLAG_EXCEPTION = "cachePrefixFlagExcepton";

    public static final String CACHE_PREFIX_SPACE_VEHICLE_TYPE_QUERY = "cachePrefixVehicleTypeQuery";
    public static final String CACHE_PREFIX_IS_SPACE_BIKE_ONLY_QUERY = "cachePrefixIsSpaceBikeQuery";
    public static final String CACHE_PREFIX_IS_MINIBUS_PARKING_ALLOWED_QUERY = "cachePrefixIsMinibusParkingAllowedQuery";
    public static final String CACHE_PREFIX_LAST_OCCUPANCY_COUNT_RESET_QUERY =
                                                  "cachePrefixLastOccupancyCountResetQuery";

    public static final String CACHE_PREFIX_CUSTOM_SPACE_ADDRESS = "cachePrefixSpaceAddress";
    public static final String CACHE_PREFIX_CUSTOM_SPACE_HEADING = "cachePrefixSpaceHeading";
    public static final String CACHE_PREFIX_CUSTOM_AGENCY_NAME = "cachePrefixAgencyName";
    public static final String CACHE_PREFIX_CUSTOM_SPACE_OWNER_MOBILE = "cachePrefixOwnerMobile";
    public static final String CACHE_PREFIX_CUSTOM_OPERATING_HOURS = "cachePrefixOperatingHours";
    public static final String CACHE_PREFIX_CUSTOM_PARKING_RATES = "cachePrefixParkingRates";
    public static final String CACHE_PREFIX_CUSTOM_BIKE_PARKING_RATES = "cachePrefixBikeParkingRates";
    public static final String CACHE_PREFIX_CUSTOM_MINIBUS_PARKING_RATES = "cachePrefixMiniBusParkingRates";
    public static final String CACHE_PREFIX_CUSTOM_BUS_PARKING_RATES = "cachePrefixBusParkingRates";
    public static final String CACHE_PREFIX_CUSTOM_NIGHT_CHARGES = "cachePrefixNightCharge";
    public static final String CACHE_PREFIX_CUSTOM_BIKE_NIGHT_CHARGES = "cachePrefixBikeNightCharge";
    public static final String CACHE_PREFIX_CUSTOM_LOST_TICKET = "cachePrefixLostTicket";
    public static final String CACHE_PREFIX_CUSTOM_URL = "cachePrefixUrl";
    public static final String CACHE_PREFIX_CUSTOM_DESC = "cachePrefixDesc";
    public static final String CACHE_PREFIX_CUSTOM_AD_TEXT = "cachePrefixCustomAdText";
    public static final String CACHE_PREFIX_CUSTOM_EXIT_SIMPLYPARK_AD_TEXT = "cachePrefixCustomSparkText";
    public static final String CACHE_PREFIX_IS_CANCELLATION_UPDATION_ALLOWED =
                                                                  "cachePrefixIsCancelUpdateAllowed";
    public static final String CACHE_PREFIX_IS_CANCELLATION_UPDATION_FEATURE_AVAIL_KNOWN =
                                                   "cachePrefixIsCancellationFeatureAvailKnown";

    public static final String CACHE_PREFIX_IS_PASS_ACTIVATION_REQUIRED =
                                                   "cachePrefixIsPassActivationRequired";

    public static final String CACHE_PREFIX_INVOICE_ID = "cachePrefixInvoiceId";
    public static final String CACHE_PREFIX_PASS_BOOKING_ID = "cachePrefixPassBookingId";

    //constants for PRICING_AVAILABLE
    public static final String CACHE_PREFIX_IS_STAFF_DISCOUNT_AVAILABLE_QUERY = "staffDiscountAvailableQuery";
    public static final String CACHE_PREFIX_IS_PENALTY_ON_MONTHLY_PASS_AVAILABLE_QUERY = "isPenaltyOnMonthlyPassAvailableQuery";
    public static final String CACHE_PREFIX_IS_BIKE_PRICING_AVAILABLE_QUERY = "bikePricingAvailableQuery";
    public static final String CACHE_PREFIX_IS_CAR_PRICING_AVAILABLE_QUERY = "carPricingAvailableQuery";
    public static final String CACHE_PREFIX_IS_BUS_PRICING_AVAILABLE_QUERY = "busPricingAvailableQuery";
    public static final String CACHE_PREFIX_IS_MINIBUS_PRICING_AVAILABLE_QUERY =
                                                             "minibusPricingAvailableQuery";


    public static final String CACHE_PREFIX_DEFAULT_VEHICLE_TYPE = "cachePrefixDefaultVehicleType";
    public static final String CACHE_PREFIX_OPERATORID_DESC_QUERY = "cachePrefixOperatorIdDescQuery_";
    public static final String CACHE_PREFIX_IS_GATEWISE_REPORT_REQUIRED_QUERY =
            "cachePrefixIsGatewiseReportsRequiredQuery";

    public static final String CACHE_PREFIX_APP_TYPE = "cachePrefixAppTypeQuery";
    public static final String CACHE_PREFIX_IS_LOST_TICKET_CHARGE_AVAILABLE =
             "cachePrefixIsLostTicketChargeQuery";

    public static final String CACHE_PREFIX_IS_REUSABLE_TOKEN_USED = "cachePrefixIsReusableTokenUsed";
    public static final String CACHE_PREFIX_USER_ID = "cachePrefixUserId";
    public static final String CACHE_PREFIX_SPACE_LIST = "cacheSpaceList";
    public static final String CACHE_PREFIX_BIG_SIZE_VEHICLE_REGSTRING = "cachePrefixBigVehicleReg";







    public static final int SYNC_STATE_IN_SYNC = 0;
    public static final int SYNC_STATE_GET_SYNC_ID_IN_PROGRESS = 1;
    public static final int SYNC_STATE_IN_PROGRESS_PULL = 2;
    public static final int SYNC_STATE_IN_PROGRESS_PUSH = 3;
    public static final int SYNC_STATE_BOTH_PUSH_AND_PULL_IN_PROGRESS = 4;
    public static final int SYNC_STATE_PUSH_WAITING_FOR_TIMER_EXPIRY = 5;

    public static final int MAX_PRINTABLE_CHAR=36;
    public static final int MAX_PARK_ID_LEN=6;
    public static final int MAX_PARK_ID_SUFFIX_LEN = 3;
    public static final byte[] mEncodeArray =  {'0','1','2','3','4','5','6','7','8','9',
            'a','b','c','d','e','f','g','h','i','j',
            'k','l','m','n','o','p','q','r','s','t',
            'u','v','w','x','y','z'
    };

    public static final int PARKING_ID_MAX_LEN = 11;
    public static final int PARKING_ID_REG_MIN_LEN_WITH_VEHICLE_TYPE = 7;
    public static final int PARKING_ID_MIN_LEN_WITH_CUSTOMER_ID = PARKING_ID_REG_MIN_LEN_WITH_VEHICLE_TYPE + MAX_PARK_ID_SUFFIX_LEN;
    public static final int PARKING_ID_LEN = 6;
    public static final int REG_MIN_LEN = 4;


    //bundle parameters
    public static final String BUNDLE_PARAM_PAYMENT_DUE = "in.simplypark.spot_app.PAYMENT_DUE";
    public static final String BUNDLE_PARAM_CURRENT_OCCUPANCY =
                                                    "in.simplypark.spot_app.CURRENT_OCCUPANCY";

    public static final String BUNDLE_PARAM_CURRENT_PARKED_CARS =
                                                     "in.simplypark.spot_app.CURRENT_PARKED_CARS";
    public static final String BUNDLE_PARAM_CURRENT_PARKED_BIKES =
                                                      "in.simplypark.spot_app.CURRENT_PARKED_BIKES";

    public static final String BUNDLE_PARAM_EARNINGS =
                                                 "in.simplypark.spot_all.TODAYS_EARNINGS";


    public static final String BUNDLE_PARAM_CAR_REG    = "in.simplypark.spot_app.CAR_REG";
    public static final String BUNDLE_PARAM_VEHICLE_REG = "in.simplypark.spot_app.VEHICLE_REG";
    public static final String BUNDLE_PARAM_ENTRY_TIME = "in.simplypark.spot_app.ENTRY_TIME";
    public static final String BUNDLE_PARAM_EXIT_TIME = "in.simplypark.spot_app.EXIT_TIME";
    public static final String BUNDLE_PARAM_BOOKING_TIME = "in.simplypark.spot_app.BOOKING_TIME";
    public static final String BUNDLE_PARAM_PARKING_ID = "in.simplypark.spot_app.PARKINGID";
    public static final String BUNDLE_PARAM_BOOKING_ID = "in.simplypark.spot_app.BOOKINGID";
    public static final String BUNDLE_PARAM_BOOKING_TYPE = "in.simplypark.spot_app.BOOKINGTYPE";
    public static final String BUNDLE_PARAM_IS_PARK_AND_RIDE =
                                                   "in.simplypark.spot_app.IS_PARK_AND_RIDE";
    public static final String BUNDLE_PARAM_PARKING_FEES =
                                                   "in.simplypark.spot_app.PARKING_FEES";

    public static final String BUNDLE_PARAM_ENCODE_PASS_CODE = "in.simplypark.spot_app.ENCODE_PASS_CODE";
    public static final String BUNDLE_PARAM_OLD_ENCODE_PASS_CODE = "in.simplypark.spot_app.OLD_ENCODE_PASS_CODE";
    public static final String BUNDLE_PARAM_SERIAL_NUMBER =  "in.simplypark.spot_app.SERIAL_NUMBER";
    public static final String BUNDLE_PARAM_OLD_SERIAL_NUMBER =  "in.simplypark.spot_app.OLD_SERIAL_NUMBER";
    public static final String BUNDLE_PARAM_CUSTOMER_DESC = "in.simplypark.spot_app.CUSTOMER_DESC";
    public static final String BUNDLE_PARAM_NUM_BOOKED_SLOTS = "in.simplypark.spot_app.NUM_SLOTS";
    public static final String BUNDLE_PARAM_BOOKING_STATUS = "in.simplypark.spot_all.BOOKING_STATUS";



    public static final String BUNDLE_PARAM_UPDATED_PARKING_FEES =
            "in.simplypark.spot_app.UPDATED_PARKING_FEES";

    public static final String BUNDLE_PARAM_UPDATED_PARKING_STATUS =
            "in.simplypark.spot_app.UPDATED_PARKING_STATUS";

    public static final String BUNDLE_PARAM_IS_NEW_RECORD =
                                                   "in.simplypark.spot_app.IS_NEW_RECORD";

    public static final String BUNDLE_PARAM_IS_NIGHT_CHARGE_APPLIED =
                                                   "in.simplypark.spot_app.IS_NIGHT_CHARGE_APPLIED";

    public static final String BUNDLE_PARAM_NUM_NIGHTS = "in.simplypark.spot_app.NUM_NIGHTS";

    public static final String BUNDLE_PARAM_EXTRA_NIGHT_CHARGE =
                            "in.simplypark.spot_app.EXTRA_NIGHT_CHARGE";

    public static final String BUNDLE_PARAM_NIGHT_CHARGES_DESC =
            "in.simplypark.spot_app.EXTRA_NIGHT_CHARGES_DESC";

    public static final String BUNDLE_PARAM_IS_PENALTY_CHARGE_APPLIED =
            "in.simplypark.spot_app.IS_PENALTY_CHARGE_APPLIED";
    public static final String BUNDLE_PARAM_PENALTY_CHARGE =
            "in.simplypark.spot_app.PENALTY_CHARGE";

    public static final String BUNDLE_PARAM_NUM_PENALTY_HOURS =
            "in.simplypark.spot_app.NUM_PENALTY_HOURS";

    public static final String BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW =
            "in.simplypark.spot_app.UPDATE_OCCUPANCY_VIEW";

    public static final String BUNDLE_PARAM_LOST_TICKET_CHARGE =
            "in.simplypark.spot_all.LOST_TICKET_CHARGE";

    public static final String BUNDLE_PARAM_OPERATOR_DESC =
                                                   "in.simplypark.spot_app.OPERATOR_DESC";


    public static final String BUNDLE_PARAM_OPERATOR_ID =
            "in.simplypark.spot_app.OPERATOR_ID";

    public static final String BUNDLE_PARAM_GCM_ACTION =
                                                   "in.simplypark.spot_app.GCM_ACTION";
    public static final String BUNDLE_PARAM_SPACE_ID = "in.simplypark.spot_app.SPACE_ID";
    public static final String BUNDLE_PARAM_SPACE_OWNER_ID = "in.simplypark.spot_app.SPACE_OWNER_ID";

    public static final String BUNDLE_PARAM_PRINTER_SIZE = "in.simplypark.spot_app.PRINTER_SIZE";

    public static final String BUNDLE_PARAM_ENTRY_TICKET = "in.simplypark.spot_app.PRINT_ENTRY_TICKET";
    public static final String BUNDLE_PARAM_EXIT_TICKET = "in.simplypark.spot_app.PRINT_EXIT_TICKET";
    public static final String BUNDLE_PARAM_MAC_ADDRESS = "in.simplypark.spot_app.MAC_ADDRESS";

    public static final String BUNDLE_PARAM_LAST_ACTIVITY_PRINTER_SETUP = "in.simplypark.spot_app." +
            "LAST_ACTIVITY_PRINTER_SETUP";

    public static final String BUNDLE_PARAM_VEHICLE_TYPE= "in.simplypark.spot_app.VEHICLE_TYPE";
    public static final String BUNDLE_PARAM_DURATION_DESCRIPTION = "in.simplypark.spot_app.DURATION";


    public static final String BUNDLE_PARAM_DAY_OVER = "in.simplypark.spot_all.DAY_OVER";
    public static final String BUNDLE_PARAM_OPENING_TIME = "in.simplypark.spot_all.OPENING_TIME";
    public static final String BUNDLE_PARAM_CLOSING_TIME = "in.simplypark.spot_all.CLOSING_TIME";
    public static final String BUNDLE_PARAM_DAY_OFFSET = "in.simplypark.spot_all.DAY_OFFSET";
    public static final String BUNDLE_PARAM_REPORT_DATETIME_FROM =
                                                      "in.simplypark.spot_all.DATETIME_FROM";
    public static final String BUNDLE_PARAM_REPORT_DATETIME_TO =
            "in.simplypark.spot_all.DATETIME_TO";

    public static final String BUNDLE_PARAM_REPORT_PERIOD =
            "in.simplypark.spot_all.REPORT_PERIOD";

    public static final String BUNDLE_PARAM_REPORT_PERIOD_EARNINGS =
            "in.simplypark.spot_all.REPORT_PERIOD_EARNINGS";

    public static final String BUNDLE_PARAM_REPORT_PERIOD_CAR_EARNINGS =
            "in.simplypark.spot_all.REPORT_PERIOD_CAR_EARNINGS";

    public static final String BUNDLE_PARAM_REPORT_PERIOD_BIKE_EARNINGS =
            "in.simplypark.spot_all.REPORT_PERIOD_BIKE_EARNINGS";

    public static final String BUNDLE_PARAM_REPORT_PERIOD_PASS_EARNINGS =
            "in.simplypark.spot_all.REPORT_PERIOD_PASS_EARNINGS";

    public static final String BUNDLE_PARAM_REPORT_PERIOD_NUM_VEHICLES_PARKED =
            "in.simplypark.spot_all.REPORT_PERIOD_NUM_VEHICLES_PARKED";

    public static final String BUNDLE_PARAM_REPORT_PERIOD_NUM_CARS_PARKED =
            "in.simplypark.spot_all.REPORT_PERIOD_NUM_CARS_PARKED";

    public static final String BUNDLE_PARAM_REPORT_PERIOD_NUM_MONTHLY_PASS_CARS_PARKED =
            "in.simplypark.spot_all.REPORT_PERIOD_NUM_MONTHLY_PASS_CARS_PARKED";

    public static final String BUNDLE_PARAM_REPORT_PERIOD_NUM_BIKES_PARKED =
            "in.simplypark.spot_all.REPORT_PERIOD_NUM_BIKES_PARKED";

    public static final String BUNDLE_PARAM_REPORT_PERIOD_NUM_MONTHLY_PASS_BIKES_PARKED =
            "in.simplypark.spot_all.REPORT_PERIOD_NUM_MONTHLY_PASS_BIKES_PARKED";


    public static final String BUNDLE_PARAM_REPORT_PERIOD_MISSING_EXITS =
            "in.simplypark.spot_all.REPORT_PERIOD_MISSING_EXITS";

    public static final String BUNDLE_PARAM_REPORT_PERIOD_MISSING_CAR_EXITS =
            "in.simplypark.spot_all.REPORT_PERIOD_MISSING_CAR_EXITS";

    public static final String BUNDLE_PARAM_REPORT_PERIOD_MISSING_MONTHLY_PASS_CAR_EXITS =
            "in.simplypark.spot_all.REPORT_PERIOD_MISSING_MONTHLY_PASS_CAR_EXITS";


    public static final String BUNDLE_PARAM_REPORT_PERIOD_MISSING_BIKE_EXITS =
            "in.simplypark.spot_all.REPORT_PERIOD_MISSING_BIKE_EXITS";

    public static final String BUNDLE_PARAM_REPORT_PERIOD_MISSING_MONTHLY_PASS_BIKE_EXITS =
            "in.simplypark.spot_all.REPORT_PERIOD_MISSING_MONTHLY_PASS_BIKE_EXITS";


    public static final String BUNDLE_PARAM_REPORT_PERIOD_CARS_INSIDE =
            "in.simplypark.spot_all.REPORT_PERIOD_CARS_INSIDE";

    public static final String BUNDLE_PARAM_REPORT_PERIOD_MONTHLY_PASS_CARS_INSIDE =
            "in.simplypark.spot_all.REPORT_PERIOD_MONTHLY_PASS_CARS_INSIDE";


    public static final String BUNDLE_PARAM_REPORT_PERIOD_BIKES_INSIDE =
            "in.simplypark.spot_all.REPORT_PERIOD_MISSING_BIKES_INSIDE";

    public static final String BUNDLE_PARAM_LOST_MONTHLY_PASS =
            "in.simplypark.spot_all.LOST_MONTHLY_PASS";

    public static final String BUNDLE_PARAM_MONTHLY_PASS_BOOKING_ID =
            "in.simplypark.spot_all.MONTHLY_PASS_BOOKING_ID";

    public static final String BUNDLE_PARAM_MONTHLY_PASS_ISSUED_TO =
            "in.simplypark.spot_all.MONTHLY_PASS_ISSUED_TO";

    public static final String BUNDLE_PARAM_MONTHLY_PASS_EMAIL =
            "in.simplypark.spot_all.MONTHLY_PASS_EMAIL";

    public static final String BUNDLE_PARAM_MONTHLY_PASS_TEL =
            "in.simplypark.spot_all.MONTHLY_PASS_TEL";

    public static final String BUNDLE_PARAM_MONTHLY_PASS_VEHICLE_REG =
            "in.simplypark.spot_all.MONTHLY_PASS_VEHICLE_REG";

    public static final String BUNDLE_PARAM_MONTHLY_PASS_VEHICLE_TYPE =
            "in.simplypark.spot_all.MONTHLY_PASS_VEHICLE_TYPE";

    public static final String BUNDLE_PARAM_MONTHLY_PASS_VALID_FROM =
            "in.simplypark.spot_all.MONTHLY_PASS_VALID_FROM";

    public static final String BUNDLE_PARAM_MONTHLY_PASS_VALID_TILL =
            "in.simplypark.spot_all.MONTHLY_PASS_VALID_TILL";

    public static final String BUNDLE_PARAM_MONTHLY_PASS_NEXT_RENEWAL_DATETIME =
            "in.simplypark.spot_all.MONTHLY_PASS_NEXT_RENEWAL_DATETIME";

    public static final String BUNDLE_PARAM_MONTHLY_PASS_ISSUE_DATE =
            "in.simplypark.spot_all.MONTHLY_PASS_ISSUE_DATE";
    public static final String BUNDLE_PARAM_MONTHLY_PASS_OLD_ISSUE_DATE =
            "in.simplypark.spot_all.MONTHLY_PASS_OLD_ISSUE_DATE";

    public static final String BUNDLE_PARAM_MONTHLY_PASS_TRANS_ID =
            "in.simplypark.spot_all.MONTHLY_PASS_TRANS_ID";

    public static final String BUNDLE_PARAM_MONTHLY_PASS_PER_MONTH_FEES =
            "in.simplypark.spot_all.MONTHLY_PASS_PER_MONTH_FEES";

    public static final String BUNDLE_PARAM_MONTHLY_PASS_DISCOUNT =
            "in.simplypark.spot_all.MONTHLY_PASS_DISCOUNT";

    public static final String BUNDLE_PARAM_STAFF_MONTHLY_PASS =
            "in.simplypark.spot_all.IS_STAFF_MONTHLY_PASS";

    public static final String BUNDLE_PARAM_LOST_MONTHLY_PASS_FEE =
            "in.simplypark.spot_all.LOST_MONTHLY_PASS_FEE";


    public static final String BUNDLE_PARAM_IS_RENEWAL =
            "in.simplypark.spot_all.IS_RENEWAL";


    public static final String BUNDLE_PARAM_PARENT_BOOKING_ID =
            "in.simplypark.spot_all.PARENT_BOOKING_ID";

    public static final String BUNDLE_PARAM_PAYMENT_MODE =
            "in.simplypark.spot_all.PAYMENT_MODE";

    public static final String BUNDLE_PARAM_IS_CALLED_FROM_DATE_CHANGE_INTENT =
            "in.simplypark.spot_all.IS_CALLED_FROM_DATE_CHANGE_INTENT";

    public static final String BUNDLE_PARAM_MONTHLY_PASS_RECEIPT_AMOUNT_RECEIVD =
            "in.simplypark.spot_all.MONTHLY_PASS_RECEIPT_AMOUNT_RECEIVED";

    public static final String BUNDLE_PARAM_MONTHLY_PASS_RECEIPT_TRANS_ID =
            "in.simplypark.spot_all.MONTHLY_PASS_RECEIPT_TRANS_ID";

    public static final String BUNDLE_PARAM_MONTHLY_PASS_RECEIPT_NEXT_PAYMENT_DATE=
            "in.simplypark.spot_all.MONTHLY_PASS_RECEIPT_NEXT_PAYMENT_DATE";




    public static final String BUNDLE_PARAM_REPORT_PERIOD_MONTHLY_PASS_BIKES_INSIDE =
            "in.simplypark.spot_all.REPORT_PERIOD_MISSING_MONTHLY_PASS_BIKES_INSIDE";

    public static final String BUNDLE_PARAM_REPORT_PERIOD_NUM_PASSES_ISSUED =
            "in.simplypark.spot_all.REPORT_PERIOD_NUM_PASSES_ISSUED";

    public static final String BUNDLE_PARAM_REPORT_PERIOD_NUM_CAR_PASSES_ISSUED =
            "in.simplypark.spot_all.REPORT_PERIOD_NUM_CAR_PASSES_ISSUED";

    public static final String BUNDLE_PARAM_REPORT_PERIOD_NUM_BIKE_PASSES_ISSUED =
            "in.simplypark.spot_all.REPORT_PERIOD_NUM_BIKE_PASSES_ISSUED";

    public static final String BUNDLE_PARAM_REPORT_GATEWISE_EARNING_PRINT_STRING = "" +
            "in.simplypark.spot_all.REPORT_GATEWISE_EARNING_PRINT_STRING";

    public static final String BUNDLE_PARAM_REPORT_GATEWISE_PASS_EARNING_PRINT_STRING = "" +
            "in.simplypark.spot_all.REPORT_GATEWISE_PASS_EARNING_PRINT_STRING";


    public static final String BUNDLE_PARAM_OP_APP_UPDATE_NAME =
            "in.simplypark.spot_all.OP_APP_UPDATE_NAME";
    public static final String BUNDLE_PARAM_OP_APP_UPDATE_EMAIL =
            "in.simplypark.spot_all.OP_APP_UPDATE_EMAIL";
    public static final String BUNDLE_PARAM_OP_APP_UPDATE_MOBILE =
            "in.simplypark.spot_all.OP_APP_UPDATE_MOBILE";
    public static final String BUNDLE_PARAM_OP_APP_UPDATE_BOOKING_ID =
            "in.simplypark.spot_all.OP_APP_UPDATE_BOOKING_ID";
    public static final String BUNDLE_PARAM_OP_APP_UPDATE_BOOKING_STATUS =
            "in.simplypark.spot_all.OP_APP_UPDATE_BOOKING_STATUS";
    public static final String BUNDLE_PARAM_OP_APP_UPDATE_BOOKING_START =
            "in.simplypark.spot_all.OP_APP_UPDATE_BOOKING_START";
    public static final String BUNDLE_PARAM_OP_APP_UPDATE_BOOKING_END =
            "in.simplypark.spot_all.OP_APP_UPDATE_BOOKING_END";
    public static final String BUNDLE_PARAM_OP_APP_UPDATE_PASS_ISSUE =
            "in.simplypark.spot_all.OP_APP_UPDATE_PASS_ISSUE";
    public static final String BUNDLE_PARAM_OP_APP_UPDATE_ENCODE_PASS_CODE =
            "in.simplypark.spot_all.OP_APP_UPDATE_ENCODE_PASS_CODE";
    public static final String BUNDLE_PARAM_OP_APP_UPDATE_SERIAL_NUM =
            "in.simplypark.spot_all.OP_APP_UPDATE_SERIAL_NUM";
    public static final String BUNDLE_PARAM_OP_APP_UPDATE_VEHICLE_REG =
            "in.simplypark.spot_all.OP_APP_UPDATE_VEHICLE_REG";
    public static final String BUNDLE_PARAM_OP_APP_UPDATE_VEHICLE_TYPE =
            "in.simplypark.spot_all.OP_APP_UPDATE_VEHICLE_TYPE";
    public static final String BUNDLE_PARAM_OP_APP_UPDATE_NUM_BOOKED_SLOTS =
            "in.simplypark.spot_all.OP_APP_UPDATE_BOOKED_SLOTS";
    public static final String BUNDLE_PARAM_OP_APP_UPDATE_PARKING_FEES =
            "in.simplypark.spot_all.OP_APP_UPDATE_PARKING_FEES";
    public static final String BUNDLE_PARAM_OP_APP_UPDATE_DISCOUNT =
            "in.simplypark.spot_all.OP_APP_UPDATE_DISCOUNT";
    public static final String BUNDLE_PARAM_OP_APP_UPDATE_SPACE_OWNER_ID =
            "in.simplypark.spot_all.OP_APP_UPDATE_SPACE_OWNER_ID";
    public static final String BUNDLE_PARAM_OP_APP_UPDATE_SYNC_ID =
            "in.simplypark.spot_all.OP_APP_UPDATE_SYNC_ID";
    public static final String BUNDLE_PARAM_OP_APP_UPDATE_IS_RENEWAL =
            "in.simplypark.spot_all.OP_APP_UPDATE_IS_RENEWAL";
    public static final String BUNDLE_PARAM_OP_APP_UPDATE_PARENT_BOOKING_ID =
            "in.simplypark.spot_all.OP_APP_UPDATE_PARENT_BOOKING_ID";

    public static final String BUNDLE_PARAM_OP_APP_UPDATE_PAYMENT_MODE =
            "in.simplypark.spot_all.OP_APP_UPDATE_PAYMENT_MODE";

    public static final String BUNDLE_PARAM_OP_APP_UPDATE_INVOICE_ID =
            "in.simplypark.spot_all.OP_APP_UPDATE_INVOICE_ID";

    public static final String BUNDLE_PARAM_OP_APP_UPDATE_PAYMENT_ID =
            "in.simplypark.spot_all.OP_APP_UPDATE_PAYMENT_ID";

    public static final String BUNDLE_PARAM_IS_STAFF_PASS =
            "in.simplypark.spot_all.IS_STAFF_PASS";



    public static final String BUNDLE_PARAM_SYNC_ID =
            "in.simplypark.spot_all.SYNC_ID";

    public static final String BUNDLE_PARAM_IS_PARTIAL_SUCCESS =
            "in.simplypark.spot_all.IS_PARTIAL_SUCCESS";

    public static final String BUNDLE_PARAM_TIMER_ACTION =
            "in.simplypark.spot_all.TIMER_ACTION";

    public static final String BUNDLE_PARAM_IS_EXIT_RECORDED = "in.simplypark.spot_app.IS_EXIT_RECORDED";


    public static final String BUNDLE_PARAM_IS_EXIT_THROUGH_PASS =
            "in.simplypark.spot_app.IS_EXIT_THROUGH_PASS";


    public static final String BUNDLE_PARAM_IS_TRANSACTION_SP =
                                                    "in.simplypark.spot_app.IS_TRANSACTION_SP";

    public static final String BUNDLE_PARAM_GENERATE_PASS_NAME =
                                                   "in.simplypark.spot_app.GENERATE_PASS_NAME";

    public static final String BUNDLE_PARAM_USER_ID    = "in.simplypark.spot_app.USER_ID";
    public static final String BUNDLE_PARAM_USER_EMAIL = "in.simplypark.spot_app.EMAIL_ID";

    public static final String BUNDLE_PARAM_INVOICE_ID = "in.simplypark.spot_app.INVOICE_ID";
    public static final String BUNDLE_PARAM_PAYMENT_COLLECTION_RESULT =
                                         "in.simplypark.spot_app.PAYMENT_COLLECTION_RESULT";
    public static final String BUNDLE_PARAM_PAYMENT_ID = "in.simplypark.spot_app.PAYMENT_ID";
    public static final String BUNDLE_PARAM_OP_APP_UPDATE_OPERATOR_ID =
            "in.simplypark.spot_app.OPERATOR_ID";

    public static final String BUNDLE_PARAM_PRINTER_RESCAN_PROCESS_ONGOING =
            "in.simplypark.spot_app.IS_PRINTER_RESCAN_PROCESS_ONGOING";

    public static final String BUNDLE_PARAM_IS_SYNC_RESET_REQUIRED =
            "in.simplypark.spot_app.IS_SYNC_RESET_REQUIRED";

    public static final String BUNDLE_PARAM_TOKEN = "in.simplypark.spot_app.REUSABLE_TOKEN";

     public static final int BOOKING_ID_LENGTH = 13;

     //server sync related constnats
    public static final int IN_SYNC = 0;
    public static final int OUT_OF_SYNC_PULL_REQUIRED = 1;
    public static final int OUT_OF_SYNC_PUSH_REQUIRED = 2;



    //qrDroid related constants
    public static final String SIMPLYPARK_BOOKING_QR_CODE_SCAN = "la.droid.qr.scan";
    public static final String ACTIVATE_MONTHLY_PASS_QR_CODE_SCAN = SIMPLYPARK_BOOKING_QR_CODE_SCAN;

    public static final String  SIMPLYPARK_BOOKING_SCANNING_RESULT = "la.droid.qr.result";
    public static final String ACTIVATE_MONTHLY_PASS_SCANNING_RESULT = SIMPLYPARK_BOOKING_SCANNING_RESULT;
    public static final int SIMPLYPARK_BOOKING_SCANNING_REQUEST_CODE = 0;
    public static final int ACTIVATE_MONTHLY_PASS_SCANNING_REQUEST_CODE = 0;
    public static final int ENTRY_REQUEST_CODE = 1;
    public static final int EXIT_REQUEST_CODE = 2;
    public static final int EXIT_BARCODE_READ_REQUEST = 3;
    public static final int SP_ENTRY_REQUEST_CODE = 4;
    public static final int EXIT_MONTHLY_PASS_READ_REQUEST = 5;
    public static final int TRANS_DETAIL_REQUEST = 6;

    //play services related constants
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    //exit related constants
    public static final int QUERY_RECEIPT_BY_CAR_REG = 1;
    public static final int QUERY_RECEIPT_BY_PARKING_ID = 2;
    public static final int QUERY_RECEIPT_BY_BOOKING_ID = 3;
    public static final int QUERY_RECEIPT_BY_TOKEN = 4;
    public static final int PARKING_STATUS_NONE = 0;
    public static final int PARKING_STATUS_ENTERED = 1;
    public static final int PARKING_STATUS_EXITED = 2;
    public static final int PARKING_STATUS_RE_ENTRY = 3;
    public static final int PARKING_STATUS_EXIT_WITHOUT_ENTRY = 4;
    public static final int PARKING_STATUS_ALREADY_USED = 5;
    public static final int PARKING_STATUS_TRANSACTION_CANCELLED = 6;
    public static final int PARKING_STATUS_EXIT_NOT_RECORDED = 7;



    //Ticket format related constants
    public static final int PRINTER_FIRST_LINE    = 0;
    public static final int PRINTER_SECOND_LINE   = 1;
    public static final int PRINTER_THIRD_LINE    = 2;
    public static final int PRINTER_FOURTH_LINE   = 3;
    public static final int PRINTER_FIFTH_LINE    = 4;
    public static final int PRINTER_SIXTH_LINE    = 5;
    public static final int PRINTER_SEVENTH_LINE  = 6;
    public static final int PRINTER_EIGTH_LINE    = 7;
    public static final int PRINTER_NINTH_LINE    = 8;
    public static final int PRINTER_TENTH_LINE    = 9;
    public static final int PRINTER_ELEVENTH_LINE = 10;
    public static final int PRINTER_TWELFTH_LINE  = 11;
    public static final int PRINTER_THIRTEEN_LINE = 12;
    public static final int PRINTER_FOURTEEN_LINE = 13;
    public static final int PRINTER_FIFTEEN_LINE  = 14;
    public static final int PRINTER_SIXTEEN_LINE  = 15;
    public static final int PRINTER_SEVENTEEN_LINE = 16;
    public static final int PRINTER_EIGTHTEEN_LINE   = 17;
    public static final int PRINTER_NINETEEN_LINE    = 18;
    public static final int PRINTER_TWENTH_LINE      = 19;
    public static final int PRINTER_TWENTYFIRST_LINE = 20;
    public static final int PRINTER_TWENTYSECOND_LINE = 21;
    public static final int PRINTER_TWENTYTHIRD_LINE = 22;
    public static final int PRINTER_TWENTYFOURTH_LINE = 23;
    public static final int PRINTER_TWENTYFIFTH_LINE = 24;
    public static final int PRINTER_TWENTYSIXTH_LINE = 25;
    public static final int PRINTER_TWENTYSEVENTH_LINE = 26;
    public static final int PRINTER_TWENTYEIGTH_LINE = 27;
    public static final int PRINTER_TWENTYNINTH_LINE = 28;
    public static final int PRINTER_THIRTETH_LINE = 29;
    public static final int PRINTER_THIRTYFIRST_LINE = 30;
    public static final int PRINTER_THIRTYSECOND_LINE = 31;
    public static final int PRINTER_THIRTYTHIRD_LINE = 32;
    public static final int PRINTER_THIRTYFOURTH_LINE = 33;
    public static final int PRINTER_THIRTYFIFTH_LINE = 34;
    public static final int PRINTER_THIRTYSIXTH_LINE = 35;
    public static final int PRINTER_THIRTYSEVENTH_LINE = 36;
    public static final int PRINTER_THIRTYEIGTH_LINE = 37;
    public static final int PRINTER_THIRTYNINTH_LINE = 38;
    public static final int PRINTER_FOURTHETH_LINE = 39;
    public static final int PRINTER_FOURTYFIRST_LINE = 40;
    public static final int PRINTER_FOURTYSECOND_LINE = 41;
    public static final int PRINTER_FOURTYTHIRD_LINE = 42;
    public static final int PRINTER_FOURTYFOURTH_LINE = 43;
    public static final int PRINTER_FOURTYFIFTH_LINE = 44;



    //vehicle type constants
    public static final int VEHICLE_TYPE_INVALID =0;
    public static final int VEHICLE_TYPE_CAR = 1;
    public static final int VEHICLE_TYPE_BIKE = 2;
    public static final int VEHICLE_TYPE_MINIBUS =3;
    public static final int VEHICLE_TYPE_BUS =4;
    public static final int MAX_SUPPORTED_VEHICLE_TYPE = 4;

    public static final Map<String,Integer> VEHICLE_TYPE_MAP=  new HashMap<String, Integer>();
    static {
        VEHICLE_TYPE_MAP.put("Car",VEHICLE_TYPE_CAR);
        VEHICLE_TYPE_MAP.put("Bike",VEHICLE_TYPE_BIKE);
        VEHICLE_TYPE_MAP.put("Mini-Bus",VEHICLE_TYPE_MINIBUS);
        VEHICLE_TYPE_MAP.put("Bus",VEHICLE_TYPE_BUS);
    }

    //type of content
    public static final int PRINTER_TYPE_TEXT = 0;
    public static final int PRINTER_TYPE_IMG = 1;
    public static final int PRINTER_TYPE_BARCODE =2;
    public static final int PRINTER_TYPE_NEWLINE = 3;

    //alignment
    public static final int PRINTER_ALIGN_NO_ALIGN = 0; //use it when multiple elements on the same
                                                        // row
    public static final int PRINTER_ALIGN_LEFT = 1;
    public static final int PRINTER_ALIGN_CENTER = 2;
    public static final int PRINTER_ALIGN_RIGHT = 3;

    public static final int PRINTER_DEFAULT_LEFT_MARGIN = 2;
    public static final int PRINTER_DEFAULT_RIGHT_MARGIN = 2;

    //size
    public static final int PRINTER_SIZE_BIG = 0;
    public static final int PRINTER_SIZE_MEDIUM = 1;
    public static final int PRINTER_SIZE_MEDIUM_BIG = 3;
    public static final int PRINTER_SIZE_SMALL = 2;

    //height
    public static final int PRINTER_HEIGHT_DOUBLE = 1;

    //width-height
    public static final int PRINTER_WIDTH_HEIGHT_DOUBLE = 1;
    public static final int PRINTER_WIDTH_HEIGHT_NORMAL = 0;

    //fonts
    public static final int PRINTER_FONT_COURIER = 1;
    public static final int PRINTER_FONT_SANS_SERIF = 2;
    public static final int PRINTER_FONT_BASE = PRINTER_FONT_COURIER;

    //style
    public static final int PRINTER_STYLE_NORMAL = 1;
    public static final int PRINTER_STYLE_BOLD = 2;

    public static final char TIER_PRICE_SEPERATOR = 'X';


    //Entry Ticket format related values

    public static final int PRINTER_KEY_HEADING         = 1;
    public static final int PRINTER_KEY_SPACE_ADDRESS   = 2;
    public static final int PRINTER_KEY_PARKING_ID      = 3;
    public static final int PRINTER_KEY_ENTRY_DATE_TIME = 4;
    public static final int PRINTER_KEY_REG_NUMBER      = 5;
    public static final int PRINTER_KEY_PARKING_RATES   = 6;
    public static final int PRINTER_KEY_URL             = 7;
    public static final int PRINTER_KEY_DESC            = 8;
    public static final int PRINTER_KEY_PARK_AND_RIDE   = 9;
    public static final int PRINTER_NEWLINE             = 10;
    public static final int PRINTER_KEY_LINE            = 11;
    public static final int PRINTER_KEY_OPERATING_HOURS = 12;
    public static final int PRINTER_KEY_LOST_TICKET     = 13;


    //Exit Ticket format related values

    public static final int PRINTER_KEY_EXIT_HEADING     = 14;
    public static final int PRINTER_KEY_EXIT_DATE_TIME   = 15;
    public static final int PRINTER_KEY_PARKING_FEES     = 16;
    public static final int PRINTER_KEY_EXIT_DESC        = 17;
    public static final int PRINTER_KEY_DURATION         = 18;

    public static final int PRINTER_KEY_REPORT_HEADING  = 19;
    public static final int PRINTER_KEY_REPORT_DATE     = 20;
    public static final int PRINTER_KEY_REPORT_CARS_PARKED = 21;
    public static final int PRINTER_KEY_REPORT_BIKES_PARKED = 22;
    public static final int PRINTER_KEY_REPORT_TOTAL_EARNINGS = 23;
    public static final int PRINTER_KEY_REPORT_CAR_EARNINGS = 24;
    public static final int PRINTER_KEY_REPORT_BIKE_EARNINGS = 25;
    public static final int PRINTER_KEY_REPORT_TOTAL_VEHICLES_PARKED=26;
    public static final int PRINTER_KEY_REPORT_TOTAL_MISSING_EXITS=27;
    public static final int PRINTER_KEY_REPORT_TOTAL_CAR_MISSING_EXITS=28;
    public static final int PRINTER_KEY_REPORT_TOTAL_BIKE_MISSING_EXITS=29;
    public static final int PRINTER_KEY_REPORT_TOTAL_VEHICLES_INSIDE = 30;
    public static final int PRINTER_KEY_REPORT_TOTAL_CARS_INSIDE = 31;
    public static final int PRINTER_KEY_REPORT_TOTAL_BIKES_INSIDE = 32;
    public static final int PRINTER_KEY_REPORT_GENERATION_TIME = 33;

    public static final int PRINTER_KEY_REPORT_MONTHLY_PASS_CARS_PARKED = 34;
    public static final int PRINTER_KEY_REPORT_MONTHLY_PASS_BIKES_PARKED = 35;
    public static final int PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_CARS_INSIDE = 36;
    public static final int PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_BIKES_INSIDE = 37;
    public static final int PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_CAR_MISSING_EXITS=38;
    public static final int PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_BIKE_MISSING_EXITS=39;


    //Generated Monthly Pass related values
    public static final int PRINTER_KEY_MONTHLY_PASS_HEADING = 40;
    public static final int PRINTER_KEY_MONTHLY_PASS_ISSUED_TO = 41;
    public static final int PRINTER_KEY_MONTHLY_PASS_EMAIL = 42;
    public static final int PRINTER_KEY_MONTHLY_PASS_TEL = 43;
    public static final int PRINTER_KEY_MONTHLY_PASS_VALID_FROM = 44;
    public static final int PRINTER_KEY_MONTHLY_PASS_VALID_TILL = 45;
    public static final int PRINTER_KEY_MONTHLY_PASS_TRANS_ID = 46;
    public static final int PRINTER_KEY_MONTHLY_PASS_FEES = 47;
    public static final int PRINTER_KEY_MONTHLY_PASS_PAY_FEES_ONLINE = 48;
    //monthly pass receipt related values
    public static final int PRINTER_KEY_MONTHLY_PASS_RECEIPT_HEADING = 49;
    public static final int PRINTER_KEY_MONTHLY_PASS_RECEIPT_AMOUNT_RECEIVED = 50;
    public static final int PRINTER_KEY_MONTHLY_PASS_RECEIPT_TRANS_ID = 51;
    public static final int PRINTER_KEY_MONTHLY_PASS_RECEIPT_NEXT_PAYMENT_DUE = 52;
    public static final int PRINTER_KEY_MONTHLY_PASS_RECEIPT_PAY_FEES_ONLINE = 53;

    //metro monthly pass related constants.
    public static final int PRINTER_KEY_REPORT_MONTHLY_PASS_EARNINGS = 54;
    public static final int PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_ISSUED = 55;
    public static final int PRINTER_KEY_REPORT_TOTAL_CAR_MONTHLY_PASS_ISSUED = 56;
    public static final int PRINTER_KEY_REPORT_TOTAL_BIKE_MONTHLY_PASS_ISSUED = 57;
    public static final int PRINTER_KEY_NIGHT_CHARGE = 58;

    //metro related parameters added
    public static final int PRINTER_KEY_AGENCY_NAME = 59;
    public static final int PRINTER_KEY_SPACE_OWNER_MOBILE = 60;
    public static final int PRINTER_KEY_DOTTED_LINE = 61;
    public static final int PRINTER_KEY_MONTHLY_PARKING_RATES_INCL_NIGHT_CHARGE = 62;

    //new minibus and bus related constants
    public static final int PRINTER_KEY_REPORT_MINIBUS_EARNINGS = 63;
    public static final int PRINTER_KEY_REPORT_BUS_EARNINGS = 64;
    public static final int PRINTER_KEY_REPORT_MINIBUS_PARKED = 65;
    public static final int PRINTER_KEY_REPORT_MONTHLY_PASS_MINIBUS_PARKED = 66;
    public static final int PRINTER_KEY_REPORT_BUS_PARKED = 67;
    public static final int PRINTER_KEY_REPORT_MONTHLY_PASS_BUS_PARKED = 68;
    public static final int PRINTER_KEY_REPORT_TOTAL_MINIBUS_INSIDE = 69;
    public static final int PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_MINIBUS_INSIDE = 70;
    public static final int PRINTER_KEY_REPORT_TOTAL_BUS_INSIDE = 71;
    public static final int PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_BUS_INSIDE = 72;
    public static final int PRINTER_KEY_REPORT_TOTAL_MINIBUS_MISSING_EXITS = 73;
    public static final int PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_MINIBUS_MISSING_EXITS = 74;
    public static final int PRINTER_KEY_REPORT_TOTAL_BUS_MISSING_EXITS = 75;
    public static final int PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_BUS_MISSING_EXITS = 76;
    public static final int PRINTER_KEY_REPORT_TOTAL_MINIBUS_MONTHLY_PASS_ISSUED = 77;
    public static final int PRINTER_KEY_REPORT_TOTAL_BUS_MONTHLY_PASS_ISSUED  =78;

    public static final int PRINTER_KEY_DYNAMIC_AD = 79;
    public static final int PRINTER_KEY_EXIT_SIMPLYPARK_AD = 80;











    public static final int PRINTER_KEY_CAR_PARKING_RATES = 333;
    public static final int PRINTER_KEY_BIKE_PARKING_RATES = 334;
    public static final int MONTHLY_PARKING_CAR_RATES_INCL_NIGHT_CHARGE = 335;
    public static final int MONTHLY_PARKING_BIKE_RATES_INCL_NIGHT_CHARGE = 336;
    public static final int PRINTER_KEY_CAR_NIGHT_CHARGE = 337;
    public static final int PRINTER_KEY_BIKE_NIGHT_CHARGE = 338;
    public static final int PRINTER_KEY_MINIBUS_PARKING_RATES = 339;
    public static final int PRINTER_KEY_BUS_PARKING_RATES = 340;
    public static final int MONTHLY_PARKING_MINIBUS_RATES_INCL_NIGHT_CHARGE = 341;
    public static final int MONTHLY_PARKING_BUS_RATES_INCL_NIGHT_CHARGE = 342;

    public static final int PRINTER_KEY_REPORT_GATEWISE_EARNING = 343;
    public static final int PRINTER_KEY_REPORT_GATEWISE_PASS_EARNING = 344;


    public static final int FOUR_DIGIT_REG_NUMBER = 4;
    public static final int FIVE_DIGIT_REG_NUMBER = 5;



    public static final String PRINTER_POWERED_BY_STRING = "powered by ";
    public static final String PRINTER_DISCLAIMER_STRING = "Parking at Owners Risk, " +
                                                           "No resp. of loosing helmet, " +
                                                           "parts,laptop, mobile, handbag etc";
    public static final String PRINTER_PARK_AND_RIDE_STRING = "R";
    public static final String PRINTER_URL_STRING = "  " + PRINTER_POWERED_BY_STRING  +
                           "simplypark.in";
    public static final String PRINTER_DYNAMIC_INFO = "dynamic";
    public static final String PRINTER_UNUSED = "unused";
    public static final String PRINTER_HEADING = "Parking Ticket";
    public static final String PRINTER_EXIT_HEADING = "   Receipt";
    public static final String PRINTER_NEW_LINE = "\\r";
    public static final String PRINTER_LINE_STR = "_";
    public static final String PRINTER_KEY_EXIT_DESC_STR = "Say hello to us "+
                                                           "customercare@simplypark.in";
    public static final String PRINTER_KEY_REPORT_HEADING_STR = "Parking Report";
    public static final String PRINTER_KEY_MONTHLY_PASS_HEADING_STR = "Month Pass";
    public static final String PRINTER_KEY_MONTHLY_PASS_PAY_FEES_ONLINE_STR =
            "You can renew/recharge your monthly pass @ SimplyPark.in";

    public static final String[] PRINTER_VALUE_PER_KEY = {PRINTER_UNUSED,                /*0*/
            PRINTER_DYNAMIC_INFO,          /*1*/
            PRINTER_DYNAMIC_INFO,          /*2*/
            PRINTER_DYNAMIC_INFO,          /*3*/
            PRINTER_DYNAMIC_INFO,          /*4*/
            PRINTER_DYNAMIC_INFO,          /*5*/
            PRINTER_DYNAMIC_INFO,          /*6*/
            PRINTER_DYNAMIC_INFO,          /*7*/
            PRINTER_DYNAMIC_INFO,          /*8*/
            PRINTER_PARK_AND_RIDE_STRING,  /*9*/
            PRINTER_NEW_LINE,              /*10*/
            PRINTER_LINE_STR,              /*11*/
            PRINTER_DYNAMIC_INFO,          /*12*/
            PRINTER_DYNAMIC_INFO,          /*13*/
            PRINTER_EXIT_HEADING,          /*14*/
            PRINTER_DYNAMIC_INFO,          /*15*/
            PRINTER_DYNAMIC_INFO,          /*16*/
            PRINTER_KEY_EXIT_DESC_STR,     /*17*/
            PRINTER_DYNAMIC_INFO,          /*18*/
            PRINTER_KEY_REPORT_HEADING_STR,/*19*/
            PRINTER_DYNAMIC_INFO,          /*20*/
            PRINTER_DYNAMIC_INFO,          /*21*/
            PRINTER_DYNAMIC_INFO,          /*22*/
            PRINTER_DYNAMIC_INFO,          /*23*/
            PRINTER_DYNAMIC_INFO,          /*24*/
            PRINTER_DYNAMIC_INFO,          /*25*/
            PRINTER_DYNAMIC_INFO,          /*26*/
            PRINTER_DYNAMIC_INFO,          /*27*/
            PRINTER_DYNAMIC_INFO,          /*28*/
            PRINTER_DYNAMIC_INFO,          /*29*/
            PRINTER_DYNAMIC_INFO,          /*30*/
            PRINTER_DYNAMIC_INFO,          /*31*/
            PRINTER_DYNAMIC_INFO,          /*32*/
            PRINTER_DYNAMIC_INFO,          /*33*/
            PRINTER_DYNAMIC_INFO,          /*34*/
            PRINTER_DYNAMIC_INFO,          /*35*/
            PRINTER_DYNAMIC_INFO,          /*36*/
            PRINTER_DYNAMIC_INFO,          /*37*/
            PRINTER_DYNAMIC_INFO,          /*38*/
            PRINTER_DYNAMIC_INFO,          /*39*/
            PRINTER_KEY_MONTHLY_PASS_HEADING_STR,      /*40*/
            PRINTER_DYNAMIC_INFO,          /*41*/
            PRINTER_DYNAMIC_INFO,          /*42*/
            PRINTER_DYNAMIC_INFO,          /*43*/
            PRINTER_DYNAMIC_INFO,          /*44*/
            PRINTER_DYNAMIC_INFO,          /*45*/
            PRINTER_DYNAMIC_INFO,          /*46*/
            PRINTER_DYNAMIC_INFO,          /*47*/
            PRINTER_KEY_MONTHLY_PASS_PAY_FEES_ONLINE_STR,          /*48*/
            PRINTER_DYNAMIC_INFO,          /*49*/
            PRINTER_DYNAMIC_INFO,          /*50*/
            PRINTER_DYNAMIC_INFO,          /*51*/
            PRINTER_DYNAMIC_INFO,          /*52*/
            PRINTER_DYNAMIC_INFO,          /*53*/
            PRINTER_DYNAMIC_INFO,          /*54*/
            PRINTER_DYNAMIC_INFO,          /*55*/
            PRINTER_DYNAMIC_INFO,          /*56*/
            PRINTER_DYNAMIC_INFO,          /*57*/
            PRINTER_DYNAMIC_INFO,          /*58*/
            PRINTER_DYNAMIC_INFO,          /*59*/
            PRINTER_DYNAMIC_INFO,          /*60*/
            PRINTER_DYNAMIC_INFO,          /*61*/
            PRINTER_DYNAMIC_INFO,          /*62*/
            PRINTER_DYNAMIC_INFO,          /*63*/
            PRINTER_DYNAMIC_INFO,          /*64*/
            PRINTER_DYNAMIC_INFO,          /*65*/
            PRINTER_DYNAMIC_INFO,          /*66*/
            PRINTER_DYNAMIC_INFO,          /*67*/
            PRINTER_DYNAMIC_INFO,          /*68*/
            PRINTER_DYNAMIC_INFO,          /*69*/
            PRINTER_DYNAMIC_INFO,          /*70*/
            PRINTER_DYNAMIC_INFO,          /*71*/
            PRINTER_DYNAMIC_INFO,          /*72*/
            PRINTER_DYNAMIC_INFO,          /*73*/
            PRINTER_DYNAMIC_INFO,          /*74*/
            PRINTER_DYNAMIC_INFO,          /*75*/
            PRINTER_DYNAMIC_INFO,          /*76*/
            PRINTER_DYNAMIC_INFO,          /*77*/
            PRINTER_DYNAMIC_INFO,          /*78*/
            PRINTER_DYNAMIC_INFO,          /*79*/
            PRINTER_DYNAMIC_INFO,          /*80*/

    };









    public static final String PRINTER_VEHICLE_NUMBER_STRING = "Vehicle";

    public static final int PRINTER_MAX_HEADER_ELEMENTS = 4;
    public static final int PRINTER_MAX_HEADER_SUB_ELEMENTS = 1;

    public static final int PRINTER_MAX_ENTRY_BODY_ELEMENTS = 7;
    public static final int PRINTER_MAX_ENTRY_BODY_SUB_ELEMENTS = 1;

    public static final int PRINTER_MAX_ENTRY_FOOTER_ELEMENTS = 5;
    public static final int PRINTER_MAX_ENTRY_FOOTER_SUB_ELEMENTS = 1;

    public static final int PRINTER_MAX_EXIT_HEADER_ELEMENTS = 4;
    public static final int PRINTER_MAX_EXIT_HEADER_SUB_ELEMENTS = 1;

    public static final int PRINTER_MAX_EXIT_BODY_ELEMENTS = 7;
    public static final int PRINTER_MAX_EXIT_BODY_SUB_ELEMENTS = 1;

    public static final int PRINTER_MAX_EXIT_FOOTER_ELEMENTS = 6;
    public static final int PRINTER_MAX_EXIT_FOOTER_SUB_ELEMENTS = 1;

    //daily report related Constants
    public static final int PRINTER_MAX_REPORT_HEADER_ELEMENTS = 2;
    public static final int PRINTER_MAX_REPORT_HEADER_SUB_ELEMENTS =1;

    public static final int PRINTER_MAX_REPORT_BODY_ELEMENTS = 42;
    public static final int PRINTER_MAX_REPORT_BODY_SUB_ELEMENTS = 1;

    public static final int PRINTER_MAX_REPORT_FOOTER_ELEMENTS = 2;
    public static final int PRINTER_MAX_REPORT_FOOTER_SUB_ELEMENTS = 1;

    //generated monthly pass related Constants
    public static final int PRINTER_MAX_MONTHLY_PASS_HEADER_ELEMENTS = 4;
    public static final int PRINTER_MAX_MONTHLY_PASS_HEADER_SUB_ELEMENTS = 1;

    public static final int PRINTER_MAX_MONTHLY_PASS_FOOTER_ELEMENTS = 4;
    public static final int PRINTER_MAX_MONTHLY_PASS_FOOTER_SUB_ELEMENTS = 1;

    public static final int PRINTER_MAX_MONTHLY_PASS_BODY_ELEMENTS = 10;
    public static final int PRINTER_MAX_MONTHLY_PASS_BODY_SUB_ELEMENTS = 1;

    //monthly pass receipt format
    public static final int PRINTER_MAX_MONTHLY_PASS_RECEIPT_HEADER_ELEMENTS = 2;
    public static final int PRINTER_MAX_MONTHLY_PASS_RECEIPT_HEADER_SUB_ELEMENTS = 1;

    public static final int PRINTER_MAX_MONTHLY_PASS_RECEIPT_FOOTER_ELEMENTS = 4;
    public static final int PRINTER_MAX_MONTHLY_PASS_RECEIPT_FOOTER_SUB_ELEMENTS = 1;

    public static final int PRINTER_MAX_MONTHLY_PASS_RECEIPT_BODY_ELEMENTS = 3;
    public static final int PRINTER_MAX_MONTHLY_PASS_RECEIPT_BODY_SUB_ELEMENTS = 1;



    public static final String ERROR_NO_CONNECTION = "Update Failed. No network Connectivity";
    public static final String ERROR_INVALID_RESPONSE = "Update Failed.Invalid Response from Server";
    public static final String ERROR_NO_SP_BOOKING_AVAILABLE = "Invalid Booking Response from Server";
    public static final String ERROR_NO_SUCH_BOOKING = "No such booking exists.";
    public static final int ERROR_INVALID_CLOCK = -1;

    public static final int NUM_SECONDS_IN_A_DAY = 86400;
    public static final int NUM_SECONDS_IN_A_HOUR = 3600;
    public static final int NUM_SECONDS_IN_A_MINUTE = 60;
    public static final int NUM_HOURS_IN_A_DAY = 24;
    public static final int NUM_MS_IN_MINUTE = 60000;
    public static final int NUM_MS_IN_SECOND = 1000;

    public static final String FULL_DAY_OPENING_TIME = "00:00:00";
    public static final String FULL_DAY_CLOSING_TIME = "23:59:59";

    public static final int NIGHT_START_TIME = 0;
    public static final int NIGHT_END_TIME   = 5;

    public static final String SYNC_PUSH_MSG_TYPE_KEY = "msg_type";
    public static final int SYNC_PUSH_MSG_TYPE_OFFLINE_UPDATE = 1;
    public static final int SYNC_PUSH_MSG_TYPE_ONLINE_UPDATE = 2;
    public static final int SYNC_PUSH_MSG_TYPE_OP_APP_BOOKING_UPDATE = 3;
    public static final int SYNC_PUSH_MSG_TYPE_OP_APP_BOOKING_MODIFIED = 4;
    //sync id related Constants
    public static final String SYNC_OFFLINE_ENTRY_EXIT_UPDATE = "OfflineEntryExitTime";
    public static final String SYNC_PARKING_ID = "parking_id";
    public static final String SYNC_REG_NUMBER = "car_reg";
    public static final String SYNC_VEHICLE_TYPE = "vehicle_type";
    public static final String SYNC_ENTRY_TIME = "entry_time";
    public static final String SYNC_EXIT_TIME  = "exit_time";
    public static final String SYNC_STATUS = "status";
    public static final String SYNC_PARKING_FEES = "parking_fees";
    public static final String SYNC_PARK_AND_RIDE = "is_park_and_ride";
    public static final String SYNC_IS_UPDATED = "is_updated";
    public static final String SYNC_OPERATOR_ID = "operator_id";
    public static final String SYNC_REUSABLE_TOKEN = "reusable_token";

    public static final String SYNC_ONLINE_BOOKING_UPDATE = "Booking";
    public static final String SYNC_ONLINE_BOOKING_SERIAL_NUM = "serial_num";
    public static final String SYNC_ONLINE_BOOKING_ENCODE_PASS_CODE = "encode_pass_code";
    public static final String SYNC_ONLINE_BOOKING_CUSTOMER_FIRST_NAME = "first_name";
    public static final String SYNC_ONLINE_BOOKING_CUSTOMER_LAST_NAME = "last_name";
    public static final String SYNC_ONLINE_BOOKING_TYPE = "booking_type";
    public static final String SYNC_ONLINE_BOOKING_START_DATE = "start_date";
    public static final String SYNC_ONLINE_BOOKING_END_DATE = "end_date";
    public static final String SYNC_ONLINE_BOOKING_BOOKING_DATE = "created";
    public static final String SYNC_ONLINE_BOOKING_NUM_SLOTS = "num_slots";
    public static final String SYNC_ONLINE_BOOKING_STATUS = "status";
    public static final String SYNC_ONLINE_BOOKING_SPACE_OWNER_ID = "space_user_id";


    public static final String SYNC_ONLINE_ENTRY_EXIT_UPDATE = "EntryExitTime";
    public static final String SYNC_ONLINE_ENTRY_EXIT_BOOKING_ID = "parent_booking_id";
    public static final String SYNC_ONLINE_ENTRY_EXIT_PARKING_ID = "parking_id";
    public static final String SYNC_ONLINE_ENTRY_EXIT_SPACE_ID = "space_id";
    public static final String SYNC_ONLINE_ENTRY_EXIT_OPERATOR_ID = "operator_id";
    public static final String SYNC_ONLINE_ENTRY_EXIT_STATUS = "status";
    public static final String SYNC_ONLINE_ENTRY_EXIT_ENTRY_TIME = "entry_time";
    public static final String SYNC_ONLINE_ENTRY_EXIT_EXIT_TIME = "exit_time";
    public static final String SYNC_ONLINE_ENTRY_EXIT_VEHICLE_REG = "vehicle_reg";
    public static final String SYNC_ONLINE_ENTRY_EXIT_VEHICLE_TYPE = "vehicle_type";
    public static final String SYNC_ONLINE_ENTRY_EXIT_IS_PARK_AND_RIDE = "is_park_and_ride";
    public static final String SYNC_ONLINE_ENTRY_EXIT_PARKING_FEES = "parking_fees";
    public static final String SYNC_ONLINE_ENTRY_EXIT_BOOKING_TYPE = "booking_type";
    public static final String SYNC_ONLINE_ENTRY_EXIT_IS_UPDATED = "is_updated";


    public static final String SYNC_ONLINE_OP_APP_BOOKING_UPDATE = "opAppBooking";
    public static final String SYNC_ONLINE_OP_APP_LOST_PASS_UPDATE = "LostPass";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_ID = "booking_id";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_SERIAL_NUM = "serial_num";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_ENCODE_PASS_CODE = "encode_pass_code";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_CUSTOMER_FIRST_NAME = "first_name";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_CUSTOMER_LAST_NAME = "last_name";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_NAME = "name";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_MOBILE = "mobile";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_EMAIL = "email";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_TYPE = "booking_type";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_START_DATE = "start_date";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_END_DATE = "end_date";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_BOOKING_DATE = "created";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_NUM_SLOTS = "num_slots";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_STATUS = "status";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_SPACE_OWNER_ID = "space_user_id";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_VEHICLE_REG = "vehicle_reg";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_VEHICLE_TYPE = "vehicle_type";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_PARKING_FEES = "booking_price";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_DISCOUNT = "discount";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_IS_RENEWAL = "is_renewal";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_PARENT_BOOKING_ID = "parent_booking_id";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_PAYMENT_MODE = "payment_mode";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_INVOICE_ID = "invoice_id";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_PAYMENT_ID = "payment_id";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_OPERATOR_ID = "operator_id";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_IS_STAFF_PASS = "is_pass_for_staff";

    public static final String SYNC_ONLINE_OP_APP_GENERATED_BOOKING_ID = "app_generated_booking_id";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_NEW_ISSUE_DATE = "new_pass_issue_date";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_OLD_ISSUE_DATE = "old_pass_issue_date";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_OLD_ENCODE_PASS_CODE = "old_encoded_pass_code";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_NEW_ENCODE_PASS_CODE = "new_encoded_pass_code";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_OLD_SERIAL_NUM = "old_serial_number";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_NEW_SERIAL_NUM = "new_serial_number";
    public static final String SYNC_ONLINE_OP_APP_BOOKING_LOST_PASS_FEES = "lost_pass_fees";


    public static final String SYNC_NO_EXIT_TIME = "0000-00-00 00:00:00";
    public static final char SYNC_PUSH_SUCCESSFUL = '1';
    public static final char SYNC_PUSH_STALE = '2';
    public static final char SYNC_PUSH_FAILED = '0';


    //timer name related actions
    public static final int TIMER_ACTION_PUSH = 0;
    public static final int TIMER_ACTION_END_OF_DAY = 1;
    public static final long TIMER_ACTION_PUSH_DURATION_IN_MS = 10000; //5 mins
    public static final int TIMER_ACTION_PUSH_DURATION_IN_SECONDS = 300; //5 secs

    //timer related constants
    public static final int TIMER_RET_VAL_ALREADY_RUNNING = 1;
    public static final int TIMER_RET_VAL_STARTED = 0;

    public static final int TIMER_ACTIONS_MAX = 2;
    public static final long TIMER_INVALID_TIME = -1;
    public static final int TIMER_STATUS_RUNNING = 1;
    public static final int TIMER_STATUS_NOT_RUNNING = 0;

    public static final int TIMER_INVALID_ACTION = -1;


    //simplypark booking related constants
    public static  final int BOOKING_TYPE_NA = 0;
    public static final int BOOKING_TYPE_HOURLY=1;
    public static final int BOOKING_TYPE_DAILY=2;
    public static final int BOOKING_TYPE_WEEKLY=3;
    public static final int BOOKING_TYPE_MONTHLY=4;
    public static final int BOOKING_TYPE_YEARLY=5;

    public static final int BOOKING_STATUS_WAITING = 0;
    public static final int BOOKING_STATUS_APPROVED = 1;
    public static final int BOOKING_STATUS_DISAPPROVED = 2;
    public static final int BOOKING_STATUS_CANCELLATION_REQUESTED = 3;
    public static final int BOOKING_STATUS_CANCELLED = 4;
    public static final int BOOKING_STATUS_EXPIRED = 5;
    public static final int BOOKING_STATUS_CANCELLED_PAYMENT_DEFAULT = 6;


    public static final String PARKING_ID_PARKING_FEES_PREPAID_SUFFIX = "_pp";
    public static final int SP_FAILURE_CODE_NA = 0;
    public static final int SP_INVALID_BOOKING_PASS = 1;
    public static final int SP_ALL_SLOTS_USED = 2;
    public static final int SP_VEHICLE_ALREADY_INSIDE = 3;
    public static final int SP_UNKNOWN_ERROR = 4;
    public static final int SP_PASS_NOT_ACTIVE = 5;
    public static final int SP_VEHICLE_REG_REQUIRED = 6;




    public static final String BOOKING_GENERATED_IN_APP = "app";

    public static int PRINTER_TYPE_NOT_SET = 0;
    public static int PRINTER_TYPE_ANALOGICS = 1;
    public static int PRINTER_TYPE_MATE = 2;

    //custom ticket formats
    public static final int ENTRY_TICKET_CUSTOM_FORMAT = 1;
    public static final String CUSTOM_SPACE_ADDRESS = "space_address";
    public static final String CUSTOM_SPACE_HEADING = "space_heading";
    public static final String CUSTOM_AGENCY_NAME = "agency_name";
    public static final String CUSTOM_SPACE_OWNER_MOBILE = "space_owner_mobile";
    public static final String CUSTOM_OPERATING_HOURS = "operating_hours";
    public static final String CUSTOM_PARKING_RATES = "parking_rates";
    public static final String CUSTOM_BIKE_PARKING_RATES = "bike_parking_rates";
    public static final String CUSTOM_NIGHT_CHARGES = "night_charge";
    public static final String CUSTOM_BIKE_NIGHT_CHARGES = "bike_night_charge";
    public static final String CUSTOM_LOST_TICKET = "lost_ticket";
    public static final String CUSTOM_URL = "url";
    public static final String CUSTOM_DESC = "desc";
    public static final String CUSTOM_AD_TEXT = "ad_text";
    public static final String CUSTOM_SPARK_AD_TEXT = "simplypark_ad_text";
    public static final String CUSTOM_VEHICLE_TEXT_SIZE = "is_vehicle_text_bold";

    public static final int CUSTOM_SPACE_ADDRESS_IDX = 0;
    public static final int CUSTOM_SPACE_HEADING_IDX = 1;
    public static final int CUSTOM_AGENCY_NAME_IDX= 2;
    public static final int CUSTOM_SPACE_OWNER_MOBILE_IDX= 3;
    public static final int CUSTOM_OPERATING_HOURS_IDX= 4;
    public static final int CUSTOM_PARKING_RATES_IDX= 5;
    public static final int CUSTOM_BIKE_PARKING_RATES_IDX = 6;
    public static final int CUSTOM_NIGHT_CHARGES_IDX= 7;
    public static final int CUSTOM_BIKE_NIGHT_CHARGES_IDX = 8;
    public static final int CUSTOM_LOST_TICKET_IDX= 9;
    public static final int CUSTOM_URL_IDX= 10;
    public static final int CUSTOM_DESC_IDX= 11;
    public static final int CUSTOM_AD_TEXT_IDX = 12;
    public static final int CUSTOM_SPARK_AD_IDX = 13;
    public static final int CUSTOM_VEHICLE_SIZE_IDX = 14;

    public static final int CUSTOM_FORMAT_MAX_IDX = 15;

    public static final String APP_GENERATED_BOOKING_ID_PREFIX = "app";
    public static final String REUSABLE_TOKEN_PREFIX = "OL";

    public static final int OFFLINE_TRANSACTIONS_PUSH_LIMIT = 64;

    public static final String RAZORPAY_PRODUCT_NAME = "Monthly Pass";
    public static final String RAZORPAY_PRODUCT_DESC = "App booking of Monthly Pass";

    public static final int PAYMENT_MODE_CASH = 1;
    public static final int PAYMENT_MODE_CARD = 2;
    public static final int PAYMENT_MODE_ONLINE = 3;

    public static final int PENALTLY_CHARGE_CONDITION_NIGHT_STAY = 4;

    public static final int LATEST_BOOKING_ISSUE_DATE = 0;
    public static final int LATEST_BOOKING_VALIDITY_DATE = 1;

    public static final int MAX_OPERATORS = 15;
    public static final int INVALID_OPERATOR_ID = -1;

    public static final int APP_TYPE_ENTRY_EXIT_ENABLED = 0;
    public static final int APP_TYPE_ONLY_ENTRY_ENABLED = 1;
    public static final int APP_TYPE_ONLY_EXIT_ENABLED = 2;

    public static final int RESET_START_SYNC_ID = 1;

    public static final int NDMC_HACK_CAR_SPACE_ID = 277;
    public static final int NDMC_HACK_BIKE_SPACE_ID = 279;

    public static final int TOKEN_STATUS_FREE = 0;
    public static final int TOKEN_STATUS_BUSY = 1;

    public static final int TOKEN_VALID = 0;
    public static final int TOKEN_INVALID = 1;
    public static final int TOKEN_BUSY = 2;

    public static final String VEHICLE_SIZE_BIG = "AVAILABLE";

}
