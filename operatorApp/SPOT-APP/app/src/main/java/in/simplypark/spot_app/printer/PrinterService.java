package in.simplypark.spot_app.printer;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.util.Log;

import java.io.IOException;

import in.simplypark.spot_app.utils.Cache;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.utils.TransactionSummary;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class PrinterService extends IntentService {


    public PrinterService() {
        super("PrinterService");
    }

    /**
     * Starts this service to perform action Print EntryTicket with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionConnect(Context context, String macAddress) {
        Intent intent = new Intent(context, PrinterService.class);
        intent.setAction(Constants.ACTION_CONNECT_TO_PRINTER);
        intent.putExtra(Constants.BUNDLE_PARAM_MAC_ADDRESS, macAddress);

        context.startService(intent);
    }

    /**
     * Starts this service to perform action Print EntryTicket with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionPrintEntryTicket(Context context, String dateTime,
                                                   String parkingId,
                                                   Boolean isParkAndRide, String regNumber,
                                                   int vehicleType) {
        Intent intent = new Intent(context, PrinterService.class);
        intent.setAction(Constants.ACTION_PRINT_ENTRY_TICKET);
        intent.putExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, dateTime);
        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_ID, parkingId);
        intent.putExtra(Constants.BUNDLE_PARAM_IS_PARK_AND_RIDE, isParkAndRide);
        intent.putExtra(Constants.BUNDLE_PARAM_CAR_REG, regNumber);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,vehicleType);

        context.startService(intent);
    }

    /**
     * Starts this service to perform action Exit Ticket with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionPrintExitTicket(Context context, String entryDateTime,
                                                  String exitDateTime, String parkingFees,
                                                  String nightChargesString,String regNumber,
                                                  String durationDescription,
                                                  int vehicleType) {
        Intent intent = new Intent(context, PrinterService.class);
        intent.setAction(Constants.ACTION_PRINT_EXIT_TICKET);
        intent.putExtra(Constants.BUNDLE_PARAM_ENTRY_TIME, entryDateTime);
        intent.putExtra(Constants.BUNDLE_PARAM_EXIT_TIME, exitDateTime);
        intent.putExtra(Constants.BUNDLE_PARAM_CAR_REG,regNumber);
        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_FEES,parkingFees);
        intent.putExtra(Constants.BUNDLE_PARAM_NIGHT_CHARGES_DESC,nightChargesString);
        intent.putExtra(Constants.BUNDLE_PARAM_DURATION_DESCRIPTION,durationDescription);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,vehicleType);

        context.startService(intent);
    }




    /**
     * Starts this service to perform action Exit Ticket with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionPrintReport(Context context,String reportPeriod,
                                              TransactionSummary summary) {
        Intent intent = new Intent(context, PrinterService.class);
        intent.setAction(Constants.ACTION_PRINT_REPORT);

        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_PERIOD, reportPeriod);
        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_PERIOD_EARNINGS, summary.getEarnings());
        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_PERIOD_CAR_EARNINGS,summary.getCarEarnings());
        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_PERIOD_BIKE_EARNINGS, summary.getBikeEarnings());
        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_PERIOD_PASS_EARNINGS, summary.getPassEarnings());

        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_PERIOD_NUM_CARS_PARKED,
                summary.getNumCarsParked());
        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_PERIOD_NUM_MONTHLY_PASS_CARS_PARKED,
                summary.getNumPassCarsParked());
        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_PERIOD_NUM_BIKES_PARKED,
                summary.getNumBikesParked());
        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_PERIOD_NUM_MONTHLY_PASS_BIKES_PARKED,
                summary.getNumPassBikesParked());

        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_PERIOD_MISSING_CAR_EXITS,
                summary.getIncompleteCarExits());
        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_PERIOD_MISSING_MONTHLY_PASS_CAR_EXITS,
                summary.getIncompletePassCarExits());

        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_PERIOD_MISSING_BIKE_EXITS,
                summary.getIncompleteBikeExits());
        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_PERIOD_MISSING_MONTHLY_PASS_BIKE_EXITS,
                summary.getIncompletePassBikeExits());


        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_PERIOD_CARS_INSIDE,
                summary.getNumCarsInside());
        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_PERIOD_MONTHLY_PASS_CARS_INSIDE,
                summary.getNumPassCarsInside());

        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_PERIOD_BIKES_INSIDE,
                summary.getNumBikesInside());
        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_PERIOD_MONTHLY_PASS_BIKES_INSIDE,
                summary.getNumPassBikesInside());

        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_PERIOD_NUM_PASSES_ISSUED,
                summary.getNumPassesIssued());

        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_PERIOD_NUM_CAR_PASSES_ISSUED,
                summary.getNumCarPassesIssued());

        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_PERIOD_NUM_BIKE_PASSES_ISSUED,
                summary.getNumBikesPassesIssued());

        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_GATEWISE_EARNING_PRINT_STRING,
                summary.getPerOperatorEarningPrintString());

        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_GATEWISE_PASS_EARNING_PRINT_STRING,
                summary.getPerOperatorPassEarningPrintString());




        context.startService(intent);
    }

    /**
     * Starts this service to perform action Monthly pass with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionPrintGeneratedMonthlyPass(Context context,
                                                            String vehicleReg,int vehicleType,
                                                            String issuedTo, String email, String tel,
                                                            String validFrom,String validTill,
                                                            String transId,int perMonthFees){
        Intent intent = new Intent(context, PrinterService.class);
        intent.setAction(Constants.ACTION_PRINT_GENERATED_MONTHLY_PASS);

        intent.putExtra(Constants.BUNDLE_PARAM_CAR_REG, vehicleReg);
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, vehicleType);
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_ISSUED_TO,issuedTo);
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_EMAIL,email);
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_TEL,tel);
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_VALID_FROM, validFrom);
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_VALID_TILL,validTill);
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_TRANS_ID,transId);
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_PER_MONTH_FEES,perMonthFees);


        context.startService(intent);
    }



    /**
     * Starts this service to perform action Monthly pass Receipt with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionPrintMonthlyPassReceipt(Context context,int amountReceived,
                                                          String transId,String nextPaymentDate){

        Intent intent = new Intent(context, PrinterService.class);
        intent.setAction(Constants.ACTION_PRINT_MONTHLY_PASS_RECEIPT);

        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_RECEIPT_AMOUNT_RECEIVD, amountReceived);
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_RECEIPT_TRANS_ID, transId);
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_RECEIPT_NEXT_PAYMENT_DATE,
                        nextPaymentDate);


        context.startService(intent);
    }




    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (Constants.ACTION_PRINT_ENTRY_TICKET.equals(action)) {
                final String dateTime = intent.getStringExtra(Constants.BUNDLE_PARAM_ENTRY_TIME);
                final String parkingId = intent.getStringExtra(Constants.BUNDLE_PARAM_PARKING_ID);
                final Boolean isParkAndRide = intent.getBooleanExtra(
                        Constants.BUNDLE_PARAM_IS_PARK_AND_RIDE,
                        false);
                final int vehicleType = intent.getIntExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                                            Constants.VEHICLE_TYPE_CAR);
                final String regNumber = intent.getStringExtra(Constants.BUNDLE_PARAM_CAR_REG);

                handleActionPrintEntryTicket(dateTime,parkingId,isParkAndRide,regNumber,vehicleType);
            } else if (Constants.ACTION_PRINT_EXIT_TICKET.equals(action)) {
                final String entryDateTime = intent.getStringExtra(Constants.BUNDLE_PARAM_ENTRY_TIME);
                final String exitDateTime = intent.getStringExtra(Constants.BUNDLE_PARAM_EXIT_TIME);
                final String parkingFees = intent.getStringExtra(Constants.BUNDLE_PARAM_PARKING_FEES);
                final String nightCharges = intent.getStringExtra(Constants.BUNDLE_PARAM_NIGHT_CHARGES_DESC);
                final String regNumber = intent.getStringExtra(Constants.BUNDLE_PARAM_CAR_REG);
                final String duration  = intent.getStringExtra(
                                                       Constants.BUNDLE_PARAM_DURATION_DESCRIPTION);
                final int vehicleType = intent.getIntExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                                           Constants.VEHICLE_TYPE_CAR);

                        handleActionPrintExitTicket(entryDateTime, exitDateTime, parkingFees,
                                                    nightCharges,
                                                    regNumber,duration,vehicleType);

            } else if (Constants.ACTION_CONNECT_TO_PRINTER.equals(action)) {
                final String macAddress = intent.getStringExtra(Constants.BUNDLE_PARAM_MAC_ADDRESS);
                handleActionConnectToPrinter(macAddress);
            } else if (Constants.ACTION_PRINT_REPORT.equals(action)) {

                final String reportPeriod = intent.getStringExtra(
                        Constants.BUNDLE_PARAM_REPORT_PERIOD);
                TransactionSummary summary = new TransactionSummary();

                summary.setCarEarnings(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_REPORT_PERIOD_CAR_EARNINGS, 0));
                summary.setBikeEarnings(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_REPORT_PERIOD_BIKE_EARNINGS, 0));
                summary.setPassEarnings(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_REPORT_PERIOD_PASS_EARNINGS, 0));

                summary.setNumCarsParked(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_REPORT_PERIOD_NUM_CARS_PARKED, 0));
                summary.setNumPassCarsParked(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_REPORT_PERIOD_NUM_MONTHLY_PASS_CARS_PARKED, 0));
                summary.setNumBikesParked(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_REPORT_PERIOD_NUM_BIKES_PARKED, 0));
                summary.setNumPassBikesParked(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_REPORT_PERIOD_NUM_MONTHLY_PASS_BIKES_PARKED, 0));

                summary.setIncompleteCarExits(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_REPORT_PERIOD_MISSING_CAR_EXITS, 0));
                summary.setIncompletePassCarExits(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_REPORT_PERIOD_MISSING_MONTHLY_PASS_CAR_EXITS, 0));

                summary.setIncompleteBikeExits(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_REPORT_PERIOD_MISSING_BIKE_EXITS, 0));
                summary.setIncompletePassBikeExits(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_REPORT_PERIOD_MISSING_MONTHLY_PASS_BIKE_EXITS, 0));


                summary.setNumCarsInside(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_REPORT_PERIOD_CARS_INSIDE, 0));
                summary.setNumPassCarsInside(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_REPORT_PERIOD_MONTHLY_PASS_CARS_INSIDE, 0));

                summary.setNumBikesInside(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_REPORT_PERIOD_BIKES_INSIDE, 0));
                summary.setNumPassBikesInside(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_REPORT_PERIOD_MONTHLY_PASS_BIKES_INSIDE, 0));

                summary.setNumPassesIssued(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_REPORT_PERIOD_NUM_PASSES_ISSUED, 0));

                summary.setNumCarsPassesIssued(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_REPORT_PERIOD_NUM_CAR_PASSES_ISSUED, 0));

                summary.setNumBikesPassesIssued(intent.getIntExtra(
                        Constants.BUNDLE_PARAM_REPORT_PERIOD_NUM_BIKE_PASSES_ISSUED, 0));

                summary.setPerOperatorEarningPrintString(intent.getStringExtra(
                        Constants.BUNDLE_PARAM_REPORT_GATEWISE_EARNING_PRINT_STRING));

                summary.setPerOperatorPassEarningPrintString(intent.getStringExtra(
                        Constants.BUNDLE_PARAM_REPORT_GATEWISE_PASS_EARNING_PRINT_STRING));


                handleActionPrintReport(reportPeriod, summary);



            }else if(Constants.ACTION_PRINT_GENERATED_MONTHLY_PASS.equals(action)){
                final String vehicleReg = intent.getStringExtra(Constants.BUNDLE_PARAM_CAR_REG);
                final int vehicleType = intent.getIntExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                        Constants.VEHICLE_TYPE_CAR);
                final String issuedTo = intent.getStringExtra(
                        Constants.BUNDLE_PARAM_MONTHLY_PASS_ISSUED_TO);

                final String email = intent.getStringExtra(
                        Constants.BUNDLE_PARAM_MONTHLY_PASS_EMAIL);
                final String tel = intent.getStringExtra(
                        Constants.BUNDLE_PARAM_MONTHLY_PASS_TEL);
                final String validFrom = intent.getStringExtra(
                        Constants.BUNDLE_PARAM_MONTHLY_PASS_VALID_FROM);
                final String validTill = intent.getStringExtra(
                        Constants.BUNDLE_PARAM_MONTHLY_PASS_VALID_TILL);
                final String transId = intent.getStringExtra(
                        Constants.BUNDLE_PARAM_MONTHLY_PASS_TRANS_ID);
                final int perMonthFees = intent.getIntExtra(
                        Constants.BUNDLE_PARAM_MONTHLY_PASS_PER_MONTH_FEES, 0);


                handleActionPrintGeneratedMonthlyPass(vehicleReg,vehicleType,issuedTo,email,tel,
                                                      validFrom,validTill,transId,perMonthFees);
            }else if (Constants.ACTION_PRINT_MONTHLY_PASS_RECEIPT.equals(action)){
                final int amountReceived = intent.getIntExtra(
                        Constants.BUNDLE_PARAM_MONTHLY_PASS_RECEIPT_AMOUNT_RECEIVD,0);

                final String transId = intent.getStringExtra(
                        Constants.BUNDLE_PARAM_MONTHLY_PASS_RECEIPT_TRANS_ID);

                final String nextPaymentDue = intent.getStringExtra(
                        Constants.BUNDLE_PARAM_MONTHLY_PASS_RECEIPT_NEXT_PAYMENT_DATE);

                handleActionPrintMonthlyPassReceipt(amountReceived,transId,nextPaymentDue);
            }

        }
    }

    /**
     * Handle action handleActionPrintEntryTicket in the provided background thread with the provided
     * parameters.
     */
    private void handleActionPrintEntryTicket(String dateTime,String parkingId,
                                              Boolean isParkAndRide,String regNumber,
                                              int vehicleType) {
        try {
            if((Boolean) Cache.getFromCache(getApplicationContext(),
                     Constants.CACHE_PREFIX_IS_PRINTER_CONFIGURED,Constants.TYPE_BOOL)) {
              //if(true){
                Printer.getPrinter().printEntryTicket(dateTime, parkingId, isParkAndRide, regNumber,
                        vehicleType);
            }

        } catch (IOException ioe) {
            Log.d("HandleActionPrintEntry", "exception" + ioe);

        }
    }

    /**
     * Handle action handleActionPrintExitTicket in the provided background thread with the provided
     * parameters.
     */
    private void handleActionPrintExitTicket(String entryDateTime,String exitDateTime,
                                             String parkingFees, String nightChargesDesc,
                                             String regNumber,
                                             String duration,int vehicleType) {
        try {
            if((Boolean)Cache.getFromCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_IS_PRINTER_CONFIGURED,Constants.TYPE_BOOL)) {
            //if(true){
                Printer.getPrinter().printExitTicket(entryDateTime, exitDateTime,
                        parkingFees, nightChargesDesc,regNumber, duration, vehicleType);
            }

        } catch (IOException ioe) {
            Log.d("HandleActionPrintEntry", "exception" + ioe);

        }
    }




    /**
     * Handle action handleActionPrintReport in the provided background thread with the provided
     * parameters.
     */
    private void handleActionPrintReport(String reportPeriod,
                                         TransactionSummary summary){
        try {
            if((Boolean)Cache.getFromCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_IS_PRINTER_CONFIGURED,Constants.TYPE_BOOL)) {
                Printer.getPrinter().printReport(reportPeriod, summary);

            }

        } catch (Exception ioe) {
            Log.d("HandleActionPrintReport", "exception" + ioe);

        }



    }

    /**
     * Handle action handleActionPrintGeneratedMonthlyPass in the provided
     * background thread with the provided
     * parameters.
     */
    private void handleActionPrintGeneratedMonthlyPass(String vehicleReg,int vehicleType,
                                                       String issuedTo,String email,String tel,
                                                       String validFrom,String validTill,
                                                       String transId,int perMonthFees){

        try {
            if((Boolean)Cache.getFromCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_IS_PRINTER_CONFIGURED,Constants.TYPE_BOOL)) {
              //if(true){
                Printer.getPrinter().printMonthlyPass(vehicleReg, vehicleType, issuedTo, email, tel,
                        validFrom,validTill, transId, perMonthFees);

            }

        } catch (Exception ioe) {
            Log.d("ActionPrintMonthlyPass", "exception" + ioe);

        }

    }

    /**
     * Handle action handleActionPrintMonthlyPassReceipt in the provided
     * background thread with the provided
     * parameters.
     */

    private void handleActionPrintMonthlyPassReceipt(int amountReceived,String transId,
                                                     String nextPaymentDue){

        try {
            if((Boolean)Cache.getFromCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_IS_PRINTER_CONFIGURED,Constants.TYPE_BOOL)) {
                Printer.getPrinter().printMonthlyPassReceipt(amountReceived,transId,nextPaymentDue);
            }

        } catch (Exception ioe) {
            Log.d("ActionPrintMonthlyPass", "exception" + ioe);

        }

    }


    /**
     * Handle action Connect to Printer.
     */
    private void handleActionConnectToPrinter(String macAddress){
        Printer mPrinterHandle = Printer.getPrinter();

        mPrinterHandle.connect(macAddress);
    }

}