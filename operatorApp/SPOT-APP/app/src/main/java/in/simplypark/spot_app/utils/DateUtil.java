package in.simplypark.spot_app.utils;
import hirondelle.date4j.DateTime;
import in.simplypark.spot_app.config.Constants;

/**
 * Created by aditya on 11/05/16.
 */
public class DateUtil {

    public class Interval{
        public int status;
        public int seconds; //currently we do not bother about seconds
        public int minutes;
        public int hours;
        public int days;
        public int months;
        public int years;
        public int nights; //number of nights (00:00 to 05:00). This is to help calculate the
                              //nightly charge



        Interval(){
            status = Constants.DATEDIFF_OK;
            seconds = 0;
            minutes = 0;
            hours = 0;
            days = 0;
            months = 0;
            years = 0;
            nights = 0;

        }


        public int getIntervalStatus(){return status;}
        public int getHours(){return hours;}
        public int getMinutes(){return minutes;}
        public int getDays(){ return days;}
        public int getNights() { return nights;}


        public String describe(){
            String describe = "";
            String years = this.years + " year";
            String months = this.months + " month";
            String days = this.days + " day";
            String hours = this.hours + " hour";
            String minutes = this.hours + " minute";

           if(this.days > 0){
               describe += this.days + " day(s)";
           }

           if(this.hours > 0){
               if(this.days > 0){
                   describe += ", " + this.hours + " hr(s)";
               }else{
                   describe += this.hours + " hr(s)";
               }
           }

           if(this.minutes > 0){
               if(this.hours > 0){
                   describe += ", " + this.minutes + " minute(s)";
               }else{
                   describe += this.minutes + " minute(s)";
               }
           }
            if(describe.isEmpty()){
                describe = "1 minute";
            }

            return describe;
        }
    }

    //this is not general purpose datehandling util. it ASSUMES both dates
    //are of the same timezone. This is written just as a helper for the app
    //and handle only APP specific scenarios

    public Interval getDateDifference(DateTime receivedFrom , DateTime receivedTo){
        Interval difference = new Interval();

        String fromStr = receivedFrom.format("YYYY-MM-DD hh:mm");
        fromStr += ":00.0";

        String toStr = receivedTo.format("YYYY-MM-DD hh:mm");
        toStr += ":00.0";


        DateTime from = new DateTime(fromStr);
        DateTime to   = new DateTime(toStr);

        if(from.gt(to)){
            difference.status = Constants.DATEDIFF_ERROR_FROM_TIME_GREATER_THAN_TO_TIME;
            return difference;
        }


        long numSeconds =   from.numSecondsFrom(to);

         if(numSeconds >= Constants.NUM_SECONDS_IN_A_DAY) {
            difference.days = (int)(numSeconds/Constants.NUM_SECONDS_IN_A_DAY);
            int remainingSeconds = (int)(numSeconds % Constants.NUM_SECONDS_IN_A_DAY);
            difference.hours = remainingSeconds/Constants.NUM_SECONDS_IN_A_HOUR;
            difference.minutes = (remainingSeconds%Constants.NUM_SECONDS_IN_A_HOUR)/
                                             Constants.NUM_SECONDS_IN_A_MINUTE;


            difference.status = Constants.DATEDIFF_DAILY_BOOKING;
        }else {
            difference.status = Constants.DATEDIFF_HOURLY_BOOKING;
            difference.hours = (int) (numSeconds / Constants.NUM_SECONDS_IN_A_HOUR);
            int remainingSeconds = (int)(numSeconds % Constants.NUM_SECONDS_IN_A_HOUR);

            difference.minutes = (int) remainingSeconds /Constants.NUM_SECONDS_IN_A_MINUTE;

            /*if(from.getHour() >= Constants.NIGHT_START_TIME &&
                    from.getHour() <= Constants.NIGHT_END_TIME){
                difference.nights = 1;
            }else if(to.getHour() >= Constants.NIGHT_START_TIME &&
                     to.getHour() <= Constants.NIGHT_END_TIME){
                difference.nights = 1;
            }*/

        }

        /*int fromDay = from.getDay();
        int toDay   = to.getDay();
        int fromMonth = from.getMonth();
        int toMonth   = to.getMonth();
        int fromYear  = from.getYear();
        int toYear    = to.getYear();


        if(fromMonth != toMonth){
            int nights;
            DateTime iteratorDate = from;
            for(int i = fromMonth; i < toMonth ; i++){
                nights = iteratorDate.getNumDaysInMonth() - fromDay;
                fromDay = 0;
                iteratorDate = from.
            }
            difference.nights = toDay + from.getNumDaysInMonth() - fromDay;
        }else {
            difference.nights = toDay - fromDay;
        }*/
        difference.nights = from.numDaysFrom(to);

        if(from.getHour() >= Constants.NIGHT_START_TIME &&
                from.getHour() < Constants.NIGHT_END_TIME){
            difference.nights++;
        }


        return difference;
    }

    //calculate date difference in terms of months & days

    public static String addMonthToDate(DateTime date){

        DateTime returnedDate = date.plus(0,1,0,0,0,0,0,DateTime.DayOverflow.LastDay);

        return returnedDate.format("YYYY-MM-DD hh:mm");

    }


}
