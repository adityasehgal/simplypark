package in.simplypark.spot_app.db_tables;

import com.orm.SugarRecord;

/**
 * Created by root on 14/11/16.
 */
public class PendingExits extends SugarRecord {
    String mBookingId;
    String mVehicleReg;
    long   mExitDateTime;

    public PendingExits(){
        mBookingId = "";
        mVehicleReg = "";
        mExitDateTime = 0;
    }

    public void save(String bookingId, String vehicleReg, long exitDateTime){
        mBookingId = bookingId;
        mVehicleReg = vehicleReg;
        mExitDateTime = exitDateTime;
        save();
    }

    public long getExitDateTime(){ return mExitDateTime;}
    public String getVehicleReg() { return mVehicleReg;}
    public String getBookingId(){ return mBookingId;}

}
