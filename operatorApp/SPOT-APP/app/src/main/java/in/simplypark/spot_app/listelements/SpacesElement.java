package in.simplypark.spot_app.listelements;

/**
 * Created by root on 21/11/16.
 */
public class SpacesElement {
    private String mSpaceName;
    private String mSpaceAddress;
    private int mNumberOfSlots;
    private int    mSpaceId;

    public SpacesElement(int spaceId,String spaceName, String spaceAddress,int numberOfSlots){
        mSpaceName = spaceName;
        mSpaceAddress = spaceAddress;
        mSpaceId = spaceId;
        mNumberOfSlots = numberOfSlots;
    }


    public SpacesElement(){
        mSpaceAddress = "";
        mSpaceName = "";
        mSpaceId = 0;

        mNumberOfSlots =0;
    }


    public void setSpaceId(int id){
        mSpaceId = id;
    }

    public void setSpaceName(String spaceName){
        mSpaceName = spaceName;
    }

    public void setSpaceAddress(String spaceAddress){
        mSpaceAddress = spaceAddress;
    }

    public String getSpaceName(){
        return mSpaceName;
    }

    public String getSpaceAddress(){
        return mSpaceAddress;
    }

    public int getSpaceId(){
        return mSpaceId;
    }

    public int getmNumberOfSlots() {
        return mNumberOfSlots;
    }

    public void setmNumberOfSlots(int mNumberOfSlots) {
        this.mNumberOfSlots = mNumberOfSlots;
    }

}
