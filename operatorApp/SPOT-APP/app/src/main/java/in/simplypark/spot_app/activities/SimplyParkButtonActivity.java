package in.simplypark.spot_app.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import hirondelle.date4j.DateTime;
import in.simplypark.spot_app.R;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.db_tables.SimplyParkBookingTbl;
import in.simplypark.spot_app.db_tables.SimplyParkTransactionTbl;
import in.simplypark.spot_app.db_tables.TransactionTbl;
import in.simplypark.spot_app.printer.PrinterService;
import in.simplypark.spot_app.services.ServerUpdationIntentService;
import in.simplypark.spot_app.utils.Cache;
import in.simplypark.spot_app.utils.MiscUtils;
import in.simplypark.spot_app.utils.ParkingId;

public class SimplyParkButtonActivity extends Activity {


    private ImageView mIconView;
    private TextView mResultTextView;
    private Button mCancelButtonView;
    private Button mOkButtonView;
    private View mVisitorDetailsView;
    private boolean mIsEntryTicketPrinted = false;
    boolean isValidBooking = false;


    private View mSpEntryProgressView;
    private View mSpView;
    private int mResCode;
    SimplyParkBookingTbl booking = null;

    String bookingId;
    String vehicleReg;
    int    vehicleType;


    private logSimplyParkEntryTask mLogSimplyParkEntryTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        if (android.os.Build.VERSION.SDK_INT>=11){
            setFinishOnTouchOutside(false);
        }
        setContentView(R.layout.activity_simply_park_button);

        mIconView = (ImageView)findViewById(R.id.sp_result_icon_id);
        mResultTextView = (TextView)findViewById(R.id.sp_result_id);
        mCancelButtonView = (Button)findViewById(R.id.sp_cancel);
        mOkButtonView     = (Button)findViewById(R.id.sp_ok);
        mVisitorDetailsView = (View) findViewById(R.id.VisitorDetailsView_entry);
        Intent intent = getIntent();

        bookingId      = intent.getStringExtra(Constants.SIMPLYPARK_BOOKING_SCANNING_RESULT);
        vehicleReg  = intent.getStringExtra(Constants.BUNDLE_PARAM_CAR_REG);
        vehicleType = intent.getIntExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                                          Constants.VEHICLE_TYPE_CAR);
        mSpView = findViewById(R.id.sp_view_id);
        mSpEntryProgressView = findViewById(R.id.sp_entry_progress_id);

        //this tasks writes to db in background, and then starts
        //intent services for receipt printing and server updation
        new veryfySimplyParkEntryTask(bookingId,vehicleReg,vehicleType).execute();
        showProgress(true);


    }

    public void handleOkButton(View view){
            mLogSimplyParkEntryTask = new logSimplyParkEntryTask(bookingId,vehicleReg,vehicleType);
            mLogSimplyParkEntryTask.execute((Void) null);
            showProgress(true);
    }


    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mSpView.setVisibility(show ? View.GONE : View.VISIBLE);
            mSpView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mSpView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mSpEntryProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mSpEntryProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mSpEntryProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mSpEntryProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mSpView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    public void handleCancelButton(View view){
        Intent intent = SimplyParkButtonActivity.this.getIntent();
        SimplyParkButtonActivity.this.setResult(mResCode,intent);
        finish();

    }

    public class veryfySimplyParkEntryTask extends AsyncTask<Void, Void, Boolean> {
        String mBookingId;
        String mVehicleReg;
        String mFailureCause;
        int            mVehicleType;
        int            mFailureCode;
        int            mBookingType;


        veryfySimplyParkEntryTask(String bookingId, String vehicleReg, int vehicleType) {
            mFailureCause = new String();
            mBookingId = bookingId;
            mVehicleReg  = vehicleReg;
            mVehicleType = vehicleType;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            String queryParams[] = {mBookingId,mBookingId};
            List<SimplyParkBookingTbl> bookingList = SimplyParkBookingTbl.find(
                    SimplyParkBookingTbl.class, "m_booking_id = ? OR m_encoded_pass_code = ?",
                    queryParams);

            //List<SimplyParkBookingTbl> list=  SimplyParkBookingTbl.listAll(SimplyParkBookingTbl.class);

            if(bookingList.isEmpty()){
                mFailureCause = "Invalid Booking Pass. Entry NOT allowed.";
                mFailureCode = Constants.SP_INVALID_BOOKING_PASS;
                return false;
            }else {
                {
                    booking = bookingList.get(0);

                    int numBookedSlots = booking.getNumBookedSlots();
                    int numSlotsInUse  = booking.getSlotsInUse();

                    if(numSlotsInUse + 1 > numBookedSlots){
                        mFailureCause = "No slots available.";
                        mFailureCode = Constants.SP_ALL_SLOTS_USED;
                        return false;
                    }else if(numBookedSlots > 1 && mVehicleReg.isEmpty()){
                        mFailureCause = "Vehicle Reg Number required";
                        mFailureCode = Constants.SP_VEHICLE_REG_REQUIRED;
                        return false;
                    }


                    long startDateTime = booking.getBookingStartTime();
                    long endDateTime = booking.getBookingEndTime();

                    long now = DateTime.now(Constants.TIME_ZONE).getMilliseconds(Constants.TIME_ZONE);

                    if(startDateTime <= now && endDateTime >= now){
                        isValidBooking = true;
                        return true;
                    }else if (now < startDateTime) {
                        mFailureCause = "Entry NOT allowed, Pass starts from " +
                                DateTime.forInstant(startDateTime, Constants.TIME_ZONE).
                                        format("DD-MM-YYYY");
                        mFailureCode = Constants.SP_PASS_NOT_ACTIVE;
                        return false;
                    } else if (now > endDateTime) {
                        mFailureCause = "Entry NOT allowed, Pass expired on " +
                                DateTime.forInstant(endDateTime, Constants.TIME_ZONE).
                                        format("DD-MM-YYYY");
                        mFailureCode = Constants.SP_PASS_NOT_ACTIVE;
                        return false;
                    }
                }
            }
            return false;

        }
        @Override
        protected void onPostExecute(final Boolean success) {
            showProgress(false);
                if(success){
                    mVisitorDetailsView.setVisibility(View.VISIBLE);
                    TextView visitorName = (TextView) findViewById(R.id.VisitorName_entry);
                    TextView regNoId = (TextView) findViewById(R.id.RegNoId_entry);
                    TextView passValidity = (TextView) findViewById(R.id.passValidity_entry);
                    visitorName.setText(booking.getCustomerDesc());
                    regNoId.setText(booking.getVehicleReg());
                    String entryDateTime =  DateTime.forInstant(booking.getBookingStartTime(),
                            Constants.TIME_ZONE).format("DD-MM-YY");

                    String exitDateTime =  DateTime.forInstant(booking.getBookingEndTime(),
                            Constants.TIME_ZONE).format("DD-MM-YY");
                    String passValidityString = entryDateTime+" to "+exitDateTime;
                    passValidity.setText(passValidityString);
                }else {
                    mResultTextView.setText(mFailureCause);
                    mIconView.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                            R.drawable.warning, null));
                    mResCode = RESULT_CANCELED;
                    mCancelButtonView.setVisibility(View.VISIBLE);
                    mOkButtonView.setVisibility(View.INVISIBLE);
                    mVisitorDetailsView.setVisibility(View.GONE);
                }
        }

        }

/**
     * Represents an asynchronous saveToTransaction task used to authenticate
     * the user.
     */
    public class logSimplyParkEntryTask extends AsyncTask<Void, Void, Boolean> {

        String         mBookingId;
        String         mFailureCause;
        SimplyParkTransactionTbl mTransaction;
        String         mVehicleReg;
        int            mVehicleType;
        int            mFailureCode;
        int            mBookingType;

        logSimplyParkEntryTask(String bookingId, String vehicleReg, int vehicleType){
            mFailureCause = new String();
            mBookingId = bookingId;
            mVehicleReg  = vehicleReg;
            mVehicleType = vehicleType;
            mFailureCode = Constants.SP_FAILURE_CODE_NA;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            {


                if(!isValidBooking) {
                  return false;
                }


                if(mVehicleReg.isEmpty()){
                    mVehicleReg  = booking.getVehicleReg();
                    mVehicleType = booking.getVehicleType();
                    mIsEntryTicketPrinted = false; //entry only the basis of pass
                }

                //check within both transactions table if the vehicle is already entered
                List<TransactionTbl> transLst = TransactionTbl.find(TransactionTbl.class,
                        "m_car_reg_number = ? and m_status = ?",
                        mVehicleReg,
                        Integer.toString(Constants.PARKING_STATUS_ENTERED));
                if(!transLst.isEmpty()) {
                    mFailureCause = "Vehicle("+mVehicleReg+") is already inside. Kindly re-check" +
                            " vehicle number";
                    mFailureCode = Constants.SP_VEHICLE_ALREADY_INSIDE;
                    return false;
                }

                List<SimplyParkTransactionTbl> simplyParkTransList =
                        SimplyParkTransactionTbl.find(SimplyParkTransactionTbl.class,
                                "m_vehicle_reg = ? and m_status = ?",
                                mVehicleReg,
                                Integer.toString(Constants.PARKING_STATUS_ENTERED));

                if(!simplyParkTransList.isEmpty()){
                    mFailureCause = "Vehicle("+mVehicleReg+") is already inside. Kindly re-check" +
                            " vehicle number";
                    mFailureCode = Constants.SP_VEHICLE_ALREADY_INSIDE;
                    return false;
                }

                mTransaction = new SimplyParkTransactionTbl();


                byte[] parkingIdInBytes = ParkingId.generateParkingId((int) Cache.getFromCache(
                        getApplicationContext(),
                        Constants.CACHE_PREFIX_OPERATOR_QUERY,
                        Constants.TYPE_INT));





                String asciiParkingId;
                try {
                    asciiParkingId = new String(parkingIdInBytes, "ASCII");
                    String bookingId = booking.getBookingID();
                    if(!bookingId.startsWith(Constants.APP_GENERATED_BOOKING_ID_PREFIX)) {
                        asciiParkingId += encodedBookingId(booking.getBookingID());
                        //TODO: the parkingID should append the exitTime of the booking also
                        //to determine any extra fees pending
                    }

                }catch(java.io.UnsupportedEncodingException ie){
                    Log.d("Parking","Could not Encode the parking String Correctly");
                    mFailureCause = "Unknown Error. Please contact SimplyPark";
                    mFailureCode = Constants.SP_UNKNOWN_ERROR;
                    return false;
                }

                mTransaction.setEntryTime(DateTime.now(Constants.TIME_ZONE).
                                              getMilliseconds(Constants.TIME_ZONE));
                mTransaction.setIsParkAndRide(false);
                mTransaction.setParkingId(asciiParkingId);
                mTransaction.setVehicleReg(mVehicleReg);
                mTransaction.setVehicleType(mVehicleType);
                mTransaction.setBookingId(booking.getBookingID());
                mTransaction.setStatus(Constants.PARKING_STATUS_ENTERED);

                mTransaction.save();

                booking.incrementSlotsInUse(getApplicationContext(),mVehicleType);
                mBookingType = booking.getBookingType();

                booking.save();


            }


            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mLogSimplyParkEntryTask = null;

            int resCode = RESULT_OK;

            if (success) {
                Context context = getApplicationContext();
                MiscUtils.incrementOccupancyCount(context,mTransaction.getVehicleType());




                //print parking receipt
                long entryTime   = mTransaction.getEntryTime();
                String vehicleReg = mTransaction.getVehicleReg();
                String parkingId = mTransaction.getParkingId();
                boolean isParkAndRide = mTransaction.getIsParkAndRide();
                DateTime dateTime = DateTime.forInstant(entryTime, Constants.TIME_ZONE);
                String entryTimeStr =  dateTime.format("DD/MM/YY hh:mm");
                int    vehicleType  = mTransaction.getVehicleType();
                String bookingId = mTransaction.getBookingId();


                if(mIsEntryTicketPrinted) {
                    PrinterService.startActionPrintEntryTicket(context,
                            entryTimeStr, vehicleType + parkingId, isParkAndRide, vehicleReg,
                            vehicleType);
                }


                //update server
                ServerUpdationIntentService.startActionSimplyParkLogEntry(context,
                        bookingId, mBookingType,parkingId,
                        entryTime,
                        vehicleReg,vehicleType,
                        isParkAndRide);


                Intent intent = SimplyParkButtonActivity.this.getIntent();
                SimplyParkButtonActivity.this.setResult(resCode,intent);
                finish();

            } else {
                showProgress(false);
                mResultTextView.setText(mFailureCause);
                mIconView.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                        R.drawable.warning, null));
                mResCode = RESULT_CANCELED;
                mCancelButtonView.setVisibility(View.VISIBLE);


            }




        }

        @Override
        protected void onCancelled() {
            mLogSimplyParkEntryTask = null;

        }

        //to be used only for monthly passes
        String encodedBookingId(String bookingId) throws java.io.UnsupportedEncodingException {


            //The booking ID is of the form SP_<SpaceId>_<OwnerId>_<customer ID>
            //we will encode the customer number in the parking ID in order to re-create
            //the booking ID (in case an exit happens through a gate where entry of the vehicle
            //is not yet updated




            int underScoreIndex = bookingId.lastIndexOf("_");
            int customerNumber = Integer.parseInt(bookingId.substring(underScoreIndex+1));
            byte[] parkingIdSuffix = new byte[Constants.MAX_PARK_ID_SUFFIX_LEN];

            int itr = 0;
            if(customerNumber < Constants.MAX_PRINTABLE_CHAR){
                parkingIdSuffix[itr++] = Constants.mEncodeArray[customerNumber];

            }else{
                parkingIdSuffix = new byte[Constants.MAX_PARK_ID_SUFFIX_LEN-1];
                parkingIdSuffix[itr++] = Constants.mEncodeArray[customerNumber];

                int overFlow = customerNumber % Constants.MAX_PRINTABLE_CHAR;

                if(overFlow > Constants.MAX_PRINTABLE_CHAR){
                    return "";
                }

                parkingIdSuffix[itr++] = Constants.mEncodeArray[overFlow-1];
            }

            while(itr < Constants.MAX_PARK_ID_SUFFIX_LEN){
                parkingIdSuffix[itr++] = Constants.mEncodeArray[0];
            }

            return new String(parkingIdSuffix, "ASCII");

        }


    }
}
