package in.simplypark.spot_app.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import hirondelle.date4j.DateTime;
import in.simplypark.spot_app.R;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.db_tables.LostPassTbl;
import in.simplypark.spot_app.db_tables.NightChargeTbl;
import in.simplypark.spot_app.db_tables.PenaltyChargeTbl;
import in.simplypark.spot_app.db_tables.PricingPerVehicleTypeTbl;
import in.simplypark.spot_app.db_tables.ReusableTokensTbl;
import in.simplypark.spot_app.db_tables.SimplyParkBookingTbl;
import in.simplypark.spot_app.db_tables.SimplyParkTransactionTbl;
import in.simplypark.spot_app.db_tables.SpotDb;
import in.simplypark.spot_app.db_tables.TransactionTbl;
import in.simplypark.spot_app.services.ServerUpdationIntentService;
import in.simplypark.spot_app.utils.Cache;
import in.simplypark.spot_app.utils.CacheParams;
import in.simplypark.spot_app.utils.MiscUtils;
import in.simplypark.spot_app.utils.PendingActions;
import in.simplypark.spot_app.utils.SystemNotificationsReceiver;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.orm.SugarRecord.find;
import static in.simplypark.spot_app.config.Constants.CUSTOM_VEHICLE_SIZE_IDX;
import static in.simplypark.spot_app.config.Constants.VEHICLE_TYPE_CAR;

public class SpaceInformation extends AppCompatActivity {

    Toolbar mToolBar;
    View mProgressView;
    View mScrollView;
    TextView mSpaceNameView;
    TextView mSpaceAddressView;
    TextView mSpaceTimingsView;
    TextView mTotalSpacesView;
    TextView mCarPricingView;
    TextView mBikePricingView;
    TextView mIsParkAndRideView;
    TextView mBikeToSlotRatioView;
    TextView mIsSpaceBikeOnlyView;
    TextView mIsParkingPrepaidView;
    TextView mStaffPassDiscountCarView;
    TableRow mStaffPassDiscountCarRow;
    TextView mStaffPassDiscountBikeView;
    TableRow mStaffPassDiscountBikeRow;


    TextView mAppVersionView;
    TextView mAppServerView;
    TextView mAppBluetoothStateView;
    TextView mAppInternetStatusView;
    TextView mAppSyncStateView;
    TextView mAppSyncIdView;
    TextView mAppPendingTransactionView;
    TextView mAppPrinterMacAddressView;
    TextView mAppPendingActionsView;
    TextView mGcmIdView;
    TextView mPendingTransactionsTimerView;
    TextView mPendingTransactionsTimerLeftView;
    TableRow mTimerLeftRowView;

    TextView mPrintCommandView;
    TextView mExceptionView;
    TextView mFlagView;
    TextView mPrintCommandSizeView;

    Button mResetSyncView;


    TableLayout mErrorTableLayout;

    SpaceInformationTask mSpaceInformationTask = null;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Settings");
        setContentView(R.layout.activity_space_information);
        mToolBar = (Toolbar) findViewById(R.id.main_activity_toolbar);
        setSupportActionBar(mToolBar);
//        getSupportActionBar().setDisplayShowTitleEnabled(true);

        mProgressView = findViewById(R.id.asi_loading_progress);
        mScrollView = findViewById(R.id.asi_main_view);
        mSpaceNameView = (TextView) findViewById(R.id.asi_space_name_id);
        mSpaceAddressView = (TextView) findViewById(R.id.asi_space_address_id);
        mSpaceTimingsView = (TextView) findViewById(R.id.asi_space_timings_id);
        mTotalSpacesView = (TextView) findViewById(R.id.asi_total_spaces_id);
        mCarPricingView = (TextView) findViewById(R.id.asi_car_pricing_id);
        mBikePricingView = (TextView) findViewById(R.id.asi_bike_pricing_id);
        mIsSpaceBikeOnlyView = (TextView) findViewById(R.id.asi_is_space_bike_only_id);
        mStaffPassDiscountCarView = (TextView) findViewById(R.id.asi_staff_pass_car_discount);
        mStaffPassDiscountCarRow = (TableRow) findViewById(R.id.asi_staff_pass_discount_car_row);
        mStaffPassDiscountBikeView = (TextView) findViewById(R.id.asi_staff_pass_bike_discount);
        mStaffPassDiscountBikeRow = (TableRow) findViewById(R.id.asi_staff_pass_discount_bike_row);

        mIsSpaceBikeOnlyView = (TextView) findViewById(R.id.asi_is_space_bike_only_id);
        mIsParkingPrepaidView = (TextView) findViewById(R.id.asi_is_parking_prepaid_id);

        mIsParkAndRideView = (TextView) findViewById(R.id.asi_is_park_and_ride_id);
        mBikeToSlotRatioView = (TextView) findViewById(R.id.asi_bike_to_slot_ratio_id);

        mAppVersionView = (TextView) findViewById(R.id.asi_app_version_id);
        mAppServerView = (TextView) findViewById(R.id.asi_app_server_id);
        mAppBluetoothStateView = (TextView) findViewById(R.id.asi_bluetooth_state_id);
        mAppInternetStatusView = (TextView) findViewById(R.id.asi_internet_status_id);
        mAppSyncStateView = (TextView) findViewById(R.id.asi_sync_state_id);
        mAppSyncIdView = (TextView) findViewById(R.id.asi_sync_id);
        mAppPendingTransactionView = (TextView) findViewById(R.id.asi_pending_transactions_id);
        mAppPendingActionsView = (TextView) findViewById(R.id.asi_pending_actions_id);
        mAppPrinterMacAddressView = (TextView) findViewById(R.id.asi_printer_mac_address_id);
        mGcmIdView = (TextView)findViewById(R.id.asi_gcm_id_at_server);
        mPendingTransactionsTimerView = (TextView)findViewById(
                                                       R.id.asi_pending_transactions_timer_id);
        mPendingTransactionsTimerLeftView = (TextView) findViewById(
                                                       R.id.asi_pending_transactions_timer_left_id);
        mTimerLeftRowView = (TableRow) findViewById(R.id.asi_timer_left_row_id);


        mPrintCommandView = (TextView)findViewById(R.id.asi_print_command);
        mExceptionView = (TextView)findViewById(R.id.asi_print_exception);
        mFlagView = (TextView)findViewById(R.id.asi_last_print_flag_id);
        mPrintCommandSizeView = (TextView)findViewById(R.id.asi_last_print_size_id);



        mResetSyncView = (Button)findViewById(R.id.asi_reset_sync_id);

        //mErrorsView = findViewById(R.id.asi_errors_card);
         //mErrorTableLayout = (TableLayout)findViewById(R.id.asi_error_table);



        showProgress(true);
        mSpaceInformationTask = new SpaceInformationTask();

        mSpaceInformationTask.execute((Void) null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings_screen, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_reload:
                reloadSettings();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mScrollView.setVisibility(show ? View.GONE : View.VISIBLE);
            mScrollView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mScrollView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mScrollView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public void handleManuallyPushButton(View view){

        ServerUpdationIntentService.startActionPushUpdatesToServer(getApplicationContext());
        mAppPendingTransactionView.setText("Push in Progress");

        Snackbar.make(mScrollView, "Pushing Updates....", Snackbar.LENGTH_LONG);

    }

    public void handleRescanPrintButton(View view){
        Intent intent = new Intent(SpaceInformation.this, SetupActivity.class);
        //pass the payment due
        Bundle b = new Bundle();
        b.putBoolean(Constants.BUNDLE_PARAM_PRINTER_RESCAN_PROCESS_ONGOING,true);
        intent.putExtras(b);
        startActivity(intent);
    }

    public void handleResetSyncButton(View view) {
        Intent intent = new Intent(SpaceInformation.this, MainActivity.class);
        //pass the reset sync option
        Bundle b = new Bundle();
        b.putBoolean(Constants.BUNDLE_PARAM_IS_SYNC_RESET_REQUIRED, true);
        intent.putExtras(b);
        startActivity(intent);
    }

    public void handlePendingActionsButton(View view){
        ServerUpdationIntentService.startActionTriggerPendingActions(getApplicationContext());
        mAppPendingActionsView.setText("Pending Operations in Progress");

        Snackbar.make(mScrollView, "Pending Actions Push in progress....", Snackbar.LENGTH_LONG);


    }

    public void handleShowHideErrors(View view){

    }


    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class SpaceInformationTask extends AsyncTask<Void, Void, Boolean> {

        int mPendingTransactions;
        long mPendingActions;


        SpaceInformationTask() {
            mPendingTransactions = 0;
            mPendingActions = 0;

        }

        @Override
        protected Boolean doInBackground(Void... params) {

            List<TransactionTbl> transactionTblList = find(
                                  TransactionTbl.class, "m_is_synced_with_server = ?",
                                  Integer.toString(0));

            if(!transactionTblList.isEmpty()){
                mPendingTransactions = transactionTblList.size();
            }

            List<SimplyParkTransactionTbl> spTransactionTbList= find(
                    SimplyParkTransactionTbl.class, "m_is_synced_with_server = ?",
                    Integer.toString(0));

            if(!spTransactionTbList.isEmpty()){
                mPendingTransactions += spTransactionTbList.size();
            }

            List<SimplyParkBookingTbl> bookingList= find(
                    SimplyParkBookingTbl.class, "m_is_synced_with_server = ?",
                    Integer.toString(0));




            if(!bookingList.isEmpty()){
                mPendingTransactions += bookingList.size();
            }

            List<LostPassTbl> lostPassTbList = find(LostPassTbl.class, "m_is_synced_with_server = ?",
                    Integer.toString(0));

            if(!lostPassTbList.isEmpty()){
                mPendingTransactions += lostPassTbList.size();
            }

            mPendingActions = PendingActions.count(PendingActions.class);

            inflateErrorRows();



            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            Context context = getApplicationContext();
            mSpaceNameView.setText((String) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_SPACE_NAME_QUERY,
                    Constants.TYPE_STRING));

            mSpaceAddressView.setText((String) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_SPACE_NAME_QUERY,
                    Constants.TYPE_STRING));

            mTotalSpacesView.setText(((Integer) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_NUM_SLOTS_QUERY,
                    Constants.TYPE_INT)).toString());

            if((Boolean)Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_IS_OPEN_24_HOURS,
                    Constants.TYPE_BOOL)){
                mSpaceTimingsView.setText("24 Hours open");

            }else{
                String value = (String)Cache.getFromCache(context,
                        Constants.CACHE_PREFIX_OPENING_TIME_QUERY,
                        Constants.TYPE_STRING);

                value += " to ";

                value += (String)Cache.getFromCache(context,
                        Constants.CACHE_PREFIX_CLOSING_TIME_QUERY,
                        Constants.TYPE_STRING);

                mSpaceTimingsView.setText(value);

            }

            String carPricing = (String) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_PRINT_PRICING_QUERY+ VEHICLE_TYPE_CAR,
                    Constants.TYPE_STRING);
            if (carPricing.indexOf(Constants.TIER_PRICE_SEPERATOR) > 0) {
                int start = 0;
                int idx = 0;
                String valueToDisplay = "";
                //tier pricing. we need to post process the string
                while ((idx = carPricing.indexOf(Constants.TIER_PRICE_SEPERATOR,
                        start)) >= 0) {
                    valueToDisplay += carPricing.substring(start, idx) + "\n";
                    start = idx + 1;
                }

                mCarPricingView.setText(valueToDisplay);

            }else{
                mCarPricingView.setText(carPricing);
            }

            String bikePricing = (String) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_PRINT_PRICING_QUERY+Constants.VEHICLE_TYPE_BIKE,
                    Constants.TYPE_STRING);

            if (bikePricing.indexOf(Constants.TIER_PRICE_SEPERATOR) > 0) {
                int start = 0;
                int idx = 0;
                String valueToDisplay = "";
                //tier pricing. we need to post process the string
                while ((idx = bikePricing.indexOf(Constants.TIER_PRICE_SEPERATOR,
                        start)) >= 0) {
                    valueToDisplay += bikePricing.substring(start, idx) + "\n";
                    start = idx + 1;
                }

                mBikePricingView.setText(valueToDisplay);

            }else{
                mBikePricingView.setText(bikePricing);
            }

            if((Boolean)Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_IS_PARK_AND_RIDE_AVAIL_QUERY,
                    Constants.TYPE_BOOL)){
                mIsParkAndRideView.setText("Yes");
            }else{
                mIsParkAndRideView.setText("No");
            }

            if((Boolean)Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY,
                    Constants.TYPE_BOOL)){
                mIsParkingPrepaidView.setText("Yes");
            }else{
                mIsParkingPrepaidView.setText("No");
            }

            mBikeToSlotRatioView.setText(((Integer)Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_BIKES_TO_SLOT_RATIO,
                    Constants.TYPE_INT)).toString());

            mAppVersionView.setText(Constants.APP_VERSION);
            mAppServerView.setText(Constants.BASE_URL);

            if(SystemNotificationsReceiver.isConnected(context)){
                mAppInternetStatusView.setText("Connected");
            }else{
                mAppInternetStatusView.setText("Disconnected");
            }

            String macAddress = (String)Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_BT_MACADDRESS_QUERY,
                    Constants.TYPE_STRING);

            if(macAddress.isEmpty()) {
                mAppPrinterMacAddressView.setText("-");
            }else{
                String macAddressString = macAddress + "\n" + ("(Rescan)");
                mAppPrinterMacAddressView.setText(macAddressString);
                mAppPrinterMacAddressView.setTextColor(Color.BLUE);
            }

            if(SystemNotificationsReceiver.isBluetoothAvailable()){
                mAppBluetoothStateView.setText("Available");
            }else{
                mAppBluetoothStateView.setText("Un-Available");
            }

            int syncState = (Integer)Cache.getFromCache(context,
                              Constants.CACHE_PREFIX_SYNC_STATE_QUERY,
                              Constants.TYPE_INT);

            switch(syncState){
                case Constants.SYNC_STATE_IN_SYNC:
                    mAppSyncStateView.setText("In Sync");
                    break;

                case Constants.SYNC_STATE_GET_SYNC_ID_IN_PROGRESS:
                    mAppSyncStateView.setText("Get Sync ID - In Progress");
                    break;

                case Constants.SYNC_STATE_IN_PROGRESS_PULL:
                    mAppSyncStateView.setText("Out of Sync - Pull in Progress");
                    break;

                case Constants.SYNC_STATE_IN_PROGRESS_PUSH:
                    mAppSyncStateView.setText("Out of Sync - Push in Progress");
                    break;

                case Constants.SYNC_STATE_BOTH_PUSH_AND_PULL_IN_PROGRESS:
                    mAppSyncStateView.setText("Out of Sync - Push & Pull in Progress");
                    break;

                case Constants.SYNC_STATE_PUSH_WAITING_FOR_TIMER_EXPIRY:
                    mAppSyncStateView.setText("Out of Sync - Push waiting for Timer expiry");
                    break;
            }

            int syncId = MiscUtils.getSyncId(context);
            mAppSyncIdView.setText(Integer.toString(syncId));

            if(mPendingTransactions == 0){
                mAppPendingTransactionView.setText("0");
                mPendingTransactionsTimerView.setText("Not Running");
            }else{
                if(syncState == Constants.SYNC_STATE_IN_SYNC ||
                         syncState == Constants.SYNC_STATE_IN_PROGRESS_PUSH) {
                    mAppPendingTransactionView.setText(mPendingTransactions + " (Push Now)");
                    mAppPendingTransactionView.setTextColor(Color.BLUE);
                    mAppPendingTransactionView.setClickable(true);
                }else{
                    mAppPendingTransactionView.setText(mPendingTransactions + " (Push In Progress)");
                }

                if(/*AlarmHandler.isRunning(Constants.TIMER_ACTION_PUSH)*/true){


                    long currentTime = DateTime.now(Constants.TIME_ZONE).getMilliseconds(
                                                                        Constants.TIME_ZONE);

                    long endTime = (Long)Cache.getFromCache(context,
                            Constants.CACHE_PREFIX_TIMER_ACITON_PUSH_END_TIME_QUERY,
                            Constants.TYPE_LONG);


                    long diffInMs = 0;

                    if(currentTime > endTime){
                        //this is just a safety net.
                        ServerUpdationIntentService.startActionPushUpdatesToServer(context);
                    }else {
                        diffInMs = endTime - currentTime;

                        if (diffInMs == 0) {
                            mPendingTransactionsTimerView.setText("Not Running");
                            mTimerLeftRowView.setVisibility(View.GONE);
                        } else {
                            mPendingTransactionsTimerView.setText("Running");
                            mPendingTransactionsTimerLeftView.setText(
                                    Long.toString(diffInMs / Constants.NUM_MS_IN_MINUTE) + " mins");
                            mTimerLeftRowView.setVisibility(View.VISIBLE);
                        }
                    }

                }else{
                    mPendingTransactionsTimerView.setText("Not Running");

                }
            }

            if(mPendingActions == 0){
                mAppPendingActionsView.setText("0");
            }else{
                mAppPendingActionsView.setText(mPendingActions + " (Push Now)");
                mAppPendingActionsView.setClickable(true);

            }



            int gcmId = (Integer)Cache.getFromCache(context,
                       Constants.CACHE_PREFIX_GCM_ID_AT_SERVER_QUERY,
                       Constants.TYPE_INT);

            mGcmIdView.setText(Integer.toString(gcmId));


            mPrintCommandView.setText((String)Cache.getFromCache(getApplicationContext(),
                                      Constants.CACHE_PREFIX_LAST_PRINT_COMMAND,Constants.TYPE_STRING));
            String exception = (String)Cache.getFromCache(getApplicationContext(),
                                      Constants.CACHE_PREFIX_PRINT_EXCEPTION,Constants.TYPE_STRING);
            if(exception.isEmpty()){
                mExceptionView.setText("No exceptions");
            }else {
                mExceptionView.setText(exception);
            }

            boolean flag = (Boolean)Cache.getFromCache(getApplicationContext(),
                                    Constants.CACHE_PREFIX_FLAG_EXCEPTION,Constants.TYPE_BOOL);
            if(flag) {
                mFlagView.setText("True");
            }else{
                mFlagView.setText("False");
            }

            int size = (Integer)Cache.getFromCache(getApplicationContext(),
                                    Constants.CACHE_PREFIX_LAST_PRINT_COMMAND_SIZE,Constants.TYPE_INT);
            mPrintCommandSizeView.setText(Integer.toString(size));


            boolean isSpaceBikeOnly = (Boolean)Cache.getFromCache(getApplicationContext(),
                                            Constants.CACHE_PREFIX_IS_SPACE_BIKE_ONLY_QUERY,
                                            Constants.TYPE_BOOL);

            if(isSpaceBikeOnly){
                mIsSpaceBikeOnlyView.setText("True");
            }else{
                mIsSpaceBikeOnlyView.setText("False");
            }

            int staffPassDiscountCar =(int) Cache.getFromCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_STAFF_DISCOUNT_RATE_QUERY+ VEHICLE_TYPE_CAR,
                    Constants.TYPE_INT);

            int staffPassDiscountBike =(int) Cache.getFromCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_STAFF_DISCOUNT_RATE_QUERY+Constants.VEHICLE_TYPE_BIKE,
                    Constants.TYPE_INT);

            if(staffPassDiscountCar>0){
                mStaffPassDiscountCarRow.setVisibility(View.VISIBLE);
                mStaffPassDiscountCarView.setText(Integer.toString(staffPassDiscountCar));
            }else{
                mStaffPassDiscountCarRow.setVisibility(View.GONE);
            }
            if(staffPassDiscountBike>0){
                mStaffPassDiscountBikeRow.setVisibility(View.VISIBLE);
                mStaffPassDiscountBikeView.setText(String.valueOf(staffPassDiscountCar));
            }else{
                mStaffPassDiscountBikeRow.setVisibility(View.GONE);
            }
            showProgress(false);


        }

        private void inflateErrorRows(){

            /*
            int size = MiscUtils.errorMsgs.size();

            for(int itr = 0; itr < size; itr++) {
                TableRow row = new TableRow(SpaceInformation.this);

                //TableRow.LayoutParams lp = new TableRow.LayoutParams(
                //                                            TableRow.LayoutParams.WRAP_CONTENT);

                //row.setLayoutParams(lp);

                TextView col1 = new TextView(SpaceInformation.this);
                col1.setText(Integer.toString(itr + 1));

                TextView col2 = new TextView(SpaceInformation.this);
                col2.setText(MiscUtils.errorMsgs.get(itr));

                row.addView(col1);
                row.addView(col2);

                mErrorTableLayout.addView(row,itr);


            }*/
        }

        @Override
        protected void onCancelled() {
            mSpaceInformationTask = null;
            showProgress(false);
        }
    }


    private void reloadSettings(){
        new ReloadConf(MiscUtils.getmUserId(getApplicationContext()),MiscUtils.getSpaceId(getApplicationContext())).execute();
    }


    public class ReloadConf extends AsyncTask<Void, String, Boolean> {

        private int mUserId;
        private String email;
        private String mFailureReason;
        private OkHttpClient mClient;
        private int spaceId;
        Context context;
        CacheParams mCacheParams;
        SpotDb mDbObj;

        public ReloadConf(int userId, int spaceId) {
            this.mUserId = userId;

            this.mClient = new OkHttpClient();
            this.spaceId = spaceId;
            this.context = getApplicationContext();
            mCacheParams = new CacheParams();
        }

        @Override
        protected void onPreExecute() {
            showProgress(true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                List<SpotDb> spaces = find(SpotDb.class, "m_space_id = ?",
                        Integer.toString(spaceId));
                mDbObj =  spaces.get(0);;
                this.email = mDbObj.getOperatorEmail();
                int operatorId = MiscUtils.getOperatorId(getApplicationContext());
                Request getSpacesRequest = buildGetSpacesRequest(mUserId,spaceId,operatorId);

                Response getSpacesResponse = mClient.newCall(getSpacesRequest).execute();

                if (getSpacesResponse.isSuccessful()) {
//                mDbObj.setOperatorDetails(true, this.email);
                    if(parseAndStoreSpaceInfoResponse(getSpacesResponse)) {
                        getSpacesResponse.body().close();
                        return true;
                    }else{
                        return false;
                    }
                }else{
                    mFailureReason = "Invalid response received. Please contact SimplyPark.in";
                    return false;
                }


            } catch (IOException | JSONException e) {
                Log.e("SPOT-APP", "EXCEPTION", e);
                mFailureReason = "Unknown error. Please try after sometime" + e.toString();
                return false;
            }


        }


        @Override
        protected void onPostExecute(final Boolean success) {
            showProgress(false);
            if(success) {
                recreate();
            }else{
                Toast.makeText(getApplicationContext(), R.string.update_failed + ", " + mFailureReason, Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected void onProgressUpdate(String ...param){
//            Toast.makeText(getApplicationContext(),""+param[0]+" ",Toast.LENGTH_SHORT).show();
        }
        @Override
        protected void onCancelled() {
            showProgress(false);
        }

        private Request buildGetSpacesRequest(int userId,int spaceId,int operatorId) throws JSONException {
            String url = Constants.SPACE_INFO_URL + userId + "/"+spaceId+ "/" + operatorId +
                                                   "/1.json";
            return (new Request.Builder()
                    .url(url)
                    .build());


        }

        /************************************************************************************
         *
         * @param spaceResponse
         * @return
         * @throws JSONException
         */

        private boolean parseAndStoreSpaceInfoResponse(Response spaceResponse)
                throws JSONException {
            try {
                String response = spaceResponse.body().string();
                publishProgress(response);
                JSONObject json = new JSONObject(response);

                int status = json.getInt("status");
                String reason = json.getString("reason");

                if (status == 1) {

                   return setmCacheParams(json);

                } else {
                    mFailureReason = reason;
                    return false;
                }


            } catch (IOException | JSONException ioe) {
                Log.e("SPOT-APP", "EXCEPTION", ioe);
                mFailureReason = "Error:: " + ioe;
                return false;


            }
        }

        /**************************
         *
         * @param json
         * @return
         * @throws JSONException
         */
        private boolean setmCacheParams(JSONObject json)throws JSONException {
            try {
                mCacheParams.setSpaceName(json.getString("name"));
                mCacheParams.setSpaceId(json.getInt("spaceId"));
                mCacheParams.setSlots(json.getInt("slots"));
                mCacheParams.setOpeningDateTime(json.getString("openingDateTime"));
                mCacheParams.setClosingDateTime(json.getString("closingDateTime"));
                mCacheParams.setOpen24Hours(json.optBoolean("is24HoursOpen", false));
                mCacheParams.setLostTicketCharge(json.optInt("lostTicketCharge", 0));
                mCacheParams.setSpaceOwnerName(json.optString("spaceOwnerName"));
                mCacheParams.setSpaceOwnerMobile(json.optString("spaceOwnerMobile"));
                mCacheParams.setMultiDayParkingPossible(
                        json.optBoolean("isMultiDayParkingPossible", false));
                mCacheParams.setSpaceBikeOnly(json.optBoolean("isSpaceBikeOnly", false));

                /*Deault vehicleType this is the default selection value while entering a vehicle.*/
                mCacheParams.setDefaultVehicleType(json.getString("spaceDefaultVehicleType"));

                mCacheParams.setGateWiseReportsRequired(
                        json.optBoolean("isGateWiseReportsRequired",false));


                mCacheParams.setParkingPrepaid(json.optBoolean("isParkingPrepaid", false));


                mCacheParams.setParkAndRideAvailable(
                        json.optBoolean("isParkAndRideAvailable", false));

                mCacheParams.setCancelUpdateAllowed(
                               json.optBoolean("isCancelUpdateAllowed",true));


                mCacheParams.setNumBikesInSingleSlot(json.optInt("numBikesInSingleSlot", 0));

                mDbObj.setSpaceDescription(mCacheParams.getSpaceName(), mCacheParams.getSpaceId(),
                        mCacheParams.getSlots());


                mCacheParams.setOperatorId(json.optInt("operatorId",
                        Constants.INVALID_OPERATOR_ID));


                boolean applyCustomTicketFormat = json.optBoolean("applyCustomTicketFormat",
                        false);

                if (applyCustomTicketFormat) {
                    JSONArray customFormat = json.getJSONArray("ticketFormat");
                    for (int i = 0; i < customFormat.length(); i++) {
                        JSONObject formatObject = customFormat.getJSONObject(i).
                                getJSONObject("TicketFormat");
                        int ticketType = Integer.parseInt((String) formatObject.get("ticket_type"));
                        switch (ticketType) {
                            case Constants.ENTRY_TICKET_CUSTOM_FORMAT: {
                                setCustomEntryTicketFormat(formatObject);
                            }
                            break;
                        }

                    }

                }

                //set NumPrepaid hours
                if (mCacheParams.isParkingPrepaid()) {
                    mCacheParams.setNumHoursPrepaid(json.optInt("numPrepaidHours", 0));
                }


                //Pricing Info - Make sure nightcharge and prepaid hours are set before
                //this
                JSONArray pricing = json.getJSONArray("pricingPerVehicleType");
                int length = pricing.length();


                for (int i = 0; i < length; i++) {
                    JSONObject vehicleTypePricing = pricing.getJSONObject(i);
                    setPricing(vehicleTypePricing);
                }


                //for online bookings,is the slot occupied assumed or will the qr code be
                //scanned
                mCacheParams.setSlotOccupiedAssumed(
                        json.optBoolean("isSlotOccupiedAssumed", false));

                //for passes generated via app, do we need to activate a qr code based monthly
                //pass

                boolean isPassActivationRequired = json.optBoolean("isPassActivationRequired",
                        false);

                Cache.setInCache(context,
                        Constants.CACHE_PREFIX_IS_PASS_ACTIVATION_REQUIRED,
                        isPassActivationRequired, Constants.TYPE_BOOL);

                //The currentOccupancy variable, although, set here is NO longer used by
                //main activity. Infact, since DB read is slow we are not even reading it
                //from the DB


                mDbObj.setCurrentOccupancy(json.getInt("currentOccupancy"));
                mDbObj.setIsSpaceInfoAvailable(true);

                boolean reusablePrintTokensUsed = json.optBoolean("reusableTokensUsed",false);


                mCacheParams.setIsReusablePrintTokenUsed(reusablePrintTokensUsed);

                if(reusablePrintTokensUsed){
                    setPrintTokens(json.getJSONObject("tokens"));
                }


                mDbObj.save();

                mCacheParams.setCache(context); //setup Cache as well

                //startDay end timer as well
                    /*AlarmHandler.startDayEndTimer(getApplicationContext(),Constants.TIMER_ACTION,
                                                  openingDateTime, closingDateTime);*/
                return true;
            }catch (JSONException ioe) {
                Log.e("SPOT-APP", "EXCEPTION", ioe);
                mFailureReason = "Error:: " + ioe;
                return false;
            }
        }

        private void setPricing(JSONObject pricingInfo) throws JSONException{
            List<PricingPerVehicleTypeTbl> pricingList =
                    find(PricingPerVehicleTypeTbl.class,"m_vehicle_type = ?",
                            Integer.toString(pricingInfo.optInt("vehicle_type",Constants.VEHICLE_TYPE_INVALID)));
            PricingPerVehicleTypeTbl pricing;
            if(pricingList.size()>0) {
                pricing = pricingList.get(0);
            }else{
                pricing = new PricingPerVehicleTypeTbl();
                pricing.setVehicleType(pricingInfo.optInt("vehicle_type",Constants.VEHICLE_TYPE_INVALID));
            }

            pricing.setHourlyPrice(pricingInfo.optDouble("hourly",0.00));
            pricing.setDailyPrice(pricingInfo.optDouble("daily",0.00));
            pricing.setWeeklyPrice(pricingInfo.optDouble("weekly",0.00));
            pricing.setMonthlyPrice(pricingInfo.optDouble("monthly",0.00));
            pricing.setYearlyPrice(pricingInfo.optDouble("yearly",0.00));
            pricing.setIsPricingTiered(pricingInfo.optBoolean("tierPricingSupport",false));
            pricing.setLostTicketCharge(pricingInfo.optDouble("lost_ticket_charge",0.00));
            pricing.setStaffPassDiscountPercentage(pricingInfo.optInt("staff_pass_discount_percentage",0));

            if(pricing.getLostTicketCharge() > 0.00){
                //this boolean is used to display the lost ticket charge switch on the exit screen
                Cache.setInCache(context,Constants.CACHE_PREFIX_IS_LOST_TICKET_CHARGE_AVAILABLE,
                        true,Constants.TYPE_BOOL);
            }

            if(pricing.getStaffPassDiscountPercentage() > 0){
                //this boolean is used to display the lost ticket charge switch on the exit screen
                Cache.setInCache(context,Constants.CACHE_PREFIX_IS_STAFF_DISCOUNT_AVAILABLE_QUERY,
                        true,Constants.TYPE_BOOL);
            }



            boolean isParkingPrepaid = (Boolean)Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY,Constants.TYPE_BOOL);
            int numPrepaidHours = 0;


            if(pricing.isPricingTiered()){
                String cacheKey = Constants.CACHE_PREFIX_IS_PRICING_TIERED + pricing.getVehicleType();
                Cache.setInCache(context,cacheKey,true,Constants.TYPE_BOOL);
                setTieredPricingInCache(pricingInfo.getJSONObject("tierPricing"),
                        pricing.getVehicleType());

                if(mCacheParams.isParkingPrepaid()){
                    String key = Constants.CACHE_PREFIX_PRICE_AT_HOUR_QUERY +
                            pricing.getVehicleType() +
                            Integer.toString(mCacheParams.getNumHoursPrepaid());
                    Double fees = ((Double)Cache.getFromCache(context, key,Constants.TYPE_DOUBLE));
                    int parkingFees = fees.intValue();

                    setPrepaidParkingFees(pricing.getVehicleType(),parkingFees);
                }
            }else{
                setPricingInCache((int)pricing.getHourlyPrice(),pricing.getVehicleType());
                if(mCacheParams.isParkingPrepaid()){
                    int parkingFees = (int)(pricing.getHourlyPrice() * numPrepaidHours);
                    setPrepaidParkingFees(pricing.getVehicleType(),parkingFees);
                }

            }

            //night charge
            pricing.setIsNightChargeAvailable(pricingInfo.optBoolean("isNightChargeAvailable", false));


            if(pricing.isNightChargeAvailable()){
                JSONObject nightChargeObj = pricingInfo.getJSONObject("nightCharge");
                NightChargeTbl nightCharge = new NightChargeTbl();
                nightCharge.setStartHour(nightChargeObj.optInt("startHour",0));
                nightCharge.setEndHour(nightChargeObj.optInt("endHour", 0));
                nightCharge.setDailyCharge(nightChargeObj.optInt("dailyCharge", 0));
                nightCharge.setMonthlyCharge(nightChargeObj.optInt("monthlyCharge",0));
                nightCharge.setVehicleType(pricing.getVehicleType());

                nightCharge.save();

                //set in Cache
                Cache.setInCache(context,
                        Constants.CACHE_PREFIX_NIGHT_CHARGE_QUERY + pricing.getVehicleType(),
                        nightCharge.getDailyCharge(),Constants.TYPE_INT);
            }

            //penalty charge
            pricing.setIsPenaltyAvailable(pricingInfo.optBoolean("isPenaltyChargeAvailable", false));
            pricing.setChargePenaltyOnMonthlyPasses(pricingInfo.optBoolean("chargePenaltyOnMonthlyPasses", false));



            if(pricing.isPenaltyAvailable()){
                JSONObject penaltyObj = pricingInfo.getJSONObject("penaltyCharge");
                PenaltyChargeTbl penalty = new PenaltyChargeTbl();
                penalty.setChargeCondition(penaltyObj.optInt("chargeCondition"));
                penalty.setHourlyCharge(penaltyObj.optInt("hourlyCharge"));
                penalty.setVehicleType(pricing.getVehicleType());
                penalty.save();
            }

            pricing.save();

            //store monthly prices for car,bike,minibus,bus
            String key = Constants.CACHE_PREFIX_MONTHLY_RATE_QUERY +  pricing.getVehicleType();
            int monthlyPrice = (int)pricing.getMonthlyPrice();
            Cache.setInCache(getApplicationContext(),key,
                    monthlyPrice ,Constants.TYPE_INT);

            String discountKey = Constants.CACHE_PREFIX_STAFF_DISCOUNT_RATE_QUERY + pricing.getVehicleType();
            Cache.setInCache(getApplicationContext(),discountKey,
                    (int)pricing.getStaffPassDiscountPercentage(),Constants.TYPE_INT);

            String penaltyOnPassKey = Constants.CACHE_PREFIX_IS_PENALTY_ON_MONTHLY_PASS_AVAILABLE_QUERY;
            Cache.setInCache(getApplicationContext(), penaltyOnPassKey,
                    pricing.isChargePenaltyOnMonthlyPasses(),Constants.TYPE_BOOL);

            setPricingAsAvailable(pricing.getVehicleType());

        }

        private void setPricingInCache(int hourlyPrice,int vehicleType){
            String pricingStr = "Rs. " + hourlyPrice + "/hour";




            //save the received pricing string to help us print
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_PRINT_PRICING_QUERY + vehicleType,
                    pricingStr, Constants.TYPE_STRING);


        }

        private void setTieredPricingInCache(JSONObject tierPricingObject, int vehicleType)
                throws JSONException {

            Iterator<String> iter = tierPricingObject.keys();


            int itr = 1;

            String tierPricingToPrint = "";
            int startHour = 0;
            Map<Integer, Double> sortedTierPrice = new TreeMap<Integer, Double>();

            int i = 0;
            while (iter.hasNext()) {
                String key = iter.next();
                Double pricePerTier = Double.parseDouble(key);
                int hours = tierPricingObject.getInt(key);
                sortedTierPrice.put(hours, pricePerTier);
            }

            Iterator entries = sortedTierPrice.entrySet().iterator();
            int maxHour = 1;
            double maxPricePerTier = 0.00;
            String cacheKey = Constants.CACHE_PREFIX_PRICE_AT_HOUR_QUERY + vehicleType;

            while (entries.hasNext()) {
                Map.Entry element = (Map.Entry) entries.next();
                int hours = (int) element.getKey();
                double pricePerTier = (double) element.getValue();

                if (entries.hasNext()) {
                    tierPricingToPrint += startHour + "-" + hours + " hours: Rs." +
                            (int) Math.ceil(pricePerTier) +
                            Constants.TIER_PRICE_SEPERATOR;
                } else {
                    tierPricingToPrint += "More than " + startHour + " hours: Rs. " +
                            (int) Math.ceil(pricePerTier) + Constants.TIER_PRICE_SEPERATOR;
                }

                startHour = hours;
                maxHour = hours;
                maxPricePerTier = pricePerTier;



                for (; itr <= hours; itr++) {
                    Cache.setInCache(context,cacheKey + Integer.toString(itr),
                            pricePerTier, Constants.TYPE_DOUBLE);
                }

            }

            if(maxHour != Constants.NUM_HOURS_IN_A_DAY){
                for(int idx = maxHour + 1; idx <= Constants.NUM_HOURS_IN_A_DAY; idx++){
                    Cache.setInCache(context,cacheKey + Integer.toString(idx),
                            maxPricePerTier, Constants.TYPE_DOUBLE);
                }
            }

            //save the received tier pricing string to help us print
            String key = Constants.CACHE_PREFIX_PRINT_PRICING_QUERY + vehicleType;
            Cache.setInCache(context,key,tierPricingToPrint, Constants.TYPE_STRING);



        }

        private  void setPrepaidParkingFees(int vehicleType,int parkingFees){



            switch (vehicleType){
                case VEHICLE_TYPE_CAR:{
                    Cache.setInCache(context,Constants.CACHE_PREFIX_CAR_PREPAID_FEES_QUERY,
                            parkingFees,Constants.TYPE_INT);
                }
                break;

                case Constants.VEHICLE_TYPE_BIKE:{
                    Cache.setInCache(context,Constants.CACHE_PREFIX_BIKE_PREPAID_FEES_QUERY,
                            parkingFees,Constants.TYPE_INT);
                }
                break;

                case Constants.VEHICLE_TYPE_MINIBUS:{
                    Cache.setInCache(context,Constants.CACHE_PREFIX_MINIBUS_PREPAID_FEES_QUERY,
                            parkingFees,Constants.TYPE_INT);
                }
                break;

                case Constants.VEHICLE_TYPE_BUS:{
                    Cache.setInCache(context,Constants.CACHE_PREFIX_BUS_PREPAID_FEES_QUERY,
                            parkingFees,Constants.TYPE_INT);
                }
                break;
            }



        }

        private void setPricingAsAvailable(int vehicleType){
            switch(vehicleType){
                case VEHICLE_TYPE_CAR:
                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_IS_CAR_PRICING_AVAILABLE_QUERY,
                            true,Constants.TYPE_BOOL);
                    break;

                case Constants.VEHICLE_TYPE_BIKE:
                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_IS_BIKE_PRICING_AVAILABLE_QUERY,
                            true,Constants.TYPE_BOOL);
                    break;

                case Constants.VEHICLE_TYPE_MINIBUS:
                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_IS_MINIBUS_PRICING_AVAILABLE_QUERY,
                            true,Constants.TYPE_BOOL);
                    break;

                case Constants.VEHICLE_TYPE_BUS:
                    Cache.setInCache(context,
                            Constants.CACHE_PREFIX_IS_BUS_PRICING_AVAILABLE_QUERY,
                            true,Constants.TYPE_BOOL);
                    break;


            }
        }

        private void setCustomEntryTicketFormat(JSONObject customEntryFormat){
            Iterator<String> iter = customEntryFormat.keys();
            String[] customStrings = new String[Constants.CUSTOM_FORMAT_MAX_IDX];

            int i = 0;
            try {
                boolean customFormatAvailable = false;
                while (iter.hasNext()) {
                    String key = iter.next();
                    if(key.equals(Constants.CUSTOM_VEHICLE_TEXT_SIZE)){
                        int value = customEntryFormat.optInt(key,0);
                        if(value == 1) {
                            Cache.setInCache(context,Constants.CACHE_PREFIX_BIG_SIZE_VEHICLE_REGSTRING,
                                    true, Constants.TYPE_BOOL);
                        }

                        continue;
                    }
                    String value = customEntryFormat.getString(key);

                    if(!value.isEmpty()) {

                        if(key.equals(Constants.CUSTOM_SPACE_ADDRESS)) {
                            customStrings[Constants.CUSTOM_SPACE_ADDRESS_IDX] = value;
                            customFormatAvailable = true;
                        }else if(key.equals(Constants.CUSTOM_SPACE_HEADING)){
                            customStrings[Constants.CUSTOM_SPACE_HEADING_IDX] = value;
                            customFormatAvailable = true;
                        }else if(key.equals(Constants.CUSTOM_AGENCY_NAME)){
                            customStrings[Constants.CUSTOM_AGENCY_NAME_IDX] = value;
                            customFormatAvailable = true;
                        }else if(key.equals(Constants.CUSTOM_SPACE_OWNER_MOBILE)){
                            customStrings[Constants.CUSTOM_SPACE_OWNER_MOBILE_IDX] = value;
                            customFormatAvailable = true;
                        }else if(key.equals(Constants.CUSTOM_OPERATING_HOURS)){
                            customStrings[Constants.CUSTOM_OPERATING_HOURS_IDX] = value;
                            customFormatAvailable = true;
                        }else if(key.equals(Constants.CUSTOM_PARKING_RATES)){
                            customStrings[Constants.CUSTOM_PARKING_RATES_IDX] = value;
                            customFormatAvailable = true;
                        }else if(key.equals(Constants.CUSTOM_BIKE_PARKING_RATES)){
                            customStrings[Constants.CUSTOM_BIKE_PARKING_RATES_IDX] = value;
                            customFormatAvailable = true;
                        }else if(key.equals(Constants.CUSTOM_NIGHT_CHARGES)){
                            customStrings[Constants.CUSTOM_NIGHT_CHARGES_IDX] = value;
                            customFormatAvailable = true;
                        }else if(key.equals(Constants.CUSTOM_BIKE_NIGHT_CHARGES)){
                            customStrings[Constants.CUSTOM_BIKE_NIGHT_CHARGES_IDX] = value;
                            customFormatAvailable = true;
                        }else if(key.equals(Constants.CUSTOM_LOST_TICKET)){
                            customStrings[Constants.CUSTOM_LOST_TICKET_IDX] = value;
                            customFormatAvailable = true;
                        }else if(key.equals(Constants.CUSTOM_URL)){
                            customStrings[Constants.CUSTOM_URL_IDX] = value;
                            customFormatAvailable = true;
                        }else if(key.equals(Constants.CUSTOM_DESC)){
                            customStrings[Constants.CUSTOM_DESC_IDX] = value;
                            customFormatAvailable = true;
                        }else if(key.equals(Constants.CUSTOM_AD_TEXT)){
                            customStrings[Constants.CUSTOM_AD_TEXT_IDX] = value;
                            customFormatAvailable = true;
                        }else if(key.equals(Constants.CUSTOM_SPARK_AD_TEXT)){
                            customStrings[Constants.CUSTOM_SPARK_AD_IDX] = value;
                            customFormatAvailable = true;
                        }
                    }

                }
                if(customFormatAvailable) {
                    MiscUtils.setCustomFormats(context, customStrings);
                }
            }catch(JSONException je){

            }

        }

        private void setPrintTokens(JSONObject tokens) throws JSONException{
            Iterator<String> keys = tokens.keys();

            //new qr codes might be added. Currently we do not support delete.
            while(keys.hasNext()){
                String key = keys.next();
                String token = tokens.getString(key);

                if(!ReusableTokensTbl.isTokenConfigured(token)) {
                    ReusableTokensTbl row = new ReusableTokensTbl(token);
                    row.save();
                }
            }
        }



    }

}
