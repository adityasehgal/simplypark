package in.simplypark.spot_app.printer;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

/**
 * Created by root on 07/10/16.
 */
@SuppressLint({"NewApi"})
public class PrinterHandle {
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;
    byte[] readBuffer = null;
    int readBufferPosition;
    int counter;
    volatile boolean stopWorker;




    public UUID getUUID(){
        return UUID.fromString("");
    }

    public void openBT(String address) throws IOException {
        this.findBT(address);

        try {
            //UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            this.mmSocket = this.mmDevice.createRfcommSocketToServiceRecord(getUUID());
            this.mmSocket.connect();
            this.mmOutputStream = this.mmSocket.getOutputStream();
            this.mmInputStream = this.mmSocket.getInputStream();
            //this.beginListenForData();
        } catch (NullPointerException var3) {
            Log.d("PRINTER", "Exception while connecting::" + var3);
        } catch (Exception var4) {
            Log.d("PRINTER", "Exception while connecting::" + var4);
        }

    }



    public void findBT(String address) {
        try {
            this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if(!this.mBluetoothAdapter.isEnabled()) {
                this.mBluetoothAdapter.enable();
            }

            Set e = this.mBluetoothAdapter.getBondedDevices();
            BluetoothDevice device1;
            if(e.size() > 0) {
                Iterator var4 = e.iterator();

                while(var4.hasNext()) {
                    device1 = (BluetoothDevice)var4.next();
                    if(device1.getAddress().equals(address)) {
                        this.mmDevice = device1;
                        break;
                    }
                }
            }

            device1 = null;
            device1 = this.mBluetoothAdapter.getRemoteDevice(address);
            this.mmDevice = device1;
            this.mmDevice.createBond();
        } catch (NullPointerException var5) {
            var5.printStackTrace();
        } catch (Exception var6) {
            var6.printStackTrace();
        }

    }

    public void closeBT() throws IOException {
        try {
            this.mmOutputStream.close();
            this.mmInputStream.close();
            this.mmSocket.close();
        } catch (NullPointerException var2) {
            var2.printStackTrace();
        } catch (Exception var3) {
            var3.printStackTrace();
        }

    }

    public boolean printData(byte[] msg) {
        boolean flag = false;

        try {
            this.mmOutputStream.write(msg);
            this.mmOutputStream.flush();
            flag = true;
        } catch (Exception var4) {
            var4.printStackTrace();
        }

        return flag;
    }

    public boolean printData(String msg) {
        boolean flag = false;

        try {
            this.mmOutputStream.write(msg.getBytes());
            this.mmOutputStream.flush();
        } catch (IOException var4) {
            var4.printStackTrace();
        }

        flag = true;
        return flag;
    }
}
