package in.simplypark.spot_app.utils;

import android.content.Context;

//import com.analogics.thermalAPI.Bluetooth_Printer_2inch_ThermalAPI;
//import com.analogics.thermalAPI.Bluetooth_Printer_3inch_ThermalAPI;

import java.util.Locale;

import hirondelle.date4j.DateTime;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.listelements.Element;

/**
 * Created by aditya on 23/05/16.
 */
public class    Prepare2InchData extends PreparePrintData {

    //size related constants
    private static final int COURIER_10 = 0;
    private static final int COURIER_19 = 1;
    private static final int COURIER_20 = 2;
    private static final int COURIER_24 = 3;
    private static final int COURIER_25 = 4;
    private static final int COURIER_27 = 5;
    private static final int COURIER_29 = 6;
    private static final int COURIER_32 = 7;
    private static final int COURIER_34 = 8;
    private static final int COURIER_38 = 9;
    private static final int COURIER_42 = 10;
    private static final int COURIER_48 = 11;


    private static final int SERIF_8  = 12;
    private static final int SERIF_32 = 13;
    private static final int SERIF_34 = 14;
    private static final int SERIF_38 = 15;

    private static final int MAX_FONT_MAX_SIZE = 16;

    private static final int charactersOnLine[] = new int[]{ 10,19,20,24,25,27,29,32,34,38,42,48,
                                                             8,32,34,38};

    private static final int COURIER_SMALL  = COURIER_48;
    private static final int COURIER_MEDIUM = COURIER_29;
    private static final int COURIER_MEDIUM_BIG = COURIER_24;
    private static final int COURIER_BIG    = COURIER_10;

    private static final int COURIER_HEADING_FONT_SIZE = COURIER_29;

    private static final int SERIF_SMALL = SERIF_38;
    private static final int SERIF_MEDIUM = SERIF_32;
    private static final int SERIF_BIG    = SERIF_8;


    private Formatter mFormatter;

    private Context mContext;
    private String mPriceListTemplate = "";
    private String mNewLine = "";


    public Prepare2InchData(Context context, int printerType){
        mContext = context;
        if(printerType == Constants.PRINTER_TYPE_ANALOGICS) {
            mFormatter = new Analogics2InchFormatter();
        }else{
            mFormatter = new Mate2InchFormatter();
            mNewLine = "\n";

        }
    }


    public String print(String data){
        String bytesToPrint = data;

        bytesToPrint += "\n";

        return bytesToPrint;
    }



    public byte[] prepareEntryTicket(byte[] headerTemplate, byte[] footerTemplate,
                                     Element[][] bodyFormat,
                                     String dateTime, String parkingId, Boolean isParkAndRide,
                                     String regNumber,int vehicleType){

        int headerLength = headerTemplate.length;
        int footerLength = footerTemplate.length;
        byte[] body = prepareBody(bodyFormat, dateTime, parkingId, regNumber, isParkAndRide,
                vehicleType);
        int length = headerLength + body.length + footerLength;

        byte[] dataToPrint = new byte[length];
        System.arraycopy(headerTemplate, 0, dataToPrint, 0,
                headerLength);
        System.arraycopy(body, 0, dataToPrint,
                headerLength,
                body.length);

        System.arraycopy(footerTemplate,0,dataToPrint,headerLength+body.length,footerLength);

        return dataToPrint;


    }

    public byte[] prepareExitTicket(byte[] headerTemplate, byte[] footerTemplate,
                                     Element [][] bodyFormat,
                                     String entryDateTime, String exitDateTime, String parkingFees,
                                     String nightChargesDesc,
                                     String regNumber,String durationDescription,int vehicleType){

        int headerLength = headerTemplate.length;
        int footerLength = footerTemplate.length;
        byte[] body = prepareExitBody(bodyFormat, entryDateTime, exitDateTime, parkingFees,
                                      nightChargesDesc,regNumber,durationDescription, vehicleType);
        int length = headerLength + body.length + footerLength;

        byte[] dataToPrint = new byte[length];
        System.arraycopy(headerTemplate, 0, dataToPrint, 0,
                headerLength);
        System.arraycopy(body, 0, dataToPrint,
                headerLength,
                body.length);

        System.arraycopy(footerTemplate,0,dataToPrint,headerLength+body.length,footerLength);

        return dataToPrint;

    }

    public  byte[] prepareReport(byte[] headerTemplate, byte[] footerTemplate,Element[][] bodyFormat,
                                 String reportPeriod, TransactionSummary summary){

        int headerLength = headerTemplate.length;
        int footerLength = footerTemplate.length;
        byte[] body = prepareReportBody(bodyFormat, reportPeriod, summary);

        int length = headerLength + body.length + footerLength;

        byte[] dataToPrint = new byte[length];
        System.arraycopy(headerTemplate, 0, dataToPrint, 0,
                headerLength);
        System.arraycopy(body, 0, dataToPrint,
                headerLength,
                body.length);

        System.arraycopy(footerTemplate,0,dataToPrint,headerLength+body.length,footerLength);

        return dataToPrint;

    }

    public byte[] prepareMonthlyPass(byte[] headerTemplate, byte[] footerTemplate,
                                     Element[][] bodyFormat,
                                     String vehicleNumber, int vehicleType,
                                     String issuedTo, String email, String tel,
                                     String validFrom,String validTill,String transId,
                                     int perMonthFees){
        int headerLength = headerTemplate.length;
        int footerLength = footerTemplate.length;
        byte[] body = prepareMonthlyPassBody(bodyFormat, vehicleNumber, vehicleType, issuedTo, email,
                tel, validFrom,validTill, transId, perMonthFees);


        int length = headerLength + body.length + footerLength;

        byte[] dataToPrint = new byte[length];
        System.arraycopy(headerTemplate, 0, dataToPrint, 0,
                headerLength);
        System.arraycopy(body, 0, dataToPrint,
                headerLength,
                body.length);

        System.arraycopy(footerTemplate,0,dataToPrint,headerLength+body.length,footerLength);

        return dataToPrint;

    }

    public byte[] prepareMonthlyPassReceipt(byte[] headerTemplate, byte[] footerTemplate,
                                            Element[][] bodyFormat,
                                            int amountTransferred, String transId,
                                            String nextPaymentDueOn){
        int headerLength = headerTemplate.length;
        int footerLength = footerTemplate.length;
        byte[] body = prepareMonthlyPassReceiptBody(bodyFormat, amountTransferred, transId,
                                                    nextPaymentDueOn);


        int length = headerLength + body.length + footerLength;

        byte[] dataToPrint = new byte[length];
        System.arraycopy(headerTemplate, 0, dataToPrint, 0,
                headerLength);
        System.arraycopy(body, 0, dataToPrint,
                headerLength,
                body.length);

        System.arraycopy(footerTemplate,0,dataToPrint,headerLength+body.length,footerLength);

        return dataToPrint;


    }



    public byte[] prepareBody(Element[][] body,String dateTime, String parkingId,
                              String regNumber,boolean isParkAndRide,int vehicleType){

        String bodyToPrint = "";


        for(int line=0 ; line < Constants.PRINTER_MAX_ENTRY_BODY_ELEMENTS; line++) {
            String lineContent = new String();
            for (int elements = 0; elements < Constants.PRINTER_MAX_ENTRY_BODY_SUB_ELEMENTS;
                                                                                       elements++) {
                Element element = body[line][elements];

                int key = element.getKey();

                switch(key){
                    case Constants.PRINTER_KEY_ENTRY_DATE_TIME: {
                        //String valueToPrint = mFormatter.font_Emphasized_On()+
                         String  valueToPrint = "In Time:" + mFormatter.horizontal_Tab() +
                                              dateTime;
                        lineContent +=
                                    prepareElement(element, valueToPrint, lineContent.length());
                        //lineContent += mFormatter.font_Emphasized_Off();
                        lineContent += "\n";



                    }
                        break;
                    case Constants.PRINTER_KEY_REG_NUMBER: {
                        //String valueToPrint = mFormatter.font_Emphasized_On();
                        String valueToPrint = "";
                        String vehiclePrefix = "";


                        if(vehicleType == Constants.VEHICLE_TYPE_CAR) {
                            vehiclePrefix = "Car: ";
                        }else if(vehicleType == Constants.VEHICLE_TYPE_BIKE){
                            vehiclePrefix = "Bike: ";
                        }else if(vehicleType == Constants.VEHICLE_TYPE_MINIBUS){
                            vehiclePrefix = "MiniBus: ";
                        }else if(vehicleType == Constants.VEHICLE_TYPE_BUS){
                            vehiclePrefix = "Bus: ";
                        }

                        valueToPrint += vehiclePrefix +  regNumber;

                        lineContent += prepareElement(element, valueToPrint, lineContent.length());
                        lineContent += "\n";
                        //lineContent += mFormatter.font_Emphasized_Off();
                    }
                        break;

                    case Constants.PRINTER_KEY_OPERATING_HOURS:{
                        String valueToPrint = getDynamicInfoToPrint(
                                                     Constants.PRINTER_KEY_OPERATING_HOURS);

                        lineContent += prepareElement(element, valueToPrint, lineContent.length());
                        lineContent += "\n";

                    }
                    break;

                    case Constants.PRINTER_KEY_PARKING_RATES:{
                        String valueToPrint = "";
                        String toPrint = "";
                        if(vehicleType == Constants.VEHICLE_TYPE_CAR) {
                            valueToPrint = getDynamicInfoToPrint(
                                    Constants.PRINTER_KEY_CAR_PARKING_RATES);
                        }else if(vehicleType == Constants.VEHICLE_TYPE_BIKE){
                            valueToPrint = getDynamicInfoToPrint(
                                    Constants.PRINTER_KEY_BIKE_PARKING_RATES);
                        }else if(vehicleType == Constants.VEHICLE_TYPE_MINIBUS){
                            valueToPrint = getDynamicInfoToPrint(
                                    Constants.PRINTER_KEY_MINIBUS_PARKING_RATES);
                        }else if(vehicleType == Constants.VEHICLE_TYPE_BUS){
                            valueToPrint = getDynamicInfoToPrint(
                                    Constants.PRINTER_KEY_BUS_PARKING_RATES);
                        }

                        if((Boolean)Cache.getFromCache(mContext,
                                                  Constants.CACHE_PREFIX_IS_PRICING_TIERED + vehicleType,
                                                  Constants.TYPE_BOOL)){
                            if (valueToPrint.indexOf(Constants.TIER_PRICE_SEPERATOR) >= 0) {
                                /*String returnedValue = valueToPrint.replace('X',',');
                                returnedValue = returnedValue.replaceAll("hours","hrs");
                                toPrint = "mFormatter.carriage_Return()  Pricing:" + mFormatter.horizontal_Tab()+
                                       returnedValue.substring(0,returnedValue.length()-1);*/
                                String[] tieredPricing = valueToPrint.split("X");

                                toPrint = "  ------------------------\n";
                                toPrint += "        Pricing \n";

                                for (int i = 0; i < tieredPricing.length; i++) {
                                    toPrint += "  " + tieredPricing[i] + "\n";
                                }
                            }else{
                                toPrint = "  ------------------------\n";
                                toPrint += "        Pricing \n";
                                toPrint += valueToPrint;
                            }

                        } else {
                            toPrint = "\n  Pricing: " + mFormatter.horizontal_Tab()+ valueToPrint;
                            //lineContent += mPriceListTemplate;
                        }


                        lineContent += prepareElement(element, toPrint,
                                lineContent.length());

                        //lineContent+= mNewLine;
                    }

                    break;

                    case Constants.PRINTER_KEY_NIGHT_CHARGE: {
                        String nightCharge = "";

                        if (vehicleType == Constants.VEHICLE_TYPE_CAR) {
                            nightCharge = getDynamicInfoToPrint(Constants.PRINTER_KEY_CAR_NIGHT_CHARGE);
                        } else {
                            nightCharge = getDynamicInfoToPrint(Constants.PRINTER_KEY_BIKE_NIGHT_CHARGE);
                        }


                        if (!nightCharge.isEmpty()) {
                            String valueToPrint = nightCharge;

                            lineContent += prepareElement(element, valueToPrint,
                                    lineContent.length());
                            //lineContent += "\n";
                        }
                        //lineContent += "\n";
                    }
                        break;

                    case Constants.PRINTER_KEY_PARKING_ID: {

                        String valueToPrint = "";

                        //valueToPrint = "    " + parkingId;
                        valueToPrint = "Parking ID:" + parkingId;

                        lineContent += prepareElement(element, valueToPrint, lineContent.length());
                        lineContent += "\n";
                    }
                    break;
                    case Constants.PRINTER_KEY_PARK_AND_RIDE: {
                        if (isParkAndRide) {
                            lineContent += prepareElement(element,
                                    Constants.PRINTER_VALUE_PER_KEY[Constants.PRINTER_KEY_PARK_AND_RIDE],
                                    lineContent.length());
                        }
                    }
                     break;

                    case Constants.PRINTER_KEY_DYNAMIC_AD: {
                        String valueToPrint = getDynamicInfoToPrint(
                                Constants.PRINTER_KEY_DYNAMIC_AD);

                        if(!valueToPrint.isEmpty()) {
                            lineContent += prepareElement(element, valueToPrint, lineContent.length());
                        }else{
                            lineContent = "";
                        }

                    }
                    break;

                    }


            }
            //lineContent += "\n";
            bodyToPrint += lineContent;

        }
        bodyToPrint += "\n";
        return bodyToPrint.getBytes();
    }


    public byte[] prepareExitBody(Element[][] body,String entryDateTime, String exitDateTime,
                                  String parkingFees, String nightChargeDesc,
                                  String regNumber,String durationDescription,
                                  int vehicleType){

        String bodyToPrint = "";


        for(int line=0 ; line < Constants.PRINTER_MAX_EXIT_BODY_ELEMENTS; line++) {
            String lineContent = new String();
            for (int elements = 0; elements < Constants.PRINTER_MAX_EXIT_BODY_SUB_ELEMENTS;
                 elements++) {
                Element element = body[line][elements];
                boolean isParkingPrepaid = (Boolean)Cache.getFromCache(mContext,
                                                     Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY,
                                                     Constants.TYPE_BOOL);


                    int key = element.getKey();

                switch(key){
                    case Constants.PRINTER_KEY_ENTRY_DATE_TIME: {
                        String valueToPrint = "In Time:  " + entryDateTime + "\n";

                        lineContent += prepareElement(element, valueToPrint, lineContent.length());
                    }
                    break;
                    case Constants.PRINTER_KEY_PARKING_RATES:{

                        if(isParkingPrepaid) {

                            String valueToPrint = "";
                            String toPrint = "";
                            if (vehicleType == Constants.VEHICLE_TYPE_CAR) {
                                valueToPrint = getDynamicInfoToPrint(
                                        Constants.PRINTER_KEY_CAR_PARKING_RATES);
                            }else if(vehicleType == Constants.VEHICLE_TYPE_BIKE){
                                valueToPrint = getDynamicInfoToPrint(
                                        Constants.PRINTER_KEY_BIKE_PARKING_RATES);
                            }else if(vehicleType == Constants.VEHICLE_TYPE_MINIBUS){
                                valueToPrint = getDynamicInfoToPrint(
                                        Constants.PRINTER_KEY_MINIBUS_PARKING_RATES);
                            }else if(vehicleType == Constants.VEHICLE_TYPE_BUS){
                                valueToPrint = getDynamicInfoToPrint(
                                        Constants.PRINTER_KEY_BUS_PARKING_RATES);
                            }

                            if (valueToPrint.indexOf(Constants.TIER_PRICE_SEPERATOR) >= 0) {
                            /*String returnedValue = valueToPrint.replace('X',',');
                            returnedValue = returnedValue.replaceAll("hours","hrs");
                            toPrint = "\n  Pricing:" + mFormatter.horizontal_Tab()+
                                       returnedValue.substring(0,returnedValue.length()-1);*/
                                String[] tieredPricing = valueToPrint.split("X");

                                toPrint = "  ------------------------\n";
                                toPrint += "        Pricing \n";

                                for (int i = 0; i < tieredPricing.length; i++) {
                                    toPrint += "  " + tieredPricing[i] + "\n";
                                }


                            } else {
                                toPrint = "\n  Pricing: " + mFormatter.horizontal_Tab() + valueToPrint;
                                //lineContent += mPriceListTemplate;
                            }


                            lineContent += prepareElement(element, toPrint,
                                    lineContent.length());
                        }else{
                            lineContent = "";
                        }

                            //lineContent += "\n";
                    }

                    break;

                    case Constants.PRINTER_KEY_NIGHT_CHARGE:{
                        if(nightChargeDesc.isEmpty()){
                            continue;
                        }
                        lineContent += prepareElement(element, nightChargeDesc,
                                                      lineContent.length());
                        lineContent += "\n";

                    }
                    break;

                    case Constants.PRINTER_KEY_EXIT_DATE_TIME: {
                        if(exitDateTime.length() == 0){
                            continue;
                        }
                        String valueToPrint = "Out Time: " + exitDateTime + "\n";

                        lineContent += prepareElement(element, valueToPrint, lineContent.length());
                    }
                    break;
                    case Constants.PRINTER_KEY_DURATION: {
                        if(durationDescription.length() == 0){
                            continue;
                        }
                        String valueToPrint = "Duration: " + durationDescription + "\n";
                        lineContent += prepareElement(element, valueToPrint, lineContent.length());
                    }
                    break;
                    case Constants.PRINTER_KEY_REG_NUMBER: {
                        String valueToPrint = "";
                        if(vehicleType == Constants.VEHICLE_TYPE_CAR) {
                            valueToPrint = "Car Number: " + regNumber + "\n";
                        }else if (vehicleType == Constants.VEHICLE_TYPE_BIKE){
                            valueToPrint = "Bike Number: " + regNumber + "\n";
                        }else if (vehicleType == Constants.VEHICLE_TYPE_MINIBUS){
                            valueToPrint = "MiniBus Number: " + regNumber + "\n";
                        }else if (vehicleType == Constants.VEHICLE_TYPE_BUS){
                            valueToPrint = "Bus Number: " + regNumber + "\n";
                        }
                        lineContent += prepareElement(element, valueToPrint, lineContent.length());
                    }
                    break;

                    case Constants.PRINTER_KEY_PARKING_FEES:
                        String valueToPrint = "  Rs. " + parkingFees + "\n";
                        lineContent += prepareElement(element,valueToPrint,lineContent.length());
                        break;

                }


            }
            //lineContent += "\n";
            if(!lineContent.isEmpty()) {
                bodyToPrint += lineContent;
            }

        }

        return bodyToPrint.getBytes();
    }

    public byte[] prepareReportBody(Element[][] body,String reportDate,TransactionSummary summary){

        String bodyToPrint = "";

        boolean isMiniBusPricingAvailable = (Boolean)Cache.getFromCache(mContext,
                                           Constants.CACHE_PREFIX_IS_MINIBUS_PRICING_AVAILABLE_QUERY,
                                           Constants.TYPE_BOOL);

        boolean isBusPricingAvailable = (Boolean)Cache.getFromCache(mContext,
                Constants.CACHE_PREFIX_IS_BUS_PRICING_AVAILABLE_QUERY,
                Constants.TYPE_BOOL);

        boolean isGateWiseEarningEnabled = (Boolean)Cache.getFromCache(mContext,
                Constants.CACHE_PREFIX_IS_GATEWISE_REPORT_REQUIRED_QUERY,Constants.TYPE_BOOL);


        for(int line=0 ; line < Constants.PRINTER_MAX_REPORT_BODY_ELEMENTS; line++) {
            String lineContent = new String();
            boolean addNewLine = true;
            for (int elements = 0; elements < Constants.PRINTER_MAX_REPORT_BODY_SUB_ELEMENTS;
                 elements++) {
                Element element = body[line][elements];

                int key = element.getKey();

                switch(key){
                    case Constants.PRINTER_KEY_REPORT_DATE:{
                        String valueToPrint = "\nReport Period: " + reportDate;
                        lineContent += prepareElement(element, valueToPrint, lineContent.length());
                    }
                    break;
                    case Constants.PRINTER_KEY_REPORT_CARS_PARKED:{
                        String valueToPrint = " Cars         :   " + summary.getNumCarsParked();
                        lineContent += prepareElement(element, valueToPrint,
                                                      lineContent.length());

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_MONTHLY_PASS_CARS_PARKED:{
                        String valueToPrint = " Cars(by Pass):   " +
                                                   summary.getNumPassCarsParked();
                        lineContent += prepareElement(element, valueToPrint,
                                lineContent.length());

                    }
                    break;


                    case Constants.PRINTER_KEY_REPORT_BIKES_PARKED:{
                        String valueToPrint = " Bikes        :   " + summary.getNumBikesParked();
                        lineContent += prepareElement(element, valueToPrint,
                                lineContent.length());

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_MONTHLY_PASS_BIKES_PARKED:{
                            String valueToPrint = " Bikes(by Pass):  "
                                               + summary.getNumPassBikesParked();
                            lineContent += prepareElement(element, valueToPrint,
                                    lineContent.length());

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_MINIBUS_PARKED:{
                        if(!isMiniBusPricingAvailable){
                            addNewLine = false;
                            continue;
                        }
                        String valueToPrint = " MiniBus      :   " + summary.getNumMiniBusParked();
                        lineContent += prepareElement(element, valueToPrint,
                                lineContent.length());

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_MONTHLY_PASS_MINIBUS_PARKED:{
                        if(!isMiniBusPricingAvailable){
                            addNewLine = false;
                            continue;
                        }
                        String valueToPrint = " MiniBus(by Pass):  "
                                + summary.getNumPassMiniBusParked();
                        lineContent += prepareElement(element, valueToPrint,
                                lineContent.length());

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_BUS_PARKED:{
                        if(!isBusPricingAvailable){
                            addNewLine = false;
                            continue;
                        }
                        String valueToPrint = " Bus      :   " + summary.getNumBusesParked();
                        lineContent += prepareElement(element, valueToPrint,
                                lineContent.length());

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_MONTHLY_PASS_BUS_PARKED:{
                        if(!isBusPricingAvailable){
                            addNewLine = false;
                            continue;
                        }
                        String valueToPrint = " Bus(by Pass):  "
                                + summary.getNumPassBusesParked();
                        lineContent += prepareElement(element, valueToPrint,
                                lineContent.length());

                    }
                    break;



                    case Constants.PRINTER_KEY_REPORT_TOTAL_EARNINGS:{
                        String valueToPrint = "\nTotal Earnings:      " + summary.getEarnings();
                        lineContent += prepareElement(element, valueToPrint,
                                lineContent.length());

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_CAR_EARNINGS:{
                        String valueToPrint = " Cars         :   " + summary.getCarEarnings();
                        lineContent += prepareElement(element, valueToPrint,
                                lineContent.length());
                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_BIKE_EARNINGS:{
                        String valueToPrint = " Bikes        :   " + summary.getBikeEarnings();
                        lineContent += prepareElement(element, valueToPrint,
                                lineContent.length());

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_MONTHLY_PASS_EARNINGS:{
                        String valueToPrint = " Passes       :   " + summary.getPassEarnings();
                        lineContent += prepareElement(element, valueToPrint,
                                lineContent.length());

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_MINIBUS_EARNINGS:{
                        if(!isMiniBusPricingAvailable){
                            addNewLine = false;
                            continue;
                        }
                        String valueToPrint = " MiniBus     :   " + summary.getMiniBusEarnings();
                        lineContent += prepareElement(element, valueToPrint,
                                lineContent.length());

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_BUS_EARNINGS:{
                        if(!isBusPricingAvailable){
                            addNewLine = false;
                            continue;
                        }
                        String valueToPrint = " Bus     :   " + summary.getMiniBusEarnings();
                        lineContent += prepareElement(element, valueToPrint,
                                lineContent.length());

                    }
                    break;



                    case Constants.PRINTER_KEY_REPORT_TOTAL_VEHICLES_PARKED:{
                        String valueToPrint = "\nVehicles Exited:     " +
                                                                summary.getTotalVehiclesParked();
                        lineContent += prepareElement(element, valueToPrint,
                                lineContent.length());

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_TOTAL_MISSING_EXITS:{
                        if(summary.getIncompleteExits() > 0){
                            String valueToPrint = "\nMissing Exits:     " +
                                                   summary.getIncompleteExits();
                            lineContent +=  prepareElement(element,
                                                           valueToPrint,
                                                           lineContent.length());
                        }else{
                            addNewLine = false;
                        }

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_TOTAL_CAR_MISSING_EXITS:{
                        if(summary.getIncompleteExits() > 0){
                            String valueToPrint = " Cars         :   " + summary.getIncompleteCarExits();
                            lineContent +=  prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());

                        }else{
                            addNewLine = false;
                        }

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_CAR_MISSING_EXITS:{
                        if(summary.getIncompleteExits() > 0){
                            String valueToPrint = " Cars(by Pass):  "
                                         + summary.getIncompletePassCarExits();
                            lineContent +=  prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());

                        }else{
                            addNewLine = false;
                        }

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_TOTAL_BIKE_MISSING_EXITS:{
                        if(summary.getIncompleteExits() > 0){
                            String valueToPrint = " Bikes         :  " +
                                                            summary.getIncompleteBikeExits();
                            lineContent +=  prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());

                        }else{
                            addNewLine = false;
                        }

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_BIKE_MISSING_EXITS:{
                        if(summary.getIncompleteExits() > 0){
                            String valueToPrint = " Bikes(Monthly Pass):  " +
                                    summary.getIncompletePassBikeExits();
                            lineContent +=  prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());

                        }else{
                            addNewLine = false;
                        }

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_TOTAL_MINIBUS_MISSING_EXITS:{
                        if(summary.getIncompleteExits() > 0 && isMiniBusPricingAvailable){
                            String valueToPrint = " MiniBus         :  " +
                                    summary.getIncompleteMiniBusExits();
                            lineContent +=  prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());

                        }else{
                            addNewLine = false;
                        }

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_MINIBUS_MISSING_EXITS:{
                        if(summary.getIncompleteExits() > 0 && isMiniBusPricingAvailable){
                            String valueToPrint = " MiniBus(Monthly Pass):  " +
                                    summary.getIncompletePassMiniBusExits();
                            lineContent +=  prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());

                        }else{
                            addNewLine = false;
                        }

                    }
                    break;


                    case Constants.PRINTER_KEY_REPORT_TOTAL_BUS_MISSING_EXITS:{
                        if(summary.getIncompleteExits() > 0 && isBusPricingAvailable){
                            String valueToPrint = " Bus         :  " +
                                    summary.getIncompleteBusExits();
                            lineContent +=  prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());

                        }else{
                            addNewLine = false;
                        }

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_BUS_MISSING_EXITS:{
                        if(summary.getIncompleteExits() > 0 && isBusPricingAvailable){
                            String valueToPrint = " Bus(Monthly Pass):  " +
                                    summary.getIncompletePassBusExits();
                            lineContent +=  prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());

                        }else{
                            addNewLine = false;
                        }

                    }
                    break;



                    case Constants.PRINTER_KEY_REPORT_TOTAL_VEHICLES_INSIDE:{
                        if(summary.getNumVehiclesInside() > 0){
                            String valueToPrint = "\nVehicles Inside:     " +
                                    summary.getNumVehiclesInside();
                            lineContent +=  prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());
                        }

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_TOTAL_CARS_INSIDE:{
                        if(summary.getNumVehiclesInside() > 0){
                            String valueToPrint = " Cars         :   " + summary.getNumCarsInside();
                            lineContent +=  prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());

                        }else{
                            addNewLine = false;
                        }

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_CARS_INSIDE:{
                        if(summary.getNumVehiclesInside() > 0){
                            String valueToPrint = " Cars(by Pass):   " +
                                                summary.getNumPassCarsInside();
                            lineContent +=  prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());

                        }else{
                            addNewLine = false;
                        }

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_TOTAL_BIKES_INSIDE:{
                        if(summary.getNumVehiclesInside() > 0){
                            String valueToPrint = " Bikes        :   " +
                                    summary.getNumBikesInside();
                            lineContent +=  prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());

                        }else{
                            addNewLine = false;
                        }

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_BIKES_INSIDE:{
                        if(summary.getNumVehiclesInside() > 0){
                            String valueToPrint = " Bikes(by Pass):  " +
                                    summary.getNumPassBikesInside();
                            lineContent +=  prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());

                        }else{
                            addNewLine = false;
                        }

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_TOTAL_MINIBUS_INSIDE:{
                        if(summary.getNumVehiclesInside() > 0 && isMiniBusPricingAvailable){
                            String valueToPrint = " MiniBus        :   " +
                                    summary.getNumMiniBusesInside();
                            lineContent +=  prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());

                        }else{
                            addNewLine = false;
                        }

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_MINIBUS_INSIDE:{
                        if(summary.getNumVehiclesInside() > 0 && isMiniBusPricingAvailable){
                            String valueToPrint = " MiniBus(by Pass):  " +
                                    summary.getNumPassMiniBusesInside();
                            lineContent +=  prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());

                        }else{
                            addNewLine = false;
                        }

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_TOTAL_BUS_INSIDE:{
                        if(summary.getNumVehiclesInside() > 0 && isBusPricingAvailable){
                            String valueToPrint = " Bus        :   " +
                                    summary.getNumBusesInside();
                            lineContent +=  prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());

                        }else{
                            addNewLine = false;
                        }

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_BUS_INSIDE:{
                        if(summary.getNumVehiclesInside() > 0 && isBusPricingAvailable){
                            String valueToPrint = " Bus(by Pass):  " +
                                    summary.getNumPassBusesInside();
                            lineContent +=  prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());

                        }else{
                            addNewLine = false;
                        }

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_ISSUED:{
                        if(summary.getNumPassesIssued() > 0){
                            String valueToPrint = "\nPasses Issued:       " +
                                    summary.getNumPassesIssued();
                            lineContent +=  prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());

                        }
                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_TOTAL_CAR_MONTHLY_PASS_ISSUED:{
                        if(summary.getNumPassesIssued() > 0){
                            String valueToPrint = " Cars         :  " +
                                    summary.getNumCarPassesIssued();
                            lineContent +=  prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());

                        }else{
                            addNewLine = false;
                        }
                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_TOTAL_BIKE_MONTHLY_PASS_ISSUED:{
                        if(summary.getNumPassesIssued() > 0){
                            String valueToPrint = " Bikes        :  " +
                                    summary.getNumBikesPassesIssued();
                            lineContent +=  prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());

                        }else{
                            addNewLine = false;
                        }
                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_TOTAL_MINIBUS_MONTHLY_PASS_ISSUED:{
                        if(summary.getNumPassesIssued() > 0 && isMiniBusPricingAvailable){
                            String valueToPrint = " MiniBus        :  " +
                                    summary.getNumMiniBusPassesIssued();
                            lineContent +=  prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());

                        }else{
                            addNewLine = false;
                        }
                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_TOTAL_BUS_MONTHLY_PASS_ISSUED:{
                        if(summary.getNumPassesIssued() > 0 && isBusPricingAvailable){
                            String valueToPrint = " Bus        :  " +
                                    summary.getNumBusPassesIssued();
                            lineContent +=  prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());

                        }else{
                            addNewLine = false;
                        }
                    }
                    break;




                    case Constants.PRINTER_KEY_REPORT_GENERATION_TIME:{
                        String valueToPrint = "\nPrinted on: " +
                              getDynamicInfoToPrint(Constants.PRINTER_KEY_REPORT_GENERATION_TIME);

                        lineContent +=  prepareElement(element,
                                valueToPrint,
                                lineContent.length());

                    }
                    break;

                    //Gatewise print

                    case Constants.PRINTER_KEY_REPORT_GATEWISE_EARNING:{
                        String earnings = summary.getPerOperatorEarningPrintString();

                        if(!earnings.isEmpty()) {
                            String valueToPrint = "\n" + earnings;

                            lineContent += prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());
                        }else{
                            addNewLine = false;
                        }

                    }
                    break;

                    case Constants.PRINTER_KEY_REPORT_GATEWISE_PASS_EARNING:{
                        String earnings = summary.getPerOperatorPassEarningPrintString();

                        if(!earnings.isEmpty()) {
                            String valueToPrint = "\n" + earnings;

                            lineContent += prepareElement(element,
                                    valueToPrint,
                                    lineContent.length());
                        }else{
                            addNewLine = false;
                        }

                    }
                    break;


                }


            }
            if(addNewLine) {
                lineContent += "\n";
            }
            bodyToPrint += lineContent;

        }



        return bodyToPrint.getBytes();
    }


    public byte[] prepareMonthlyPassBody(Element[][] body, String vehicleNumber, int vehicleType,
                                         String issuedTo, String email, String tel,
                                         String validFrom,String validTill,
                                         String transId,int monthlyFees){
        String bodyToPrint = "";


        for(int line=0 ; line < Constants.PRINTER_MAX_MONTHLY_PASS_BODY_ELEMENTS; line++) {
            String lineContent = new String();
            for (int elements = 0; elements < Constants.PRINTER_MAX_MONTHLY_PASS_BODY_SUB_ELEMENTS;
                 elements++) {
                Element element = body[line][elements];

                int key = element.getKey();

                switch(key){
                    case Constants.PRINTER_KEY_REG_NUMBER:{
                        String valueToPrint = "";
                        if(vehicleType == Constants.VEHICLE_TYPE_CAR){
                            valueToPrint = "Car Number: " + vehicleNumber;
                        }else if(vehicleType == Constants.VEHICLE_TYPE_BIKE){
                            valueToPrint = "Bike Number: " + vehicleNumber;
                        }else if(vehicleType == Constants.VEHICLE_TYPE_MINIBUS){
                            valueToPrint = "MiniBus Number: " + vehicleNumber;
                        }else if(vehicleType == Constants.VEHICLE_TYPE_BUS){
                            valueToPrint = "Bus Number: " + vehicleNumber;
                        }

                        lineContent += prepareElement(element, valueToPrint, lineContent.length());
                    }
                    break;

                    case Constants.PRINTER_KEY_DOTTED_LINE:{
                        String valueToPrint = getDynamicInfoToPrint(key);
                        lineContent += prepareElement(element, valueToPrint, lineContent.length());
                    }
                    break;

                    case Constants.PRINTER_KEY_MONTHLY_PARKING_RATES_INCL_NIGHT_CHARGE:{
                        switch(vehicleType){
                            case Constants.VEHICLE_TYPE_CAR:{
                                key = Constants.MONTHLY_PARKING_CAR_RATES_INCL_NIGHT_CHARGE;
                            }
                            break;
                            case Constants.VEHICLE_TYPE_BIKE:{
                                key = Constants.MONTHLY_PARKING_BIKE_RATES_INCL_NIGHT_CHARGE;
                            }
                            break;
                            case Constants.VEHICLE_TYPE_MINIBUS:{
                                key = Constants.MONTHLY_PARKING_MINIBUS_RATES_INCL_NIGHT_CHARGE;
                            }
                            break;
                            case Constants.VEHICLE_TYPE_BUS:{
                                key = Constants.MONTHLY_PARKING_BUS_RATES_INCL_NIGHT_CHARGE;

                            }
                            break;
                        }


                        String valueToPrint = getDynamicInfoToPrint(key);
                        lineContent += prepareElement(element, valueToPrint, lineContent.length());
                    }
                    break;

                    case Constants.PRINTER_KEY_MONTHLY_PASS_ISSUED_TO:{
                        String valueToPrint = "Issued To: " + issuedTo;
                        lineContent += prepareElement(element, valueToPrint, lineContent.length());

                    }
                    break;

                    case Constants.PRINTER_KEY_MONTHLY_PASS_EMAIL : {
                        continue;
                        /*
                        String valueToPrint = "("+email+")";
                        lineContent += prepareElement(element, valueToPrint, lineContent.length());
                        */
                    }


                    case Constants.PRINTER_KEY_MONTHLY_PASS_TEL : {
                        String valueToPrint = "("+tel+")";
                        lineContent += prepareElement(element, valueToPrint, lineContent.length());
                    }
                    break;

                    case Constants.PRINTER_KEY_MONTHLY_PASS_VALID_FROM : {
                        String valueToPrint = "\nValid from: " + validFrom;
                        lineContent += prepareElement(element, valueToPrint, lineContent.length());


                    }
                    break;

                    case Constants.PRINTER_KEY_MONTHLY_PASS_VALID_TILL : {
                        String valueToPrint = "Valid till: " + validTill;
                        lineContent += prepareElement(element, valueToPrint, lineContent.length());

                    }
                    break;

                    case Constants.PRINTER_KEY_MONTHLY_PASS_TRANS_ID : {
                        String valueToPrint = "\nTrans ID: " + transId;
                        lineContent += prepareElement(element, valueToPrint, lineContent.length());

                    }
                    break;

                    case Constants.PRINTER_KEY_MONTHLY_PASS_FEES : {
                        String valueToPrint = "\nPer Month Fees: " + monthlyFees + "\n";

                        lineContent += prepareElement(element, valueToPrint, lineContent.length());

                    }
                    break;




                }


            }
            lineContent += "\n";
            bodyToPrint += lineContent;

        }


        return bodyToPrint.getBytes();
    }


    public byte[] prepareMonthlyPassReceiptBody(Element[][] body, int amountReceived,String transId,
                                                String nextPaymentOn){

        String bodyToPrint = "";


        for(int line=0 ; line < Constants.PRINTER_MAX_MONTHLY_PASS_BODY_ELEMENTS; line++) {
            String lineContent = new String();
            for (int elements = 0; elements < Constants.PRINTER_MAX_MONTHLY_PASS_BODY_SUB_ELEMENTS;
                 elements++) {
                Element element = body[line][elements];

                int key = element.getKey();

                switch(key){
                    case Constants.PRINTER_KEY_MONTHLY_PASS_RECEIPT_AMOUNT_RECEIVED:{
                        String valueToPrint = "";
                        valueToPrint = "Amount Received: " + amountReceived;
                        lineContent += prepareElement(element, valueToPrint, lineContent.length());
                    }
                    break;

                    case Constants.PRINTER_KEY_MONTHLY_PASS_RECEIPT_TRANS_ID:{
                        String valueToPrint = "";
                        valueToPrint = "Trans ID: " + transId;
                        lineContent += prepareElement(element, valueToPrint, lineContent.length());
                    }

                    case Constants.PRINTER_KEY_MONTHLY_PASS_RECEIPT_NEXT_PAYMENT_DUE:{
                        String valueToPrint = "";
                        valueToPrint = "Next Payment due on: " + nextPaymentOn;
                        lineContent += prepareElement(element, valueToPrint, lineContent.length());
                    }

                 }


            }
            lineContent += "\n";
            bodyToPrint += lineContent;

        }

        return bodyToPrint.getBytes();

    }






    /**********************************************************************************************
     *
     *       Template related Functions
     *
     **********************************************************************************************/

    public byte[] getEntryTicketHeaderTemplate(Element[][] header){
        String headerTemplate = "";
        byte[] logo = {};



        for(int line=0 ; line < Constants.PRINTER_MAX_HEADER_ELEMENTS; line++) {
            String lineContent = new String();
            boolean addNewLine = true;
            for (int elements = 0; elements < Constants.PRINTER_MAX_HEADER_SUB_ELEMENTS;
                 elements++) {
                Element element = header[line][elements];
                String valueToPrint = Constants.PRINTER_VALUE_PER_KEY[element.getKey()];
                int key = element.getKey();

                if(valueToPrint.equals(Constants.PRINTER_DYNAMIC_INFO)){
                    valueToPrint = getDynamicInfoToPrint(key);
                    if(valueToPrint.isEmpty()){
                        addNewLine = false;
                        continue;
                    }

                    lineContent += prepareElement(element,valueToPrint,lineContent.length());

                }else {

                    lineContent += prepareElement(element,
                            Constants.PRINTER_VALUE_PER_KEY[element.getKey()],lineContent.length());
                }

            }
            if(addNewLine) {
                lineContent += "\n";
            }
            headerTemplate += lineContent;
        }


        headerTemplate += "\n";
        return headerTemplate.getBytes();

    }

    public byte[] getExitTicketHeaderTemplate(Element[][] header){
        String headerTemplate = "";
        byte[] logo = {};



        for(int line=0 ; line < Constants.PRINTER_MAX_EXIT_HEADER_ELEMENTS; line++) {
            String lineContent = new String();
            boolean addNewLine = true;
            for (int elements = 0; elements < Constants.PRINTER_MAX_EXIT_HEADER_SUB_ELEMENTS;
                 elements++) {
                Element element = header[line][elements];
                String valueToPrint = Constants.PRINTER_VALUE_PER_KEY[element.getKey()];
                int key = element.getKey();

                if(valueToPrint.equals(Constants.PRINTER_DYNAMIC_INFO)){
                    valueToPrint = getDynamicInfoToPrint(key);
                    if(valueToPrint.isEmpty()){
                        addNewLine = false;
                        continue;
                    }
                    lineContent += prepareElement(element,valueToPrint,lineContent.length());

                }else {

                    lineContent += prepareElement(element,
                            Constants.PRINTER_VALUE_PER_KEY[element.getKey()],lineContent.length());
                }

            }
            if(addNewLine) {
                lineContent += "\n";
            }
            headerTemplate += lineContent;
        }

        headerTemplate += "\n";




        return headerTemplate.getBytes();

    }



    public byte[] getEntryTicketFooterTemplate(Element[][] footer){

        String footerTemplate = "";

        for(int line=0 ; line < Constants.PRINTER_MAX_ENTRY_FOOTER_ELEMENTS; line++){
            String lineContent  = new String();
            for(int elements = 0; elements < Constants.PRINTER_MAX_ENTRY_FOOTER_SUB_ELEMENTS ;
                elements++){

                Element element = footer[line][elements];

                String valueToPrint = Constants.PRINTER_VALUE_PER_KEY[element.getKey()];
                int key = element.getKey();

                if(valueToPrint.equals(Constants.PRINTER_DYNAMIC_INFO)){
                    valueToPrint = getDynamicInfoToPrint(key);
                    if(valueToPrint.isEmpty()){
                        continue;
                    }
                    lineContent += prepareElement(element,valueToPrint,lineContent.length());

                }else {

                    lineContent += prepareElement(element,
                            Constants.PRINTER_VALUE_PER_KEY[element.getKey()],lineContent.length());
                }


            }
            lineContent += "\n";

            footerTemplate += lineContent;
        }

        footerTemplate += "\n";
        footerTemplate += "\n";

        return footerTemplate.getBytes();
    }

    public byte[] getExitTicketFooterTemplate(Element[][] footer){

        String footerTemplate = "";

        for(int line=0 ; line < Constants.PRINTER_MAX_EXIT_FOOTER_ELEMENTS; line++){
            String lineContent  = new String();
            for(int elements = 0; elements < Constants.PRINTER_MAX_EXIT_FOOTER_SUB_ELEMENTS ;
                elements++){

                Element element = footer[line][elements];
                int key = element.getKey();
                String valueToPrint = Constants.PRINTER_VALUE_PER_KEY[key];


                if(key == Constants.PRINTER_NEWLINE){
                    break;
                }

                if(valueToPrint.equals(Constants.PRINTER_DYNAMIC_INFO)){
                    valueToPrint = getDynamicInfoToPrint(key);
                    if(valueToPrint.isEmpty()){
                        continue;
                    }
                    lineContent += prepareElement(element,valueToPrint,lineContent.length());

                }else {

                    lineContent += prepareElement(element,
                            Constants.PRINTER_VALUE_PER_KEY[element.getKey()],lineContent.length());
                }


            }
            lineContent += "\n";

            footerTemplate += lineContent;
        }

        footerTemplate += "\n";
        footerTemplate += "\n";


        return footerTemplate.getBytes();
    }


    public byte[] getReportHeaderTemplate(Element[][] header){
        String headerTemplate = "";
        byte[] logo = {};



        for(int line=0 ; line < Constants.PRINTER_MAX_REPORT_HEADER_ELEMENTS; line++) {
            String lineContent = new String();
            for (int elements = 0; elements < Constants.PRINTER_MAX_REPORT_HEADER_SUB_ELEMENTS;
                 elements++) {
                Element element = header[line][elements];
                String valueToPrint = Constants.PRINTER_VALUE_PER_KEY[element.getKey()];
                int key = element.getKey();

                if(valueToPrint.equals(Constants.PRINTER_DYNAMIC_INFO)){
                    valueToPrint = getDynamicInfoToPrint(key);
                    lineContent += prepareElement(element,valueToPrint,lineContent.length());

                }else {

                    lineContent += prepareElement(element,
                            Constants.PRINTER_VALUE_PER_KEY[element.getKey()],lineContent.length());
                }

            }
            lineContent += "\n";
            headerTemplate += lineContent;
        }



        return headerTemplate.getBytes();
    }

    public byte[] getReportFooterTemplate(Element[][] footer){
        String footerTemplate = "";

        for(int line=0 ; line < Constants.PRINTER_MAX_REPORT_FOOTER_ELEMENTS; line++){
            String lineContent  = new String();
            for(int elements = 0; elements < Constants.PRINTER_MAX_REPORT_FOOTER_SUB_ELEMENTS ;
                elements++){

                Element element = footer[line][elements];
                int key = element.getKey();
                String valueToPrint = Constants.PRINTER_VALUE_PER_KEY[key];

                if(key == Constants.PRINTER_NEWLINE){
                    break;
                }


                if(valueToPrint.equals(Constants.PRINTER_DYNAMIC_INFO)){
                    valueToPrint = getDynamicInfoToPrint(key);
                    lineContent += prepareElement(element,valueToPrint,lineContent.length());

                }else {

                    lineContent += prepareElement(element,
                            Constants.PRINTER_VALUE_PER_KEY[element.getKey()],lineContent.length());
                }



            }
            lineContent += "\n";

            footerTemplate += lineContent;
        }

        footerTemplate += "\n";
        footerTemplate += "\n";


        return footerTemplate.getBytes();
    }

    public byte[] getMonthlyPassHeaderTemplate(Element[][] header){
        String headerTemplate = "";
        byte[] logo = {};



        for(int line=0 ; line < Constants.PRINTER_MAX_MONTHLY_PASS_HEADER_ELEMENTS; line++) {
            String lineContent = new String();
            for (int elements = 0; elements < Constants.PRINTER_MAX_MONTHLY_PASS_HEADER_SUB_ELEMENTS;
                 elements++) {
                Element element = header[line][elements];
                String valueToPrint = Constants.PRINTER_VALUE_PER_KEY[element.getKey()];
                int key = element.getKey();

                if(valueToPrint.equals(Constants.PRINTER_DYNAMIC_INFO)){
                    valueToPrint = getDynamicInfoToPrint(key);
                    lineContent += prepareElement(element,valueToPrint,lineContent.length());

                }else {

                    lineContent += prepareElement(element,
                            Constants.PRINTER_VALUE_PER_KEY[element.getKey()],lineContent.length());
                }

            }
            lineContent += "\n";
            headerTemplate += lineContent;
        }



        return headerTemplate.getBytes();

    }

    public byte[] getMonthlyPassFooterTemplate(Element[][] footer){
        String footerTemplate = "";

        for(int line=0 ; line < Constants.PRINTER_MAX_MONTHLY_PASS_FOOTER_ELEMENTS; line++){
            String lineContent  = new String();
            for(int elements = 0; elements < Constants.PRINTER_MAX_MONTHLY_PASS_FOOTER_SUB_ELEMENTS ;
                elements++){

                Element element = footer[line][elements];
                int key = element.getKey();

                if(key == Constants.PRINTER_NEWLINE){
                    break;
                }

                String valueToPrint = Constants.PRINTER_VALUE_PER_KEY[key];

                /*lineContent += prepareElement(element,
                        Constants.PRINTER_VALUE_PER_KEY[key],lineContent.length());*/
                if(valueToPrint.equals(Constants.PRINTER_DYNAMIC_INFO)){
                    valueToPrint = getDynamicInfoToPrint(key);
                    lineContent += prepareElement(element,valueToPrint,lineContent.length());

                }else {
                    lineContent += prepareElement(element,
                            Constants.PRINTER_VALUE_PER_KEY[element.getKey()],lineContent.length());
                }

            }
            lineContent += "\n";

            footerTemplate += lineContent;
        }

        footerTemplate += "\n";
        footerTemplate += "\n";


        return footerTemplate.getBytes();

    }

    public byte[] getMonthlyPassReceiptHeaderTemplate(Element[][] header){
        String headerTemplate = "";
        byte[] logo = {};


        for(int line=0 ; line < Constants.PRINTER_MAX_MONTHLY_PASS_RECEIPT_HEADER_ELEMENTS; line++) {
            String lineContent = new String();
            for (int elements = 0; elements < Constants.PRINTER_MAX_MONTHLY_PASS_RECEIPT_HEADER_SUB_ELEMENTS;
                 elements++) {
                Element element = header[line][elements];
                String valueToPrint = Constants.PRINTER_VALUE_PER_KEY[element.getKey()];
                int key = element.getKey();

                if(valueToPrint.equals(Constants.PRINTER_DYNAMIC_INFO)){
                    valueToPrint = getDynamicInfoToPrint(key);
                    lineContent += prepareElement(element,valueToPrint,lineContent.length());

                }else {

                    lineContent += prepareElement(element,
                            Constants.PRINTER_VALUE_PER_KEY[element.getKey()],lineContent.length());
                }

            }
            lineContent += "\n";
            headerTemplate += lineContent;
        }



        return headerTemplate.getBytes();

    }

    public byte[] getMonthlyPassReceiptFooterTemplate(Element[][] footer){
        String footerTemplate = "";

        for(int line=0 ; line < Constants.PRINTER_MAX_MONTHLY_PASS_RECEIPT_FOOTER_ELEMENTS; line++){
            String lineContent  = new String();
            for(int elements = 0; elements < Constants.PRINTER_MAX_MONTHLY_PASS_RECEIPT_FOOTER_SUB_ELEMENTS ;
                elements++){

                Element element = footer[line][elements];
                int key = element.getKey();

                if(key == Constants.PRINTER_NEWLINE){
                    break;
                }

                lineContent += prepareElement(element,
                        Constants.PRINTER_VALUE_PER_KEY[key],lineContent.length());

            }
            lineContent += "\n";

            footerTemplate += lineContent;
        }

        footerTemplate += "\n";
        footerTemplate += "\n";


        return footerTemplate.getBytes();

    }





    /**********************************************************************************************
     *
     *       PrepareElements related Functions
     *
     **********************************************************************************************/

    private String prepareElement(Element element,String value,int preExistingElmLength){
        String printerReadyElement = "";
        String firstStage = "";
        String secondStage = "";
        String thirdStage ="";

        boolean skipFontSizeStage = false;
        boolean skipAllStages = false;
        int type = element.getType();


        switch(type){

            case Constants.PRINTER_TYPE_TEXT:{

                if(element.getFontAttribute() == Constants.PRINTER_WIDTH_HEIGHT_DOUBLE){
                    firstStage += mFormatter.font_Double_Height_Width_On();
                }else if(element.getFontAttribute() == Constants.PRINTER_HEIGHT_DOUBLE){
                    firstStage += mFormatter.font_Double_Height_On();
                }


                firstStage += value;
            }
            break;
            case Constants.PRINTER_TYPE_IMG:{
                firstStage = value;
                skipFontSizeStage = true;
            }
            break;
            case Constants.PRINTER_TYPE_BARCODE: {
                firstStage = mFormatter.barcode_Code_128_Alpha_Numerics(value);
                skipFontSizeStage = true;
            }
            break;
            case Constants.PRINTER_TYPE_NEWLINE: {
                firstStage += "\n";
                skipAllStages = true;
                return "";

            }
            case Constants.PRINTER_KEY_LINE: {
                firstStage += value;
                skipAllStages = true;
            }
        }

        if(skipAllStages){
            return firstStage;
        }

        //stage 2 --> align
        secondStage = align(element, firstStage, preExistingElmLength);

        if(type == Constants.PRINTER_TYPE_TEXT) {
            if (element.getFontAttribute() == Constants.PRINTER_WIDTH_HEIGHT_DOUBLE) {
                secondStage += mFormatter.font_Double_Height_Width_Off();

            } else if (element.getFontAttribute() == Constants.PRINTER_HEIGHT_DOUBLE) {
                secondStage += mFormatter.font_Double_Height_Off();
            }


        }

        if(!skipFontSizeStage) {
            //stage 2 --> font and correct size
            thirdStage = applyFontSize(element, secondStage);
            return thirdStage;
        }else {
            return secondStage;
        }

    }

    /* Alignement should not be used if there are multiple elements on the same line.
       as it will produce un-defined results. Use alignment only when 1 element on the line
       needs to be aligned
     */

    private String align(Element element,String value, int preExistingElmLength){
        String alignedValue = new String();

        int len = value.length();
        int maxCharactersOnSingleLine = 0;

        switch(element.getSize()){
            case Constants.PRINTER_SIZE_BIG:
                maxCharactersOnSingleLine = charactersOnLine[COURIER_BIG];
                break;
            case Constants.PRINTER_SIZE_MEDIUM:
                maxCharactersOnSingleLine = charactersOnLine[COURIER_MEDIUM];
                break;
            case Constants.PRINTER_SIZE_SMALL:
                maxCharactersOnSingleLine = charactersOnLine[COURIER_SMALL];
                break;
        }

        if(element.getFontAttribute() == Constants.PRINTER_WIDTH_HEIGHT_DOUBLE){
            maxCharactersOnSingleLine /= 2;
        }

       /* if(element.getFont() == Constants.PRINTER_NEWLINE){
            for(int i=0; i < maxCharactersOnSingleLine; i++){
                alignedValue += Constants.PRINTER_LINE_STR;

            }

        }*/

        switch(element.getAlignment()){
            case Constants.PRINTER_ALIGN_LEFT:
                if(len + Constants.PRINTER_DEFAULT_LEFT_MARGIN <= maxCharactersOnSingleLine) {
                    alignedValue += padString(0,Constants.PRINTER_DEFAULT_LEFT_MARGIN);
                    alignedValue += value;
                }else{
                    return value;
                }
                break;

            case Constants.PRINTER_ALIGN_CENTER:{
                /*
                //center is the index where the center of the value should lie
                int center = maxCharactersOnSingleLine/2;
                int remainingCharsOnLine = maxCharactersOnSingleLine - len;
                if(remainingCharsOnLine >= len){
                    alignedValue += padString(0,center);
                    alignedValue += value;

                }else{
                    alignedValue += padString(0,remainingCharsOnLine/2);
                    alignedValue += value;
                }
                //int remainingCharsOnLine = maxCharactersOnSingleLine - (len + preExistingElmLength);
               */
               int center = maxCharactersOnSingleLine/2;
               int valueCenter = len/2;

                if(len < maxCharactersOnSingleLine){
                    alignedValue += padString(0,center-valueCenter);
                    alignedValue += value;
                }else{
                    alignedValue += value;
                }



            }

            break;

            case Constants.PRINTER_ALIGN_RIGHT:
                if(len + Constants.PRINTER_DEFAULT_RIGHT_MARGIN <= maxCharactersOnSingleLine) {
                    alignedValue += padString(0,maxCharactersOnSingleLine -
                                               (len+Constants.PRINTER_DEFAULT_RIGHT_MARGIN));
                    alignedValue += value;
                }else{
                    return value;
                }

                break;

            case Constants.PRINTER_ALIGN_NO_ALIGN:
                return value;

        }

        return alignedValue;
    }

    private String applyFontSize(Element element,String value){

        switch(element.getFont()){

            case Constants.PRINTER_FONT_COURIER:
                switch(element.getSize()){
                    case Constants.PRINTER_SIZE_BIG:
                        return applyFontSize(COURIER_BIG,value);

                    case Constants.PRINTER_SIZE_MEDIUM:
                        return applyFontSize(COURIER_MEDIUM,value);

                    case Constants.PRINTER_SIZE_SMALL:
                        return applyFontSize(COURIER_SMALL,value);

                    case Constants.PRINTER_SIZE_MEDIUM_BIG:
                        return applyFontSize(COURIER_MEDIUM_BIG,value);
                }
                break;

            case Constants.PRINTER_FONT_SANS_SERIF:
                switch(element.getSize()){
                    case Constants.PRINTER_SIZE_BIG:
                        return applyFontSize(SERIF_BIG,value);

                    case Constants.PRINTER_SIZE_MEDIUM:
                        return applyFontSize(SERIF_MEDIUM,value);

                    case Constants.PRINTER_SIZE_SMALL:
                        return applyFontSize(SERIF_SMALL,value);
                }
                break;

        }
        return applyFontSize(COURIER_MEDIUM, value);
    }

    private String applyFontSize(int size,String value){
        switch(size){
            case COURIER_10:
                return mFormatter.font_Courier_10(value);
            case COURIER_19:
                return mFormatter.font_Courier_19(value);
            case COURIER_20:
                return mFormatter.font_Courier_20(value);
            case COURIER_24:
                return mFormatter.font_Courier_24(value);
            case COURIER_25:
                return mFormatter.font_Courier_25(value);
            case COURIER_27:
                return mFormatter.font_Courier_27(value);
            case COURIER_29:
                return mFormatter.font_Courier_29(value);
            case COURIER_32:
                return mFormatter.font_Courier_32(value);
            case COURIER_34:
                return mFormatter.font_Courier_34(value);
            case COURIER_38:
                return mFormatter.font_Courier_38(value);
            case COURIER_42:
                return mFormatter.font_Courier_42(value);
            case COURIER_48:
                return mFormatter.font_Courier_48(value);

            case SERIF_8:
                return mFormatter.font_SansSerif_8(value);
            case SERIF_32:
                return mFormatter.font_SansSerif_32(value);
            case SERIF_34:
                return mFormatter.font_SansSerif_34(value);
            case SERIF_38:
                return mFormatter.font_SansSerif_38(value);

            default:
                return mFormatter.font_Courier_29(value);
        }
    }


    private String padString(int index, int len){
        String paddedString = new String();

        for(int i =0 ; i < len; i++){
            paddedString += " ";
        }
        return paddedString;

    }

    private String getDynamicInfoToPrint(int key){
        String value = "";
        switch(key){
            case Constants.PRINTER_KEY_HEADING:{
                value = (String)Cache.getFromCache(mContext,
                                                   Constants.CACHE_PREFIX_CUSTOM_SPACE_HEADING,
                                                   Constants.TYPE_STRING);

                if(value.isEmpty()){
                    value = Constants.PRINTER_HEADING;
                }

            }
            break;
            case Constants.PRINTER_KEY_URL:{
                value = (String)Cache.getFromCache(mContext,Constants.CACHE_PREFIX_CUSTOM_URL,
                                                 Constants.TYPE_STRING);

                if(value.isEmpty()){
                    value = Constants.PRINTER_URL_STRING;
                }

            }
            break;
            case Constants.PRINTER_KEY_DESC:{
                value = (String)Cache.getFromCache(mContext,Constants.CACHE_PREFIX_CUSTOM_URL,
                                                   Constants.TYPE_STRING);

                if(value.isEmpty()){
                    value = Constants.PRINTER_DISCLAIMER_STRING;
                }

            }
            break;

            case Constants.PRINTER_KEY_SPACE_ADDRESS: {

                value = (String) Cache.getFromCache(mContext,
                                                    Constants.CACHE_PREFIX_CUSTOM_SPACE_ADDRESS,
                                                    Constants.TYPE_STRING);
                if(value.isEmpty()) {
                    value = (String) Cache.getFromCache(mContext, Constants.CACHE_PREFIX_SPACE_NAME_QUERY,
                            Constants.TYPE_STRING);
                }

                break;
            }

            case Constants.PRINTER_KEY_CAR_PARKING_RATES:{
                value = (String) Cache.getFromCache(mContext,
                                                    Constants.CACHE_PREFIX_CUSTOM_PARKING_RATES,
                                                    Constants.TYPE_STRING);

                if(value.isEmpty()) {
                    value = (String) Cache.getFromCache(mContext,
                            Constants.CACHE_PREFIX_PRINT_PRICING_QUERY + Constants.VEHICLE_TYPE_CAR,
                            Constants.TYPE_STRING);
                }

            }
                break;

            case Constants.PRINTER_KEY_BIKE_PARKING_RATES:{

                value = (String) Cache.getFromCache(mContext,
                        Constants.CACHE_PREFIX_CUSTOM_BIKE_PARKING_RATES,
                        Constants.TYPE_STRING);

                if(value.isEmpty()) {
                    value = (String) Cache.getFromCache(mContext,
                            Constants.CACHE_PREFIX_PRINT_PRICING_QUERY + Constants.VEHICLE_TYPE_BIKE,
                            Constants.TYPE_STRING);
                }

            }
            break;

            case Constants.PRINTER_KEY_MINIBUS_PARKING_RATES:{

                value = (String) Cache.getFromCache(mContext,
                        Constants.CACHE_PREFIX_CUSTOM_MINIBUS_PARKING_RATES,
                        Constants.TYPE_STRING);

                if(value.isEmpty()) {
                    value = (String) Cache.getFromCache(mContext,
                            Constants.CACHE_PREFIX_PRINT_PRICING_QUERY +
                                    Constants.VEHICLE_TYPE_MINIBUS,
                            Constants.TYPE_STRING);
                }

            }
            break;

            case Constants.PRINTER_KEY_BUS_PARKING_RATES:{

                value = (String) Cache.getFromCache(mContext,
                        Constants.CACHE_PREFIX_CUSTOM_BUS_PARKING_RATES,
                        Constants.TYPE_STRING);

                if(value.isEmpty()) {
                    value = (String) Cache.getFromCache(mContext,
                            Constants.CACHE_PREFIX_PRINT_PRICING_QUERY +
                                    Constants.VEHICLE_TYPE_BUS,
                            Constants.TYPE_STRING);
                }

            }
            break;

            case Constants.PRINTER_KEY_OPERATING_HOURS:{
                value = (String)Cache.getFromCache(mContext,
                                Constants.CACHE_PREFIX_CUSTOM_OPERATING_HOURS,Constants.TYPE_STRING);

                if(value.isEmpty()) {
                    if ((Boolean) Cache.getFromCache(mContext,
                            Constants.CACHE_PREFIX_IS_OPEN_24_HOURS,
                            Constants.TYPE_BOOL)) {
                        value = "Timings:" + mFormatter.horizontal_Tab() + "24 Hours open";
                    } else {
                        value = "Timings:" + mFormatter.horizontal_Tab();
                        value += (String) Cache.getFromCache(mContext,
                                Constants.CACHE_PREFIX_OPENING_TIME_QUERY,
                                Constants.TYPE_STRING);
                        value += " to ";
                        value += (String) Cache.getFromCache(mContext,
                                Constants.CACHE_PREFIX_CLOSING_TIME_QUERY,
                                Constants.TYPE_STRING);

                    }
                }

            }
            break;


           case Constants.PRINTER_KEY_REPORT_GENERATION_TIME:{
               DateTime currentDateTime = DateTime.now(Constants.TIME_ZONE);

               value = currentDateTime.format("DD-MMM-YY hh:mm a", Locale.US);

            }
             break;

            case Constants.PRINTER_KEY_LOST_TICKET:{

                value = (String)Cache.getFromCache(mContext,
                        Constants.CACHE_PREFIX_CUSTOM_LOST_TICKET,Constants.TYPE_STRING);

                if(value.isEmpty()) {
                    int lostTicket = (Integer) Cache.getFromCache(mContext,
                            Constants.CACHE_PREFIX_LOST_TICKET_CHARGE,
                            Constants.TYPE_INT);
                    value = "Lost Ticket Penalty : Rs ";

                    if (lostTicket == 0) {
                        value += "50";
                    } else {
                        value += Integer.toString(lostTicket);
                    }
                }
            }
            break;

            case Constants.PRINTER_KEY_CAR_NIGHT_CHARGE:{
                value = (String)Cache.getFromCache(mContext,
                        Constants.CACHE_PREFIX_CUSTOM_NIGHT_CHARGES,Constants.TYPE_STRING);

                if(value.isEmpty()) {
                    int nightCharge = (Integer) Cache.getFromCache(mContext,
                            Constants.CACHE_PREFIX_NIGHT_CHARGE_QUERY, Constants.TYPE_INT);


                    if (nightCharge == 0) {
                        value = "";
                    } else {
                        value = "Night Charge: Rs." + Integer.toString(nightCharge);
                    }
                }
            }
            break;

            case Constants.PRINTER_KEY_BIKE_NIGHT_CHARGE:{
                value = (String)Cache.getFromCache(mContext,
                        Constants.CACHE_PREFIX_CUSTOM_BIKE_NIGHT_CHARGES,Constants.TYPE_STRING);

                if(value.isEmpty()) {
                    int nightCharge = (Integer) Cache.getFromCache(mContext,
                            Constants.CACHE_PREFIX_NIGHT_CHARGE_QUERY, Constants.TYPE_INT);


                    if (nightCharge == 0) {
                        value = "";
                    } else {
                        int ratio = (Integer) Cache.getFromCache(mContext,
                                Constants.CACHE_PREFIX_BIKE_PRICING_RATIO, Constants.TYPE_INT);

                        nightCharge = (nightCharge * ratio) / 100;
                        value = "Night Charge: Rs." + Integer.toString(nightCharge);
                    }
                }
            }
            break;

            case Constants.PRINTER_KEY_AGENCY_NAME:{
                value = (String)Cache.getFromCache(mContext,
                        Constants.CACHE_PREFIX_CUSTOM_AGENCY_NAME,Constants.TYPE_STRING);

                /*if(value.isEmpty()) {

                    String agencyName = (String) Cache.getFromCache(mContext,
                            Constants.CACHE_PREFIX_SPACE_OWNER_NAME_QUERY, Constants.TYPE_STRING);

                    if (!agencyName.isEmpty()) {
                        value = "Agency: " + agencyName;
                    }
                }*/
            }
            break;

            case Constants.PRINTER_KEY_SPACE_OWNER_MOBILE:{
                value = (String)Cache.getFromCache(mContext,
                        Constants.CACHE_PREFIX_CUSTOM_SPACE_OWNER_MOBILE,Constants.TYPE_STRING);

                /*if(value.isEmpty()) {

                    String mobile = (String) Cache.getFromCache(mContext,
                            Constants.CACHE_PREFIX_SPACE_OWNER_MOBILE_QUERY, Constants.TYPE_STRING);

                    if (!mobile.isEmpty()) {
                        value = "Mob: " + mobile;
                    }
                }else if(value.trim().length() == 0){
                    value = ""; //this is an indication from server not to add mobile number
                }*/
            }
            break;

            case Constants.PRINTER_KEY_DOTTED_LINE:{
                value = "---------------------";
            }
            break;

            case Constants.MONTHLY_PARKING_CAR_RATES_INCL_NIGHT_CHARGE:{
                int carMonthlyRate = (Integer)Cache.getFromCache(mContext,
                        Constants.CACHE_PREFIX_MONTHLY_RATE_QUERY + Constants.VEHICLE_TYPE_CAR,
                        Constants.TYPE_INT);

                value = "Car Monthly Pass: Rs." + carMonthlyRate + " pm";
                value += mFormatter.carriage_Return() +
                         "Night Charges (00 to 05): " + mFormatter.carriage_Return()+
                        "Rs." + carMonthlyRate +" pm";

                int nightCharge = (Integer)Cache.getFromCache(mContext,
                        Constants.CACHE_PREFIX_NIGHT_CHARGE_QUERY + Constants.VEHICLE_TYPE_CAR,
                        Constants.TYPE_INT);

                value += ",Rs.";

                if(nightCharge!= 0) {
                    value += nightCharge + "/day";
                }

            }
            break;

            case Constants.MONTHLY_PARKING_BIKE_RATES_INCL_NIGHT_CHARGE:{
                int bikeMonthlyRate = (Integer)Cache.getFromCache(mContext,
                        Constants.CACHE_PREFIX_MONTHLY_RATE_QUERY + Constants.VEHICLE_TYPE_BIKE,
                        Constants.TYPE_INT);

                value = "Bike Monthly Pass: Rs." + bikeMonthlyRate + " pm";
                value += mFormatter.carriage_Return() +
                         "Night Charges (00 to 05): " + mFormatter.carriage_Return() +
                        "Rs. " + bikeMonthlyRate +" pm";
                int nightCharge = (Integer)Cache.getFromCache(mContext,
                        Constants.CACHE_PREFIX_NIGHT_CHARGE_QUERY + Constants.VEHICLE_TYPE_BIKE,
                        Constants.TYPE_INT);


                value += ",Rs.";

                if(nightCharge!= 0) {
                    value += nightCharge + "/day";
                }


            }
            break;

            case Constants.MONTHLY_PARKING_MINIBUS_RATES_INCL_NIGHT_CHARGE:{
                int miniBusMonthlyRate = (Integer)Cache.getFromCache(mContext,
                        Constants.CACHE_PREFIX_MONTHLY_RATE_QUERY + Constants.VEHICLE_TYPE_MINIBUS,
                        Constants.TYPE_INT);

                value = "MiniBus Monthly Pass: Rs." + miniBusMonthlyRate + " pm";
                value += mFormatter.carriage_Return() +
                        "Night Charges (00 to 05): " + mFormatter.carriage_Return() +
                        "Rs. " + miniBusMonthlyRate +" pm";
                int nightCharge = (Integer)Cache.getFromCache(mContext,
                        Constants.CACHE_PREFIX_NIGHT_CHARGE_QUERY + Constants.VEHICLE_TYPE_MINIBUS,
                        Constants.TYPE_INT);


                value += ",Rs.";

                if(nightCharge!= 0) {
                    value += nightCharge + "/day";
                }


            }
            break;

            case Constants.MONTHLY_PARKING_BUS_RATES_INCL_NIGHT_CHARGE:{
                int busMonthlyRate = (Integer)Cache.getFromCache(mContext,
                        Constants.CACHE_PREFIX_MONTHLY_RATE_QUERY + Constants.VEHICLE_TYPE_BUS,
                        Constants.TYPE_INT);

                value = "Bus Monthly Pass: Rs." + busMonthlyRate + " pm";
                value += mFormatter.carriage_Return() +
                        "Night Charges (00 to 05): " + mFormatter.carriage_Return() +
                        "Rs. " + busMonthlyRate +" pm";
                int nightCharge = (Integer)Cache.getFromCache(mContext,
                        Constants.CACHE_PREFIX_NIGHT_CHARGE_QUERY + Constants.VEHICLE_TYPE_BUS,
                        Constants.TYPE_INT);

                value += ",Rs.";

                if(nightCharge!= 0) {
                    value += nightCharge + "/day";
                }


            }
            break;

            case Constants.PRINTER_KEY_DYNAMIC_AD:{
                value = (String)Cache.getFromCache(mContext,
                        Constants.CACHE_PREFIX_CUSTOM_AD_TEXT,Constants.TYPE_STRING);
            }
            break;

            case Constants.PRINTER_KEY_EXIT_SIMPLYPARK_AD:{
                value = (String)Cache.getFromCache(mContext,
                        Constants.CACHE_PREFIX_CUSTOM_EXIT_SIMPLYPARK_AD_TEXT,Constants.TYPE_STRING);

            }
            break;


        }

        return value;
    }



}
