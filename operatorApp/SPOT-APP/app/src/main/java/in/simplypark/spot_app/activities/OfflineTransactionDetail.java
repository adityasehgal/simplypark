package in.simplypark.spot_app.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import in.simplypark.spot_app.R;
import in.simplypark.spot_app.config.Constants;


public class OfflineTransactionDetail extends AppCompatActivity {

    private TextView mParkingIdView;
    private TextView mVehicleRegView;
    private TextView mVehicleTypeView;
    private TextView mEntryTimeView;
    private TextView mExitTimeView;
    private TextView mParkingFeesView;
    private TextView mDurationView;
    private boolean mIsNewRecord;
    private int mVehicleType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_transaction_detail);

        mParkingIdView = (TextView)findViewById(R.id.otd_parking_id);
        mVehicleRegView = (TextView)findViewById(R.id.otd_vehicle_reg_id);
        mVehicleTypeView = (TextView)findViewById(R.id.otd_vehicle_type_id);
        mEntryTimeView = (TextView)findViewById(R.id.otd_entry_time_id);
        mExitTimeView = (TextView)findViewById(R.id.otd_exit_time_id);
        mParkingFeesView = (TextView)findViewById(R.id.otd_parking_fees_id);
        mDurationView = (TextView)findViewById(R.id.otd_duration_id);

        Bundle passedParams = getIntent().getExtras();

        mParkingIdView.setText(passedParams.getString(Constants.BUNDLE_PARAM_PARKING_ID));
        mVehicleRegView.setText(passedParams.getString(Constants.BUNDLE_PARAM_VEHICLE_REG));
        mVehicleType = passedParams.getInt(Constants.BUNDLE_PARAM_VEHICLE_TYPE,
                                              Constants.VEHICLE_TYPE_CAR);
        if(mVehicleType == Constants.VEHICLE_TYPE_CAR) {
            mVehicleTypeView.setText("Car");
        }else{
            mVehicleTypeView.setText("Bike");
        }

        mIsNewRecord = passedParams.getBoolean(Constants.BUNDLE_PARAM_IS_NEW_RECORD,false);

        mEntryTimeView.setText(passedParams.getString(Constants.BUNDLE_PARAM_ENTRY_TIME));
        mExitTimeView.setText(passedParams.getString(Constants.BUNDLE_PARAM_EXIT_TIME));
        int parkingFees = (int)(passedParams.getDouble(Constants.BUNDLE_PARAM_PARKING_FEES));
        mParkingFeesView.setText(Integer.toString(parkingFees));

        mDurationView.setText(passedParams.getString(Constants.BUNDLE_PARAM_DURATION_DESCRIPTION));


    }



    //Cancel Button
    public void handleDoneButton(View view){
        Intent intent = OfflineTransactionDetail.this.getIntent();
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, mVehicleType);
        intent.putExtra(Constants.BUNDLE_PARAM_IS_NEW_RECORD, mIsNewRecord);
        OfflineTransactionDetail.this.setResult(RESULT_OK, intent);
        finish();

    }
}
