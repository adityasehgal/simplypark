package in.simplypark.spot_app.activities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import in.simplypark.spot_app.db_tables.ReusableTokensTbl;
import in.simplypark.spot_app.listelements.SpacesElement;
import in.simplypark.spot_app.R;
import in.simplypark.spot_app.adapters.CustomSpaceListAdapter;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.db_tables.NightChargeTbl;
import in.simplypark.spot_app.db_tables.PenaltyChargeTbl;
import in.simplypark.spot_app.db_tables.PricingPerVehicleTypeTbl;
import in.simplypark.spot_app.db_tables.SpotDb;
import in.simplypark.spot_app.utils.Cache;
import in.simplypark.spot_app.utils.CacheParams;
import in.simplypark.spot_app.utils.MiscUtils;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class SpaceSelection extends AppCompatActivity {

    private View     mLoadingView;
    private ListView mSpaceDetailsView;
    private Context mContext;
    private String mUserId;
    private String mEmail;
    private GetSpacesTask mGetSpacesTask;
    private ArrayList<SpacesElement> mElements;
    private CustomSpaceListAdapter mCustomSpaceListAdapter;
    private SpotDb mDbObj;
    private int mCurrentOccupancy;
    private CacheParams mCacheParams;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEmail =getIntent().getStringExtra(Constants.BUNDLE_PARAM_USER_EMAIL);
        mUserId = getmUserId();
        setContentView(R.layout.activity_space_selection);
        mContext = getApplicationContext();
        mElements = new ArrayList<SpacesElement>();
        mLoadingView      = findViewById(R.id.ass_space_details_loading);
        mSpaceDetailsView = (ListView)findViewById(R.id.ass_space_details);
        mSpaceDetailsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                int mySpaceId = (int)view.getTag();
                selectSpace(mySpaceId,Integer.valueOf(mUserId),mEmail);
                /*Toast.makeText(getApplicationContext(),
                        "Click ListItem Number " + position+" Space ID: "+mySpaceId, Toast.LENGTH_SHORT)
                        .show();*/

            }
        });
        //hide the list view
        showProgress(true);
        mGetSpacesTask =new GetSpacesTask(Integer.valueOf(mUserId));
        mGetSpacesTask.execute();
        //Get UserId from the Cache.
    }
    private String getmUserId(){
        return (String)Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_USER_ID,
                Constants.TYPE_STRING);
    }
    private void selectSpace(int space_id, int userId, String email){
        showProgress(true);
        new GetSpaceDetails(userId,space_id,email).execute();
    }

    private void inflateSpaceList(String spaceListString){

        try {
                JSONArray spaceJSONArray = new JSONArray(spaceListString);
                for(int i = 0; i < spaceJSONArray.length(); i++){
                    JSONObject space = spaceJSONArray.getJSONObject(i);
                    mElements.add(new SpacesElement(space.getInt("spaceId"),
                            space.getString("name"),space.getString("address"),space.getInt("slots")));
                }
            mCustomSpaceListAdapter = new CustomSpaceListAdapter(this,mElements);
            mSpaceDetailsView.setAdapter(mCustomSpaceListAdapter);

        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    private void showProgress(boolean show){
        MiscUtils.showProgress(mContext,show,mLoadingView,mSpaceDetailsView);
    }

    /**
     * Represents an asynchronous GetSpaces Task used for choosing the space
     * this app will be setup for.
     */
    public class GetSpacesTask extends AsyncTask<Void, Void, Boolean> {

        private int mUserId;
        private String mFailureReason;
        private OkHttpClient mClient;

        GetSpacesTask(int userId) {
            mUserId = userId;
            mClient = new OkHttpClient();
            mDbObj = new SpotDb();
            mCacheParams = new CacheParams();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                Request getSpacesRequest = buildGetSpacesRequest(mUserId);

                Response getSpacesResponse = mClient.newCall(getSpacesRequest).execute();

                if (getSpacesResponse.isSuccessful()) {
                    Log.d("RESPONSE", "SPACE DETAILS FOUND");

                    String response = getSpacesResponse.body().string();
                    JSONObject json = new JSONObject(response);
                    int status = json.getInt("status");
                    String reason = json.getString("reason");
                    if (status == 1) {
                        int numSpaces = json.getInt("spaceCount");

                        if (numSpaces == 0) {
                            mFailureReason = "No space listed with this User. Please contact SimplyPark.in";
                            return false;
                        }
                        JSONArray spaceJSONArray = json.getJSONArray("spaces");
                        Cache.setInCache(getApplicationContext(),
                                Constants.CACHE_PREFIX_SPACE_LIST,
                                spaceJSONArray.toString(), Constants.TYPE_STRING);
                        getSpacesResponse.body().close();
                        return true;
                    } else {
                        mFailureReason = "Invalid response received. Please contact SimplyPark.in";
                        getSpacesResponse.body().close();
                        return false;

                    }

                } else {
                    mFailureReason = "Invalid response received. Please contact SimplyPark.in";

                    getSpacesResponse.body().close();
                    return false;
                }

            } catch (IOException | JSONException e) {
                Log.e("SPOT-APP", "EXCEPTION", e);
                mFailureReason = "Unknown error. Please try after sometime" + e.toString();
                return false;
            }


        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mGetSpacesTask = null;
            showProgress(false);

            if (success) {
                String spaceListString = (String) Cache.getFromCache(getApplicationContext(),
                                                                   Constants.CACHE_PREFIX_SPACE_LIST,
                                                                     Constants.TYPE_STRING);
                inflateSpaceList(spaceListString);
            } else {
                MiscUtils.displayNotification(getApplicationContext(),mFailureReason,true);

            }
        }

        @Override
        protected void onCancelled() {
            mGetSpacesTask = null;
            showProgress(false);
        }

        private Request buildGetSpacesRequest(int userId) throws JSONException {
            String url = Constants.SPACE_LIST_URL + userId + ".json";
            return (new Request.Builder()
                    .url(url)
                    .build());


        }
    }

    public class GetSpaceDetails extends AsyncTask<Void, Void, Boolean> {

        private int mUserId;
        private String email;
        private String mFailureReason;
        private OkHttpClient mClient;
        private int spaceId;

        GetSpaceDetails(int userId, int spaceId, String email) {
            this.mUserId = userId;
            this.email = email;
            this.mClient = new OkHttpClient();
            this.spaceId = spaceId;
            mDbObj = new SpotDb();
            mCacheParams = new CacheParams();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                Request getSpacesRequest = buildGetSpacesRequest(mUserId,spaceId);

                Response getSpacesResponse = mClient.newCall(getSpacesRequest).execute();

                if (getSpacesResponse.isSuccessful()) {

                    mDbObj.setOperatorDetails(true, mEmail);
                    if(parseAndStoreSpaceInfoResponse(getSpacesResponse)) {
                        getSpacesResponse.body().close();
                        return true;
                    }else{
                       return false;
                    }
                }else{
                    mFailureReason = "Invalid response received. Please contact SimplyPark.in";
                    return false;
                }


            } catch (IOException | JSONException e) {
                Log.e("SPOT-APP", "EXCEPTION", e);
                mFailureReason = "Unknown error. Please try after sometime" + e.toString();
                return false;
            }


        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mGetSpacesTask = null;
            showProgress(false);
            if (success) {
                Intent intent = new Intent(SpaceSelection.this, SetupActivity.class);
                startActivity(intent);
                finish();
            } else {
                MiscUtils.displayNotification(getApplicationContext(),mFailureReason,true);
            }
        }

        @Override
        protected void onCancelled() {
            mGetSpacesTask = null;
            showProgress(false);
        }

        private Request buildGetSpacesRequest(int userId,int spaceId) throws JSONException {
            String url = Constants.SPACE_INFO_URL + userId + "/"+spaceId+".json";
            return (new Request.Builder()
                    .url(url)
                    .build());


        }

        private boolean parseAndStoreSpaceInfoResponse(Response spaceResponse)
                throws JSONException {
            try {
                String response = spaceResponse.body().string();
                JSONObject json = new JSONObject(response);

                int status = json.getInt("status");
                String reason = json.getString("reason");

                if (status == 1) {

                    mCacheParams.setSpaceName(json.getString("name"));
                    mCacheParams.setSpaceId(json.getInt("spaceId"));
                    mCacheParams.setSlots(json.getInt("slots"));
                    mCacheParams.setOperatorId(json.optInt("operatorId",
                            Constants.INVALID_OPERATOR_ID)); //TODO: change to getInt
                    mCacheParams.setOpeningDateTime(json.getString("openingDateTime"));
                    mCacheParams.setClosingDateTime(json.getString("closingDateTime"));
                    mCacheParams.setOpen24Hours(json.optBoolean("is24HoursOpen", false));
                    mCacheParams.setLostTicketCharge(json.optInt("lostTicketCharge", 0));
                    mCacheParams.setSpaceOwnerName(json.optString("spaceOwnerName"));
                    mCacheParams.setSpaceOwnerMobile(json.optString("spaceOwnerMobile"));
                    mCacheParams.setMultiDayParkingPossible(
                            json.optBoolean("isMultiDayParkingPossible",false));
                    mCacheParams.setGateWiseReportsRequired(
                            json.optBoolean("isGateWiseReportsRequired",false));
                    mCacheParams.setSpaceBikeOnly(json.optBoolean("isSpaceBikeOnly",false));


                    mCacheParams.setParkingPrepaid(json.optBoolean("isParkingPrepaid",false));
                    /*Deault vehicleType this is the default selection value while entering a vehicle.*/
                    mCacheParams.setDefaultVehicleType(json.optString("spaceDefaultVehicleType"));

                    mCacheParams.setCancelUpdateAllowed(json.optBoolean("isCancelUpdateAllowed",true));



                    mCacheParams.setParkAndRideAvailable(
                            json.optBoolean("isParkAndRideAvailable",false));


                    mCacheParams.setNumBikesInSingleSlot(json.optInt("numBikesInSingleSlot", 0));

                    mDbObj.setSpaceDescription(mCacheParams.getSpaceName(), mCacheParams.getSpaceId(),
                            mCacheParams.getSlots());



                    boolean applyCustomTicketFormat = json.optBoolean("applyCustomTicketFormat",
                            false);

                    if(applyCustomTicketFormat){
                        JSONArray customFormat = json.getJSONArray("ticketFormat");
                        for(int i=0; i < customFormat.length(); i++){
                            JSONObject formatObject = customFormat.getJSONObject(i).
                                    getJSONObject("TicketFormat");
                            int ticketType = Integer.parseInt((String)formatObject.get("ticket_type"));
                            switch(ticketType){
                                case Constants.ENTRY_TICKET_CUSTOM_FORMAT:{
                                    setCustomEntryTicketFormat(formatObject);
                                }
                                break;
                            }

                        }

                    }

                    //set NumPrepaid hours
                    if(mCacheParams.isParkingPrepaid()){
                        mCacheParams.setNumHoursPrepaid(json.optInt("numPrepaidHours",0));
                    }


                    //Pricing Info - Make sure nightcharge and prepaid hours are set before
                    //this
                    JSONArray pricing = json.getJSONArray("pricingPerVehicleType");
                    int length = pricing.length();


                    for(int i=0; i < length; i++){
                        JSONObject vehicleTypePricing = pricing.getJSONObject(i);
                        setPricing(vehicleTypePricing);
                    }


                    //for online bookings,is the slot occupied assumed or will the qr code be
                    //scanned
                    mCacheParams.setSlotOccupiedAssumed(
                            json.optBoolean("isSlotOccupiedAssumed",false));

                    boolean reusablePrintTokensUsed = json.optBoolean("reusableTokensUsed",false);


                    mCacheParams.setIsReusablePrintTokenUsed(reusablePrintTokensUsed);

                    if(reusablePrintTokensUsed){
                        setPrintTokens(json.getJSONObject("tokens"));
                    }

                    //for passes generated via app, do we need to activate a qr code based monthly
                    //pass

                    boolean isPassActivationRequired = json.optBoolean("isPassActivationRequired",
                            false);

                    Cache.setInCache(getApplicationContext(),
                            Constants.CACHE_PREFIX_IS_PASS_ACTIVATION_REQUIRED,
                            isPassActivationRequired,Constants.TYPE_BOOL);

                    //The currentOccupancy variable, although, set here is NO longer used by
                    //main activity. Infact, since DB read is slow we are not even reading it
                    //from the DB

                    mCurrentOccupancy = json.getInt("currentOccupancy");
                    mDbObj.setCurrentOccupancy(mCurrentOccupancy);
                    mDbObj.setIsSpaceInfoAvailable(true);

                    mDbObj.save();




                    setCache(mCacheParams); //setup Cache as well

                    //startDay end timer as well
                    /*AlarmHandler.startDayEndTimer(getApplicationContext(),Constants.TIMER_ACTION,
                                                  openingDateTime, closingDateTime);*/

                    return true;

                } else {
                    mFailureReason = reason;
                    return false;
                }


            } catch (IOException | JSONException ioe) {
                Log.e("SPOT-APP", "EXCEPTION", ioe);
                mFailureReason = "Error:: " + ioe;
                return false;


            }
        }

        private void setCache(CacheParams params) {
            Context context = getApplicationContext();

            //is cancellation/update allowed
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_IS_CANCELLATION_UPDATION_ALLOWED,
                    params.isCancelUpdateAllowed(),Constants.TYPE_BOOL);

            //is Logged In
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_LOGIN_QUERY,
                    true,
                    Constants.TYPE_BOOL);

            //spaceId
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_SPACEID_QUERY,
                    params.getSpaceId(),
                    Constants.TYPE_INT);

            //operatorId
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_OPERATOR_QUERY,
                    params.getOperatorId(),
                    Constants.TYPE_INT);

            //slots
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_NUM_SLOTS_QUERY,
                    params.getSlots(),
                    Constants.TYPE_INT);

            //spaceName
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_SPACE_NAME_QUERY,
                    params.getSpaceName(),
                    Constants.TYPE_STRING);

            //is Park And Ride available
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_IS_PARK_AND_RIDE_AVAIL_QUERY,
                    params.isParkAndRideAvailable(),
                    Constants.TYPE_BOOL);

            //set bikes to slot ratio
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_BIKES_TO_SLOT_RATIO,
                    params.getNumBikesInSingleSlot(),
                    Constants.TYPE_INT);
/*
            //bike pricing ratio
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_BIKE_PRICING_RATIO,
                    params.getBikePricingRatio(),Constants.TYPE_INT);
*/
            //openingDateTime
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_OPENING_DATE_TIME_QUERY,
                    params.getOpeningDateTime(),Constants.TYPE_STRING);

            //closingDateTime
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_CLOSING_DATE_TIME_QUERY,
                    params.getClosingDateTime(),Constants.TYPE_STRING);

            //store the opening and closing time (just time) seperately
            //to help us store summaries

            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_CLOSING_TIME_QUERY,
                    MiscUtils.getClosingTimeFromDateTime(params.getClosingDateTime()),
                    Constants.TYPE_STRING);

            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_OPENING_TIME_QUERY,
                    MiscUtils.getOpeningTimeFromDateTime(params.getOpeningDateTime()),
                    Constants.TYPE_STRING);

            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_OPERATING_HOURS_SET_QUERY,
                    true,Constants.TYPE_BOOL);



            //is Slot occupied assumed
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_IS_SLOT_OCCUPIED_ASSUMED_QUERY,
                    params.isSlotOccupiedAssumed(),Constants.TYPE_BOOL);

            //is open 24 hours
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_IS_OPEN_24_HOURS,
                    params.isOpen24Hours(), Constants.TYPE_BOOL);
            //multiday parking
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_IS_MULTI_DAY_PARKING_POSSIBLE_QUERY,
                    params.isMultiDayParkingPossible(),Constants.TYPE_BOOL);

            //isParkingPrepaid
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY,
                    params.isParkingPrepaid(),Constants.TYPE_BOOL);

            //set numHours
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_NUM_PREPAID_HOURS_QUERY,
                    params.getNumHoursPrepaid(),Constants.TYPE_INT);


            //set up sync id as 0
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_SYNC_ID_QUERY,
                    0,Constants.TYPE_INT);


            /*set up next expected syncId as 0
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_NEXT_EXPECTED_SYNC_ID_QUERY,
                    0,Constants.TYPE_INT);*/

            //set up sync state as INSYNC
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_SYNC_STATE_QUERY,
                    Constants.SYNC_STATE_IN_SYNC,Constants.TYPE_INT);

            //lostTicket charge
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_LOST_TICKET_CHARGE,params.getLostTicketCharge(),Constants.TYPE_INT);

            //spaceOwner Name
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_SPACE_OWNER_NAME_QUERY,params.getSpaceOwnerName(),
                    Constants.TYPE_STRING);

            //spaceOwner Mobile
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_SPACE_OWNER_MOBILE_QUERY,params.getSpaceOwnerMobile(),
                    Constants.TYPE_STRING);

            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_IS_SPACE_BIKE_ONLY_QUERY,
                    params.isSpaceBikeOnly(),Constants.TYPE_BOOL);

            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_SPACE_VEHICLE_TYPE_QUERY,
                    true,Constants.TYPE_BOOL);

            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_IS_GATEWISE_REPORT_REQUIRED_QUERY,
                    params.isGateWiseReportsRequired(),Constants.TYPE_BOOL);

            //reusable print token
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_IS_REUSABLE_TOKEN_USED,params.isReusablePrintTokenUsed(),
                    Constants.TYPE_BOOL);

        }

        private void setPricing(JSONObject pricingInfo) throws JSONException{

            PricingPerVehicleTypeTbl pricing = new PricingPerVehicleTypeTbl();
            pricing.setVehicleType(pricingInfo.optInt("vehicle_type",Constants.VEHICLE_TYPE_INVALID));
            pricing.setHourlyPrice(pricingInfo.optDouble("hourly",0.00));
            pricing.setDailyPrice(pricingInfo.optDouble("daily",0.00));
            pricing.setWeeklyPrice(pricingInfo.optDouble("weekly",0.00));
            pricing.setMonthlyPrice(pricingInfo.optDouble("monthly",0.00));
            pricing.setYearlyPrice(pricingInfo.optDouble("yearly",0.00));
            pricing.setIsPricingTiered(pricingInfo.optBoolean("tierPricingSupport",false));
            pricing.setLostTicketCharge(pricingInfo.optDouble("lost_ticket_charge",0.00));
            pricing.setStaffPassDiscountPercentage(pricingInfo.optInt("staff_pass_discount_percentage",0));

            Context context = getApplicationContext();

            if(pricing.getLostTicketCharge() > 0.00){
                //this boolean is used to display the lost ticket charge switch on the exit screen
                Cache.setInCache(context,Constants.CACHE_PREFIX_IS_LOST_TICKET_CHARGE_AVAILABLE,
                        true,Constants.TYPE_BOOL);
            }
            if(pricing.getStaffPassDiscountPercentage() > 0){
                //this boolean is used to display the lost ticket charge switch on the exit screen
                Cache.setInCache(context,Constants.CACHE_PREFIX_IS_STAFF_DISCOUNT_AVAILABLE_QUERY,
                        true,Constants.TYPE_BOOL);
            }


            boolean isParkingPrepaid = (Boolean)Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY,Constants.TYPE_BOOL);
            int numPrepaidHours = 0;


            if(pricing.isPricingTiered()){
                String cacheKey = Constants.CACHE_PREFIX_IS_PRICING_TIERED + pricing.getVehicleType();
                Cache.setInCache(context,cacheKey,true,Constants.TYPE_BOOL);
                setTieredPricingInCache(pricingInfo.getJSONObject("tierPricing"),
                        pricing.getVehicleType());

                if(mCacheParams.isParkingPrepaid()){
                    String key = Constants.CACHE_PREFIX_PRICE_AT_HOUR_QUERY +
                            pricing.getVehicleType() +
                            Integer.toString(mCacheParams.getNumHoursPrepaid());
                    Double fees = ((Double)Cache.getFromCache(context, key,Constants.TYPE_DOUBLE));
                    int parkingFees = fees.intValue();

                    setPrepaidParkingFees(pricing.getVehicleType(),parkingFees);
                }
            }else{
                setPricingInCache((int)pricing.getHourlyPrice(),pricing.getVehicleType());
                if(mCacheParams.isParkingPrepaid()){
                    int parkingFees = (int)(pricing.getHourlyPrice() * numPrepaidHours);
                    setPrepaidParkingFees(pricing.getVehicleType(),parkingFees);
                }
            }

            //night charge
            pricing.setIsNightChargeAvailable(pricingInfo.optBoolean("isNightChargeAvailable", false));


            if(pricing.isNightChargeAvailable()){
                JSONObject nightChargeObj = pricingInfo.getJSONObject("nightCharge");
                NightChargeTbl nightCharge = new NightChargeTbl();
                nightCharge.setStartHour(nightChargeObj.optInt("startHour",0));
                nightCharge.setEndHour(nightChargeObj.optInt("endHour", 0));
                nightCharge.setDailyCharge(nightChargeObj.optInt("dailyCharge", 0));
                nightCharge.setMonthlyCharge(nightChargeObj.optInt("monthlyCharge",0));
                nightCharge.setVehicleType(pricing.getVehicleType());

                nightCharge.save();

                //set in Cache
                Cache.setInCache(context,
                        Constants.CACHE_PREFIX_NIGHT_CHARGE_QUERY + pricing.getVehicleType(),
                        nightCharge.getDailyCharge(),Constants.TYPE_INT);
            }

            //penalty charge
            pricing.setIsPenaltyAvailable(pricingInfo.optBoolean("isPenaltyChargeAvailable", false));



            if(pricing.isPenaltyAvailable()){
                JSONObject penaltyObj = pricingInfo.getJSONObject("penaltyCharge");
                PenaltyChargeTbl penalty = new PenaltyChargeTbl();
                penalty.setChargeCondition(penaltyObj.optInt("chargeCondition"));
                penalty.setHourlyCharge(penaltyObj.optInt("hourlyCharge"));
                penalty.setVehicleType(pricing.getVehicleType());
                penalty.save();
            }

            pricing.save();

            //store monthly prices for car,bike,minibus,bus
            String key = Constants.CACHE_PREFIX_MONTHLY_RATE_QUERY + pricing.getVehicleType();
            Cache.setInCache(getApplicationContext(),key,
                    (int)pricing.getMonthlyPrice(),Constants.TYPE_INT);
            //store staff discount in cache
            String discountKey = Constants.CACHE_PREFIX_STAFF_DISCOUNT_RATE_QUERY + pricing.getVehicleType();
            Cache.setInCache(getApplicationContext(),discountKey,
                    (int)pricing.getStaffPassDiscountPercentage(),Constants.TYPE_INT);

            Cache.setInCache(getApplicationContext(),discountKey,
                    (int)pricing.getStaffPassDiscountPercentage(),Constants.TYPE_INT);


            setPricingAsAvailable(pricing.getVehicleType());

        }

        private void setPricingInCache(int hourlyPrice,int vehicleType){
            String pricingStr = "Rs. " + hourlyPrice + "/hour";

            Context context = getApplicationContext();


            //save the received pricing string to help us print
            Cache.setInCache(context,
                    Constants.CACHE_PREFIX_PRINT_PRICING_QUERY + vehicleType,
                    pricingStr, Constants.TYPE_STRING);


        }

        private void setTieredPricingInCache(JSONObject tierPricingObject, int vehicleType)
                throws JSONException {

            Iterator<String> iter = tierPricingObject.keys();


            int itr = 1;
            Context context = getApplicationContext();
            String tierPricingToPrint = "";
            int startHour = 0;
            Map<Integer, Double> sortedTierPrice = new TreeMap<Integer, Double>();

            int i = 0;
            while (iter.hasNext()) {
                String key = iter.next();
                Double pricePerTier = Double.parseDouble(key);
                int hours = tierPricingObject.getInt(key);
                sortedTierPrice.put(hours, pricePerTier);
            }

            Iterator entries = sortedTierPrice.entrySet().iterator();
            int maxHour = 1;
            double maxPricePerTier = 0.00;
            String cacheKey = Constants.CACHE_PREFIX_PRICE_AT_HOUR_QUERY + vehicleType;

            while (entries.hasNext()) {
                Map.Entry element = (Map.Entry) entries.next();
                int hours = (int) element.getKey();
                double pricePerTier = (double) element.getValue();

                if (entries.hasNext()) {
                    tierPricingToPrint += startHour + "-" + hours + " hours: Rs." +
                            (int) Math.ceil(pricePerTier) +
                            Constants.TIER_PRICE_SEPERATOR;
                } else {
                    tierPricingToPrint += "More than " + startHour + " hours: Rs. " +
                            (int) Math.ceil(pricePerTier) + Constants.TIER_PRICE_SEPERATOR;
                }

                startHour = hours;
                maxHour = hours;
                maxPricePerTier = pricePerTier;



                for (; itr <= hours; itr++) {
                    Cache.setInCache(context,cacheKey + Integer.toString(itr),
                            pricePerTier, Constants.TYPE_DOUBLE);
                }

            }

            if(maxHour != Constants.NUM_HOURS_IN_A_DAY){
                for(int idx = maxHour + 1; idx <= Constants.NUM_HOURS_IN_A_DAY; idx++){
                    Cache.setInCache(context,cacheKey + Integer.toString(idx),
                            maxPricePerTier, Constants.TYPE_DOUBLE);
                }
            }

            //save the received tier pricing string to help us print
            String key = Constants.CACHE_PREFIX_PRINT_PRICING_QUERY + vehicleType;
            Cache.setInCache(context,key,tierPricingToPrint, Constants.TYPE_STRING);



        }

        private  void setPrepaidParkingFees(int vehicleType,int parkingFees){

            Context context = getApplicationContext();

            switch (vehicleType){
                case Constants.VEHICLE_TYPE_CAR:{
                    Cache.setInCache(context,Constants.CACHE_PREFIX_CAR_PREPAID_FEES_QUERY,
                            parkingFees,Constants.TYPE_INT);
                }
                break;

                case Constants.VEHICLE_TYPE_BIKE:{
                    Cache.setInCache(context,Constants.CACHE_PREFIX_BIKE_PREPAID_FEES_QUERY,
                            parkingFees,Constants.TYPE_INT);
                }
                break;

                case Constants.VEHICLE_TYPE_MINIBUS:{
                    Cache.setInCache(context,Constants.CACHE_PREFIX_MINIBUS_PREPAID_FEES_QUERY,
                            parkingFees,Constants.TYPE_INT);
                }
                break;

                case Constants.VEHICLE_TYPE_BUS:{
                    Cache.setInCache(context,Constants.CACHE_PREFIX_BUS_PREPAID_FEES_QUERY,
                            parkingFees,Constants.TYPE_INT);
                }
                break;
            }



        }

        private void setPricingAsAvailable(int vehicleType){
            switch(vehicleType){
                case Constants.VEHICLE_TYPE_CAR:
                    Cache.setInCache(getApplicationContext(),
                            Constants.CACHE_PREFIX_IS_CAR_PRICING_AVAILABLE_QUERY,
                            true,Constants.TYPE_BOOL);
                    break;

                case Constants.VEHICLE_TYPE_BIKE:
                    Cache.setInCache(getApplicationContext(),
                            Constants.CACHE_PREFIX_IS_BIKE_PRICING_AVAILABLE_QUERY,
                            true,Constants.TYPE_BOOL);
                    break;

                case Constants.VEHICLE_TYPE_MINIBUS:
                    Cache.setInCache(getApplicationContext(),
                            Constants.CACHE_PREFIX_IS_MINIBUS_PRICING_AVAILABLE_QUERY,
                            true,Constants.TYPE_BOOL);
                    break;

                case Constants.VEHICLE_TYPE_BUS:
                    Cache.setInCache(getApplicationContext(),
                            Constants.CACHE_PREFIX_IS_BUS_PRICING_AVAILABLE_QUERY,
                            true,Constants.TYPE_BOOL);
                    break;


            }
        }



    }


    private void setCustomEntryTicketFormat(JSONObject customEntryFormat){
        Iterator<String> iter = customEntryFormat.keys();
        String[] customStrings = new String[Constants.CUSTOM_FORMAT_MAX_IDX];

        int i = 0;
        try {
            boolean customFormatAvailable = false;
            while (iter.hasNext()) {
                String key = iter.next();

                if(key.equals(Constants.CUSTOM_VEHICLE_TEXT_SIZE)){
                    int value = customEntryFormat.optInt(key,0);
                    if(value == 1) {
                        Cache.setInCache(mContext,Constants.CACHE_PREFIX_BIG_SIZE_VEHICLE_REGSTRING,
                                true, Constants.TYPE_BOOL);
                    }

                    continue;
                }
                String value = customEntryFormat.getString(key);

                if(!value.isEmpty()) {

                    if(key.equals(Constants.CUSTOM_SPACE_ADDRESS)) {
                        customStrings[Constants.CUSTOM_SPACE_ADDRESS_IDX] = value;
                        customFormatAvailable = true;
                    }else if(key.equals(Constants.CUSTOM_SPACE_HEADING)){
                        customStrings[Constants.CUSTOM_SPACE_HEADING_IDX] = value;
                        customFormatAvailable = true;
                    }else if(key.equals(Constants.CUSTOM_AGENCY_NAME)){
                        customStrings[Constants.CUSTOM_AGENCY_NAME_IDX] = value;
                        customFormatAvailable = true;
                    }else if(key.equals(Constants.CUSTOM_SPACE_OWNER_MOBILE)){
                        customStrings[Constants.CUSTOM_SPACE_OWNER_MOBILE_IDX] = value;
                        customFormatAvailable = true;
                    }else if(key.equals(Constants.CUSTOM_OPERATING_HOURS)){
                        customStrings[Constants.CUSTOM_OPERATING_HOURS_IDX] = value;
                        customFormatAvailable = true;
                    }else if(key.equals(Constants.CUSTOM_PARKING_RATES)){
                        customStrings[Constants.CUSTOM_PARKING_RATES_IDX] = value;
                        customFormatAvailable = true;
                    }else if(key.equals(Constants.CUSTOM_BIKE_PARKING_RATES)){
                        customStrings[Constants.CUSTOM_BIKE_PARKING_RATES_IDX] = value;
                        customFormatAvailable = true;
                    }else if(key.equals(Constants.CUSTOM_NIGHT_CHARGES)){
                        customStrings[Constants.CUSTOM_NIGHT_CHARGES_IDX] = value;
                        customFormatAvailable = true;
                    }else if(key.equals(Constants.CUSTOM_BIKE_NIGHT_CHARGES)){
                        customStrings[Constants.CUSTOM_BIKE_NIGHT_CHARGES_IDX] = value;
                        customFormatAvailable = true;
                    }else if(key.equals(Constants.CUSTOM_LOST_TICKET)){
                        customStrings[Constants.CUSTOM_LOST_TICKET_IDX] = value;
                        customFormatAvailable = true;
                    }else if(key.equals(Constants.CUSTOM_URL)){
                        customStrings[Constants.CUSTOM_URL_IDX] = value;
                        customFormatAvailable = true;
                    }else if(key.equals(Constants.CUSTOM_DESC)){
                        customStrings[Constants.CUSTOM_DESC_IDX] = value;
                        customFormatAvailable = true;
                    }else if(key.equals(Constants.CUSTOM_AD_TEXT)){
                        customStrings[Constants.CUSTOM_AD_TEXT_IDX] = value;
                        customFormatAvailable = true;
                    }else if(key.equals(Constants.CUSTOM_SPARK_AD_TEXT)){
                        customStrings[Constants.CUSTOM_SPARK_AD_IDX] = value;
                        customFormatAvailable = true;
                    }
                }

            }
            if(customFormatAvailable) {
                MiscUtils.setCustomFormats(getApplicationContext(), customStrings);
            }
        }catch(JSONException je){

        }

    }

    private void setPrintTokens(JSONObject tokens) throws JSONException{
        Iterator<String> keys = tokens.keys();

        while(keys.hasNext()){
            String key = keys.next();
            String token = tokens.getString(key);

            ReusableTokensTbl row = new ReusableTokensTbl(token);
            row.save();


        }
    }

}
