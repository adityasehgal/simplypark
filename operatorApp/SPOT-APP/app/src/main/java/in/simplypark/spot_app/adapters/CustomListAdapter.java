package in.simplypark.spot_app.adapters;

import android.app.Activity;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;

import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.R;
import in.simplypark.spot_app.listelements.ReportElements;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by aditya on 06/06/16.
 */
public class CustomListAdapter extends ArrayAdapter<String> {
    private final Activity mContext;
    private ArrayList<ReportElements> mElements;
    private ArrayList<ReportElements> mOriginalElements;
    private Filter mFilter;

    public CustomListAdapter(Activity context, ArrayList<ReportElements> elements){
        super(context, R.layout.report_list);

        mContext = context;
        mElements = new ArrayList<ReportElements>(elements);
        //mElements = elements;
        mOriginalElements = new ArrayList<ReportElements>(elements);
    }


    public void add(ReportElements row){
       super.add("item");
       mElements.add(row);
       mOriginalElements.add(row);

    }

    public void addAll(ArrayList<ReportElements> elements){
        String []item = new String[elements.size()];
        java.util.Arrays.fill(item,"item1");

        super.addAll(item);
        mElements.addAll(elements);
        mOriginalElements.addAll(elements);
    }

    public ReportElements get(int position){
        return mElements.get(position);
    }

    @Override
    public int getCount() {
        return mElements.size();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent){
        LayoutInflater inflater = mContext.getLayoutInflater();

        View rowView = inflater.inflate(R.layout.report_list, null, true);
        TextView  heading = (TextView)rowView.findViewById(R.id.item);
        //ImageView icon = (ImageView)rowView.findViewById(R.id.icon);
        FancyButton icon  = (FancyButton)rowView.findViewById(R.id.icon);
        TextView  subHeading = (TextView)rowView.findViewById(R.id.textView1);
        TextView  earnings = (TextView)rowView.findViewById(R.id.earning_per_parking);
        TextView  customer = (TextView)rowView.findViewById(R.id.monthly_pass_customer_id);
        View      lineView = rowView.findViewById(R.id.rl_synched_with_server_state);

        ReportElements row = mElements.get(position);
//        rowView.setTag();
        heading.setText(row.getHeading());
        //icon.setImageResource(row.getIcon());
        icon.setIconResource(row.getIcon());
        subHeading.setText(row.getSubHeading());
        earnings.setText(row.getParkingFees());
        customer.setText(row.getCustomer());

        if(row.getParkingStatus() == Constants.PARKING_STATUS_TRANSACTION_CANCELLED){
            heading.setPaintFlags(heading.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            subHeading.setPaintFlags(heading.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            earnings.setPaintFlags(heading.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        if(row.getSyncedWithServerState()){
            lineView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.simplypark_blue3));
        }else{
            lineView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.red));
        }



        return rowView;
    }

    @Override
    public Filter getFilter()
    {
        if (mFilter == null)
            mFilter = new CustomListAdapterFilter();

        return mFilter;
    }

    private class CustomListAdapterFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            String prefix = constraint.toString().toLowerCase();

            if (prefix == null || prefix.length() == 0)
            {
                results.values = mOriginalElements;
                results.count  = mOriginalElements.size();
            }else{

                final ArrayList<ReportElements> list = mOriginalElements;
                int count = list.size();
                final ArrayList<ReportElements> filteredList = new ArrayList<ReportElements>(count);

                for (int i=0; i<count; i++)
                {
                    final ReportElements element = list.get(i);
                    final String vehicleReg = element.getHeading();



                    //if (vehicleReg.startsWith(prefix))
                    if(vehicleReg.toLowerCase().contains(prefix))
                    {
                        filteredList.add(element);
                    }
                }

                results.values = filteredList;
                results.count  = filteredList.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            int count = results.count;

            if(count > 0) {
                mElements = (ArrayList<ReportElements>)(results.values);
                notifyDataSetChanged();
            }else{
                mElements.clear();
                //notifyDataSetInvalidated();
                notifyDataSetChanged();
            }
        }

    }

}
