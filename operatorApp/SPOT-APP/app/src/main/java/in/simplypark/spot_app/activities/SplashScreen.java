package in.simplypark.spot_app.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import in.simplypark.spot_app.utils.Cache;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.R;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash_screen);


        if(checkPlayServices()) {
            //Reading from Cache instead of DB
            if (isOperatorLoggedIn()) {
                if(!isPrinterSetup()) {
                    Log.d("SPLASH SCREEN", "Printer NOT setup");
                    Intent intent = new Intent(this, SetupActivity.class);
                    //Intent intent = new Intent(this, PrinterSetup.class);
                    startActivity(intent);

                }else {
                        Log.d("SPLASH SCREEN", "Space Info is available");
                        Intent intent = new Intent(this, MainActivity.class);
                        //Intent intent = new Intent(this, PrinterSetup.class);
                        startActivity(intent);

                }


            } else {
                //start the login entry only when the user is not logged in or we dont have the space
                // information
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);

            }

            finish();
        }

    }



    private Boolean isOperatorLoggedIn(){


        return (Boolean) Cache.getFromCache(getApplicationContext(),
                                  Constants.CACHE_PREFIX_LOGIN_QUERY,
                                  Constants.TYPE_BOOL);
    }

    private Boolean isPrinterSetup(){

        if((Boolean)Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_ISBT_SUPPORTED_QUERY,Constants.TYPE_BOOL) == false){
            return true;
        }


        return (Boolean)Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_IS_PRINTER_CONFIGURED,
                Constants.TYPE_BOOL);

    }

    private Boolean isTestPagePrinted(){

        return false;
        /*
        return (Boolean)Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_IS_TEST_PAGE_PRINTED,
                Constants.TYPE_BOOL);
                */
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode,
                                        Constants.PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                //Log.i("checkPlayServices", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
}
