package in.simplypark.spot_app.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.support.v4.content.LocalBroadcastManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import in.simplypark.spot_app.utils.Cache;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.db_tables.PaymentInfoTbl;
import in.simplypark.spot_app.db_tables.SimplyParkBookingTbl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static in.simplypark.spot_app.utils.MiscUtils.checkForConnectivity;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class PaymentGatewayIntentService extends IntentService {


    public PaymentGatewayIntentService() {
        super("PaymentGatewayIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (Constants.ACTION_CREATE_INVOICE.equals(action)) {
                final String bookingId = intent.getStringExtra(Constants.BUNDLE_PARAM_BOOKING_ID);
                final String name = intent.getStringExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_ISSUED_TO);
                final String email = intent.getStringExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_EMAIL);
                final String mobile = intent.getStringExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_TEL);
                final int amount = intent.getIntExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_PER_MONTH_FEES,0);

                handleActionCreateInvoice(bookingId,name,email,mobile,amount);

            }else if(Constants.ACTION_COLLECT_PAYMENT_SUCCESSFUL_RESULT.equals(action)){
                final String bookingId = intent.getStringExtra(Constants.BUNDLE_PARAM_BOOKING_ID);
                final String invoiceId = intent.getStringExtra(Constants.BUNDLE_PARAM_INVOICE_ID);
                final String paymentId = intent.getStringExtra(Constants.BUNDLE_PARAM_PAYMENT_ID);

                handleActionCollectPaymentSuccessfulResult(bookingId,invoiceId,paymentId);
            }else if(Constants.ACTION_COLLECT_PAYMENT_FAILURE_RESULT.equals(action)){
                final String bookingId = intent.getStringExtra(Constants.BUNDLE_PARAM_BOOKING_ID);

                handleActionCollectPaymentFailureResult(bookingId);
            }
        }
    }


    /**
     * Starts this service to perform action startActionCreateInvoice with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionCreateInvoice(Context context, SimplyParkBookingTbl booking){
        Intent intent = new Intent(context,PaymentGatewayIntentService.class);
        intent.setAction(Constants.ACTION_CREATE_INVOICE);

        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_ID,booking.getBookingID());
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_ISSUED_TO,booking.getCustomerDesc());
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_EMAIL,booking.getEmail());
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_TEL,booking.getMobile());
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_PER_MONTH_FEES,booking.getParkingFees());

        context.startService(intent);


    }

    /**
     * Starts this service to perform action startActionPaymentResult ith the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionSuccessfulPaymentResult(Context context, String bookingId,
                                                          String invoiceId,String paymentId){
        Intent intent = new Intent(context, PaymentGatewayIntentService.class);
        intent.setAction(Constants.ACTION_COLLECT_PAYMENT_SUCCESSFUL_RESULT);

        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_ID, bookingId);
        intent.putExtra(Constants.BUNDLE_PARAM_INVOICE_ID,invoiceId);
        intent.putExtra(Constants.BUNDLE_PARAM_PAYMENT_ID,paymentId);

        context.startService(intent);


    }

    /**
     * Starts this service to perform action startActionPaymentResult ith the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionFailurePaymentResult(Context context, String bookingId){
        Intent intent = new Intent(context, PaymentGatewayIntentService.class);
        intent.setAction(Constants.ACTION_COLLECT_PAYMENT_FAILURE_RESULT);

        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_ID, bookingId);



        context.startService(intent);


    }






    /**
     * Handle action handleActionCreateInvoice in the provided background thread with the provided
     * parameters.
     */
    private void handleActionCreateInvoice(String bookingId,String name,String email,String mobile,
                                           int amount){
        if(!checkForConnectivity(getApplicationContext())) {

            sendResult(Constants.TRIGGER_ACTION_CREATE_INVOICE, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);

            return;
        }

        try {
            OkHttpClient client = new OkHttpClient();
            Request request = buildCreateInvoiceRequest(bookingId, name, email, mobile, amount,
                    Constants.RAZORPAY_PRODUCT_NAME,
                    Constants.RAZORPAY_PRODUCT_DESC);

            Response response = client.newCall(request).execute();

            int result = Constants.OK;
            int receivedSyncId = 0;

            if(response.isSuccessful()) {

                String responseData = response.body().string();
                JSONObject json = new JSONObject(responseData);

                String invoiceId = json.getString("invoiceId");


                Intent intent = prepareResult(Constants.TRIGGER_ACTION_CREATE_INVOICE,result);

                intent.putExtra(Constants.BUNDLE_PARAM_INVOICE_ID,invoiceId);
                intent.putExtra(Constants.BUNDLE_PARAM_PARKING_FEES,amount);
                intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_ID,bookingId);


                // Broadcasts the Intent to receivers in this app.
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
            }else{
                sendResult(Constants.TRIGGER_ACTION_CREATE_INVOICE, Constants.NOK,
                        Constants.ERROR_INVALID_RESPONSE);

            }
            response.body().close();

        }catch(JSONException ioe){
            sendResult(Constants.TRIGGER_ACTION_CREATE_INVOICE, Constants.NOK,
                    Constants.ERROR_INVALID_RESPONSE);
        }catch(java.io.IOException ioe){
            sendResult(Constants.TRIGGER_ACTION_CREATE_INVOICE, Constants.NOK,
                    Constants.ERROR_NO_CONNECTION);
        }

    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionCollectPaymentSuccessfulResult(String bookingId,
                                                            String invoiceId,String paymentId){

        List<SimplyParkBookingTbl> bookings = SimplyParkBookingTbl.find(
                SimplyParkBookingTbl.class, "m_booking_id = ?",bookingId);

        if(bookings.isEmpty()){
            sendResult(Constants.TRIGGER_ACTION_COLLECT_PAYMENT_SUCCESSFUL_RESULT,Constants.NOK,
                    Constants.ERROR_NO_SUCH_BOOKING);
            return;
        }

        SimplyParkBookingTbl booking = bookings.get(0);


        PaymentInfoTbl payment = new PaymentInfoTbl();
        payment.setBookingId(bookingId);
        payment.setInvoiceId(invoiceId);
        payment.setPaymentId(paymentId);

        payment.save();

        Intent intent = prepareResult(Constants.TRIGGER_ACTION_COLLECT_PAYMENT_SUCCESSFUL_RESULT,
                Constants.OK);

        intent.putExtra(Constants.BUNDLE_PARAM_CAR_REG,booking.getVehicleReg());
        intent.putExtra(Constants.BUNDLE_PARAM_VEHICLE_TYPE, booking.getVehicleType());
        intent.putExtra(Constants.BUNDLE_PARAM_CUSTOMER_DESC,booking.getCustomerDesc());
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_EMAIL, booking.getEmail());
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_TEL, booking.getMobile());
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_VALID_FROM, booking.getBookingStartTime());
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_VALID_TILL, booking.getBookingEndTime());
        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_ID,bookingId);

        intent.putExtra(Constants.BUNDLE_PARAM_BOOKING_STATUS,booking.getBookingStatus());
        intent.putExtra(Constants.BUNDLE_PARAM_MONTHLY_PASS_ISSUE_DATE,booking.getPassIssueDate());
        intent.putExtra(Constants.BUNDLE_PARAM_ENCODE_PASS_CODE,booking.getEncodedPassCode());
        intent.putExtra(Constants.BUNDLE_PARAM_SERIAL_NUMBER,booking.getSerialNumber());
        intent.putExtra(Constants.BUNDLE_PARAM_NUM_BOOKED_SLOTS, booking.getNumBookedSlots());
        intent.putExtra(Constants.BUNDLE_PARAM_IS_RENEWAL,booking.getIsRenewalBooking());
        intent.putExtra(Constants.BUNDLE_PARAM_PARENT_BOOKING_ID,booking.getParentBookingID());
        intent.putExtra(Constants.BUNDLE_PARAM_PARKING_FEES, booking.getParkingFees());

        intent.putExtra(Constants.BUNDLE_PARAM_INVOICE_ID,payment.getInvoiceId());
        intent.putExtra(Constants.BUNDLE_PARAM_PAYMENT_ID,payment.getPaymentId());

        // Broadcasts the Intent to receivers in this app.
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);


    }

    /*********************************************************************************
     * RazorPay payment failure. Delete the booking and the associated paymentInfo obj
     * @param bookingId
     */
    private void handleActionCollectPaymentFailureResult(String bookingId){

        List<SimplyParkBookingTbl> bookings = SimplyParkBookingTbl.find(
                SimplyParkBookingTbl.class, "m_booking_id = ?",bookingId);

        if(bookings.isEmpty()){
           return;
        }

        SimplyParkBookingTbl booking = bookings.get(0);

        booking.delete();

        Intent intent = prepareResult(Constants.TRIGGER_ACTION_COLLECT_PAYMENT_UNSUCCESSFUL_RESULT,
                Constants.OK);

        // Broadcasts the Intent to receivers in this app.
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);



    }


    /********************************************************************************
     *  Misc Util functions
     *
     *
     *********************************************************************************/
    private Intent prepareResult(int triggerActionName, int result){
            /*
          * Creates a new Intent containing a Uri object
          * BROADCAST_ACTION is a custom Intent action
          */
        Intent localIntent =
                new Intent(Constants.SERVER_UPDATION_PAYMENT_GATEWAY_RESULTS_ACTION);

        localIntent.putExtra(Constants.SERVER_UPDATION_PAYMENT_GATEWAY_RESULTS_STATUS, result);

        localIntent.putExtra(Constants.TRIGGER_ACTION_NAME, triggerActionName);

        return localIntent;
    }

    private void sendResult(int triggerActionName, int result){
            /*
          * Creates a new Intent containing a Uri object
          * BROADCAST_ACTION is a custom Intent action
          */
        Intent localIntent =
                new Intent(Constants.SERVER_UPDATION_PAYMENT_GATEWAY_RESULTS_ACTION);

        localIntent.putExtra(Constants.SERVER_UPDATION_PAYMENT_GATEWAY_RESULTS_STATUS,result);

        localIntent.putExtra(Constants.TRIGGER_ACTION_NAME, triggerActionName);

        // Broadcasts the Intent to receivers in this app.
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);

    }


    private void sendResult(int triggerActionName, int result, String error){
            /*
          * Creates a new Intent containing a Uri object
          * BROADCAST_ACTION is a custom Intent action
          */
        Intent localIntent =
                new Intent(Constants.SERVER_UPDATION_PAYMENT_GATEWAY_RESULTS_ACTION);

        localIntent.putExtra(Constants.SERVER_UPDATION_PAYMENT_GATEWAY_RESULTS_STATUS,result);

        localIntent.putExtra(Constants.TRIGGER_ACTION_NAME, triggerActionName);
        localIntent.putExtra(Constants.ERROR,error);


        // Broadcasts the Intent to receivers in this app.
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);

    }

    private Request buildCreateInvoiceRequest(String bookingId,String name,String email,
                                              String mobile,int amount,String productName,
                                              String productDesc) throws JSONException {

        String spaceId = ((Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT)).toString();

        String url = Constants.CREATE_INVOICE + spaceId + ".json";


        JSONObject jsonDataObj = new JSONObject();
        jsonDataObj.put("name", name);
        jsonDataObj.put("contact",mobile);
        jsonDataObj.put("email",email);
        jsonDataObj.put("amount",amount);
        jsonDataObj.put("productName",productName);
        jsonDataObj.put("productDescription",productDesc);


        RequestBody body = RequestBody.create(Constants.JSON, jsonDataObj.toString());

        return (new Request.Builder()
                .url(url)
                .post(body)
                .build());

    }



}
