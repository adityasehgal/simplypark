package in.simplypark.spot_app.db_tables;

import com.orm.SugarRecord;

import java.util.List;

import in.simplypark.spot_app.config.Constants;

import static in.simplypark.spot_app.config.Constants.TOKEN_STATUS_BUSY;
import static in.simplypark.spot_app.config.Constants.TOKEN_STATUS_FREE;

/**
 * Created by root on 05/03/2017.
 */

public class ReusableTokensTbl extends SugarRecord {

    private String mToken;
    private int mStatus;

    public ReusableTokensTbl(){
        mToken  = "";
        mStatus = TOKEN_STATUS_FREE;
    }

    public ReusableTokensTbl(String token){
        mToken = token;
        mStatus = TOKEN_STATUS_FREE;
    }


    public void setTokenStatus(int status){
        mStatus = status;
    }

    public int getTokenStatus(){
        return mStatus;
    }

    public static int isTokenValid(String scannedToken){
        List<ReusableTokensTbl> tokens = ReusableTokensTbl.find(ReusableTokensTbl.class,
                          "m_token = ?",scannedToken);

        if(tokens.isEmpty()){
            return Constants.TOKEN_INVALID;
        }

        ReusableTokensTbl token = tokens.get(0);

        if(token.getTokenStatus() == Constants.TOKEN_STATUS_BUSY){
            return Constants.TOKEN_BUSY;
        }

        return Constants.TOKEN_VALID;
    }

    public static void markTokenAsFree(String scannedToken){
        List<ReusableTokensTbl> tokens = ReusableTokensTbl.find(ReusableTokensTbl.class,
                "m_token = ?",scannedToken);

        if(tokens.isEmpty()){
            return;
        }

        ReusableTokensTbl token = tokens.get(0);

        token.setTokenStatus(Constants.TOKEN_STATUS_FREE);

        token.save();

    }

    public static void markTokenAsBusy(String scannedToken){
        List<ReusableTokensTbl> tokens = ReusableTokensTbl.find(ReusableTokensTbl.class,
                "m_token = ?",scannedToken);

        if(tokens.isEmpty()){
            return;
        }

        ReusableTokensTbl token = tokens.get(0);

        token.setTokenStatus(Constants.TOKEN_STATUS_BUSY);

        token.save();

    }

    public static boolean isTokenConfigured(String token){
        List<ReusableTokensTbl> tokens = ReusableTokensTbl.find(ReusableTokensTbl.class,
                "m_token = ?",token);

        if(tokens.isEmpty()){
            return false;
        }else{
            return true;
        }

    }

}
