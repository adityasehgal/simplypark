package in.simplypark.spot_app.utils;

import java.io.IOException;

import in.simplypark.spot_app.listelements.Element;


/**
 * Created by aditya on 23/05/16.
 */
public abstract class PreparePrintData{
    public abstract String print(String data) throws IOException;


    public abstract byte[] prepareEntryTicket(byte[] headerTemplate, byte[] footerTemplate,
                                              Element[][] bodyFormat,
                                              String dateTime, String parkingId, Boolean isParkAndRide,
                                              String regNumber,int vehicleType);
    public abstract byte[] prepareExitTicket(byte[] headerTemplate, byte[] footerTemplate,
                                             Element[][] bodyFormat,
                                             String entryDateTime, String exitDateTime,
                                             String parkingFees, String nightChargesDesc,
                                             String regNumber,String durationDescription,
                                             int vehicleType);
    public abstract byte[] prepareReport(
                                  byte[] headerTemplate, byte[] footerTemplate,
                                  Element[][] bodyFormat,
                                  String reportPeriod, TransactionSummary summary);

    public abstract byte[] prepareMonthlyPass(
                                  byte[] headerTemplate, byte[] footerTemplate,
                                  Element[][] bodyFormat,
                                  String vehicleNumber, int vehicleType,
                                  String issuedTo, String email, String tel,
                                  String validFrom,String validTill,String transId,
                                  int perMonthFees);

    public abstract byte[] prepareMonthlyPassReceipt(byte[] headerTemplate, byte[] footerTemplate,
                                                     Element[][] bodyFormat,
                                                     int amountTransferred, String transId,
                                                     String nextPaymentDueOn);

    public abstract byte[] getEntryTicketHeaderTemplate(Element[][] header);
    public abstract byte[] getEntryTicketFooterTemplate(Element[][] footer);

    public abstract byte[] getExitTicketHeaderTemplate(Element[][] header);
    public abstract byte[] getExitTicketFooterTemplate(Element[][] footer);

    public abstract byte[] getReportHeaderTemplate(Element[][] header);
    public abstract byte[] getReportFooterTemplate(Element[][] footer);

    public abstract byte[] getMonthlyPassHeaderTemplate(Element[][] header);
    public abstract byte[] getMonthlyPassFooterTemplate(Element[][] footer);

    public abstract byte[] getMonthlyPassReceiptHeaderTemplate(Element[][] header);
    public abstract byte[] getMonthlyPassReceiptFooterTemplate(Element[][] footer);






}
