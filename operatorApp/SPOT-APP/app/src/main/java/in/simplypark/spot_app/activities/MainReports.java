package in.simplypark.spot_app.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import hirondelle.date4j.DateTime;
import in.simplypark.spot_app.R;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.db_tables.LostPassTbl;
import in.simplypark.spot_app.db_tables.SimplyParkBookingTbl;
import in.simplypark.spot_app.db_tables.SimplyParkTransactionTbl;
import in.simplypark.spot_app.db_tables.TransactionTbl;
import in.simplypark.spot_app.printer.PrinterService;
import in.simplypark.spot_app.utils.Cache;
import in.simplypark.spot_app.utils.TransactionSummary;

public class MainReports extends AppCompatActivity {

    private TransactionSummaryTask mTransactionSummaryTask;
    private View     mProgressView;
    private View     mScrollView;
    private TextView mDateTextView;
    private TextView mTodaysEarningTextView;
    private TextView mCarEarningTextView;
    private TextView mBikeEarningTextView;
    private TextView mMiniBusEarningTextView;
    private TextView mBusEarningTextView;
    private TextView mPassesEarningTextView;
    private TextView mLostPassesEarningTextView;

    private TextView mTotalVehiclesParkedTextView;
    private TextView mNumCarsParkedTextView;
    private TextView mNumPassCarsParkedTextView;
    private TextView mNumBikesParkedTextView;
    private TextView mNumPassBikesParkedTextView;
    private TextView mNumMiniBusParkedTextView;
    private TextView mNumPassMiniBusParkedTextView;
    private TextView mNumBusesParkedTextView;
    private TextView mNumPassBusesParkedTextView;

    private TextView mInCompleteCarExits;
    private TextView mInCompletePassCarExits;
    private TextView mInCompleteBikeExits;
    private TextView mInCompletePassBikeExits;
    private TextView mInCompleteMiniBusExits;
    private TextView mInCompletePassMiniBusExits;
    private TextView mInCompleteBusExits;
    private TextView mInCompletePassBusExits;
    private TextView mInCompleteTotalExits;
    private View     mInCompleteExitsView;

    private View     mTotalVehiclesView;
    private TextView mTotalVehiclesInsideView;
    private TextView mTotalCarsInsideView;
    private TextView mTotalPassCarsInsideView;
    private TextView mTotalBikesInsideView;
    private TextView mTotalPassBikesInsideView;
    private TextView mTotalMiniBusInsideView;
    private TextView mTotalPassMiniBusInsideView;
    private TextView mTotalBusesInsideView;
    private TextView mTotalPassBusesInsideView;

    private TextView mPassesIssuedView;
    private TextView mCarPassesIssuedView;
    private TextView mBikePassesIssuedView;
    private TextView mMiniBusPassesIssuedView;
    private TextView mBusPassesIssuedView;

    private TextView mPassesReIssuedView;

    private TableRow mPassBikesParkedRowView;
    private TableRow mPassBikesInsideRowView;
    private TableRow mPassBikesIncompleteExitsView;


    private View mPassesIssuedCardIdView;
    private View mOperatorEarningsView;
    private View mOperatorPassEarningsView;
    private View makeLostPassEarningsCardView;
    private View makeLostPassCountView;


    private Button mNextButton;
    private Button mPrevButton;
    private Button mShowAllButton;

    private EditText mFromDateView;
    private EditText mToDateView;

    private DatePickerDialog mFromDatePicker;
    private DatePickerDialog mToDatePicker;
    private String mDate4jFormattedFromDate;
    private String mDate4jFormattedToDate;
    private SimpleDateFormat  mDateFormatter;
    private SimpleDateFormat  mDate4jStyleFormatter;

    private int mDayOffset = 0;

    private long mFrom, mTo;
    private TransactionSummary mSummary;
    private String mReportTimeFrame;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setTitle("Summary");

        setContentView(R.layout.activity_main_reports);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);


        mProgressView = findViewById(R.id.cmr_loading_progress);
        mScrollView   = findViewById(R.id.cmr_main_view);
        mDateTextView = (TextView)findViewById(R.id.cmr_heading);
        mTodaysEarningTextView = (TextView)findViewById(R.id.cmr_daily_earnings_id);
        mCarEarningTextView = (TextView)findViewById(R.id.cmr_car_earnings_id);
        mBikeEarningTextView = (TextView)findViewById(R.id.cmr_bike_earnings_id);
        mPassesEarningTextView = (TextView)findViewById(R.id.cmr_passes_earnings_id);
        mLostPassesEarningTextView = (TextView)findViewById(R.id.cmr_lost_passes_earnings_id);
        mMiniBusEarningTextView = (TextView)findViewById(R.id.cmr_minibus_earnings_id);
        mBusEarningTextView = (TextView)findViewById(R.id.cmr_bus_earnings_id);

        mTotalVehiclesParkedTextView = (TextView)findViewById(R.id.cmr_total_vehicles_parked_id);
        mNumCarsParkedTextView   = (TextView)findViewById(R.id.cmr_num_cars_parked_id);
        mNumPassCarsParkedTextView   = (TextView)findViewById(R.id.cmr_num_pass_cars_parked_id);
        mNumBikesParkedTextView  = (TextView)findViewById(R.id.cmr_num_bike_parked_id);
        mNumPassBikesParkedTextView  = (TextView)findViewById(R.id.cmr_num_pass_bike_parked_id);
        mNumMiniBusParkedTextView = (TextView)findViewById(R.id.cmr_num_minibus_parked_id);
        mNumPassMiniBusParkedTextView = (TextView)findViewById(R.id.cmr_num_pass_minibus_parked_id);
        mNumBusesParkedTextView = (TextView)findViewById(R.id.cmr_num_bus_parked_id);
        mNumPassBusesParkedTextView = (TextView)findViewById(R.id.cmr_num_pass_bus_parked_id);



        mInCompleteTotalExits = (TextView)findViewById(R.id.cmr_incomplete_total_exits_id);
        mInCompleteBikeExits = (TextView)findViewById(R.id.cmr_incomplete_bike_exits_id);
        mInCompletePassBikeExits = (TextView)findViewById(R.id.cmr_incomplete_pass_bike_exits_id);
        mInCompleteCarExits = (TextView)findViewById(R.id.cmr_incomplete_car_exits_id);
        mInCompletePassCarExits = (TextView)findViewById(R.id.cmr_incomplete_pass_car_exits_id);
        mInCompleteMiniBusExits = (TextView)findViewById(R.id.cmr_incomplete_minibus_exits_id);
        mInCompletePassMiniBusExits = (TextView)findViewById(R.id.cmr_incomplete_pass_minibus_exits_id);
        mInCompleteBusExits = (TextView)findViewById(R.id.cmr_incomplete_bus_exits_id);
        mInCompletePassBusExits = (TextView)findViewById(R.id.cmr_incomplete_pass_bus_exits_id);


        mInCompleteExitsView = findViewById(R.id.cmr_incomplete_exits_card_id);

        mTotalVehiclesView       = findViewById(R.id.cmr_vehicles_inside_card_id);
        mTotalVehiclesInsideView = (TextView)findViewById(R.id.cmr_total_vehicles_inside_id);
        mTotalCarsInsideView     = (TextView)findViewById(R.id.cmr_total_cars_inside_id);
        mTotalPassCarsInsideView     = (TextView)findViewById(R.id.cmr_total_pass_cars_inside_id);
        mTotalBikesInsideView    = (TextView)findViewById(R.id.cmr_total_bikes_inside_id);
        mTotalPassBikesInsideView    = (TextView)findViewById(R.id.cmr_total_pass_bikes_inside_id);
        mTotalMiniBusInsideView    = (TextView)findViewById(R.id.cmr_total_minibus_inside_id);
        mTotalPassMiniBusInsideView   = (TextView)findViewById(R.id.cmr_total_pass_minibus_inside_id);
        mTotalBusesInsideView    = (TextView)findViewById(R.id.cmr_total_bus_inside_id);
        mTotalPassBusesInsideView   = (TextView)findViewById(R.id.cmr_total_pass_bus_inside_id);


        mPassesIssuedView     = (TextView)findViewById(R.id.cmr_passes_issued_today_id);
        mCarPassesIssuedView  = (TextView)findViewById(R.id.cmr_car_passes_issued_id);
        mBikePassesIssuedView = (TextView)findViewById(R.id.cmr_bike_passes_issued_id);
        mMiniBusPassesIssuedView = (TextView)findViewById(R.id.cmr_minibus_passes_issued_id);
        mBusPassesIssuedView = (TextView)findViewById(R.id.cmr_bus_passes_issued_id);

        mPassesReIssuedView     = (TextView)findViewById(R.id.cmr_passes_reissued_today_id);

        mPassesIssuedCardIdView = findViewById(R.id.cmr_passes_issued_card_id);

        //bikes row view
        mPassBikesParkedRowView = (TableRow)findViewById(R.id.cmr_pass_bike_parked_table_row_id);
        mPassBikesInsideRowView = (TableRow)findViewById(R.id.cmr_pass_bike_inside_table_row_id);
        mPassBikesIncompleteExitsView = (TableRow)findViewById(
                R.id.cmr_pass_bike_incomplete_exits_table_row_id);

        //operator earnings view
        mOperatorEarningsView = findViewById(R.id.cmr_per_operator_earnings_card_id);
        mOperatorPassEarningsView = findViewById(R.id.cmr_per_operator_pass_earnings_card_id);
        makeLostPassEarningsCardView = findViewById(R.id.cmr_per_operator_lost_pass_earnings_card_id);

        makeLostPassCountView = findViewById(R.id.cmr_bus_passes_reissued_table_id);


        mNextButton = (Button)findViewById(R.id.cmr_next_button);
        mPrevButton = (Button)findViewById(R.id.cmr_prev_button);
        mShowAllButton = (Button)findViewById(R.id.cmr_transactions_button);

        mFromDateView = (EditText)findViewById(R.id.cmr_from_date_id);
        mFromDateView.setInputType(InputType.TYPE_NULL);
        mFromDateView.setFocusable(false);
        mFromDateView.setClickable(true);

        mToDateView   = (EditText)findViewById(R.id.cmr_to_date_id);
        mToDateView.setInputType(InputType.TYPE_NULL);
        mToDateView.setFocusable(false);
        mToDateView.setClickable(true);

        mDateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);//this is to show to user
        mDate4jStyleFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);//this is to pass to code



        showProgress(true);




        Bundle passedParams = getIntent().getExtras();


        if(passedParams != null) {
            mDayOffset = passedParams.getInt(Constants.BUNDLE_PARAM_DAY_OFFSET);

        }

        if(mDayOffset == 0){
            mNextButton.setVisibility(View.INVISIBLE);
        }

        setDateTimeField();


        mTransactionSummaryTask = new TransactionSummaryTask(mDayOffset);
        mTransactionSummaryTask.execute((Void) null);
    }

    private void setDateTimeField() {


        Calendar newCalendar = Calendar.getInstance();
        mFromDatePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                mFromDateView.setText(mDateFormatter.format(newDate.getTime()));
                mDate4jFormattedFromDate = mDate4jStyleFormatter.format(newDate.getTime());

                if(mToDateView.getText().toString().length() != 0){
                    mToDateView.setText(null);
                }
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH),
                          newCalendar.get(Calendar.DAY_OF_MONTH));

        mFromDatePicker.getDatePicker().setMaxDate(newCalendar.getTimeInMillis());


        mToDatePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                mToDateView.setText(mDateFormatter.format(newDate.getTime()));
                mDate4jFormattedToDate = mDate4jStyleFormatter.format(newDate.getTime());

                String fromDate = mFromDateView.getText().toString();
                String toDate   = mToDateView.getText().toString();

                if(fromDate.length() != 0){
                    showProgress(true);

                    mTransactionSummaryTask = new TransactionSummaryTask(mDate4jFormattedFromDate,
                                                                         mDate4jFormattedToDate);
                    mTransactionSummaryTask.execute((Void)null);

                }
            }

        },newCalendar.get(Calendar.YEAR),
           newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        mToDatePicker.getDatePicker().setMaxDate(newCalendar.getTimeInMillis());

    }



    public void handleShowTransactionsButton(View view){
        Intent intent = new Intent(this,Reports.class);
        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_DATETIME_FROM, mFrom);
        intent.putExtra(Constants.BUNDLE_PARAM_REPORT_DATETIME_TO,mTo);
        startActivity(intent);

    }

    public void handlePrevButton(View view){
        Intent intent = new Intent(this,MainReports.class);
        intent.putExtra(Constants.BUNDLE_PARAM_DAY_OFFSET,--mDayOffset);
        startActivity(intent);
        finish();
    }

    public void handleNextButton(View view){
        Intent intent = new Intent(this,MainReports.class);
        intent.putExtra(Constants.BUNDLE_PARAM_DAY_OFFSET,++mDayOffset);
        startActivity(intent);
        finish();

    }

    public void handleFromDateOnClick(View view){
        mFromDatePicker.show();
    }

    public void handleToDateOnClick(View view){
        mToDatePicker.show();
    }

    public void handlePrintReportButton(View view){

        if(mSummary == null){

            Toast.makeText(getApplicationContext(), "No Entries Found to print",
                    Toast.LENGTH_LONG).show();


        }else {

           Toast.makeText(getApplicationContext(), "Printing Report....", Toast.LENGTH_LONG).show();
            PrinterService.startActionPrintReport(getApplicationContext(),mReportTimeFrame,
                                                  mSummary);
        }

    }


    public void updateCounters(){

    }


    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mScrollView.setVisibility(show ? View.GONE : View.VISIBLE);
            mScrollView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mScrollView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mScrollView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }



    /*
     * Represents an asynchronous Transactions Summary task used to authenticate
     * the user.
     */
    public class TransactionSummaryTask extends AsyncTask<Void, Void, Boolean> {

        private List<TransactionTbl> mTransactionsList;


        private String mFromStr, mToStr;
        private boolean mIsMiniBusParkingAllowed = false;
        private boolean mIsBusParkingAllowed = false;


        private boolean isPassSummaryAvailable = false;
        private boolean isLostPassSummaryAvailable = false;
        private boolean isOnlineSummaryAvailable = false;
        private boolean isOfflineSummaryAvailable = false;


        TransactionSummaryTask(String openingDate, String closingDate){
            String openingTimeSubStr,closingTimeSubStr;

            if((Boolean) Cache.getFromCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_IS_OPEN_24_HOURS,Constants.TYPE_BOOL)) {

                openingTimeSubStr = Constants.FULL_DAY_OPENING_TIME;
                closingTimeSubStr = Constants.FULL_DAY_CLOSING_TIME;
            }else{
                openingTimeSubStr = (String)Cache.getFromCache(getApplicationContext(),
                               Constants.CACHE_PREFIX_OPENING_TIME_QUERY,
                               Constants.TYPE_STRING);
                closingTimeSubStr = (String)Cache.getFromCache(getApplicationContext(),
                        Constants.CACHE_PREFIX_CLOSING_TIME_QUERY,
                        Constants.TYPE_STRING);

            }

            mFromStr = openingDate + " " + openingTimeSubStr;
            mToStr   = closingDate + " " + closingTimeSubStr;

            mReportTimeFrame = new DateTime(mFromStr).format("DD-MM",Locale.US) + " to "+
                                new DateTime(mToStr).format("DD-MM",Locale.US);

            mFrom = new DateTime(mFromStr).getMilliseconds(Constants.TIME_ZONE);
            mTo   = new DateTime(mToStr).getMilliseconds(Constants.TIME_ZONE);
        }

        TransactionSummaryTask(int dayOffset) {
            String openingTimeSubStr = "";
            String closingTimeSubStr = "";

            if((Boolean)Cache.getFromCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_IS_OPEN_24_HOURS,Constants.TYPE_BOOL)){
                openingTimeSubStr = Constants.FULL_DAY_OPENING_TIME;
                closingTimeSubStr = Constants.FULL_DAY_CLOSING_TIME;

                DateTime now = DateTime.today(Constants.TIME_ZONE);

                if(dayOffset < 0){
                    now = now.minusDays(Math.abs(dayOffset));

                }else if (dayOffset > 0){
                    now = now.plusDays(dayOffset);
                }

                String nowStr = now.format("YYYY-MM-DD");

                String openingTime = nowStr + " " + openingTimeSubStr;
                String closingTime = nowStr +" "  + closingTimeSubStr;

                mReportTimeFrame = new DateTime(openingTime).format("DD-MMM-YY",Locale.US);
                //mFromStr = new DateTime(openingTime).format("DD-MMM",Locale.US);
                mToStr   = closingTime;

                mTo = new DateTime(closingTime).getMilliseconds(Constants.TIME_ZONE);
                mFrom = new DateTime(openingTime).getMilliseconds(Constants.TIME_ZONE);


            }else {

                String openingDateTime = (String) Cache.getFromCache(getApplicationContext(),
                        Constants.CACHE_PREFIX_OPENING_DATE_TIME_QUERY,
                        Constants.TYPE_STRING);

                String closingDateTime = (String) Cache.getFromCache(getApplicationContext(),
                        Constants.CACHE_PREFIX_CLOSING_DATE_TIME_QUERY,
                        Constants.TYPE_STRING);


                openingTimeSubStr = (String)Cache.getFromCache(getApplicationContext(),
                                         Constants.CACHE_PREFIX_OPENING_TIME_QUERY,
                                         Constants.TYPE_STRING);

                closingTimeSubStr = (String)Cache.getFromCache(getApplicationContext(),
                                              Constants.CACHE_PREFIX_CLOSING_TIME_QUERY,
                                              Constants.TYPE_STRING);

                DateTime openingDateTimeObj = new DateTime(openingDateTime);
                DateTime closingDateTimeObj = new DateTime(closingDateTime);


                DateTime now = DateTime.today(Constants.TIME_ZONE);

                if(dayOffset < 0){
                    now = now.minusDays(Math.abs(dayOffset));

                }else if (dayOffset > 0){
                    now = now.plusDays(dayOffset);
                }


                String openingTime = now.format("YYYY-MM-DD");

                openingTime += " " + openingTimeSubStr;
                //mFromStr = new DateTime(openingTime).format("DD-MMM",Locale.US);
                mReportTimeFrame = new DateTime(openingTime).format("DD-MMM-YY",Locale.US);


                mFrom = new DateTime(openingTime).getMilliseconds(Constants.TIME_ZONE);

                if (closingDateTimeObj.getHour() < openingDateTimeObj.getHour()) {
                    DateTime closingTimeObj = now.plusDays(1);

                    String closingTime = closingTimeObj.format("YYYY-MM-DD");
                    closingTime += " " + closingTimeSubStr;

                    mTo = new DateTime(closingTime).getMilliseconds(Constants.TIME_ZONE);
                    mToStr = closingTime;

                } else {
                    String closingTime = now.format("YYYY-MM-DD") + " " + closingTimeSubStr;

                    mTo = new DateTime(closingTime).getMilliseconds(Constants.TIME_ZONE);
                    mToStr = closingTime;

                }
            }



        }



        @Override
        protected Boolean doInBackground(Void... params) {

          boolean retValue = true;

          mIsBusParkingAllowed = (Boolean)Cache.getFromCache(getApplicationContext(),
                                Constants.CACHE_PREFIX_IS_BUS_PRICING_AVAILABLE_QUERY,
                                Constants.TYPE_BOOL);

          mIsMiniBusParkingAllowed =  (Boolean)Cache.getFromCache(getApplicationContext(),
                  Constants.CACHE_PREFIX_IS_MINIBUS_PRICING_AVAILABLE_QUERY,
                  Constants.TYPE_BOOL);

          List<TransactionTbl> transactionTblList = TransactionTbl.find(TransactionTbl.class,
            "(m_exit_time >= ? AND m_exit_time <= ?) OR (m_entry_time >= ? AND m_entry_time <= ? AND m_status == ?)",
                          Long.toString(mFrom),Long.toString(mTo),
                          Long.toString(mFrom), Long.toString(mTo),
                          Integer.toString(Constants.PARKING_STATUS_ENTERED));

          List<SimplyParkTransactionTbl> spTransactionTbList =
                                       SimplyParkTransactionTbl.find(SimplyParkTransactionTbl.class,
            "(m_exit_time >= ? AND m_exit_time <= ?) OR (m_entry_time >= ? AND m_entry_time <= ? AND m_status == ?)",
                           Long.toString(mFrom),Long.toString(mTo),
                           Long.toString(mFrom), Long.toString(mTo),
                           Integer.toString(Constants.PARKING_STATUS_ENTERED));

          List<SimplyParkBookingTbl> bookingsList =
                                      SimplyParkBookingTbl.find(SimplyParkBookingTbl.class,
                                              "m_pass_issue_date >= ? AND m_pass_issue_date <= ?",
                                               Long.toString(mFrom),Long.toString(mTo));

            List<LostPassTbl> lostPassList = LostPassTbl.find(LostPassTbl.class,"m_new_pass_issue_date >=? AND m_new_pass_issue_date <=?",Long.toString(mFrom),Long.toString(mTo));

            mSummary = new TransactionSummary();

            if(transactionTblList.isEmpty() && spTransactionTbList.isEmpty() &&
                                                           bookingsList.isEmpty()&&lostPassList.isEmpty()){

             return false;
           }else{


               if(getSummary(transactionTblList)){
                   isOfflineSummaryAvailable = true;
               }
               if(getSpTransactionSummary(spTransactionTbList)){
                   isOnlineSummaryAvailable = true; //this includes bulk booking customers as well
               }

               if(getPassesIssuedSummary(bookingsList)){
                   isPassSummaryAvailable = true;
               }

                if(getLostPassReissueSummary(lostPassList)){
                    isLostPassSummaryAvailable = true;
                }


               if(isOfflineSummaryAvailable || isPassSummaryAvailable || isOnlineSummaryAvailable||isLostPassSummaryAvailable){
                   return true;
               }else{
                   return false;
               }


           }


        }

        @Override
        protected void onPostExecute(final Boolean success) {

            showProgress(false);

            if(success){
                mTodaysEarningTextView.setText(Integer.toString(mSummary.getEarnings()));
                mCarEarningTextView.setText(Integer.toString(mSummary.getCarEarnings()));
                mBikeEarningTextView.setText(Integer.toString(mSummary.getBikeEarnings()));
                if(mIsMiniBusParkingAllowed){
                    mMiniBusEarningTextView.setVisibility(View.VISIBLE);
                    mMiniBusEarningTextView.setText(Integer.toString(mSummary.getMiniBusEarnings()));
                }
                if(mIsBusParkingAllowed){
                    mBusEarningTextView.setVisibility(View.VISIBLE);
                    mBusEarningTextView.setText(Integer.toString(mSummary.getBusEarnings()));
                }

                mPassesEarningTextView.setText(Integer.toString(mSummary.getPassEarnings()));
                /*Displaying the lost pass earning*/
                mLostPassesEarningTextView.setText(Integer.toString(mSummary.getLostPassEarnings()));

                mTotalVehiclesParkedTextView.setText(Integer.toString(mSummary.
                                                                       getTotalVehiclesParked()));
                mNumCarsParkedTextView.setText(Integer.toString(mSummary.getNumCarsParked()));
                mNumPassCarsParkedTextView.setText(Integer.toString(mSummary.getNumPassCarsParked()));

                mNumBikesParkedTextView.setText(Integer.toString(mSummary.getNumBikesParked()));
                mPassBikesParkedRowView.setVisibility(View.VISIBLE);
                mNumPassBikesParkedTextView.setText(Integer.toString(mSummary.getNumPassBikesParked()));



                if(mIsMiniBusParkingAllowed){
                    mNumMiniBusParkedTextView.setVisibility(View.VISIBLE);
                    mNumMiniBusParkedTextView.setText(Integer.toString(mSummary.getNumMiniBusParked()));

                    mNumPassMiniBusParkedTextView.setVisibility(View.VISIBLE);
                    mNumPassMiniBusParkedTextView.setText(Integer.toString(mSummary.getNumPassMiniBusParked()));
                }

                if(mIsBusParkingAllowed){
                    mNumBusesParkedTextView.setVisibility(View.VISIBLE);
                    mNumBusesParkedTextView.setText(Integer.toString(mSummary.getNumBusesParked()));

                    mNumPassBusesParkedTextView.setVisibility(View.VISIBLE);
                    mNumPassBusesParkedTextView.setText(Integer.toString(mSummary.getNumPassBusesParked()));
                }


                mDateTextView.setText("Earnings Report for " + mReportTimeFrame);

                int totalInCompleteExits = mSummary.getIncompleteExits();

                if(totalInCompleteExits == 0){
                    mInCompleteExitsView.setVisibility(View.GONE);

                }else {
                    mInCompleteTotalExits.setText(Integer.toString(totalInCompleteExits));
                    mInCompleteCarExits.setText(
                            Integer.toString(mSummary.getIncompleteCarExits()));
                    mInCompletePassCarExits.setText(
                            Integer.toString(mSummary.getIncompletePassCarExits()));

                    mInCompleteBikeExits.setText(
                            Integer.toString(mSummary.getIncompleteBikeExits()));

                    mPassBikesIncompleteExitsView.setVisibility(View.VISIBLE);
                    mInCompletePassBikeExits.setText(
                            Integer.toString(mSummary.getIncompletePassBikeExits()));

                    if(mIsMiniBusParkingAllowed){
                        mInCompleteMiniBusExits.setVisibility(View.VISIBLE);
                        mInCompleteMiniBusExits.setText(
                                Integer.toString(mSummary.getIncompleteMiniBusExits()));

                        mInCompletePassMiniBusExits.setVisibility(View.VISIBLE);
                        mInCompletePassMiniBusExits.setText(
                                Integer.toString(mSummary.getIncompletePassMiniBusExits()));
                    }

                    if(mIsBusParkingAllowed){
                        mInCompleteBusExits.setVisibility(View.VISIBLE);
                        mInCompleteBusExits.setText(
                                Integer.toString(mSummary.getIncompleteBusExits()));

                        mInCompletePassBusExits.setVisibility(View.VISIBLE);
                        mInCompletePassBusExits.setText(
                                Integer.toString(mSummary.getIncompletePassBusExits()));
                    }
                }

                if(isLostPassSummaryAvailable){
                    makeLostPassCountView.setVisibility(View.VISIBLE);
                    mPassesReIssuedView.setText(String.valueOf(mSummary.getNumLostPassesIssued()));
                }

                int vehiclesInside = mSummary.getNumVehiclesInside();

                if(vehiclesInside == 0){
                    mTotalVehiclesView.setVisibility(View.GONE);
                }else {
                    mTotalVehiclesInsideView.setText(Integer.toString(vehiclesInside));
                    mTotalCarsInsideView.setText(Integer.toString(mSummary.getNumCarsInside()));
                    mTotalPassCarsInsideView.setText(Integer.toString(mSummary.getNumPassCarsInside()));
                    mTotalBikesInsideView.setText(Integer.toString(mSummary.getNumBikesInside()));
                    mPassBikesInsideRowView.setVisibility(View.VISIBLE);
                    mTotalPassBikesInsideView.setText(Integer.toString(mSummary.getNumPassBikesInside()));

                    if (mIsMiniBusParkingAllowed) {
                        mTotalMiniBusInsideView.setVisibility(View.VISIBLE);
                        mTotalMiniBusInsideView.setText(Integer.toString(mSummary.getNumMiniBusesInside()));
                        mTotalPassMiniBusInsideView.setVisibility(View.VISIBLE);
                        mTotalPassMiniBusInsideView.setText(Integer.toString(mSummary.getNumPassMiniBusesInside()));
                    }

                    if (mIsBusParkingAllowed) {
                        mTotalBusesInsideView.setVisibility(View.VISIBLE);
                        mTotalBusesInsideView.setText(Integer.toString(mSummary.getNumBusesInside()));
                        mTotalPassBusesInsideView.setVisibility(View.VISIBLE);
                        mTotalPassBusesInsideView.setText(Integer.toString(mSummary.getNumPassBusesInside()));
                    }
                    mTotalVehiclesView.setVisibility(View.VISIBLE);
                }

                if(mSummary.getNumPassesIssued() == 0){
                    mPassesIssuedCardIdView.setVisibility(View.GONE);
                }else{
                    mPassesIssuedView.setText(Integer.toString(mSummary.getNumPassesIssued()));
                    mCarPassesIssuedView.setText(Integer.toString(mSummary.getNumCarPassesIssued()));
                    mBikePassesIssuedView.setText(Integer.toString(mSummary.getNumBikesPassesIssued()));

                    if(mIsMiniBusParkingAllowed){
                        mMiniBusPassesIssuedView.setVisibility(View.VISIBLE);
                        mMiniBusPassesIssuedView.setText(Integer.toString(mSummary.getNumMiniBusPassesIssued()));
                    }

                    if(mIsBusParkingAllowed){
                        mBusPassesIssuedView.setVisibility(View.VISIBLE);
                        mBusPassesIssuedView.setText(Integer.toString(mSummary.getNumBusPassesIssued()));
                    }
                }

                if((Boolean)Cache.getFromCache(getApplicationContext(),
                                 Constants.CACHE_PREFIX_IS_GATEWISE_REPORT_REQUIRED_QUERY,
                                 Constants.TYPE_BOOL)) {

                    createEarningsTableRows(mSummary.getPerOperatorEarning());
                    createPassEarningsTableRows(mSummary.getPerOperatorPassesIssuedEarning());
                    createLostPassEarningsTableRows(mSummary.getPerOperatorLostPassesEarning());

                }

            }else{
                mDateTextView.setText("Report for " + mReportTimeFrame);
                mTodaysEarningTextView.setText("0");
                mCarEarningTextView.setText("0");
                mBikeEarningTextView.setText("0");
                mTotalVehiclesParkedTextView.setText("0");
                mPassesEarningTextView.setText("0");
                mLostPassesEarningTextView.setText("0");

                mNumCarsParkedTextView.setText("0");
                mNumPassCarsParkedTextView.setText("0");

                mNumBikesParkedTextView.setText("0");
                mNumPassBikesParkedTextView.setText("0");

                mInCompleteBikeExits.setText("0");
                mInCompletePassBikeExits.setText("0");
                mInCompleteCarExits.setText("0");
                mInCompletePassCarExits.setText("0");
                mInCompleteExitsView.setVisibility(View.GONE);

                mTotalVehiclesView.setVisibility(View.GONE);
                mPassesIssuedCardIdView.setVisibility(View.GONE);




                Toast toast = Toast.makeText(getApplicationContext(), "No Entries found",
                                              Toast.LENGTH_SHORT);
                toast.show();

            }

            //mFromDateView.requestFocus();
            //mToDateView.requestFocus();



        }

        @Override
        protected void onCancelled() {
            mTransactionSummaryTask = null;
            showProgress(false);
        }

        private void createEarningsTableRows(int[] perOperatorEarnings){

            String gateWiseEarnings = "Gate Wise Earnings";
            boolean makeEarningsCardVisible = false;
            Context context = getApplicationContext();
            int selfOperatorId = (Integer)Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_OPERATOR_QUERY,Constants.TYPE_INT);

            TableLayout table = (TableLayout) findViewById(R.id.cmr_per_operator_earnings_table_id);

            for(int i=0; i < Constants.MAX_OPERATORS; i++){

                if(perOperatorEarnings[i] == 0){
                    continue;
                }

                makeEarningsCardVisible = true;

                TableRow row = new TableRow(MainReports.this);
                TextView operatorDesc = new TextView(MainReports.this);
                String operatorDescString = (String)Cache.getFromCache(context,
                        Constants.CACHE_PREFIX_OPERATORID_DESC_QUERY+i,Constants.TYPE_STRING);
                operatorDesc.setText(operatorDescString);
                operatorDesc.setGravity(Gravity.CENTER);


                TextView operatorIncome = new TextView(MainReports.this);
                String incomeStr = Integer.toString(perOperatorEarnings[i]);
                operatorIncome.setText(incomeStr);
                operatorIncome.setGravity(Gravity.CENTER);

                if(i == selfOperatorId){
                    operatorDesc.setTypeface(Typeface.DEFAULT_BOLD);
                    operatorIncome.setTypeface(Typeface.DEFAULT_BOLD);
                }

                row.addView(operatorDesc);
                row.addView(operatorIncome);

                table.addView(row);

                gateWiseEarnings += "\n    " + operatorDescString + "\t:" + incomeStr;

            }

            if(makeEarningsCardVisible){
                mOperatorEarningsView.setVisibility(View.VISIBLE);
                mSummary.setPerOperatorEarningPrintString(gateWiseEarnings);
            }

        }
        private void createPassEarningsTableRows(int[] perOperatorPassEarnings){


            String gateWisePassEarnings = "Gate Wise Earnings (Pass)";
            Context context = getApplicationContext();
            int selfOperatorId = (Integer)Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_OPERATOR_QUERY,Constants.TYPE_INT);

            boolean makePassEarningsCardVisible = false;
            boolean makeLostPassEarningsCardVisible = false;

            TableLayout table = (TableLayout) findViewById(R.id.cmr_per_operator_pass_earnings_table_id);

            for(int i=0; i < Constants.MAX_OPERATORS; i++){

                if(perOperatorPassEarnings[i] == 0){
                    continue;
                }

                makePassEarningsCardVisible = true;

                TableRow row = new TableRow(MainReports.this);
                TextView operatorDesc = new TextView(MainReports.this);
                String operatorDescStr = (String)Cache.getFromCache(context,
                        Constants.CACHE_PREFIX_OPERATORID_DESC_QUERY+i,Constants.TYPE_STRING);
                operatorDesc.setText(operatorDescStr);
                operatorDesc.setGravity(Gravity.CENTER);


                TextView operatorIncome = new TextView(MainReports.this);
                String incomeStr = Integer.toString(perOperatorPassEarnings[i]);
                operatorIncome.setText(incomeStr);
                operatorIncome.setGravity(Gravity.CENTER);

                if(i == selfOperatorId){
                    operatorDesc.setTypeface(Typeface.DEFAULT_BOLD);
                    operatorIncome.setTypeface(Typeface.DEFAULT_BOLD);
                }

                row.addView(operatorDesc);
                row.addView(operatorIncome);

                table.addView(row);
                gateWisePassEarnings += "\n    " + operatorDescStr + ":\t" + incomeStr;


            }



            if(makePassEarningsCardVisible){
                mOperatorPassEarningsView.setVisibility(View.VISIBLE);
                mSummary.setPerOperatorPassEarningPrintString(gateWisePassEarnings);
            }
        }

        private void createLostPassEarningsTableRows(int[] perOperatorLostPassEarnings){
            String gateWiseLostPassEarnings = "Gate Wise Earnings (Lost Pass)";
            Context context = getApplicationContext();
            int selfOperatorId = (Integer)Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_OPERATOR_QUERY,Constants.TYPE_INT);

            boolean makeLostPassEarningsCardVisible = false;

            TableLayout table = (TableLayout) findViewById(R.id.cmr_per_operator_lost_pass_earnings_table_id);

            for(int i=0; i < Constants.MAX_OPERATORS; i++){

                if(perOperatorLostPassEarnings[i] == 0){
                    continue;
                }

                makeLostPassEarningsCardVisible = true;

                TableRow row = new TableRow(MainReports.this);
                TextView operatorDesc = new TextView(MainReports.this);
                String operatorDescStr = (String)Cache.getFromCache(context,
                        Constants.CACHE_PREFIX_OPERATORID_DESC_QUERY+i,Constants.TYPE_STRING);
                operatorDesc.setText(operatorDescStr);
                operatorDesc.setGravity(Gravity.CENTER);


                TextView operatorIncome = new TextView(MainReports.this);
                String incomeStr = Integer.toString(perOperatorLostPassEarnings[i]);
                operatorIncome.setText(incomeStr);
                operatorIncome.setGravity(Gravity.CENTER);

                if(i == selfOperatorId){
                    operatorDesc.setTypeface(Typeface.DEFAULT_BOLD);
                    operatorIncome.setTypeface(Typeface.DEFAULT_BOLD);
                }

                row.addView(operatorDesc);
                row.addView(operatorIncome);

                table.addView(row);
                gateWiseLostPassEarnings += "\n    " + operatorDescStr + ":\t" + incomeStr;


            }



            if(makeLostPassEarningsCardVisible){
                makeLostPassEarningsCardView.setVisibility(View.VISIBLE);
                mSummary.setPerOperatorLostPassEarningPrintString(gateWiseLostPassEarnings);
            }
        }

    }


        private boolean getSummary(List<TransactionTbl> transactionsLst) {

            int earnings = 0;
            int carEarnings = 0;
            int bikeEarnings = 0;
            int miniBusEarnings = 0;
            int busEarnings = 0;
            int lostPassEarning =0;
            int numBikesParked = 0;
            int numCarsParked = 0;

            int numMiniBusParked = 0;
            int numBusParked = 0;
            long averageCarStayTime = 0;
            long averageBikeStayTime = 0;
            long averageMiniBusStayTime = 0;
            long averageBusStayTime = 0;
            int inCompleteCarExits = 0;
            int inCompleteBikeExits = 0;
            int inCompleteMiniBusExits = 0;
            int inCompleteBusExits = 0;
            int numCarsInside = 0;
            int numBikesInside = 0;
            int numMiniBusesInside = 0;
            int numBusesInside = 0;

            boolean isParkingPrepaid = (Boolean) Cache.getFromCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_IS_PARKING_PREPAID_QUERY,
                    Constants.TYPE_BOOL);

            for (Iterator<TransactionTbl> iterator = transactionsLst.iterator();
                 iterator.hasNext(); ) {
                TransactionTbl transaction = iterator.next();

                if (transaction.getParkingStatus() ==
                        Constants.PARKING_STATUS_TRANSACTION_CANCELLED) {
                    continue;
                }


                int operatorId = transaction.getOperatorId();

                if(operatorId != Constants.INVALID_OPERATOR_ID) {
                    mSummary.addToOperatorEarnings(operatorId, (int)transaction.getParkingFees());
                }



                long exitTime = transaction.getExitTime();
                int status = transaction.getParkingStatus();



                switch(transaction.getVehicleType()){
                    case Constants.VEHICLE_TYPE_CAR:{

                        carEarnings += transaction.getParkingFees();


                        if (status == Constants.PARKING_STATUS_EXITED) {
                            numCarsParked++;
                            averageCarStayTime += exitTime - transaction.getEntryTime();
                            averageCarStayTime /= numCarsParked;
                        } else if (status == Constants.PARKING_STATUS_EXIT_NOT_RECORDED
                                && !isParkingPrepaid) {
                            inCompleteCarExits++;
                        } else if (status == Constants.PARKING_STATUS_ENTERED) {
                            numCarsInside++;
                        }

                    }
                    break;

                    case Constants.VEHICLE_TYPE_BIKE:{

                        bikeEarnings += transaction.getParkingFees();


                        if (status == Constants.PARKING_STATUS_EXITED) {
                            numBikesParked++;
                            averageBikeStayTime += exitTime - transaction.getEntryTime();
                            averageBikeStayTime /= numBikesParked;
                        } else if (status == Constants.PARKING_STATUS_EXIT_NOT_RECORDED &&
                                !isParkingPrepaid) {
                            inCompleteBikeExits++;

                        } else if (status == Constants.PARKING_STATUS_ENTERED) {
                            numBikesInside++;
                        }

                    }
                    break;

                    case Constants.VEHICLE_TYPE_MINIBUS:{

                        miniBusEarnings += transaction.getParkingFees();


                        if (status == Constants.PARKING_STATUS_EXITED) {
                            numMiniBusParked++;
                            averageMiniBusStayTime += exitTime - transaction.getEntryTime();
                            averageMiniBusStayTime /= numMiniBusParked;
                        } else if (status == Constants.PARKING_STATUS_EXIT_NOT_RECORDED
                                && !isParkingPrepaid) {
                            inCompleteMiniBusExits++;
                        } else if (status == Constants.PARKING_STATUS_ENTERED) {
                            numMiniBusesInside++;
                        }

                    }
                    break;

                    case Constants.VEHICLE_TYPE_BUS:{

                        busEarnings += transaction.getParkingFees();


                        if (status == Constants.PARKING_STATUS_EXITED) {
                            numBusParked++;
                            averageBusStayTime += exitTime - transaction.getEntryTime();
                            averageBusStayTime /= numBusParked;
                        } else if (status == Constants.PARKING_STATUS_EXIT_NOT_RECORDED
                                && !isParkingPrepaid) {
                            inCompleteBusExits++;
                        } else if (status == Constants.PARKING_STATUS_ENTERED) {
                            numBusesInside++;
                        }

                    }
                    break;
                }



            }


            //mSummary.setEarnings(earnings);
            mSummary.setAverageBikeStayTime(averageBikeStayTime);
            mSummary.setAverageCarStayTime(averageCarStayTime);
            mSummary.setBikeEarnings(bikeEarnings);
            mSummary.setCarEarnings(carEarnings);
            mSummary.setMiniBusEarnings(miniBusEarnings);
            mSummary.setBusEarnings(busEarnings);
            mSummary.setNumBikesParked(numBikesParked);
            mSummary.setNumCarsParked(numCarsParked);
            mSummary.setNumMiniBusParked(numMiniBusParked);
            mSummary.setNumBusesParked(numMiniBusParked);
            mSummary.setIncompleteBikeExits(inCompleteBikeExits);
            mSummary.setIncompleteCarExits(inCompleteCarExits);
            mSummary.setIncompleteMiniBusExits(inCompleteMiniBusExits);
            mSummary.setIncompleteBusExits(inCompleteBusExits);
            mSummary.setNumCarsInside(numCarsInside);
            mSummary.setNumBikesInside(numBikesInside);
            mSummary.setNumMiniBusesInside(numMiniBusesInside);
            mSummary.setNumBusesInside(numBusesInside);

            return true;
        }

        private boolean getSpTransactionSummary(List<SimplyParkTransactionTbl> spTransactionsLst){
            //calculate from monthly pass transactions
            int  numPassBikesParked = 0;
            int  numPassCarsParked = 0;
            long averagePassCarStayTime = 0;
            long averagePassBikeStayTime = 0;
            int inCompletePassCarExits = 0;
            int inCompletePassBikeExits = 0;
            int numPassCarsInside = 0;
            int numPassBikesInside = 0;

            int  numPassMiniBusParked = 0;
            int  numPassBusParked = 0;
            long averagePassMiniBusStayTime = 0;
            long averagePassBusStayTime = 0;
            int inCompletePassMiniBusExits = 0;
            int inCompletePassBusExits = 0;
            int numPassMiniBusInside = 0;
            int numPassBusInside = 0;


            for (Iterator<SimplyParkTransactionTbl> iterator = spTransactionsLst.iterator();
                 iterator.hasNext(); ) {
                SimplyParkTransactionTbl transaction = iterator.next();

                if(transaction.getStatus() ==
                        Constants.PARKING_STATUS_TRANSACTION_CANCELLED){
                    continue;
                }

                //earnings += transaction.getParkingFees();

                long exitTime = transaction.getExitTime();
                int  status = transaction.getStatus();
                switch (transaction.getVehicleType()) {
                    case Constants.VEHICLE_TYPE_CAR: {
                        //carEarnings += transaction.getParkingFees();

                        if (status == Constants.PARKING_STATUS_EXITED) {
                            numPassCarsParked++;
                            averagePassCarStayTime += exitTime - transaction.getEntryTime();
                            averagePassCarStayTime /= numPassCarsParked;
                        } else if (status == Constants.PARKING_STATUS_EXIT_NOT_RECORDED) {
                            inCompletePassCarExits++;
                        } else if (status == Constants.PARKING_STATUS_ENTERED) {
                            numPassCarsInside++;
                        }
                        break;
                    }
                    case Constants.VEHICLE_TYPE_BIKE:{
                        //bikeEarnings += transaction.getParkingFees();

                        if (status == Constants.PARKING_STATUS_EXITED) {
                            numPassBikesParked++;
                            averagePassBikeStayTime += exitTime - transaction.getEntryTime();
                            averagePassBikeStayTime /= numPassBikesParked;
                        } else if (status == Constants.PARKING_STATUS_EXIT_NOT_RECORDED) {
                            inCompletePassBikeExits++;

                        } else if (status == Constants.PARKING_STATUS_ENTERED) {
                            numPassBikesInside++;
                        }
                        break;
                    }
                    case Constants.VEHICLE_TYPE_MINIBUS:{
                        //bikeEarnings += transaction.getParkingFees();

                        if (status == Constants.PARKING_STATUS_EXITED) {
                            numPassMiniBusParked++;
                            averagePassMiniBusStayTime += exitTime - transaction.getEntryTime();
                            averagePassMiniBusStayTime /= numPassMiniBusParked;
                        } else if (status == Constants.PARKING_STATUS_EXIT_NOT_RECORDED) {
                            inCompletePassMiniBusExits++;

                        } else if (status == Constants.PARKING_STATUS_ENTERED) {
                            numPassMiniBusInside++;
                        }
                        break;
                    }
                    case Constants.VEHICLE_TYPE_BUS:{
                        //bikeEarnings += transaction.getParkingFees();

                        if (status == Constants.PARKING_STATUS_EXITED) {
                            numPassBusParked++;
                            averagePassBusStayTime += exitTime - transaction.getEntryTime();
                            averagePassBusStayTime /= numPassBusParked;
                        } else if (status == Constants.PARKING_STATUS_EXIT_NOT_RECORDED) {
                            inCompletePassBusExits++;

                        } else if (status == Constants.PARKING_STATUS_ENTERED) {
                            numPassBusInside++;
                        }
                        break;
                    }

                }

            }


            //mSummary.setEarnings(earnings);
            mSummary.setAveragePassBikeStayTime(averagePassBikeStayTime);
            mSummary.setAveragePassCarStayTime(averagePassCarStayTime);
            mSummary.setAveragePassMiniBusStayTime(averagePassMiniBusStayTime);
            mSummary.setAveragePassBusStayTime(averagePassBusStayTime);

            mSummary.setNumPassBikesParked(numPassBikesParked);
            mSummary.setNumPassCarsParked(numPassCarsParked);
            mSummary.setNumPassMiniBusParked(numPassMiniBusParked);
            mSummary.setNumPassBusesParked(numPassBusParked);


            mSummary.setIncompletePassBikeExits(inCompletePassBikeExits);
            mSummary.setIncompletePassCarExits(inCompletePassCarExits);
            mSummary.setIncompletePassMiniBusExits(inCompletePassMiniBusExits);
            mSummary.setIncompletePassBusExits(inCompletePassBusExits);

            mSummary.setNumPassCarsInside(numPassCarsInside);
            mSummary.setNumPassBikesInside(numPassBikesInside);
            mSummary.setNumPassMiniBusesInside(numPassMiniBusInside);
            mSummary.setNumPassBusesInside(numPassBusInside);




            return true;

        }

        private boolean getLostPassReissueSummary(List<LostPassTbl> lostPassList){
            int lostPassesEarnings  = 0;
            int numLostPassesIssued = 0;

            for(LostPassTbl lostPass:lostPassList){
                lostPassesEarnings += lostPass.getLostPassIssueFees();
                int operatorId = lostPass.getOperatorId();

                if(operatorId != Constants.INVALID_OPERATOR_ID) {
                    mSummary.addToOperatorLostPassEarnings(operatorId,
                            (int)lostPass.getLostPassIssueFees());
                }
                numLostPassesIssued++;
            }
            mSummary.setLostPassEarnings(lostPassesEarnings);
            mSummary.setNumLostPassesIssued(numLostPassesIssued);
            return true;
        }

        private boolean getPassesIssuedSummary(List<SimplyParkBookingTbl> bookingList){

            int passesEarnings  = 0;
            int numPassesIssued = 0;
            int numCarPassesIssued = 0;
            int numBikesPassesIssued = 0;

            int numMiniBusesPassesIssued = 0;
            int numBusesPassesIssued = 0;


            for (Iterator<SimplyParkBookingTbl> iterator = bookingList.iterator();
                 iterator.hasNext(); ) {
                SimplyParkBookingTbl booking = iterator.next();

                passesEarnings += booking.getParkingFees();
                int operatorId = booking.getOperatorId();

                if(operatorId != Constants.INVALID_OPERATOR_ID) {
                    mSummary.addToOperatorPassIssuedEarnings(operatorId,
                                                            (int)booking.getParkingFees());
                }
                numPassesIssued++;

                switch (booking.getVehicleType()) {
                    case Constants.VEHICLE_TYPE_CAR:
                    {
                        numCarPassesIssued++;
                        break;
                    }
                    case Constants.VEHICLE_TYPE_BIKE:
                    {
                        numBikesPassesIssued++;
                        break;
                    }
                    case Constants.VEHICLE_TYPE_MINIBUS:
                    {
                        numMiniBusesPassesIssued++;
                        break;
                    }
                    case Constants.VEHICLE_TYPE_BUS:
                    {
                        numBusesPassesIssued++;
                        break;
                    }

                }
            }

            mSummary.setPassEarnings(passesEarnings);
            mSummary.setNumPassesIssued(numPassesIssued);
            mSummary.setNumCarsPassesIssued(numCarPassesIssued);
            mSummary.setNumBikesPassesIssued(numBikesPassesIssued);
            mSummary.setNumMiniBusPassesIssued(numMiniBusesPassesIssued);
            mSummary.setNumBusesPassesIssued(numBusesPassesIssued);

            return true;

        }

    }


