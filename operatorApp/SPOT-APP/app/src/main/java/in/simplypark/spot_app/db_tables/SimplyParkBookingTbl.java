package in.simplypark.spot_app.db_tables;

import android.content.Context;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.locks.ReentrantLock;

import hirondelle.date4j.DateTime;
import in.simplypark.spot_app.utils.Cache;
import in.simplypark.spot_app.config.Constants;

/**
 * Created by root on 28/07/16.
 */
public class SimplyParkBookingTbl extends SugarRecord {
    String mBookingID;
    String mParentBookingID; //used in case this is a renewal Booking
    String mSerialNumber;
    String mEncodedPassCode;
    String mCustomerDesc;
    String mEmail;
    String mMobile;


    int mBookingType;
    long mBookingStartTime;
    long mBookingEndTime;
    long mNextInstallmentTime;
    long mNextRenewalDateTime; //relevant only for parent booking Id
    long mPassIssueDate;

    int mBookingStatus;

    int mNumBookedSlots;
    int mNumSlotsInUse;

    String mVehicleReg; //only used if numSlots booked == 1
    int mVehicleType;

    int mParkingFees;


    int mNumCarsParked;
    int mNumBikesParked;

    boolean mIsSlotOccupiedAssumed; //for metro passes, we always assume slot to be occupied
    boolean mIsSyncedWithServer;
    boolean mIsRenewalBooking;

    int mPaymentMode;

    int mOperatorId;

    boolean mIsStaffPass;

    @Ignore
    private final ReentrantLock mIncrementDecrementLock = new ReentrantLock();


    public SimplyParkBookingTbl() {
        mBookingID = "";
        mSerialNumber = "";
        mEncodedPassCode = "";
        mCustomerDesc = "";

        mBookingType = Constants.BOOKING_TYPE_NA;
        mBookingStartTime = 0;
        mBookingEndTime = 0;
        mNumBookedSlots = 0;
        mNumSlotsInUse = 0;
        mBookingStatus = Constants.BOOKING_STATUS_WAITING;
        mNumCarsParked = 0;
        mNumBikesParked = 0;
        mVehicleType = Constants.VEHICLE_TYPE_CAR;
        mIsSlotOccupiedAssumed = false;
        mVehicleReg = "";
        mParkingFees = 0;
        mNextInstallmentTime = 0;
        mPassIssueDate = DateTime.today(Constants.TIME_ZONE).getMilliseconds(Constants.TIME_ZONE);
        mMobile = "";
        mEmail = "";
        //issue109: setting the booking as NOT synced by default
        mIsSyncedWithServer = false;
        mIsRenewalBooking = false;
        mParentBookingID = "";
        mPaymentMode = Constants.PAYMENT_MODE_CASH;
        mOperatorId = Constants.INVALID_OPERATOR_ID;
        mIsStaffPass = false;

    }

    public int getOperatorId() {
        return mOperatorId;
    }

    public void setOperatorId(int mOperatorId) {
        this.mOperatorId = mOperatorId;
    }

    public void setBookingID(String bookingID) {
        mBookingID = bookingID;
    }

    public String getBookingID() {
        return mBookingID;
    }

    public void setSerialNumber(String serialNumber) {
        mSerialNumber = serialNumber;
    }

    public String getSerialNumber() {
        return mSerialNumber;
    }

    public void setEncodedPassCode(String encodedPassCode) {
        mEncodedPassCode = encodedPassCode;
    }

    public String getEncodedPassCode() {
        return mEncodedPassCode;
    }


    public void setCustomerDesc(String customerDesc) {
        mCustomerDesc = customerDesc;
    }

    public String getCustomerDesc() {
        return mCustomerDesc;
    }

    public void setBookingType(int bookingType) {
        mBookingType = bookingType;
    }

    public int getBookingType() {
        return mBookingType;
    }

    public void setBookingStartTime(long bookingStartTime) {
        mBookingStartTime = bookingStartTime;
    }

    public long getBookingStartTime() {
        return mBookingStartTime;
    }

    public void setBookingEndTime(long bookingEndTime) {
        mBookingEndTime = bookingEndTime;
    }

    public long getBookingEndTime() {
        return mBookingEndTime;
    }

    public void setNextRenewalDateTime(long nextRenewalDateTime) {
        mNextRenewalDateTime = nextRenewalDateTime;
    }

    public long getNextRenewalDateTime() {
        return mNextRenewalDateTime;
    }


    public void setNumBookedSlots(int numBookedSlots) {
        mNumBookedSlots = numBookedSlots;
    }

    public int getNumBookedSlots() {
        return mNumBookedSlots;
    }

    public void setSlotsInUse(int slotsInUse) {
        mIncrementDecrementLock.lock();
        mNumSlotsInUse = slotsInUse;
        mIncrementDecrementLock.unlock();
    }

    public int getSlotsInUse() {
        return mNumSlotsInUse;
    }

    public int getSlotsInUseFromChildBookings(){
        int numSlotsInUse = 0;
        List<SimplyParkBookingTbl> bookings = SimplyParkBookingTbl.find(SimplyParkBookingTbl.class,
                "m_parent_booking_id = ?", getBookingID());
        if(!bookings.isEmpty()) {
            for (Iterator<SimplyParkBookingTbl> iter = bookings.iterator();iter.hasNext();) {
                SimplyParkBookingTbl booking = iter.next();
                numSlotsInUse += booking.getSlotsInUse();
            }
        }

        return numSlotsInUse;

    }


    public void setBookingStatus(int status) {
        mBookingStatus = status;
    }

    public int getBookingStatus() {
        return mBookingStatus;
    }

    public void setNumCarsParked(int carsParked) {
        mNumCarsParked = carsParked;
    }

    public int getNumCarsParked() {
        return mNumCarsParked;
    }

    public void setNumBikesParked(int bikesParked) {
        mNumBikesParked = bikesParked;
    }

    public int getNumBikesParked() {
        return mNumBikesParked;
    }

    public void setVehicleType(int vehicleType) {
        mVehicleType = vehicleType;
    }

    public int getVehicleType() {
        return mVehicleType;
    }

    public void setIsSlotOccupiedAssumed(boolean isSlotOccupiedAssumed) {
        mIsSlotOccupiedAssumed = isSlotOccupiedAssumed;
    }

    public boolean getIsSlotOccupiedAssumed() {
        return mIsSlotOccupiedAssumed;
    }


    public void setVehicleReg(String vehicleReg) {
        mVehicleReg = vehicleReg;
    }

    public String getVehicleReg() {
        return mVehicleReg;
    }

    public void setParkingFees(int parkingFees) {
        mParkingFees = parkingFees;
    }

    public int getParkingFees() {
        return mParkingFees;
    }

    public void setNextInstallmentTime(long nextInstallmentTime) {
        mNextInstallmentTime = nextInstallmentTime;
    }

    public long getNextInstallmentTime() {
        return mNextInstallmentTime;
    }

    public void setIsRenewalBooking(boolean isRenewalBooking) {
        mIsRenewalBooking = isRenewalBooking;
    }

    public boolean getIsRenewalBooking() {
        return mIsRenewalBooking;
    }

    public void setParentBookingID(String parentBookingID) {
        mParentBookingID = parentBookingID;
    }

    public String getParentBookingID() {
        return mParentBookingID;
    }

    public void setPaymentMode(int paymentMode) {
        mPaymentMode = paymentMode;
    }

    public int getPaymentMode() {
        return mPaymentMode;
    }


    public void incrementSlotsInUse(Context context, int vehicleType) {

        mIncrementDecrementLock.lock();
        if (vehicleType == Constants.VEHICLE_TYPE_CAR) {
            setSlotsInUse(getSlotsInUse() + 1);
            setNumCarsParked(getNumCarsParked() + 1);

        } else {
            int slotToBikeRatio = (Integer) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_BIKES_TO_SLOT_RATIO,
                    Constants.TYPE_INT);
            setNumBikesParked(getNumBikesParked() + 1);

            if (getNumBikesParked() % slotToBikeRatio == 1) {
                setSlotsInUse(getSlotsInUse() + 1);

            }
        }

        mIncrementDecrementLock.unlock();

    }

    public void decrementSlotsInUse(Context context, int vehicleType) {


        mIncrementDecrementLock.lock();
        int numSlotsInUse = getSlotsInUse();
        if (numSlotsInUse > 0) {
            if (vehicleType == Constants.VEHICLE_TYPE_CAR) {
                setSlotsInUse(getSlotsInUse() - 1);
                setNumCarsParked(getNumCarsParked() - 1);

            } else {
                int slotToBikeRatio = (Integer) Cache.getFromCache(context,
                        Constants.CACHE_PREFIX_BIKES_TO_SLOT_RATIO,
                        Constants.TYPE_INT);


                setNumBikesParked(getNumBikesParked() - 1);

                if (getNumBikesParked() % slotToBikeRatio == 0) {
                    setSlotsInUse(getSlotsInUse() - 1);
                }
            }
        }

        mIncrementDecrementLock.unlock();

    }

    public void setPassIssueDate(long passIssueDate) {
        mPassIssueDate = passIssueDate;
    }

    public long getPassIssueDate() {
        return mPassIssueDate;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setMobile(String mobile) {
        mMobile = mobile;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setIsSyncedWithServer(boolean isSyncedWithServer) {
        mIsSyncedWithServer = isSyncedWithServer;
    }

    public boolean getIsSyncedWithServer() {
        return mIsSyncedWithServer;
    }

    public List<PendingExits> getPendingExits() {
        return (PendingExits.find(PendingExits.class, "m_booking_id = ?", mBookingID));
    }

    public boolean getIsIsStaffPass() {
        return mIsStaffPass;
    }

    public void setIsStaffPass(boolean isStaffPass) {
        this.mIsStaffPass = isStaffPass;
    }

    public PaymentInfoTbl getPaymentInfo() {
        List<PaymentInfoTbl> payments = find(PaymentInfoTbl.class,
                "m_booking_id = ?", mBookingID);

        if (payments.isEmpty()) {
            return null;
        } else {
            return payments.get(0);
        }
    }

    /*
      getDates : This function returns important dates regarding the parent booking ID.
      Essentially, it returns the booking/pass issue date and validity date
      Only called for passes issued via the app
     */

    public long[] getDates(){
        long[] dates = new long[2];
        dates[Constants.LATEST_BOOKING_ISSUE_DATE] = 0;
        dates[Constants.LATEST_BOOKING_VALIDITY_DATE] = 0;

        long issueDate    = 0;
        long validityDate = 0;

       if(mParentBookingID.isEmpty()){
           issueDate = getPassIssueDate();
           validityDate = getBookingEndTime();

           String bookingID = getBookingID();
            List<SimplyParkBookingTbl> bookings = SimplyParkBookingTbl.find(
                    SimplyParkBookingTbl.class,"m_parent_booking_id = ?",
                    bookingID);

            for (Iterator<SimplyParkBookingTbl> iter = bookings.iterator(); iter.hasNext(); ) {
                SimplyParkBookingTbl booking = iter.next();

                long expiryDateTime = booking.getBookingEndTime();

                if(validityDate < expiryDateTime){
                    validityDate = expiryDateTime;
                    issueDate    = booking.getPassIssueDate();
                }

            }

        }

        if(validityDate != 0){
            dates[Constants.LATEST_BOOKING_ISSUE_DATE] = issueDate;
            dates[Constants.LATEST_BOOKING_VALIDITY_DATE] = validityDate;
        }

        return dates;
    }
}