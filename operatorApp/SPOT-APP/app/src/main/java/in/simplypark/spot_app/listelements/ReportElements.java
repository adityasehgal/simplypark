package in.simplypark.spot_app.listelements;


import java.util.Comparator;

import in.simplypark.spot_app.config.Constants;

/**
 * Created by aditya on 06/06/16.
 */
public class ReportElements {
    private int    mIcon;
    private String mHeading;
    private String mSubHeading;
    private String mParkingFees;
    private int    mParkingStatus;

    private String  mCustomer;
    private boolean mIsSyncedWithServer;

    private long    mTime; //the basis on which to sort
    private boolean mIsSimplyParkTransaction;
    private int     mPosition; //this is the position in the transaction or simplyparkTransaction list

    public ReportElements(int icon,String heading,String subHeading,String parkingFees,
                   boolean isSyncedWithServer,boolean isSimplyParkTransaction,
                   String customer){
        mIcon        = icon;
        mHeading     = heading;
        mSubHeading  = subHeading;
        mParkingFees = parkingFees;
        mParkingStatus = Constants.PARKING_STATUS_ENTERED;
        mIsSyncedWithServer = isSyncedWithServer;
        mIsSimplyParkTransaction = isSimplyParkTransaction;
        mCustomer = customer;
        mTime = 0;
        mPosition = 0;

    }

    public ReportElements(){
        mIcon = -1;
        mHeading = "";
        mSubHeading = "";
        mParkingFees = "";
        mParkingStatus = Constants.PARKING_STATUS_NONE;
        mIsSyncedWithServer = true;
        mIsSimplyParkTransaction = false;
        mCustomer = "";
        mTime = 0;
        mPosition = 0;
    }

    public int getIcon() { return mIcon;}
    public void setIcon(int icon){ mIcon = icon;}

    public String getHeading() { return mHeading;}
    public void setHeading(String heading) { mHeading = heading;}

    public String getSubHeading() { return mSubHeading;}
    public void setSubHeading(String subHeading) { mSubHeading = subHeading;}

    public String getParkingFees() { return mParkingFees;}

    public void setParkingFess(String parkingFees) { mParkingFees = parkingFees; }

    public int getParkingStatus() { return mParkingStatus; }
    public void setParkingStatus(int parkingStatus){ mParkingStatus = parkingStatus;}

    public boolean getSyncedWithServerState() { return mIsSyncedWithServer;}
    public void setSyncedWithServerState(boolean inSync) { mIsSyncedWithServer = inSync;}

    public boolean getIsSimplyParkTransaction() { return mIsSimplyParkTransaction;}
    public void setIsSimplyParkTransaction(boolean inSimplyParkTransaction) {
        mIsSimplyParkTransaction = inSimplyParkTransaction;
    }

    public String getCustomer() { return mCustomer;}

    public void setCustomer(String customer) { mCustomer = customer; }

    public void setTime(long time){ mTime = time;}
    public long getTime(){ return mTime;}

    public void setPosition(int position){ mPosition = position;}
    public int getPosition(){ return mPosition;}

    public static final Comparator<ReportElements> ENTRY_TIME_ORDER =
            new Comparator<ReportElements>() {
                public int compare(ReportElements e1, ReportElements e2) {
                    return (int)(e2.getTime() - e1.getTime());
                }
            };



}
