package in.simplypark.spot_app.db_tables;

import com.orm.SugarRecord;


import java.util.List;

import in.simplypark.spot_app.config.Constants;

/**
 * Created by aditya on 04/05/16.
 */
public class  SpotDb extends SugarRecord {
    /* Operator realted Params */
    Boolean mIsLoggedIn;
    String  mEmail;
    int     mOperatorId;


    Boolean mIsSpaceInfoAvailable;
    /* Space Description related params */
    String  mSpaceName;
    int     mSpaceId;
    int     mSlots;

    /* SLot Occupancy level */
    int     mCurrentOccupancy;

    /* Space Timing related Params */
    Boolean mIsOpenAllDay;
    String  mOpeningHour;
    String  mClosingHour;





    public SpotDb(){
        mIsLoggedIn = false;
        mEmail = "";
        mIsSpaceInfoAvailable = false;

        mSpaceName = "";
        mSpaceId = 0;
        mSlots = 0;


        mCurrentOccupancy = 0;


        mIsOpenAllDay = false;
        mOpeningHour = "";
        mClosingHour = "";

    }

    public void setIsSpaceInfoAvailable(Boolean isSpaceInfoAvailable){
        this.mIsSpaceInfoAvailable = isSpaceInfoAvailable;
    }

    public Boolean IsSpaceInfoAvailable(){
        return mIsSpaceInfoAvailable;
    }

    public void setOperatorDetails(Boolean isLoggedIn, String email){
        this.mIsLoggedIn = isLoggedIn;
        this.mEmail      = email;

    }

    public Boolean isOperatorLoggedIn() {
        return this.mIsLoggedIn;
    }

    public String getOperatorEmail(){
        return this.mEmail;
    }

    public void setSpaceDescription(String spaceName, int spaceId, int slots){
        this.mSpaceName = spaceName;
        this.mSpaceId   = spaceId;
        this.mSlots     = slots;
    }



    public void setSpaceTimingRelatedParams(Boolean isOpenAllDay,String openingHour,
                                            String closingHour){
        this.mIsOpenAllDay = isOpenAllDay;
        this.mOpeningHour   = openingHour;
        this.mClosingHour   = closingHour;
    }



    public void setCurrentOccupancy(int currentOccupancy){
        this.mCurrentOccupancy = currentOccupancy;
    }

    public void setOperatorId(int operatorId){
        mOperatorId = operatorId;
    }

    public int getOperatorId(){
        return mOperatorId;
    }

    public PricingPerVehicleTypeTbl getPricing(int vehicleType){
        PricingPerVehicleTypeTbl pricing = null;
        switch(vehicleType){
           case Constants.VEHICLE_TYPE_CAR:
               pricing = getCarPricing();
               break;
           case Constants.VEHICLE_TYPE_BIKE:
               pricing =  getBikePricing();
               break;
           case Constants.VEHICLE_TYPE_MINIBUS:
               pricing = getMiniBusPricing();
               break;
           case Constants.VEHICLE_TYPE_BUS:
               pricing = getBusPricing();
               break;
       }

        return pricing;

    }


    private PricingPerVehicleTypeTbl getCarPricing(){
        List<PricingPerVehicleTypeTbl> pricing =
                find(PricingPerVehicleTypeTbl.class,"m_vehicle_type = ?",
                 Integer.toString(Constants.VEHICLE_TYPE_CAR));


        if(pricing.isEmpty()){
            return null;
        }else{
            return pricing.get(0);
        }

    }

    private PricingPerVehicleTypeTbl getBikePricing(){
        List<PricingPerVehicleTypeTbl> pricing =
                find(PricingPerVehicleTypeTbl.class,"m_vehicle_type = ?",
                        Integer.toString(Constants.VEHICLE_TYPE_BIKE));


        if(pricing.isEmpty()){
            return null;
        }else{
            return pricing.get(0);
        }

    }

    private PricingPerVehicleTypeTbl getMiniBusPricing(){
        List<PricingPerVehicleTypeTbl> pricing =
                find(PricingPerVehicleTypeTbl.class,"m_vehicle_type = ?",
                        Integer.toString(Constants.VEHICLE_TYPE_MINIBUS));


        if(pricing.isEmpty()){
            return null;
        }else{
            return pricing.get(0);
        }

    }

    private PricingPerVehicleTypeTbl getBusPricing(){
        List<PricingPerVehicleTypeTbl> pricing =
                find(PricingPerVehicleTypeTbl.class,"m_vehicle_type = ?",
                        Integer.toString(Constants.VEHICLE_TYPE_BUS));


        if(pricing.isEmpty()){
            return null;
        }else{
            return pricing.get(0);
        }

    }





}
