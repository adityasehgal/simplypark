package in.simplypark.spot_app.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import hirondelle.date4j.DateTime;
import in.simplypark.spot_app.R;
import in.simplypark.spot_app.adapters.CustomMonthlyPassesListAdapter;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.db_tables.SimplyParkBookingTbl;
import in.simplypark.spot_app.db_tables.SimplyParkTransactionTbl;
import in.simplypark.spot_app.listelements.MonthlyPassesReportElements;



public class MonthlyPassesBookingReport extends AppCompatActivity {

    private CustomMonthlyPassesListAdapter mCustomListAdapter;
    private ListView mListViewBookings;
    private ArrayList<MonthlyPassesReportElements> mElements;
    private View mProgressView;
    private BookingHistoryTask mBookingsHistoryTask;
    private View mAllBookingsView;
    private List<SimplyParkBookingTbl> mBookingsList;
    private Button mLostPass;

    private EditText mSearchView;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setTitle("Monthly Passes");
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_monthly_passes_booking_report);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mElements = new ArrayList<MonthlyPassesReportElements>();


        mAllBookingsView = findViewById(R.id.cltbr_all_bookings_view_id);
        mListViewBookings = (ListView) findViewById(R.id.cltbr_bookings_list_id);
        mCustomListAdapter = new CustomMonthlyPassesListAdapter(MonthlyPassesBookingReport.this,
                                                                mElements);

        mListViewBookings.setAdapter(mCustomListAdapter);
        mProgressView = findViewById(R.id.cltbr_progress_id);

        mSearchView = (EditText)findViewById(R.id.cltbr_search_id); 

        showProgress(true);

        mBookingsHistoryTask = new BookingHistoryTask();
        mBookingsHistoryTask.execute((Void) null);
        mLostPass = (Button)findViewById(R.id.ltbr_lost_pass_id);

        mSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mCustomListAdapter.getFilter().filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



    }





    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mAllBookingsView.setVisibility(show ? View.GONE : View.VISIBLE);
            mAllBookingsView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mAllBookingsView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mAllBookingsView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    public void handleLostPassButton(View view){

    }

    /**
     * Represents an asynchronous transaction history task
     * the user.
     */
    public class BookingHistoryTask extends AsyncTask<Void, Void, Boolean> {
        List<SimplyParkBookingTbl> pendingOpAppBookings_1;
        List<SimplyParkBookingTbl> pendingOpAppBookings_2;
        String text="";

       public BookingHistoryTask() {


        }


        @Override
        protected Boolean doInBackground(Void... params) {

            //mBookingsList = SimplyParkBookingTbl.find(SimplyParkBookingTbl.class,"");
            pendingOpAppBookings_1= SimplyParkBookingTbl.listAll(SimplyParkBookingTbl.class);

            pendingOpAppBookings_2 = SimplyParkBookingTbl.find(
                    SimplyParkBookingTbl.class, "m_is_synced_with_server = ?",
                    Integer.toString(0));

            mBookingsList =  SimplyParkBookingTbl.find(SimplyParkBookingTbl.class,
                                                       "m_is_renewal_booking = ?","0");

            if(addBookingRecords()) {
                return true;
            }else{
                return false;
            }


        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mBookingsHistoryTask = null;
            showProgress(false);
//            Toast.makeText(getApplicationContext(),"All list size -"+pendingOpAppBookings_1.size()+", Unsync size -  "+pendingOpAppBookings_2.size() +", Element List Size:- "+mElements.size()+text,Toast.LENGTH_SHORT).show();
            if (success) {
                if(mElements.size() != 0){
                    mCustomListAdapter.addAll(mElements);
                }else {

                    MonthlyPassesReportElements row = new MonthlyPassesReportElements();

                    row.setCustomer("-");
                    row.setBookingId("-");
                    row.setBookingStartDateTime("-");
                    row.setBookingEndDateTime("-");

                    row.setNumBookedSlots(0);
                    row.setNumSlotsInUse(0);
                    row.setNumCarsParked(0);
                    row.setNumBikesParked(0);
                    row.setPerMonthFees(0);
                    //mElements.add(row);
                    mCustomListAdapter.add(row);

                }

                mCustomListAdapter.notifyDataSetChanged();


            }
        }

        @Override
        protected void onCancelled() {
            mBookingsHistoryTask = null;
            showProgress(false);
        }


        private boolean addBookingRecords(){

            String carCount[] = {"",
                                 String.valueOf(Constants.PARKING_STATUS_ENTERED),
                                 String.valueOf(Constants.VEHICLE_TYPE_CAR)};

            String bikeCount[] = {"",
                    String.valueOf(Constants.PARKING_STATUS_ENTERED),
                    String.valueOf(Constants.VEHICLE_TYPE_BIKE)};

            for( Iterator<SimplyParkBookingTbl> iterator = mBookingsList.iterator();
                 iterator.hasNext();  ){
                SimplyParkBookingTbl booking = (SimplyParkBookingTbl)iterator.next();

                carCount[0] = booking.getBookingID();

                long carsParked = SimplyParkTransactionTbl.count(SimplyParkTransactionTbl.class,
                        "m_booking_id = ? AND m_status = ? AND m_vehicle_type = ?",carCount);

                bikeCount[0] = booking.getBookingID();

                long bikesParked = SimplyParkTransactionTbl.count(SimplyParkTransactionTbl.class,
                        "m_booking_id = ? AND m_status = ? AND m_vehicle_type = ?",bikeCount);

                long[] dates = booking.getDates();
                String issueDate;
                String validityDate;





                MonthlyPassesReportElements row = new MonthlyPassesReportElements();



                row.setCustomer(booking.getCustomerDesc());
                row.setEmail(booking.getEmail());
                row.setMobile(booking.getMobile());
                row.setBookingId(booking.getBookingID());
                row.setEncodedPassCode(booking.getEncodedPassCode());


                String entryDateTime =  DateTime.forInstant(booking.getBookingStartTime(),
                                                           Constants.TIME_ZONE).format("DD-MM-YY");

                String exitDateTime =  DateTime.forInstant(booking.getBookingEndTime(),
                        Constants.TIME_ZONE).format("DD-MM-YY");

                row.setBookingStartDateTimeLong(booking.getBookingStartTime());
                row.setBookingStartDateTime(entryDateTime);

                if(dates[0] != 0){
                    issueDate = DateTime.forInstant(dates[0],Constants.TIME_ZONE).
                            format("DD-MMM-YYYY", Locale.US);
                    row.setBookingIssueDateTime(issueDate);
                    row.setBookingIssueDateTimeLong(dates[0]);

                    validityDate  = DateTime.forInstant(dates[1],Constants.TIME_ZONE).
                            format("DD-MMM-YYYY", Locale.US);
                    row.setBookingValidityDateTime(validityDate);

                }else{
                    row.setBookingIssueDateTime("N/A");
                    row.setBookingIssueDateTimeLong(0);
                    row.setBookingValidityDateTime("N/A");
                }


                row.setBookingEndDateTime(exitDateTime);
                row.setBookingEndDateTimeLong(booking.getBookingEndTime());
                row.setNextRenewalDateTime(booking.getNextRenewalDateTime());

                row.setNumBookedSlots(booking.getNumBookedSlots());
                row.setNumSlotsInUse(booking.getSlotsInUse() +
                                     booking.getSlotsInUseFromChildBookings());
                row.setNumCarsParked((int)carsParked);
                row.setNumBikesParked((int)bikesParked);
                row.setPerMonthFees(booking.getParkingFees());
                row.setStatus((int)booking.getBookingStatus());
                row.setIsSyncedWithServer(booking.getIsSyncedWithServer());

                if(booking.getNumBookedSlots() == 1){
                    row.setVehicleReg(booking.getVehicleReg());
                    row.setVehicleType(booking.getVehicleType());
                }
                row.setIsStaffPass(booking.getIsIsStaffPass());
                text +=booking.getCustomerDesc()+":- "+booking.getIsIsStaffPass()+":- "+booking.getIsSyncedWithServer()+" ## ";
                Log.i("MonthlyPasses",text);


                mElements.add(row);

            }

            //sort based on entry time
            Collections.sort(mElements,MonthlyPassesReportElements.ISSUE_TIME_ORDER);

            return true;

        }



    }
}


