package in.simplypark.spot_app.utils;

/**
 * Created by root on 11/10/16.
 */
public class Analogics2InchFormatter extends Formatter {
    public  String font_Emphasized_On(){
        byte[] rf1 = new byte[]{(byte)27, (byte)85, (byte)49};
        String s = new String(rf1);
        return s;
    }
    public  String font_Emphasized_Off(){
        byte[] rf1 = new byte[]{(byte)27, (byte)85, (byte)48};
        String s = new String(rf1);
        return s;
    }
    public  String font_Double_Height_Width_On() {
        byte[] rf1 = new byte[]{(byte) 18, (byte) 68};
        String s = new String(rf1);
        return s;
    }

    public  String font_Double_Height_Width_Off(){
        byte[] rf1 = new byte[]{(byte)18, (byte)100};
        String s = new String(rf1);
        return s;
    }
    public  String font_Double_Height_On() {
        byte[] rf1 = new byte[]{(byte) 28};
        String s = new String(rf1);
        return s;
    }
    public  String font_Double_Height_Off(){
        byte[] rf1 = new byte[]{(byte)29};
        String s = new String(rf1);
        return s;
    }

    public  String horizontal_Tab(){
        byte[] rf1 = new byte[]{(byte)9};
        String s = new String(rf1);
        return s;
    }

    public  String barcode_Code_128_Alpha_Numerics(String value){
        int len = value.length() + 1;
        //System.out.println(len);
        String strHexNumber = Integer.toHexString(len);
        byte[] var10000 = new byte[]{Byte.valueOf(String.valueOf(len)).byteValue()};
        //System.out.println(strHexNumber);
        byte[] rf1 = new byte[]{(byte)27, (byte)90, (byte)50, Byte.parseByte(strHexNumber, 16),
                                (byte)80, Byte.parseByte("C", 16)};
        String s = new String(rf1) + value;
        //System.out.println("sdfsdfsd::" + s);
        return s;

    }

    public String font_Courier_10(String value) {
        byte[] rf1 = new byte[]{(byte)27, (byte)75, (byte)0};
        String s = new String(rf1);
        return s + value;
    }

    public String font_Courier_19(String value) {
        byte[] rf1 = new byte[]{(byte)27, (byte)75, (byte)1};
        String s = new String(rf1);
        return s + value;
    }

    public String font_Courier_20(String value) {
        byte[] rf1 = new byte[]{(byte)27, (byte)75, (byte)2};
        String s = new String(rf1);
        return s + value;
    }

    public String font_Courier_24(String value) {
        byte[] rf1 = new byte[]{(byte)27, (byte)75, (byte)3};
        String s = new String(rf1);
        return s + value;
    }

    public String font_Courier_25(String value) {
        byte[] rf1 = new byte[]{(byte)27, (byte)75, (byte)4};
        String s = new String(rf1);
        return s + value;
    }

    public String font_Courier_27(String value) {
        byte[] rf1 = new byte[]{(byte)27, (byte)75, (byte)5};
        String s = new String(rf1);
        return s + value;
    }

    public String font_Courier_29(String value) {
        byte[] rf1 = new byte[]{(byte)27, (byte)75, (byte)6};
        String s = new String(rf1);
        return s + value;
    }

    public String font_Courier_32(String value) {
        byte[] rf1 = new byte[]{(byte)27, (byte)75, (byte)7};
        String s = new String(rf1);
        return s + value;
    }

    public String font_Courier_34(String value) {
        byte[] rf1 = new byte[]{(byte)27, (byte)75, (byte)8};
        String s = new String(rf1);
        return s + value;
    }

    public String font_Courier_38(String value) {
        byte[] rf1 = new byte[]{(byte)27, (byte)75, (byte)9};
        String s = new String(rf1);
        return s + value;
    }

    public String font_Courier_42(String value) {
        byte[] rf1 = new byte[]{(byte)27, (byte)75, (byte)10};
        String s = new String(rf1);
        return s + value;
    }

    public String font_Courier_48(String value) {
        byte[] rf1 = new byte[]{(byte)27, (byte)75, (byte)11};
        String s = new String(rf1);
        return s + value;
    }

    public String font_SansSerif_32(String value) {
        byte[] rf1 = new byte[]{(byte)27, (byte)75, (byte)12};
        String s = new String(rf1);
        return s + value;
    }

    public String font_SansSerif_34(String value) {
        byte[] rf1 = new byte[]{(byte)27, (byte)75, (byte)13};
        String s = new String(rf1);
        return s + value;
    }

    public String font_SansSerif_38(String value) {
        byte[] rf1 = new byte[]{(byte)27, (byte)75, (byte)14};
        String s = new String(rf1);
        return s + value;
    }

    public String font_SansSerif_8(String value) {
        byte[] rf1 = new byte[]{(byte)27, (byte)75, (byte)15};
        String s = new String(rf1);
        return s + value;
    }

    public String carriage_Return() {
        byte[] rf1 = new byte[]{13};
        String s = new String(rf1);
        return s;
    }
}
