package in.simplypark.spot_app.utils;

import android.util.Log;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import in.simplypark.spot_app.config.Constants;

/**
 * Created by aditya on 07/05/16.
 */
public class ParkingId {

    /* The purpose of the Parking Id is to enable ANY operator to calculate the parking fees
     * without contacting the server. ParkingID is therefore the entry time encoded in base 36(as
     * we have 36 printable characters (a-z and 0-9)
     * Total Length : 6 digits
     * [OperatorId][Year][Entry Time]
     */



    //private static final AbstractMap.SimpleEntry<Character,Integer> mdecodeArray = {}

    private static int mNumberIndexStart = 0;
    private static int mAlphabetIndexStart = 10;



    private static final int mBaseYear = 2016;
    private static final long mRadix = 36;




    public static byte[] generateParkingId(int operatorId){

        byte[] parkingId = new byte[Constants.MAX_PARK_ID_LEN];

        byte operatorByte = (byte)operatorId;

        Date currentTime = new GregorianCalendar().getTime();



        //TODO: we only support 2 years in the parking ID for now
        int year = (Calendar.getInstance()).get(Calendar.YEAR);
        //byte year = (byte)((((Calendar.getInstance()).get(Calendar.YEAR)) - 1900);

        //our epoch is 1st Jan, [current year] 00:00:00
        Date epoch = (new GregorianCalendar(year,0,1,0,0,0)).getTime();

        //parkingId to store
        long time = (currentTime.getTime() - epoch.getTime())/1000;

        //we are storing the year as an offset from 2016.
        // This means we can handle bookings which started till 31st Dec, 2017
        byte yearInByte= (byte)(year - mBaseYear);

        //we only have 5 bits out of possible 8 to encode operatorId and year.
        //as 2^5 is 32 which is withing the range of 36 printable ascii characters
        //we have

        operatorByte <<=1;
        operatorByte |= yearInByte;

        parkingId[0] = Constants.mEncodeArray[operatorByte];
        int itr = 1;

        while(time > 0){
            parkingId[itr++] = Constants.mEncodeArray[(int)(time % mRadix)];
            time = time / mRadix;
        }

        while(itr < Constants.MAX_PARK_ID_LEN){
            parkingId[itr++] = Constants.mEncodeArray[0];
        }


        return parkingId;


    }

    public static Date getEntryTime(String parkingId){

        try {

            //first create the byte array back

            byte yearAndOperatorId = (byte) parkingId.charAt(0);

            if (yearAndOperatorId >= '0' && yearAndOperatorId <= '9') {
                //yearAndOperatorId = (yearAndOperatorId - '0') + mNumberIndexStart;
                yearAndOperatorId -= '0';
                yearAndOperatorId += mNumberIndexStart;
            } else {
                yearAndOperatorId -= 'a';
                yearAndOperatorId += mAlphabetIndexStart;
            }

            int year = mBaseYear + (yearAndOperatorId & 0x01);
            int operatorId = (yearAndOperatorId & 0x1E) >> 1;
            int idx = 1;
            long exitTime = 0;
            byte itr;
            byte valueAtItr;


            //the number stored is in reverse so we just convert it to base 36

            while (idx < Constants.MAX_PARK_ID_LEN) {
                itr = (byte) parkingId.charAt(idx);

                if (itr >= '0' && itr <= '9') {
                    valueAtItr = (byte) (itr - '0' + mNumberIndexStart);
                } else {
                    valueAtItr = (byte) (itr - 'a' + mAlphabetIndexStart);
                }
                exitTime += valueAtItr * radixToPowerOf(idx - 1);
                idx++;
            }

            //convert int ms
            exitTime *= 1000;

            //our epoch is 1st Jan, [current year] 00:00:00
            Date epoch = (new GregorianCalendar(year, 0, 1, 0, 0, 0)).getTime();

            long epocTime = epoch.getTime();

            exitTime += epocTime;

            return new Date(exitTime);
        }catch(Exception e){
            Log.d("SPOT-APP", "Failed to get Entry time for invalid parking ID " + parkingId);
            Log.d("SPOT-APP", "Exception ::" + e);
            return null;
        }

    }

    private static long radixToPowerOf(int y){
        long res = 1;

        if(y == 0){
            return 1;
        }

        while (y > 0) {
           res = res * mRadix;
           y--;
        }


        return res;
    }
}
