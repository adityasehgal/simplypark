package in.simplypark.spot_app.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import in.simplypark.spot_app.utils.Cache;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.printer.Printer;
import in.simplypark.spot_app.printer.PrinterService;
import in.simplypark.spot_app.R;

public class PrinterSetup extends AppCompatActivity {

    Boolean mIsSampleEntryPagePrinted = false;
    Boolean mIsSampleExitPagePrinted  = false;
    Button  mProceedButton = null;
    Button  mScanAgainButton = null;
    int     mPrinterSize = 0;
    boolean mIsPrinterRescanProcessOngoing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_printer_setup);
        setTitle("Print a Sample Entry/Exit Ticket");
        Bundle passedParams = getIntent().getExtras();


        mPrinterSize = Constants.PRINTER_SIZE_2_INCH;

        if (passedParams != null) {
            mPrinterSize = passedParams.getInt(Constants.BUNDLE_PARAM_PRINTER_SIZE);
            mIsPrinterRescanProcessOngoing = passedParams.getBoolean(
                    Constants.BUNDLE_PARAM_PRINTER_RESCAN_PROCESS_ONGOING);


        }


        Printer.getPrinter().setPrinterSize(mPrinterSize, getApplicationContext());

        showNotifications("Connected to Printer");
        mProceedButton   = (Button)findViewById(R.id.aps_proceed);
        mScanAgainButton = (Button)findViewById(R.id.aps_scan_again);




    }

    public void handlePrintSampleEntryTicket(View view){
        String dateTime = "14/02/16 15:34";
        String parkingId = "078ab14646";
        Boolean isParkAndRide= true;
        String regNumber = "4646";

        PrinterService.startActionPrintEntryTicket(getApplicationContext(),
                dateTime, parkingId, isParkAndRide, regNumber,Constants.VEHICLE_TYPE_CAR);

        mIsSampleEntryPagePrinted = true;

        if(mIsSampleExitPagePrinted){
            mProceedButton.setVisibility(View.VISIBLE);
            mScanAgainButton.setVisibility(View.VISIBLE);
        }

    }

    public void handlePrintSampleExitTicket(View view){
        String entryDateTime = "14/02/2016 15:34";
        String exitDateTime  = "14/02/2016 18:22";
        String parkingFees   = Integer.toString(40);
        String nightCharge   = "";
        String regNumber     = "4646";
        String duration      = "2 Hour(s),43 minute(s)";

        PrinterService.startActionPrintExitTicket(getApplicationContext(),
                entryDateTime, exitDateTime, parkingFees, nightCharge,regNumber,duration,
                Constants.VEHICLE_TYPE_CAR);

        mIsSampleExitPagePrinted = true;

        if(mIsSampleEntryPagePrinted){
            mProceedButton.setVisibility(View.VISIBLE);
            mScanAgainButton.setVisibility(View.VISIBLE);
        }
    }

    public void handleProceedButton(View view){
        Context context = getApplicationContext();
        Cache.setInCache(context,
                Constants.CACHE_PREFIX_IS_PRINTER_CONFIGURED,true,Constants.TYPE_BOOL);
        Cache.setInCache(context,
                Constants.CACHE_PREFIX_PRINTER_SIZE,mPrinterSize,Constants.TYPE_INT);
        Bundle b = new Bundle();
        b.putBoolean(Constants.BUNDLE_PARAM_LAST_ACTIVITY_PRINTER_SETUP,true);
        b.putBoolean(Constants.BUNDLE_PARAM_PRINTER_RESCAN_PROCESS_ONGOING,mIsPrinterRescanProcessOngoing);

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();

    }

    public void handleScanAgainButton(View view){
        Printer.getPrinter().disconnect();
        Intent intent = new Intent(this, SetupActivity.class);
        Bundle b = new Bundle();
        b.putBoolean(Constants.BUNDLE_PARAM_PRINTER_RESCAN_PROCESS_ONGOING,
                      mIsPrinterRescanProcessOngoing);
        intent.putExtras(b);
        startActivity(intent);
        finish();

    }

    public void showNotifications(String msg){
        Toast.makeText(getApplicationContext(), msg,
                Toast.LENGTH_SHORT).show();

    }
}
