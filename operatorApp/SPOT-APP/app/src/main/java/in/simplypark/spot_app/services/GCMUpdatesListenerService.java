package in.simplypark.spot_app.services;

import android.os.Bundle;

import com.google.android.gms.gcm.GcmListenerService;

import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.utils.Cache;
import in.simplypark.spot_app.utils.MiscUtils;

public class GCMUpdatesListenerService extends GcmListenerService {
    public GCMUpdatesListenerService() {
    }



    @Override
    public void onMessageReceived(String from, Bundle data) {


        try {

            int action = Integer.parseInt(data.getString("action"));
            if (action == Constants.GCM_ACTION_SYNC) {
                ServerUpdationIntentService.startActionSyncWithServer(getApplicationContext());

            }else if(action == Constants.GCM_ACTION_CANCEL){
                String parkingId = data.getString("parkingId");
                int operatorId   = Integer.parseInt(data.getString("operatorId"));
                int syncId = Integer.parseInt(data.getString("syncId"));
                String vehicleReg = data.getString("vehicleReg");
                int vehicleType = Integer.parseInt(data.getString("vehicleType"));
                long entryTime = Long.parseLong(data.getString("entryDateTime"));
                String token = data.getString("reusableToken");


                Cache.setInCache(getApplicationContext(),
                                 Constants.CACHE_PREFIX_OPERATORID_DESC_QUERY+operatorId,
                                 data.getString("operatorDesc"),Constants.TYPE_STRING);


                ServerUpdationIntentService.startActionHandleCancelNotificationFromAnotherApp(
                        getApplicationContext(), parkingId, operatorId,syncId,
                        vehicleReg,vehicleType,entryTime,token);

            }else if(action == Constants.GCM_ACTION_UPDATE){
                String parkingId = data.getString("parkingId");
                int operatorId   = Integer.parseInt(data.getString("operatorId"));
                int status       = Integer.parseInt(data.getString("status"));
                int parkingFees  = Integer.parseInt(data.getString("parkingFees"));
                int syncId = Integer.parseInt(data.getString("syncId"));
                String vehicleReg = data.getString("vehicleReg");
                int vehicleType = Integer.parseInt(data.getString("vehicleType"));
                long entryTime = Long.parseLong(data.getString("entryDateTime"));
                String token = data.getString("reusableToken");



                Cache.setInCache(getApplicationContext(),
                        Constants.CACHE_PREFIX_OPERATORID_DESC_QUERY+operatorId,
                        data.getString("operatorDesc"),Constants.TYPE_STRING);


                ServerUpdationIntentService.startActionHandleUpdateNotificationFromAnotherApp(
                        getApplicationContext(), parkingId, operatorId,status,parkingFees,syncId,
                        vehicleReg,vehicleType,entryTime,token);


            }else if(action == Constants.GCM_ACTION_BOOKING_UPDATE){
                String serialNumber   = data.getString("serialNumber");
                String bookingId      = data.getString("encodePassCode");
                String customer       = data.getString("customer");
                int    bookingType    = Integer.parseInt(data.getString("bookingType"));
                String   startTime      = data.getString("startTime");
                String   endTime        = data.getString("endTime");
                String   bookingTime   = data.getString("bookingTime");
                int    numBookedSlots = Integer.parseInt(data.getString("numBookedSlots"));
                int    syncId         = Integer.parseInt(data.getString("syncId"));
                int    status         = Integer.parseInt(data.getString("status"));
                int    spaceOwnerId   = Integer.parseInt(data.getString("spaceOwnerId"));
                boolean isPassForStaff =  (data.getBoolean("isPassForStaff"));

                ServerUpdationIntentService.startActionHandleBookingUpdateFromServer(
                            getApplicationContext(),serialNumber,bookingId,bookingType,
                            customer,startTime,endTime,bookingTime,numBookedSlots, isPassForStaff,status,
                            syncId,spaceOwnerId);

            }else if(action == Constants.GCM_ACTION_OPAPP_BOOKING_UPDATE){


                String name = data.getString("name");
                String email = data.getString("email");
                String mobile = data.getString("mobile");
                String bookingId = data.getString("bookingId");
                boolean isPassForStaff = data.getBoolean("isPassForStaff",false);

                int bookingStatus = Integer.parseInt(data.getString("bookingStatus"));
                String bookingStartDateTime = data.getString("bookingStartDateTime");
                String bookingEndDateTime = data.getString("bookingEndDateTime");
                String passIssueDate = data.getString("passIssueDate");
                String encodedPassCode = data.getString("encodedPassCode");
                String serialNumber = data.getString("serialNumber");
                String vehicleReg = data.getString("vehicleReg");
                int vehicleType = Integer.parseInt(data.getString("vehicleType"));
                int numBookedSlots = Integer.parseInt(data.getString("numBookedSlots"));
                int parkingFees = Integer.parseInt(data.getString("parkingFees"));
                int discount = Integer.parseInt(data.getString("discount"));
                int spaceOwnerId   = Integer.parseInt(data.getString("spaceOwnerId"));
                int    syncId         = Integer.parseInt(data.getString("syncId"));
                boolean isRenewal = Boolean.parseBoolean(data.getString("isRenewal","false"));
                String parentBookingId = data.getString("parentBookingId","");
                int paymentMode = Integer.parseInt(data.getString("paymentMode"));
                int operatorId = Integer.parseInt(data.getString("operatorId"));
                String invoiceId = "";
                String paymentId = "";

                if(paymentMode == Constants.PAYMENT_MODE_CARD){
                    invoiceId = data.getString("invoiceId");
                    paymentId = data.getString("paymentId");
                }


                Cache.setInCache(getApplicationContext(),
                        Constants.CACHE_PREFIX_OPERATORID_DESC_QUERY+operatorId,
                        data.getString("operatorDesc"),Constants.TYPE_STRING);


                ServerUpdationIntentService.startActionHandleOpAppBookingUpdate(
                        getApplicationContext(),name,email,mobile,
                        bookingId,bookingStatus,bookingStartDateTime,bookingEndDateTime,passIssueDate,
                        encodedPassCode,serialNumber,vehicleReg,vehicleType,numBookedSlots,parkingFees,
                        discount,spaceOwnerId,syncId,isRenewal,parentBookingId,
                        paymentMode,invoiceId,paymentId,operatorId,isPassForStaff);


            }else if(action == Constants.GCM_ACTION_SP_ENTRY){
                boolean isParkAndRide = Boolean.parseBoolean(data.getString("isParkAndRide"));
                String vehicleReg = data.getString("reg");
                String parkingId = data.getString("parkingId");
                String bookingId = data.getString("bookingId");
                int vehicleType = Integer.parseInt(data.getString("vehicleType"));
                int bookingType = Integer.parseInt(data.getString("bookingType"));
                int numOccupied = Integer.parseInt(data.getString("numOccupied"));
                int numParkedCars = Integer.parseInt(data.getString("numParkedCars"));
                int numParkedBikes = Integer.parseInt(data.getString("numParkedBikes"));
                int spaceId = Integer.parseInt(data.getString("spaceId"));
                int operatorId = Integer.parseInt(data.getString("operatorId"));
                String operatorDesc = data.getString("operatorDesc");
                int syncId = Integer.parseInt(data.getString("syncId"));
                long entryTime =  Long.parseLong(data.getString("entryTime"));


                Cache.setInCache(getApplicationContext(),
                        Constants.CACHE_PREFIX_OPERATORID_DESC_QUERY+operatorId,
                        data.getString("operatorDesc"),Constants.TYPE_STRING);


                ServerUpdationIntentService.startActionHandleSpEntryNotificationFromAnotherApp(
                        getApplicationContext(), bookingId,parkingId,
                        vehicleReg,vehicleType,syncId,entryTime,operatorId,operatorDesc,isParkAndRide);


            }else if(action == Constants.GCM_ACTION_SP_EXIT){
                String parkingId = data.getString("parkingId");
                String bookingId = data.getString("bookingId");
                int parkingFees = Integer.parseInt(data.getString("parkingFees"));
                long exitTime   = Long.parseLong(data.getString("exitTime"));
                int syncId = Integer.parseInt(data.getString("syncId"));
                int vehicleType = Integer.parseInt(data.getString("vehicleType"));
                String operatorDesc = data.getString("operatorDesc");
                int operatorId = Integer.parseInt(data.getString("operatorId"));


                Cache.setInCache(getApplicationContext(),
                        Constants.CACHE_PREFIX_OPERATORID_DESC_QUERY+operatorId,
                        data.getString("operatorDesc"),Constants.TYPE_STRING);

                ServerUpdationIntentService.startActionHandleSpExitNotificationFromAnotherApp(
                        getApplicationContext(), bookingId, parkingId, parkingFees, exitTime, syncId,
                        vehicleType,operatorId,operatorDesc);



            }else if(action == Constants.GCM_ACTION_SP_CANCEL){
                String parkingId = data.getString("parkingId");
                int operatorId   = Integer.parseInt(data.getString("operatorId"));
                int syncId = Integer.parseInt(data.getString("syncId"));
                String vehicleReg = data.getString("vehicleReg");
                int vehicleType = Integer.parseInt(data.getString("vehicleType"));
                long entryTime = Long.parseLong(data.getString("entryDateTime"));
                String bookingId = data.getString("bookingId");
                int bookingType = Integer.parseInt(data.getString("bookingType"));



                Cache.setInCache(getApplicationContext(),
                        Constants.CACHE_PREFIX_OPERATORID_DESC_QUERY+operatorId,
                        data.getString("operatorDesc"),Constants.TYPE_STRING);


                ServerUpdationIntentService.startActionHandleSpCancelNotificationFromAnotherApp(
                        getApplicationContext(), parkingId, operatorId, syncId,
                        vehicleReg,vehicleType,entryTime,bookingId,bookingType);


            }else if(action == Constants.GCM_ACTION_SP_UPDATE){
                String parkingId = data.getString("parkingId");
                int status       = Integer.parseInt(data.getString("status"));
                int parkingFees  = Integer.parseInt(data.getString("parkingFees"));
                int syncId = Integer.parseInt(data.getString("syncId"));
                int operatorId   = Integer.parseInt(data.getString("operatorId"));
                String vehicleReg = data.getString("vehicleReg");
                int vehicleType = Integer.parseInt(data.getString("vehicleType"));
                long entryTime = Long.parseLong(data.getString("entryDateTime"));
                long exitTime = Long.parseLong(data.getString("exitDateTime"));
                String bookingId = data.getString("bookingId");
                int bookingType = Integer.parseInt(data.getString("bookingType"));

                Cache.setInCache(getApplicationContext(),
                        Constants.CACHE_PREFIX_OPERATORID_DESC_QUERY+operatorId,
                        data.getString("operatorDesc"),Constants.TYPE_STRING);

                ServerUpdationIntentService.startActionHandleSpUpdateNotificationFromAnotherApp(
                        getApplicationContext(), parkingId, status,parkingFees,syncId,operatorId,
                        vehicleReg,vehicleType,entryTime,exitTime,bookingId,bookingType);

            }else if(action == Constants.GCM_ACTION_OPAPP_BOOKING_MODIFICATION){
                int operatorId = Integer.parseInt(data.getString("operatorId"));
                String  operatorDesc = data.getString("operatorDesc");
                String bookingId = data.getString("bookingId");;
                String  oldEncodedPassCode =  data.getString("oldEncodedPassCode");
                String newEncodedPassCode =  data.getString("oldEncodedPassCode");
                Long oldPassIssueDate =  data.getLong("oldEncodedPassCode");
                Long newPassIssueDate =  data.getLong("oldEncodedPassCode");
                String oldSerialNumber =  data.getString("oldEncodedPassCode");
                String newSerialNumber =  data.getString("oldEncodedPassCode");
                int paymentMode =  Integer.parseInt(data.getString("oldEncodedPassCode"));
                int lostPassFees =  Integer.parseInt(data.getString("oldEncodedPassCode"));

                Cache.setInCache(getApplicationContext(),
                        Constants.CACHE_PREFIX_OPERATORID_DESC_QUERY+operatorId,
                        data.getString("operatorDesc"),Constants.TYPE_STRING);


                ServerUpdationIntentService.startActionHandleLostPassNotificationFromAnotherApp(getApplicationContext(), operatorId, operatorDesc,
                        bookingId,oldEncodedPassCode,newEncodedPassCode,oldPassIssueDate,newPassIssueDate,oldSerialNumber,newSerialNumber,paymentMode,lostPassFees);

            }
            else {
                int numOccupied = Integer.parseInt(data.getString("numOccupied"));
                int numParkedCars = Integer.parseInt(data.getString("numParkedCars"));
                int numParkedBikes = Integer.parseInt(data.getString("numParkedBikes"));
                int spaceId = Integer.parseInt(data.getString("spaceId"));
                String token = data.getString("reusableToken");
                String operatorDesc = data.getString("operatorDesc");
                int operatorId = Integer.parseInt(data.getString("operatorId"));


                Cache.setInCache(getApplicationContext(),
                        Constants.CACHE_PREFIX_OPERATORID_DESC_QUERY+operatorId,
                        operatorDesc,Constants.TYPE_STRING);

                String parkingId = data.getString("parkingId");
                int vehicleType = Integer.parseInt(data.getString("vehicleType"));
                int syncId = Integer.parseInt(data.getString("syncId"));

                long entryTime, exitTime;


                if (action == Constants.GCM_ACTION_ENTRY) {
                    boolean isParkAndRide = Boolean.parseBoolean(data.getString("isParkAndRide"));
                    entryTime = Long.parseLong(data.getString("entryTime"));
                    String reg = data.getString("reg");


                    ServerUpdationIntentService.startActionHandleEntryNotificationFromAnotherApp(
                            getApplicationContext(), parkingId, reg, isParkAndRide, entryTime, vehicleType,
                            syncId, operatorId,operatorDesc,token);

                } else {
                    exitTime = Long.parseLong(data.getString("exitTime"));
                    entryTime = Long.parseLong(data.getString("entryTime"));
                    double parkingFees = Double.parseDouble(data.getString("parkingFees"));
                    String vehicleReg = data.getString("vehicleReg","");


                    ServerUpdationIntentService.startActionHandleExitNotificationFromAnotherApp(
                            getApplicationContext(), parkingId,parkingFees, entryTime,exitTime, vehicleType,
                            vehicleReg,syncId, operatorId,operatorDesc,token);

                }
            }


        }catch(Exception e){
            MiscUtils.errorMsgs.add("GcmUpdateListenerServer - Exception :: " + e);
        }

    }
}
