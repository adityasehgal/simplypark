package in.simplypark.spot_app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import in.simplypark.spot_app.listelements.SpacesElement;
import in.simplypark.spot_app.R;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by root on 21/11/16.
 */
public class CustomSpaceListAdapter extends ArrayAdapter<String>{
    private final Activity mContext;
    private ArrayList<SpacesElement> mElements;

    public CustomSpaceListAdapter(Activity context, ArrayList<SpacesElement> elements){
        super(context, R.layout.space_list);
        mContext = context;
        mElements = (elements);
    }

    @Override
    public int getCount() {
        return mElements.size();
    }
    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = mContext.getLayoutInflater();
        final View rowView = inflater.inflate(R.layout.space_list, null, true);
        FancyButton icon  = (FancyButton)rowView.findViewById(R.id.icon);
        TextView heading = (TextView)rowView.findViewById(R.id.item);
        TextView space_address = (TextView)rowView.findViewById(R.id.space_address);

        SpacesElement element = mElements.get(position);

        heading.setText(element.getSpaceName()+" ("+element.getmNumberOfSlots()+")");
        space_address.setText(element.getSpaceAddress());
        rowView.setTag(element.getSpaceId());
        return rowView;
    }

}

