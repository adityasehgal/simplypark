package in.simplypark.spot_app.db_tables;

import com.orm.SugarRecord;

import java.util.List;

import in.simplypark.spot_app.config.Constants;


/**
 * Created by root on 04/12/16.
 */

public class PricingPerVehicleTypeTbl extends SugarRecord {
    public int getVehicleType() {
        return mVehicleType;
    }

    public void setVehicleType(int vehicleType) {
        this.mVehicleType = vehicleType;
    }

    public boolean isPricingTiered() {
        return mIsPricingTiered;
    }

    public void setIsPricingTiered(boolean isPricingTiered) {
        this.mIsPricingTiered = isPricingTiered;
    }

    public double getHourlyPrice() {
        return mHourlyPrice;
    }

    public void setHourlyPrice(double hourlyPrice) {
        this.mHourlyPrice = hourlyPrice;
    }

    public double getDailyPrice() {
        return mDailyPrice;
    }

    public void setDailyPrice(double dailyPrice) {
        this.mDailyPrice = dailyPrice;
    }

    public double getWeeklyPrice() {
        return mWeeklyPrice;
    }

    public void setWeeklyPrice(double weeklyPrice) {
        this.mWeeklyPrice = weeklyPrice;
    }

    public double getMonthlyPrice() {
        return mMonthlyPrice;
    }

    public void setMonthlyPrice(double monthlyPrice) {
        this.mMonthlyPrice =  monthlyPrice;
    }

    public double getmYearlyPrice() {
        return mYearlyPrice;
    }

    public void setYearlyPrice(double yearlyPrice) {
        this.mYearlyPrice = yearlyPrice;
    }



    public boolean isNightChargeAvailable() {
        return mIsNightChargeAvailable;
    }

    public void setIsNightChargeAvailable(boolean mIsNightChargeAvailable) {
        this.mIsNightChargeAvailable = mIsNightChargeAvailable;
    }

    public boolean isPenaltyAvailable() {
        return mIsPenaltyAvailable;
    }

    public void setIsPenaltyAvailable(boolean mIsPenaltyAvailable) {
        this.mIsPenaltyAvailable = mIsPenaltyAvailable;
    }

    public boolean isChargePenaltyOnMonthlyPasses() {
        return mChargePenaltyOnMonthlyPasses;
    }

    public void setChargePenaltyOnMonthlyPasses(boolean mChargePenaltyOnMonthlyPasses) {
        this.mChargePenaltyOnMonthlyPasses = mChargePenaltyOnMonthlyPasses;
    }

    public PenaltyChargeTbl getPenalty(){
        PenaltyChargeTbl penaltyCharge = null;

        if(isPenaltyAvailable()){
            List<PenaltyChargeTbl> penaltyChargeTblList = PenaltyChargeTbl.find(
                    PenaltyChargeTbl.class, "m_vehicle_type = ?",Integer.toString(getVehicleType()));

            if(!penaltyChargeTblList.isEmpty()){
                penaltyCharge = penaltyChargeTblList.get(0);
            }
        }

        return penaltyCharge;
    }

    public NightChargeTbl getNightCharges(){
        NightChargeTbl nightCharge = null;

        if(isNightChargeAvailable()){
            List<NightChargeTbl> nightChargeTblList = NightChargeTbl.find(
                    NightChargeTbl.class, "m_vehicle_type = ?",Integer.toString(getVehicleType()));

            if(!nightChargeTblList.isEmpty()){
                nightCharge = nightChargeTblList.get(0);
            }
        }

        return nightCharge;
    }

    public double getLostTicketCharge() {
        return mLostTicketCharge;
    }

    public void setLostTicketCharge(double mLostTicketCharge) {
        this.mLostTicketCharge = mLostTicketCharge;
    }

    public int getStaffPassDiscountPercentage() {
        return mStaffPassDiscountPercentage;
    }

    public void setStaffPassDiscountPercentage(int staffPassDiscountPercentage) {
        this.mStaffPassDiscountPercentage = staffPassDiscountPercentage;
    }

    private boolean mIsNightChargeAvailable;
    private boolean mIsPenaltyAvailable;
    private boolean mChargePenaltyOnMonthlyPasses;
    private double mHourlyPrice;
    private double mDailyPrice;
    private double mWeeklyPrice;
    private double mMonthlyPrice;
    private double mYearlyPrice;
    private int mVehicleType;
    private boolean mIsPricingTiered;
    private double mLostTicketCharge;
    private int mStaffPassDiscountPercentage;




    public PricingPerVehicleTypeTbl(){
        mVehicleType = Constants.VEHICLE_TYPE_INVALID;
        mIsPricingTiered = false;
        mHourlyPrice = 0.00;
        mDailyPrice = 0.00;
        mWeeklyPrice = 0.00;
        mMonthlyPrice = 0.00;
        mYearlyPrice = 0.00;
        mLostTicketCharge = 0.00;
        mStaffPassDiscountPercentage = 0;


    }




}
