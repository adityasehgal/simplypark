package in.simplypark.spot_app.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;

import hirondelle.date4j.DateTime;
import in.simplypark.spot_app.utils.Cache;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.utils.SystemNotificationsReceiver;

public class AlarmHandler {

    private static int[] timer = new int[Constants.TIMER_ACTIONS_MAX];

    public static boolean isRunning(int action){
        if(timer[action] == Constants.TIMER_STATUS_RUNNING){
            return true;
        }else{
            return false;
        }
    }

    public static void setTimerRunning(int action){
        timer[action] = Constants.TIMER_STATUS_RUNNING;
    }

    public static void setTimerExpired(int action){
        timer[action] = Constants.TIMER_STATUS_NOT_RUNNING;
    }

    public AlarmHandler(){}

    public static void startAlarm(Context context, int action, int timeOutInSeconds,
                                  boolean startIfAlreadyStarted) {

        if(!startIfAlreadyStarted && isRunning(action)){
            return;
        }

        setTimerRunning(action);

        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, SystemNotificationsReceiver.class);

        intent.putExtra(Constants.BUNDLE_PARAM_TIMER_ACTION,action);
        intent.setAction(Constants.TIMER_ACTION);
        PendingIntent pendingIntent =
                PendingIntent.getBroadcast(context, 0, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar time = Calendar.getInstance();
        long startTime = System.currentTimeMillis();
        time.setTimeInMillis(startTime);
        long endTime = startTime + (timeOutInSeconds * Constants.NUM_MS_IN_SECOND);


        storeTimerStartAndEndTime(context, action, startTime, endTime);

        time.add(Calendar.SECOND, timeOutInSeconds);
        alarmManager.set(AlarmManager.RTC_WAKEUP, time.getTimeInMillis(),
                pendingIntent);

        /*if(isRepeating){
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, time.getTimeInMillis(),
                    AlarmManager.INTERVAL_DAY, pendingIntent);
        }*/
    }

    public static int startEndOfDayTimer(Context context){
        int action = Constants.TIMER_ACTION_END_OF_DAY;
        if(isRunning(action)){
            return Constants.TIMER_RET_VAL_ALREADY_RUNNING;
        }

        DateTime now = DateTime.now(Constants.TIME_ZONE);

        DateTime endOfDay;

        endOfDay = now.getEndOfDay();

        // //irrespective of the space operating hours, we have assumed that the pass starts at
        //00 and ends at 23 in time

        /*
        if((Boolean)Cache.getFromCache(context,
                Constants.CACHE_PREFIX_OPERATING_HOURS_SET_QUERY,
                Constants.TYPE_BOOL) == true) {

            boolean is24Hours = (Boolean)Cache.getFromCache(context,
                                        Constants.CACHE_PREFIX_IS_OPEN_24_HOURS,
                                        Constants.TYPE_BOOL);




            if(is24Hours){
                endOfDay = now.getEndOfDay();

            }else{

                String openingTimeSubStr = (String)Cache.getFromCache(context,
                        Constants.CACHE_PREFIX_OPENING_TIME_QUERY,
                        Constants.TYPE_STRING);

                String closingTimeSubStr = (String)Cache.getFromCache(context,
                        Constants.CACHE_PREFIX_CLOSING_TIME_QUERY,
                        Constants.TYPE_STRING);

                int fromHour = Integer.parseInt(openingTimeSubStr.substring(0,3));
                int toHour   = Integer.parseInt(closingTimeSubStr.substring(0,3));

                String closingTime = "";

                if(fromHour > toHour){
                    DateTime tempDate = now.plusDays(1);
                    closingTime = tempDate.format("YYYY-MM-DD");
                }else{
                    closingTime = now.format("YYYY-MM-DD");
                }

                closingTime += " " + closingTimeSubStr;
                endOfDay = new DateTime(closingTime);


            }
        }else{
            endOfDay = now.getEndOfDay();
        }
        */


        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, SystemNotificationsReceiver.class);

        intent.putExtra(Constants.BUNDLE_PARAM_TIMER_ACTION,action);
        intent.setAction(Constants.TIMER_ACTION);
        PendingIntent pendingIntent =
                PendingIntent.getBroadcast(context, 0, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);




        alarmManager.set(AlarmManager.RTC_WAKEUP,endOfDay.getMilliseconds(Constants.TIME_ZONE),
                             pendingIntent);

        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                                        endOfDay.getMilliseconds(Constants.TIME_ZONE),
                                        AlarmManager.INTERVAL_DAY,pendingIntent);

        setTimerRunning(action);

        return Constants.TIMER_RET_VAL_STARTED;

    }

    private static void storeTimerStartAndEndTime(Context context,
                                                  int action, long startTime, long endTime){

        switch(action){
            case Constants.TIMER_ACTION_PUSH:
               Cache.setInCache(context,Constants.CACHE_PREFIX_TIMER_ACITON_PUSH_START_TIME_QUERY,
                                startTime,Constants.TYPE_LONG);

                Cache.setInCache(context,Constants.CACHE_PREFIX_TIMER_ACITON_PUSH_END_TIME_QUERY,
                        endTime,Constants.TYPE_LONG);

                break;
        }

    }



}