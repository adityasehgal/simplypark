package in.simplypark.spot_app.printer;

import java.util.UUID;

/**
 * Created by root on 07/10/16.
 */
public class MatePrinterHandle extends PrinterHandle {
    private UUID mUUID;

    public UUID getUUID(){
        return mUUID;
    }

    public MatePrinterHandle(){
        mUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    }

}
