package in.simplypark.spot_app.db_tables;

import com.orm.SugarRecord;

import in.simplypark.spot_app.config.Constants;

/**
 * Created by root on 31/07/16.
 */
public class SimplyParkTransactionTbl extends SugarRecord {
    String  mBookingId;
    String  mParkingId;
    long    mEntryTime;
    long    mExitTime;
    String  mVehicleReg;
    int     mVehicleType;
    int     mStatus;
    int     mParkingFees;
    boolean mIsSyncedWithServer;
    boolean mIsParkAndRide;
    boolean mIsUpdated;
    int     mOperatorId;

    public SimplyParkTransactionTbl(){
        mBookingId = "";
        mParkingId = "";
        mEntryTime = 0;
        mExitTime = 0;
        mVehicleReg = "";
        mStatus = Constants.PARKING_STATUS_NONE;
        mVehicleType = Constants.VEHICLE_TYPE_CAR;
        //issue109: setting the transaction as not synced by default
        mIsSyncedWithServer = false;
        mIsParkAndRide = false;
        mParkingFees = 0;
        mIsUpdated = false;
        mOperatorId = Constants.INVALID_OPERATOR_ID;
    }

    public int getOperatorId() {
        return mOperatorId;
    }

    public void setOperatorId(int mOperatorId) {
        this.mOperatorId = mOperatorId;
    }

    public void setBookingId(String bookingId){
        mBookingId = bookingId;
    }
    public String getBookingId(){ return mBookingId;}

    public void setParkingId(String parkingId){
        mParkingId = parkingId;
    }
    public String getParkingId(){
        return mParkingId;
    }

    public void setEntryTime(long entryTime){
        mEntryTime = entryTime;
    }
    public long getEntryTime(){ return mEntryTime;}

    public void setExitTime(long exitTime){
        mExitTime = exitTime;
    }
    public long getExitTime(){ return mExitTime;}

    public void setVehicleReg(String vehicleReg){
        mVehicleReg = vehicleReg;
    }
    public String getVehicleReg(){ return mVehicleReg;}

    public void setVehicleType(int vehicleType){
        mVehicleType = vehicleType;
    }
    public int getVehicleType(){ return mVehicleType; }

    public void setStatus(int status){
        mStatus = status;
    }
    public int getStatus(){ return mStatus; }

    public void setIsSyncedWithServer(boolean isSyncedWithServer){
        mIsSyncedWithServer = isSyncedWithServer;
    }
    public boolean getIsSyncedWithServer(){ return mIsSyncedWithServer;}

    public void setIsParkAndRide(boolean isParkAndRide){
        mIsParkAndRide = isParkAndRide;
    }
    public boolean getIsParkAndRide(){ return mIsParkAndRide;}

    public void setIsUpdated(boolean isUpdated){
        mIsUpdated = isUpdated;
    }
    public boolean getIsUpdated(){ return mIsUpdated;}


    public void setParkingFees(int parkingFees){
        mParkingFees = parkingFees;
    }
    public int getParkingFees(){ return mParkingFees;}


}
