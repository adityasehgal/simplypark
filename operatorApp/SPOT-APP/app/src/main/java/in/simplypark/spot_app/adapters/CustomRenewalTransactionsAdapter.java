package in.simplypark.spot_app.adapters;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;

import hirondelle.date4j.DateTime;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.printer.PrinterService;
import in.simplypark.spot_app.R;
import in.simplypark.spot_app.db_tables.SimplyParkBookingTbl;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by root on 19/10/16.
 */
public class CustomRenewalTransactionsAdapter  extends ArrayAdapter<String> implements Filterable  {
    private final Activity mContext;
    private ArrayList<SimplyParkBookingTbl> mElements;
    private ArrayList<SimplyParkBookingTbl> mOriginalElements;
    private Filter mFilter;
    public CustomRenewalTransactionsAdapter(Activity context,
                                          ArrayList<SimplyParkBookingTbl> elements){
        super(context, R.layout.report_list);

        mContext = context;
        mElements = new ArrayList<SimplyParkBookingTbl>(elements);
        mOriginalElements = new ArrayList<SimplyParkBookingTbl>(elements);

    }

    public void add(SimplyParkBookingTbl row){
        super.add("item");
        mElements.add(row);
        mOriginalElements.add(row);

    }

    public void addAll(ArrayList<SimplyParkBookingTbl> elements){
        String []item = new String[elements.size()];
        java.util.Arrays.fill(item, "item1");
        super.addAll(item);
        mElements.addAll(elements);
        mOriginalElements.addAll(elements);

    }

    @Override
    public int getCount() {
        return mElements.size();
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent){
        LayoutInflater inflater = mContext.getLayoutInflater();
        final View rowView = inflater.inflate(R.layout.renewal_transaction_list, null, true);
        FancyButton printButton = (FancyButton) rowView.findViewById(R.id.rt_icon);
        TextView  validityPeriod = (TextView)rowView.findViewById(R.id.rt_validity_period);
        TextView  bookingId = (TextView)rowView.findViewById(R.id.rt_booking_id);
        TextView  fees = (TextView)rowView.findViewById(R.id.rt_renewal_amount);
        TextView  paymentMode = (TextView)rowView.findViewById(R.id.rt_payment_mode_id);
        View syncWithServerView = rowView.findViewById(R.id.rt_sync_state);

        final SimplyParkBookingTbl row = mElements.get(position);
        bookingId.setText(row.getBookingID());
        fees.setText(Integer.toString(row.getParkingFees()));
        if(row.getIsSyncedWithServer()){
            syncWithServerView.setBackgroundColor(ContextCompat.getColor(mContext,
                                                                         R.color.simplypark_blue3));
        }else{
            syncWithServerView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.red));
        }

        DateTime from = DateTime.forInstant(row.getBookingStartTime(), Constants.TIME_ZONE);
        DateTime  to  = DateTime.forInstant(row.getBookingEndTime(),Constants.TIME_ZONE);
        String validity = from.format("DD-MM");
        validity += " to ";
        validity += to.format("DD-MM");
        validityPeriod.setText(validity);

        if(row.getPaymentMode() == Constants.PAYMENT_MODE_CASH){
            paymentMode.setText("Payment Mode: Cash");
        }else if(row.getPaymentMode() == Constants.PAYMENT_MODE_CARD){
            paymentMode.setText("Payment Mode: Card/Wallet");
        }else if(row.getPaymentMode() == Constants.PAYMENT_MODE_ONLINE){
            paymentMode.setText("Payment Mode: Online");
        }else{
            paymentMode.setVisibility(View.INVISIBLE);
        }

        printButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String fromStr = DateTime.forInstant(row.getBookingStartTime(),
                                                        Constants.TIME_ZONE).format("DD-MM-YY");
                    String toStr = DateTime.forInstant(row.getBookingEndTime(),
                            Constants.TIME_ZONE).format("DD-MM-YY");

                    //print the monthly pass
                    PrinterService.startActionPrintGeneratedMonthlyPass(getContext(),
                            row.getVehicleReg(), row.getVehicleType(), row.getCustomerDesc(),
                            row.getEmail(), row.getMobile(),
                            fromStr, toStr,row.getBookingID(), row.getParkingFees());
                }catch(Exception e){
                    Log.d("Printer","Exception is " + e);
                }

            }
        });


       return rowView;

    }

    @Override
    public Filter getFilter()
    {
        if (mFilter == null)
            mFilter = new CustomRenewalTransactionsFilter();

        return mFilter;
    }

    private class CustomRenewalTransactionsFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            String prefix = constraint.toString().toLowerCase();

            if (prefix == null || prefix.length() == 0)
            {
                results.values = mOriginalElements;
                results.count  = mOriginalElements.size();
            }else{

                final ArrayList<SimplyParkBookingTbl> list = mOriginalElements;
                int count = list.size();
                final ArrayList<SimplyParkBookingTbl> filteredList =
                        new ArrayList<SimplyParkBookingTbl>(count);

                for (int i=0; i<count; i++)
                {
                    final SimplyParkBookingTbl element = list.get(i);
                    final String from = DateTime.forInstant(element.getBookingStartTime(),
                                                            Constants.TIME_ZONE).format("DD-MM-YY").
                                                            toLowerCase();
                    final String to = DateTime.forInstant(element.getBookingEndTime(),
                                                          Constants.TIME_ZONE).format("DD-MM-YY").
                                                          toLowerCase();
                    final String bookingId = element.getBookingID().toLowerCase();

                    if (from.startsWith(prefix))
                    {
                        filteredList.add(element);
                    }else if (to.startsWith(prefix)){
                        filteredList.add(element);
                    }else if (bookingId.startsWith(prefix)){
                        filteredList.add(element);
                    }
                }

                results.values = filteredList;
                results.count  = filteredList.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            int count = results.count;

            if(count > 0) {
                mElements = (ArrayList<SimplyParkBookingTbl>)(results.values);
                notifyDataSetChanged();
            }else{
                mElements.clear();
                //notifyDataSetInvalidated();
                notifyDataSetChanged();
            }
        }

    }

}
