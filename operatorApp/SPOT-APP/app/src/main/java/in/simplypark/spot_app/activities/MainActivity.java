package in.simplypark.spot_app.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import in.simplypark.spot_app.R;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.printer.Printer;
import in.simplypark.spot_app.services.ServerUpdationIntentService;
import in.simplypark.spot_app.utils.BatteryStatus;
import in.simplypark.spot_app.utils.Cache;
import in.simplypark.spot_app.utils.MiscUtils;
import in.simplypark.spot_app.utils.SystemNotificationsReceiver;


public class MainActivity extends AppCompatActivity {

    //UI references
    private View mMainActivityView;
    private View mProgressView;

    IntentFilter mStatusIntentFilter;
    ResponseReceiver mResponseReceiver;
    int mSpaceId;
    int mCurrentOccupancy;
    int mNumParkedCars;
    int mNumParkedBikes;
    boolean mIsSyncResetRequired = false;

    TextView mCurrentOccupancyView;
    TextView mNumParkedBikesView;
    TextView mNumParkedCarsView;
    TextView mDailyEarningsView;

    boolean mIsGlancesVisible;
    Toolbar mToolBar;
    TextView mToolBarTitle;

    private static MainActivity mThis;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.app_settings:
                Intent intent = new Intent(this,SpaceInformation.class);
                startActivity(intent);

                return true;

            case R.id.app_transactions:
                handleGenerateReportButton();
                return true;


            case R.id.app_active_monthly_passes:
                Intent bookingsIntent = new Intent(this,MonthlyPassesBookingReport.class);
                startActivity(bookingsIntent);

                return true;

            case R.id.app_generate_monthly_pass:
                Intent generatePassIntent = new Intent(this,GeneratePass.class);
                startActivity(generatePassIntent);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }



    @Override
    protected void onStop(){
        super.onStop();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(
                mResponseReceiver);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mThis = this;

        // remove title
        /*requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setTitle("Loading data from Server.....");*/






        mSpaceId = (Integer) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_SPACEID_QUERY,
                Constants.TYPE_INT);

        Bundle passedParams = getIntent().getExtras();
        mCurrentOccupancy = 0;
        boolean isCurrentOccupancyAvailable = false;
        boolean isLastActivityPrinterSetup = false;
        boolean isPrinterRescanProcessOngoing = false;


        if (passedParams != null) {
            mCurrentOccupancy = passedParams.getInt(Constants.BUNDLE_PARAM_CURRENT_OCCUPANCY);
            mNumParkedCars = passedParams.getInt(Constants.BUNDLE_PARAM_CURRENT_PARKED_CARS);
            mNumParkedBikes = passedParams.getInt(Constants.BUNDLE_PARAM_CURRENT_PARKED_BIKES);
            isPrinterRescanProcessOngoing  = passedParams.getBoolean(
                                          Constants.BUNDLE_PARAM_PRINTER_RESCAN_PROCESS_ONGOING);

            mIsSyncResetRequired = passedParams.getBoolean(
                                              Constants.BUNDLE_PARAM_IS_SYNC_RESET_REQUIRED);

            isCurrentOccupancyAvailable = true;


        }



        // The filter's action is SERVER_UPDATION_BROADCAST_RESULTS ACTION
        mStatusIntentFilter = new IntentFilter(
                Constants.SERVER_UPDATION_BROADCAST_RESULTS_ACTION);

        mResponseReceiver = new ResponseReceiver();



        setContentView(R.layout.activity_main);

        mToolBar = (Toolbar) findViewById(R.id.main_activity_toolbar);
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mToolBarTitle = (TextView) findViewById(R.id.toolbar_title);




        mMainActivityView = findViewById(R.id.main_activity_view);
        mProgressView = findViewById(R.id.main_activity_progress);


        showProgress(true);

        //setWindowTitle("Loading data from Server....");
        boolean updateOccupancyView = false;

        if (!((Boolean) Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_ISCONFIGURED_QUERY,
                Constants.TYPE_BOOL))) {
            //register the app with google notification & simplypark server
            ServerUpdationIntentService.startActionRegister(this);

        } else {
            //ServerUpdationIntentService.startActionGetSyncIdFromServer(this);
            String spaceName = (String) Cache.getFromCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_SPACE_NAME_QUERY,
                    Constants.TYPE_STRING);
            setTitle(spaceName);
            showProgress(false);
            updateOccupancyView = true;

            if((Boolean)Cache.getFromCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_OPERATING_HOURS_SET_QUERY,
                    Constants.TYPE_BOOL) == false){
                ServerUpdationIntentService.startActionGetOperatingHours(this);

            }

            if((Boolean)Cache.getFromCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_SPACE_VEHICLE_TYPE_QUERY,
                    Constants.TYPE_BOOL) == false){
                ServerUpdationIntentService.startActionIsSpaceBikeOnly(this);
            }
        }



        mCurrentOccupancyView = (TextView)findViewById(R.id.current_occupancy);
        mNumParkedBikesView   = (TextView)findViewById(R.id.bike_count);
        mNumParkedCarsView    = (TextView)findViewById(R.id.car_count);
        mDailyEarningsView    = (TextView)findViewById(R.id.earnings_count);

        mIsGlancesVisible = false;


        Context context = getApplicationContext();
        int numParkedBikes = (Integer)(Cache.getFromCache(context,
                                       Constants.CACHE_PREFIX_PARKED_BIKES_COUNT_QUERY,
                                       Constants.TYPE_INT));
        int numParkedCars = (Integer)(Cache.getFromCache(context,
                Constants.CACHE_PREFIX_PARKED_CARS_COUNT_QUERY,
                Constants.TYPE_INT));
        
        int dailyEarnings = (Integer)(Cache.getFromCache(context,
                Constants.CACHE_PREFIX_EARNINGS_PER_DAY_QUERY,
                Constants.TYPE_INT));


        mNumParkedBikesView.setText(Integer.toString(numParkedBikes));
        mNumParkedCarsView.setText(Integer.toString(numParkedCars));
        //mDailyEarningsView.setText(Integer.toString(dailyEarnings));
        mDailyEarningsView.setText(R.string.earings_view);

        if(updateOccupancyView){ updateOccupancyView(); }

        mCurrentOccupancyView.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.getInstance());
                 // Add the buttons
                builder.setPositiveButton("Calculate Occupancy?",
                                      new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK button

                        showProgress(true);
                        ServerUpdationIntentService.startActionCalculateOccupancyFromDb(
                                                                         getApplicationContext());


                        updateNotificationView("Calculating Occupancy .....");

                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();


                return false;
            }
        });


        //start push timer if required. Useful in situations when their are transactions to
        //push and the phone is restarted
        ServerUpdationIntentService.startActionStartPushTimerIfRequired(context);

        /*if(AlarmHandler.startEndOfDayTimer(getApplicationContext()) ==
                                                           Constants.TIMER_RET_VAL_STARTED){*/
        ServerUpdationIntentService.startActionEndOfDay(context);

        if(!((Boolean)Cache.getFromCache(context,
                              Constants.CACHE_PREFIX_IS_CANCELLATION_UPDATION_FEATURE_AVAIL_KNOWN,
                              Constants.TYPE_BOOL))){
            ServerUpdationIntentService.startActionIsCancellationUpdationEnabled(
                                                                         getApplicationContext());

        }
        //}
        int appType = (Integer)Cache.getFromCache(getApplicationContext(),
                Constants.CACHE_PREFIX_APP_TYPE,Constants.TYPE_INT);
        switch(appType){
            case Constants.APP_TYPE_ONLY_ENTRY_ENABLED:{
                ImageButton exitButton = (ImageButton)findViewById(R.id.exit_button_id);
                exitButton.setVisibility(View.INVISIBLE);
            }
            break;
            case Constants.APP_TYPE_ONLY_EXIT_ENABLED:{
                ImageButton entryButton = (ImageButton)findViewById(R.id.entry_button_id);
                entryButton.setVisibility(View.INVISIBLE);

            }
            break;
        }

        //Added a hack for NDMC
        switch(mSpaceId){
            case Constants.NDMC_HACK_CAR_SPACE_ID:
                Cache.setInCache(context,Constants.CACHE_PREFIX_IS_BIKE_PRICING_AVAILABLE_QUERY,
                                  false,Constants.TYPE_BOOL);
                break;
            case Constants.NDMC_HACK_BIKE_SPACE_ID:
                Cache.setInCache(context,Constants.CACHE_PREFIX_IS_CAR_PRICING_AVAILABLE_QUERY,
                        false,Constants.TYPE_BOOL);
                break;
        }

    }
/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    */



    @Override
    protected void onStart() {
        super.onStart();

        /*Cache.setInCache(getApplicationContext(),
                Constants.CACHE_PREFIX_IS_PRINTER_CONFIGURED,true,Constants.TYPE_BOOL);*/

        //if(Printer.getPrinter().getState() != Constants.PRINTER_STATE_CONNECTED)
         if((Boolean)Cache.getFromCache(getApplicationContext(),
                 Constants.CACHE_PREFIX_IS_PRINTER_CONFIGURED,Constants.TYPE_BOOL)){
           //if(true){

            Context context = getApplicationContext();
            int printerSize = (Integer) Cache.getFromCache(context,
                                                           Constants.CACHE_PREFIX_PRINTER_SIZE,
                                                           Constants.TYPE_INT);
            //printerSize = Constants.PRINTER_SIZE_2_INCH;
            Printer.getPrinter().setPrinterSize(printerSize, context);

            //connect to printer as well
            String macAddress = (String) Cache.getFromCache(context,
                    Constants.CACHE_PREFIX_BT_MACADDRESS_QUERY,
                    Constants.TYPE_STRING);
            Printer.getPrinter().connect(macAddress);

            updateNotificationView("Connected to Printer");

            Printer.getPrinter().setState(Constants.PRINTER_STATE_CONNECTED);
        }/*else{
             Printer.getPrinter().setPrinterSize(Constants.PRINTER_SIZE_2_INCH,
                     getApplicationContext());

        }*/


        LocalBroadcastManager.getInstance(this).registerReceiver(
                mResponseReceiver,
                mStatusIntentFilter);


        ImageView internetStatus = (ImageView) findViewById(R.id.internet_status);


        if (SystemNotificationsReceiver.isConnected(getApplicationContext())) {
            internetStatus.setImageResource(R.drawable.wifi_green);

        } else {
            internetStatus.setImageResource(R.drawable.wifi_red);
        }

        ImageView bluetoothStatus = (ImageView) findViewById(R.id.bluetooth_status);

        if (isBluetoothEnabled()) {
            bluetoothStatus.setImageResource(R.drawable.bluetooth_enabled);
        } else {
            bluetoothStatus.setImageResource(R.drawable.bluetooth_disabled);
        }

        BatteryStatus battery = new BatteryStatus(getApplicationContext());

        ImageView batteryStatus = (ImageView) findViewById(R.id.battery_status);
        showBatteryIcon(batteryStatus, battery.getBatteryPercentage());

        ImageView charging = (ImageView) findViewById(R.id.charger_status);
        showChargingIcon(charging, battery.isBatteryCharging());

        if(mIsSyncResetRequired){
            ServerUpdationIntentService.startActionResetSyncWithServer(getApplicationContext());
            showProgress(true);
        }else {
            //onStart, try and get in sync with server
            ServerUpdationIntentService.startActionGetSyncIdFromServer(getApplicationContext());
            //ServerUpdationIntentService.startActionSyncWithServer(getApplicationContext());
        }

        updateOccupancyView();


    }

    /**
     * Connectivity Change Broadcast receiver will use this to change
     * the text view
     *
     * @return MainActivity instance
     */

    public static MainActivity getInstance() {
        return mThis;
    }

    public void handleConnectivityChangeEvent(final int action) {

        MainActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                ImageView iStatusView = (ImageView) findViewById(R.id.internet_status);

                if (action == Constants.INTERNET_CONNECTIVITY_DOWN) {
                    iStatusView.setImageResource(R.drawable.wifi_red);
                } else if (action == Constants.INTERNET_CONNECTIVITY_UP) {
                    iStatusView.setImageResource(R.drawable.wifi_green);
                }
            }
        });
    }

    public void handleBluetoothChangeEvent(final int action) {

        MainActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                ImageView iStatusView = (ImageView) findViewById(R.id.bluetooth_status);

                if (action == Constants.BLUETOOTH_CONNECTIVITY_DOWN) {
                    iStatusView.setImageResource(R.drawable.bluetooth_red);
                } else if (action == Constants.BLUETOOTH_CONNECTIVITY_UP) {
                    iStatusView.setImageResource(R.drawable.bluetooth_green);
                }
            }
        });
    }

    public void handleBatteryEvent() {

        MainActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                BatteryStatus battery = new BatteryStatus(getApplicationContext());

                ImageView batteryStatus = (ImageView) findViewById(R.id.battery_status);
                showBatteryIcon(batteryStatus, battery.getBatteryPercentage());

                ImageView charging = (ImageView) findViewById(R.id.charger_status);
                showChargingIcon(charging, battery.isBatteryCharging());

            }
        });
    }

    public void handleDayOver() {

        MainActivity.this.runOnUiThread(new Runnable() {
            public void run() {

                mDailyEarningsView.setText(R.string.earings_view);
                boolean isMultiDayParkingsPossible =
                        (Boolean) Cache.getFromCache(getApplicationContext(),
                                Constants.CACHE_PREFIX_IS_MULTI_DAY_PARKING_POSSIBLE_QUERY,
                                Constants.TYPE_BOOL);

                if (!isMultiDayParkingsPossible) {
                    MiscUtils.resetOccupancyCount(getApplicationContext());
                    updateOccupancyView();

                    ServerUpdationIntentService.startActionEndOfDay(getApplicationContext());
                }
            }
        });
    }

    private void setTitle(String title){
        mToolBarTitle.setText(title);
    }

    private boolean isBluetoothEnabled() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            return false;
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                return false;
            }
        }
        return true;
    }

    private void showBatteryIcon(ImageView view, int percentage) {
        if (percentage <= Constants.BATTERY_STATUS_20) {
            view.setImageResource(R.drawable.battery_status_20);
        } else if (percentage <= Constants.BATTERY_STATUS_40) {
            view.setImageResource(R.drawable.battery_status_40);
        } else if (percentage <= Constants.BATTERY_STATUS_60) {
            view.setImageResource(R.drawable.battery_status_60);
        } else if (percentage <= Constants.BATTERY_STATUS_80) {
            view.setImageResource(R.drawable.battery_status_80);
        } else {
            view.setImageResource(R.drawable.battery_status_full);
        }
    }

    private void showChargingIcon(ImageView view, boolean isCharging) {

        if (isCharging) {
            view.setImageResource(R.drawable.charger);
        } else {
            view.setVisibility(View.INVISIBLE);
        }

    }


    private void updateOccupancyView(){
        Context context = getApplicationContext();
        MiscUtils.updateOccupancyView(context, mCurrentOccupancyView);

    }


    private void updateNotificationView(String text) {
        /*TextView notification = (TextView) findViewById(R.id.notifications);
        notification.setText(text);
        notification.setVisibility(View.VISIBLE);*/
        Context context = getApplicationContext();
        //CharSequence text = "Hello toast!";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        //toast.setGravity(Gravity.TOP| Gravity.LEFT, 0, 0);
        toast.show();
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void rotateTextView(final TextView view,final int rotate) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            view.setRotation(rotate);

        } else {
            RotateAnimation animation = new RotateAnimation(0,rotate);
            animation.setDuration(100);
            animation.setFillAfter(true);
            view.startAnimation(animation);

        }
    }


    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mMainActivityView.setVisibility(show ? View.GONE : View.VISIBLE);
            mMainActivityView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mMainActivityView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mMainActivityView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private void setWindowTitle(String title){
        /*TextView titleView = (TextView)findViewById(R.id.titleText);
        titleView.setText(title);*/
    }

    //Entry Button code
    public void handleEntryButton(View view) {
        Intent intent = new Intent(this, EntryButtonActivity.class);
        startActivityForResult(intent, Constants.ENTRY_REQUEST_CODE);

    }

    //Exit Button code
    public void handleExitButton(View view) {
        Intent intent = new Intent(this, ExitButtonActivity.class);
        startActivityForResult(intent, Constants.EXIT_REQUEST_CODE);

    }


    //SimplyPark Button code
    public void handleSimplyParkButton(View view) {

        //Create a new Intent to scan QR Code
        Intent scanActivity = new Intent(Constants.SIMPLYPARK_BOOKING_QR_CODE_SCAN);

        try {
            startActivityForResult(scanActivity, Constants.SIMPLYPARK_BOOKING_SCANNING_REQUEST_CODE);

        } catch (ActivityNotFoundException activity) {
            Log.e("QrCodeScanning", "Appropriate APP(QrCode Private) is NOT installed");
        }

    }

    //current occupany view
    public void handleOccupancyButton(View view){

        if(mIsGlancesVisible) {
            //mNumParkedBikesView.setVisibility(View.INVISIBLE);
            //mNumParkedCarsView.setVisibility(View.INVISIBLE);
            mDailyEarningsView.setVisibility(View.INVISIBLE);

            mIsGlancesVisible = false;
        }else{
            //mNumParkedBikesView.setVisibility(View.VISIBLE);
            //mNumParkedCarsView.setVisibility(View.VISIBLE);
            mDailyEarningsView.setVisibility(View.VISIBLE);

            mIsGlancesVisible = true;

        }
    }

    public void handleGenerateReportButton(){

        if((Boolean)Cache.getFromCache(getApplicationContext(),
                           Constants.CACHE_PREFIX_OPERATING_HOURS_SET_QUERY,
                           Constants.TYPE_BOOL) == true) {
            Intent reportActivity = new Intent(this, MainReports.class);
            startActivity(reportActivity);
        }else{
            updateNotificationView("Space Operating Hours are not set. Please connect to Internet");
        }


    }

    @Override
    /**
     * Reads data scanned by user and returned by QR Droid
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.ENTRY_REQUEST_CODE) {

            if (resultCode == RESULT_OK) {
                updateOccupancyView();
            } else {
                //TODO: fill the textView with Error
            }

        } else if (requestCode == Constants.EXIT_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                updateOccupancyView();
            } else {
                //TODO: fill the textView with Error
            }

        } else if (Constants.SIMPLYPARK_BOOKING_SCANNING_REQUEST_CODE == requestCode &&
                null != data && data.getExtras() != null) {
            //Read result from QR Droid (it's stored in la.droid.qr.result)
            String result = data.getExtras().getString(Constants.SIMPLYPARK_BOOKING_SCANNING_RESULT);

            if (resultCode != RESULT_OK || null == result || result.length() == 0) {
                Log.d("QRCODE RESULTS", "failed");

            }
            //Just set result to EditText to be able to view it
            Log.d("QRCODE RESULTS", "result");

            //mSpaceId = 47;
            String url = "https://www.simplypark.in/bookings/log_entry_exit/eng/" +
                    Integer.toString(mSpaceId) + "/" + result;

            Uri uri = Uri.parse(url);

            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);

        }
    }


    private int handleServerSyncId(int serverSyncId) {

        MiscUtils.lockSyncId();
        int syncState;
        try {

            int syncIdAtClient = (Integer) Cache.getFromCache(getApplicationContext(),
                    Constants.CACHE_PREFIX_SYNC_ID_QUERY,
                    Constants.TYPE_INT);

            if (syncIdAtClient == serverSyncId) {
                syncState = Constants.IN_SYNC;
            } else if (syncIdAtClient < serverSyncId) {
                syncState = Constants.OUT_OF_SYNC_PULL_REQUIRED;
            } else {
                syncState = Constants.OUT_OF_SYNC_PUSH_REQUIRED;
            }


            switch (syncState) {
                case Constants.SYNC_STATE_IN_SYNC:
                    //set up sync state
                    Cache.setInCache(getApplicationContext(),
                            Constants.CACHE_PREFIX_SYNC_STATE_QUERY,
                            Constants.SYNC_STATE_IN_SYNC,
                            Constants.TYPE_INT);
                    break;


                case Constants.OUT_OF_SYNC_PULL_REQUIRED: {
                    //set up sync state
                    Cache.setInCache(getApplicationContext(),
                            Constants.CACHE_PREFIX_SYNC_STATE_QUERY,
                            Constants.SYNC_STATE_IN_PROGRESS_PULL,
                            Constants.TYPE_INT);

                    ServerUpdationIntentService.startActionGetUpdatesFromServer(
                            getApplicationContext(), syncIdAtClient);

                }

                break;

                case Constants.OUT_OF_SYNC_PUSH_REQUIRED: {
                    //to be safe, start both push and all
                    Cache.setInCache(getApplicationContext(),
                            Constants.CACHE_PREFIX_SYNC_STATE_QUERY,
                            Constants.SYNC_STATE_BOTH_PUSH_AND_PULL_IN_PROGRESS,
                            Constants.TYPE_INT);
                    ServerUpdationIntentService.startActionSyncWithServer(
                            getApplicationContext());

                }
                break;

            }
        }finally{
            MiscUtils.unlockSyncId();
        }

        return syncState;


    }

    /**********************************************************************************************
     * Sub Class to Handle Results of the background activites
     *********************************************************************************************/

    // Broadcast receiver for receiving status updates from the IntentService
    public class ResponseReceiver extends BroadcastReceiver {

        public ResponseReceiver(){

        }
        // Called when the BroadcastReceiver gets an Intent it's registered to receive
        @Override
        public void onReceive(Context context, Intent intent) {
            // Enter relevant functionality for when the last widget is recieved
            final String action = intent.getAction();
            Bundle extras = getIntent().getExtras();
            final int triggerRequest = intent.getIntExtra(Constants.TRIGGER_ACTION_NAME,
                    Constants.TRIGGER_ACTION_NONE);
            final int status = intent.getIntExtra(Constants.SERVER_UPDATION_BROADCAST_RESULTS_STATUS,
                    Constants.NOK);

            Log.d("INSIDE", action);
            if (action.equals(Constants.SERVER_UPDATION_BROADCAST_RESULTS_ACTION)) {
                if (status == Constants.OK) {
                    switch (triggerRequest) {
                        case Constants.TRIGGER_ACTION_REGISTER:{
                            final int syncId = intent.getIntExtra(Constants.BUNDLE_PARAM_SYNC_ID,
                                       Constants.INVALID_SYNC_ID);

                            String spaceName = (String) Cache.getFromCache(getApplicationContext(),
                                    Constants.CACHE_PREFIX_SPACE_NAME_QUERY,
                                    Constants.TYPE_STRING);
                            setTitle(spaceName);

                            if(syncId == 0){
                                showProgress(false);
                                updateOccupancyView();
                                updateNotificationView("Synced with Server");
                            }else {
                                updateNotificationView("Downloading updates from Server....");
                            }

                            //Commented as onStart also calls getSyncId and then will take
                            //corresponding action

                        }
                        break;
                        case Constants.TRIGGER_ACTION_GET_SYNC_ID_FROM_SERVER:
                        case Constants.TRIGGER_ACTION_ENTRY:
                        case Constants.TRIGGER_ACTION_SP_ENTRY:
                        case Constants.TRIGGER_ACTION_EXIT:
                        case Constants.TRIGGER_ACTION_SP_EXIT:
                        case Constants.TRIGGER_ACTION_CANCEL_TRANSACTION:
                        case Constants.TRIGGER_ACTION_UPDATE_TRANSACTION:
                        case Constants.TRIGGER_ACTION_SP_CANCEL_TRANSACTION:
                        case Constants.TRIGGER_ACTION_SP_UPDATE_TRANSACTION:
                        case Constants.TRIGGER_ACTION_MONTHLY_PASS_GENERATED:
                        case Constants.TRIGGER_ACTION_LOST_MONTHLY_PASS:
                        case Constants.TRIGGER_ACTION_GET_CURRENT_OCCUPANCY: {
                            final int syncId = intent.getIntExtra(Constants.BUNDLE_PARAM_SYNC_ID,
                                                  Constants.INVALID_SYNC_ID);

                            switch (handleServerSyncId(syncId)) {
                                case Constants.IN_SYNC:
                                    showProgress(false);
                                    updateOccupancyView();

                                    break;

                                case Constants.OUT_OF_SYNC_PULL_REQUIRED:
                                    //updateNotificationView("Downloading updates from Server....");
                                    showProgress(false);
                                    updateOccupancyView();

                                    break;

                                case Constants.OUT_OF_SYNC_PUSH_REQUIRED:
                                     showProgress(false);
                                     updateOccupancyView();
                                     //updateNotificationView("Pushing Updates to Server....");

                                     break;
                                }
                            }


                        break;

                        case Constants.TRIGGER_ACTION_REFRESH_TOKEN:
                        case Constants.TRIGGER_ACTION_GET_SPACE_OPERATING_HOURS: {

                        }
                        break;

                        case Constants.TRIGGER_ACTION_PUSH_UPDATES_TO_SERVER:{
                            //set up sync state
                            Cache.setInCache(getApplicationContext(),
                                    Constants.CACHE_PREFIX_SYNC_STATE_QUERY,
                                    Constants.SYNC_STATE_IN_SYNC,
                                    Constants.TYPE_INT);

                            //TODO: maybe we need to check if all updates have been pushed

                        }
                        break;

                        case Constants.TRIGGER_ACTION_PULL_UPDATES_FROM_SERVER:{
                            showProgress(false);
                            updateOccupancyView();
                            //updateNotificationView("Synced with Server");

                            int state = (Integer)Cache.getFromCache(getApplicationContext(),
                                          Constants.CACHE_PREFIX_SYNC_STATE_QUERY,
                                           Constants.TYPE_INT);



                            if(state == Constants.SYNC_STATE_BOTH_PUSH_AND_PULL_IN_PROGRESS) {

                                Cache.setInCache(getApplicationContext(),
                                                    Constants.CACHE_PREFIX_SYNC_STATE_QUERY,
                                                    Constants.SYNC_STATE_IN_PROGRESS_PUSH,
                                                    Constants.TYPE_INT);

                                ServerUpdationIntentService.startActionPushUpdatesToServer(
                                        getApplicationContext());

                            }else{
                                //set up sync state
                                Cache.setInCache(getApplicationContext(),
                                        Constants.CACHE_PREFIX_SYNC_STATE_QUERY,
                                        Constants.SYNC_STATE_IN_SYNC,
                                        Constants.TYPE_INT);

                            }
                        }
                        break;

                        case Constants.TRIGGER_ACTION_OPAPP_BOOKING_UPDATE_RECEIVED:
                        case Constants.TRIGGER_ACTION_BOOKING_UPDATE_FROM_SERVER:
                        case Constants.TRIGGER_ACTION_ASYNC_GCM_UPDATE_ENTRY:
                        case Constants.TRIGGER_ACTION_ASYNC_GCM_UPDATE_SP_ENTRY:{
                            final String operatorDesc = intent.getStringExtra(
                                    Constants.BUNDLE_PARAM_OPERATOR_DESC);

                            final int syncId = intent.getIntExtra(
                                    Constants.BUNDLE_PARAM_SYNC_ID,Constants.INVALID_SYNC_ID);
                            final Boolean updateView = intent.getBooleanExtra(
                                    Constants.BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW, false);


                            if(updateView) {
                                updateOccupancyView();

                                /*String notification = "Last Entry through " + operatorDesc;
                                updateNotificationView(notification);*/
                            }

                            handleServerSyncId(syncId);

                        }

                        break;

                        case Constants.TRIGGER_ACTION_ASYNC_GCM_UPDATE_EXIT:
                        case Constants.TRIGGER_ACTION_ASYNC_GCM_UPDATE_SP_EXIT:{
                            final String operatorDesc = intent.getStringExtra(
                                    Constants.BUNDLE_PARAM_OPERATOR_DESC);

                            final Boolean updateView = intent.getBooleanExtra(
                                    Constants.BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW, false);

                            final int syncId = intent.getIntExtra(
                                    Constants.BUNDLE_PARAM_SYNC_ID, Constants.INVALID_SYNC_ID);



                            if(updateView) {
                                updateOccupancyView();
                                /*String notification = "Last Exit through " + operatorDesc;
                                updateNotificationView(notification);*/
                            }

                            handleServerSyncId(syncId);

                         }
                        break;


                        case Constants.TRIGGER_ACTION_CANCEL_TRANSACTION_FROM_ANOTHER_APP:
                        case Constants.TRIGGER_ACTION_SP_CANCEL_TRANSACTION_FROM_ANOTHER_APP:
                        case Constants.TRIGGER_ACTION_UPDATE_TRANSACTION_FROM_ANOTHER_APP:
                        case Constants.TRIGGER_ACTION_SP_UPDATE_TRANSACTION_FROM_ANOTHER_APP:{
                            final Boolean updateView = intent.getBooleanExtra(
                                    Constants.BUNDLE_PARAM_UPDATE_OCCUPANCY_VIEW, false);

                            final int syncId = intent.getIntExtra(Constants.BUNDLE_PARAM_SYNC_ID,
                                                  Constants.INVALID_SYNC_ID);

                            if(updateView) {
                                updateOccupancyView();
                            }


                            handleServerSyncId(syncId);


                        }
                        break;

                        case Constants.TRIGGER_ACTION_CALCULATE_OCCUPANCY_COUNT_FROM_DB:
                            showProgress(false);
                            updateOccupancyView();
                            break;

                        case Constants.TRIGGER_ACTION_END_OF_DAY:
                            updateOccupancyView();
                            break;

                        case Constants.TRIGGER_ACTION_IS_SPACE_BIKE_ONLY:
                            //nothing to do here
                            break;

                        case Constants.TRIGGER_ACTION_RESET_OCCUPANCY_COUNT:
                            updateOccupancyView();
                            break;


                    }
                } else {
                    final String errorString = intent.getStringExtra(Constants.ERROR);

                    if(triggerRequest == Constants.TRIGGER_ACTION_REGISTER){
                        setTitle("No Internet connectivity");
                        updateOccupancyView();
                    }else if (triggerRequest ==  Constants.TRIGGER_ACTION_PUSH_UPDATES_TO_SERVER) {
                        //set up sync state
                        Cache.setInCache(getApplicationContext(),
                                Constants.CACHE_PREFIX_SYNC_STATE_QUERY,
                                Constants.SYNC_STATE_PUSH_WAITING_FOR_TIMER_EXPIRY,
                                Constants.TYPE_INT);

                        //TODO: maybe we need to check if all updates have been pushed
                    }

                    /*if(errorString == null || errorString.isEmpty()){
                        updateNotificationView("Error Updating server.");
                    }else {
                       updateNotificationView(errorString);
                    }*/
                    showProgress(false);
                }
            }
        }

    }
}


