package in.simplypark.spot_app.db_tables;

import com.orm.SugarRecord;

import java.util.List;

import in.simplypark.spot_app.config.Constants;

/**
 * Created by aditya on 08/05/16.
 */
public class TransactionTbl extends SugarRecord{
    String  mParkingId;
    String  mCarRegNumber;
    long    mEntryTime;
    long    mExitTime;
    double  mParkingFees;
    boolean mIsParkAndRide;
    int     mStatus;
    int     mVehicleType;
    boolean mIsSyncedWithServer;
    boolean mIsUpdated;
    String  mChangeDescription;
    String  mNightChargeDescription;
    String  mPenaltyChargeDescription;
    int     mOperatorId;
    String  mToken; //reusable print tokens




    public TransactionTbl(){
        mParkingId = "";
        mCarRegNumber = "";
        mEntryTime = 0;
        mExitTime = 0;
        mParkingFees = 0.00;
        mIsParkAndRide = false;
        mStatus        = Constants.PARKING_STATUS_NONE;
        mVehicleType    = Constants.VEHICLE_TYPE_CAR;
        //issue109: setting the transaction as not synced by default
        mIsSyncedWithServer = false;
        mIsUpdated = false;
        mChangeDescription = "";
        mNightChargeDescription = "";
        mPenaltyChargeDescription = "";
        mOperatorId = Constants.INVALID_OPERATOR_ID;
        mToken = "";


    }

    public int getOperatorId() {
        return mOperatorId;
    }

    public void setOperatorId(int mOperatorId) {
        this.mOperatorId = mOperatorId;
    }

    public void setParkingId(String parkingId){
        mParkingId = parkingId;
    }

    public String getParkingId(){
        return mParkingId;
    }

    public void setCarRegNumber(String carReg){
        mCarRegNumber = carReg;
    }

    public String getCarRegNumber(){
        return mCarRegNumber;
    }

    public void setEntryTime(long entryTime){
        mEntryTime = entryTime;
    }

    public long getEntryTime(){
        return mEntryTime;
    }

    public void setExitTime(long exitTime){
        mExitTime = exitTime;
    }

    public long getExitTime(){
        return mExitTime;
    }

    public boolean getIsParkAndRide() { return mIsParkAndRide; }

    public void setIsParkAndRide(boolean isParkAndRide) { mIsParkAndRide = isParkAndRide;}

    public void setParkingFees(double parkingFees) { mParkingFees = parkingFees; }
    public double getParkingFees() { return mParkingFees;}

    public void setParkingStatus(int status) { mStatus = status;}

    public int getParkingStatus() { return mStatus; }

    public void setVehicleType(int vehicleType){ mVehicleType = vehicleType;}

    public int getVehicleType() { return mVehicleType; }

    public void setIsSyncedWithServer(boolean isSynced){ mIsSyncedWithServer = isSynced;}
    public boolean getIsSyncedWithServer(){ return mIsSyncedWithServer;}


    public void setIsUpdated(boolean isUpdated) { mIsUpdated = isUpdated;}
    public boolean getIsUpdated() { return mIsUpdated; }

    public void setChangeDescription(String desc) { mChangeDescription = desc;}
    public String getChangeDescription() { return mChangeDescription;}

    public void setNightChargeDescription(String description) {
        mNightChargeDescription = description;
    }

    //TODO: this is a hack.
    public String getNightChargeDescription() {
        return mNightChargeDescription;}

    public String getPenaltyChargeDescription() {
        return mPenaltyChargeDescription;
    }

    public void setPenaltyChargeDescription(String mPenaltyChargeDescription) {
        this.mPenaltyChargeDescription = mPenaltyChargeDescription;
    }
    @Override
    public String toString() {
        return this.getCarRegNumber();
    }

    public void setToken(String token){
        mToken = token;
    }

    public String getToken(){
        return mToken;
    }




}
