package in.simplypark.spot_app.printer;

import android.content.Context;
import android.util.Log;

//import com.analogics.thermalprinter.AnalogicsThermalPrinter;

import java.io.IOException;

import in.simplypark.spot_app.utils.AnalogicsPrinterHandle;
import in.simplypark.spot_app.utils.Cache;
import in.simplypark.spot_app.config.Constants;
import in.simplypark.spot_app.listelements.Element;
import in.simplypark.spot_app.utils.Format;
import in.simplypark.spot_app.utils.Prepare2InchData;
import in.simplypark.spot_app.utils.PreparePrintData;
import in.simplypark.spot_app.utils.TransactionSummary;

/**
 * Created by aditya on 23/05/16.
 */
public class Printer {
    private PrinterHandle mPrinterHandle;

    private int mState;


    private Printer(int printerType){
        mPrinterType = printerType;
        if(printerType == Constants.PRINTER_TYPE_ANALOGICS) {
            mPrinterHandle = new AnalogicsPrinterHandle();
        }else if(printerType == Constants.PRINTER_TYPE_MATE){
            mPrinterHandle = new MatePrinterHandle();
        }
        mEntryTicketFormat = new Format();
        mExitTicketFormat  = new Format();
        mReportFormat = new Format();
        mMonthlyPassFormat = new Format();
        mMonthlyPassReceiptFormat = new Format();
        setEntryTicketFormat();
        setExitTicketFormat();
        setReportFormat();
        setMonthlyPassFormat();
        setMonthlyPassReceiptFormat();
        mState = Constants.PRINTER_STATE_DISCONNECTED;

    }
    public static Printer mPrinter;
    private static int mPrinterType = Constants.PRINTER_TYPE_ANALOGICS;
    private PreparePrintData mDataFormatter;
    private Format mEntryTicketFormat;
    private Format mExitTicketFormat;
    private Format mReportFormat;
    private Format mMonthlyPassFormat; //the monthly pass that is generated from the App
    private Format mMonthlyPassReceiptFormat;

    private byte[] mEntryTicketHeaderTemplate;
    private byte[] mEntryTicketFooterTemplate;
    private byte[] mExitTicketHeaderTemplate;
    private byte[] mExitTicketFooterTemplate;

    private byte[] mReportHeaderTemplate;
    private byte[] mReportFooterTemplate;

    private byte[] mMonthlyPassHeaderTemplate;
    private byte[] mMonthlyPassFooterTemplate;

    private byte[] mMonthlyPassPaymentReceiptHeaderTemplate;
    private byte[] mMonthlyPassPaymentReceiptFooterTemplate;

    private Context mContext;

    public void setState(int state){ mState = state;}

    public int getState() { return mState; }

    public static Printer getPrinter(){
        if(mPrinter == null){
            mPrinter = new Printer(mPrinterType);

        }

        return mPrinter;
    }

    public static Printer getPrinter(String printerName){

        if(mPrinter == null){
            if(printerName.equals(Constants.ANALOGICS_PRINTER_NAME)){
                mPrinter = new Printer(Constants.PRINTER_TYPE_ANALOGICS);
            }else{
                mPrinter = new Printer(Constants.PRINTER_TYPE_MATE);
            }
        }

        return mPrinter;
    }

    private void setEntryTicketFormat(){
        Element[][] headerElm = new Element
                                                  [Constants.PRINTER_MAX_HEADER_ELEMENTS]
                                                  [Constants.PRINTER_MAX_HEADER_SUB_ELEMENTS];
        //address
        headerElm[Constants.PRINTER_FIRST_LINE][0] = new Element(Constants.PRINTER_KEY_SPACE_ADDRESS,
                                                                  Constants.PRINTER_TYPE_TEXT,
                                                                  Constants.PRINTER_ALIGN_LEFT,
                                                                  Constants.PRINTER_SIZE_MEDIUM_BIG);

        //heading
            headerElm[Constants.PRINTER_SECOND_LINE][0] = new Element(Constants.PRINTER_KEY_HEADING,
                                                                  Constants.PRINTER_TYPE_TEXT,
                                                                  Constants.PRINTER_ALIGN_CENTER,
                                                                  Constants.PRINTER_SIZE_MEDIUM);
        //agency name
        headerElm[Constants.PRINTER_THIRD_LINE][0] = new Element(Constants.PRINTER_KEY_AGENCY_NAME,
                                                                 Constants.PRINTER_TYPE_TEXT,
                                                                 Constants.PRINTER_ALIGN_LEFT,
                                                                 Constants.PRINTER_SIZE_SMALL);



        //mobile
        headerElm[Constants.PRINTER_FOURTH_LINE][0] = new Element(Constants.PRINTER_KEY_SPACE_OWNER_MOBILE,
                                                                 Constants.PRINTER_TYPE_TEXT,
                                                                 Constants.PRINTER_ALIGN_LEFT,
                                                                 Constants.PRINTER_SIZE_SMALL);




         mEntryTicketFormat.setHeader(headerElm);


         //date and time


        Element[][] body = new Element[Constants.PRINTER_MAX_ENTRY_BODY_ELEMENTS]
                                            [Constants.PRINTER_MAX_ENTRY_BODY_SUB_ELEMENTS];

        //date and time
        body[Constants.PRINTER_FIRST_LINE][0]= new Element(Constants.PRINTER_KEY_ENTRY_DATE_TIME,
                                                  Constants.PRINTER_TYPE_TEXT,
                                                  Constants.PRINTER_ALIGN_LEFT,
                                                  Constants.PRINTER_SIZE_MEDIUM,
                                                  Constants.PRINTER_STYLE_BOLD);


        /*if((Boolean)Cache.getFromCache(mContext,Constants.CACHE_PREFIX_BIG_SIZE_VEHICLE_REGSTRING,
                                       Constants.TYPE_BOOL)){
            //car number
            body[Constants.PRINTER_SECOND_LINE][0] = new Element(Constants.PRINTER_KEY_REG_NUMBER,
                    Constants.PRINTER_TYPE_TEXT,
                    Constants.PRINTER_ALIGN_LEFT,
                    Constants.PRINTER_SIZE_MEDIUM,
                    Constants.PRINTER_STYLE_BOLD);

        }else*/ {
            //car number
            body[Constants.PRINTER_SECOND_LINE][0] = new Element(Constants.PRINTER_KEY_REG_NUMBER,
                    Constants.PRINTER_TYPE_TEXT,
                    Constants.PRINTER_ALIGN_LEFT,
                    Constants.PRINTER_SIZE_BIG);

        }
        //parkingId
        body[Constants.PRINTER_THIRD_LINE][0]= new Element(Constants.PRINTER_KEY_PARKING_ID,
                                                  Constants.PRINTER_TYPE_TEXT,
                                                  Constants.PRINTER_ALIGN_LEFT,
                                                  Constants.PRINTER_SIZE_MEDIUM);
        //operating hours
        body[Constants.PRINTER_FOURTH_LINE][0] = new Element(Constants.PRINTER_KEY_OPERATING_HOURS,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_MEDIUM);


        //parking rates
        body[Constants.PRINTER_FIFTH_LINE][0]= new Element(Constants.PRINTER_KEY_PARKING_RATES,
                                                            Constants.PRINTER_TYPE_TEXT,
                                                            Constants.PRINTER_ALIGN_LEFT,
                                                            Constants.PRINTER_SIZE_MEDIUM);

        //nightcharge
        body[Constants.PRINTER_SIXTH_LINE][0]= new Element(Constants.PRINTER_KEY_NIGHT_CHARGE,
                //Constants.PRINTER_TYPE_BARCODE,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_MEDIUM);







        body[Constants.PRINTER_SEVENTH_LINE][0] = new Element(Constants.PRINTER_KEY_PARK_AND_RIDE,
                                                    Constants.PRINTER_TYPE_TEXT,
                                                    Constants.PRINTER_ALIGN_RIGHT,
                                                    Constants.PRINTER_SIZE_BIG);




        mEntryTicketFormat.setBody(body);

        Element[][] footer = new Element
                                                  [Constants.PRINTER_MAX_ENTRY_FOOTER_ELEMENTS]
                                                  [Constants.PRINTER_MAX_ENTRY_FOOTER_SUB_ELEMENTS];

        //dynamic ad
        //lost ticket
        footer[Constants.PRINTER_FIRST_LINE][0] = new Element(Constants.PRINTER_KEY_DYNAMIC_AD,
                                                              Constants.PRINTER_TYPE_TEXT,
                                                              Constants.PRINTER_ALIGN_LEFT,
                                                              Constants.PRINTER_SIZE_MEDIUM);


        //lost ticket
        footer[Constants.PRINTER_SECOND_LINE][0] = new Element(Constants.PRINTER_KEY_LOST_TICKET,
                                                              Constants.PRINTER_TYPE_TEXT,
                                                              Constants.PRINTER_ALIGN_LEFT,
                                                              Constants.PRINTER_SIZE_SMALL);


        //simplypark URL
        footer[Constants.PRINTER_THIRD_LINE][0]= new Element(Constants.PRINTER_KEY_URL,
                                                    Constants.PRINTER_TYPE_TEXT,
                                                    Constants.PRINTER_ALIGN_LEFT,
                                                    Constants.PRINTER_SIZE_SMALL);
        //disclaimer
        footer[Constants.PRINTER_FOURTH_LINE][0]= new Element(Constants.PRINTER_KEY_DESC,
                                                     Constants.PRINTER_TYPE_TEXT,
                                                     Constants.PRINTER_ALIGN_CENTER,
                                                     Constants.PRINTER_SIZE_SMALL);

        footer[Constants.PRINTER_FIFTH_LINE][0] = new Element(Constants.PRINTER_NEWLINE,
                                                      Constants.PRINTER_TYPE_NEWLINE,
                                                      Constants.PRINTER_ALIGN_LEFT,
                                                     Constants.PRINTER_SIZE_SMALL);
        /*
        footer[Constants.PRINTER_FIFTH_LINE][0] = new Element(Constants.PRINTER_NEWLINE,
                                                       Constants.PRINTER_TYPE_NEWLINE,
                                                       Constants.PRINTER_ALIGN_LEFT,
                                                       Constants.PRINTER_SIZE_SMALL);

        footer[Constants.PRINTER_FIFTH_LINE][0] = new Element(Constants.PRINTER_KEY_LINE,
                                                        Constants.PRINTER_TYPE_NEWLINE,
                                                        Constants.PRINTER_ALIGN_LEFT,
                                                        Constants.PRINTER_SIZE_SMALL);
                                                        */
        mEntryTicketFormat.setFooter(footer);

    }

    private void setExitTicketFormat(){
        Element[][] headerElm = new Element
                [Constants.PRINTER_MAX_EXIT_HEADER_ELEMENTS]
                [Constants.PRINTER_MAX_EXIT_HEADER_SUB_ELEMENTS];

       //address
        headerElm[Constants.PRINTER_FIRST_LINE][0] = new Element(Constants.PRINTER_KEY_SPACE_ADDRESS,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_MEDIUM_BIG);

        //heading
        headerElm[Constants.PRINTER_SECOND_LINE][0] = new Element(Constants.PRINTER_KEY_HEADING,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_CENTER,
                Constants.PRINTER_SIZE_MEDIUM);
        //agency name
        headerElm[Constants.PRINTER_THIRD_LINE][0] = new Element(Constants.PRINTER_KEY_AGENCY_NAME,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_SMALL);


        //mobile
        headerElm[Constants.PRINTER_FOURTH_LINE][0] = new Element(Constants.PRINTER_KEY_SPACE_OWNER_MOBILE,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_SMALL);


        mExitTicketFormat.setHeader(headerElm);


        //date and time


        Element[][] body = new Element[Constants.PRINTER_MAX_EXIT_BODY_ELEMENTS]
                [Constants.PRINTER_MAX_EXIT_BODY_SUB_ELEMENTS];

        //car number
        body[Constants.PRINTER_FIRST_LINE][0]= new Element(Constants.PRINTER_KEY_REG_NUMBER,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_MEDIUM);
        //date and time
        body[Constants.PRINTER_SECOND_LINE][0]= new Element(Constants.PRINTER_KEY_ENTRY_DATE_TIME,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_MEDIUM,Constants.PRINTER_STYLE_BOLD);

        //exit date and time
        body[Constants.PRINTER_THIRD_LINE][0]= new Element(Constants.PRINTER_KEY_EXIT_DATE_TIME,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_MEDIUM);

        //duration
        body[Constants.PRINTER_FOURTH_LINE][0]= new Element(Constants.PRINTER_KEY_DURATION,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_MEDIUM);

        //pricing - to be displayed only for prepaid parking
        body[Constants.PRINTER_FIFTH_LINE][0]= new Element(Constants.PRINTER_KEY_PARKING_RATES,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_MEDIUM);


        //fees
        body[Constants.PRINTER_SIXTH_LINE][0]= new Element(Constants.PRINTER_KEY_PARKING_FEES,
                                                            Constants.PRINTER_TYPE_TEXT,
                                                            Constants.PRINTER_ALIGN_CENTER,
                                                            Constants.PRINTER_SIZE_BIG);


        body[Constants.PRINTER_SEVENTH_LINE][0] = new Element(Constants.PRINTER_KEY_NIGHT_CHARGE,
                                                              Constants.PRINTER_TYPE_TEXT,
                                                              Constants.PRINTER_ALIGN_LEFT,
                                                              Constants.PRINTER_SIZE_MEDIUM);

        mExitTicketFormat.setBody(body);

        Element[][] footer = new Element
                [Constants.PRINTER_MAX_EXIT_FOOTER_ELEMENTS]
                [Constants.PRINTER_MAX_EXIT_FOOTER_SUB_ELEMENTS];


        //dynamic ad
        //lost ticket
        footer[Constants.PRINTER_FIRST_LINE][0] = new Element(Constants.PRINTER_KEY_DYNAMIC_AD,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_MEDIUM);


        //lost ticket
        footer[Constants.PRINTER_SECOND_LINE][0] = new Element(Constants.PRINTER_KEY_LOST_TICKET,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_SMALL);


         //disclaimer
        footer[Constants.PRINTER_THIRD_LINE][0]= new Element(Constants.PRINTER_KEY_DESC,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_CENTER,
                Constants.PRINTER_SIZE_SMALL);


        //simplypark URL
        footer[Constants.PRINTER_FOURTH_LINE][0]= new Element(Constants.PRINTER_KEY_URL,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_SMALL);

        //cusomt ad
        footer[Constants.PRINTER_FIFTH_LINE][0]= new Element(Constants.PRINTER_KEY_EXIT_SIMPLYPARK_AD,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_SMALL);


        footer[Constants.PRINTER_SIXTH_LINE][0] = new Element(Constants.PRINTER_NEWLINE,
                Constants.PRINTER_TYPE_NEWLINE,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_SMALL);



        /*
        footer[Constants.PRINTER_FIFTH_LINE][0] = new Element(Constants.PRINTER_NEWLINE,
                                                       Constants.PRINTER_TYPE_NEWLINE,
                                                       Constants.PRINTER_ALIGN_LEFT,
                                                       Constants.PRINTER_SIZE_SMALL);

        footer[Constants.PRINTER_FIFTH_LINE][0] = new Element(Constants.PRINTER_KEY_LINE,
                                                        Constants.PRINTER_TYPE_NEWLINE,
                                                        Constants.PRINTER_ALIGN_LEFT,
                                                        Constants.PRINTER_SIZE_SMALL);
                                                        */

        mExitTicketFormat.setFooter(footer);


    }

    private void setReportFormat(){
        Element[][] headerElm = new Element
                [Constants.PRINTER_MAX_REPORT_HEADER_ELEMENTS]
                [Constants.PRINTER_MAX_REPORT_HEADER_SUB_ELEMENTS];

        //heading

        headerElm[Constants.PRINTER_FIRST_LINE][0] = new Element(
                                                         Constants.PRINTER_KEY_REPORT_HEADING,
                                                         Constants.PRINTER_TYPE_TEXT,
                                                         Constants.PRINTER_ALIGN_CENTER,
                                                         Constants.PRINTER_SIZE_MEDIUM_BIG);

        //address
        headerElm[Constants.PRINTER_SECOND_LINE][0] = new Element(Constants.PRINTER_KEY_SPACE_ADDRESS,
                                                                  Constants.PRINTER_TYPE_TEXT,
                                                                 Constants.PRINTER_ALIGN_LEFT,
                                                                Constants.PRINTER_SIZE_SMALL);


        mReportFormat.setHeader(headerElm);


        //date and time


        Element[][] body = new Element[Constants.PRINTER_MAX_REPORT_BODY_ELEMENTS]
                [Constants.PRINTER_MAX_REPORT_BODY_SUB_ELEMENTS];

        //date
        body[Constants.PRINTER_FIRST_LINE][0]= new Element(Constants.PRINTER_KEY_REPORT_DATE,
                                                           Constants.PRINTER_TYPE_TEXT,
                                                           Constants.PRINTER_ALIGN_LEFT,
                                                           Constants.PRINTER_SIZE_MEDIUM);

        //total earning
        body[Constants.PRINTER_SECOND_LINE][0]= new Element(
                                                        Constants.PRINTER_KEY_REPORT_TOTAL_EARNINGS,
                                                        Constants.PRINTER_TYPE_TEXT,
                                                        Constants.PRINTER_ALIGN_LEFT,
                                                        Constants.PRINTER_SIZE_MEDIUM);

        //earnings breakup - Car
        body[Constants.PRINTER_THIRD_LINE][0]= new Element(
                                                        Constants.PRINTER_KEY_REPORT_CAR_EARNINGS,
                                                        Constants.PRINTER_TYPE_TEXT,
                                                        Constants.PRINTER_ALIGN_LEFT,
                                                        Constants.PRINTER_SIZE_MEDIUM);

        //earnings breakup - Bike
        body[Constants.PRINTER_FOURTH_LINE][0]= new Element(
                                                        Constants.PRINTER_KEY_REPORT_BIKE_EARNINGS,
                                                        Constants.PRINTER_TYPE_TEXT,
                                                        Constants.PRINTER_ALIGN_LEFT,
                                                        Constants.PRINTER_SIZE_MEDIUM);
        //earnings breakup - minibus
        body[Constants.PRINTER_FIFTH_LINE][0]= new Element(
                                                         Constants.PRINTER_KEY_REPORT_MINIBUS_EARNINGS,
                                                         Constants.PRINTER_TYPE_TEXT,
                                                         Constants.PRINTER_ALIGN_LEFT,
                                                         Constants.PRINTER_SIZE_MEDIUM);

        //earnings breakup - Bus
        body[Constants.PRINTER_SIXTH_LINE][0]= new Element(
                                                    Constants.PRINTER_KEY_REPORT_BUS_EARNINGS,
                                                    Constants.PRINTER_TYPE_TEXT,
                                                    Constants.PRINTER_ALIGN_LEFT,
                                                    Constants.PRINTER_SIZE_MEDIUM);

        //earnings breakup - monthly passes
        body[Constants.PRINTER_SEVENTH_LINE][0] = new Element(
                                                 Constants.PRINTER_KEY_REPORT_MONTHLY_PASS_EARNINGS,
                                                 Constants.PRINTER_TYPE_TEXT,
                                                 Constants.PRINTER_ALIGN_LEFT,
                                                  Constants.PRINTER_SIZE_MEDIUM);


        //Vehicles Parked
        body[Constants.PRINTER_EIGTH_LINE][0]= new Element(
                                                 Constants.PRINTER_KEY_REPORT_TOTAL_VEHICLES_PARKED,
                                                 Constants.PRINTER_TYPE_TEXT,
                                                 Constants.PRINTER_ALIGN_LEFT,
                                                 Constants.PRINTER_SIZE_MEDIUM);

        //numCars Parked
        body[Constants.PRINTER_NINTH_LINE][0]= new Element(
                                                     Constants.PRINTER_KEY_REPORT_CARS_PARKED,
                                                     Constants.PRINTER_TYPE_TEXT,
                                                     Constants.PRINTER_ALIGN_LEFT,
                                                     Constants.PRINTER_SIZE_MEDIUM);
        //numCars Parked monthly pass
        body[Constants.PRINTER_TENTH_LINE][0]= new Element(
                                           Constants.PRINTER_KEY_REPORT_MONTHLY_PASS_CARS_PARKED,
                                           Constants.PRINTER_TYPE_TEXT,
                                           Constants.PRINTER_ALIGN_LEFT,
                                           Constants.PRINTER_SIZE_MEDIUM);


        //numBikes Parked
        body[Constants.PRINTER_ELEVENTH_LINE][0]= new Element(
                                                    Constants.PRINTER_KEY_REPORT_BIKES_PARKED,
                                                    Constants.PRINTER_TYPE_TEXT,
                                                    Constants.PRINTER_ALIGN_LEFT,
                                                    Constants.PRINTER_SIZE_MEDIUM);

        //numBikes Parked - monthly pass
        body[Constants.PRINTER_TWELFTH_LINE][0]= new Element(
                                             Constants.PRINTER_KEY_REPORT_MONTHLY_PASS_BIKES_PARKED,
                                             Constants.PRINTER_TYPE_TEXT,
                                             Constants.PRINTER_ALIGN_LEFT,
                                             Constants.PRINTER_SIZE_MEDIUM);

        //numMiniBus Parked
        body[Constants.PRINTER_THIRTEEN_LINE][0]= new Element(
                                                    Constants.PRINTER_KEY_REPORT_MINIBUS_PARKED,
                                                    Constants.PRINTER_TYPE_TEXT,
                                                    Constants.PRINTER_ALIGN_LEFT,
                                                    Constants.PRINTER_SIZE_MEDIUM);

        //numMiniBus Parked - monthly pass
        body[Constants.PRINTER_FOURTEEN_LINE][0]= new Element(
                                             Constants.PRINTER_KEY_REPORT_MONTHLY_PASS_MINIBUS_PARKED,
                                             Constants.PRINTER_TYPE_TEXT,
                                             Constants.PRINTER_ALIGN_LEFT,
                                             Constants.PRINTER_SIZE_MEDIUM);

        //numBus Parked
        body[Constants.PRINTER_FIFTEEN_LINE][0]= new Element(
                                               Constants.PRINTER_KEY_REPORT_BUS_PARKED,
                                               Constants.PRINTER_TYPE_TEXT,
                                               Constants.PRINTER_ALIGN_LEFT,
                                               Constants.PRINTER_SIZE_MEDIUM);

        //numBus Parked - monthly pass
        body[Constants.PRINTER_SIXTEEN_LINE][0]= new Element(
                                               Constants.PRINTER_KEY_REPORT_MONTHLY_PASS_BUS_PARKED,
                                               Constants.PRINTER_TYPE_TEXT,
                                               Constants.PRINTER_ALIGN_LEFT,
                                               Constants.PRINTER_SIZE_MEDIUM);


        //Vehicles Inside
        body[Constants.PRINTER_SEVENTEEN_LINE][0]= new Element(
                                                 Constants.PRINTER_KEY_REPORT_TOTAL_VEHICLES_INSIDE,
                                                 Constants.PRINTER_TYPE_TEXT,
                                                 Constants.PRINTER_ALIGN_LEFT,
                                                 Constants.PRINTER_SIZE_MEDIUM);

        //numCars Inside
        body[Constants.PRINTER_EIGTHTEEN_LINE][0]= new Element(
                                                 Constants.PRINTER_KEY_REPORT_TOTAL_CARS_INSIDE,
                                                 Constants.PRINTER_TYPE_TEXT,
                                                 Constants.PRINTER_ALIGN_LEFT,
                                                 Constants.PRINTER_SIZE_MEDIUM);

        //numCars Inside - monthly pass
        body[Constants.PRINTER_NINETEEN_LINE][0]= new Element(
                                         Constants.PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_CARS_INSIDE,
                                         Constants.PRINTER_TYPE_TEXT,
                                         Constants.PRINTER_ALIGN_LEFT,
                                         Constants.PRINTER_SIZE_MEDIUM);


        //numBikes Inside
        body[Constants.PRINTER_TWENTH_LINE][0]= new Element(
                                                 Constants.PRINTER_KEY_REPORT_TOTAL_BIKES_INSIDE,
                                                 Constants.PRINTER_TYPE_TEXT,
                                                 Constants.PRINTER_ALIGN_LEFT,
                                                 Constants.PRINTER_SIZE_MEDIUM);

        //numBikes Inside - monthly pass
        body[Constants.PRINTER_TWENTYFIRST_LINE][0]= new Element(
                                      Constants.PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_BIKES_INSIDE,
                                      Constants.PRINTER_TYPE_TEXT,
                                      Constants.PRINTER_ALIGN_LEFT,
                                      Constants.PRINTER_SIZE_MEDIUM);

        //numMiniBus Inside
        body[Constants.PRINTER_TWENTYSECOND_LINE][0]= new Element(
                                      Constants.PRINTER_KEY_REPORT_TOTAL_MINIBUS_INSIDE,
                                      Constants.PRINTER_TYPE_TEXT,
                                      Constants.PRINTER_ALIGN_LEFT,
                                      Constants.PRINTER_SIZE_MEDIUM);

        //numMiniBus Inside - monthly pass
        body[Constants.PRINTER_TWENTYTHIRD_LINE][0]= new Element(
                                     Constants.PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_MINIBUS_INSIDE,
                                     Constants.PRINTER_TYPE_TEXT,
                                     Constants.PRINTER_ALIGN_LEFT,
                                     Constants.PRINTER_SIZE_MEDIUM);

        //numBus Inside
        body[Constants.PRINTER_TWENTYFOURTH_LINE][0]= new Element(
                                      Constants.PRINTER_KEY_REPORT_TOTAL_BUS_INSIDE,
                                      Constants.PRINTER_TYPE_TEXT,
                                      Constants.PRINTER_ALIGN_LEFT,
                                      Constants.PRINTER_SIZE_MEDIUM);

        //numMiniBus Inside - monthly pass
        body[Constants.PRINTER_TWENTYFIFTH_LINE][0]= new Element(
                                    Constants.PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_BUS_INSIDE,
                                    Constants.PRINTER_TYPE_TEXT,
                                    Constants.PRINTER_ALIGN_LEFT,
                                    Constants.PRINTER_SIZE_MEDIUM);



        //MisisngExits
        body[Constants.PRINTER_TWENTYSIXTH_LINE][0]= new Element(
                                                  Constants.PRINTER_KEY_REPORT_TOTAL_MISSING_EXITS,
                                                  Constants.PRINTER_TYPE_TEXT,
                                                  Constants.PRINTER_ALIGN_LEFT,
                                                  Constants.PRINTER_SIZE_MEDIUM);

        //missing Car Exits
        body[Constants.PRINTER_TWENTYSEVENTH_LINE][0]= new Element(
                                               Constants.PRINTER_KEY_REPORT_TOTAL_CAR_MISSING_EXITS,
                                               Constants.PRINTER_TYPE_TEXT,
                                               Constants.PRINTER_ALIGN_LEFT,
                                               Constants.PRINTER_SIZE_MEDIUM);

        //missing Car Exits
        body[Constants.PRINTER_TWENTYEIGTH_LINE][0]= new Element(
                Constants.PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_CAR_MISSING_EXITS,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_MEDIUM);


        //missing bike exits
        body[Constants.PRINTER_TWENTYNINTH_LINE][0]= new Element(
                                              Constants.PRINTER_KEY_REPORT_TOTAL_BIKE_MISSING_EXITS,
                                              Constants.PRINTER_TYPE_TEXT,
                                              Constants.PRINTER_ALIGN_LEFT,
                                              Constants.PRINTER_SIZE_MEDIUM);

        //missing bike exits
        body[Constants.PRINTER_THIRTETH_LINE][0]= new Element(
                Constants.PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_BIKE_MISSING_EXITS,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_MEDIUM);

        //missing miniBus exits
        body[Constants.PRINTER_THIRTYFIRST_LINE][0]= new Element(
                                             Constants.PRINTER_KEY_REPORT_TOTAL_MINIBUS_MISSING_EXITS,
                                             Constants.PRINTER_TYPE_TEXT,
                                             Constants.PRINTER_ALIGN_LEFT,
                                             Constants.PRINTER_SIZE_MEDIUM);

        //missing minibus exits
        body[Constants.PRINTER_THIRTYSECOND_LINE][0]= new Element(
                                 Constants.PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_MINIBUS_MISSING_EXITS,
                                 Constants.PRINTER_TYPE_TEXT,
                                 Constants.PRINTER_ALIGN_LEFT,
                                 Constants.PRINTER_SIZE_MEDIUM);

        //missing Bus exits
        body[Constants.PRINTER_THIRTYTHIRD_LINE][0]= new Element(
                                          Constants.PRINTER_KEY_REPORT_TOTAL_BUS_MISSING_EXITS,
                                          Constants.PRINTER_TYPE_TEXT,
                                          Constants.PRINTER_ALIGN_LEFT,
                                          Constants.PRINTER_SIZE_MEDIUM);

        //missing bus exits
        body[Constants.PRINTER_THIRTYFOURTH_LINE][0]= new Element(
                Constants.PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_BUS_MISSING_EXITS,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_MEDIUM);




        //Num Passes Issued
        body[Constants.PRINTER_THIRTYFIFTH_LINE][0]= new Element(
                                             Constants.PRINTER_KEY_REPORT_TOTAL_MONTHLY_PASS_ISSUED,
                                             Constants.PRINTER_TYPE_TEXT,
                                             Constants.PRINTER_ALIGN_LEFT,
                                             Constants.PRINTER_SIZE_MEDIUM);

        body[Constants.PRINTER_THIRTYSIXTH_LINE][0]= new Element(
                                         Constants.PRINTER_KEY_REPORT_TOTAL_CAR_MONTHLY_PASS_ISSUED,
                                         Constants.PRINTER_TYPE_TEXT,
                                         Constants.PRINTER_ALIGN_LEFT,
                                         Constants.PRINTER_SIZE_MEDIUM);

        body[Constants.PRINTER_THIRTYSEVENTH_LINE][0]= new Element(
                                        Constants.PRINTER_KEY_REPORT_TOTAL_BIKE_MONTHLY_PASS_ISSUED,
                                        Constants.PRINTER_TYPE_TEXT,
                                        Constants.PRINTER_ALIGN_LEFT,
                                        Constants.PRINTER_SIZE_MEDIUM);

        body[Constants.PRINTER_THIRTYEIGTH_LINE][0]= new Element(
                Constants.PRINTER_KEY_REPORT_TOTAL_MINIBUS_MONTHLY_PASS_ISSUED,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_MEDIUM);

        body[Constants.PRINTER_THIRTYNINTH_LINE][0]= new Element(
                Constants.PRINTER_KEY_REPORT_TOTAL_BUS_MONTHLY_PASS_ISSUED,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_MEDIUM);


        body[Constants.PRINTER_FOURTHETH_LINE][0] = new Element(
            Constants.PRINTER_KEY_REPORT_GATEWISE_EARNING,
            Constants.PRINTER_TYPE_TEXT,
            Constants.PRINTER_ALIGN_LEFT,
            Constants.PRINTER_SIZE_MEDIUM
        );

        body[Constants.PRINTER_FOURTYFIRST_LINE][0] = new Element(
                Constants.PRINTER_KEY_REPORT_GATEWISE_PASS_EARNING,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_MEDIUM
        );


        //report Generation Time
        body[Constants.PRINTER_FOURTYSECOND_LINE][0]= new Element(
                                               Constants.PRINTER_KEY_REPORT_GENERATION_TIME,
                                               Constants.PRINTER_TYPE_TEXT,
                                               Constants.PRINTER_ALIGN_LEFT,
                                               Constants.PRINTER_SIZE_SMALL);




        mReportFormat.setBody(body);

        Element[][] footer = new Element
                [Constants.PRINTER_MAX_REPORT_FOOTER_ELEMENTS]
                [Constants.PRINTER_MAX_REPORT_FOOTER_SUB_ELEMENTS];



        //simplypark URL
        footer[Constants.PRINTER_FIRST_LINE][0]= new Element(Constants.PRINTER_KEY_URL,
                                                             Constants.PRINTER_TYPE_TEXT,
                                                             Constants.PRINTER_ALIGN_LEFT,
                                                             Constants.PRINTER_SIZE_SMALL);

        footer[Constants.PRINTER_SECOND_LINE][0] = new Element(Constants.PRINTER_NEWLINE,
                                                               Constants.PRINTER_TYPE_NEWLINE,
                                                               Constants.PRINTER_ALIGN_LEFT,
                                                               Constants.PRINTER_SIZE_SMALL);

        mReportFormat.setFooter(footer);

    }

    private void setMonthlyPassFormat(){


        Element[][] headerElm = new Element
                [Constants.PRINTER_MAX_MONTHLY_PASS_HEADER_ELEMENTS]
                [Constants.PRINTER_MAX_MONTHLY_PASS_HEADER_SUB_ELEMENTS];


        //agency name
        headerElm[Constants.PRINTER_FIRST_LINE][0] = new Element(Constants.PRINTER_KEY_AGENCY_NAME,
                                                                 Constants.PRINTER_TYPE_TEXT,
                                                                 Constants.PRINTER_ALIGN_LEFT,
                                                                 Constants.PRINTER_SIZE_SMALL);
        //heading
        headerElm[Constants.PRINTER_SECOND_LINE][0] = new Element(
                                                          Constants.PRINTER_KEY_MONTHLY_PASS_HEADING,
                                                          Constants.PRINTER_TYPE_TEXT,
                                                          Constants.PRINTER_ALIGN_CENTER,
                                                          Constants.PRINTER_SIZE_BIG);


        //mobile
        headerElm[Constants.PRINTER_THIRD_LINE][0] = new Element(Constants.PRINTER_KEY_SPACE_OWNER_MOBILE,
                                                                 Constants.PRINTER_TYPE_TEXT,
                                                                 Constants.PRINTER_ALIGN_LEFT,
                                                                  Constants.PRINTER_SIZE_SMALL);
        //address
        headerElm[Constants.PRINTER_FOURTH_LINE][0] = new Element(
                                                               Constants.PRINTER_KEY_SPACE_ADDRESS,
                                                               Constants.PRINTER_TYPE_TEXT,
                                                               Constants.PRINTER_ALIGN_LEFT,
                                                               Constants.PRINTER_SIZE_SMALL);





        mMonthlyPassFormat.setHeader(headerElm);


        //date and time


        Element[][] body = new Element[Constants.PRINTER_MAX_MONTHLY_PASS_BODY_ELEMENTS]
                [Constants.PRINTER_MAX_MONTHLY_PASS_BODY_SUB_ELEMENTS];

        //rates
        body[Constants.PRINTER_FIRST_LINE][0] = new Element(
                Constants.PRINTER_KEY_MONTHLY_PARKING_RATES_INCL_NIGHT_CHARGE,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_MEDIUM);

        //dotted line
        body[Constants.PRINTER_SECOND_LINE][0] = new Element(
                Constants.PRINTER_KEY_DOTTED_LINE,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_MEDIUM);

        //issued to
        body[Constants.PRINTER_THIRD_LINE][0]= new Element(Constants.PRINTER_KEY_MONTHLY_PASS_ISSUED_TO,
                                                           Constants.PRINTER_TYPE_TEXT,
                                                           Constants.PRINTER_ALIGN_LEFT,
                                                           Constants.PRINTER_SIZE_MEDIUM);

        //email
        body[Constants.PRINTER_FOURTH_LINE][0] = new Element(
                Constants.PRINTER_KEY_MONTHLY_PASS_EMAIL,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_RIGHT,
                Constants.PRINTER_SIZE_MEDIUM);

        //phone
        body[Constants.PRINTER_FIFTH_LINE][0]= new Element(
                Constants.PRINTER_KEY_MONTHLY_PASS_TEL,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_RIGHT,
                Constants.PRINTER_SIZE_MEDIUM);

        //vehicle number
        body[Constants.PRINTER_SIXTH_LINE][0]= new Element(
                Constants.PRINTER_KEY_REG_NUMBER,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_MEDIUM);

        //valid

        body[Constants.PRINTER_SEVENTH_LINE][0] = new Element(
                Constants.PRINTER_KEY_MONTHLY_PASS_VALID_FROM,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_MEDIUM);

        body[Constants.PRINTER_EIGTH_LINE][0] = new Element(
                Constants.PRINTER_KEY_MONTHLY_PASS_VALID_TILL,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_MEDIUM);


        //transID
        body[Constants.PRINTER_NINTH_LINE][0] = new Element(
                Constants.PRINTER_KEY_MONTHLY_PASS_TRANS_ID,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_MEDIUM);

        //transID
        body[Constants.PRINTER_TENTH_LINE][0] = new Element(
                Constants.PRINTER_KEY_MONTHLY_PASS_FEES,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_MEDIUM);


        mMonthlyPassFormat.setBody(body);

        Element[][] footer = new Element
                [Constants.PRINTER_MAX_MONTHLY_PASS_FOOTER_ELEMENTS]
                [Constants.PRINTER_MAX_MONTHLY_PASS_FOOTER_SUB_ELEMENTS];

        //lost ticket
        footer[Constants.PRINTER_FIRST_LINE][0] = new Element(
                Constants.PRINTER_KEY_MONTHLY_PASS_PAY_FEES_ONLINE,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_CENTER,
                Constants.PRINTER_SIZE_MEDIUM);

/*        footer[Constants.PRINTER_SECOND_LINE][0] = new Element(Constants.PRINTER_NEWLINE,
                Constants.PRINTER_TYPE_NEWLINE,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_SMALL);*/



        //disclaimer
        footer[Constants.PRINTER_SECOND_LINE][0]= new Element(Constants.PRINTER_KEY_DESC,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_CENTER,
                Constants.PRINTER_SIZE_SMALL);

        /*footer[Constants.PRINTER_FOURTH_LINE][0] = new Element(Constants.PRINTER_NEWLINE,
                Constants.PRINTER_TYPE_NEWLINE,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_SMALL);*/
        //simplypark URL
        footer[Constants.PRINTER_THIRD_LINE][0]= new Element(Constants.PRINTER_KEY_URL,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_SMALL);

        footer[Constants.PRINTER_FOURTH_LINE][0] = new Element(Constants.PRINTER_NEWLINE,
                Constants.PRINTER_TYPE_NEWLINE,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_SMALL);


        mMonthlyPassFormat.setFooter(footer);


    }

    private void setMonthlyPassReceiptFormat(){
        Element[][] headerElm = new Element
                [Constants.PRINTER_MAX_MONTHLY_PASS_RECEIPT_HEADER_ELEMENTS]
                [Constants.PRINTER_MAX_MONTHLY_PASS_RECEIPT_HEADER_SUB_ELEMENTS];


        //heading

        headerElm[Constants.PRINTER_FIRST_LINE][0] = new Element(
                Constants.PRINTER_KEY_MONTHLY_PASS_RECEIPT_HEADING,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_CENTER,
                Constants.PRINTER_SIZE_MEDIUM,
                true);
        //address
        headerElm[Constants.PRINTER_SECOND_LINE][0] = new Element(
                Constants.PRINTER_KEY_SPACE_ADDRESS,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_SMALL,true);


        mMonthlyPassReceiptFormat.setHeader(headerElm);


        //date and time


        Element[][] body = new Element[Constants.PRINTER_MAX_MONTHLY_PASS_RECEIPT_BODY_ELEMENTS]
                [Constants.PRINTER_MAX_MONTHLY_PASS_RECEIPT_BODY_SUB_ELEMENTS];

        //Amount receipved
        body[Constants.PRINTER_FIRST_LINE][0]= new Element(
                                         Constants.PRINTER_KEY_MONTHLY_PASS_RECEIPT_AMOUNT_RECEIVED,
                                         Constants.PRINTER_TYPE_TEXT,
                                         Constants.PRINTER_ALIGN_LEFT,
                                         Constants.PRINTER_SIZE_MEDIUM);
        //transId
        body[Constants.PRINTER_SECOND_LINE][0]= new Element(
                                        Constants.PRINTER_KEY_MONTHLY_PASS_RECEIPT_TRANS_ID,
                                        Constants.PRINTER_TYPE_TEXT,
                                        Constants.PRINTER_ALIGN_LEFT,
                                        Constants.PRINTER_SIZE_MEDIUM);

        //next payment due on
        body[Constants.PRINTER_THIRD_LINE][0] = new Element(
                                       Constants.PRINTER_KEY_MONTHLY_PASS_RECEIPT_NEXT_PAYMENT_DUE,
                                       Constants.PRINTER_TYPE_TEXT,
                                       Constants.PRINTER_ALIGN_RIGHT,
                                       Constants.PRINTER_SIZE_MEDIUM);




        mMonthlyPassReceiptFormat.setBody(body);

        Element[][] footer = new Element
                [Constants.PRINTER_MAX_MONTHLY_PASS_RECEIPT_FOOTER_ELEMENTS]
                [Constants.PRINTER_MAX_MONTHLY_PASS_RECEIPT_FOOTER_SUB_ELEMENTS];

        //lost ticket
        footer[Constants.PRINTER_FIRST_LINE][0] = new Element(
                Constants.PRINTER_KEY_MONTHLY_PASS_RECEIPT_PAY_FEES_ONLINE,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_CENTER,
                Constants.PRINTER_SIZE_MEDIUM);


        //simplypark URL
        footer[Constants.PRINTER_SECOND_LINE][0]= new Element(Constants.PRINTER_KEY_URL,
                Constants.PRINTER_TYPE_TEXT,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_SMALL);

        footer[Constants.PRINTER_THIRD_LINE][0] = new Element(Constants.PRINTER_NEWLINE,
                Constants.PRINTER_TYPE_NEWLINE,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_SMALL);
        footer[Constants.PRINTER_FOURTH_LINE][0] = new Element(Constants.PRINTER_NEWLINE,
                Constants.PRINTER_TYPE_NEWLINE,
                Constants.PRINTER_ALIGN_LEFT,
                Constants.PRINTER_SIZE_SMALL);


        mMonthlyPassReceiptFormat.setFooter(footer);


    }

    /* the caller needs to make sure this is run on a seperate thread or as async task */
    public boolean connect(String macAddress){
        try {
            mPrinterHandle.openBT(macAddress);
            return true;
        }catch(IOException ioe){
            Log.d("PRINTER", "Exception while connecting::" + ioe);
            return false;
        }
    }

    /* the caller needs to make sure this is run on a seperate thread or as async task */
    public boolean disconnect(){
        try {
            mPrinterHandle.closeBT();
            mPrinterHandle = null;
            return true;
        }catch(IOException ioe){
            Log.d("PRINTER", "Exception while disconnecting::" + ioe);
            return false;
        }
    }

    public void setPrinterSize(int size, Context context){

        mContext = context;

        if(size == Constants.PRINTER_SIZE_3_INCH) {
            //  mDataFormatter = new Prepare3InchData(context);
        }else if (size == Constants.PRINTER_SIZE_2_INCH){
            mDataFormatter = new Prepare2InchData(context,mPrinterType);
        }

        mEntryTicketHeaderTemplate = mDataFormatter.getEntryTicketHeaderTemplate(
                mEntryTicketFormat.getHeader());
        mEntryTicketFooterTemplate = mDataFormatter.getEntryTicketFooterTemplate(
                mEntryTicketFormat.getFooter());

        mExitTicketHeaderTemplate = mDataFormatter.getExitTicketHeaderTemplate(
                mExitTicketFormat.getHeader());
        mExitTicketFooterTemplate = mDataFormatter.getExitTicketFooterTemplate(
                mExitTicketFormat.getFooter());

        mReportHeaderTemplate = mDataFormatter.getReportHeaderTemplate(
                mReportFormat.getHeader());

        mReportFooterTemplate = mDataFormatter.getReportFooterTemplate(
                mReportFormat.getFooter());

        mMonthlyPassHeaderTemplate = mDataFormatter.getMonthlyPassHeaderTemplate(
                mMonthlyPassFormat.getHeader());

        mMonthlyPassFooterTemplate = mDataFormatter.getMonthlyPassFooterTemplate(
                                                                    mMonthlyPassFormat.getFooter());

        mMonthlyPassPaymentReceiptHeaderTemplate =
           mDataFormatter.getMonthlyPassReceiptHeaderTemplate(mMonthlyPassReceiptFormat.getHeader());

        mMonthlyPassPaymentReceiptFooterTemplate =
           mDataFormatter.getMonthlyPassReceiptFooterTemplate(mMonthlyPassReceiptFormat.getFooter());


    }

    public void print(String data) throws IOException{
        String dataToPrint = mDataFormatter.print(data);
        mPrinterHandle.printData(dataToPrint);
    }


    public void printEntryTicket(String dateTime, String parkingId, Boolean isParkAndRide,
                                 String regNumber,int vehicleType) throws IOException{

        try {
            byte[] dataToPrint = mDataFormatter.prepareEntryTicket(mEntryTicketHeaderTemplate,
                    mEntryTicketFooterTemplate,
                    mEntryTicketFormat.getBody(),
                    dateTime, parkingId,
                    isParkAndRide, regNumber, vehicleType);

             /*String toPrint = new String(dataToPrint, "UTF-8");
            Cache.setInCache(mContext, Constants.CACHE_PREFIX_LAST_PRINT_COMMAND, toPrint,
                    Constants.TYPE_STRING);



            boolean flag = mPrinterHandle.printData(dataToPrint);
            Cache.setInCache(mContext,Constants.CACHE_PREFIX_FLAG_EXCEPTION,flag,
                    Constants.TYPE_BOOL);

            Cache.setInCache(mContext,Constants.CACHE_PREFIX_LAST_PRINT_COMMAND_SIZE,dataToPrint.length,
                             Constants.TYPE_INT);

            mPrinterHandle.printData(mEntryTicketHeaderTemplate);
            mPrinterHandle.printData(dataToPrint);
            mPrinterHandle.printData(mEntryTicketFooterTemplate);
            */

            mPrinterHandle.printData(dataToPrint);

        }catch(Exception e){
            Cache.setInCache(mContext,Constants.CACHE_PREFIX_PRINT_EXCEPTION,e.toString(),
                             Constants.TYPE_STRING);
        }

    }

    public void printExitTicket(String entryDateTime, String exitDateTime, String parkingFees,
                                String nightChargesDesc,String regNumber,String duration,
                                int vehicleType) throws IOException{
        try {
            byte[] dataToPrint = mDataFormatter.prepareExitTicket(mExitTicketHeaderTemplate,
                    mExitTicketFooterTemplate,
                    mExitTicketFormat.getBody(),
                    entryDateTime, exitDateTime, parkingFees, nightChargesDesc,
                    regNumber, duration, vehicleType);

            /*String toPrint = new String(dataToPrint, "UTF-8");
            Cache.setInCache(mContext, Constants.CACHE_PREFIX_LAST_PRINT_COMMAND, toPrint,
                    Constants.TYPE_STRING);*/


            boolean flag = mPrinterHandle.printData(dataToPrint);
            /*Cache.setInCache(mContext, Constants.CACHE_PREFIX_FLAG_EXCEPTION, flag,
                    Constants.TYPE_BOOL);

            Cache.setInCache(mContext, Constants.CACHE_PREFIX_LAST_PRINT_COMMAND_SIZE, dataToPrint.length,
                    Constants.TYPE_INT);*/

        }catch(Exception e){
            Cache.setInCache(mContext,Constants.CACHE_PREFIX_PRINT_EXCEPTION,e.toString(),
                    Constants.TYPE_STRING);
        }


    }

    public void printReport(String reportPeriod,TransactionSummary summary) throws IOException{

      try{
        byte[] dataToPrint = mDataFormatter.prepareReport(
                mReportHeaderTemplate, mReportFooterTemplate, mReportFormat.getBody(),
                reportPeriod, summary);

        /*String toPrint = new String(dataToPrint,"UTF-8");
        Cache.setInCache(mContext, Constants.CACHE_PREFIX_LAST_PRINT_COMMAND, toPrint,
                Constants.TYPE_STRING);*/

          boolean flag = mPrinterHandle.printData(dataToPrint);
          /*Cache.setInCache(mContext, Constants.CACHE_PREFIX_FLAG_EXCEPTION, flag,
                  Constants.TYPE_BOOL);

          Cache.setInCache(mContext, Constants.CACHE_PREFIX_LAST_PRINT_COMMAND_SIZE, dataToPrint.length,
                  Constants.TYPE_INT);*/


      }catch(Exception e){
        Cache.setInCache(mContext,Constants.CACHE_PREFIX_PRINT_EXCEPTION,e.toString(),
                Constants.TYPE_STRING);
        }
    }

    public void printMonthlyPass(String vehicleReg, int vehicleType, String issuedTo,String email,
                                 String tel,String validFrom,String validTill,
                                 String transId,int perMonthFees) throws IOException{

        try {
            byte[] dataToPrint = mDataFormatter.prepareMonthlyPass(
                    mMonthlyPassHeaderTemplate, mMonthlyPassFooterTemplate, mMonthlyPassFormat.getBody(),
                    vehicleReg, vehicleType, issuedTo, email, tel, validFrom, validTill, transId, perMonthFees);

            /*String toPrint = new String(dataToPrint, "UTF-8");
            Cache.setInCache(mContext, Constants.CACHE_PREFIX_LAST_PRINT_COMMAND, toPrint,
                    Constants.TYPE_STRING);*/


            boolean flag = mPrinterHandle.printData(dataToPrint);
            /*Cache.setInCache(mContext, Constants.CACHE_PREFIX_FLAG_EXCEPTION, flag,
                    Constants.TYPE_BOOL);

            Cache.setInCache(mContext, Constants.CACHE_PREFIX_LAST_PRINT_COMMAND_SIZE, dataToPrint.length,
                    Constants.TYPE_INT);*/

        }catch(Exception e){
        Cache.setInCache(mContext,Constants.CACHE_PREFIX_PRINT_EXCEPTION,e.toString(),
                Constants.TYPE_STRING);
        }

    }


    public void printMonthlyPassReceipt(int amountTransferred, String transId, String nextPaymentDue) throws IOException{
        try {
            byte[] dataToPrint = mDataFormatter.prepareMonthlyPassReceipt(
                    mMonthlyPassPaymentReceiptHeaderTemplate, mMonthlyPassPaymentReceiptFooterTemplate,
                    mMonthlyPassReceiptFormat.getBody(), amountTransferred, transId, nextPaymentDue);

            /*String toPrint = new String(dataToPrint, "UTF-8");
            Cache.setInCache(mContext, Constants.CACHE_PREFIX_LAST_PRINT_COMMAND, toPrint,
                    Constants.TYPE_STRING);*/


            boolean flag = mPrinterHandle.printData(dataToPrint);
            /*Cache.setInCache(mContext, Constants.CACHE_PREFIX_FLAG_EXCEPTION, flag,
                    Constants.TYPE_BOOL);

            Cache.setInCache(mContext, Constants.CACHE_PREFIX_LAST_PRINT_COMMAND_SIZE, dataToPrint.length,
                    Constants.TYPE_INT);*/

        }catch(Exception e){
            Cache.setInCache(mContext,Constants.CACHE_PREFIX_PRINT_EXCEPTION,e.toString(),
                    Constants.TYPE_STRING);
        }

    }




}
