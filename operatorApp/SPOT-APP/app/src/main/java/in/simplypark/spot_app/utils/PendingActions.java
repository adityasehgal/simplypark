package in.simplypark.spot_app.utils;

import com.orm.SugarRecord;

import in.simplypark.spot_app.config.Constants;

/**
 * Created by aditya on 03/06/16.
 */
public class PendingActions extends SugarRecord {

    int     mActionName;
    String  mCarReg;
    long    mEntryTime;
    long    mExitTime;
    String  mParkingId;
    boolean mIsParkAndRide;
    int     mVehicleType;
    double  mParkingFees;

    public PendingActions(int action){
        mActionName = action;
        mCarReg = "";
        mEntryTime = 0;
        mExitTime  = 0;
        mParkingId = "";
        mIsParkAndRide = false;
        mVehicleType = Constants.VEHICLE_TYPE_CAR;
        mParkingFees = 0.00;
    }

    public PendingActions(int action,String carReg,long entryTime,String parkingId,
                          boolean isParkAndRide,int vehicleType){
        mActionName    = action;
        mCarReg        = carReg;
        mEntryTime     = entryTime;
        mExitTime      = 0;
        mParkingId     = parkingId;
        mIsParkAndRide = isParkAndRide;
        mVehicleType   = vehicleType;
        mParkingFees   = 0.00;

    }

    public PendingActions(int action,String parkingId, long exitTime, double parkingFees,
                          int vehicleType){
        mActionName    = action;
        mCarReg        = "";
        mEntryTime     = 0;
        mExitTime      = exitTime;
        mParkingId     = parkingId;
        mIsParkAndRide = false;
        mVehicleType   = vehicleType;
        mParkingFees   = parkingFees;
    }


    public int getActionName(){ return mActionName; }
    public String getCarReg(){ return mCarReg; }
    public long getEntryTime(){ return mEntryTime; }
    public long getExitTime(){ return mExitTime; }
    public String getParkingId() { return mParkingId; }
    public boolean getIsParkAndRide() { return mIsParkAndRide; }
    public int getVehicleType() { return mVehicleType; }
    public double getParkingFees() { return mParkingFees; }


}
