package in.simplypark.spot_app.db_tables;

import com.orm.SugarRecord;

/**
 * Created by root on 28/11/16.
 */

public class PaymentInfoTbl extends SugarRecord {
    String mBookingId;
    String mInvoiceId;
    String mPaymentId;

    public PaymentInfoTbl(){
        mBookingId = "";
        mInvoiceId = "";
        mPaymentId = "";
    }

    public void setBookingId(String bookingId){
        mBookingId = bookingId;
    }

    public String getBookingId(){
        return mBookingId;
    }

    public void setInvoiceId(String invoiceId){
        mInvoiceId = invoiceId;
    }

    public String getInvoiceId(){
        return mInvoiceId;
    }

    public void setPaymentId(String paymentId){
        mPaymentId = paymentId;
    }

    public String getPaymentId(){
        return mPaymentId;
    }

}
