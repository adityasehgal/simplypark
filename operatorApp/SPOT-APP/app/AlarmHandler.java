package in.simplypark.spot_app;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

import java.util.Calendar;

import hirondelle.date4j.DateTime;

public class AlarmHandler {

    public AlarmHandler(){}

    public AlarmHandler(Context context,Bundle extras, int timeOutInSeconds,boolean isRepeating) {
       /* AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, SystemNotificationsReceiver.class);

        intent.putExtra(Constants.BUNDLE_PARAM_DAY_OVER, extras);
        intent.setAction(Constants.TIMER_ACTION);
        PendingIntent pendingIntent =
                PendingIntent.getBroadcast(context, 0, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar time = Calendar.getInstance();
        time.setTimeInMillis(System.currentTimeMillis());

        time.add(Calendar.SECOND, timeOutInSeconds);
        alarmManager.set(AlarmManager.RTC_WAKEUP, time.getTimeInMillis(),
                pendingIntent);

        if(isRepeating){
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, time.getTimeInMillis(),
                    AlarmManager.INTERVAL_DAY, pendingIntent);
        }
        */
    }

    public AlarmHandler(Context context,String action, int startHour, int startMinute,
                            boolean isRepeating) {
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, SystemNotificationsReceiver.class);

        //intent.putExtra(Constants.BUNDLE_PARAM_DAY_OVER,extras);
        intent.setAction(action);

        PendingIntent pendingIntent =
                PendingIntent.getBroadcast(context, 0, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());

        calendar.set(Calendar.HOUR_OF_DAY, startHour);
        calendar.set(Calendar.MINUTE, startMinute);







        //calculate the first instance of the alaram
        DateTime now = DateTime.today(Constants.TIME_ZONE);
        DateTime alarmDateTime;

        int currentHour = now.getHour();
        int currentMinute = now.getMinute();

        int offsetHour   = 0;
        int offsetMinute = 0;

        if(currentHour < startHour){
            offsetHour = startHour - currentHour;

            if(currentMinute < startMinute){
               offsetMinute = startMinute - currentMinute;
            }else{
                offsetMinute = (startMinute + Constants.DATEDIFF_MINUTES_IN_HOUR) - currentMinute;
            }

            alarmDateTime = new DateTime(now.getYear(),now.getMonth(),now.getDay(),offsetHour,
                                           offsetMinute,0,0);

        }else{
            //the first alamr would be scheduled next day
            DateTime nextDay = now.plusDays(1);
            offsetHour   = startHour;
            offsetMinute = startMinute;
            alarmDateTime = new DateTime(nextDay.getYear(), nextDay.getMonth(),nextDay.getDay(),
                                          offsetHour,offsetMinute,0,0);
        }


        Log.e("ALARMHANDLER","Setting the alarm for :: " +
                Long.toString(alarmDateTime.getMilliseconds(Constants.TIME_ZONE)));
        Log.e("ALARMHANDLER","Setting alarm for :: " + alarmDateTime.toString());

        if(isRepeating){
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                                             alarmDateTime.getMilliseconds(Constants.TIME_ZONE),
                                             AlarmManager.INTERVAL_DAY, pendingIntent);
        }else{
            alarmManager.set(AlarmManager.RTC_WAKEUP,
                             alarmDateTime.getMilliseconds(Constants.TIME_ZONE),
                             pendingIntent);
        }

        /*
        ComponentName receiver = new ComponentName(context, SystemNotificationsReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
        */
    }

    public static void startDayEndTimer(Context context, String action,
                                        String openingDateTime, String closingDateTime){

        //DateTime oDateTime = new DateTime(openingDateTime);
        DateTime cDateTime = new DateTime(closingDateTime);

        int closingHour = cDateTime.getHour();
        int closingMinute = cDateTime.getMinute();

        AlarmHandler alarm = new AlarmHandler(context,Constants.TIMER_ACTION,
                closingHour,closingMinute,true);


    }



}
