<?php
/**
 * Class GoogleVisionApiRequest
 */
class GoogleVisionApiResponse
{
	
	private function getAnnotations($type, $response, &$annotations) 
	{
		if(empty($response) or empty($response["responses"])) {
			return;
		}
		
		foreach ($response["responses"] as $key => $val) {
			if(isset($val[$type])) {
				$annotations = $val[$type];
			}
		}
	}
	
	private function getFirstAnnotationElement($type, $response, &$element)
	{	
		$annotations = array();
		$this->getAnnotations($type, $response, $annotations);
		if(empty($annotations)) {
			return;
		}
		$element = $annotations[0];		
	}
	
	public function getTextAnnotations($response, &$text_annotations)
	{
		$annotations = array();
		$this->getAnnotations("textAnnotations", $response, $annotations);
		if(empty($annotations)) {
			return;
		}
	
		foreach ($annotations as $annotation) {
			$text_annotations[]= array('description'=>$annotation["description"],
						                'position'=>$annotation["boundingPoly"]["vertices"]);
				
		}	
	}

	public function getLabelAnnotations($response, &$label_annotations)
	{
		$annotations = array();
		$this->getAnnotations("labelAnnotations", $response, $annotations);
		if(empty($annotations)) {
			return;
		}
	
		foreach ($annotations as $annotation) {
			$label_annotations[]= array('description'=>$annotation["description"],
					'confidence'=>$annotation["score"]);
	
		}
	}
	public function getTextAndPosn($response, $regex_format, $special_chars, &$text_annotation)
	{
		$annotations = array();
		$this->getAnnotations("textAnnotations", $response, $annotations);
		if(empty($annotations)) {
			return;
		}
	
		$description;
		$posn;
		foreach ($annotations as $annotation) {
			if(isset($annotation["description"])) {
				$trimmed_array= trim($annotation["description"], $special_chars);
				$output_array =  array();
	
				$posn = $annotation["boundingPoly"]["vertices"];
				preg_match($regex_format, $trimmed_array, $output_array);
				if(!empty($output_array)) {
					foreach ($output_array as $val) {
						if($val != "\n") {
							$description.= $val;
							break;
						}
					}
				}
	
			}
		}
		if(!empty($description)) {
			$text_annotation["description"] = $description;
		}
		if(!empty($posn)) {
			$text_annotation["posn"] = $posn;
		}
	
	}

	public function getFirstLabelAndScore($response, &$label_annotation)
	{
		$element = array();
		$this->getFirstAnnotationElement("labelAnnotations", $response, $element);
		if(empty($element)) {
			return;
		}
			
		$label_annotation["description"] = $element["description"];
		$label_annotation["score"] = $element["score"];
	}
	
	
	
}