<?php
#namespace App\Vision;
App::uses('HttpSocket', 'Network/Http');
App::uses('Component', 'Controller');

App::build(array('Vendor' => array(APP . 'Vendor' . DS . 'Vision' . DS)));
App::uses('GoogleVisionApiRequest', 'Vendor');
App::uses('GoogleVisionApiResponse', 'Vendor');


class GoogleVisionApi 
{
	
	public function post($uri = null, $data = array(), $request = array()) {
		$request = Hash::merge(array('method' => 'POST', 'uri' => $uri, 'body' => $data), $request);
		return $this->$request;
	}

	public function detect($input, &$output)
	{
		// Send a JSON request body.
		$http = new HttpSocket(array('ssl_verify_host' => false));
		
		$visionApiRequest = new GoogleVisionApiRequest();

		$visionApiRequest->setUri($input["uri"]);		
		
		$detectText = false;
		$detectLabel = false;
		foreach ($input["features"] as $feature){
			if ($feature["type"] == "TEXT") {
				$visionApiRequest->addFeature("TEXT_DETECTION", $feature["maxResults"]);							
				$detectText = true;
			} else if ($feature["type"] == "LABEL") {
				$visionApiRequest->addFeature("LABEL_DETECTION", $feature["maxResults"]);
				$detectLabel = true;
			} else {
				throw new Exception("feature not supported".$feature["type"], 1);
			}			
		}
		
		$visionApiRequest->setImage($input["path"]);
		
		/*
		
		$this->request = array(
				'method' => 'POST',
				'uri' => array(
						'host' => 'uploads.gdata.youtube.com',
						'path' => '/feeds/api/users/default/uploads',
				),
				'header' => array(
						'Content-Type' => 'multipart/related; boundary="' . $boundaryString . '"',
						'Slug' => $data[$this->alias]['file']['name']
				),
				'auth' => array(
						'method' => 'OAuth',
				),
				'body' => $body,
		); */
		
		$requestLocal = $visionApiRequest->request();
		
		$httprequest = array(
				'method' => 'POST',
				'uri' => $requestLocal["uri"],
				'body' =>json_encode($requestLocal["body"]),
				
				'header' => array(
						'Content-Type' => 'application/json'
				)
		);

		//CakeLog::write('debug', print_r($httprequest,true));
		$response = $http->request($httprequest);
		//CakeLog::write('debug', print_r($response,true));
		
		
		//$responseStr = file_get_contents('/var/www/html/simplypark/app/tmp/google_vision_api_response_text.json', true);
		
		
		//do processing
		
		if($response->isOk()) {
			//if(True) {

			$responseData = json_decode($response->body, true);
			//$responseData = json_decode($responseStr, true);

			//CakeLog::write('debug', print_r($responseData,true));
			$visionApiResponse = new GoogleVisionApiResponse();
			if($detectText) {
				$textAnnotations = array();
				$visionApiResponse->getTextAnnotations($responseData, $textAnnotations);
				if(!empty($textAnnotations)) {
					$output["textAnnotations"] = $textAnnotations;
				}
			}
			if($detectLabel) {
				$labelAnnotations = array();
				$visionApiResponse->getLabelAnnotations($responseData, $labelAnnotations);
				if(!empty($labelAnnotations)) {
					$output["labelAnnotations"] = $labelAnnotations;
				}
			}

			$output["response"] = $responseData;

		} else {
			$status;
			$status.= 'response error...status'.$response->reasonPhrase;
			throw new Exception($status, 1);
		}


	}
	
	 
}
?>
