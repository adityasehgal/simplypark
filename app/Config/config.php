<?php

	$config['UserTypes'] = array(
			'Admin' => 1,
			'SubAdmin' => 2,
			'User' => 3
		);

	$config['Bollean'] = array(
			'True' => 1,
			'False' => 0
		);

	$config['Activate'] = array(
			'True' => 1,
			'False' => 0
		);
	$config['EmailTemplateId'] = array(
			'registration_notification'    => 1,
			'forgot_password_email'    => 2,
			'resend_activation' => 3,
			'add_space' => 4,
			'contact_us' => 5,
			'space_approved' => 6,
			'space_disapproved' => 7,
			'search_not_found' => 8,
			'booking_confirmation' => 9,
			'alert_booking' => 10,
			'approve_booking_email' => 11,
			'disapprove_booking_email' => 12,
			'reminder_approval' => 13,
			'auto_disapprove' => 14,
			'cancel_booking' => 15,
			'installment_remidmer' => 16,
			'installment_amount_paid' => 17,
			'newly_registered_users' => 18,
			'newly_made_bookings' => 19,
			'pending_spaces' => 20,
			'immediate_booking_alert_to_owner' => 21,
            'cancel_long_booking' => 22,
            'cancel_long_booking_default' => 23,
			'pay_to_owner' => 25,
			'automatic_booking_approval_email_to_owner' => 26,
                        'cancel_booking_email_to_driver' => 27,
                        'cancel_booking_email_to_owner' => 28,
                        'cancel_long_term_booking_email_to_driver' => 29,
                        'cancel_long_term_booking_email_to_owner' => 30
		);

	$config['MessageId'] = array(
			'approve_disapprove_booking'    => 1,
			'booking_confirmed'    => 3,
			'booking_not_confirmed' => 4,
			'booking_cancelled_by_driver' => 5,
			'cancellation_request_reveived' => 6,
			'cancellation_request_reveived_to_admin' => 7,
			'due_installment' => 8,
			'installment_payment_overdue_to_owner' => 9,
			'installment_payment_overdue_to_admin' => 10,
			'amount_successfully_received' => 12,
			'payment_received' => 13,
			'cancellation_request_processed' => 14,
			'booking_expire_for_listed_space_to_owner' => 15,
			'booking_expire_for_listed_space_to_driver' => 16,
			'booking_request_noted' => 17,
			'booking_request_pending' => 18,
			'installment_payment_overdue_to_driver' => 19,
                        'free_space_location' => 20
		);
	/** Mobile Verification **/

	$config['VerifyMobile'] = array(
    		'Url' => "https://webaroo-mobile-verification.p.mashape.com/mobileVerification?"
    		
    	);
	
	$config['X-Mashape'] = array(
			'X-Mashape-Key' => ' AeYO1JgBZ8mshXTvIMvkwdJ5cdjTp1LRfTJjsnmWqtLrX7xRFt',
			'Accept' => 'application/json'
		);
	/** ------------------ **/

	$config['Email'] = array(
			'EmailMarketing' => 'customercare@simplypark.in',
			'EmailName' => 'SimplyPark',
			'EmailAdmin' => 'webapp@simplypark.in',
			'EmailDisplay' => 'customercare@simplypark.in'
		);
        $config['PropertyType'] = array(
                               'PrivateResidence' => 1,
                               'GatedCommunity' => 2
                              );
        $config['BookingType'] = array('hourly' => 1, 
                                       'daily' => 2,
                                       'weekly' => 3,
                                       'monthly' => 4,
                                       'yearly' => 5
                                     );


	$config['SITE_SETTINGS'] = array(
		   'Name' => 'Admin @ SimplyPark',
		   'Slogan' => 'The first step to park your car.',
		   'Copyright' => "Copyright &copy; 2015 SimplyPark Pvt. Ltd."
		);

	$config['PartnerLogoPath'] = 'files/partners_logo/';
	$config['EventImagePath'] = 'files/Event/event.';
	$config['SpaceImagePath'] = 'files/SpacePic/';
	$config['UserImagePath'] = '/files/User/user.';
	$config['QrImagePath'] = 'files/Qr/';
	$config['OfflineTokenImagePath'] = 'files/OfflineTokens/';
	$config['SpotterImagePath'] = 'files/Spotter/';
	$config['UserDummyImagePath'] = 'userdummy.jpg';

	$config['EventGridClass'] = array(
			1 => '12',
			2 => '6',
			3 => '4',
			4 => '3'
		);

	$config['SpaceStatus'] = array(
			'None' => 0,
			'Approved' => 1,
			'NotApproved' => 2
		);

	$config['UserAddressType'] = array(
			'Billing' => 1,
			'Home' => 2
		);

	$config['SimplyParkServiceTax'] = 14;
	$config['BulkCarId'] = 12;

        $config['EntryThresholds'] = array(
                                  'preEntryThreshold' => 15,  //in minutes
                                  'postEntryThreshold' => 15  //in minutes
                                  );
   
        $config['BookingValidationStatus'] = array(
                                   'validBooking' => 0,
                                   'noSuchBooking' => 1,
                                   'bookingWaitingForApproval' => 2,
                                   'bookingDisapproved' => 3,
                                   'bookingExpired' => 4,
                                   'bookingCancelled' => 5,
                                   'tooEarly' => 6,
                                   'tooLate' => 7,
                                   'reentry' => 8,
                                   'exitWithoutEntry' => 9,
                                   'exitAlreadyRecorded' => 10,
                                   'paymentDue' => 11
                                   );

        $config['BookingEntryOrExit'] = array(
                                        'default' => 0,
                                        'entry' => 1,
                                        'exit' => 2,
                                        'reEntry' => 3,
                                        'exitWithoutEntry' => 4,
					'alreadyUsed' => 5,
					'cancelled' => 6,
					'exitNotRecorded' => 7
                                     );

	$config['CommissionPaidBy'] = array(
			'Driver' => 1,
			'Owner' => 2
		);

	$config['TaxPaidBy'] = array(
			'Owner' => 1,
			'SimplyPark' => 2
		);
	$config['RazorPayKeys'] = array(
			'KeyID' => 'rzp_live_d9HZLLSQxifZxJ',
			'KeySecret' => 'S6jQkaPlEdsCclwLHFBBW3lH',
			//'KeyID' => 'rzp_test_DjM3xVZrzsFHYv',
			//'KeySecret' => 'DK0wOURDV7ZUKyApiAgbSFoW',
			'APIURL' => 'https://api.razorpay.com/v1/payments/',
			'INVOICEAPIURL' => 'https://api.razorpay.com/v1/invoices/'
		);
	
        $config['RazorPayInvoiceKeys'] = array(
			'KeyID' => 'rzp_live_d9HZLLSQxifZxJ',
			'KeySecret' => 'S6jQkaPlEdsCclwLHFBBW3lH',
			//'KeyID' => 'rzp_test_DjM3xVZrzsFHYv',
			//'KeySecret' => 'DK0wOURDV7ZUKyApiAgbSFoW',
			'INVOICEAPIURL' => 'https://api.razorpay.com/v1/invoices/'
		);

	$config['HTTPStatusCode'] = array(
			'OK' => 200,
			'BadRequest' => 400,
			'NotAuthorized' => 401
		);
	
        $config['GoogleShortURLKeys'] = array(
			'APIURL' => 'https://www.googleapis.com/urlshortener/v1/url',
                        'APIKEY' => 'AIzaSyBTb140HRHoycmh9HJhbNJ7JFz47WeXTBA',
                        'LongURL' =>'http://maps.google.com/maps?daddr='
		);

	$config['SpaceUnavailabelPrice'] = 0.00;

	$config['StatusType'] = array(
			'Error' => 0,
			'Success' => 1
		);

	$config['BookingMadeReceivedStatus'] = array(
			'Approved' => 'Approved',
			'DisApproved' => 'DisApproved',
			'Waiting' => 'Waiting',
            'Cancelled' => 'Cancelled',
			'Expired' => 'Expired'
		);

	$config['SaveBookingStatus'] = array(
			'Approved' => 1,
			'DisApproved' => 2
		);


	$config['UserPerFieldPercentage'] = '6.81875';

	$config['YearDays'] = '366';
	$config['MonthDays'] = '30';

	$config['DepositDays'] = '30';
    $config['MonthlyNoticePeriod'] = 7;
    $config['YearlyNoticePeriod'] = 30;


    $config['NearestSpaceDistance'] = '1.0';
    $config['DefaultLat'] = 28.543748;
    $config['DefaultLng'] = 77.248388;
    $config['radius'] = 1.0;   	    



    $config['BookingStatus'] = array(
    		'Waiting' => 0,
    		'Approved' => 1,
    		'DisApproved' => 2,
    		'CancellationRequested' => 3,
    		'Cancelled' => 4,
    		'Expired' => 5,
    		'CancelledThroughPaymentDefault' => 6
    	);
    $config['BookingStatusText'] = array(
    		'Waiting' => 'Waiting',
    		'Approved' => 'Approved',
    		'DisApproved' => 'DisApproved',
    		'CancellationRequested' => 'Cancelled(in notice period)',
    		'Cancelled' => 'Cancelled',
    		'Expired' => 'Expired',
    		'CancelledThroughPaymentDefault' => 'Cancelled Through Payment'
    	);
    
   $config['BookingStatus1'] = array(
    		'Waiting' => 0,
                'Approved' => 1,
    		'Disapproved' => 2,
    		'CancellationRequested' => 3,
    		'Cancelled' => 4,
    		'Expired' => 5,
                'CancelledPaymentDefault' => 6
    	);

    $config['ReminderInstallmentInterval'] = '3'; //2 Means three days interval i.e. it starts from 0 to 3.


    $config['SMSNotifications'] = true;

    $config['SMSAPICredentials'] = array(
    		'UserID' => '2000148446',
    		'Password' => 'Jm4dbwXOD',
    		'URL' => "http://enterprise.smsgupshup.com/GatewayAPI/rest?"
    	);

    $config['AdminUserID'] = 1;

    $config['AutoCancelBookingDuration'] = '2';

    $config['CarTypes'] = array(
    		1 => 'Hatchback',
    		2 => 'SUV',
    		3 => 'Minivans/MPV'
    	);

    $config['BookingTableStatusField'] = 'status';

    $config['PayToOwnerDueDate'] = 2;

    $config['Server'] = array(
    		'Live' => 'www.simplypark.in',
    		'Test' => 'test.simplypark.in',
    		'Local' => '127.0.0.1'
    	);

	include ROOT."/ConfigrationLibrary.php";
	$config['AppDefaultDatabase'] = ConfigrationLibrary::getConfigVars('database');
	$config['AppDefaultEnvironment'] = ConfigrationLibrary::ENV;
	$config['AppDefaultDatabaseDefault'] = ConfigrationLibrary::DATABASEDEFAULT;
	$config['ROOTURL'] = ConfigrationLibrary::getConfigVars('RootUrl');
	$config['GoogleSiteSecretKey'] = ConfigrationLibrary::getConfigVars('GoogleSiteSecretKey');

	$config['booking_confirmation'] = array(
			0 => 'I want to manually confirm',
			1 => 'Automatically accept bookings'
		);

	$config['weekdays'] = array(
			'mon' => 1,
            'tue' => 1,
            'wed' => 1,
            'thu' => 1,
            'fri' => 1
		);

	$config['weeklongdays'] = array(
			'mon' => 'Monday',
            'tue' => 'Tuesday',
            'wed' => 'Wednesday',
            'thu' => 'Thrusday',
            'fri' => 'Friday',
            'sat' => 'Saturday',
            'sun' => 'Sunday'
		);

	$config['weekends'] = array(
			'sat' => 1,
            'sun' => 1
		);

	$config['coupon_users'] = array(
			1 => 'Space Owner',
            2 => 'Driver'
		);
	$config['discount_type'] = array(
			0 => 'Flat Discount',
            1 => 'Discount in Percentage'
		);
	$config['deposit_months'] = array(
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4'
		);
	$config['gated_community'] = 2;
	
	$config['space_approval_states'] = array(
			'wating' => 0, 
			'approved' => 1,
			'disapproved' => 2
		);

	$config['GCM'] = array(
		          'url' => 'https://gcm-http.googleapis.com/gcm/send',
			  //'serverKey' => 'AIzaSyBsH6irV1ud7Xgw7OV7wW_YJv3z4w3gr2U'
			   'serverKey' => 'AIzaSyDo_J-MuqR3pmOmNc9fPlmZF-byQ0OubxM'
			   );
	

        //temp changes. remove after a certain booking
        $config['TempSpaceID'] = 122;
	$config['TempCommission'] = 75;
        $config['TempSpaceID1'] = 58;
	$config['TempCommission1'] = 75;
	$config['TempSpaceIDPercent'] = 180;
	$config['TempPerCentCommission'] = 5;
	$config['rmlSpaceId'] = 243;


	/********************************************************************
	 *   Operator APP and Mobile APP common Constants.
	 *   The values MUST match
	 * ******************************************************************/
	$config['UpdateType'] =  array('offline_entry_exit_update' => 1,
		                       'simplypark_booking_update' => 2,
				       'online_entry_exit_update' => 3,
			               'operatorApp_booking_update' => 4,
                                       'operatorApp_booking_modification' => 5);
	$config['PaymentStatus'] =  array('no_dues' => 0,
		                          'payment_pending' => 1);
	
        $config['PaymentInterval'] =  array('na' => 0,
		                            'monthly' => 1);

	/*********************************************************************
	 *              Operator APP related Constants
	 *********************************************************************/
        $config['MaxOperators'] = 16;	
	$config['OperatorGCM'] = array(
		          'url' => 'https://gcm-http.googleapis.com/gcm/send',
			  //'serverKey' => 'AIzaSyB8dT-Qi_J6rcu9ldo41lZMTbKtK2zQRcA'
			  'serverKey' => 'AIzaSyDb2RUTfrund88T333hXeI1uX8kV1_OXso'
		  );
	$config['Action'] = array('entry' => 1,
		                  'exit' => 2,
				  'sync' => 3,
				  'cancel' => 4,
				  'update' => 5,
                                  'simplypark_booking_update' => 6,
                                  'simplypark_online_entry' => 7,
				  'simplypark_online_exit' => 8,
				  'simplypark_online_cancel' => 9,
				  'simplypark_online_update' => 10,
				  'operator_app_generated_booking_update' => 11,
				  'operator_app_generated_booking_modification' => 12
	                         );

	$config['PushToServerResults'] = array('failed' => '0',
		                               'success' => '1',
					       'stale' => '2');

        $config['PushToServerUpdateType'] = array('OfflineEntryExitUpdate' => 1,
		                                  'OnlineEntryExitUpdate' => 2,
					          'opAppBookingUpdate' => 3,	
					          'opAppBookingModification' => 4);	

	$config['VehicleType'] = array('Hatchback' => 1,
		                       'Suv' => 2,
				       'Mpv' => 3,
				       'Bike' => 4,
                                       'MiniBus' => 5,
                                       'Bus' => 6);
	
	$config['AppVehicleType'] = array('Car' => 1,
		                          'Bike' => 2,
                                          'MiniBus' => 3,
                                          'Bus' =>4);

	$config['AdminId'] = 1; 

	$config['ParkingOwnerOperatorId'] = 0;

        $config['TokenType'] = array('Widget' => 0,
                                     'operatorApp' => 1,
                                     'masterOperatorApp' => 2);
	
	$config['PaymentMode'] = array('Cash' => 1,
                                       'Card' => 2,
                                       'Online' => 3);

        $config['maxPullResponse'] = 256;
    
    /*
     * (([A-z]{0,2})([,.-]?|\s{0,4}|(\\n)?)(\w{0,2})([,.-]?|\s{0,4}|(\\n)?)(\w{0,4})([,.-]?|\s{0,4}|(\\n)?)(\d{4}))
     * 
     * 
     */
    $config['MinimumLabelDetectionThreshold'] = 70;
	
    //please make sure all search terms added are in lowercaps
    $config['CarSearchTerms']  = array('car');
    $config['BikeSearchTerms'] = array('bike','moped','scooter','motorcycle','motorcycling'); 	
    $config['NumberPlate'] = array('regex_format'=> '((\\n)?([A-z]{0,2})(\\n)?(\w{0,2})(\\n)?(\w{0,4})(\\n)?(\d{4})(\\n)?)',
        		                    'special_chars'=>array(' ','.','-',','));
        
    $config['VisionApiDefault'] = array('type' => 1, 'cfg' =>  array('KeyID' => 'AIzaSyDHihfD3695_S-mqnurW8vcbYH8nISfnYE', 
    							'host' => 'https://vision.googleapis.com',
    						    'uri' => 'v1/images:annotate'));
    
    $config['gooleApi'] =array('KeyID' => '', 
    							'host' => 'https://vision.googleapis.com',
    						    'uri' => 'v1/images:annotate');
    $config['msApi'] = array('KeyID' => '', 
    							'host' => '',
							'uri' => '');

    $config['DetectionResult'] = array('Success' => 0,
	                                'Failed' => 1);

   $config['DetectionFailureReason'] = array('VehicleCategoryNotFound' => 1,
	                                     'RegNumberNotFound' => 2);
    
        $config['maxPullResponse'] = 256;

        $config['ReturnTypes'] = array('OK' => 0,
                                       'Stale' => 1,
                                       'NoSuchRecord' => 2,
                                       'FailedToSave' => 3);

?>
