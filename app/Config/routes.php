<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

   //Router::mapResources('blob');
   //Router::parseExtensions();
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'homes', 'action' => 'index'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	Router::connect('/admin', array('controller' => 'admins', 'action' => 'login', 'prefix' => 'admin'));
	//Router::connect('/blob', array('controller' => 'blob', 'action' => 'index'));
	Router::parseExtensions('json');
        
	
	// index

	Router::connect(
	   '/opauth-complete/*',
	   array('controller' => 'homes', 'action' => 'opauth_complete')
   );

	/******************************************************************
	 * Rest Routes
	 *
	 *
	 * ***************************************************************/
	// detect
         
        Router::connect('/detect/:spaceid/:device/:operation/:parkingid/:timestamp', 
          array(
              'controller' => 'rest',
	      'action' => 'detect',
               ),
          array('pass' => array('spaceid','device','operation','parkingid','timestamp'))
       );

        Router::connect('/blob/:spaceid/:operation/:timestamp/:deviceid/*', 
          array(
              'controller' => 'rest',
	      'action' => 'add',
               ),
          array('pass' => array('spaceid','operation','timestamp','deviceid'))
       );
	
        Router::connect('/occupancy/:spaceid', 
          array(
              'controller' => 'rest',
	      'action' => 'count',
              ),
          array('pass' => array('spaceid'))
       );
	
	Router::connect('/parkings', 
          array(
              'controller' => 'rest',
	      'action' => 'parkings',
               )
        );

	Router::connect('/parkings/:id', 
          array(
              'controller' => 'rest',
	      'action' => 'parkings',
               ),
          array('pass' => array('id'))
       );


	
	Router::parseExtensions('json');

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
