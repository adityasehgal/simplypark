<?php
App::uses('AuthComponent', 'Controller/Component');
class DiscountCoupon extends AppModel {
	public $validate = array(
            'name' => array(
                    'required' => array(
                        'rule' => array('notEmpty'),
                        'message' => 'Coupon name is empty.'
                    ),
            ),
            'description' => array(
                    'required' => array(
                        'rule' => array('notEmpty'),
                        'message' => 'Coupon description is empty.'
                    ),
            ),
            'code' => array(
                    'required' => array(
                        'rule' => array('notEmpty'),
                        'message' => 'Coupon code is empty.'
                    ),
                    'unique' => array(
                        'rule'    => array('isUniqueCode'),
                        'message' => 'This Code is already in use',
                    )
            ),
            // 'discount_percentage' => array(
            //         'required' => array(
            //             'rule' => array('notEmpty'),
            //             'message' => 'Coupon Discount Percentage is empty.'
            //         ),
            // ),
            // 'max_amount_discount' => array(
            //         'required' => array(
            //             'rule' => array('notEmpty'),
            //             'message' => 'Coupon Max Amount Discount is empty.'
            //         )
            // ),
        );

    function isUniqueCode($check) {
 
        $code = $this->find(
            'first',
            array(
                'fields' => array(
                    'DiscountCoupon.id'
                ),
                'conditions' => array(
                    'DiscountCoupon.code' => $check['code']
                )
            )
        );
 
        if(!empty($code)){
            if(isset($this->data[$this->alias]['id']) && $this->data[$this->alias]['id'] == $code['DiscountCoupon']['id']){
                return true; 
            }else{
                return false; 
            }
        }else{
            return true; 
        }
    }

/**
 * Method validateDiscountCouponStatus to check that is_frontend field is not set while inactivate any coupon
 *
 * @param $id int id of the discount coupon
 * @return bool
 */
    function validateDiscountCouponStatus($id = null) {
        $getCoupon = $this->findById($id, array('is_frontend'));
        if ($getCoupon['DiscountCoupon']['is_frontend'] == Configure::read('Bollean.True')) {
            return false;
        }
        return true;
    }

/**
 * Method checkIsActivated to check is activated status
 *
 * @param $id int id of the discount coupon
 * @return bool
 */
    function checkIsActivated($id = null) {
        $getCouponStatus = $this->findById($id, array('is_active'));
        if ($getCouponStatus['DiscountCoupon']['is_active'] == Configure::read('Bollean.True')) {
            return true;
        }
        return false;
    }

/**
 * Method checkIsActivated to check is activated status
 *
 * @param $id int id of the discount coupon
 * @return bool
 */
    function getCouponSetFrontend() {
        $getFrontendSetCouponID = $this->findByIsFrontend(Configure::read('Bollean.True'), array('id'));
        if (!empty($getFrontendSetCouponID)) {
            return $getFrontendSetCouponID['DiscountCoupon']['id'];
        }
        return false;
    }

/**
 * Method getCouponDetails to get details of a particular coupon
 *
 * @param $couponCode string the coupon code of the discount coupon
 * @return $couponDetails array containing the coupon details of a particular discount coupon
 */
    function getCouponDetails($couponCode = null) {
        $couponDetails = $this->find(
                                'first',
                                array(
                                        'conditions' => array(
                                                'BINARY (`DiscountCoupon`.`code`) LIKE' => $couponCode,
                                                'is_active' => Configure::read('Bollean.True'),
                                                'is_deleted' => Configure::read('Bollean.False'),
                                            ),
                                        'fields' => array(
                                                'id',
                                                'for_user_type',
                                                'discount_type',
                                                'flat_amount',
                                                'discount_percentage',
                                                'max_amount_discount',
                                                'is_one_time_use',
                                                'is_for_bulk_booking',
                                                'num_discounted_installments',
                                                'property_type_id'
                                            ),
                                    )
                            );
        return $couponDetails;
    }
}
