<?php
class Booking extends AppModel {

	var $belongsTo = array('Space', 'User', 'UserCar','SpacePark','Operator');
	var $hasOne = array('Transaction','CouponApply');
	var $hasMany = array('BookingInstallment',);
	var $actsAs = array('Containable');

/**
 * Method getBookingRecordCongratulationPage to getting the booking record
 *
 *
 * @param $bookingID int the id of the booking
 * @return $getBookingRecord array containing the record of the booked space
 */
	function getBookingRecordCongratulationPage($bookingID = null) {
		$getBookingRecord = $this->find('first',
				array(
					'conditions' => array(
						'Booking.id' => $bookingID
						),
					'fields' => array(
						'first_name',
						'last_name',
						'start_date',
						'end_date',
						'email',
						'booking_price',
						'refundable_deposit',
                                                'num_slots',
						Configure::read('BookingTableStatusField'),
                                                'qrcodefilename'
						),
					'contain' => array(
						'Space' => array(
							'fields' => array(
								'id',
								'name',
								'flat_apartment_number',
								'address1',
								'address2',
								'booking_confirmation',
								'post_code',
								'property_type_id',
								'tower_number',
								'city_id',
								'state_id'
								),
							'User' => array(
								'UserProfile' => array(
									'fields' => array(
										'mobile'
										)
									),
								'fields' => array(
									'id',
									'email'
									)
								),
							'State' => array(
									'fields' => array(
											'name'
										)
								),
							'City' => array(
								'fields' => array(
									'name'
									)
								)
						),
						'Transaction' => array(
									'payment_id'
						),
						'SpacePark' => array(
									'park_number'
						),
						'UserCar' => array(
								'fields' => array(
									'registeration_number',
                                                                        'car_type'
									)
								),
					)
					)
								);
		//pr($getBookingRecord);die;
		return $getBookingRecord;
	}

/**
 * Method getBookingInfo to get the space from space id
 *
 * @param $spaceID the id of the particular space
 * @return $spaceInfo array containing the information for space
 */
    public function getBookingInfo($bookingID = null) {
		$options['conditions'] = array(
									'Booking.id' => $bookingID
								);
    	// $this->contain(array('BookingInstallment','UserCar'));
    	$options['contain'] = array( 
				        		'Space' => array(
									'fields' => array(
										'id',
										'name',
										'flat_apartment_number',
										'address1',
										'address2',
										'booking_confirmation',
										'post_code',
										'property_type_id',
										),
				        		    'State' => array(
										'fields' => array(
												'name'
											)
										),
									'City' => array(
										'fields' => array(
												'name'
											)
										)
								),
				        		'User' => array(
				        			 
									'WithdrawalSources' => array(
										'fields' => array(
											'account_number'
											),
										
										)
									),
				        		'Transaction' => array(
									'payment_id'
									),
				        		'BookingInstallment',
				        		'UserCar'
				          
				            	);
        $bookingInfo = $this->find('first',$options);
        return $bookingInfo;
    }


/**
 * Method getConfirmedBookings to get all the bookings which are confirmed from space id
 *
 *
 * @return total count of confirmed bookings
 */
    public function getConfirmedBookings() {
        $bookingCount = $this->find('count',
                                        array(
                                                'conditions' => array(
                                                        'Booking.user_id' => CakeSession::read('Auth.User.id'),
                                                        'Booking.'.Configure::read('BookingTableStatusField') => Configure::read('BookingStatus.Approved')
                                                    ), 
                                            )
                    );
        return $bookingCount;
    }


/**
 * Method getConfirmedBookings to get all the bookings which are confirmed from space id
 *
 *
 * @return total count of confirmed bookings
 */
    public function getUnderApprovalBookingsCount() {
        $bookingCount = $this->find('count',
                                        array(
                                                'conditions' => array(
                                                        'Booking.space_user_id' => CakeSession::read('Auth.User.id'),
                                                        'Booking.'.Configure::read('BookingTableStatusField') => Configure::read('BookingStatus.Waiting')
                                                    ), 
                                            )
                    );
        return $bookingCount;
    }

/**
 * Method getBookingMadeReceived to get all the booking made and received records
 *
 *
 * @param $bookingID int the id of the space
 * @return $getChargeSetting array containing the information for charging tax and commission
 */
	function getBookingMadeReceived() {
		$getBookingRecord = $this->find('all',
								array(
									'conditions' => array(
										'OR' => array(
												'Booking.user_id' => CakeSession::read('Auth.User.id'),
												'Booking.space_user_id' => CakeSession::read('Auth.User.id')
											)
									),
									'fields' => array(
										'id',
										'user_id',
										'space_user_id',
										'start_date',
										'end_date',
										'booking_price',
										'booking_type',
										'pre_start_cancellation_deadline',
										'post_start_cancellation_deadline',
                                                                                'num_slots',
                                                                                'qrcodefilename',
                                                                                'refundable_deposit',
                                        Configure::read('BookingTableStatusField'),
                                        '(
                                        	IFNULL(Booking.owner_income,0) + 
                                        	IFNULL(Booking.owner_service_tax_amount,0) + 
                                        	IFNULL(Booking.simplypark_income,0) + 
                                        	IFNULL(Booking.simplypark_service_tax_amount,0)
                                        ) as booking_made_amount',
										'(
											IFNULL(Booking.owner_income,0) + 
											IFNULL(Booking.owner_service_tax_amount,0)
										) as booking_received_amount',
										'(
											IFNULL(Booking.booking_price,0) + 
											IFNULL(Booking.refundable_deposit,0)
										) as amount_to_capture'
									),
									'order' => 'Booking.created DESC',
									'contain' => array(
										'Space' => array(
											'fields' => array(
												'name'
												)
											),
										'Transaction' => array(
											'payment_id'
											)
									)
								)
							);

		
		return $getBookingRecord;
	}

/**
 * Method getBookingMadeReceived to get all the booking made and received records
 *
 *
 * @param $bookingID int the id of the space
 * @return $getChargeSetting array containing the information for charging tax and commission
 */
	function getBookingMade($start_date = null, $end_date = null) {
		$conditions = array();

		if (!empty($start_date) && !empty($end_date)) {
			$conditions = array(
								'DATE(Booking.start_date) >= ' => $start_date,
								'DATE(Booking.end_date) <= ' => $end_date
							);
		} elseif (!empty($start_date) && empty($end_date)) {
			$conditions = array(
								'DATE(Booking.start_date) >= ' => $start_date
							);
		} elseif (empty($start_date) && !empty($end_date)) {
			$conditions = array(
								'DATE(Booking.end_date) <= ' => $end_date
							);
		}

		$conditions = array_merge($conditions,array('Booking.user_id' => CakeSession::read('Auth.User.id')));

		$getBookingRecord = $this->find('all',
										array(
												'conditions' => array(
														$conditions
													),
												'fields' => array(
														'id',
														'user_id',
														'space_user_id',
														'start_date',
														'end_date',
														'booking_price',
														Configure::read('BookingTableStatusField'),
														'booking_type',
														'pre_start_cancellation_deadline',
														'post_start_cancellation_deadline',
														'(
				                                        	IFNULL(Booking.owner_income,0) + 
				                                        	IFNULL(Booking.owner_service_tax_amount,0) + 
				                                        	IFNULL(Booking.simplypark_income,0) + 
				                                        	IFNULL(Booking.simplypark_service_tax_amount,0)
				                                        ) as booking_made_amount',
														'(
															IFNULL(Booking.booking_price,0) + 
															IFNULL(Booking.refundable_deposit,0)
														) as amount_to_capture'
													),
												'order' => 'Booking.created DESC',
												'contain' => array(
														'Space' => array(
																'fields' => array(
																		'name'
																	)
															),
														'Transaction' => array(
																'payment_id'
															)
													)
											)
							);
		return $getBookingRecord;
	}

/**
 * Method getBookingMadeReceived to get all the booking made and received records
 *
 *
 * @param $bookingID int the id of the space
 * @return $getChargeSetting array containing the information for charging tax and commission
 */
	function getBookingReceived($start_date = null, $end_date = null) {

		$conditions = array();

		if (!empty($start_date) && !empty($end_date)) {
			$conditions = array(
								'DATE(Booking.start_date) >= ' => $start_date,
								'DATE(Booking.end_date) <= ' => $end_date
							);
		} elseif (!empty($start_date) && empty($end_date)) {
			$conditions = array(
								'DATE(Booking.start_date) >= ' => $start_date
							);
		} elseif (empty($start_date) && !empty($end_date)) {
			$conditions = array(
								'DATE(Booking.end_date) <= ' => $end_date
							);
		}

		$conditions = array_merge($conditions,array('Booking.space_user_id' => CakeSession::read('Auth.User.id')));

		$getBookingRecord = $this->find('all',
										array(
												'conditions' => array(
														$conditions
													),
												'fields' => array(
														'id',
														'user_id',
														'space_user_id',
														'start_date',
														'end_date',
														'booking_price',
														'booking_type',
														'pre_start_cancellation_deadline',
														'post_start_cancellation_deadline',
														Configure::read('BookingTableStatusField'),
														'(
															IFNULL(Booking.owner_income,0) + 
															IFNULL(Booking.owner_service_tax_amount,0)
														) as booking_received_amount',
														'(
															IFNULL(Booking.booking_price,0) + 
															IFNULL(Booking.refundable_deposit,0)
														) as amount_to_capture'
													),
												'order' => 'Booking.created DESC',
												'contain' => array(
														'Space' => array(
																'fields' => array(
																		'name'
																	)
															),
														'Transaction' => array(
																'payment_id'
															)
													)
											)
							);
		return $getBookingRecord;
	}
/**
 * Method getBookingDetailApprovalEmail to get the booking record for sending in the approve booking email
 *
 *
 * @param $bookingID int the id of the booking
 * @return $getBookingRecord array containing the record of the booked space
 */
	function getBookingDetailApprovalEmail($bookingID = null) {
		$getBookingRecord = $this->find('first',
				array(
					'conditions' => array(
						'Booking.id' => $bookingID
						),
					'fields' => array(
						'start_date',
						'end_date',
						Configure::read('BookingTableStatusField'),
						'pre_start_cancellation_deadline',
						'post_start_cancellation_deadline',
						'booking_type',
                                                'booking_price',
                                                'refundable_deposit'
						),
					'contain' => array(
						'Space' => array(
							'fields' => array(
								'name',
								'flat_apartment_number',
								'address1',
								'address2',
								'post_code',
								'property_type_id',
								'booking_confirmation'
								),
							'City' => array(
								'fields' => array(
									'name'
									)
								),
						         'User' => array(
							        'UserProfile' => array(
								        'fields' => array(
								        	'first_name',
								        	'last_name'
								        	)
								        ),
							        'fields' => array(
							        	'email',
							        	'id'
							        	)
							        ),
							),
						'User' => array(
							'UserProfile' => array(
								'fields' => array(
									'first_name',
									'last_name'
									)
								),
							'fields' => array(
								'email',
								'id'
								)
							),
						'SpaceUser' => array(
							'UserProfile' => array(
								'fields' => array(
									'first_name',
									'last_name'
									)
								),
							'fields' => array(
								'email',
								'id'
								)
							),
						'UserCar' => array(
								'fields' => array(
									'registeration_number',
                                                                        'car_type'
									)
								),
						'SpacePark' => array(
									'park_number'
							)
							)
							)
							);
		return $getBookingRecord;
	}

function getBookedRecordsOfSpace($spaceID = null, $startDate = null, $endDate = null) 
{
	$getBookedRecords = $this->find(
			'all',
			array(
				'conditions' => array(
					'Booking.space_id' => $spaceID,
					'OR' => array(
						'Booking.start_date BETWEEN ? AND ?' => array($startDate,$endDate),
						'Booking.end_date BETWEEN ? AND ?' => array($startDate,$endDate),
						array(
							'Booking.start_date <=' => $startDate,
							'Booking.end_date >=' => $endDate
						     )
						),
					'NOT' => array(
						'Booking.'.Configure::read('BookingTableStatusField') => array(
							Configure::read('BookingStatus.Cancelled'),
							Configure::read('BookingStatus.Expired'),
							Configure::read('SaveBookingStatus.DisApproved')
							)
						)
					),
				'fields' => array(
					'space_park_id',
					'num_slots'
					),
				'recursive' => -1
					)
					);
	return $getBookedRecords;
}
	

/**
 * Method newBookings to get all the new booking after last login date of the user
 *
 * @return $newBookings int the count of the new bookings
 */
	function newBookings() {
		$newBookings = $this->find(
				'count',
				array(
					'conditions' => array(
						'Booking.space_user_id' => CakeSession::read('Auth.User.id'),
						'Booking.created >' => CakeSession::read('Auth.User.last_login')
						)
				     )
				);
		return $newBookings;
	}

/**
 * Method getBookingSummary to get all the booking to show in user dashboard page
 *
 * @return $bookingsSummary array containing all the bookings summary.
 */
	function getBookingSummary() {
		$bookingsSummary = $this->find(
				'all',
				array(
					'conditions' => array(
						'OR' => array(
				            'Booking.space_user_id' => CakeSession::read('Auth.User.id'),
				            'Booking.user_id' => CakeSession::read('Auth.User.id')
    					)),
					'fields' => array(
						'id',
						'start_date',
						'booking_price',
						'user_id',
						'end_date',
						'space_user_id',
						'status',
						'(
                        	IFNULL(Booking.owner_income,0) + 
                        	IFNULL(Booking.owner_service_tax_amount,0) + 
                        	IFNULL(Booking.simplypark_income,0) + 
                        	IFNULL(Booking.simplypark_service_tax_amount,0)
                        ) as booking_made_amount',
						'(
                        	IFNULL(Booking.owner_income,0) + 
                        	IFNULL(Booking.owner_service_tax_amount,0)
                        ) as booking_received_amount'
						),
					'limit' => 5,
					'contain' => array(
						'Transaction' => array(
							'payment_id'
							),
						'Space' => array(
							'fields' => array(
								'name'
								)
							)
						)
						)
						);
		return $bookingsSummary;
	}

/**
 * Method getCurrentBookedSpaces to get all the booked space for current date for current user
 *
 * @return $currentlyBookedSpaces array containing records of booked space for current date and current user.
 */
	function getCurrentBookedSpaces() {

		$currentlyBookedSpaces = $this->query(
				"select SpaceDetail.name, SpaceDetail.number_slots, SpaceDetail.is_activated ,count(BookingDetail.id) AS bookings_count, SpaceTimeDay.from_date, SpaceTimeDay.to_date from spaces SpaceDetail
				inner join bookings BookingDetail on SpaceDetail.id = BookingDetail.space_id AND BookingDetail.".Configure::read('BookingTableStatusField')." = ".Configure::read('BookingStatus.Approved')." AND DATE(BookingDetail.start_date) <= CURDATE() AND DATE(BookingDetail.end_date) >= CURDATE()
				left join space_time_days SpaceTimeDay on SpaceDetail.id = SpaceTimeDay.space_id
				where BookingDetail.space_user_id = ".CakeSession::read('Auth.User.id')."
				group by BookingDetail.space_id"
				);
		
		return $currentlyBookedSpaces;
	}

/**
 * Method approvedBookingCount is to get all the approved bookings count
 *
 * @param none
 * @return total count of approved bookings 
 */
	function approvedBookingCount() {
		$bookings = $this->find(
						'count',
						array(
								'conditions' => array(
									'Booking.'.Configure::read('BookingTableStatusField') => Configure::read('BookingStatus.Approved')
									)
						)
					);
		return $bookings;
	}

/**
 * Method getTotalRevenue is to get the total income earned by simply park
 *
 * @param none
 * @return total income amount  
 */

function getTotalRevenue() 
{
	$amount = 0;
	$data = $this->find(
			'all',
			array(
				'fields' => array(
					'simplypark_income',
					'simplypark_income_after_cancellation',
					//'discount_amount',
					Configure::read('BookingTableStatusField'),
					),
				'conditions' => array(

					'Booking.'.Configure::read('BookingTableStatusField') => array(Configure::read('BookingStatus.Approved'),Configure::read('BookingStatus.Cancelled'))

					)
			     ));
	foreach ($data as $key => $value) 
	{
		if ($value['Booking'][Configure::read('BookingTableStatusField')] == Configure::read('BookingStatus.Cancelled') ) {
			$amount += $value['Booking']['simplypark_income_after_cancellation']; //cancelled bookings income

		}else {	
			$amount += $value['Booking']['simplypark_income']; //approved bookings income 
		}
	}


	return $amount;
}
/**
 * Method getTotalServiceTax is to get the total service tax
 *
 * @param none
 * @return total service tax
 */

	function getTotalServiceTax() {
		$amount = 0;
		$data = $this->find(
						'all',
						array(
								'fields' => array(
									'simplypark_service_tax_amount',
									'simplypark_service_tax_amount_after_cancellation',
									Configure::read('BookingTableStatusField')
									),
								'conditions' => array(
										'Booking.'.Configure::read('BookingTableStatusField') => array(Configure::read('BookingStatus.Approved'),Configure::read('BookingStatus.Cancelled'))
									)		

						)
					);
		
		foreach ($data as $key => $value) {
			
			$bookingsIncome = $value['Booking']['simplypark_service_tax_amount']; //approved bookings income 

			if ($value['Booking'][Configure::read('BookingTableStatusField')] == Configure::read('BookingStatus.Cancelled') ) {
			
				$bookingsIncome = $value['Booking']['simplypark_service_tax_amount_after_cancellation']; //cancelled bookings income
			
			}	
			
			$amount += $bookingsIncome; 
		}
		
		
		return $amount;
	}

/**
 * Method ownerIncome is to get the total income earned by owner
 *
 * @param none
 * @return total owner income  
 */

	function ownerIncome() {
		$amount = 0;
		$data = $this->find(
						'all',
						array(
								'fields' => array(
									'owner_income',
									'owner_income_after_cancellation',
									Configure::read('BookingTableStatusField')
									),
								'conditions' => array(
									'Booking.space_user_id' => CakeSession::read('Auth.User.id'),
									'Booking.'.Configure::read('BookingTableStatusField') => array(
														Configure::read('BookingStatus.Approved'),
														Configure::read('BookingStatus.Cancelled'),
														Configure::read('BookingStatus.CancellationRequested')
										)	
									)		

						)
					);
		
		foreach ($data as $key => $value) {
			
			$bookingsIncome = $value['Booking']['owner_income']; //approved bookings income 

			if ($value['Booking'][Configure::read('BookingTableStatusField')] == Configure::read('BookingStatus.Cancelled') || $value['Booking'][Configure::read('BookingTableStatusField')] == Configure::read('BookingStatus.CancellationRequested') ) {
			
				$bookingsIncome = $value['Booking']['owner_income_after_cancellation']; //cancelled bookings income
			
			}	
			
			$amount += $bookingsIncome; 
		}
		
		
		return $amount;
	}

/**
 * Method confirmedAndPaidAmount is to get the total confirmed amount earned by owner
 *
 * @param none
 * @return total confirmed owner income  
 */

	function confirmedAndPaidAmount() {
		$amount = 0;
		$data = $this->find(
						'all',
						array(
								'fields' => array(
									'owner_income',
									'owner_income_after_cancellation',
									Configure::read('BookingTableStatusField')
									),
								'conditions' => array(
									'Booking.space_user_id' => CakeSession::read('Auth.User.id'),
									'Booking.'.Configure::read('BookingTableStatusField') => Configure::read('BookingStatus.Approved'),
									"NOT" => array( 
										"Booking.".Configure::read('BookingTableStatusField') => array(Configure::read('BookingStatus.DisApproved')) 
										),
									'Booking.paid_to_owner' => 1
									
									)		

						)
					);
		
		foreach ($data as $key => $value) {
			
			$bookingsIncome = $value['Booking']['owner_income']; //approved bookings income 

			if ($value['Booking'][Configure::read('BookingTableStatusField')] == Configure::read('BookingStatus.Cancelled') ) {
			
				$bookingsIncome = $value['Booking']['owner_income_after_cancellation']; //cancelled bookings income
			
			}	
			
			$amount += $bookingsIncome; 
		}
		
		
		return $amount;
	}

/**
 * Method ownerIncome is to get the total income earned by owner
 *
 * @param none
 * @return total income amount  
 */

	// function ownerIncome() {
	// 	$currentUserSessionId = CakeSession::read('Auth.User.id');

	// 	$userBookingData = $this->find(
	// 					'all',
	// 					array(
	// 							'fields' => array(
	// 								'owner_income'
	// 								),
	// 							'conditions' => array(
	// 								'Booking.space_user_id' => $currentUserSessionId,
	// 								'Booking.is_approved' => 1
	// 								'Booking.is_canc'
	// 								)		

	// 					)
	// 				);
		
	// }	


/**
 * Method getBookingRecordForCancellation to getting the booking record
 *
 *
 * @param $bookingID int the id of the booking
 * @return $getBookingRecordForCancellation array containing the record of the booked space
 */
	function getBookingRecordForCancellation($bookingID = null) {
		$bookingRecord = $this->find('first',
				array(
					'conditions' => array(
						'Booking.id' => $bookingID
						),
					'fields' => array(
                                                'space_id',
                                                'user_id',
                                                'space_user_id',
						'start_date',
						'end_date',
						'booking_price',
                        Configure::read('BookingTableStatusField'),
						'booking_type',
                                                'internet_handling_charges',
                                                'space_price',
						'pre_start_cancellation_deadline',
						'post_start_cancellation_deadline',
                                                'booking_price',
                                                'service_tax',
                                                'service_tax_by_owner',
                                                'owner_income',
                                                'owner_service_tax_amount',
                                                'simplypark_service_tax_amount',
                                                'simplypark_income',
                                                'service_tax_by_simplypark',
                                                'discount_amount',
                                                'internet_handling_charges',
                                                'refundable_deposit',
                                                'total_installments',
                                                'commission',
                                                'commission_paid_by'
						),
					'contain' => array(
							'Transaction' => array(
									'payment_id'
									),
                                                        'Space' => array(
						                'User' => array(
							              'UserProfile' => array(
								                'fields' => array(
									                      'first_name',
									                      'last_name'
									                       )
								                       ),
							                         'fields' => array(
								                                 'email'
								                      )
							                    ),
                                                                   'City' => array('name'),
                                                                   'State' => array('name'),
						                   'fields' => array(
							                          'name',
							                           'flat_apartment_number',
							                           'address1',
							                           'address2',
							                           'post_code'
							                            )
						                      ),
                                                        'User' => array(
					                        'UserProfile' => array(
						                           'fields' => array(
							                                   'first_name',
							                                   'last_name'
							                                   )
						                       ),
					                          'fields' => array(
						                                 'email'
						                           )
				                                     ),
			                                'UserCar' => array(
					                                 'registeration_number',
                                                                         'car_type'
					                              ),

							  )
					    )
						);
		return $bookingRecord;
	}


/**
 * Method getBookingRecordForRejection to getting the booking record
 *
 *
 * @param $bookingID int the id of the booking
 * @return $getBookingRecordForCancellation array containing the record of the booked space
 */
function getBookingRecordForRejection($bookingID = null) 
{
		$bookingRecord = $this->find('first',
				array(
					'conditions' => array(
						'Booking.id' => $bookingID
						),
					'fields' => array(
                                                'space_id',
                                                'user_id',
                                                'space_user_id',
						'start_date',
						'end_date',
						'booking_price',
                                                Configure::read('BookingTableStatusField'),
						'booking_type',
                                                'internet_handling_charges',
                                                'space_price',
                                                'booking_price',
                                                'service_tax',
                                                'service_tax_by_owner',
                                                'owner_income',
                                                'owner_service_tax_amount',
                                                'simplypark_service_tax_amount',
                                                'simplypark_income',
                                                'service_tax_by_simplypark',
                                                'discount_amount',
                                                'internet_handling_charges',
                                                'refundable_deposit',
                                                'total_installments',
                                                'commission',
                                                'commission_paid_by'
						),
					'contain' => array(
							'Transaction' => array(
									'payment_id'
									),
                                                        'Space' => array(
						                'User' => array(
							              'UserProfile' => array(
								                'fields' => array(
									                      'first_name',
									                      'last_name'
									                       )
								                       ),
							                         'fields' => array(
								                                 'email'
								                      )
							                    ),
                                                                   'City' => array('name'),
                                                                   'State' => array('name'),
						                   'fields' => array(
							                          'name',
							                           'flat_apartment_number',
							                           'address1',
							                           'address2',
							                           'post_code'
							                            )
						                      ),
                                                        'User' => array(
					                        'UserProfile' => array(
						                           'fields' => array(
							                                   'first_name',
							                                   'last_name'
							                                   )
						                       ),
					                          'fields' => array(
						                                 'email'
						                           )
				                                     ),
			                                'UserCar' => array(
					                                 'registeration_number',
                                                                         'car_type'
					                              ),

							  )
					    )
						);
		return $bookingRecord;
	}
/**
 * Method updateBookingRecordAfterCancellation to getting the booking record
 *
 *
 * @param $bookingID int the id of the booking
 * @param $totalRefundPaid decimal 
 * @param $refundStatus tinyint 
 * @param $status tinyint 
 * @param $lastDateAfterCancellation dateTime 
 * @param $isDepositRefunded decimal 
 * @param $depositToRefund decimal 
 * @param $ownerIncomeAfterCancellation decimal 
 * @param $ownerServiceTaxAfterCancellation decimal 
 * @param $spIncomeAfterCancellation decimal 
 * @param $spServiceTaxAfterCancellation decimal 
 * @param $cancellationFees decimal 
 * 
 * @return true if update succeeded false otherwise 
 */
function updateBookingRecordAfterCancellation($bookingId,
                                              $totalRefundPaid, $refundStatus, $status,$lastParkingDate,
                                              $isDepositRefunded,$depositToRefund,
                                              $ownerIncomeAfterCancellation, $ownerServiceTaxAfterCancellation,
                                              $spIncomeAfterCancellation, $spServiceTaxAfterCancellation,
                                              $cancellationFees) 
   {
	$this->updateAll(
			array(
					'Booking.total_refund_paid' => $totalRefundPaid,
					'Booking.refund_status' => $refundStatus,
					'Booking.'.Configure::read('BookingTableStatusField') => $status,
					'Booking.last_date_after_cancellation' => "'$lastParkingDate'",
					'Booking.owner_income_after_cancellation' => $ownerIncomeAfterCancellation,
					'Booking.owner_service_tax_amount_after_cancellation' => $ownerServiceTaxAfterCancellation,
					'Booking.simplypark_income_after_cancellation' => $spIncomeAfterCancellation,
					'Booking.simplypark_service_tax_amount_after_cancellation' => $spServiceTaxAfterCancellation,
					'Booking.cancellation_fees' => $cancellationFees,
	                                'Booking.refundable_deposit_after_cancellation' => $depositToRefund,
                                        'Booking.is_deposit_refunded' => $isDepositRefunded
        
			     ),
			array(
					'Booking.id' => $bookingId
				)
		);
  }
/**
 * Method updateBookingRecordsStatus to set the status of bookings which have expired 
 *
 *
 * @param $bookingID Arrays of bookingIds 
 * @param $status 
 * @return true if update succeeded false otherwise 
 * @notes only one status can be set for all bookings
 */
function updateBookingRecords($bookingIdArray, $status)
 {
   $this->updateAll(
         array('Booking.'.Configure::read('BookingTableStatusField') => $status),
         array('Booking.id' => $bookingIdArray)
    );
 }

/**
 * Method getUnderApprovalBookings to get all the bookings which are under approval mode
 *
 * @return $underApprovalBookings array containing the bookings records only
 */
function getUnderApprovalBookings() {
	$underApprovalBookings = $this->find(
			'all',
			array(
				'conditions' => array(
					'Booking.'.Configure::read('BookingTableStatusField') => Configure::read('BookingStatus.Waiting')
					),
				'fields' => array(
					'id',
					'space_user_id',
					'space_id',
					'user_id',
					'start_date',
					'end_date',
                                        'booking_price',
                                        'refundable_deposit',
                                        'owner_income'
					),
				'contain' => array(
					'Space' => array(
						'User' => array(
							'UserProfile' => array(
								'fields' => array(
									'first_name',
									'last_name'
									)
								),
							'fields' => array(
								'email'
								)
							),
						'fields' => array(
							'name',
							'flat_apartment_number',
							'address1',
							'address2',
							'post_code'
							)
						),
			'User' => array(
					'UserProfile' => array(
						'fields' => array(
							'first_name',
							'last_name'
							)
						),
					'fields' => array(
						'email'
						)
				       ),
			'UserCar' => array(
					'registeration_number',
                                        'car_type'
					),
				
                         'Transaction' => array(
                                        'payment_id'
                                        )
                                )
				)
				);
	return $underApprovalBookings;
}


/**
 * Method getNewBookings to get the count of the booking which are made on current date
 *
 * @return $getBookingsCount int total count
*/
	function getNewBookings() {
		$getBookingsCount = $this->find(
									'count',
									array(
											'conditions' => array(
													'DATE(Booking.created)' => date('Y-m-d')
												)
										)
								);
		return $getBookingsCount;
	}

/**
 * Method getExipedBooking to get all the booking which has been expired with 24 hours
 *
 * @return $expiredBookings array containing the records of bookings
 */
	function getExipedBooking() {
		$expiredBookings = $this->find(
								'all',
								array(
										'conditions' => array(
												'DATE(Booking.end_date)' => date('Y-m-d',strtotime('-1 days')),
												'Booking.'.Configure::read('BookingTableStatusField') => Configure::read('BookingStatus.Approved')
											),
										'fields' => array(
												'id',
												'user_id',
												'space_user_id'
											),
										'recursive' => -1
									)
							);
		return $expiredBookings;
	}


/**
 * Method getDueOnSimplyPark to get all the bookings for which the 48 hours has been passed
 * and it calls from the DueONSImplyPark shell file
 *
 * @return $getDues array containing the list of bookign with due payment on simplypark
 */
	function getDueOnSimplyPark() {
		$getDues = $this->find(
						'all',
						array(
								'conditions' => array(
										'DATE(Booking.start_date)' => date('Y-m-d', strtotime('-2 days')),
										'Booking.'.Configure::read('BookingTableStatusField') => Configure::read('Bollean.False')
									),
								'fields' => array(
										'id',
										'(
											IFNULL(booking_price,0) + 
											IFNULL(owner_service_tax_amount,0)
										) as pay_to_owner',
										'start_date',
										'end_date'
									),
								'contain' => array(
										'Space' => array(
												'User' => array(
														'UserProfile' => array(
																'fields' => array(
																		'first_name',
																		'last_name'
																	)
															),
														'fields' => array(
																'email'
															)
													),
												'fields' => array(
														'id',
														'name'
													)
											)
									)
							)
					);
		return $getDues;
	}
/**
 * Method getBookingOwnerDriverId's to get the ID of the owner and driver
 *
 * @param $bookingID the id of the particular bookingId
 * @return $ownerDriverInfo array containing the ID of driver & owner
 */
    public function getBookingOwnerDriverInfo($bookingID = null) {
        $ownerDriverInfo = $this->find('first',
                                        array(
                                                'conditions' => array(
                                                        'Booking.id' => $bookingID
                                                    ), 
                                                 'fields' => array(
                                                                 'user_id',
                                                                 'space_user_id'
                                                     )
                                            )
                    );
        return $ownerDriverInfo;
    }

/**
 * Method checkSpaceBooked to check that is there any ongoiing booking on this space,
 * or is there any pending booking on this space
 *
 * @param @spaceID int the id of the space
 * @return $bookedSpace int the total count of the bookings for a particular space
 */
    public function checkSpaceBooked($spaceID = null) {
    	$bookedSpace = $this->find(
    						'count',
    						array(
    								'conditions' => array(
    										'Booking.space_id' => $spaceID,
    										'Booking.status' => array(
    												Configure::read('BookingStatus.Approved'),
    												Configure::read('BookingStatus.CancellationRequested')
    										)
    								)
    						)
    					);
    	return $bookedSpace;
    }

/**
 * Method validateBooking : This function will validate a booking at entry time
 *                                 
 *      @param space_id
 *      @param- booking_id
 *      @param trans_id
 * @return void
 */

public function validateBooking($spaceId,$bookingId, $transId,&$status,&$bookingInfo)
{
	$bookingInfo = $this->find('first',
			   array(
				'conditions' => array(
					'Booking.id' => $bookingId,
                                        'Booking.space_id' => $spaceId
					), 
				'fields' => array(
					'status',
					'pre_entry_datetime',
                                        'post_entry_datetime',
                                        'booking_type'
					),
                                'contain' => array(
						'UserCar' => array(
								'fields' => array(
									'registeration_number',
                                                                        'car_type'
									)
								),
                                                'Transaction' => array(
                                                                'fields' => array('payment_id')
                                                                  )
                                                 )
			     )
			);

    //CakeLog::write('debug','in validateBooking model ' . print_r($bookingInfo,true));
    if(empty($bookingInfo))
    {
       $status = Configure::read('BookingValidationStatus.noSuchBooking');
       return false;
    }
   elseif($bookingInfo['Booking']['status'] != Configure::read('BookingStatus.Approved')) 
    {
        if($bookingInfo['Booking']['status'] == Configure::read('BookingStatus.Waiting')) 
         {
            $status = Configure::read('BookingValidationStatus.bookingWaitingForApproval');
         }
       elseif($bookingInfo['Booking']['status'] == Configure::read('BookingStatus.DisApproved')) 
        {
           $status = Configure::read('BookingValidationStatus.bookingDisapproved');
        }
       elseif($bookingInfo['Booking']['status'] == Configure::read('BookingStatus.CancellationRequested') || 
              $bookingInfo['Booking']['status'] == Configure::read('BookingStatus.Cancelled') ||
              $bookingInfo['Booking']['status'] == Configure::read('BookingStatus.CancelledThroughPaymentDefault')) 
        {
           $status = Configure::read('BookingValidationStatus.bookingCancelled');
        }
       else if($bookingInfo['Booking']['status'] == Configure::read('BookingStatus.Expired'))
        { 
          $status = Configure::read('BookingValidationStatus.bookingExpired');
        }
       return true;
    }

    //see if the payment id matches
    $storedTransId = str_replace('pay_','SP_',$bookingInfo['Transaction']['payment_id']);

    if($storedTransId != $transId) {
       $status = Configure::read('BookingValidationStatus.noSuchBooking');
       return false;
    } 


    //Calculate if the user has arrived too early or too late
    $currentDateTimeObj  = new DateTime();
    $preEntryDateTimeObj = new DateTime($bookingInfo['Booking']['pre_entry_datetime']);

    if($currentDateTimeObj < $preEntryDateTimeObj){
      $status = Configure::read('BookingValidationStatus.tooEarly');
      return true;
    }

    $postEntryDateTimeObj = new DateTime($bookingInfo['Booking']['post_entry_datetime']);

    if($currentDateTimeObj >= $postEntryDateTimeObj) {
      $status = Configure::read('BookingValidationStatus.tooLate');
      return true;
    }
   
    return true;
}


/**
 * Method validateReEntry : This function will validate a Long booking Re-Entry time
 *                                 
 *      @param space_id
 *      @param- booking_id
 *      @param trans_id
 * @return void
 */

public function validateReEntry($spaceId,$bookingId, $transId,&$status,&$bookingInfo)
{
	$bookingInfo = $this->find('first',
			   array(
				'conditions' => array(
					'Booking.id' => $bookingId,
                                        'Booking.space_id' => $spaceId
					), 
				'fields' => array(
					'status',
					'pre_entry_datetime',
                                        'post_entry_datetime',
                                        'booking_type'
					),
                                'contain' => array(
						'UserCar' => array(
								'fields' => array(
									'registeration_number',
                                                                        'car_type'
									)
								),
                                                'Transaction' => array(
                                                                'fields' => array('payment_id')
                                                                  )
                                                 )
			     )
			);

    //CakeLog::write('debug','in validateBooking model ' . print_r($bookingInfo,true));
    if(empty($bookingInfo))
    {
       $status = Configure::read('BookingValidationStatus.noSuchBooking');
       return false;
    }
   elseif($bookingInfo['Booking']['status'] != Configure::read('BookingStatus.Approved')) 
    {
        if($bookingInfo['Booking']['status'] == Configure::read('BookingStatus.Waiting')) 
         {
            $status = Configure::read('BookingValidationStatus.bookingWaitingForApproval');
         }
       elseif($bookingInfo['Booking']['status'] == Configure::read('BookingStatus.DisApproved')) 
        {
           $status = Configure::read('BookingValidationStatus.bookingDisapproved');
        }
       elseif($bookingInfo['Booking']['status'] == Configure::read('BookingStatus.CancellationRequested') || 
              $bookingInfo['Booking']['status'] == Configure::read('BookingStatus.Cancelled') ||
              $bookingInfo['Booking']['status'] == Configure::read('BookingStatus.CancelledThroughPaymentDefault')) 
        {
           $status = Configure::read('BookingValidationStatus.bookingCancelled');
        }
       else if($bookingInfo['Booking']['status'] == Configure::read('BookingStatus.Expired'))
        { 
          $status = Configure::read('BookingValidationStatus.bookingExpired');
        }
       return true;
    }

    //see if the payment id matches
    $storedTransId = str_replace('pay_','SP_',$bookingInfo['Transaction']['payment_id']);

    if($storedTransId != $transId) {
       $status = Configure::read('BookingValidationStatus.noSuchBooking');
       return false;
    } 


    //Calculate if the user has arrived too early or too late
    $currentDateTimeObj  = new DateTime();
    $preEntryDateTimeObj = new DateTime($bookingInfo['Booking']['pre_entry_datetime']);

    if($currentDateTimeObj < $preEntryDateTimeObj){
      $status = Configure::read('BookingValidationStatus.tooEarly');
      return true;
    }

    $postEntryDateTimeObj = new DateTime($bookingInfo['Booking']['post_entry_datetime']);

    if($currentDateTimeObj >= $postEntryDateTimeObj) {
      $status = Configure::read('BookingValidationStatus.tooLate');
      return true;
    }
   
    return true;
}



/**
 * Method getQualifyingBookingsForParkLive : 
 * This function will select all bookings on which qualifies for applying park live
 *                                 
 * @return void
 */

public function getQualifyingBookingsForParkLive()
{
        /*
	$bookingInfo = $this->find('list',
			   array(
				'conditions' => array(
					'Booking.park_live_apply_algorithm' => 1,
                                        'Booking.park_live_checked' => 0,
                                        'Booking.booking_type' => 1, //hourly bookings
                                        'Booking.start_date BETWEEN '.$start_date.' AND DATE_ADD('.$start_date.', INTERVAL 30 DAY)')));
					) 
                                 )
                             );
        */
}

/**
 * Method getRecordsToPushToClient : 
 * This function will select all bookings with matching booking IDs 
 * and create an array of relevant information to send to the mobile apps
 * 
 *                                 
 * @return an array containing relevant information about the supplied booking ids 
 */

public function getRecordsToPushToClient($idArray)
{
   //CakeLog::write('debug','In booking getRecordsToPushClient..' . print_r($idArray,true));	
   $recordsToPush = $this->find(
	                       'all',
			       array( 
				       'conditions' => array('Booking.id' => $idArray),
				       'fields' => array('serial_num','encode_pass_code',
				                         'first_name','last_name','mobile','email',
							 'booking_type','start_date','end_date',
							 'num_slots','status','space_user_id','created','operator_id',
						 ), 
			              'recursive' => -1			 
				    )
			    );

    //CakeLog::write('debug','getRecords To Push Client...'. print_r($recordsToPush,true));
    return $recordsToPush;
}

/**
 * Method getOpAppRecordsToPushToClient : 
 * This function will select all bookings with matching booking IDs 
 * and create an array of relevant information to send to the mobile apps
 * 
 *                                 
 * @return an array containing relevant information about the supplied booking ids 
 */

public function getOpAppRecordsToPushToClient($idArray)
{
   //CakeLog::write('debug','In booking getOpAppRecordsToPushClient..' . print_r($idArray,true));	
   $recordsToPush = $this->find(
	                       'all',
			       array( 
				       'conditions' => array('Booking.id' => $idArray),
				       'fields' => array('serial_num','encode_pass_code',
				                         'first_name','last_name','mobile','email',
							 'booking_type','start_date','end_date','booking_price','is_pass_for_staff',
							 'num_slots','status','space_user_id','created','operator_id'
						 ),
					'contain' => array('UserCar' => array('fields' => array('registeration_number','car_type')),
					                   'Transaction' => array('fields' => array('is_renewal','parent_transaction_id','payment_id',
					                                                            'payment_mode','invoice_id','invoice_payment_id'))
                                                          ),	 
			                'recursive' => -1			 
				    )
			    );

    //CakeLog::write('debug','getopAppRecords To Push Client...'. print_r($recordsToPush,true));
    return $recordsToPush;
}

/******************************************************************************
 * modifies booking created via the operator App
 *
 ******************************************************************************/
function modifyBooking($spaceId,$bookingId,$newPassCode,$newSerialNumber)
{
   //step 1. find the booking corresponding to booking ID
   $bookingRecord = $this->find(
	                       'first',
			       array( 
				       'conditions' => array('Booking.id' => $bookingId,
                                                             'Booking.space_id' => $spaceId),
                                       'fields' => array('id','serial_num','encode_pass_code'),
                                       'recursive' => -1
                                    )
                                 );

   if(!empty($bookingRecord))
   {
     CakeLog::write('debug','Inside ModifyBooking..' . print_r($bookingRecord,true));
      $existingPassCode = $bookingRecord['Booking']['encode_pass_code'];
      if($existingPassCode == $newPassCode)
      {
         CakeLog::write('debug','State update received to modify Booking');
         CakeLog::write('debug','ExistingPassCode = '. $existingPassCode . ' New PassCode ' . $newPassCode);
         return Configure::read('ReturnTypes.Stale');
      } 
      $bookingRecord['Booking']['encode_pass_code'] = $newPassCode;
      $bookingRecord['Booking']['serial_number']    = $newSerialNumber;
      $bookingRecord['Booking']['modified'] = date('Y-m-d');

     
      if($this->save($bookingRecord)){
         return Configure::read('ReturnTypes.OK');
      }else{
         return Configure::read('ReturnTypes.FailedToSave'); 
      } 
   }else{
     return Configure::read('ReturnTypes.NoSuchRecord'); 
   }   
  
}
}
