<?php
class EventAddress extends AppModel {
	public $belongsTo = array('State', 'City');
    public $hasMany = array('EventTiming');

    public $actsAs = array('Containable');

    public $validate = array(
    'address' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Address is empty.'
            )
        ),
    'post_code' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Post Code is empty.'
            )
        ),
    'lat' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Latitude is empty.'
            )
        ),
    'long' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Longitude is empty.'
            )
        ),
    'start_date' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Start Date not selected.'
            )
        ),
    'end_date' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'End Date not selected.'
            )
        )
	);

/**
 * Method getAddresses to get event addresses according to the filter parametters at event detail page
 *
 * @param $conditions array contains the conditions to be implemented on query
 * @return $eventaddresses array contains the event addresses for a particular event
 */
    function getAddresses($conditions = array()) {
        
        $options['order'] = 'EventAddress.start_date ASC';
        $options['fields'] = array('EventAddress.id','EventAddress.event_id', 'EventAddress.address', 'EventAddress.start_date');
        $options['recursive'] = -1;
        $options['conditions'] = array('EventAddress.event_id' => $conditions['event_id']);
        if (!empty($conditions['state'])) {
            $options['conditions'] = array_merge(
                                        $options['conditions'], array('EventAddress.state_id' => $conditions['state'])
                                    );
        }

        if (!empty($conditions['city'])) {
            $options['conditions'] = array_merge(
                                        $options['conditions'], array('EventAddress.city_id' => $conditions['city'])
                                    );
        }

        if (!empty($conditions['mob']['month'])) {
            $options['conditions'] = array_merge(
                                        $options['conditions'], array('MONTH(EventAddress.start_date)' => $conditions['mob']['month'])
                                    );
        }

        $options['contain'] = array(
                                    'State' => array('name'),
                                    'City' => array('name')
                                );
        $eventaddresses = $this->find('all', $options);
        $formatEventaddresses['EventAddress'] = array_map(array($this, '_formatData'), $eventaddresses);
        return $formatEventaddresses;
    }

    protected function _formatData($data) {
        $result = $data['EventAddress'];
        $result['State'] = $data['State'];
        $result['City'] = $data['City'];
        return $result;
    }

/**
 * Method getEventAndAddressData to get the event address data and event data
 *
 * @param eventAddressID int the id of the event address
 * @return $eventAddressData array contains the array data of event address
 */
    function getEventAndAddressData($eventAddressID = null) {
        $eventAddressData = $this->find(
                                    'first',
                                    array(
                                            'conditions' => array(
                                                    'EventAddress.id' => $eventAddressID
                                                ),
                                            'fields' => array(
                                                    'event_id',
                                                    'address',
                                                    'state_id',
                                                    'city_id'
                                                ),
                                            'contain' => array(
                                                    'State' => array(
                                                            'fields' => array(
                                                                    'name'
                                                                )
                                                        ),
                                                    'City' => array(
                                                            'fields' => array(
                                                                    'name'
                                                                )
                                                        ),
                                                    'Event' => array(
                                                            'fields' => array(
                                                                    'Event.id',
                                                                    'Event.event_name',
                                                                    'Event.event_pic',
                                                                    'Event.description',
                                                                    'Event.tags'
                                                                )
                                                        )
                                                )
                                        )
                            );
        return $eventAddressData;
    }

/**
 * Method getSearchedRecords to get the event venue records according to the search parametter
 *
 * @param $searchParams string value of the searched parametter
 * @return $getEvents array containing the array after search
 */
    function getSearchedEvents($searchParams = null) {
        $conditions = array(
                            'Event.is_activated' => Configure::read('Bollean.True'),
                            'Event.is_deleted' => Configure::read('Bollean.False')
                        );
        $searchData = array(
                'OR' => array(
                        'Event.event_name LIKE' => '%'. $searchParams .'%',
                        'Event.tags LIKE' => '%'. $searchParams .'%',
                        'EventAddress.address LIKE' => '%'. $searchParams .'%',
                        'State.name LIKE' => '%'. $searchParams .'%',
                        'City.name LIKE' => '%'. $searchParams .'%'
                    )
                );
        $conditions = array_merge($conditions, $searchData);

        $options['joins'] = array(
                                array('table' => 'events',
                                        'alias' => 'Event',
                                        'conditions' => array(
                                            'EventAddress.event_id = Event.id'
                                        )
                                    ),
                                array('table' => 'states',
                                        'alias' => 'State',
                                        'conditions' => array(
                                            'State.id = EventAddress.state_id'
                                        )
                                    )
                                ,
                                array('table' => 'cities',
                                        'alias' => 'City',
                                        'conditions' => array(
                                            'City.id = EventAddress.city_id'
                                        )
                                    )
                            );
        $options['order'] = 'Event.created Desc';
        $options['limit'] = 5;
        $options['group'] = 'Event.id';
        $options['recursive'] = -1;
        $options['fields'] = array(
                                'Event.id',
                                'Event.event_name',
                                'EventAddress.id',
                                'EventAddress.address',
                                'State.name',
                                'City.name'
                            );
        $options['conditions'] = $conditions;
        $getEvents = $this->find('all',$options);
        return $getEvents;
    }

/**
 * Method getEventAddress to get the event address details from a event id and state id
 *
 * @param $eventID 
 * @param $stateId
 * @return $Data array contains the array of EventAddress
 */
    function getEventAddress($eventID = null, $stateId = null) {
        $data = $this->find('all',
                                array(
                                    'fields' => array('city_id'),
                                    'conditions' => array(
                                            'EventAddress.event_id' => urldecode(base64_decode($eventID)),
                                            'EventAddress.state_id' => urldecode(base64_decode($stateId))
                                    )
                                )
                );

        return $data;
    }
}