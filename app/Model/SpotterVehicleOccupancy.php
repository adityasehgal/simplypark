<?php
class SpotterVehicleOccupancy extends AppModel {

/******************************************************************************
 *  Function: updateCount
 *  Parameters: $spaceId, $vehicleType,$direction
 *
 *****************************************************************************/

public function updateCount($spaceId, $vehicleType, $direction)
{
   //first get the existing entry
   $existingRecord = $this->find('first',
	                         array('conditions' => array('SpotterVehicleOccupancy.space_id' => $spaceId),
				       'fields' => array('id','num_parked_cars','num_parked_bikes','modified')
			        ));
   $vehicleTypeCar  = false;
   $vehicleTypeBike = false;
   $id = -1;

   if($vehicleType == Configure::read('AppVehicleType.Car')){
      $vehicleTypeCar = true;
   }else{
      $vehicleTypeBike = true;
   }

   if(empty($existingRecord)){
      $record = array();
      $record['SpotterVehicleOccupancy']['space_id']         = $spaceId;    
      $record['SpotterVehicleOccupancy']['num_parked_cars']  = $vehicleTypeCar?1:0;    
      $record['SpotterVehicleOccupancy']['num_parked_bikes'] = $vehicleTypeBike?1:0;    
      $record['SpotterVehicleOccupancy']['created']          = date("Y-m-d H:i");    
      $record['SpotterVehicleOccupancy']['modified']         = date("Y-m-d H:i");    
      if($this->save($record)){
        $id = $this->getLastInsertId();
        CakeLog::write('debug','Saved SpotterVehicleOccupancy, last ID is ' . $id);
      }
   }else{
      $existingRecord['SpotterVehicleOccupancy']['num_parked_cars']  = $vehicleTypeCar?$existingRecord['SpotterVehicleOccupancy']['num_parked_cars'] + 1:
		                                                            $existingRecord['SpotterVehicleOccupancy']['num_parked_cars'] ;    
      $existingRecord['SpotterVehicleOccupancy']['num_parked_bikes'] = $vehicleTypeBike?$existingRecord['SpotterVehicleOccupancy']['num_parked_bikes'] + 1: 
		                                                            $existingRecord['SpotterVehicleOccupancy']['num_parked_bikes'];    
      $existingRecord['SpotterVehicleOccupancy']['modified']         = date("Y-m-d H:i");    

      if($this->save($existingRecord)){
         $id = $existingRecord['SpotterVehicleOccupancy']['id'];
      }
   }

   return $id;

}
}
