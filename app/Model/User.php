<?php
App::uses('AuthComponent', 'Controller/Component');
class User extends AppModel {
	public $name = 'User';
	public $actsAs = array('Containable');
	public $hasOne = array('UserProfile');
    public $hasMany = array('UserAddress', 'UserCar','WithdrawalSources','Space');

	public $validate = array(
            'username' => array(
                    'email' => array(
                            'rule' => array('notEmpty'),
                            'required' => true,
                            'allowEmpty' => false,
                            'message' => 'Username is empty'
                        ),
                    'unique' => array(
                        'rule'    => array('isUniqueUsername'),
                        'message' => 'This username is already in use',
                    )
                ),
            'email' => array(
                    /*'email' => array(
                            'rule'     => 'email',
                            'required' => true,
                            'allowEmpty' => false,
                            'message' => 'Invalid Email'
                        ),*/
                    'unique' => array(
                        'rule'    => array('isUniqueEmail'),
                        'message' => 'This email is already in use',
                    )
                ),
            'old_password' => array(  
                    'match' => array(  
                                'rule'          => 'matchOldPassword',  
                                'required'      => true,  
                                'allowEmpty'    => false,  
                                'message'       => 'Old Password does not match'
                        )
                ),
            'password' => array(
                    'password' => array(
                            'rule' => array('notEmpty'),
                            'required' => true,
                            'allowEmpty' => false,
                            'message' => 'Password is empty'
                        )
                ),
            'confirm_password' => array(
                'length' => array(
                    'rule'      => array('between', 6, 40),
                    'message'   => 'Your password must be between 8 and 40 characters.',
                ),
                'compare'    => array(
                    'rule'      => array('validate_passwords'),
                    'message' => 'Confirm password does not match.',
                )
            )
        );

/**
 * Before Save
 * @param array $options
 * @return boolean
 */
    public function beforeSave($options = array()) {
        // hash our password
        if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
        return true;
    }

/**
 * Method validate_passwords compare new and confirm passwords
 *
 * @return boolean
 */
    public function validate_passwords() {
        return $this->data[$this->alias]['password'] === $this->data[$this->alias]['confirm_password'];
    }

/**
 * Method matchOldPassword to check old password validation
 * @param array $options
 * @return boolean
 */
    function matchOldPassword($check) {
        
        $oldPassword = AuthComponent::password($check['old_password']);
        $password = $this->find(
            'first',
            array(
                'fields' => array(
                    'User.id'
                ),
                'conditions' => array(
                    'User.password' => $oldPassword,
                    'User.id' => $this->data[$this->alias]['id']
                )
            )
        );
        if(!empty($password)){
            if($this->data[$this->alias]['id'] == $password['User']['id']){
                return true; 
            }else{
                return false; 
            }
        }else{
            return false; 
        }
    }

/**
 * Before isUniqueEmail
 * @param array $options
 * @return boolean
 */
    function isUniqueEmail($check) {
 
        $email = $this->find(
            'first',
            array(
                'fields' => array(
                    'User.id'
                ),
                'conditions' => array(
                    'User.email' => $check['email']
                )
            )
        );
 
        if(!empty($email)){
            if(isset($this->data[$this->alias]['id']) && $this->data[$this->alias]['id'] == $email['User']['id']){
                return true; 
            }else{
                return false; 
            }
        }else{
            return true; 
        }
    }

/**
 * Before isUniqueUsername
 * @param array $options
 * @return boolean
 */
    function isUniqueUsername($check) {
 
        $username = $this->find(
            'first',
            array(
                'fields' => array(
                    'User.id'
                ),
                'conditions' => array(
                    'User.username' => $check['username']
                )
            )
        );
 
        if(!empty($username)){
            if(isset($this->data[$this->alias]['id']) && $this->data[$this->alias]['id'] == $username['User']['id']){
                return true; 
            }else{
                return false; 
            }
        }else{
            return true; 
        }
    }

    public function saveUserData($userData) {
        $data = array(
            'User' => array(
                    'user_type_id' => $userData['user_type_id'],
                    'username' => $userData['username'],
                    'email' => $userData['email'],
                    'password' => $userData['password'],
                    'activation_key' => $userData['activation_key'],
                    'persist_code' => $userData['persist_code'],
                ),
            'UserProfile' => array(
                'first_name' => $userData['first_name'],
                'last_name' => $userData['last_name'],
            ),
        );
        $this->saveAssociated();
    }

/**
 * Method activeUserCount is to get all the active users count
 *
 * @param none
 * @return total count of active users 
 */
    function activeUserCount() {
        $activeUsers = $this->find('count',array(
                                'conditions' => array(
                                        'User.user_type_id' => configure::read('UserTypes.User'),
                                        'User.is_activated' => 1,
                                        'User.is_deleted' => 0
                                    ),
                                'recursive' => -1
                                
                    ));
        
        return $activeUsers;
    }
    
    public function activateAccount($activation_key) {
        $res = $this->find('first',
                 array(
                    'fields' => array(
                        'User.id'
                    ),
                    'conditions' => array(
                        'User.activation_key' => $activation_key
                    )
                )
            );
        
        if(empty($res)) {
            return false;
        }

        $data = array(
            'is_activated' => 1
            );

        
        if($this->updateAll(
            array(
                'User.is_activated' => configure::read('Activate.True'),
                'User.activation_key' => null
                ),
            array(
                'User.id' => $res['User']['id']
                )
        )) {
            return true;
        }
        return false;
    }

/**
 * Method getUserDataToEdit used to get user data from table while updating profile
 * common function to call from update profile page and dashboard summary page to calculate profile completion percentage
 *
 * @param $userID int the id of the user, of whom the data should be fetch
 * @return $getUser array containing the user data
 */
    function getUserDataToEdit($userID = null) {

        $conditions = array('User.id' => $userID);

        $getUser = $this->find('first',
                                    array(
                                            'conditions' => $conditions,
                                            'fields' => array(
                                                            'id',
                                                            'username',
                                                            'email',
                                
                                                            ),
                                            'contain' => array(
                                                            'UserProfile' => array(
                                                                                'id',
                                                                                'first_name',
                                                                                'last_name',
                                                                                'mobile',
                                                                                'company',
                                                                                'tan_number',
                                                                                'profile_pic',
                                                                                'is_mobile_verified'
                                                                ),
                                                            'UserCar' => array(
                                                                            'conditions' => array(
                                                                                    'UserCar.is_deleted' => configure::read('Bollean.False')
                                                                                ),
                                                                            'fields' => array(
                                                                                    'id',
                                                                                    'registeration_number',
                                                                                    'car_type'
                                                                                )
                                                                ),
                                                            'UserAddress' => array(
                                                                            'id',
                                                                            'type',
                                                                            'flat_apartment_number',
                                                                            'address',
                                                                            'state_id',
                                                                            'city_id',
                                                                            'pincode'
                                                                ),
                                                            'WithdrawalSources'
                                                )
                                        )
                            );
        return $getUser;
    }

/**
 * Method getNewRegisteredUsers to get all the newly registered user who are registered in current date
 * 
 * @return $getNewUsers array containing the information for all the newly registered users
 */
    function getNewRegisteredUsers() {
        $getNewUsers = $this->find(
                            'all',
                            array(
                                    'conditions' => array(
                                            'DATE(User.created)' => date('Y-m-d')
                                        ),
                                    'fields' => array(
                                            'email'
                                        ),
                                    'contain' => array(
                                            'UserProfile' => array(
                                                    'first_name',
                                                    'last_name'
                                                )
                                        )
                                )
                        );
        return $getNewUsers;
    }

/**
 * Method getInactiveUsers to get all users who are inactive
 * 
 * @return $getNewUsers array containing the information for all the inactive users
 */
function getInactiveUsers() 
{
        $getInactiveUsers = $this->find(
                                    'all',
                            array(
                                    'conditions' => array(
                                            'User.is_deleted' => 0,
                                            'User.is_activated' => 0
                                        ),
                                    'fields' => array(
                                            'email',
                                            'activation_key'
                                        ),
                                    'contain' => array(
                                            'UserProfile' => array(
                                                    'first_name',
                                                    'last_name'
                                                )
                                        )
                                )
                        );
        return $getInactiveUsers;
    }
/**
 * Method getUserEmail used to get user Email for a id
 * common function to call from cancellation to get email id 
 *
 * @param $userID int the id of the user, of whom the data should be fetch
 * @return $getUser array containing the user data
 */
    function getUserEmail($userID = null) {

        $conditions = array('User.id' => $userID);

        $getUser = $this->find('first',
                                    array(
                                            'conditions' => $conditions,
                                            'fields' => array(
                                                            'id',
                                                            'username',
                                                            'email'
                                                            )
                                          )
                                 );
         return $getUser;
    }

/**
 * Method getUserDetails used to get user Email for a id
 * common function to call from cancellation to get email id 
 *
 * @param $userID int the id of the user, of whom the data should be fetch
 * @return $getUser array containing the user data
 */

function getUserDetails($userID = null) 
{
        $conditions = array('User.id' => $userID);
	$userDetails = $this->find('first',
			array(
				'conditions' => $conditions,
				'fields' => array(
					'id',
					'email'
					),
				'contain' => array(
					'UserProfile' => array(
						'id',
						'first_name',
						'last_name',
						'mobile'
						)
					)
			     )
			);
	return $userDetails;
  }

/**
 * Method getUserMobileNumber to get the user's mobile number from their email id
 *
 * @param $email string the email id of the user
 * @return $userInfo array containing user info
 */
    function getUserInfo($email = null) {
        $this->contain(
                array(
                        'UserProfile' => array(
                                        'fields' => array(
                                                'first_name',
                                                'last_name',
                                                'mobile'
                                            )
                                    )
                    )
            );
        $userInfo = $this->findByEmail($email,array('id'));
        return $userInfo;
    }
    

/**
 * Method getUserMobileNumber to get the user's mobile number from their email id
 *
 * @param $email string the email id of the user
 * @return $userInfo array containing user info
 */
    function checkForverifiedNumber($mobileNumber = null) {
        $this->contain(
                array(
                        'UserProfile' => array(
                                        'fields' => array(
                                                
                                                'mobile'
                                            )
                                    )
                    )
            );
        $user_data = $this->find('first',array(
                        'conditions' => array(
                            'User.id' => CakeSession::read("Auth.User.id"),
                        ),
                        'fields' => array(
                            'is_verified',
                            'verified_mobile_number'
                            ),
                    )
                );

        if ($user_data['User']['verified_mobile_number'] == $mobileNumber && $user_data['User']['is_verified'] == Configure::read('Bollean.True')) {
          
            return true;
        }
        return false;

    }
        
}
