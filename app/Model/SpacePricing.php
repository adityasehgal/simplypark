<?php
class SpacePricing extends AppModel {
/**
 * Method getSpacePrices to get all the space prices
 *
 * @param $spaceID int the id of the space
 * @return $spacePrices array containing the prices of the space
 */

	function getSpacePrices($spaceID = null) {
		$spacePrices = $this->find(
							'first',
							array(
									'conditions' => array(
											'SpacePricing.space_id' => $spaceID
										),
									'fields' => array(
											'hourly',
											'daily',
											'weekely',
											'monthly',
											'yearly'
										)
								)
						);
		return $spacePrices;
	}
}