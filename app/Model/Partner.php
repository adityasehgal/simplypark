<?php
App::uses('AuthComponent', 'Controller/Component');
class Partner extends AppModel {
	public $actsAs = array(
        'CakeAttachment.Upload' => array(
            'logo' => array(
                'dir' => "{FILES}partners_logo",
                //thumbnails declaration
                // 'thumbsizes' => array(
                //     'main' => array('width' => '100', 'height' => '100', 'name' =>  'partner.{$file}.{$ext}'),
                //     //'preview' => array('width' => 250, 'height' => 250, 'name' => 'preview.{$file}.{$ext}'),
                //     //'thumb' => array('width' => 100, 'height' => 100, 'name' =>  'thumb.{$file}.{$ext}', 'proportional' => false)
                // )
            )
        )
    );
    public $validate = array(
            'name' => array(
                'rule' => array('notEmpty'),
                'required' => true,
                'allowEmpty' => false,
                'message' => 'Name is empty'
                ),
            'logo' => array(
                // 'rule' => array('notEmpty'),
                // 'required' => true,
                'validExtension' => array (
                    'rule' => array('validateFileExtension', array('jpg', 'jpeg', 'png', 'gif')),
                    'message' => 'Invalid file: upload only jpg, png or gif'
                ),
                'maxFileSize' => array(
                    'rule' => array('maxFileSize', 5242880),
                    'message' => 'Invalid file size. Maximum file size is 5mb.'
                ),
            ),
    );
}