<?php
class PropertyType extends AppModel {
	public $name = 'PropertyType';

	public $validate = array(
            'name' => array(
                    'required' => array(
                        'rule' => array('notEmpty'),
                        'message' => 'Property type is empty.'
                    ),
                    'unique' => array(
                        'rule'    => array('isUniqueProperty'),
                        'message' => 'This property type is already added.',
                    )
                )
        );

/**
 * Before isUniqueUsername
 * @param array $options
 * @return boolean
 */
    function isUniqueProperty($check) {
 
        $property = $this->find(
            'first',
            array(
                'fields' => array(
                    'PropertyType.id'
                ),
                'conditions' => array(
                    'PropertyType.name' => $check['name']
                )
            )
        );
 
        if(!empty($property)){
            if(isset($this->data[$this->alias]['id']) && $this->data[$this->alias]['id'] == $property['PropertyType']['id']){
                return true; 
            }else{
                return false; 
            }
        }else{
            return true; 
        }
    }
}