<?php
class LostPass extends AppModel {


/**
 * Method getBookingRecordCongratulationPage to getting the booking record
 *
 *
 * @param $bookingID int the id of the booking
 * @return $getBookingRecord array containing the record of the booked space
 */
function getOpAppRecordsToPushToClient($idsArray) 
{
   $recordsToPush = $this->find('all',
				array(
					'conditions' => array('LostPass.id' => $idsArray),
					'fields' => array( 'operator_id', 'app_generated_booking_id',
						            'old_encoded_pass_code','new_encoded_pass_code',
						            'old_pass_issue_date','new_pass_issue_date',
						            'old_serial_number','new_serial_number',
                                                            'lost_pass_fees','payment_mode'
						        ),
                                        'recursive' => -1
                                     )
                                 );
   return $recordsToPush;
}

function addRecord($spaceId,$operatorId,$bookingId,$oldEncodedPassCode,$newEncodedPassCode,
                   $oldPassIssueDate,$newPassIssueDate,$oldSerialNumber,$newSerialNumber,
                   $lostPassFees,$paymentMode,&$id)
{

  $record = array();
  $record['LostPass']['space_id'] = $spaceId;
  $record['LostPass']['operator_id'] = $operatorId;
  $record['LostPass']['app_generated_booking_id'] = $bookingId;
  $record['LostPass']['old_encoded_pass_code'] = $oldEncodedPassCode;
  $record['LostPass']['new_encoded_pass_code'] = $newEncodedPassCode;
  $record['LostPass']['old_pass_issue_date'] = $oldPassIssueDate;
  $record['LostPass']['new_pass_issue_date'] = $newPassIssueDate;
  $record['LostPass']['old_serial_number'] = $oldSerialNumber;
  $record['LostPass']['new_serial_number'] = $newSerialNumber;
  $record['LostPass']['lost_pass_fees'] = $lostPassFees;
  $record['LostPass']['payment_mode'] = $paymentMode;
  $record['LostPass']['created']  = date('Y-m-d');
  $record['LostPass']['modified'] = date('Y-m-d');

  if($this->save($record)){
     $id = $this->getLastInsertId(); 
     return Configure::read('ReturnTypes.OK');
  }else{
     return Configure::read('ReturnTypes.FailedToSave');
  }


}
 

}
