<?php
class CouponApply extends AppModel {
	
/**
 * Method checkAlreadyApplied to check that user didn't apply the coupon before
 *
 * @param $userID int the id of the user
 * @param $discountCouponID int the id of the discount coupon code
 * @return $checkUserCouponApplied int the count of the records
 */
    function checkAlreadyApplied($userID = null, $discountCouponID = null) {
        $checkUserCouponApplied = $this->find(
                                'count',
                                array(
                                        'conditions' => array(
                                                'CouponApply.user_id' => $userID,
                                                'CouponApply.discount_coupon_id' => $discountCouponID
                                            )
                                    )
                            );
        return $checkUserCouponApplied;
    }

/**
 * Method saveCouponApplied to save coupon applied by which user
 *
 * @param $saveCouponApplied array containing the data to save for coupon apply table
 * @return bool
 */
    function saveCouponApplied($saveCouponApplied = array()) {
        if ($this->save($saveCouponApplied)) {
            return true;
        }
        return false;
    }
}