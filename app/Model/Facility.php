<?php
class Facility extends AppModel {
	public $name = 'Facility';
    public $actsAs = array(
        'CakeAttachment.Upload' => array(
            'icon' => array(
                'dir' => "{FILES}FacilityIcon",
                'deleteMainFile' => true,
                //thumbnails declaration
                'thumbsizes' => array(
                    'main' => array('width' => '24', 'height' => '24', 'name' =>  'icon.{$file}.{$ext}'),
                    //'preview' => array('width' => 250, 'height' => 250, 'name' => 'preview.{$file}.{$ext}'),
                    //'thumb' => array('width' => 100, 'height' => 100, 'name' =>  'thumb.{$file}.{$ext}', 'proportional' => false)
                )
            )
        )
    );
	public $validate = array(
            'name' => array(
                    'required' => array(
                        'rule' => array('notEmpty'),
                        'message' => 'Facility name is empty.'
                    ),
                    'unique' => array(
                        'rule'    => array('isUniqueFacility'),
                        'message' => 'This facility is already added.',
                    )
                ),
            'icon' => array(
                'rule1' => array(
                   'allowEmpty' => true
                ),
                // 'rule' => array('notEmpty'),
                // 'required' => true,
                'validExtension' => array (
                    'rule' => array('validateFileExtension', array('jpg', 'jpeg', 'png', 'gif')),
                    'message' => 'Invalid file: upload only jpg, png or gif'
                ),
                'maxFileSize' => array(
                    'rule' => array('maxFileSize', 5242880),
                    'message' => 'Invalid file size. Maximum file size is 5mb.'
                ),
            ),
        );

/**
 * Before isUniqueUsername
 * @param array $options
 * @return boolean
 */
    function isUniqueFacility($check) {
 
        $facility = $this->find(
            'first',
            array(
                'fields' => array(
                    'Facility.id'
                ),
                'conditions' => array(
                    'Facility.name' => $check['name']
                )
            )
        );
 
        if(!empty($facility)){
            if(isset($this->data[$this->alias]['id']) && $this->data[$this->alias]['id'] == $facility['Facility']['id']){
                return true; 
            }else{
                return false; 
            }
        }else{
            return true; 
        }
    }
}