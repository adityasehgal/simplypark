<?php
class OfflineEntryExitTime extends AppModel {
	public $name = 'OfflineEntryExitTime';
	var $actsAs = array('Containable');
        var $belongsTo = array('Space');





/**
 * Method to log Entry   
 * 
 * @return void 
 */
function logEntry($spaceId,$operatorId,$carReg,$parkingId,$entryTime,$isParkAndRide,$vehicleType=1,$reusableToken,&$incrementSlotInUse) 
{
   CakeLog::write('debug','Inside LogEntry, spaceId = ' . $spaceId . ', parkingID = ' . $parkingId);
   $existingEntryRecord = $this->find(
	                       'first',
			       array( 
				       'conditions' => array('OfflineEntryExitTime.space_id' => $spaceId,
				                             'OfflineEntryExitTime.parking_id' => $parkingId
						     ),
						     'fields' => array( 'id','operator_id','car_reg','parking_id','exit_time','status','parking_fees','reusable_token','created','modified')
				    )
			    );


   if(empty($existingEntryRecord)){
            $incrementSlotInUse = 1;
	   $OfflineEntryExit = array();
	   /* update the Entry_Exit_Times table & current occupancies table */
	   $OfflineEntryExit['OfflineEntryExitTime']['space_id']         = $spaceId;
	   $OfflineEntryExit['OfflineEntryExitTime']['operator_id']      = $operatorId;
	   $OfflineEntryExit['OfflineEntryExitTime']['car_reg']          = $carReg;
	   $OfflineEntryExit['OfflineEntryExitTime']['parking_id']       = $parkingId;
	   $OfflineEntryExit['OfflineEntryExitTime']['entry_time']       = $entryTime; 
	   $OfflineEntryExit['OfflineEntryExitTime']['is_park_and_ride'] = $isParkAndRide; 
	   $OfflineEntryExit['OfflineEntryExitTime']['vehicle_type']     = $vehicleType; 
	   $OfflineEntryExit['OfflineEntryExitTime']['status']           = Configure::read('BookingEntryOrExit.entry'); 
	   $OfflineEntryExit['OfflineEntryExitTime']['reusable_token']   = $reusableToken; 
	   $OfflineEntryExit['OfflineEntryExitTime']['created']          = date("Y-m-d H:i");
	   $OfflineEntryExit['OfflineEntryExitTime']['modified']         = date("Y-m-d H:i");

	   if($this->save($OfflineEntryExit)){
	      //update the sync table and get a sync id
	      $id = $this->getLastInsertId();
	   }else{
	     $id = -1;
	   }
	   CakeLog::write('debug','In LogEntry, last ID is ' . $id);
	   return $id;
   }else{
	   if($existingEntryRecord['OfflineEntryExitTime']['status'] == Configure::read('BookingEntryOrExit.exit')){
		   //consider the scenario, car 1234 enters through app 1 (which is offline),
		   //exits through app 2 (via parkingId). App2 updates the exit
		   //App1 then comes online and pushes its entry update. 
		   //We might need to store the car reg number
	           CakeLog::write('debug','In LogEntry, ENTRY is received after EXIT for parking ID' . $parkingId);
		   CakeLog::write('debug','In LogEntry, Updating the REG number' . $carReg);
	   }else{
		   CakeLog::write('debug','In LogEntry, ENTRY is received for parking ID' . $parkingId);	
		   CakeLog::write('debug','when status is ' . $existingEntryRecord['OfflineEntryExitTime']['status']);
		   CakeLog::write('debug','In LogEntry, Updating the REG number' . $carReg);
	   }

	   $existingEntryRecord['OfflineEntryExitTime']['car_reg'] = $carReg;
	   $existingEntryRecord['OfflineEntryExitTime']['entry_time'] = $entryTime;
	   if($this->save($existingEntryRecord)){
	      $incrementSlotInUse = 0;
	      return $existingEntryRecord['OfflineEntryExitTime']['id'];
	   }else{
	      return -1;
	   }
   }

}




/**
 * Method to take actions at exit time  
 * 
 * @return  
 */
function logExit($spaceId,$operatorId,$parkingId,$entryTime,$exitTime,$parkingFees,$vehicleReg,$vehicleType = 1,$reusableToken,&$decrementSlotInUse) 
{
   CakeLog::write('debug','Inside LogExit, spaceId = ' . $spaceId . ', parkingID = ' . $parkingId);
   $existingRecord = $this->find(
	                       'first',
			       array( 
				       'conditions' => array('OfflineEntryExitTime.space_id' => $spaceId,
				                             'OfflineEntryExitTime.parking_id' => $parkingId
						     ),
						     'fields' => array( 'id','operator_id','car_reg','parking_id','entry_time',
                                                                        'exit_time','status','parking_fees','reusable_token','created','modified')
				    )
			    );


   if(!empty($existingRecord)){
	   CakeLog::write('debug',"In logExit..." . print_r($existingRecord,true));

	   $existingRecord['OfflineEntryExitTime']['status']         = Configure::read('BookingEntryOrExit.exit'); 
	   $existingRecord['OfflineEntryExitTime']['exit_time']      = $exitTime; 
	   $existingRecord['OfflineEntryExitTime']['parking_id']     = $parkingId;
	   $existingRecord['OfflineEntryExitTime']['parking_fees']   = $parkingFees; 
	   $existingRecord['OfflineEntryExitTime']['operator_id']    = $operatorId; 
	   $existingRecord['OfflineEntryExitTime']['vehicle_type']   = $vehicleType;
	   $existingRecord['OfflineEntryExitTime']['reusable_token'] = $reusableToken;
	   $existingRecord['OfflineEntryExitTime']['is_updated']     = 0; 
	   $existingRecord['OfflineEntryExitTime']['modified']       = date("Y-m-d H:i");

	   //vehicleReg is currently sent only in case of prepaid parking. An existing record can only exist if an entry record
	   //was first received by the server. In that case, vehicleReg would already be updated 
           //CakeLog::write('debug','Inside offlineExit...updating existing record '. print_r($existingRecord,true));
           if(!empty($vehicleReg)){
	      $existingRecord['OfflineEntryExitTime']['car_reg'] = $vehicleReg;
           }
           
           if($entryTime != ""){
	      $existingRecord['OfflineEntryExitTime']['entry_time'] = $entryTime;
           }


	   if($this->save($existingRecord)){
		$decrementSlotInUse = 1;
		return $existingRecord['OfflineEntryExitTime']['id'];
	   }else{
		 CakeLog::write('debug','In Offline Log Exit, db save failed');
	         return -1;
	   }
   }else{
	   CakeLog::write('debug',"No such Entry exists for parkingID" . $parkingId);
	   CakeLog::write('debug',"Adding a new exit as entry update might have been missed/still pending at the app");
	   
	   $OfflineExit['OfflineEntryExitTime']['status']         = Configure::read('BookingEntryOrExit.exit'); 
	   $OfflineExit['OfflineEntryExitTime']['entry_time']     = $entryTime; 
	   $OfflineExit['OfflineEntryExitTime']['exit_time']      = $exitTime; 
	   $OfflineExit['OfflineEntryExitTime']['parking_id']     = $parkingId; 
	   $OfflineExit['OfflineEntryExitTime']['parking_fees']   = $parkingFees; 
           $OfflineExit['OfflineEntryExitTime']['vehicle_type']   = $vehicleType; 
           $OfflineExit['OfflineEntryExitTime']['car_reg']        = $vehicleReg; 
	   $OfflineExit['OfflineEntryExitTime']['space_id']       = $spaceId;
	   $OfflineExit['OfflineEntryExitTime']['operator_id']    = $operatorId;
           $OfflineExit['OfflineEntryExitTime']['is_updated']     = 0; 
           $OfflineExit['OfflineEntryExitTime']['reusable_token'] = $reusableToken; 
	   $OfflineExit['OfflineEntryExitTime']['date']           = date("Y-m-d H:i"); 
	   $OfflineExit['OfflineEntryExitTime']['modified']       = date("Y-m-d H:i"); 

           CakeLog::write('debug','Inside offlineExit...saving a new exit record '. print_r($OfflineExit,true));
	   if($this->save($OfflineExit)){
		   //update the sync table and get a sync id
		   $id = $this->getLastInsertId();
		   CakeLog::write('debug','In LogExit, creating a fresh exit entry, last ID is ' . $id);

		   $decrementSlotInUse = 0;
		   return $id;
	   }else{
		 CakeLog::write('debug','In Offline Log Exit, db save failed(New DB entry)');
	         return -1;
	   }
   }

}

/**
 * Method to take cancel a transaction  
 * 
 * @return  
 */
function cancelTransaction($spaceId,$parkingId,$operatorId,$vehicleReg,$vehicleType,$entryTime,$reusableToken,
                           &$vehicleTypeOut) 
{
   $existingRecord = $this->find(
	                       'first',
			       array( 
				       'conditions' => array('OfflineEntryExitTime.space_id' => $spaceId,
				                             'OfflineEntryExitTime.parking_id' => $parkingId
						     ),
						     'fields' => array( 'id','vehicle_type','is_updated','operator_id','car_reg','parking_id','exit_time','status','parking_fees','reusable_token','created','modified')
				    )
			    );


   if(!empty($existingRecord)){
           //entry found.
	   CakeLog::write('debug',"In cancelTransaction..." . print_r($existingRecord,true));

	   $existingRecord['OfflineEntryExitTime']['status']       = Configure::read('BookingEntryOrExit.cancelled'); 
	   $existingRecord['OfflineEntryExitTime']['is_updated']   = 1;
	   $existingRecord['OfflineEntryExitTime']['operator_id']  = $operatorId;
           $vehicleTypeOut = $existingRecord['OfflineEntryExitTime']['vehicle_type'];

           if(empty($existingRecord['OfflineEntryExitTime']['car_reg'] && !empty($vehicleReg)))
           {
	      $existingRecord['OfflineEntryExitTime']['car_reg']  = $vehicleReg;
           }
	   
	   if($this->save($existingRecord)){
		   return $existingRecord['OfflineEntryExitTime']['id'];
	   }else{
		   return -1;
	   }
   }else{
	   CakeLog::write('debug',"No such Transaction Exists for cancellation. ParkingID : ". $parkingId . "SpaceID : ". $spaceId);
           //see if the app has sent optional parameters
	   if(!empty($vehicleReg)){
		   CakeLog::write('debug',"Creating a new transaction");
		   $OfflineEntryExit = array();
		   $OfflineEntryExit['OfflineEntryExitTime']['space_id']         = $spaceId;
		   $OfflineEntryExit['OfflineEntryExitTime']['operator_id']      = $operatorId;
		   $OfflineEntryExit['OfflineEntryExitTime']['car_reg']          = $vehicleReg;
		   $OfflineEntryExit['OfflineEntryExitTime']['parking_id']       = $parkingId;
		   $OfflineEntryExit['OfflineEntryExitTime']['entry_time']       = $entryTime; 
		   $OfflineEntryExit['OfflineEntryExitTime']['is_park_and_ride'] = 0; 
		   $OfflineEntryExit['OfflineEntryExitTime']['vehicle_type']     = $vehicleType; 
		   $OfflineEntryExit['OfflineEntryExitTime']['status']           = Configure::read('BookingEntryOrExit.cancelled'); 
	           $OfflineEntryExit['OfflineEntryExitTime']['is_updated']       = 1;
	           $OfflineEntryExit['OfflineEntryExitTime']['reusable_token']   = $reusableToken;
		   $OfflineEntryExit['OfflineEntryExitTime']['created']          = date("Y-m-d H:i");
		   $OfflineEntryExit['OfflineEntryExitTime']['modified']         = date("Y-m-d H:i");
		   
		   if($this->save($OfflineEntryExit)){
			   //update the sync table and get a sync id
			   $id = $this->getLastInsertId();
			   CakeLog::write('debug','In CancelTransaction, last ID is ' . $id);
			   $vehicleTypeOut = $vehicleType;
			   return $id;
		   }else{
			  CakeLog::write('debug','Inside CancelTransaction. DB write failed');
			   return -1;
		   }
	   }else{
	       return -1;
           }
   }

}

/**
 * Method to take update a transaction  
 * 
 * @return  
 */
function updateTransaction($spaceId,$parkingId,$parkingStatus,$parkingFees,$operatorId,
                           $vehicleReg,$vehicleType,$entryTime,$reusableToken,&$vehicleTypeOut) 
{
   $existingRecord = $this->find(
	                       'first',
			       array( 
				       'conditions' => array('OfflineEntryExitTime.space_id' => $spaceId,
				                             'OfflineEntryExitTime.parking_id' => $parkingId
						     ),
						     'fields' => array( 'id','operator_id','vehicle_type','is_updated','car_reg','parking_id','exit_time','status','parking_fees','reusable_token','created','modified')
				    )
			    );


   if(!empty($existingRecord)){
	   CakeLog::write('debug',"In updateTransaction..." . print_r($existingRecord,true));

           if(empty($existingRecord['OfflineEntryExitTime']['car_reg'] && !empty($vehicleReg)))
           {
	      $existingRecord['OfflineEntryExitTime']['car_reg']  = $vehicleReg;
           }

	   $existingRecord['OfflineEntryExitTime']['status']       = $parkingStatus;
	   $existingRecord['OfflineEntryExitTime']['parking_fees'] = $parkingFees;
	   $existingRecord['OfflineEntryExitTime']['operator_id']  = $operatorId;
	   $existingRecord['OfflineEntryExitTime']['is_updated']   = 1;

	   $vehicleTypeOut = $existingRecord['OfflineEntryExitTime']['vehicle_type'];

	   if($this->save($existingRecord)){
		   return $existingRecord['OfflineEntryExitTime']['id'];
	   }else{
		   return -1;
	   }
   }else{
           CakeLog::write('error',"No such Transaction Exists for updation. ParkingID : ". $parkingId . "SpaceID : ". $spaceId);
           //see if the app has sent optional parameters
	   if(!empty($vehicleReg)){
		   CakeLog::write('debug',"Creating a new transaction");
		   $OfflineEntryExit = array();
		   $OfflineEntryExit['OfflineEntryExitTime']['space_id']         = $spaceId;
		   $OfflineEntryExit['OfflineEntryExitTime']['operator_id']      = $operatorId;
		   $OfflineEntryExit['OfflineEntryExitTime']['car_reg']          = $vehicleReg;
		   $OfflineEntryExit['OfflineEntryExitTime']['parking_id']       = $parkingId;
		   $OfflineEntryExit['OfflineEntryExitTime']['entry_time']       = $entryTime; 
		   $OfflineEntryExit['OfflineEntryExitTime']['is_park_and_ride'] = 0; 
		   $OfflineEntryExit['OfflineEntryExitTime']['vehicle_type']     = $vehicleType; 
		   $OfflineEntryExit['OfflineEntryExitTime']['status']           = $parkingStatus; 
		   $OfflineEntryExit['OfflineEntryExitTime']['parking_fees']     = $parkingFees; 
	           $OfflineEntryExit['OfflineEntryExitTime']['is_updated']       = 1;
	           $OfflineEntryExit['OfflineEntryExitTime']['reusable_token']   = $reusableToken;
		   $OfflineEntryExit['OfflineEntryExitTime']['created']          = date("Y-m-d H:i");
		   $OfflineEntryExit['OfflineEntryExitTime']['modified']         = date("Y-m-d H:i");
		   
		   if($this->save($OfflineEntryExit)){
			   //update the sync table and get a sync id
			   $id = $this->getLastInsertId();
			   CakeLog::write('debug','In UpdateTransaction, last ID is ' . $id);

			   $vehicleTypeOut = $vehicleType;
			   return $id;
		   }else{
			   return -1;
		   }
	   }else{
		   return -1;
	   }
   }

}


/*****
 * Method: getRecordsToPushToClient
 *
 * *****/
function getRecordsToPushToClient($idArray)
{
   //CakeLog::write('debug','In OfflineEntryExitTime getRecordsToPushClient..' . print_r($idArray,true));	
   $recordsToPush = $this->find(
	                       'all',
			       array( 
				       'conditions' => array('OfflineEntryExitTime.id' => $idArray),
				       'fields' => array(  'space_id',
                                                           'is_updated',
                                                           'operator_id',
							   'car_reg',
							   'parking_id',
							   'entry_time',
							   'exit_time',
							   'status',
							   'parking_fees',
							   'reusable_token',
							   'vehicle_type'),
                                       )
			    );

   //CakeLog::write('debug','In OfflineEntryExitTime getRecordsToPushClient..' . print_r($recordsToPush,true));	
   return $recordsToPush;
}

/*****
 * Method: getRecordsToPushToClient
 *
 * *****/
function getLatestRecordsToPushToClient($idArray)
{
   //CakeLog::write('debug','In OfflineEntryExitTime getRecordsToPushClient..' . print_r($idArray,true));	
   $endDate = date("Y-m-d");
   //$startDate = (((new DateTime())->sub(new DateInterval('P11D')))->format("Y-m-d H:i:s")));
   $dateObj = new DateTime();
   $dateObj = $dateObj->sub(new DateInterval('P11D'));
   $startDate = $dateObj->format("Y-m-d");

   $recordsToPush = $this->find(
	                       'all',
			       array( 
				       'conditions' => array(
                                                            'OfflineEntryExitTime.id' => $idArray,
                                                            'OR' => array('OfflineEntryExitTime.status' => 1,
                                                                          'OfflineEntryExitTime.entry_time >= ' => $startDate,
                                                                          'OfflineEntryExitTime.exit_time >= ' => $startDate)
                                                            ),
				       'fields' => array(  'space_id',
                                                                        'is_updated',
                                                                        'operator_id',
									 'car_reg',
									 'parking_id',
									 'entry_time',
									 'exit_time',
									 'status',
									 'parking_fees',
									 'reusable_token',
								         'vehicle_type'),
				    )
			    );

   //CakeLog::write('debug','In OfflineEntryExitTime getRecordsToPushClient..' . print_r($recordsToPush,true));	
   return $recordsToPush;
}

function addUpdateRecord($transaction,$spaceId,$operatorId,&$isStale,&$updateSlotInUse){

	$this->create();
        $updateSlotInUse = 1;
	$isStale = 0;
   //first see if this record exists
	$existingRecord = $this->find('first',
		array('conditions' => array('OfflineEntryExitTime.parking_id' => $transaction['parking_id'],
                                            'OfflineEntryExitTime.space_id' => $spaceId))
                );

	if(empty($existingRecord)){
		CakeLog::write('debug','EmptyRecord for parking ID ' . $transaction['parking_id']);
		$offlineEntryExitRecord['OfflineEntryExitTime']['space_id']         = $spaceId;
		$offlineEntryExitRecord['OfflineEntryExitTime']['operator_id']       = $operatorId;
		$offlineEntryExitRecord['OfflineEntryExitTime']['is_park_and_ride'] = $transaction['is_park_and_ride'];
		$offlineEntryExitRecord['OfflineEntryExitTime']['parking_id']       = $transaction['parking_id'];
		$offlineEntryExitRecord['OfflineEntryExitTime']['car_reg']          = $transaction['car_reg'];
		$offlineEntryExitRecord['OfflineEntryExitTime']['entry_time']       = $transaction['entry_time'];
		$offlineEntryExitRecord['OfflineEntryExitTime']['vehicle_type']     = $transaction['vehicle_type'];
		$offlineEntryExitRecord['OfflineEntryExitTime']['status']           = $transaction['status'];
		$offlineEntryExitRecord['OfflineEntryExitTime']['reusable_token']   = $transaction['reusable_token'];
                $offlineEntryExitRecord['OfflineEntryExitTime']['created']          = date("Y-m-d H:i");
                $offlineEntryExitRecord['OfflineEntryExitTime']['modified']         = date("Y-m-d H:i");
		
		if($offlineEntryExitRecord['OfflineEntryExitTime']['status'] == 2){
		   $offlineEntryExitRecord['OfflineEntryExitTime']['exit_time']  = $transaction['exit_time'];
		   $offlineEntryExitRecord['OfflineEntryExitTime']['parking_fees']  = $transaction['parking_fees'];
                   $updateSlotInUse = 0; //direct exit received. no need to update slots
		}	
		CakeLog::write('debug','In addUpdate...saving ' . print_r($offlineEntryExitRecord,true));
		if($this->save($offlineEntryExitRecord)){
		   $updateSlotInUse = 1;
		   return $this->getLastInsertId();
		}else{
		   return -1;
		}
	}else{
		CakeLog::write('debug','NON EmptyRecord for parking ID ' . $transaction['parking_id'] . ' id :: ' . $existingRecord['OfflineEntryExitTime']['id']);
		if($existingRecord['OfflineEntryExitTime']['status'] == Configure::read('BookingEntryOrExit.exit')
			&& $transaction['is_updated'] != 1) 
		{
			CakeLog::write('debug','Stale Information returned for ' . $transaction['parking_id']);
			if($existingRecord['OfflineEntryExitTime']['car_reg'] == "" && $transaction['car_reg'] != ""){
			        CakeLog::write('debug','Updating Car Reg Number and entry time');
		                $existingRecord['OfflineEntryExitTime']['entry_time'] = $transaction['entry_time'];
				$existingRecord['OfflineEntryExitTime']['car_reg']    = $transaction['car_reg'];
				$this->save($existingRecord);
		                $updateSlotInUse = 0;
				return $existingRecord['OfflineEntryExitTime']['id']; 
			}
			CakeLog::write('debug','Stale Information returned for ' . $transaction['parking_id']);
			CakeLog::write('debug','Existing status = 2 and update Flag is ' . $transaction['is_updated']);
			$isStale = 1;
			return -1;	
		}else if($existingRecord['OfflineEntryExitTime']['status'] == $transaction['status'] /*&& $transaction['is_updated'] != 1*/){
		       CakeLog::write('debug','Stale Information returned for ' . $transaction['parking_id']);
		       CakeLog::write('debug','Existing status and transaction status is same. Transaction Status =   ' . $transaction['status']);
		       CakeLog::write('debug','Existing status  =   ' . $existingRecord['OfflineEntryExitTime']['status']);
		       $isStale = 1;
		       return -1;	
		}
		$existingRecord['OfflineEntryExitTime']['status']         = $transaction['status'];
		$existingRecord['OfflineEntryExitTime']['exit_time']      = $transaction['exit_time'];
		$existingRecord['OfflineEntryExitTime']['parking_fees']   = $transaction['parking_fees'];
		$existingRecord['OfflineEntryExitTime']['operator_id']    = $operatorId;
		$existingRecord['OfflineEntryExitTime']['reusable_token'] = $transaction['reusable_token'];
		$existingRecord['OfflineEntryExitTime']['modified']       = date("Y-m-d H:i");

                if($this->save($existingRecord)){
		   $updateSlotInUse = 1;
		   return $existingRecord['OfflineEntryExitTime']['id'];
		}else{
		   return -1;
		}
	}
}

function cancelNdmcTransactions(){
	$records = $this->find('all',
		   array('conditions' => array('OfflineEntryExitTime.space_id' => 277,
                                               'OfflineEntryExitTime.status' => 1,
                                                'OfflineEntryExitTime.entry_time <=' => '2017-01-04 02:00'),
                         'limit' => 45,
                         'recursive' => -1)
                );

         return $records;



}

}
