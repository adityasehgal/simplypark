<?php
class SpaceDisapprovalReason extends AppModel {
    
	public $validate = array(
        'reason' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Reason is empty.'
            ),
        )
    );
}