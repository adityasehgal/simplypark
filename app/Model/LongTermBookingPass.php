<?php
class LongTermBookingPass extends AppModel {

/**
 * Method savePassInfo
 * 
 */
function savePassInfo($spaceId,$encodedPassCode,$serialNum,$qrFileName)
{
	$record = array();
	$this->create();
	$record['LongTermBookingPass']['space_id']         = $spaceId;
	$record['LongTermBookingPass']['encode_pass_code'] = $encodedPassCode;
	$record['LongTermBookingPass']['serial_num']       = $serialNum;
	$record['LongTermBookingPass']['qr_file_name']     = $qrFileName;
	$record['LongTermBookingPass']['created']          = date("Y-m-d H:i");
	$record['LongTermBookingPass']['modified']         = date("Y-m-d H:i");

	$this->save($record);

}

/**
 * Method getNextAvailablePass 
 * 
 */
function getNextAvailablePass($spaceId)
{
	$existingRecord = $this->find('first',
                                       array('conditions' => array('LongTermBookingPass.space_id' => $spaceId,
                                                                   'LongTermBookingPass.is_used' => 0),
                                             'fields'     => array('qr_file_name',
                                                                   'serial_num',
                                                                   'encode_pass_code',
                                                                   'id')
                                             )
                                      );

        if(!empty($existingRecord)){
           $existingRecord['LongTermBookingPass']['is_used']  = 1;
           $existingRecord['LongTermBookingPass']['modified'] = date("Y-m-d H:i");
           $this->save($existingRecord);
           return($existingRecord);
        }else{
          //TODO: create a new pass

        } 
        

}

/**
 * Method getPassInfo 
 * 
 */
function getPassInfo($spaceId,$encodedPassCode)
{
   $passInfo = $this->find('first',
                           array('conditions' => array('LongTermBookingPass.encode_pass_code' => $encodedPassCode),
                                 'fields' => array('qr_file_name','serial_num')
                                )
			);


   return $passInfo;
}

/**
 * Method setPassAsUsed 
 * 
 */
function setPassAsUsed($encodedPassCode)
{
   $passInfo = $this->find('first',
                           array('conditions' => array('LongTermBookingPass.encode_pass_code' => $encodedPassCode),
                                 'fields' => array('is_used','id')
                                )
                          );

   if(!empty($passInfo)){
      $passInfo['LongTermBookingPass']['is_used']  = 1; 
      $passInfo['LongTermBookingPass']['modified'] = date("Y-m-d H:i");
      $this->save($passInfo);
   } 

}
    
                             

}
