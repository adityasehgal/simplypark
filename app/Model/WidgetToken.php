<?php
class WidgetToken extends AppModel
{


  /**
   * Method to register a widget 
   * 
   * @return NULL 
   */

   function registerWidget($spaceId, $token,$type = 0,$operatorId=0)
   {
      $widgetRegisterData = array();
      
      $widgetRegisterData['WidgetToken']['space_id'] = $spaceId;
      $widgetRegisterData['WidgetToken']['token'] = $token;
      $widgetRegisterData['WidgetToken']['type'] = $type;
      $widgetRegisterData['WidgetToken']['operator_id'] = $operatorId;
      $widgetRegisterData['WidgetToken']['created'] = date("Y-m-d H:i:s");
      $widgetRegisterData['WidgetToken']['modified'] = date("Y-m-d H:i:s");
      $id = -1;

      if($this->save($widgetRegisterData)){
	      $id = $this->getLastInsertID();
      }

      return $id;
   }
   
   /**
   * Method to deRegister a widget 
   * 
   * @return NULL 
   */

   function deRegisterWidget($token)
   {
      $this->deleteAll(array('WidgetToken.token' => $token),false);	   
   }
   
   /**
   * Method get all installed widgets with the space spaceId 
   * 
   * @return NULL 
   */

   function getWidgetTokens($spaceId,$currentWidgetId)
   { 
	   $tokensList = $this->find('list',
		                     array('conditions' => array('WidgetToken.space_id' => $spaceId,
	                                                         'WidgetToken.id !=' => $currentWidgetId),
				           'fields' => array('token')
				          )
				  ); 
	   return $tokensList;
   }
   
   /**
   * Method get all installed apps with the space spaceId 
   * 
   * @return NULL 
   */

   function getAppTokens($spaceId,$currentAppId)
   { 
	   $tokensList = $this->find('list',
		                     array('conditions' => array('WidgetToken.space_id' => $spaceId,
				                                 'WidgetToken.id !=' => $currentAppId,
			                                         'WidgetToken.type' => 1),
				           'fields' => array('token')
				          )
				  ); 
	   return $tokensList;
   }
   
   /**
   * Method get all installed apps with the space spaceId 
   * 
   * @return NULL 
   */

   function getAllAppTokens($spaceId)
   { 
	   $tokensList = $this->find('list',
		                     array('conditions' => array('WidgetToken.space_id' => $spaceId,
			                                         'WidgetToken.type' => 1),
				           'fields' => array('token')
				          )
				  ); 
	   return $tokensList;
   }
   
   /**
   * Method to refresh Token 
   * 
   * @return NULL 
   */

   function refreshToken($oldToken,$newToken)
   { 
	   $token = $this->find('first',
		                     array('conditions' => array('WidgetToken.token' => $oldToken),
				           'fields' => array('id')
				          )
				  ); 
	   $updateToken = array();

	   if(!empty($token))
	   {
	      $updateToken['WidgetToken']['id'] = $token['WidgetToken']['id'];
	      $updateToken['WidgetToken']['token'] = $newToken;
	      $updateToken['WidgetToken']['modified'] = date("Y-m-d H:i:s");

	      $this->save($updateToken);
	   }

   }
   
   function refreshAppToken($id,$oldToken,$newToken)
   { 
	   //directly overwrite the refresh Token
	   $updateToken = array();

	   $updateToken['WidgetToken']['id'] = $token['WidgetToken']['id'];
	   $updateToken['WidgetToken']['token'] = $newToken;
	   $updateToken['WidgetToken']['modified'] = date("Y-m-d H:i:s");

	   $this->save($updateToken);

   }

}
