<?php
class Space extends AppModel {
    public $belongsTo = array('User', 'SpaceType', 'PropertyType', 'City', 'State', 'CancellationPolicy');
    public $hasMany = array('SpaceFacility','SpacePark','SpacePricing','SpaceTierPricing','ExtraNightCharges','PenaltyCharges');
    public $hasOne = array('SpaceDisapprovalReason', 'SmallCarSpace','SpaceTimeDay','CurrentSlotOccupancy');

    public $actsAs = array(
        'Containable',
        'GeoLocation',
        'CakeAttachment.Upload' => array(
            'place_pic' => array(
                'dir' => "{FILES}SpacePic",
                'deleteMainFile' => true,
                //thumbnails declaration
                'thumbsizes' => array(
                    'main' => array('width' => '640', 'height' => '480', 'name' =>  'place_pic.{$file}.{$ext}'),
                    //'preview' => array('width' => 250, 'height' => 250, 'name' => 'preview.{$file}.{$ext}'),
                    //'thumb' => array('width' => 100, 'height' => 100, 'name' =>  'thumb.{$file}.{$ext}', 'proportional' => false)
                )
            )
        )
    );

	public $validate = array(
        'name' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Space type name is empty.'
            ),
        ),
        'place_pic' => array(
            'rule1' => array(
               'allowEmpty' => true
            ),
            // 'rule' => array('notEmpty'),
            // 'required' => true,
            'validExtension' => array (
                'rule' => array('validateFileExtension', array('jpg', 'jpeg', 'png', 'gif')),
                'message' => 'Invalid file: upload only jpg, png or gif'
            ),
            'maxFileSize' => array(
                'rule' => array('maxFileSize', 5242880),
                'message' => 'Invalid file size. Maximum file size is 5mb.'
            ),
        ),
        'state_id' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'State not selected.'
            ),
        ),
        'address' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Address is empty.'
            ),
        ),
        'post_code' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Postal Code is empty.'
            ),
        ),
        // 'hourly' => array(
        //     'required' => array(
        //         'rule' => array('notEmpty'),
        //         'message' => 'Hourly price not entered.'
        //     ),
        // ),
    );

/**
 * Method getSpaceUser to get the space and its associated user
 *
 * @param $spaceID int the id of the space to fetch
 * @return $getSpaceUser array the space and its user data
 */
    function getSpaceUser($spaceID = null) {
        $getSpaceUser = $this->find(
                            'first',
                            array(
                                    'conditions' => array(
                                            'Space.id' => $spaceID
                                        ),
                                    'contain' => array(
                                            'User' => array(
                                                    'fields' => array(
                                                            'email'
                                                        ),
                                                    'UserProfile' => array(
                                                            'first_name', 'last_name'
                                                        )
                                                ),
                                            'State' => array(
                                                    'fields' => array(
                                                            'name'
                                                        )
                                                ),
                                            'SpaceTimeDay' => array(
                                                    'fields' => array(
                                                            'from_date',
                                                            'to_date'
                                                        )
                                                )
                                        )
                                )
                        );
        return $getSpaceUser;
    }

/**
 * Method getSpacePricingAndOperatingHours to get the space,its pricing and operating hours
 *
 * @param $spaceID int the id of the space to fetch
 * @return $spaceInfo array the space,pricing and operating hours
 */
    function getSpacePricingAndOperatingHours($spaceID = null) {
        $getSpaceUser = $this->find(
                            'first',
                            array(
                                    'conditions' => array(
                                            'Space.id' => $spaceID
                                        ),
                                    'fields' => array(
                                                 'is_deposit_required',
						 'tier_pricing_support',
						 'is_yearly_payment_collected_in_advance'
                                                  ),
                                    'contain' => array(
                                            'SpaceTimeDay' => array(
                                                    'fields' => array(
                                                            'from_date',
                                                            'to_date',
                                                            'open_all_hours'
                                                        )
                                                ),
                                            'SpacePricing' => array(
                                                    'fields' => array(
                                                            'hourly',
                                                            'daily',
                                                            'weekely',
                                                            'monthly',
							    'yearly',
							    'vehicle_type'
                                                        )
                                                )
                                        )
                                )
                        );
        return $getSpaceUser;
    }

/**
 * Method getSpaceOperatingHours to get the space,its pricing and operating hours
 *
 * @param $spaceID int the id of the space to fetch
 * @return $spaceInfo array the space,operating hours
 */
    function getSpaceOperatingHours($spaceID = null) {
        $getSpaceUser = $this->find(
                            'first',
                            array(
                                    'conditions' => array(
                                            'Space.id' => $spaceID
                                        ),
                                    'contain' => array(
                                            'SpaceTimeDay' => array(
                                                    'fields' => array(
                                                            'from_date',
                                                            'to_date',
                                                            'open_all_hours'
                                                        )
                                                )
                                        )
                                )
                        );
        return $getSpaceUser;
    }
/**
 * Method getAllSpaces to get all the spaces 
 *
 * @param none
 * @return $getSpaces array 
 */
    function getAllSpaces() {
        $conditions = array(
                            'Space.is_completed' => Configure::read('Bollean.True'),
                            'Space.is_activated' => Configure::read('Bollean.True'),
                            'Space.is_approved' => Configure::read('Bollean.True'),
                            'Space.is_deleted' => Configure::read('Bollean.False')
                        );
        $getSpaces = $this->find('all',array(
                                    'conditions' => $conditions
			    ));

        return $getSpaces;
    }

    /**
 * Method getCurrentSpaces to get Current the spaces 
 *
 * @param none
 * @return $getSpaces array 
 */
    public function getCurrentSpaces() {
        $this->contain('SpaceTimeDay');
        $getCurrentSpaces = $this->find('all',
                                        array(
                                            'conditions' => array(
                                                    'Space.user_id' => CakeSession::read('Auth.User.id'),
                                                    'Space.is_completed' => Configure::read('Bollean.True'),
                                                    'Space.is_deleted' => Configure::read('Bollean.False')
                                                )
                                ));
        
        return $getCurrentSpaces;
    }
/**
 * Method getEventNearestSpaces to get all the spaces nearest to a particular event
 *
 * @param $lat float latitude of the event address
 * @param $long float longitude of the event address
 * @return $nearestSpaces array containing the nearest space of an event address
 */
    function getEventNearestSpaces($lat = null, $long = null) {
        $nearestSpaces = $this->search(
                                        'all',
                                        array (
                                            'center' => array (
                                                'lat'  => $lat,
                                                'lng' => $long,
                                                'radius'    => Configure::read('NearestSpaceDistance')  // Kmts
                                            ),
                                            'conditions' => array(
                                                    'Space.is_approved' => Configure::read('Bollean.True'),
                                                    'Space.is_completed' => Configure::read('Bollean.True'),
                                                    'Space.is_deleted' => Configure::read('Bollean.False'),
                                                    'Space.is_activated' => Configure::read('Bollean.True'),
                                                    'Space.property_type_id !=' => 2,
                                                    'DATE(SpaceTimeDay.to_date) >=' => date('Y-m-d')
                                                ),
                                            'recursive' => -1,
                                            'contain' => array(
                                                    'SpacePricing' => array(
                                                            'fields' => array(
                                                                    'hourly'
                                                                )
                                                        ),
                                                    'SpaceTimeDay' => array(
                                                            'fields' => array(
                                                                    'id',
                                                                    'from_date',
                                                                    'to_date'
                                                                )
                                                        ),
                                                    'City' => array(
                                                            'fields' => array(
                                                                    'name'
                                                                )
                                                        )
                                                )
                                        )
                                );
        return $nearestSpaces;
    }
/**
 * Method getNearestSpaces to get all the spaces nearest to a particular event
 *
 * @param $lat float latitude of the event address
 * @param $long float longitude of the event address
 * @return $nearestSpaces array containing the nearest space of an event address
 */
    function getNearestSpaces($lat = null, $long = null,$conditions = array(),$distance = null) {
        $nearestSpaces = $this->search(
                                        'all',
                                        array (
                                            'center' => array (
                                                'lat'  => $lat,
                                                'lng' => $long,
                                                'radius'    => $distance  // Kmts
                                            ),
                                            'conditions' => $conditions,
                                            'recursive' => -1,
                                            'contain' => array(
                                                    'SpaceFacility' => array(
                                                             'Facility' => array(
                                                                      'name',
                                                                      'icon'
                                                                 )
                                                      ),
                                                    'SpacePricing' ,
                                                    'SpaceTimeDay' => array(
                                                            'fields' => array(
                                                                    'id',
                                                                    'from_date',
                                                                    'to_date'
                                                                )
                                                        ),
                                                    'City' => array(
                                                            'fields' => array(
                                                                    'name'
                                                                )
                                                        )
                                                )
                                        )
                                );
        //CakeLog::write('debug','IN getNearest spaces model...' . print_r($nearestSpaces,true));
        return $nearestSpaces;
    }

/**
 * Method getSpaceInfo to get the space from space id
 *
 * @param $spaceID the id of the particular space
 * @return $spaceInfo array containing the information for space
 */
    public function getSpaceInfo($spaceID = null) {
        $spaceInfo = $this->find('first',
                                        array(
                                                'conditions' => array(
                                                        'Space.id' => $spaceID
                                                    ),
                                                'fields' => array(
                                                        'id',
                                                        'name',
                                                        'place_pic',
                                                        'flat_apartment_number',
                                                        'address1',
                                                        'address2',
                                                        'post_code',
                                                        'lat',
                                                        'lng',
                                                        'description',
                                                        'number_slots',
                                                        'sign_agreement',
                                                        'cancellation_policy_id',
                                                        'booking_confirmation',
                                                        'admin_description',
                                                        'tower_number',
                                                        'property_type_id',
                                                        'allow_bulk_bookings',
                                                        'tier_pricing_support',
                                                        'tier_desc',
                                                        'tier_formatted_desc',
                                                        'is_available_with_simplypark',
                                                        'is_parking_free',
                                                    ),
                                                'contain' => array(
                                                        'SmallCarSpace' => array(
                                                                'fields' => array(
                                                                        'small_car_slot'
                                                                    )
                                                            ),
                                                        'SpacePricing' => array(
                                                                'fields' => array(
                                                                        'id',
                                                                        'hourly',
                                                                        'daily',
                                                                        'weekely',
                                                                        'monthly',
									'yearly',
									'vehicle_type'
                                                                    )
                                                            ),
                                                        'SpaceFacility' => array(
                                                                'Facility' => array(
                                                                        'name',
                                                                    )
                                                            ),
                                                        'SpaceTimeDay' => array(
                                                                'fields' => array(
                                                                        'mon',
                                                                        'tue',
                                                                        'wed',
                                                                        'thu',
                                                                        'fri',
                                                                        'sat',
                                                                        'sun',
                                                                        'open_all_hours',
                                                                        'from_date',
                                                                        'to_date'
                                                                    )
                                                            ),
                                                        'CancellationPolicy' => array(
                                                                'policy'
                                                            ),
                                                        'PropertyType' => array(
                                                                'id'
                                                            ),
                                                        'User' => array(
                                                                'UserProfile' => array(
                                                                        'fields' => array(
                                                                                'mobile'
                                                                            )
                                                                    )
                                                            )
                                                    )
                                            )
                    );
        return $spaceInfo;
    }

/**
 * Method getSearchedSpaces to get the spaces records according to the search parametter
 *
 * @param $searchParams string value of the searched parametter
 * @return $getSpaces array containing the array after search
 */
    function getSpaces($searchParams = null) {
        $data = array();
        $gatedData = array();

        $conditions = array(
                            'Space.is_completed' => Configure::read('Bollean.True'),
                            'Space.is_activated' => Configure::read('Bollean.True'),
                            'Space.is_approved' => Configure::read('Bollean.True'),
                            'Space.is_deleted' => Configure::read('Bollean.False')
                        );

        /*if (CakeSession::check('Auth.User')) {
            $conditions = array_merge($conditions, array('Space.user_id <>' => CakeSession::read('Auth.User.id')));
        }*/

        $searchData = array(
                'OR' => array(
                    
                    'Space.name LIKE' => '%'. $searchParams .'%',
                    'Space.search_string' => $searchParams,
                    'Space.flat_apartment_number LIKE' => '%'. $searchParams .'%',
                    'Space.address1 LIKE' => '%'. $searchParams .'%',
                    'Space.address2 LIKE' => '%'. $searchParams .'%',
                    'Space.search_tags LIKE' => '%'. $searchParams .'%',
                   // 'Space.search_string' => $searchParams,
                    'SpaceType.name LIKE' => '%'. $searchParams .'%',
                    'PropertyType.name LIKE' => '%'. $searchParams .'%',
                    'State.name LIKE' => '%'. $searchParams .'%',
                    'City.name LIKE' => '%'. $searchParams .'%'
                    )
                );
        $conditions = array_merge($conditions, $searchData);

        $options['joins'] = array(
                                array('table' => 'states',
                                        'alias' => 'State',
                                        'conditions' => array(
                                            'State.id = Space.state_id'
                                        )
                                    ),
                                array('table' => 'space_types',
                                        'alias' => 'SpaceType',
                                        'type' => 'left',
                                        'conditions' => array(
                                            'SpaceType.id = Space.space_type_id'
                                        )
                                    ),
                                array('table' => 'property_types',
                                        'alias' => 'PropertyType',
                                        'type' => 'left',
                                        'conditions' => array(
                                            'PropertyType.id = Space.property_type_id'
                                        )
                                    ),
                                array('table' => 'cities',
                                        'alias' => 'City',
                                        'type' => 'left',
                                        'conditions' => array(
                                            'City.id = Space.city_id'
                                        )
                                    ),
                                
                            );
        $options['order'] = 'Space.created Desc';

        $options['group'] = 'Space.id';
        $options['recursive'] = -1;
        $options['contain'] = 'SpacePricing';
        $options['fields'] = array(
                                'Space.id',
                                'Space.name',
                                'Space.search_string',
                                'Space.flat_apartment_number',
                                'Space.address1',
                                'Space.address2',
                                'SpaceType.name',
                                'PropertyType.name',
                                'Space.property_type_id',
                                'PropertyType.id',
                                'State.name',
                                'City.name',
                                'Space.number_slots',
                                'Space.lat',
                                'Space.lng'
                                /*'SpacePricing.hourly',
                                'SpacePricing.daily',
                                'SpacePricing.weekely',
                                'SpacePricing.monthly',
				'SpacePricing.yearly'*/
                            );
	$options['conditions'] = $conditions;
        $getSpaces = $this->find('all',$options);
	CakeLog::write('debug','In getSpaces....' . print_r($getSpaces,true));
        $space = array();
        $data = array();
        foreach($getSpaces as $getSpace) { 
            if($getSpace['Space']['property_type_id'] == Configure::read('gated_community')) {
                        //pr($getSpace);
                        $gatedData[] = array(
                                'id' => $getSpace['Space']['id'],
                                'search_string' => $getSpace['Space']['search_string'] 
                            );
                           
            } else {
                $data[] = $getSpace;
            }

        }
        if(!empty($gatedData)) {
            $gatedDataSent = array(
                    'count' => count($gatedData),
                    'search_string' => $searchParams
                );
            $space['GatedSpace']['gatedData'] = $gatedDataSent;
        }
        $new_data = array_merge($space,$data);
        return $new_data;
    }

/**
 * Method getSearchedSpaces to get the spaces records according to the search parametter
 *
 * @param $searchParams string value of the searched parametter
 * @return $getSpaces array containing the array after search
 */
    function getSearchedSpaces($searchParams = null) {
        $data = array();
        $gatedData = array();

        $conditions = array(
                            'Space.is_completed' => Configure::read('Bollean.True'),
                            'Space.is_activated' => Configure::read('Bollean.True'),
                            'Space.is_approved' => Configure::read('Bollean.True'),
                            'Space.is_deleted' => Configure::read('Bollean.False')
                        );

        /*if (CakeSession::check('Auth.User')) {
            $conditions = array_merge($conditions, array('Space.user_id <>' => CakeSession::read('Auth.User.id')));
        }*/

        $searchData = array(
                'OR' => array(
                    'Space.name LIKE' => '%'. $searchParams .'%',
                    'Space.flat_apartment_number LIKE' => '%'. $searchParams .'%',
                    'Space.address1 LIKE' => '%'. $searchParams .'%',
                    'Space.address2 LIKE' => '%'. $searchParams .'%',
                    'Space.search_tags LIKE' => '%'. $searchParams .'%',
                    'Space.search_string LIKE' => '%'. $searchParams .'%',
                    'SpaceType.name LIKE' => '%'. $searchParams .'%',
                    'PropertyType.name LIKE' => '%'. $searchParams .'%',
                    'State.name LIKE' => '%'. $searchParams .'%',
                    'City.name LIKE' => '%'. $searchParams .'%'
                    )
                );
        $conditions = array_merge($conditions, $searchData);

        $options['joins'] = array(
                                array('table' => 'states',
                                        'alias' => 'State',
                                        'conditions' => array(
                                            'State.id = Space.state_id'
                                        )
                                    ),
                                array('table' => 'space_types',
                                        'alias' => 'SpaceType',
                                        'type' => 'left',
                                        'conditions' => array(
                                            'SpaceType.id = Space.space_type_id'
                                        )
                                    ),
                                array('table' => 'property_types',
                                        'alias' => 'PropertyType',
                                        'type' => 'left',
                                        'conditions' => array(
                                            'PropertyType.id = Space.property_type_id'
                                        )
                                    ),
                                array('table' => 'cities',
                                        'alias' => 'City',
                                        'type' => 'left',
                                        'conditions' => array(
                                            'City.id = Space.city_id'
                                        )
                                    ),
                                
                            );
        $options['order'] = 'Space.created Desc';
        $options['limit'] = 5;
        $options['group'] = 'Space.id';
        $options['recursive'] = -1;
        $options['contain'] = 'SpacePricing';
        $options['fields'] = array(
                                'Space.id',
                                'Space.name',
                                'Space.search_string',
                                'Space.flat_apartment_number',
                                'Space.address1',
                                'Space.address2',
                                'SpaceType.name',
                                'PropertyType.name',
                                'PropertyType.id',
                                'State.name',
                                'City.name',
                                'Space.number_slots',
                                'Space.lat',
                                'Space.lng',
                                'SpacePricing.hourly',
                                'SpacePricing.daily',
                                'SpacePricing.weekely',
                                'SpacePricing.monthly',
                                'SpacePricing.yearly'
                            );
        $options['conditions'] = $conditions;
        $getSpaces = $this->find('all',$options);

        foreach($getSpaces as $getSpace) {
            if($getSpace['PropertyType']['id'] == Configure::read('gated_community')) {
                if(strcasecmp($getSpace['Space']['search_string'], $searchParams) == 0) {
                    $gatedData[] = array(
                            'id' => $getSpace['Space']['id'],
                            'search_string' => $getSpace['Space']['search_string'] 
                        );
                    unset($getSpace['Space']);
                    unset($getSpace['SpaceType']);
                    unset($getSpace['PropertyType']);
                    unset($getSpace['State']);
                    unset($getSpace['City']);
                } else {
                    //die('else');
                    unset($getSpace['Space']['flat_apartment_number']);
                    unset($getSpace['Space']['address1']);
                    unset($getSpace['Space']['address2']);
                }
            }
            if(!empty($getSpace)) {
                $data[] = $getSpace;
            }
        }
        if(!empty($gatedData)) {
            $gatedDataSent = array(
                    'count' => count($gatedData),
                    'search_string' => $searchParams
                );
            $data[]['gatedData'] = $gatedDataSent;
        }
        
        return $data;
    }

/**
 * Method bookingSpaceInfo to get space info which is currently being booked
 *
 * @param spaceID int the id of the space
 * @return $spaceInfo array containing the information for the space
 */
    function bookingSpaceInfo($spaceID = null) {
        $spaceInfo = $this->find(
                            'first',
                            array(
                                    'conditions' => array(
                                            'Space.id' => $spaceID
                                        ),
                                    'fields' => array(
                                            'id',
                                            'name',
                                            'place_pic',
                                            'flat_apartment_number',
                                            'address1',
                                            'address2',
                                            'post_code',
                                            'booking_confirmation',
                                            'is_deposit_required',
                                            'deposit_for_months',
                                            'tower_number',
                                            'property_type_id'
                                        ),
                                    'contain' => array(
                                            'User' => array(
                                                    'UserCar' => array(
                                                            'registeration_number'
                                                        ),
                                                    'fields' => array(
                                                            'id',
                                                            'email'
                                                        )
                                                ),
                                            'City' => array(
                                                    'name'
                                                )
                                        )
                                )
                        );
        return $spaceInfo;
    }

/**
 * Method confirmBookingSpaceInfo to get space info for sending in confirmation booking email
 *
 * @param spaceID int the id of the space
 * @return $spaceInfo array containing the information for the space
 */
    function confirmBookingSpaceInfo($spaceID = null) {
        $spaceInfo = $this->find(
                            'first',
                            array(
                                    'conditions' => array(
                                            'Space.id' => $spaceID
                                        ),
                                    'fields' => array(
                                            'id',
                                            'name',
                                            'flat_apartment_number',
                                            'address1',
                                            'address2',
                                            'post_code',
                                            'booking_confirmation',
                                            'property_type_id'
                                        ),
                                    'contain' => array(
                                            'User' => array(
                                                    'fields' => array('email'),
                                                    'UserProfile' => array('first_name','last_name')
                                                   ),
                                            'City' => array(
                                                    'name'
                                                ),
                                            'SpacePark' => array(
                                                    'park_number'
                                                )
                                        )
                                )
                        );
       // pr($spaceInfo);die;
        return $spaceInfo;
    }

/**
 * Method getDepositInfo to get the space deposit information
 *
 * @param $spaceID int the id of the spacea
 * @return $depositInfo array containing the deposit information for the space
 */
    /*function getDepositInfo($spaceID = null) {
        $depositInfo = $this->find(
                            'first',
                            array(
                                    'conditions' => array(
                                            'Space.id' => $spaceID
                                        ),
                                    'fields' => array(
                                            'id',
                                            'is_deposit',
                                            'deposit_for_months'
                                        ),
                                    'recursive' => -1
                                )
                        );
        return $depositInfo;
    }*/

/*
 * Method getAllSlots to get all slots of a particular space
 *
 * @param $spaceID int the id of the space
 * @return $totalSlots int the number of total slots
 */
    function getAllSlots($spaceID = null) {
        $totalSlots = $this->find(
                            'first',
                            array(
                                'conditions' => array(
                                        'Space.id' => $spaceID
                                    ),
                                'fields' => array(
                                        'number_slots'
                                    ),
                                'recursive' => -1
                            )
                        );
        return $totalSlots;
    }

/**
 * Method activeSpaces to get all the active spaces of a user
 *
 * @return $activeParkings int the total number of active parking spaces
 */
    function activeSpaces() {
        $activeParkings = $this->find(
                                'count',
                                array(
                                        'conditions' => array(
                                                'Space.user_id' => CakeSession::read('Auth.User.id'),
                                                'Space.is_approved' => Configure::read('Bollean.True'),
                                                'Space.is_activated' => Configure::read('Bollean.True'),
                                                'SpaceTimeDay.from_date <= ' => date('Y-m-d H:i:s'),
                                                'SpaceTimeDay.to_date >=' => date('Y-m-d H:i:s')
                                            )
                                    )
                            );
        return $activeParkings;
    }

/**
 * Method activeParkingSpaceCount is to get all the active parking spaces count
 *
 * @param none
 * @return total count of active parking space 
 */
    function activeParkingSpaceCount() {    
        $parkingSpaces = $this->find('count',array(
                                'conditions' => array(
                                        'Space.is_activated' => 1,
                                        'Space.is_approved' => 1,
                                        'Space.is_deleted' => 0,
                                        'SpaceTimeDay.from_date <=' => date('Y-m-d H:i:s'),
                                        'SpaceTimeDay.to_date >=' => date('Y-m-d H:i:s')
                                    ), 
                    ));
        return $parkingSpaces;
    }

/**
 * Method getPendingApprovalSpaces to get all the spaces for pending approval disapproval decision by admin
 *
 * @return $getPendingSpaces array containing records of pending spaces
 */
    function getPendingApprovalSpaces() {
        $getPendingSpaces = $this->find(
                                'all',
                                array(
                                        'conditions' => array(
                                                'Space.is_approved' => Configure::read('Bollean.False')
                                            ),
                                        'fields' => array(
                                                'name',
                                                'flat_apartment_number',
                                                'address1',
                                                'address2'
                                            ),
                                        'recursive' => -1
                                    )
                            );
        return $getPendingSpaces;
    }
/**
 * Method getSpaceType to get the space Type  
 *
 * @param $spaceID int the id of the space to fetch
 * @return $spaceType 
 */
function getSpaceType($spaceID = null) 
{
        $spaceType = $this->find(
                            'first',
                            array(
                                    'conditions' => array(
                                            'Space.id' => $spaceID
                                        ),
                                    'fields' => array(
                                                      'space_type_id',
                                                      'property_type_id',
                                                      'number_slots'
                                                     )
                                 )
                              );

       return $spaceType;
}

/**
 * Method getFormattedSpaceAddress to get the formatted space address  
 *
 * @param $spaceID int the id of the space to fetch
 * @return $spaceAddress in a formatted form 
 */
function getFormattedSpaceAddress($spaceID = null) 
{
        $spaceAddress = $this->find(
                            'first',
                            array(
                                    'conditions' => array(
                                            'Space.id' => $spaceID
                                        ),
                                    'fields' => array(
                                                'name',
                                                'flat_apartment_number',
                                                'address1',
                                                'address2',
                                                'post_code' 
                                              ),
                                     'contain' => array(
                                               'State' => array(
                                                            'fields' => array('name')
                                                              ),
                                                'City' => array(
                                                             'fields' => array('name')
                                                              )
                                              )
                                     )
                                  );
      //CakeLog::write('debug','PreFormatted Space Address ' . print_r($spaceAddress,true));

      $formattedSpaceAddress = $spaceAddress['Space']['name'] . '<br />' .
                               $spaceAddress['Space']['flat_apartment_number'] . '<br />' .
                               //$spaceAddress['Space']['address1'] . ' ' . $spaceAddress['Space']['address2'] . '<br />' .
                               $spaceAddress['City']['name'] . ' ' . $spaceAddress['State']['name'] . '<br />' .
                               $spaceAddress['Space']['post_code'];
      //CakeLog::write('debug','Formatted Space Address ' . $formattedSpaceAddress);
      return $formattedSpaceAddress;
}

/**
 * Method isSpaceConfigured   
 *
 * @param $spaceID int the id of the space to check 
 */
function isSpaceConfigured($spaceID)
{
	$spaceId = $this->find('first',
                            array(
                                    'conditions' => array(
                                            'Space.id' => $spaceID
                                        ),
                                    'fields' => array(
		                             'Space.id')
				 ));
	if(empty($spaceId)){
		return false;
	}

	return true;
         	   


}

/**
 * Method admin_activateSpace   
 *
 * @param $spaceID int the id of the space to activate
 */
function admin_changeSpaceState($spaceID,$state)
{
   $spaceArray = array();
   $spaceArray['Space']['id'] = $spaceID;
   $spaceArray['Space']['is_activated'] = $state;

   $this->save($spaceArray);  

}


/**
 * Method getSpaceForPassesGeneration
 *
 * */

function getSpacesForPassesGeneration()
{
   $spaces = $this->find('all',
	                   array(
				   'conditions' => array(
					   'Space.allow_bulk_bookings' => Configure::read('Bollean.True'),
					   'Space.pre_generate_booking_passes' => Configure::read('Bollean.True'),
					   'Space.booking_passes_created' => Configure::read('Bollean.False')
				   ),
				   'fields' => array('id',
				                     'num_passes'),
				   'contain' => array(
					             'User' => array(
							      'fields' => array('id','username','email'),
							      'UserProfile' => array('first_name', 'last_name')
						                   )
						   )
			     )
                        );


   CakeLog::write('debug','Inside getSpaceForPassesGeneration, found space ...' . print_r($spaces));
   return $spaces;


}
 


/******************************************************************************
 *
 *    Operator App Specific functions
 *
 *
 * ****************************************************************************/
function opAppGetSpaceDetails($spaceOwnerId, $spaceId = 0)
{

	$conditions = array();
	if($spaceId != 0)
	{
		$conditions['Space.id'] = $spaceId;
	}
	if($spaceOwnerId!=0){
		$conditions['Space.user_id'] = $spaceOwnerId;
	}
	$conditions['Space.is_approved']  =  Configure::read('Bollean.True');
	$conditions['Space.is_activated'] = Configure::read('Bollean.True');


	$activeSpace = $this->find(
		'first',
		array(
			'conditions' => $conditions,
			'fields' => array('id',
			'tier_pricing_support',
			'name',
			'flat_apartment_number',
			'address1',
			'address2',
			'post_code',
			'number_slots',
			'tier_desc',
			'is_available_with_simplypark',
			'is_parking_free',
			'is_park_and_ride_available',
			'num_bikes_in_single_slot',
			'bike_pricing_ratio',
			'is_space_bike_only',
			'is_multi_day_parking_possible',
			'is_offline_parking_prepaid',
			'num_hours_prepaid',
			'night_charge',
			'is_slot_occupancy_assumed',
			'lost_ticket_charge',
			'apply_custom_ticket_format',
                        'is_cancel_update_allowed',
			'is_pass_activation_required',
			'is_gatewise_reports_required',
			'default_parking',
			'reusable_tokens_used'
		),
		'contain' => array(
			'SpaceTimeDay' => array(
				'fields' => array(
					'from_date',
					'to_date',
					'open_all_hours'
				)
			),
			'SpacePricing' => array(
				'fields' => array(
					'hourly',
					'daily',
					'weekely',
					'monthly',
					'yearly',
					'lost_ticket_charge',
					'staff_pass_discount_percentage',
					'vehicle_type'
				)
			),
			'ExtraNightCharges' => array(
				'fields' => array(
					'vehicle_type',
					'start_hour',
					'end_hour',
					'daily_charge',
					'monthly_charge'
				)
			),
			'PenaltyCharges' => array(
				'fields' => array(
					'vehicle_type',
					'charge_condition',
					'hourly_charge',
				)
			),
			'User'=>array(
				'UserProfile' => array('first_name', 'last_name', 'mobile')
			),
			'CurrentSlotOccupancy' => array(
				'fields' => array('num_slots_in_use','num_bikes_in_single_slot',
				'num_parked_cars','num_parked_bikes')
			)
		)

	)
);

	return $activeSpace;

}

function opAppGetSpaceOperatingHours($spaceOwnerId)
{
   $activeSpace = $this->find(
                                'first',
                                array(
                                        'conditions' => array(
                                                'Space.user_id' => $spaceOwnerId,
					),
					'fields' => array('id',
						         ),
					'contain' => array(
						      'SpaceTimeDay' => array(
                                                            'fields' => array(
                                                                        'from_date',
                                                                        'to_date',
                                                                        'open_all_hours'
                                                                    )
                                                        ),
                                                )

					)
				);
        return $activeSpace;


}

function getBikesToSlotRatio($spaceId)
{
   $bikesToSlotRatio = $this->find(
                                'first',
                                array(
                                        'conditions' => array(
                                                'Space.id' => $spaceId,
					),
					'fields' => array('id',
					                'num_bikes_in_single_slot'
					               )
				     )
			     );
   if(empty($bikesToSlotRatio)){
	   return -1;
   }else{
	   return $bikesToSlotRatio['Space']['num_bikes_in_single_slot'];
   }  

}

function isParkAndRideAvailable($spaceId)
{
   $parkAndRideAvailability = $this->find(
                                'first',
                                array(
                                        'conditions' => array(
                                                'Space.user_id' => $spaceId,
					),
					'fields' => array('id',
					                'is_park_and_ride_available'
					               )
				     )
			     );

   if(!empty($parkAndRideAvailability)){
	   return false;
   }else{
	   return true; 
   }  

}


function getSpaceOwner($spaceId)
{
   $spaceInfo = $this->find('first',
                            array('conditions' => array('Space.id' => $spaceId),
                                  'fields' => array('user_id')
                                 )
                           );

   CakeLog::write('debug','In getSpaceOwnerModel...' . print_r($spaceInfo,true));
   return $spaceInfo;
}

function isSpaceBikeOnly($spaceId)
{
   $spaceInfo = $this->find('first',
                            array('conditions' => array('Space.id' => $spaceId),
                                  'recursive' => -1,
                                  'fields' => array('is_space_bike_only')
                                 )
                           );

   CakeLog::write('debug','In isSpaceBikeOnly...' . print_r($spaceInfo,true));
   return $spaceInfo;


}

function isCancelUpdateEnabled($spaceId)
{
   $isCancelUpdateEnabled = $this->find('first',
                            array('conditions' => array('Space.id' => $spaceId),
                                  'recursive' => -1,
                                  'fields' => array('is_cancel_update_allowed')
                                 )
                           );

   CakeLog::write('debug','In isCancelUpdateEnabled...' . print_r($isCancelUpdateEnabled,true));
   if(!empty($isCancelUpdateEnabled))
   {
      if($isCancelUpdateEnabled['Space']['is_cancel_update_allowed']){
         return true;
      }else{
         return false;
      } 
   }
  
   return false;
}

/**
 * Method activespaces to get all active spaces for a user
 *
 * @param none
 * @return $count array 
 */
function activeSpaceIds($ownerId = 0) 
{
        $conditions = array(
                            'Space.is_completed' => Configure::read('Bollean.True'),
                            'Space.is_activated' => Configure::read('Bollean.True'),
                            'Space.is_approved' => Configure::read('Bollean.True'),
                            'Space.is_deleted' => Configure::read('Bollean.False')
		    );
	if($ownerId != 0){
          $conditions['Space.user_id'] = $ownerId;
	}

        $spaceIdArray = $this->find('list',array(
		                  'conditions' => $conditions,
		                  'fields' => array('id'),
		                  'recursive' => -1,
			    ));

	$spaceIds = array_keys($spaceIdArray);
	//CakeLog::write('debug','In Model ActiveSpaceIds ..' . print_r($spaceIds,true));
	return $spaceIds;
}

/**
 * Method activespaces to get all active spaces for a user
 *
 * @param none
 * @return $count array 
 */
function activeSpaceCount($ownerId = 0) 
{
        $conditions = array(
                            'Space.is_completed' => Configure::read('Bollean.True'),
                            'Space.is_activated' => Configure::read('Bollean.True'),
                            'Space.is_approved' => Configure::read('Bollean.True'),
                            'Space.is_deleted' => Configure::read('Bollean.False')
		    );
	if($ownerId != 0){
          $conditions['Space.user_id'] = $ownerId;
	}

        $count = $this->find('count',array(
                                    'conditions' => $conditions
			    ));

        return $count;
}

/**
 * Method getLocationLatLng to get lat/lngs of all spaces 
 *
 * @param none
 * @return $location array 
 */
function getLocationsLatLng($ownerId = 0) 
{
        $conditions = array(
                            'Space.is_completed' => Configure::read('Bollean.True'),
                            'Space.is_activated' => Configure::read('Bollean.True'),
                            'Space.is_approved' => Configure::read('Bollean.True'),
                            'Space.is_deleted' => Configure::read('Bollean.False')
		    );
	if($ownerId != 0){
          $conditions['Space.user_id'] = $ownerId;
	}
	CakeLog::write('debug','Inside Model getLocationLatLng...' . print_r($conditions,true));

        $locations = $this->find('all',array(
		                   'conditions' => $conditions,
		                   'fields' => array('lat','lng'),
		                   'recursive' => -1,
			   ));

	CakeLog::write('debug','Inside getLocationLatLng...' . print_r($locations,true));
        return $locations;
}

/**
 * Method isAnprAvailable 
 *
 * @param spaceId 
 * @return true if available, false otherwise 
 */
function isAnprAvailable($spaceId) 
{

        $isAnprAvailable = $this->find('first',array(
		                  'conditions' => array('Space.id' => $spaceId),
				  'fields' => array('is_anpr_available'),
				  'recursive' => -1
			  ));

	CakeLog::write('debug','Inside Model Space.php...' . print_r($isAnprAvailable,true));

	if(empty($isAnprAvailable)){
           CakeLog::write('error','Invalid Space ID provided in isAnprAvailable. space Id passed is ' . $spaceId);
	   return false;
	}

	if($isAnprAvailable['Space']['is_anpr_available'] == 1){
	   return true;	
	}else{
	   return false;
        }	   

}

/******************************************************************************
 * Issue 99
 * Function: opAppGetSpaces
 * Description: Get all spaces registered to the particular ownerid
 *
 * ****************************************************************************/
function opAppGetSpaces($userId)
{
	$space_data = $this->find('all',array(
		'conditions' => array(
			'Space.user_id' => $userId,
			'Space.is_deleted' => 0,
			'Space.is_completed' => 1,
			'Space.is_activated' => 1,
			'Space.is_approved' => 1,
		        ),
		'fields' => array(
			'Space.id',
			'Space.name',
			'Space.flat_apartment_number',
			'Space.number_slots',                            
		        )
        	)
       );
	//CakeLog::write('debug','Active Space for this user ' . print_r($space_data,true));
	return $space_data; 
}

}

