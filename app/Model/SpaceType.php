<?php
class SpaceType extends AppModel {
	public $name = 'SpaceType';

	public $validate = array(
            'name' => array(
                    'required' => array(
                        'rule' => array('notEmpty'),
                        'message' => 'Space type name is empty.'
                    ),
                    'unique' => array(
                        'rule'    => array('isUniqueSpace'),
                        'message' => 'This space type is already added.',
                    )
                )
        );

/**
 * Before isUniqueUsername
 * @param array $options
 * @return boolean
 */
    function isUniqueSpace($check) {
 
        $space = $this->find(
            'first',
            array(
                'fields' => array(
                    'SpaceType.id'
                ),
                'conditions' => array(
                    'SpaceType.name' => $check['name']
                )
            )
        );
 
        if(!empty($space)){
            if(isset($this->data[$this->alias]['id']) && $this->data[$this->alias]['id'] == $space['SpaceType']['id']){
                return true; 
            }else{
                return false; 
            }
        }else{
            return true; 
        }
    }
}