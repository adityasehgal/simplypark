<?php
class CurrentSlotOccupancy extends AppModel {


/**
 * Method to increment the used slots by 1 
 * 
 * @return NULL 
 */
function incrementSlotInUse($spaceId,$vehicleType = 1) 
{
	$retValueArray = array();
        $dbResult = $this->find(
                               'first',
                            array(
                                    'conditions' => array(
                                            'CurrentSlotOccupancy.space_id' => $spaceId,
                                        ),
                                    'fields' => array(
                                            'id',
					    'num_slots_in_use',
					    'num_parked_cars',
					    'num_parked_bikes',
					    'num_bikes_in_single_slot'
				    )
                                )
			);

        $toSaveArray = array();
        $toSaveArray['CurrentSlotOccupancy']['space_id']         = $spaceId;
        $toSaveArray['CurrentSlotOccupancy']['date_modified']    =  date("Y-m-d H:i:s");

	if(empty($dbResult)) {
		/*create a new entry */
		$toSaveArray['CurrentSlotOccupancy']['num_slots_in_use'] = 1;
		$toSaveArray['CurrentSlotOccupancy']['date_created'] = date("Y-m-d H:i:s");

		$toSaveArray['CurrentSlotOccupancy']['num_bikes_in_single_slot'] = 
			ClassRegistry::init('Space')->getBikesToSlotRatio($spaceId);

		if($vehicleType == Configure::read('VehicleType.car')){
			$toSaveArray['CurrentSlotOccupancy']['num_parked_cars']  = 1;
			$toSaveArray['CurrentSlotOccupancy']['num_parked_bikes'] = 0;
		}else{
			$toSaveArray['CurrentSlotOccupancy']['num_parked_cars']  = 0;
			$toSaveArray['CurrentSlotOccupancy']['num_parked_bikes'] = 1;
		}

		$retValueArray['slotOccupancy']  =  $toSaveArray['CurrentSlotOccupancy']['num_slots_in_use'];
		$retValueArray['numParkedCars']  =  $toSaveArray['CurrentSlotOccupancy']['num_parked_cars'];
		$retValueArray['numParkedBikes'] =  $toSaveArray['CurrentSlotOccupancy']['num_parked_bikes'];
	}else {
		CakeLog::write('debug','in currentslotoccupancy..(GETSLOT)' . print_r($dbResult,true));
		$toSaveArray['CurrentSlotOccupancy']['id'] = $dbResult['CurrentSlotOccupancy']['id'];

		if($vehicleType == Configure::read('VehicleType.car')){
			$toSaveArray['CurrentSlotOccupancy']['num_slots_in_use'] = 
				$dbResult['CurrentSlotOccupancy']['num_slots_in_use'] + 1;
			$toSaveArray['CurrentSlotOccupancy']['num_parked_cars']  = 
				$dbResult['CurrentSlotOccupancy']['num_parked_cars'] + 1;
		   
			$retValueArray['slotOccupancy']  =  $toSaveArray['CurrentSlotOccupancy']['num_slots_in_use'];
			$retValueArray['numParkedCars']  =  $toSaveArray['CurrentSlotOccupancy']['num_parked_cars'];
		        $retValueArray['numParkedBikes'] =  $dbResult['CurrentSlotOccupancy']['num_parked_bikes'];
		}else{
			$toSaveArray['CurrentSlotOccupancy']['num_parked_bikes']  = 
				$dbResult['CurrentSlotOccupancy']['num_parked_bikes'] + 1;

			if($toSaveArray['CurrentSlotOccupancy']['num_parked_bikes'] % 
				$dbResult['CurrentSlotOccupancy']['num_bikes_in_single_slot'] == 1)
			{
				$toSaveArray['CurrentSlotOccupancy']['num_slots_in_use'] = 
					$dbResult['CurrentSlotOccupancy']['num_slots_in_use'] + 1;
			         
				$retValueArray['slotOccupancy']  =  $toSaveArray['CurrentSlotOccupancy']
					                                           ['num_slots_in_use'];
			}else{
				$retValueArray['slotOccupancy']  =  $dbResult['CurrentSlotOccupancy']
					                                    ['num_slots_in_use'];
			}
			
			$retValueArray['numParkedCars']  =  $dbResult['CurrentSlotOccupancy']['num_parked_cars'];
		        $retValueArray['numParkedBikes'] =  $toSaveArray['CurrentSlotOccupancy']['num_parked_bikes'];
		}


	}
	

	CakeLog::write('debug','In LogEntry...' . print_r($toSaveArray,true));

	$this->save($toSaveArray);

	return $retValueArray;

 }

/**
 * Method to decrement the used slots by 1 
 * 
 * @return NULL 
 */
function decrementSlotInUse($spaceId,$vehicleType = 1) 
{
	$retValueArray = array();
	$retValueArray['slotOccupancy']  =  0;
	$retValueArray['numParkedCars']  =  0;
	$retValueArray['numParkedBikes'] =  0;
	
	$dbResult = $this->find(
                               'first',
                            array(
                                    'conditions' => array(
                                            'CurrentSlotOccupancy.space_id' => $spaceId,
                                        ),
                                    'fields' => array(
                                            'id',
                                            'num_slots_in_use',
					    'num_parked_cars',
					    'num_parked_bikes',
					    'num_bikes_in_single_slot'
                                        ),
                                )
                        );

        $toSaveArray = array();
        $toSaveArray['CurrentSlotOccupancy']['space_id']         = $spaceId;
        $toSaveArray['CurrentSlotOccupancy']['date_modified']    =  date("Y-m-d H:i:s");

        if(empty($dbResult)) {
          CakeLog::write('error','No Entry recorded for Offline User and an EXIT is being recorded');
          return ($retValueArray);
        }else {
           if($dbResult['CurrentSlotOccupancy']['num_slots_in_use'] == 0 ){
             CakeLog::write('error','Slot Usage is already 0. More exit barcodes are scanned than entry');
             return ($retValueArray);
           }
	   
           $toSaveArray['CurrentSlotOccupancy']['id'] = $dbResult['CurrentSlotOccupancy']['id'];
	   
	   if($vehicleType == Configure::read('VehicleType.car')){
		$toSaveArray['CurrentSlotOccupancy']['num_slots_in_use'] = $dbResult['CurrentSlotOccupancy']['num_slots_in_use'] - 1;
                $toSaveArray['CurrentSlotOccupancy']['num_parked_cars'] = $dbResult['CurrentSlotOccupancy']['num_parked_cars'] - 1;
		
		$retValueArray['slotOccupancy']  =  $toSaveArray['CurrentSlotOccupancy']['num_slots_in_use'];
		$retValueArray['numParkedCars']  =  $toSaveArray['CurrentSlotOccupancy']['num_parked_cars'];
		$retValueArray['numParkedBikes'] =  $dbResult['CurrentSlotOccupancy']['num_parked_bikes'];

	   }else{
		   $toSaveArray['CurrentSlotOccupancy']['num_parked_bikes'] = $dbResult['CurrentSlotOccupancy']['num_parked_bikes'] - 1;
		   
		   $retValueArray['numParkedCars']  =  $dbResult['CurrentSlotOccupancy']['num_parked_cars'];
		   $retValueArray['numParkedBikes'] =  $toSaveArray['CurrentSlotOccupancy']['num_parked_bikes'];
		   
		   if($toSaveArray['CurrentSlotOccupancy']['num_parked_bikes'] %
			   $dbResult['CurrentSlotOccupancy']['num_bikes_in_single_slot'] == 0)
		   {
			   $toSaveArray['CurrentSlotOccupancy']['num_slots_in_use'] = 
				   $dbResult['CurrentSlotOccupancy']['num_slots_in_use'] - 1;
			   
			   $retValueArray['slotOccupancy']  =  $toSaveArray['CurrentSlotOccupancy']
				                                              ['num_slots_in_use'];
		   }else{
			   $retValueArray['slotOccupancy']  =  $dbResult['CurrentSlotOccupancy']
				                                       ['num_slots_in_use'];
		   }


           }
	   
	}
	
	$this->save($toSaveArray);

        return ($retValueArray);

 }

/**
 * Method to get the current slot occupancy 
 * 
 * @return NULL 
 */
function getSlotsInUse($spaceId,&$numParkedCars,&$numParkedBikes) 
{
        $getSlotUsage = $this->find(
                               'first',
                              array(
                                    'conditions' => array(
                                            'CurrentSlotOccupancy.space_id' => $spaceId,
                                        ),
                                    'fields' => array(
                                            'id',
					    'num_slots_in_use',
					    'num_parked_cars',
					    'num_parked_bikes'
                                        ),
                                )
                        );

	if(!empty($getSlotUsage)){
	    $numParkedCars  = $getSlotUsage['CurrentSlotOccupancy']['num_parked_cars'];
	    $numParkedBikes = $getSlotUsage['CurrentSlotOccupancy']['num_parked_bikes'];      	
            return $getSlotUsage['CurrentSlotOccupancy']['num_slots_in_use'];
        }else{
            return 0;
        }
        
}

function updateSlotsInUse($spaceId,$updatedCurrentOccupancy,$updatedCarsParked,$updatedBikesParked)
{
   $numParkedCars = 0;
   $numParkedBikes = 0;
   $currentOccupancy = 0;   

    $getSlotUsage = $this->find(
                               'first',
                              array(
                                    'conditions' => array(
                                            'CurrentSlotOccupancy.space_id' => $spaceId,
                                        ),
                                    'fields' => array(
                                            'id',
					    'num_slots_in_use',
					    'num_parked_cars',
					    'num_parked_bikes'
                                        ),
                                )
                        );

	if(!empty($getSlotUsage)){
	    $numParkedCars  = $getSlotUsage['CurrentSlotOccupancy']['num_parked_cars'];
	    $numParkedBikes = $getSlotUsage['CurrentSlotOccupancy']['num_parked_bikes'];      	
            $currentOccupancy = $getSlotUsage['CurrentSlotOccupancy']['num_slots_in_use'];
        }

   $numParkedCars     += $updatedCarsParked;
   $numParkedBikes    += $updatedBikesParked;
   $currentOccupancy  += $updatedCurrentOccupancy;

   $getSlotUsage['CurrentSlotOccupancy']['num_parked_cars']  = $numParkedCars; 
   $getSlotUsage['CurrentSlotOccupancy']['num_parked_bikes'] = $numParkedBikes; 
   $getSlotUsage['CurrentSlotOccupancy']['num_slots_in_use']  = $currentOccupancy;

   $this->save($getSlotUsage); 
}


/******************************************************************************
 *
 *  UserDashBoard related functions
 *
 *
 *******************************************************************************/

function getOccupancy($spaceIds)
{
	if(!empty($spaceIds)){
		$occupancyArray = $this->find('all',
			array('conditions' => array('CurrentSlotOccupancy.space_id' => $spaceIds),
			'fields' => array('space_id','num_parked_cars','num_parked_bikes')
		));
	}else{
		$occupancyArray = $this->find('all',
			array('fields' => array('space_id','num_parked_cars','num_parked_bikes')
		));
	}

	return $occupancyArray;

}

}
