<?php
class BulkBookingSetting extends AppModel
{
    public $belongsTo = array('Space');


/**
 * Method getSettings to get the bulk booking settings from space id
 *
 * @param $spaceID the id of the particular space
 * @return $bulkBookingSettings array containing the bulk booking settings for space
 */

public function getSettings($spaceID = null)
{
   $bulkBookingSettings = $this->find('first',
                                       array(
                                                'conditions' => array(
                                                        'Space.id' => $spaceID
                                                    ),
                                                'fields' => array(
                                                            'hourly_price',
                                                            'daily_price',
                                                            'weekly_price',
                                                            'monthly_price',
                                                            'yearly_price',
                                                            'deposit',
                                                            'commission_paid_by',
                                                            'commission_percentage',
                                                            'service_tax_paid_by_simplypark',
                                                            'service_tax_paid_by_owner',
                                                            'service_tax_percentage'

                                                    )
                                             )
                                       );

    return $bulkBookingSettings;

}



 

}
