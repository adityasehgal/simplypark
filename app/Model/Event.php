<?php
class Event extends AppModel {
	var $name = 'Event';

    public $hasMany = array('EventAddress', 'EventTiming');

    public $actsAs = array(
        'Containable',
        'CakeAttachment.Upload' => array(
            'event_pic' => array(
                'dir' => "{FILES}Event",
                'deleteMainFile' => true,
                //thumbnails declaration
                'thumbsizes' => array(
                    'main' => array('width' => 280, 'height' => 189, 'name' =>  'event.{$file}.{$ext}'),
                    //'preview' => array('width' => 250, 'height' => 250, 'name' => 'preview.{$file}.{$ext}'),
                    //'thumb' => array('width' => 100, 'height' => 100, 'name' =>  'thumb.{$file}.{$ext}', 'proportional' => false)
                )
            )
        )
    );

    public $validate = array(
        'event_name' => array(
                'required' => array(
                    'rule' => array('notEmpty'),
                    'message' => 'Event name is empty.'
                ),
                'unique' => array(
                    'rule'    => array('isUniqueName'),
                    'message' => 'Event Name is already in use',
                )
            ),
        'address' => array(
                'required' => array(
                    'rule' => array('notEmpty'),
                    'message' => 'Address is empty.'
                )
            ),
        'event_pic' => array(
                'rule1' => array(
                    'allowEmpty' => true
                ),
                'validExtension' => array (
                    'rule' => array('validateFileExtension', array('jpg', 'jpeg', 'png', 'gif')),
                    'message' => 'Invalid file: upload only jpg, png or gif'
                ),
                'maxFileSize' => array(
                    'rule' => array('maxFileSize', 5242880),
                    'message' => 'Invalid file size. Maximum file size is 5mb.'
                ),
            ),
        'tags' => array(
                'required' => array(
                        'rule' => array('notEmpty'),
                        'message' => 'Tags are empty.'
                    )
            )
    );

    function isUniqueName($check) {
 
        $name = $this->find(
            'first',
            array(
                'fields' => array(
                    'Event.id'
                ),
                'conditions' => array(
                    'Event.event_name' => $check['event_name']
                )
            )
        );

        if(!empty($name)){
            if(isset($this->data[$this->alias]['id']) && $this->data[$this->alias]['id'] == $name['Event']['id']){
                return true; 
            }else{
                return false; 
            }
        }else{
            return true; 
        }
    }

/**
 * Method getEvents to get event from database and used as common functions to get event venues from multiple locations
 *
 * @param $limit int number of event venues to fetch at a time
 * @param $isPopular bool to set is_popular field true in query
 * @param $unWantedEvent int event id of the record which is not needed to fetch
 * @return $popularEvents array returns the event venues fetched from database
 */
    function getEvents($limit,$isPopular,$unWantedEvent = null) {
        $options['joins'] = array(
                                array('table' => 'event_addresses',
                                        'alias' => 'EventAddress',
                                        'conditions' => array(
                                            'Event.id = EventAddress.event_id'
                                        )
                                    ),
                                array('table' => 'event_timings',
                                        'alias' => 'EventTiming',
                                        'conditions' => array(
                                            'Event.id = EventTiming.event_id'
                                        )
                                    )
                            );
        $options['contain'] = array(
                                'EventAddress',
                                'EventTiming' => array(
                                        'conditions' => array('EventTiming.to >=' => date("Y-m-d h:i:s"))
                                    )
                                );
         //$options['order'] = 'EventAddress.start_date ASC';
        //$options['order'] = 'rand()';
        $options['limit'] = $limit;
        $options['group'] = 'Event.id';
        $options['recursive'] = -1;
        $options['fields'] = array('Event.id', 'Event.event_name', 'Event.event_pic');
        $options['conditions'] = array(
                                'Event.is_activated' => Configure::read('Bollean.True'),
                                'Event.is_deleted' => Configure::read('Bollean.False'),
                    
                            );
        if ($isPopular) {
            $options['conditions'] = array_merge(
                                        $options['conditions'],
                                        array(
                                            'Event.is_popular' => Configure::read('Bollean.True'),
                                            
                                            )
                                        );
        }

        if (!empty($unWantedEvent)) {
            $options['conditions'] = array_merge(
                                        $options['conditions'],
                                        array(
                                            'Event.id <>' => $unWantedEvent
                                            )
                                        );
        }
        //pr($options['conditions']);
        $popularEvents = $this->find('all', $options);
        //pr($popularEvents);die;
// $log = $this->getDataSource()->getLog(false, false);
// debug($log);
        //pr($popularEvents);die;
        return $popularEvents;
    }

/**
 * Method getEventThroughID to get the event details from a particular id
 *
 * @param $eventID int id of the event
 * @param $eventAddressID int the id of the event address
 * @return $eventData array contains the array of event
 */
    function getEventAndAddressThroughID($eventID = null, $eventAddressID = null) {
        $eventData = $this->find(
                                'first',
                                array(
                                        'conditions' => array(
                                                    'Event.id' => $eventID
                                            ),
                                        'fields' => array(
                                                    'id',
                                                    'event_name',
                                                    'event_pic',
                                                    'description',
                                                    'state_id',
                                                    'city_id',
                                                    'tags'
                                            ),
                                        'contain' => array(
                                                'EventAddress' => array(
                                                        'conditions' => array(
                                                                'EventAddress.id' => $eventAddressID
                                                            ),
                                                        'State' => array(
                                                                'fields' => array(
                                                                        'id',
                                                                        'name'
                                                                    )
                                                            ),
                                                        'City' => array(
                                                                'fields' => array(
                                                                        'id',
                                                                        'name'
                                                                    )
                                                            ),
                                                        'fields' => array(
                                                                'id',
                                                                'address',
                                                                'lat',
                                                                'long'
                                                            )
                                                    )
                                            )
                                    )
                    );

        $eventData['EventAddress'] = array_map(array($this, '_formatData'), $eventData['EventAddress'][0]);
        return $eventData;
    }

/**
 * Method _formatData to replace event address array with index value to non index 
 *
 * @param $data array containing the event address values
 * @return $data array containing the event address values so that the main array can replace with non index values
 */
    protected function _formatData($data) {
        return $data;
    }


}


