<?php
App::uses('AuthComponent', 'Controller/Component');
class UserProfile extends AppModel {
	var $name = 'UserProfile';

    public $actsAs = array(
        'CakeAttachment.Upload' => array(
            'profile_pic' => array(
                'dir' => "{FILES}User",
                'deleteMainFile' => true,
                //thumbnails declaration
                'thumbsizes' => array(
                    'main' => array('width' => 128, 'height' => 128, 'name' =>  'user.{$file}.{$ext}'),
                    //'preview' => array('width' => 250, 'height' => 250, 'name' => 'preview.{$file}.{$ext}'),
                    //'thumb' => array('width' => 100, 'height' => 100, 'name' =>  'thumb.{$file}.{$ext}', 'proportional' => false)
                )
            )
        )
    );

    public $validate = array(
            'first_name' => array(
                    'required' => array(
                        'rule' => array('notEmpty'),
                        'message' => 'First name is empty.'
                    )
                ),
            /*'last_name' => array(
                    'required' => array(
                            'rule' => array('notEmpty'),
                            'message' => 'Last name is empty.'
                        )
                ),*/
            'tan_number' => array(
                    'required' => array(
                            'rule' => array('numeric'),
                            'allowEmpty' => true,
                            'message' => 'Invalid tan number.'
                        )
                    
                ),
            'mobile' => array(
                    'numeric' => array(
                            'rule' => array('numeric'),
                            'allowEmpty' => true,
                            'message' => 'Invalid mobile number.'
                        )
                    
                ),
            'profile_pic' => array(
                'notEmpty' => array(
                    'rule' => 'requiredFile',
                    'message' => 'Profile pic is empty'
                  ),
                'validExtension' => array (
                    'rule' => array('validateFileExtension', array('jpg', 'jpeg', 'png', 'gif')),
                    'message' => 'Invalid file: upload only jpg, png or gif'
                ),
                'maxFileSize' => array(
                    'rule' => array('maxFileSize', 5242880),
                    'message' => 'Invalid file size. Maximum file size is 5mb.'
                ),
            )
        );
    
/**
 * Method getUserName to get user's first and last name for sending in confirmation booking email
 *
 * @param $userID int the id of the user
 * @return $getUserName array containing the first and last name of the user
 */
    function getUserName($userID = null) {
        $getUserName = $this->find(
                            'first',
                            array(
                                    'conditions' => array(
                                            'UserProfile.user_id' => $userID
                                        ),
                                    'fields' => array(
                                            'id',
                                            'first_name',
                                            'last_name',
                                            'mobile'
                                        )
                                )
                        );
        return $getUserName;
    }


/**
 * Method getUserMobile to get user's Mobile to send location SMS 
 *
 * @param $userID int the id of the user
 * @return $mobile 
 */
function getUserMobile($userID) 
{
        $getUserMobile = $this->find(
                            'first',
                            array(
                                    'conditions' => array(
                                            'UserProfile.user_id' => $userID
                                        ),
                                    'fields' => array(
                                            'mobile'
                                        )
                                )
                        );
        return $getUserMobile['UserProfile']['mobile'];
    }
}
