<?php
class SpaceTierPricing extends AppModel {

/**
 * Method getTierHourlyPricing to get the price to charge given a number of hours
 * @param $spaceID int the id of the space 
 * @param $hours int the hours for which the space will be used
 * @return booking Price to charge for the hours supplied
*/

function getTierHourlyPrice($spaceID = null, $numHours)
{
   $bookingPrice = $this->find(
                          'first',
                           array(
                                  'conditions' => array(
                                                 'SpaceTierPricing.space_id' => $spaceID,
                                                 'SpaceTierPricing.hours' => $numHours
                                                ),
                                  'fields' => array(
                                                'price'
                                               )
                                 )
                             );
    //CakeLog::write('debug','In getTierHourlyPrice ...' . print_r($bookingPrice,true));
    return $bookingPrice['SpaceTierPricing']['price'];


}

/**
 * Method getTierPricingList to get the tier price  
 * @param $spaceID int the id of the space 
 * @return list of per hour price 
*/

function getTierPricingList($spaceID = null)
{
   $bookingPriceList = $this->find(
                          'list',
                           array(
                                  'conditions' => array(
                                                 'SpaceTierPricing.space_id' => $spaceID,
                                                ),
                                  'fields' => array(
                                                'price'
                                               )
                                 )
                             );
    //CakeLog::write('debug','In getTierPricingList ...' . print_r($bookingPriceList,true));
    return $bookingPriceList;


}

}
