<?php
class ServiceTaxCharge extends AppModel {
	public $validate = array(
            'service_charge_percentage' => array(
                    'required' => array(
                            'rule' => array('numeric'),
                            'allowEmpty' => true,
                            'message' => 'Invalid Service Charge Percentage.'
                        )
                ),
            'service_tax_percentage' => array(
                    'numeric' => array(
                            'rule' => array('numeric'),
                            'allowEmpty' => true,
                            'message' => 'Invalid Service Tax Percentage.'
                        )
                )
        );

/**
 * Method getGlobalSpaceCharges to getting the globally defined space charge including service tax and commission
 *
 *
 * @return $getChargeSetting array containing the information for charging tax and commission
 */
    function getGlobalSpaceCharges() {
        $getChargeSetting = $this->find('first',
                                        array(
                                                'fields' => array(
                                                        'commission_paid_by',
                                                        'commission_percentage',
                                                        'service_tax_paid_by_simplypark'
                                                    )
                                            )
                            );
        return $getChargeSetting;
    }

/**
 * Method getGlobalSpaceTaxOnly to getting the globally defined service tax only
 *
 *
 * @return $getServiceTax array containing the service tax value only
 */
    function getGlobalSpaceTaxAndPaidBy() {
        $getServiceTax = $this->find('first',
                                        array(
                                                'fields' => array(
                                                        'service_tax_percentage',
                                                        'service_tax_paid_by_simplypark'
                                                    )
                                            )
                            );
        return $getServiceTax;
    }
}