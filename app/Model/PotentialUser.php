<?php
class PotentialUser extends AppModel {
    public $hasMany = array('Space','User');

 
/**
 * Method addPotentialUser to add a potential User 
 *
 * @param $spaceID int the id of the space which was searched 
 * @param $mobile mobile number 
 * @return 
 */
function addPotentialUser($spaceID,$userID=null,$mobile) 
{
   $pUser['PotentialUser']['space_id'] = $spaceID;
   $pUser['PotentialUser']['user_id'] = $userID;
   $pUser['PotentialUser']['mobile'] = $mobile;

   $this->save($pUser);

}

}
