<?php
class Parking extends AppModel {

  public $actsAs = array('GeoLocation');	

/**
 * Method parkings to get all the parkings close to a lat/lng within a given radius 
 *
 * @param $lat float latitude 
 * @param $long float longitude 
 * @param $radius radius 
 * @return $parkings array containing all the parkings 
 */

  function parkings($lat = null, $lng = null, $radius = null)
  {

	  $parkings = $this->search('all', 
		                     array(
					   'center'=> array('lat' => $lat, 'lng' => $lng, 'radius' => $radius),
				        )
	                           );

	  CakeLog::write('debug','Inside parkings...' . print_r($parkings,true));

	  return $parkings;
		   
  }	  
  
  
 /**
 * Method detail to parking detail of the given parking id 
 *
 * @param $id  
 * @return $parkings array containing all the parkings 
 */

  function detail($id)
  {

	  $detail = $this->find('first', array('conditions'=> array('Parking.id' => $id)));

	  CakeLog::write('debug','Inside parkings...' . print_r($detail,true));

	  return $detail;
		   
  }	  
}

