<?php
class EventTiming extends AppModel {

	public $belongsTo = array('EventAddress');
	public $actsAs = array('Containable');

	/**
 * Method getAddresses to get event addresses according to the filter parametters at event detail page
 *
 * @param $conditions array contains the conditions to be implemented on query
 * @return $eventaddresses array contains the event addresses for a particular event
 */
    function getAddresses($conditions = array()) {
        /*$options['order'] = 'EventAddress.start_date ASC';
        $options['fields'] = array('EventAddress.id','EventAddress.event_id', 'EventAddress.address', 'EventAddress.start_date');
        $options['recursive'] = -1;*/
        $options['conditions'] = array('EventAddress.event_id' => $conditions['event_id']);
        if (!empty($conditions['state'])) {
            $options['conditions'] = array_merge(
                                        $options['conditions'], array('EventAddress.state_id' => $conditions['state'])
                                    );
        }

        if (!empty($conditions['city'])) {
            $options['conditions'] = array_merge(
                                        $options['conditions'], array('EventAddress.city_id' => $conditions['city'])
                                    );
        }

        if (!empty($conditions['mob'])) {
            $options['conditions'] = array_merge(
                                        $options['conditions'],
                                        array(
                                        		'MONTH(EventTiming.from) <=' => $conditions['mob'],
                                        		'MONTH(EventTiming.to) >=' => $conditions['mob'],
                                        	)
                                    );
        }

        $options['contain'] = array(
        							'EventAddress' => array(
        									'fields' => array(
        											'EventAddress.id',
        											'EventAddress.event_id',
        											'EventAddress.address',
        											'EventAddress.start_date'
        										),
        									'State' => array('name'),
                                    		'City' => array('name')
        								)
                                );
        $eventaddresses = $this->find('all', $options);
        //$formatEventaddresses['EventAddress'] = array_map(array($this, '_formatData'), $eventaddresses);
        //return $formatEventaddresses;
        return $eventaddresses;
    }

    protected function _formatData($data) {
        //$result = $data['EventAddress'];
        /*$result['State'] = $data['EventAddress']['State'];
        $result['City'] = $data['EventAddress']['City'];*/
        $result['EventTiming'][] = $data['EventTiming'];
        return $result;
    }

/**
 * Method getNonExpiredEvent to get all the event ids which are not expired yet
 *
 * @return $expiredEvents array containing the list of event ids which are not expired
 */
    function getNonExpiredEvent() {
        $expiredEvents = $this->find(
                                'list',
                                array(
                                        'conditions' => array(
                                                'DATE(EventTiming.to) >' => date('Y-m-d')
                                            ),
                                        'group' => 'event_id',
                                        'fields' => array(
                                                'event_id'
                                            )
                                    )
                            );
        return $expiredEvents;
    }

/**
 * Method getEventDates to get only from and to time of the event timing for space detail page
 *
 * @param $eventTimingId int the id of the event timing table
 * @return $times array containing from and to time of the event
 */
    function getEventDates($eventTimingId = null) {
        $times = $this->find(
                                'first',
                                array(
                                        'conditions' => array(
                                                'EventTiming.id' => $eventTimingId
                                            ),
                                        'fields' => array(
                                                'from',
                                                'to'
                                            )
                                    )
                            );
        return $times;
    }

}