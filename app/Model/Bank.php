<?php

class Bank extends AppModel {
	var $name = 'Bank';


/**
 * Method getBankData to get bank details from database 
 *
 * @param $ifscCode int 
 * @return $data array returns the bank details from the ifsc code
 */
	public function getBankData($ifscCode) {
		$data = $this->find('first',array(
								'conditions' => array(
									'Bank.ifsc_code' => $ifscCode
								)
							));

		return $data;
	}
}