<?php
class SpacePark extends AppModel {

/**
 * Method getSlot to get slot of a space
 *
 * @param $spaceID int the id of the space
 * @return $spaceSlot array containing the slot
 */

	function getSlot($spaceID = null) {
		$spaceSlot = $this->find(
							'first',
							array(
									'conditions' => array(
											'SpacePark.space_id' => $spaceID
										),
									'fields' => array(
											'id'
										),
									'order' => 'SpacePark.id ASC'
								)
						);
		return $spaceSlot;
	}
/**
 * Method checkAvailableSlots to check available slots
 *
 * @param $spaceID int the id of the space
 * @param $spaceParkIDs array containing the id of the booked slots
 * @return $spaceAvailableSlot array containing the available slot
 */
	function checkAvailableSlots($spaceID = null, $spaceParkIDs = array()) {
		$spaceAvailableSlot = $this->find(
							'first',
							array(
									'conditions' => array(
											'SpacePark.space_id' => $spaceID,
											'NOT' => array(
													'SpacePark.id' => $spaceParkIDs
												)
										),
									'limit' => 1,
									'fields' => array(
											'id'
										),
									'order' => 'SpacePark.id ASC'
								)
						);
		return $spaceAvailableSlot;
	}

/**
 * Method getkAvailableSlots to check available slots
 *
 * @param $spaceID int the id of the space
 * @param $spaceParkIDs array containing the id of the booked slots
 * @return $spaceAvailableSlot array containing the available slot
 */
function getAvailableSlots($spaceID = null, $spaceParkIDs = array())
{
	$numAvailableSlots = $this->find(
			       'count',
			array(
				'conditions' => array(
					'SpacePark.space_id' => $spaceID,
					'NOT' => array(
						'SpacePark.id' => $spaceParkIDs
						)
					),
			     )
			);
	return $spaceAvailableSlot;
}
}
