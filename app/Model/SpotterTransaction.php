<?php
class SpotterTransaction extends AppModel {


/******************************************************************************
 * Function  : createTransaction
 * Parameters: $spaceId, $deviceId,$annotationResultId,$datetime,$direction,
 *             $vehicleType,$regNumber
 ******************************************************************************/ 
 public function createTransaction($spaceId,$deviceId,$annotationResultId,$dateTime,
		                          $direction,$vehicleType,$regNumber)
 {
    $spotterTransaction = array();
    $id = -1;
    $spotterTransaction['SpotterTransaction']['space_id']                    = $spaceId;
    $spotterTransaction['SpotterTransaction']['device_id']                   = $deviceId;
    $spotterTransaction['SpotterTransaction']['spotter_annotation_result_id'] = $annotationResultId;
    $spotterTransaction['SpotterTransaction']['time']                        = $dateTime;
    $spotterTransaction['SpotterTransaction']['direction']                   = $direction;
    $spotterTransaction['SpotterTransaction']['vehicle_type']                = $vehicleType;
    $spotterTransaction['SpotterTransaction']['reg_number']                  = $regNumber;
    $spotterTransaction['SpotterTransaction']['created']                     = date("Y-m-d H:i");
    $spotterTransaction['SpotterTransaction']['modified']                    = date("Y-m-d H:i");
    
    if($this->save($spotterTransaction)){
      $id = $this->getLastInsertId();
      CakeLog::write('debug','Saved SpotterTransaction, last ID is ' . $id);
    }else{
      CakeLog::write('debug','SpotterTransaction save Failed');
    }

   return $id;
 }
}
