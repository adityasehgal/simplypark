<?php
class EntryExitTime extends AppModel {
	public $name = 'EntryExitTime';
	var $actsAs = array('Containable');
        var $belongsTo = array('Booking','Space','Operator');





/**
 * Method to log Entry   
 * 
 * @return void 
 */
function logEntry($spaceId,$operatorId,$bookingId,$parkingId,$bookingType,
 	          $vehicleReg,$vehicleType,$entryTime,$isParkAndRide,
	          &$recordId,&$incrementSlotInUse)
{
    
    $existingRecord = $this->find('first',
                                   array('conditions' => array('EntryExitTime.space_id' => $spaceId,
                                                               'EntryExitTime.parking_id' => $parkingId),
                                          'recursive' => -1 
                                        )
				);

    $incrementSlotInUse = 1;
    $retValue = 0;

    CakeLog::write('debug','In LogEntry with bookingType as ' . $bookingType);

    if(empty($existingRecord))
    { 
	    $entryRecord = array();
	    $entryRecord['EntryExitTime']['parent_booking_id'] = $bookingId;
	    $entryRecord['EntryExitTime']['parking_id']        = $parkingId;
	    $entryRecord['EntryExitTime']['space_id']          = $spaceId;
	    $entryRecord['EntryExitTime']['operator_id']       = $operatorId;
	    $entryRecord['EntryExitTime']['status']            = Configure::read('BookingEntryOrExit.entry');
	    $entryRecord['EntryExitTime']['entry_time']        = $entryTime;
	    $entryRecord['EntryExitTime']['exit_time']         = "0000-00-00 00:00:00";
	    $entryRecord['EntryExitTime']['vehicle_reg']       = $vehicleReg; 
	    $entryRecord['EntryExitTime']['vehicle_type']      = $vehicleType;
	    $entryRecord['EntryExitTime']['is_park_and_ride']  = $isParkAndRide;
	    $entryRecord['EntryExitTime']['parking_fees']      = 0;
	    $entryRecord['EntryExitTime']['booking_type']       = $bookingType;
	    $entryRecord['EntryExitTime']['created']           = date("Y-m-d H:i");
	    $entryRecord['EntryExitTime']['modified']          = date("Y-m-d H:i");
	    
	    if($this->save($entryRecord)){
               $recordId = $this->getLastInsertId();
               $retValue = 0;
	    }else{
               $retValue = -1;
	       $recordId = -1;
	       $incrementSlotInUse = 0;

	    }

    }else{
	    $incrementSlotInUse = 0;
            $save = false;
	    if(empty($existingRecord['EntryExitTime']['vehicle_reg']) && 
               !empty($vehicleReg)){
	         $existingRecord['EntryExitTime']['vehicle_reg'] =  $vehicleReg;
                 $save = true;
            }
	    if($existingRecord['EntryExitTime']['status'] == Configure::read('BookingEntryOrExit.exit'))
	    {
               //only update the vehicle Type , entry time and vehicle reg
               $existingRecord['EntryExitTime']['entry_time']   = $entryTime;
               $existingRecord['EntryExitTime']['vehicle_type'] = $vehicleType;
	       $existingRecord['EntryExitTime']['vehicle_reg']  = $vehicleReg;

	       CakeLog::write('debug','Status as exit..updating vehicle type and reg ' . print_r($existingRecord,true));
               $save = true;
            }else{
	       //stale result
	       $retValue = -1;
	       $recordId = -2;
	    }
            
            if($save)
            {
	       if($this->save($existingRecord))
               {
	          $recordId = $existingRecord['EntryExitTime']['id'];
	 	  $retValue = 0;
	       }else{
		  $retValue = -1;
                  $recordId = -1;
	       }
            }
         }

    return $retValue;  

}

/**
 * Method to take actions at exit time  
 * 
 * @return  
 */
function logExit($spaceId,$operatorId,$parentBookingId,$parkingId,$parkingFees,
	         $vehicleReg,$vehicleType,$exitTime,&$exitId,&$decrementSlotInUse) 
{
    $existingRecord = $this->find('first',
                                   array('conditions' => array('EntryExitTime.space_id' => $spaceId,
                                                               'EntryExitTime.parking_id' => $parkingId),
                                          'recursive' => -1 
                                        )
				);
    $retValue = 0;


    if(empty($existingRecord))
    {
            CakeLog::write('debug','No entry found for this exit');
	    //an exit update is received before an entry update. Create a new record
	    $exitRecord = array();
	    $exitRecord['EntryExitTime']['parent_booking_id'] = $parentBookingId;
	    $exitRecord['EntryExitTime']['parking_id']        = $parkingId;
	    $exitRecord['EntryExitTime']['space_id']          = $spaceId;
	    $exitRecord['EntryExitTime']['operator_id']       = $operatorId;
	    $exitRecord['EntryExitTime']['status']            = Configure::read('BookingEntryOrExit.exit');
	    $exitRecord['EntryExitTime']['entry_time']        = "0000-00-00 00:00:00";
	    $exitRecord['EntryExitTime']['exit_time']         = $exitTime;
	    $exitRecord['EntryExitTime']['vehicle_reg']       = $vehicleReg; 
	    $exitRecord['EntryExitTime']['vehicle_type']      = $vehicleType;
	    $exitRecord['EntryExitTime']['is_park_and_ride']  = 0;
	    $exitRecord['EntryExitTime']['parking_fees']      = $parkingFees;
	    $exitRecord['EntryExitTime']['created']           = date("Y-m-d H:i");
            $exitRecord['EntryExitTime']['modified']          = date("Y-m-d H:i");
	   
            CakeLog::write('debug','Inside LogOnlineExit.., creating a new  record' . print_r($exitRecord,true));
	    if($this->save($exitRecord))
	    {
	       $retValue = 0;
	       $exitId = $this->getLastInsertId();

	    }else{
	       $retValue = -1;
	       $exitId = -1;
	    }

	    $decrementSlotInUse = 0;
    }else{
            $save = false;
	    if(empty($existingRecord['EntryExitTime']['vehicle_reg']) && 
               !empty($vehicleReg)){
	         $existingRecord['EntryExitTime']['vehicle_reg'] =  $vehicleReg;
                 $save = true;
            }
	    
            if($existingRecord['EntryExitTime']['status'] == Configure::read('BookingEntryOrExit.entry'))
	    {
		    CakeLog::write('debug','Entry found for this exit');
		    $existingRecord['EntryExitTime']['exit_time']         = $exitTime;
		    $existingRecord['EntryExitTime']['parking_fees']      = $parkingFees;
		    $existingRecord['EntryExitTime']['status']            = Configure::read('BookingEntryOrExit.exit');
                    CakeLog::write('debug','Inside LogOnlineExit.., updating a record' . print_r($existingRecord,true)); 
                    $save = true;
            }else{
                 //stale
	       $retValue = -1;
	       $exitId = -2;
	       $decrementSlotInUse = 0;
	    }
          
          if($save)
          {
             if($this->save($existingRecord))
             {
	        $exitId = $existingRecord['EntryExitTime']['id'];
	        $decrementSlotInUse = 1;
	        $retValue = 0;
             }else{
	       $retValue = -1;
	       $exitId = -1;
	       $decrementSlotInUse = 0;
            }
         }
       }

    return $retValue;
}


/**
 * Method to cancel a Entry/Exit recorded on the app  
 * 
 * @return void 
 */
function cancelTransaction($spaceId,$parkingId,$operatorId,$vehicleReg,$vehicleType,$entryTime,$bookingId,$bookingType,&$vehicleTypeOut) 
{
   $existingRecord = $this->find('first',
			       array( 
				       'conditions' => array('EntryExitTime.space_id' => $spaceId,
				                             'EntryExitTime.parking_id' => $parkingId),
				       'recursive' => -1		     
				    )
			    );

   if(!empty($existingRecord))
   {
      CakeLog::write('debug',"In cancelTransaction..." . print_r($existingRecord,true));

      $existingRecord['EntryExitTime']['status']       = Configure::read('BookingEntryOrExit.cancelled'); 
      $existingRecord['EntryExitTime']['operator_id']  = $operatorId;
      $existingRecord['EntryExitTime']['is_updated']   = 1;
      $vehicleTypeOut = $existingRecord['EntryExitTime']['vehicle_type'];

     if(empty($existingRecord['EntryExitTime']['vehicle_reg']) && !empty($vehicleReg))
     {
        $existingRecord['EntryExitTime']['vehicle_reg'] = $vehicleReg;
     }

     if($this->save($existingRecord)){
	      return $existingRecord['EntryExitTime']['id'];
     }else{
	      return -1;
     }
   }else{
	   CakeLog::write('error',"No such Transaction Exists for cancellation. ParkingID : ". $parkingId . "SpaceID : ". $spaceId);
           if(!empty($bookingId))
	   {
	           CakeLog::write('error',"Creating a new transaction for vehicleReg: ". $vehicleReg);
		   $entryRecord = array();
		   $entryRecord['EntryExitTime']['parent_booking_id'] = $bookingId;
		   $entryRecord['EntryExitTime']['parking_id']        = $parkingId;
		   $entryRecord['EntryExitTime']['space_id']          = $spaceId;
		   $entryRecord['EntryExitTime']['operator_id']       = $operatorId;
		   $entryRecord['EntryExitTime']['status']            = Configure::read('BookingEntryOrExit.cancelled');
		   $entryRecord['EntryExitTime']['entry_time']        = $entryTime;
		   $entryRecord['EntryExitTime']['exit_time']         = "0000-00-00 00:00:00";
		   $entryRecord['EntryExitTime']['vehicle_reg']       = $vehicleReg; 
		   $entryRecord['EntryExitTime']['vehicle_type']      = $vehicleType;
		   $entryRecord['EntryExitTime']['parking_fees']      = 0;
		   $entryRecord['EntryExitTime']['booking_type']       = $bookingType;
		   $entryRecord['EntryExitTime']['created']           = date("Y-m-d H:i");
		   $entryRecord['EntryExitTime']['modified']          = date("Y-m-d H:i");

		   if($this->save($entryRecord)){
			   $recordId = $this->getLastInsertId();
			   $retValue = 0;
                           $vehicleTypeOut = $vehicleType;
		   }else{
			   $retValue = -1;
			   $recordId = -1;
		   }
	   }else{
		   return -1;
	   }
   }


}


/**
 * Method to update a Entry/Exit recorded on the app  
 * 
 * @return void 
 */
function updateTransaction($spaceId,$parkingId,$parkingStatus,$parkingFees,$operatorId,
                           $vehicleReg,$vehicleType,$entryTime,$exitTime,$bookingId,$bookingType,&$vehicleTypeOut) 
{
   $existingRecord = $this->find('first',
			       array( 
				       'conditions' => array('EntryExitTime.space_id' => $spaceId,
				                             'EntryExitTime.parking_id' => $parkingId),
				       'recursive' => -1		     
				    )
			    );

   CakeLog::write('debug','In update Transaction Model, parkingID is ' . $parkingId);

   if(!empty($existingRecord))
   {
      CakeLog::write('debug',"In updateTansaction..." . print_r($existingRecord,true));

      $existingRecord['EntryExitTime']['status']       = $parkingStatus;
      $existingRecord['EntryExitTime']['parking_fees'] = $parkingFees;
      $existingRecord['EntryExitTime']['operator_id']  = $operatorId;
      $existingRecord['EntryExitTime']['exit_time']    = "0000-00-00 00:00:00";
      $existingRecord['EntryExitTime']['is_updated']   = 1;

      $vehicleTypeOut = $existingRecord['EntryExitTime']['vehicle_type'];
      if(empty($existingRecord['EntryExitTime']['vehicle_reg']) && !empty($vehicleReg))
      {
        $existingRecord['EntryExitTime']['vehicle_reg'] = $vehicleReg;
      }

      if($this->save($existingRecord)){
	      return $existingRecord['EntryExitTime']['id'];
      }else{
	      return -1;
      }
   }else{
	   CakeLog::write('error',"No such Transaction Exists for updation. ParkingID : ". $parkingId . "SpaceID : ". $spaceId);
	   if(!empty($bookingId))
	   {
		   CakeLog::write('error',"Creating a new transaction for vehicleReg: ". $vehicleReg);
		   $entryRecord = array();
		   $entryRecord['EntryExitTime']['parent_booking_id'] = $bookingId;
		   $entryRecord['EntryExitTime']['parking_id']        = $parkingId;
		   $entryRecord['EntryExitTime']['space_id']          = $spaceId;
		   $entryRecord['EntryExitTime']['operator_id']       = $operatorId;
		   $entryRecord['EntryExitTime']['status']            = Configure::read('BookingEntryOrExit.cancelled');
		   $entryRecord['EntryExitTime']['entry_time']        = $entryTime;
		   $entryRecord['EntryExitTime']['exit_time']         = "0000-00-00 00:00:00";
		   $entryRecord['EntryExitTime']['vehicle_reg']       = $vehicleReg; 
		   $entryRecord['EntryExitTime']['vehicle_type']      = $vehicleType;
		   $entryRecord['EntryExitTime']['parking_fees']      = 0;
		   $entryRecord['EntryExitTime']['booking_type']       = $bookingType;
		   $entryRecord['EntryExitTime']['created']           = date("Y-m-d H:i");
		   $entryRecord['EntryExitTime']['modified']          = date("Y-m-d H:i");

		   if($this->save($entryRecord)){
			   $recordId = $this->getLastInsertId();
			   $retValue = 0;
			   $vehicleTypeOut = $vehicleType;
		   }else{
			   $retValue = -1;
			   $recordId = -1;
		   }
	   }else{
	      return -1;
	   }
   }


}


/**
 * Method to get Records to Push To Client 
 * 
 * @return  
 */

public function getRecordsToPushToClient($idArray)
{
   //CakeLog::write('debug','In EntryExitTime getRecordsToPushClient..' . print_r($idArray,true));	
   $recordsToPush = $this->find(
	                       'all',
			       array( 
				       'conditions' => array('EntryExitTime.id' => $idArray),
				       'fields' => array('parent_booking_id','parking_id','space_id',
                                                          'status','entry_time','exit_time','operator_id',
                                                          'vehicle_reg','vehicle_type','is_park_and_ride',
                                                          'parking_fees','booking_type','is_updated'),
                                       'recursive' => -1
				    )
			    );

   return $recordsToPush;


}

/**
 * Method to add/update records when a push is called from the app 
 * 
 * @return  
 */

function addUpdateRecord($transaction,$spaceId,$operatorId,&$isStale,&$updateSlotInUse){

	$this->create();
        $updateSlotInUse = 1;
	$isStale = 0;
   //first see if this record exists
	$existingRecord = $this->find('first',
		                  array(
					  'conditions' => array('EntryExitTime.parking_id' => $transaction['parking_id']),
					  'recursive' => -1
				      )

                );

	if(empty($existingRecord)){
		CakeLog::write('debug','EmptyRecord for parking ID ' . $transaction['parking_id']);
		$entryExitRecord['EntryExitTime']['parent_booking_id'] = $transaction['parent_booking_id'];
		$entryExitRecord['EntryExitTime']['parking_id']        = $transaction['parking_id'];
		$entryExitRecord['EntryExitTime']['space_id']          = $spaceId;
		$entryExitRecord['EntryExitTime']['operator_id']       = $operatorId;
		$entryExitRecord['EntryExitTime']['status']            = $transaction['status'];
		$entryExitRecord['EntryExitTime']['entry_time']        = $transaction['entry_time'];
		$entryExitRecord['EntryExitTime']['vehicle_reg']       = $transaction['vehicle_reg'];
		$entryExitRecord['EntryExitTime']['vehicle_type']      = $transaction['vehicle_type'];
		$entryExitRecord['EntryExitTime']['is_park_and_ride']  = $transaction['is_park_and_ride'];
		$entryExitRecord['EntryExitTime']['booking_type']      = $transaction['booking_type'];
		$entryExitRecord['EntryExitTime']['is_updated']        = $transaction['is_updated'];
                $entryExitRecord['EntryExitTime']['created']          = date("Y-m-d H:i");
                $entryExitRecord['EntryExitTime']['modified']         = date("Y-m-d H:i");
		
		if($entryExitRecord['EntryExitTime']['status'] == Configure::read('BookingEntryOrExit.exit')){
		   $entryExitRecord['EntryExitTime']['exit_time']  = $transaction['exit_time'];
		   $entryExitRecord['EntryExitTime']['parking_fees']  = $transaction['parking_fees'];
		   
                   $updateSlotInUse = 0; //direct exit received. no need to update slot
		}

             	
		CakeLog::write('debug','In addUpdate...saving ' . print_r($entryExitRecord,true));
		if($this->save($entryExitRecord))
		{
		   return $this->getLastInsertId();
		}else{
	           return -1;
                }		   
		 
	}else{
		CakeLog::write('debug','NON EmptyRecord for parking ID ' . $transaction['parking_id'] . ' id :: ' . $existingRecord['EntryExitTime']['id']);
		if($existingRecord['EntryExitTime']['status'] == Configure::read('BookingEntryOrExit.exit')
			&& $transaction['is_updated'] != 1) 
		{
			CakeLog::write('debug','Stale Information returned for ' . $transaction['parking_id']);
			if($existingRecord['EntryExitTime']['vehicle_reg'] == "" && $transaction['vehicle_reg'] != ""){
			        CakeLog::write('debug','Updating Car Reg Number and entry time');
		                $existingRecord['EntryExitTime']['entry_time'] = $transaction['entry_time'];
				$existingRecord['EntryExitTime']['vehicle_reg']= $transaction['vehicle_reg'];
				if($this->save($existingRecord)){
		                   $updateSlotInUse = 0;
				   return $existingRecord['EntryExitTime']['id']; 
				}else{
                                   return -1;
				}
			}
			CakeLog::write('debug','Stale Information returned for ' . $transaction['parking_id']);
			CakeLog::write('debug','Existing status = 2 and update Flag is ' . $transaction['is_updated']);
			$isStale = 1;
			return -1;	
		}else if($existingRecord['EntryExitTime']['status'] == $transaction['status'] && $transaction['is_updated'] != 1){
		       CakeLog::write('debug','Stale Information returned for ' . $transaction['parking_id']);
		       CakeLog::write('debug','Existing status and transaction status is same. Transaction Status =   ' . $transaction['status']);
		       CakeLog::write('debug','Existing status  =   ' . $existingRecord['EntryExitTime']['status']);
		       $isStale = 1;
		       return -1;	
		}
		$existingRecord['EntryExitTime']['status']        = $transaction['status'];
		$existingRecord['EntryExitTime']['exit_time']     = $transaction['exit_time'];
		$existingRecord['EntryExitTime']['parking_fees']  = $transaction['parking_fees'];
		$existingRecord['EntryExitTime']['modified']      = date("Y-m-d H:i");

                if($this->save($existingRecord)){
		   $updateSlotInUse = 1;
		   return $existingRecord['EntryExitTime']['id'];
		}else{
		   return -1;
		}
	}
}



/**
 * Method to display booking information for a booking pass that is presented again 
 * 
 * @return  
 */
function getBookingInfo($bookingId,$spaceId,$transId) 
{
        $bookingInfo = $this->find(
                                    'first',
                            array(
                                    'conditions' => array(
                                            'EntryExitTime.booking_id' => $bookingId,
                                            'EntryExitTime.space_id' => $spaceId,
                                            'EntryExitTime.transaction_id' => $transId 
                                        ),
                                    'fields' => array(
                                            'status',
                                            'entry_time',
                                            'exit_time',
                                        ),
                                    
                                    'contain' => array(
                                            'Booking' => array(
                                                   'fields' => array(
                                                             'start_date',
                                                             'end_date',
                                                             'booking_type',
                                                             'space_price'
                                                             ),
						'UserCar' => array(
								'fields' => array(
									'registeration_number',
                                                                        'car_type'
									)
								),
                                                       ),
                                       )
                               )
                              );

       //CakeLog::write('debug','Inside getBookingInfo --->Exit' . print_r($bookingInfo,true));
       
    return $bookingInfo;
}


/**
 * Method to update overdue payment field in EntryExit Times table 
 * 
 * @return  
 */
function updatePaymentInfo($dbId,$overDuePayment,$displayString) 
{
  //CakeLog::write('debug','updatePaymentInfo with db id ' . $dbId . ' overDuePayment = ' . $overDuePayment . ' displayString= '. $displayString);
  $entryExitRow['EntryExitTime']['id'] = $dbId;
  $entryExitRow['EntryExitTime']['overStayCharge'] = $overDuePayment;
  $entryExitRow['EntryExitTime']['overStayDisplayString'] = $displayString; 
  
  $this->save($entryExitRow);

}


}
