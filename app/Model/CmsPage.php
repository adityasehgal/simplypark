<?php
class CmsPage extends AppModel {
	public $name = 'CmsPage';

	public $validate = array(
            'meta_title' => array(
                    'required' => array(
                            'rule' => array('notEmpty'),
                            'message' => 'Meta title is empty.'
                        )
                ),
            'meta_keyword' => array(
                    'required' => array(
                            'rule' => array('notEmpty'),
                            'message' => 'Meta keyword is empty.'
                        )
                ),
            'meta_description' => array(
                    'required' => array(
                            'rule' => array('notEmpty'),
                            'message' => 'Description is empty.'
                        )
                ),
            'page_title' => array(
                    'required' => array(
                            'rule' => array('notEmpty'),
                            'message' => 'Page title is empty.'
                        )
                ),
            'page_name' => array(
                    'required' => array(
                            'rule' => array('notEmpty'),
                            'message' => 'Page name is empty.'
                        )
                )
        );
}