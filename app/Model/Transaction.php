<?php
class Transaction extends AppModel {
	var $name = 'Transaction';

/******************************************************************************
 * getBookingId for a payment_id
 *
 *  Notes: only to be used for bookings generated through app where the pass
 *         is scanned
 ******************************************************************************/

public function getBookingId($paymentId)
{
   $record = $this->find('first',
                          array('conditions' => array('Transaction.payment_id' => $paymentId),
                                'fields' => array('booking_id')
                               )
                         );

   if(!empty($record)){
     return $record['Transaction']['booking_id'];
   }else{
     return -1;
   }
                       
}

}
