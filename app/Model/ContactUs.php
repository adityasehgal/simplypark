<?php
class ContactUs extends AppModel {
	public $name = 'ContactUs';
	
	public $validate = array(
            'name' => array(
                    'required' => array(
                        'rule' => array('notEmpty'),
                        'message' => 'Name is empty.'
                    )
                ),
            'email' => array(
                    'email' => array(
                            'rule'     => 'email',
                            'required' => true,
                            'allowEmpty' => false,
                            'message' => 'Invalid Email'
                        )
                ),
            'message' => array(  
                    'required' => array(
                        'rule' => array('notEmpty'),
                        'message' => 'Message is empty.'
                    )
                )
        );
}