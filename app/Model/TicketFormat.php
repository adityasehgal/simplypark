<?php
class TicketFormat extends AppModel {
    public $belongsTo = array('Space');

    
/**
 * Method getCustomTicketFormats 
 *
 *
 * @param $spaceID int the id of the space
 * @return $customTicketFormat array containing all possible ticket formats
 */
function getCustomTicketFormats($spaceID = null)
{
   $customTicketFormats = $this->find('all',
                                     array('conditions' => array('TicketFormat.space_id' => $spaceID,
                                                                 'TicketFormat.is_active' => 1
                                                                ),
                                           'fields'=>array('ticket_type',
                                                           'space_address',
                                                           'space_heading',
                                                           'agency_name',
                                                           'space_owner_mobile',
                                                           'operating_hours',
                                                           'parking_rates',
                                                           'bike_parking_rates',
                                                           'night_charge',
                                                           'bike_night_charge',
                                                           'lost_ticket',
                                                           'url',
                                                           'url_desc',
							   'ad_text',
							   'simplypark_ad_text',
						           'is_vehicle_text_bold'),
                                           'recursive' => -1
                                           )
                                    );
   return $customTicketFormats;

}
}

