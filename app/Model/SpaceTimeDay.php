<?php
class SpaceTimeDay extends AppModel {

/**
 * Method getSpaceDays to get all the available and unavailable days
 *
 * @param $spaceID int the id of the space
 * @return $spaceDays array containing the week days of the space
 */

	function getSpaceDays($spaceID = null) {
		$spaceDays = $this->find(
							'first',
							array(
									'conditions' => array(
											'SpaceTimeDay.space_id' => $spaceID
										),
									'fields' => array(
											'mon',
											'tue',
											'wed',
											'thu',
											'fri',
											'sat',
											'sun'
										)
								)
						);
		return $spaceDays;
	}
}