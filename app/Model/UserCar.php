<?php
class UserCar extends AppModel {
	public $name = 'UserCar';

	public $validate = array(
            'registeration_number' => array(
                    'registeration_number' => array(
                            'rule'     => array('notEmpty'),
                            'required' => true,
                            'allowEmpty' => false,
                            'message' => 'Registeration number is empty'
                        )/*,
                    'unique' => array(
                        'rule'    => 'isUnique',
                        'required' => 'create',
                        'message' => 'This registeration number already exists',
                    )*/
                )
        );

/**
 * Method getCarRegisterationNumber to get user car's regiksteration number
 *
 * @param $userCarID int the id of the user car
 * @return $getUserCar array containing the car number of the user car
 */
    function getCarRegisterationNumber($userCarID = null) {
        $getUserCar = $this->find(
                            'first',
                            array(
                                    'conditions' => array(
                                            'UserCar.id' => $userCarID
                                        ),
                                    'fields' => array(
                                            'id',
                                            'registeration_number',
                                            'car_type'
                                        )
                                )
                        );
        return $getUserCar;
    }

/**
 * Method getUserCars to get all user cars
 *
 * @param $userID int the id of the user
 * @return $getUserCars array containing all the cars of the user
 */
    function getUserCars($userID = null) {
        $getUserCars = $this->find(
                            'all',
                            array(
                                    'conditions' => array(
                                            'UserCar.user_id' => $userID
                                        ),
                                    'fields' => array(
                                            'id',
                                            'registeration_number'
                                        )
                                )
                        );
        return $getUserCars;
    }

/**
 * Method getOrCreate 
 *
 * @param $userID int the id of the user
 * @param $vehicleReg vehicle registeration number to search
 * @return $user_carId array containing all the cars of the user
 */
    function addUpdateCar($userID,$vehicleReg,$vehicleType) {
        $userCars = $this->find(
                            'first',
                            array(
                                    'conditions' => array(
					    'UserCar.user_id' => $userID,
					    'UserCar.registeration_number' => $vehicleReg
                                        ),
                                    'fields' => array(
                                            'id',
                                        )
                                )
			);

	if(empty($userCars)){
		$newCar = array();
		$newCar['UserCar']['registeration_number'] = $vehicleReg;
		$newCar['UserCar']['user_id'] = $userID;
		$newCar['UserCar']['car_type'] = $vehicleType;
		$newCar['UserCar']['is_deleted'] = 0;
		$newCar['UserCar']['created'] = date('Y-m-d H:i');
		$newCar['UserCar']['modified'] = date('Y-m-d H:i');
                CakeLog::write('debug', 'addUpdateCar...' . print_r($newCar,true));
		if($this->save($newCar)){
		   return $this->getLastInsertId();
                }else{
                   return 0;
                }
	}else{
                CakeLog::write('debug','AddUpdateCar '. print_r($userCars,true));
		return $userCars['UserCar']['id'];
	}

        
    }
}
