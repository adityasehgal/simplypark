<?php
class  Operator extends AppModel {


/**
 * Method getNextOperatorId get slot of a space
 *
 * @param $spaceID int the id of the space
 * @return operatorId , -1 if error
 */

	function getNextOperatorId($spaceID) 
	{
		$numOperators = $this->find('count',
					array(
					     'conditions' => array(
								'Operator.space_id' => $spaceID
					                        ),
							)
	 			  	);


		if($numOperators >= Configure::read('MaxOperators'))
		{
		   return -1;
		}else{
			CakeLog::write('debug','Wrote a new operator with ID ' . $numOperators . ' and spaceID= ' . $spaceID);
			$operators['operator_id'] = $numOperators;
			$operators['space_id'] = $spaceID;
			$operators['create'] = date("Y-m-d H:i");
			$operators['modified'] = date("Y-m-d H:i");
			$this->save($operators);
			CakeLog::write('debug','Wrote a new operator with ID ' . $numOperators);
			return $numOperators; //next id is 1 less than Number of operators.
		}
	}
	
/**
 * Method updateOperatorDesc 
 *
 * @param $operatorId int the id of the operator
 * @param $operatorDesc int the id of the space
 * 
 * @return 
 */

  function updateOperatorDesc($operatorId,$spaceId,$operatorDesc) 
  {
       CakeLog::write('debug','updateOperatorDesc .... ' . $operatorDesc);
	$operator = $this->find('first',
				array(
				     'conditions' => array(
							'Operator.operator_id' => $operatorId,
                                                        'Operator.space_id' => $spaceId,
						),
				      'fields' => array('id')
				     )
				     );

       CakeLog::write('debug','updateOperatorDesc .... ' . print_r($operator,true));
	if(!empty($operator)){
		$this->id = $operator['Operator']['id'];
		$this->saveField('operator_desc',$operatorDesc);
	}

  }
	
/**
 * Method getOperatorDesc 
 *
 * @param $operatorId int the id of the operator
 * 
 * @return  operatorDesc string
 */

  function getOperatorDesc($operatorId,$spaceId) 
  {
	$operator = $this->find('first',
				array(
				     'conditions' => array(
							'Operator.operator_id' => $operatorId,
                                                        'Operator.space_id' => $spaceId
						),
				      'fields' => array('operator_desc')
				     )
				     );

	if(!empty($operator)){
		return $operator['Operator']['operator_desc'];
	}

	return "";

  }

/**
 * Method getAllOperators
 *
 * @param $spaceId int the id of the space for which all operators are desired 
 * 
 * @return  array of operator ID, operatorDesc string
 */

  function getAllOperators($spaceId) 
  {
	$operators = $this->find('list',
				array(
				     'conditions' => array(
							'Operator.space_id' => $spaceId
						),
				      'fields' => array('operator_id','operator_desc')
				     )
				     );

	return $operators;

  }
}
