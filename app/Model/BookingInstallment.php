<?php
class BookingInstallment extends AppModel {
	
	var $belongsTo = array('Booking');
	var $actsAs = array('Containable');

 public $virtualFields = array(
         'total_booking_amount' => 'SUM(amount)',
         'total_handling_charges' => 'SUM(BookingInstallment.internet_handling_charges)',
         'total_owner_income' => 'SUM(BookingInstallment.owner_income)',
);

/**
 * Method getDueInstallments to get all the due installments
 *
 * @return $underApprovalBookings array containing the bookings records only
 */
	function getDueInstallments() {
		$dueInstallments = $this->find(
				'all',
				array(
					'conditions' => array(
                                                'OR' => array(
						'BookingInstallment.date_pay BETWEEN (curdate() - interval 0 day)' . ' and (curdate() + interval '.Configure::read('ReminderInstallmentInterval').' day)',
						'BookingInstallment.date_pay <= NOW()',
                                                ),
						'BookingInstallment.status' => Configure::read('Bollean.False'),
						'BookingInstallment.is_deleted' => Configure::read('Bollean.False'),
						'NOT' => array(
								'Booking.'.Configure::read('BookingTableStatusField') => array(
										Configure::read('BookingStatus.CancellationRequested'),
										Configure::read('BookingStatus.Cancelled')
								)
							)
					),
					'fields' => array(
							'id',
							'date_pay',
							'amount'
					),
					'contain' => array(
						'Booking' => array(
							'User' => array(
								'UserProfile' => array(
									'first_name',
									'last_name'
								),
								'fields' => array(
									'email'
								)
							),
							'Space' => array(
								'User' => array(
									'email'
								),
								'fields' => array(
									'name',
									'flat_apartment_number',
									'address1',
									'address2',
									'post_code'
								)
							),
							'UserCar' => array(
								'registeration_number'
							),
							'fields' => array(
									'id',
									'start_date',
									'end_date'
							)

						)
					)
				)
			);
		return $dueInstallments;
	}

/**
 * Method getBookingIntallment to get booking installment record
 *
 * @param $bookingIntallmentID int the booking installment id
 * @return $getBookingIntallmentRecord array containing the booking installment and booking record
 */
	function getBookingIntallment($bookingIntallmentID) {
		$getBookingIntallmentRecord = $this->find(
											'first',
											array(
													'conditions' => array(
															'BookingInstallment.id' => $bookingIntallmentID,
															'BookingInstallment.status' => Configure::read('Bollean.False'),
															'BookingInstallment.is_deleted' => Configure::read('Bollean.False'),
														),
													'fields' => array(
															'id',
															'booking_id',
															'amount',
															'date_pay',
															'end_date',
															'commission',
															'service_tax',
															'service_tax_by_owner',
															'owner_service_tax_amount',
															'simplypark_service_tax_amount',
															'owner_income',
															'simplypark_income'
														),
													'contain' => array(
															'Booking' => array(
																	'fields' => array(
																			'first_name',
																			'last_name',
																			'mobile',
																			'email',
																			'space_id',
																			'booking_type',
																			'user_id',
																			'space_user_id',
																			'commission',
																			'service_tax',
																			'service_tax_by_owner',
																			'owner_service_tax_amount',
																			'simplypark_service_tax_amount',
																			'owner_income',
																			'simplypark_income'
																		),
																	'Space' => array(
																			'fields' => array(
																					'name'
																				),
																			'City' => array(
																					'name'
																				)
																		),
																	'UserCar' => array(
																			'registeration_number'
																		)
																)
														)
												)
										);
		return $getBookingIntallmentRecord;
	}

/**
 * Method emailBookingIntallmentRecord to get booking installment record
 *
 * @param $bookingIntallmentID int the booking installment id
 * @return $emailBookingIntallmentRecord array containing the booking installment and booking record
 */
function emailBookingIntallmentRecord($bookingIntallmentID) 
{
	$emailBookingIntallmentRecord = $this->find(
		'first',
		array(
			'conditions' => array(
				'BookingInstallment.id' => $bookingIntallmentID
			),
			'fields' => array(
				'id',
				'booking_id',
				'amount'
			),
			'contain' => array(
				'Booking' => array(
					'fields' => array(
						'space_id',
						'start_date',
						'end_date'
					),
					'Space' => array(
						'fields' => array(
							'name'
						),
						'User' => array(
							'email'
						)
					),
					'User' => array(
						'fields' => array(
							'email'
						),
						'UserProfile' => array(
							'first_name',
							'last_name'
						)
					)
				)
			)
		)
	);
	return $emailBookingIntallmentRecord;
}
 
/* Method updateInstallment to set a particular installment
 *
 * @return 
 */
function updateInstallment($bookingId,$nextInstallmentDate,
                               $spIncome,$spServiceTax,
                               $ownerIncome,$ownerServiceTax,
                               $internetHandlingCharges,
                               $amount) 
	{
	   $this->updateAll(
	               array('BookingInstallment.simplypark_income' => $spIncome,
	                     'BookingInstallment.simplypark_service_tax_amount' => $spServiceTax,
	                     'BookingInstallment.owner_income' => $ownerIncome,
	                     'BookingInstallment.owner_service_tax_amount' => $ownerServiceTax,
	                     'BookingInstallment.amount' => $amount,
	                     'BookingInstallment.internet_handling_charges' => $internetHandlingCharges),
	               array('BookingInstallment.booking_id' => $bookingId,
	                     'BookingInstallment.date_pay =' => $nextInstallmentDate)
	   );
	}
 /* Method cancelAllInstallments to set all the installments to cancel
 *
 * @return $cancelAllInstallments cancel all installments for a particular
 *         booking Id
 */
	function cancelAllInstallments($bookingId) 
	{
	   $this->updateAll(
	               array('BookingInstallment.is_deleted' => 1),
	               array('BookingInstallment.booking_id' => $bookingId)
	   );
	}
 
/* Method cancelAllInstallmentsAfterDate to set all the installments to cancel 
 *         falling after a certain date.does not include the passed date
 *
 * @return true 
 */

	function cancelAllInstallmentsAfterDate($bookingId,$Date) 
	{
	   $this->updateAll(
	               array('BookingInstallment.is_deleted' => 1),
	               array('BookingInstallment.booking_id' => $bookingId,
	                     'BookingInstallment.date_pay >' => $Date)
	   );
	}

/* Method cancelAllInstallmentsAfterDateInclusive to set all the installments to cancel 
 *         falling after a certain date. Also includes  Date
 *
 * @return true 
 */
function cancelAllInstallmentsAfterDateInclusive($bookingId,$Date) 
{
   $this->updateAll(
               array('BookingInstallment.is_deleted' => 1),
               array('BookingInstallment.booking_id' => $bookingId,
                     'BookingInstallment.date_pay >=' => $Date)
   );
}
 
/* Method isAnyPreviousInstallmentsUnPaid to return if any previous
 * Installments are due from the date passed 
 *
 * @return true or false
 */
	function isAnyPreviousInstallmentsUnPaid($bookingId,$currentDate) 
	{
	   
	   $installmentsDue = $this->find(
	                                  'all',
	                                  array(
	                                           'conditions' => array('BookingInstallment.booking_id' => $bookingId,
	                                                                 'BookingInstallment.status' => 0,
	                                                                 'BookingInstallment.date_pay <=' => $currentDate
	                                                                ),
	                                            'fields' => array('date_pay',
	                                                              'amount'
	                                                             )
	                                       )
	                                  );


	   return $installmentsDue;
	}

/* Method getNextInstallmentDate to get the next installment date
 * for a particular booking id    
 *
 * @return $nextInstallmentDate
 */
	function getNextInstallmentDate($bookingId) 
	{
	   $nextInstallmentDate = $this->find(
	                                  'first',
	                                  array(
	                                           'conditions' => array('BookingInstallment.booking_id' => $bookingId,
	                                                                 'BookingInstallment.status' => 0
	                                                                ),
	                                           'order' => array('BookingInstallment.date_pay'),
	                                            'fields' => array('date_pay',
	                                                              'amount'
	                                                             )
	                                       )
	                                   );

	   return $nextInstallmentDate;
	}


/**
 * Method getPassedInstallments to get all the installments 
 * of which the installments last date are passed
 *
 *@return $getAllPassedInstallments array containing all the booking installments
 */

	function getPassedInstallments() {
		$getAllPassedInstallments = $this->find(
										'all',
										array(
												'conditions' => array(
														'BookingInstallment.date_pay' => date('Y-m-d',strtotime('-1 days')),
														'BookingInstallment.status' => Configure::read('Bollean.False'),
														'BookingInstallment.is_deleted' => Configure::read('Bollean.False')
													),
												'fields' => array(
														'id',
														'date_pay'
													),
												'contain' => array(
														'Booking' => array(
																'user_id',
																'space_user_id'
															)
													)
											)
									);
		return $getAllPassedInstallments;
	}

/**
 * Method getBookingsToCancel to get all the booking of which the last payment date 
 * on which the au
 *
 */
	function getBookingsToCancel() {
		$getBookingsToCancel = $this->find(
									'all',
									array(
											'conditions' => array(
													'BookingInstallment.date_pay' => date('Y-m-d', strtotime('-'.Configure::read('AutoCancelBookingDuration').' days')),
													'BookingInstallment.status' => Configure::read('Bollean.False'),
													'BookingInstallment.is_deleted' => Configure::read('Bollean.False')
												),
											'fields' => array(
													'booking_id',
													'date_pay'
												)
										)
								);
		return $getBookingsToCancel;
	}

/**
 * Method getTotalBookingAmount to sum up all installments for a particular booking Id to get 
 *  - total booking amount
 *  - total handling charges
 *  - total owner income
 */
function getTotalBookingAmount($bookingId) 
{
	$getBookingAmount = $this->find(
			'all',
			array(
				'conditions' => array(
					'BookingInstallment.booking_id' => $bookingId
					),
				'fields' => array(
					'total_booking_amount',
                                        'total_handling_charges',
                                        'total_owner_income'
					)
			     )
			);
	return $getBookingAmount;
}

/**
 * Method getTotalDueAndPaidInstallments to get all installments due on driver which were paid 
 *
 */
function getTotalDueAndPaidInstallments($bookingId) 
{
	$getDueAndPaidInstallmentAmount = $this->find(

			                        'all',
			                        array(
				                        'conditions' => array(
					                        'BookingInstallment.booking_id' => $bookingId,
                                                                'BookingInstallment.status' => 1
					                        ),
				                        'fields' => array(
					                        'total_booking_amount',
					                        )
			                             )
			                        );
	return $getDueAndPaidInstallmentAmount[0]['BookingInstallment']['total_booking_amount'];
}

/**
 * Method getAllInstallmentsForBookingId to get all installments
 *        for a particular booking Id 
 *
 */

function getAllInstallmentsForBookingId($bookingId)
{
	$allInstallments = $this->find('all',
		                       array(
				              'conditions' => array('BookingInstallment.booking_id' => $bookingId),
				              'fields' => array('date_pay','amount','status')
				             )
			               );
        return $allInstallments;	
}


/*****************************************************************************
 *   UserDashboard related functions
 *
 *
 *
 * ***************************************************************************/
/**
 * Method eInstallments to get all the due installments
 *
 * @return $underApprovalBookings array containing the bookings records only
 /
	function getDueInstallments() {
		$dueInstallments = $this->find(
				'all',
				array(
					'conditions' => array(
                                                'OR' => array(
						'BookingInstallment.date_pay BETWEEN (curdate() - interval 0 day)' . ' and (curdate() + interval '.Configure::read('ReminderInstallmentInterval').' day)',
						'BookingInstallment.date_pay <= NOW()',
                                                ),
						'BookingInstallment.status' => Configure::read('Bollean.False'),
						'BookingInstallment.is_deleted' => Configure::read('Bollean.False'),
						'NOT' => array(
								'Booking.'.Configure::read('BookingTableStatusField') => array(
										Configure::read('BookingStatus.CancellationRequested'),
										Configure::read('BookingStatus.Cancelled')
								)
							)
					),
					'fields' => array(
							'id',
							'date_pay',
							'amount'
					),
					'contain' => array(
						'Booking' => array(
							'User' => array(
								'UserProfile' => array(
									'first_name',
									'last_name'
								),
								'fields' => array(
									'email'
								)
							),
							'Space' => array(
								'User' => array(
									'email'
								),
								'fields' => array(
									'name',
									'flat_apartment_number',
									'address1',
									'address2',
									'post_code'
								)
							),
							'UserCar' => array(
								'registeration_number'
							),
							'fields' => array(
									'id',
									'start_date',
									'end_date'
							)

						)
					)
				)
			);
		return $dueInstallments;
	}*/
}
