<?php
//loading qr code generater library 
require_once(APP . 'Vendor' . DS . 'qrcode' . DS . 'phpqrcode' . DS . 'qrlib.php');

class ReusableToken extends AppModel {

    
    /******************************************************************************
     * Function: getReusableTokens
     * params: spaceId, numTokens append 
     *
     *
     * ***************************************************************************/	

    public function getReusablePrintTokens($spaceId)
    {
	    $tokens = $this->find('list',
		                  array('conditions' => array('ReusableToken.space_id' => $spaceId),
				        'fields' => array('token'))
				);

	    return $tokens;

    }	    

}
