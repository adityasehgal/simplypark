<?php
class SpotterAnnotationResult extends AppModel {

/******************************************************************************
 * Function: saveResult
 * Parameters: $visionApiId, $detectionResult,$categoryScore,$categoryDescripton
 *             $saveFiledPath
 ******************************************************************************/ 
 public function saveResult($visionApiId,$detectionResult,$categoryScore,$categoryDescripton,$blobPath)
 {
   $spotterAnnotationResult = array();
   $id = -1;   

   $spotterAnnotationResult['SpotterAnnotationResult']['api']                = $visionApiId; 
   $spotterAnnotationResult['SpotterAnnotationResult']['result']             = $detectionResult; 
   $spotterAnnotationResult['SpotterAnnotationResult']['vehicle_type_score'] = $categoryScore; 
   $spotterAnnotationResult['SpotterAnnotationResult']['label']              = $categoryDescripton; 
   $spotterAnnotationResult['SpotterAnnotationResult']['blob_path']          = $blobPath; 
   $spotterAnnotationResult['SpotterAnnotationResult']['created']            = date("Y-m-d H:i");
   $spotterAnnotationResult['SpotterAnnotationResult']['modified']           = date("Y-m-d H:i");

   if($this->save($spotterAnnotationResult)){
      $id = $this->getLastInsertId();
      CakeLog::write('debug','Saved SpotterAnotationResult, last ID is ' . $id);
   }

   return $id;
 }	 
}
