<?php
class SyncUpdate extends AppModel {
	public $name = 'SyncUpdate';


/******************************************************************************
 *   Get Current Sync Update
 *
 * ****************************************************************************/	

 function getCurrentSyncId($spaceId)
 {
    $currentSyncIdRow = $this->find(
	                        'first',
			         array('conditions' => array('SyncUpdate.space_id' => $spaceId),
				 'order'=>array('id'=>'DESC') ));


    if(empty($currentSyncIdRow)){
            CakeLog::write('debug','INside getCurrentSyncId current SyncID is 0');
	    return 0;
    }else{
            CakeLog::write('debug','INside getCurrentSyncId ' . print_r($currentSyncIdRow,true));
	    return $currentSyncIdRow['SyncUpdate']['sync_id'];
    }
 }	 
	
	
/******************************************************************************
 *  Get offline_entry_exit_id   
 *
 * ****************************************************************************/	

 function getUpdateIdsToSync($spaceId,$syncId)
 {
    //$limit = Configure::read('maxPullResponse');
    $IdsToSyncList =  $this->find(
	                        'list',
				array('conditions' => array('SyncUpdate.space_id' => $spaceId,
			                                    'SyncUpdate.sync_id >' => $syncId),
				      'fields'=>array('id','update_id','update_type'),
                                      //'limit' => $limit
			             )
			         );
    
    //CakeLog::write('debug',"GetUpdateIdsToSync" . print_r($IdsToSyncList,true));
    return $IdsToSyncList;
 }	 
 
 /******************************************************************************
 *  Add update to the sync Table   
 *
 * ****************************************************************************/	

 function generateSyncId($spaceId,$id,$updateType = 1) /* 1 == Configure::read('UpdateType.offline_entry_exit_update') */
 {
	 $this->create();	 
	 $currentSyncId = $this->getCurrentSyncId($spaceId);
	 $newSyncId = $currentSyncId + 1;
	 $numSaveAttempts = 10;
	 CakeLog::write('debug','In generateSyncId...currentSyncId is ' . $currentSyncId);
	 $syncIdRow = array();
	 $syncIdRow['SyncUpdate']['space_id']     = $spaceId;
	 $syncIdRow['SyncUpdate']['update_id']    = $id;
	 $syncIdRow['SyncUpdate']['sync_id']      = $newSyncId;
	 $syncIdRow['SyncUpdate']['update_type']  = $updateType;
	 $syncIdRow['SyncUpdate']['created']      = date("Y-m-d H:i");
	 $syncIdRow['SyncUpdate']['modified']     = date("Y-m-d H:i");

	 CakeLog::write('debug','In generateSyncId...updatedSyncId is ' . print_r($syncIdRow,true));
	 //Issue113: the spaceId + syncId combination is unique in the sync table. If, due to multithreading,
	 //another thread writes to the sync table, our newSyncId will no longer be valid. In that case, we will
	 //try again. For 10 times
         $itr = 0;
	 while($itr < $numSaveAttempts){
                 CakeLog::write('debug','For spaceID ' . $spaceId . ' trying attempt ' . $itr + 1 . ' with syncId ' . $newSyncId);
		 if(!$this->_generateSyncId($syncIdRow)){
			 $currentSyncId = $this->getCurrentSyncId($spaceId);
			 $newSyncId = $currentSyncId + 1; 
	                 $syncIdRow['SyncUpdate']['sync_id']      = $newSyncId;

		 }else{  
                    break;     
		 }
		 $itr++;
	 }
	 if($itr == $numSaveAttempts){
		 return -1;
	 }else{
		 CakeLog::write('debug','Inside GenerateSyncId .. syncId is ' . $newSyncId);
		 return $newSyncId;
	 } 

 }

private function _generateSyncId($record)
{
   try{
       $this->save($record);
       return true;
    }catch(Exception $e){
       CakeLog::write('debug','Inside _generateSyncId ' . $e->getMessage());
       return false;
    }

}

}
