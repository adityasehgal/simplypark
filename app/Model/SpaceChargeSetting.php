<?php
class SpaceChargeSetting extends AppModel {

/**
 * Method getSpaceChargeSetting to getting the space charge settings
 *
 *
 * @param $spaceID int the id of the space
 * @return $getChargeSetting array containing the information for charging tax and commission
 */
function getSpaceChargeSetting($spaceID = null)
{
	$getChargeSetting = $this->findBySpaceId($spaceID,
			array(
				'charge_service_tax_to_owner',
				'commission_paid_by',
				'commission_percentage',
                                'is_one_time_charge',
                                'max_commission_value'
			     )
			);
	return $getChargeSetting;
}
}
