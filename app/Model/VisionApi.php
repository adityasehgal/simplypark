<?php
class VisionApi extends AppModel {
	public $name = 'VisionApi';

	public $validate = array(
			'apis' => array(
					'type'=> 'isUnique',
					'name',
					'freeCalls',		
					'callsUsed',
					'accuracy'				
			)
	);

	function getType($spaceId) {
		//find preference first
	}
	
	function getTypeWithMaxAccuracy() {		
		
		$getType = $this->find(
				'all',
				array('conditions' => array('MAX(accuracy) as max_accuracy'),
				     'fields' => array('type', 'name') )
				);
		return $getType;
	}

	function getTypeWithMaxFreeCalls() {
	
		$getType = $this->find(
				'all',
				array('conditions' => array('MAX(SUM(freeCalls-callsUsed) as remaining))) as max_free_calls'),
						'fields' => array('type', type) )
				);
		return $getType->type;
	}
	
	function getTypeWithMaxAccuracyAndMaxFreeCalls() {
	
		$params['conditions'] = array(
				'accuracy' => 'MAX(accuracy)',
				'AND'=>array('MAX(SUM(freeCalls-callsUsed) as remaining))) as max_free_calls'));
		$getType = $this->find(
				'all',
				array('conditions' => $params['conditions'],
			    'fields' => array('type', 'name') )
				);
		return $getType;
	}
	
	
}
