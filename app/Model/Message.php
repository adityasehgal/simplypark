<?php
class Message extends AppModel {
	public $name = 'Message';
	
/**
 * Method getMessage to get the message details
 *
 * @param $messageID int the id of the message
 * @return $messageDetail array containing the details of the message
 *
 */
	function getMessage($messageID) {
		$messageDetail = $this->findById(
                                $messageID,
                                array(
                                        'body'
                                    )
                            );
		return $messageDetail;
	}
}