<?php
class WithdrawalSources extends AppModel {
	public $belongsTo = array('City', 'State');

    public $validate = array(
                'account_first_name' => array(
                        'required' => array(
                            'rule' => array('notEmpty'),
                            'message' => 'Account Holder First Name is empty.'
                        ),
                ),
                'account_number' => array(
                        'required' => array(
                            'rule' => array('notEmpty'),
                            'message' => 'Account Number is empty.'
                        ),
                ),
                'branch_code' => array(
                        'required' => array(
                            'rule' => array('notEmpty'),
                            'message' => 'Branch Code is empty.'
                        ),
                ),
                'ifsc_code' => array(
                        'required' => array(
                            'rule' => array('notEmpty'),
                            'message' => 'IFSC is empty.'
                        ),
                ),
                'address' => array(
                        'required' => array(
                            'rule' => array('notEmpty'),
                            'message' => 'Address is empty.'
                        ),
                ),
                'postal_code' => array(
                        'required' => array(
                            'rule' => array('notEmpty'),
                            'message' => 'Postal Code empty.'
                        ),
                ),
                'state_id' => array(
                        'required' => array(
                            'rule' => array('notEmpty'),
                            'message' => 'Select State.'
                        ),
                ), 
                'bank_name' => array(
                        'required' => array(
                            'rule' => array('notEmpty'),
                            'message' => 'Bank Name is empty.'
                        ),
                ),
    );
}