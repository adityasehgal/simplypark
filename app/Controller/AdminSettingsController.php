<?php
class AdminSettingsController extends AppController
{
	public $helper = array('Html', 'Form');
	public $components = array('Paginator', 'RequestHandler');

	public $uses = array('SpaceType', 'PropertyType', 'Facility', 'CmsPage', 'DiscountCoupon', 'Partner', 'ServiceTaxCharges');

	public function beforeFilter() {
		parent::beforeFilter();
		if ($_SERVER["REMOTE_ADDR"] != Configure::read('Server.Local') && $_SERVER['SERVER_NAME'] == Configure::read('Server.Live')) {
			$this->Security->unlockedActions = array('admin_addupdateDiscountCoupon','admin_addFreeSpace');
		}
	}

/**
 * Method admin_index to display all created spaces
 *
 * @return void 
 */
	public function admin_index() {
		$this->layout = 'backend';

		$conditions = array();
		if (isset($this->request->query) && !empty($this->request->query)) {
			$searchData = array(
					'SpaceType.name LIKE' => '%'. $this->request->query['search'] .'%'
				);
			$conditions = array_merge($conditions, $searchData);
		}

		$this->Paginator->settings = array(
										'conditions' => array($conditions),
										'limit' => 10,
										'order' => 'SpaceType.id Asc'
									);
		$spaces = $this->Paginator->paginate('SpaceType');
		$this->set('spaces',$spaces);
		if ($this->request->is('ajax')) {
			$this->layout = '';
			$this->autoRender = false;
			$this->viewPath = 'Elements' . DS . 'backend' . DS . 'AdminSettings';
			$this->render('listing');
		}
	}

/**
 * Method admin_index to display all created spaces
 *
 * @return void 
 */
	public function admin_showSimplyParkSpaces() {
		$this->layout = 'backend';

		$conditions = array('SpaceType.is_avail_with_simplypark' => 1);
		if (isset($this->request->query) && !empty($this->request->query)) {
			$searchData = array(
					'SpaceType.name LIKE' => '%'. $this->request->query['search'] .'%'
				);
			$conditions = array_merge($conditions, $searchData);
		}

		$this->Paginator->settings = array(
										'conditions' => array($conditions),
										'limit' => 10,
										'order' => 'SpaceType.id Asc'
									);
		$spaces = $this->Paginator->paginate('SpaceType');
		$this->set('spaces',$spaces);
		if ($this->request->is('ajax')) {
			$this->layout = '';
			$this->autoRender = false;
			$this->viewPath = 'Elements' . DS . 'backend' . DS . 'AdminSettings';
			$this->render('listing');
		}
	}


/**
 * Method admin_addSpace used to save new space in table
 *
 * @return void 
 */
	public function admin_addUpdateSpace() {
		$this->request->allowMethod('post','put');
		if ($this->SpaceType->save($this->request->data)) {

			$this->Session->setFlash(__('Space type has been added successfully'), 'default', 'success');

			if (isset($this->request->data['SpaceType']['id'])) {
				$this->Session->setFlash(__('Space type has been updated successfully'), 'default', 'success');
			}
			$this->redirect($this->referer());
		}
		$errors = $this->SpaceType->validationErrors;
		if (!empty($errors)) {
			$errorMsg = $this->_setValidaiotnError($errors);
		}
		$this->Session->setFlash(__('Add space type request not completed due to following errors: <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
		if (isset($this->request->data['SpaceType']['id'])) {
			$this->Session->setFlash(__('Update space type request not completed due to following errors: <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
		}
		$this->redirect($this->referer());
	}

/**
 * Method admin_ajaxGetSpaceData to get space data from id while updating space record
 *
 * @return void 
 */
	public function admin_ajaxGetSpaceData($spaceTypeID = null) {
		$spaceTypeID = urldecode(base64_decode($spaceTypeID));
		$getSpaceData = $this->SpaceType->findById($spaceTypeID, 
			array(
				'id', 'name'
				)
			);
		$this->set(
				array(
					'response' => $getSpaceData,
					'_serialize' => 'response'
				)
			);
	}

/**
 * Method admin_listProperty to get all properties added
 *
 * @return void 
 */
	public function admin_listProperty() {
		$this->layout = 'backend';

		$conditions = array();
		if (isset($this->request->query) && !empty($this->request->query)) {
			$searchData = array(
					'PropertyType.name LIKE' => '%'. $this->request->query['search'] .'%'
				);
			$conditions = array_merge($conditions, $searchData);
		}

		$this->Paginator->settings = array(
										'conditions' => array($conditions),
										'limit' => 10,
										'order' => 'PropertyType.id Asc'
									);
		$getProperties = $this->Paginator->paginate('PropertyType');
		$this->set('getProperties',$getProperties);
		if ($this->request->is('ajax')) {
			$this->layout = '';
			$this->autoRender = false;
			$this->viewPath = 'Elements' . DS . 'backend' . DS . 'AdminSettings' . DS . 'Property';
			$this->render('listing');
		}
	}

/**
 * Method admin_addUpdateProperty used to save new property in table
 *
 * @return void 
 */
	public function admin_addUpdateProperty() {
		$this->request->allowMethod('post','put');
		if ($this->PropertyType->save($this->request->data)) {

			$this->Session->setFlash(__('Property type has been added successfully'), 'default', 'success');

			if (isset($this->request->data['PropertyType']['id'])) {
				$this->Session->setFlash(__('Property type has been updated successfully'), 'default', 'success');
			}
			$this->redirect($this->referer());
		}
		$errors = $this->PropertyType->validationErrors;
		if (!empty($errors)) {
			$errorMsg = $this->_setValidaiotnError($errors);
		}
		$this->Session->setFlash(__('Add property type request not completed due to following errors: <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
		if (isset($this->request->data['Space']['id'])) {
			$this->Session->setFlash(__('Update property type request not completed due to following errors: <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
		}
		$this->redirect($this->referer());
	}

/**
 * Method admin_ajaxGetPropertyData to get property data from id while updating space record
 *
 * @return void 
 */
	public function admin_ajaxGetPropertyData($propertyID = null) {
		$propertyID = urldecode(base64_decode($propertyID));
		$getPropertyData = $this->PropertyType->findById($propertyID, 
			array(
				'id', 'name'
				)
			);
		$this->set(
				array(
					'response' => $getPropertyData,
					'_serialize' => 'response'
				)
			);
	}

/**
 * Method admin_listFacilities to get all facilities added
 *
 * @return void 
 */
	public function admin_listFacilities() {
		$this->layout = 'backend';

		$conditions = array();
		if (isset($this->request->query) && !empty($this->request->query)) {
			$searchData = array(
					'Facility.name LIKE' => '%'. $this->request->query['search'] .'%'
				);
			$conditions = array_merge($conditions, $searchData);
		}

		$this->Paginator->settings = array(
										'conditions' => array($conditions),
										'limit' => 10,
										'order' => 'Facility.id Asc'
									);
		$getFacilities = $this->Paginator->paginate('Facility');
		$this->set('getFacilities',$getFacilities);
		if ($this->request->is('ajax')) {
			$this->layout = '';
			$this->autoRender = false;
			$this->viewPath = 'Elements' . DS . 'backend' . DS . 'AdminSettings' . DS . 'Facility';
			$this->render('listing');
		}
	}

/**
 * Method admin_addUpdateFacility used to save and update new facility in table
 *
 * @return void 
 */
	public function admin_addUpdateFacility() {
		$this->request->allowMethod(['post','put']);
		if ($this->Facility->save($this->request->data)) {

			$this->Session->setFlash(__('Facility has been added successfully'), 'default', 'success');

			if (isset($this->request->data['Facility']['id'])) {
				$this->Session->setFlash(__('Facility has been updated successfully'), 'default', 'success');
			}
			$this->redirect($this->referer());
		}
		$errors = $this->Facility->validationErrors;
		if (!empty($errors)) {
			$errorMsg = $this->_setValidaiotnError($errors);
		}
		$this->Session->setFlash(__('Add facility request not completed due to following errors: <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
		if (isset($this->request->data['Space']['id'])) {
			$this->Session->setFlash(__('Update facility request not completed due to following errors: <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
		}
		$this->redirect($this->referer());
	}

/**
 * Method admin_ajaxGetFacilityData to get facility data from id while updating space record
 *
 * @return void 
 */
	public function admin_ajaxGetFacilityData($facilityID = null) {
		$facilityID = urldecode(base64_decode($facilityID));
		$getFacilityData = $this->Facility->findById($facilityID, 
			array(
				'id', 'name', 'icon'
				)
			);
		$path['path'] = Configure::read('ROOTURL').'files/FacilityIcon/';
		$getFacilityData = array_merge($getFacilityData, $path);
		$this->set(
				array(
					'response' => $getFacilityData,
					'_serialize' => 'response'
				)
			);
	}

/**
 * Method admin_listCmsPages to view all the cms pages of the website
 *
 * @return void 
 */
	public function admin_listCmsPages() {
		$this->layout = 'backend';
		$getAllCmsPages = $this->CmsPage->find('all');
		$this->set('getAllCmsPages', $getAllCmsPages);
	}

/**
 * Method admin_editCms to save upadted records
 *
 * @return void 
 */
	public function admin_editCms($cmsID=null) {
		if(isset($this->request->data) && !empty($this->request->data)) {
			if ($this->CmsPage->save($this->request->data)) {
				$this->Session->setFlash(__('Cms page updated successfully.'), 'default', 'success');
				$this->redirect(array('action' => 'listCmsPages', 'admin' => true));
			} else {
				$errors = $this->CmsPage->validationErrors;
	            if (!empty($errors)) {
	                $errorMsg = $this->_setValidaiotnError($errors);
	            }
				$this->Session->setFlash(__('Cms page update request not completed due to following errors: <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
			}
			$this->redirect($this->referer());
		}
		$this->layout = 'backend';
		$cmsID = base64_decode($cmsID);
		$cms = $this->CmsPage->findById($cmsID, array(
			'id','meta_title', 'meta_description', 'meta_keyword', 'page_name', 'page_title'));
		//pr($cms);die;
		$this->set('cms',$cms);
	}

/**
 * Method to list Discount Coupons.
 *
 */
	public function admin_listDiscountCoupons() {
		$this->layout = 'backend';

		$conditions = array();
		if (isset($this->request->query) && !empty($this->request->query)) {
			$searchData = array(
					'DiscountCoupon.name LIKE' => '%'. $this->request->query['search'] .'%'
				);
			$conditions = array_merge($conditions, $searchData);
		}

		$this->Paginator->settings = array(
										'conditions' => array($conditions),
										'limit' => 100,
										'order' => 'DiscountCoupon.id Asc'
									);
		$discount_coupons = $this->Paginator->paginate('DiscountCoupon');
		$path['path'] = Configure::read('ROOTURL');
		$discount_coupons = array_merge($discount_coupons, $path);
                $propertyTypeList = $this->_getPropertyTypeList();
                
                $propertyTypeList[0] = 'All';
                $this->set('property_type_list', $propertyTypeList);

		$this->set('discount_coupons',$discount_coupons);
		if ($this->request->is('ajax')) {
			$this->layout = '';
			$this->autoRender = false;
			$this->viewPath = 'Elements' . DS . 'backend' . DS . 'AdminSettings' . DS . 'ListDiscountCoupons';
			$this->render('listing');
		}
	}

/**
 * Method to details of single Discount Coupon.
 *
 */
	public function admin_ajaxGetDiscountCouponData($discount_coupon_id = null) {
		$data = array();
		$discount_coupon_id = urldecode(base64_decode($discount_coupon_id));
		$getDiscountCouponData = $this->DiscountCoupon->findById($discount_coupon_id, 
			                                                 array(
				                                                 'id', 'name', 'description', 'code', 'for_user_type', 'discount_type', 'flat_amount',
				                                                 'discount_percentage', 'max_amount_discount',
				                                                 'is_one_time_use','is_for_bulk_booking','property_type_id'
				                                               )
			                                                );
		$data['user_type'] = Configure::read('coupon_users');
		$data['discount_type'] = Configure::read('discount_type');
		$data['DiscountCoupon'] = $getDiscountCouponData['DiscountCoupon'];
                $propertyTypeList = $this->_getPropertyTypeList();
                
                $propertyTypeList[0] = 'All';
                $this->set('property_type_list', $propertyTypeList);
	        $data['property_type_list'] = $propertyTypeList;
	
                $this->set(
				array(
					'response' => $data,
					'_serialize' => 'response'
				)
			);
	}

/**
 * Method to save, upadted Discount Coupon.
 *
 */
	public function admin_addupdateDiscountCoupon() {
		$this->request->allowMethod('post','put');
		$data['created_by_user_id'] = $this->Auth->user('id');

                //CakeLog::write('debug','In addupdateDiscountCoupon ...' . print_r($this->request->data,true));
		$this->request->data = array_merge($this->request->data['DiscountCoupon'], $data);
		
		if ($this->DiscountCoupon->save($this->request->data)) {

			$this->Session->setFlash(__('Discount Coupon has been added successfully'), 'default', 'success');

			if (isset($this->request->data['DiscountCoupon']['id'])) {
				$this->Session->setFlash(__('Discount Coupon has been updated successfully'), 'default', 'success');
			}
			$this->redirect($this->referer());
		}
		$errors = $this->DiscountCoupon->validationErrors;
		if (!empty($errors)) {
			$errorMsg = $this->_setValidaiotnError($errors);
		}
		$this->Session->setFlash(__('Add Discount Coupon request not completed due to following errors: <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
		if (isset($this->request->data['DiscountCoupon']['id'])) {
			$this->Session->setFlash(__('Update Discount Coupon request not completed due to following errors: <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
		}
		$this->redirect($this->referer());
	}

	public function admin_listPartners() {
		$this->layout = 'backend';

		$conditions = array();
		$conditions = array(
					'Partner.is_deleted' => 0,
				);
		if (isset($this->request->query) && !empty($this->request->query)) {
			$searchData = array(
					'Partner.name LIKE' => '%'. $this->request->query['search'] .'%'
				);
			$conditions = array_merge($conditions, $searchData);
		}

		$this->Paginator->settings = array(
										'conditions' => array($conditions),
										'limit' => 10,
										'order' => 'Partner.id Asc'
									);
		$partners = $this->Paginator->paginate('Partner');
		if(!empty($partners)) {
			$path['path'] = Configure::read('ROOTURL').'files/partners_logo/';
			$partners = array_merge($partners, $path);
		}
		$this->set('partners',$partners);
		if ($this->request->is('ajax')) {
			$this->layout = '';
			$this->autoRender = false;
			$this->viewPath = 'Elements' . DS . 'backend' . DS . 'AdminSettings' . DS . 'Partners';
			$this->render('listing');
		}
	}

	public function admin_addupdatePartner() {
        if ($this->Partner->save($this->request->data)) {

			$this->Session->setFlash(__('Partner has been added successfully'), 'default', 'success');

			if (isset($this->request->data['Partner']['id'])) {
				$this->Session->setFlash(__('Partner has been updated successfully'), 'default', 'success');
			}
			$this->redirect($this->referer());
		}

		$errors = $this->Partner->validationErrors;
		if (!empty($errors)) {
			$errorMsg = $this->_setValidaiotnError($errors);
		}
		$this->Session->setFlash(__('Add Partner request not completed due to following errors: <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
		if (isset($this->request->data['Partner']['id'])) {
			$this->Session->setFlash(__('Update Partner request not completed due to following errors: <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
		}
		$this->redirect($this->referer());
	}

	public function admin_ajaxGetPartnerData($partner_id = null) {
		$partner_id = urldecode(base64_decode($partner_id));
		$getPartnerData = $this->Partner->findById($partner_id, 
			array(
				'id', 'name', 'logo'
				)
			);
		$path['path'] = Configure::read('ROOTURL').'files/partners_logo/';
		$getPartnerData = array_merge($getPartnerData, $path);
		$this->set(
				array(
					'response' => $getPartnerData,
					'_serialize' => 'response'
				)
			);
	}

	public function admin_setDiscountFront() {
		$this->autoRender = false ;
		$data = array();
		$discount_coupon_id = $this->request->data['result']['coupon_id'];

		$frontEndActivatedCouponID = $this->DiscountCoupon->getCouponSetFrontend();
		if (!$this->DiscountCoupon->checkIsActivated($discount_coupon_id)) {
			$data['status'] = 'error';
			$data['msg'] = 'Please activate this discount coupon first.';
			$data['frontendsetcoupon'] = $frontEndActivatedCouponID;
			return json_encode($data);
		}

		$this->DiscountCoupon->updateAll(
		    array('DiscountCoupon.is_frontend' => 0),
		    array('DiscountCoupon.is_frontend' => 1)
		);

		$data['status'] = 'error';
		$data['msg'] = 'Unable to set Discount Coupon.';
		$data['frontendsetcoupon'] = $frontEndActivatedCouponID;

		$save_data = array(
			'is_frontend' => 1
			);
		$this->DiscountCoupon->id = $discount_coupon_id;

		if($this->DiscountCoupon->save($save_data)) {
			$data['status'] = 'sucess';
			$data['msg'] = 'Discount Coupon set successfully.';
		}

		return json_encode($data);
	}

	public function admin_serviceTaxCharge() {
		$this->layout = 'backend';
		$data = array();

		$service_tax_charges_data = $this->ServiceTaxCharges->find('first',array(
						'fields' => array(
							'ServiceTaxCharges.id',
							'ServiceTaxCharges.commission_paid_by',
							'ServiceTaxCharges.commission_percentage',
							'ServiceTaxCharges.service_tax_paid_by_simplypark',
							'ServiceTaxCharges.service_tax_percentage',
						)
					)
				);

		if(!empty($service_tax_charges_data)) {
			$data['service_tax_charges_data'] = $service_tax_charges_data['ServiceTaxCharges'];
		}

		if(isset($this->request->data['ServiceTaxCharges'])) {
			if(empty($this->request->data['ServiceTaxCharges']['service_tax_percentage'])) {
				$this->request->data['ServiceTaxCharges']['service_tax_percentage'] = Configure::read('SimplyParkServiceTax');
			}

			if ($this->ServiceTaxCharges->save($this->request->data)) {
    			
    			if (isset($this->request->data['ServiceTaxCharges']['id'])) {
					$this->Session->setFlash(__('Service Tax/Charges Details has been updated successfully'), 'default', 'success');
				}
				$this->redirect($this->referer());
    		} 
			$errors = $this->ServiceTaxCharges->validationErrors;
			if (!empty($errors)) {
				$errorMsg = $this->_setValidaiotnError($errors);
			}
			$this->Session->setFlash(__('Your Request could not completed due to following error : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
			$this->redirect($this->referer());
		}

		if(!empty($data)) {
			$this->set('data',$data);
		}
	}

	public function admin_ajaxDisableCouponFront() {
		$this->autoRender = false ;
		if($this->DiscountCoupon->updateAll(
		    array('DiscountCoupon.is_frontend' => 0),
		    array('DiscountCoupon.is_frontend' => 1)
			)
		) {
			return $this->Session->setFlash(__('Discount Coupon Removed from frontend.'), 'default', 'success');
		} else {
			return $this->Session->setFlash(__('Unable to complete your request. Error Occured.'), 'default', 'error');
		}
	}
}
