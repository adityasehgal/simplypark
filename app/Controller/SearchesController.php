<?php
App::uses('HttpSocket', 'Network/Http');
class SearchesController extends AppController
{
	public $helper = array('Html', 'Form');
	public $components = array('RequestHandler');

	public $uses = array('Space');

	public function beforeFilter() {
		parent::beforeFilter();

		$this->Auth->allow(
				'index',
				'sendTagsEmail',
				'ajaxGetGatedSpace'
			);
	}

/**
 * Method index to show the landing page of the website
 *
 * @return void 
 */
	public function index() {
		$searchTerm = $this->request->query['term'];
		$searchedRecords = array();
		$this->loadModel('Space');
		$getSpaces = $this->Space->getSpaces($searchTerm);//Getting spaces for searched parametter
		if (!empty($getSpaces)) {
			$searchedRecords = array_merge($searchedRecords,$getSpaces);
		}
		// pr($getSpaces);
		$this->loadModel('EventAddress');
		$getEvents = $this->EventAddress->getSearchedEvents($searchTerm);//Getting event venues for searched parametter
		if (!empty($getEvents)) {
			$searchedRecords = array_merge($searchedRecords,$getEvents);
		}
		
		if (empty($searchedRecords)) {
			$searchedRecords = array('error');
		}
		// pr($searchedRecords);die;
		$this->set(
				array(
						'response' => $searchedRecords,
						'_serialize' => 'response'
					)
			);
	}



/**
 * Method sendTagsEmail to send email to the user who didn't get the search record according to the tag the user searched
 *
 * @return void
 */
	public function sendTagsEmail() {
		$this->request->allowMethod('post','put');

		$this->loadModel('EmailTemplate');

		$temp = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => Configure::read('EmailTemplateId.search_not_found'))));
	   	$temp['EmailTemplate']['mail_body'] = str_replace(
	    	array('../../..', '#EMAIL', '#TAG'),
	    	array(FULL_BASE_URL, $this->request->data['Search']['email'], $this->request->data['Search']['keyword']), $temp['EmailTemplate']['mail_body']);
		
		$this->_sendEmailMessage(Configure::read('Email.EmailAdmin'), $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject']);

		$this->_sendMessage();

		$this->Session->setFlash(__('Your request has been submitted successfully. Our team will response you soon if any search records mathching with your keyword be availabel in the SimplyPark.'), 'default', 'success');
		$this->redirect($this->referer());
	}

/**
 * Method _sendMessage to send message to user if their string does not match any space information
 *
 * @return bool
 */
	protected function _sendMessage() {
		if (empty($this->request->data['Search']['mobile'])) {
			$this->loadModel('User');
			$mobileNumber = $this->User->getUserInfo($this->request->data['Search']['email']); //getting user mobile number

			if (empty($mobileNumber)) {
				return false;
			}

			$this->request->data['Search']['mobile'] = $mobileNumber['UserProfile']['mobile'];
			$this->request->data['Search']['name'] = $mobileNumber['UserProfile']['first_name'].' '.$mobileNumber['UserProfile']['last_name'];
		}

		$this->loadModel('Message');
		//getting message body
        $messageContent = $this->Message->getMessage(Configure::read('MessageId.booking_request_noted'));

		$messageContent['Message']['body'] = str_replace(
												'#USER',
												$this->request->data['Search']['name'],
												$messageContent['Message']['body']
											);

        $messageResponse = $this->_sendMessageNotification($this->request->data['Search']['mobile'],$messageContent['Message']['body']);

        if ($messageResponse->code == Configure::read('HTTPStatusCode.OK')) {
            return true;
        }
        return false;

	}

	public function ajaxGetGatedSpace($search_string = null) {
		if($search_string != null) {
			$conditions = array(
	                            'Space.is_completed' => Configure::read('Bollean.True'),
	                            'Space.is_activated' => Configure::read('Bollean.True'),
	                            'Space.is_approved' => Configure::read('Bollean.True'),
	                            'Space.is_deleted' => Configure::read('Bollean.False'),
	                            
	                            'Space.property_type_id' => Configure::read('gated_community')
	                        );
			$searchData = array(
                'OR' => array(
                    'Space.name LIKE' => '%'. $search_string .'%',
                     'Space.search_string' => $search_string,
                    'Space.flat_apartment_number LIKE' => '%'. $search_string .'%',
                    'Space.address1 LIKE' => '%'. $search_string .'%',
                    'Space.address2 LIKE' => '%'. $search_string .'%',
                    'Space.search_tags LIKE' => '%'. $search_string .'%',       
                    'SpaceType.name LIKE' => '%'. $search_string .'%',
                    'PropertyType.name LIKE' => '%'. $search_string .'%',
                    'State.name LIKE' => '%'. $search_string .'%',
                    'City.name LIKE' => '%'. $search_string .'%'
                    )
                );
			$conditions = array_merge($conditions, $searchData);
	        $options['joins'] = array(
                                array('table' => 'states',
                                        'alias' => 'State',
                                        'conditions' => array(
                                            'State.id = Space.state_id'
                                        )
                                    ),
                                array('table' => 'space_types',
                                        'alias' => 'SpaceType',
                                        'type' => 'left',
                                        'conditions' => array(
                                            'SpaceType.id = Space.space_type_id'
                                        )
                                    ),
                                array('table' => 'property_types',
                                        'alias' => 'PropertyType',
                                        'type' => 'left',
                                        'conditions' => array(
                                            'PropertyType.id = Space.property_type_id'
                                        )
                                    ),
                                array('table' => 'cities',
                                        'alias' => 'City',
                                        'type' => 'left',
                                        'conditions' => array(
                                            'City.id = Space.city_id'
                                        )
                                    ),
                                
                            );
		$options['contain'] = 'SpacePricing';
	        $options['order'] = 'Space.created Desc';
	        $options['recursive'] = -1;
	        $options['fields'] = array(
	                                'Space.id',
	                                'Space.name',
	                                'Space.search_string',
	                                'Space.tower_number',
	                                'Space.place_pic',
				);
	        $options['conditions'] = $conditions;
		$getSpaces['gatedSpace'] = $this->Space->find('all',$options);
		//CakeLog::write('debug','Inside gatecommunity ...' . print_r($getSpaces,true));
	        $getSpaces['count'] = count($getSpaces['gatedSpace']);
    	}
    	//pr($getSpaces);
		$this->set(
				array(
					'response' => $getSpaces,
					'_serialize' => 'response'
				)
			);
	}
}
