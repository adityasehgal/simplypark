<?php

App::uses('Component', 'Controller');

App::build(array('Vendor' => array(APP . 'Vendor' . DS . 'Vision' . DS)));
App::uses('GoogleVisionApi', 'Vendor');

#App::import('Vendor', 'Vision', array('file' => 'Vision/GoogleVisionApi.php'));

#require_once(APP .'Vendor' . DS  . 'Vision' . DS . 'GoogleVisionApi.php');



class VisionApiFactoryComponent extends Component
{
	
	public function detect($type, $input, &$output)
	{
		if($type == 1 ) {
			$visionApi =  new GoogleVisionApi();
		} else if($type == 2) {
			$visionApi =  new MsVisionApi();
		} else {
			return false;
		}
   	    return $visionApi->detect($input, $output);

	}	
	
}
?>