<?php
// App::uses('User', 'View/Helper'); 
class AdminsController extends AppController
{
	public $helper = array('Html', 'Form', 'Js', 'Admin', 'UserData');
	public $components = array('RequestHandler', 'Paginator');
	public $uses = array('User','UserCar','UserAddress','UserProfile','Space','Booking');

	public function beforeFilter() {
		parent::beforeFilter();
		
		$this->Auth->allow('admin_subAdminActivateAccount', 'admin_forgotPassword', 'admin_resetPassword');
		$this->_loginWithCookie();
	}

/**
 * Method admin_index get the list of all sub admins
 *
 * @return void 
 */
	public function admin_index() {
		if (!$this->_checkIfAdminOrUser()) {
			$this->redirect(array('action' => 'dashboard'));
		}
		$this->layout = 'backend';
		$conditions = array(
			'User.user_type_id' => configure::read('UserTypes.SubAdmin')
			);
		if (isset($this->request->query) && !empty($this->request->query)) {
			$searchData = array(
				'OR' => array(
					'UserProfile.first_name LIKE' => '%'. $this->request->query['search'] .'%',
					'UserProfile.last_name LIKE' => '%'. $this->request->query['search'] .'%',
					'User.email LIKE' => '%'. $this->request->query['search'] .'%',
					'UserProfile.mobile LIKE' => '%'. $this->request->query['search'] .'%',
					)
				);
			$conditions = array_merge($conditions, $searchData);
		}
		
		$this->Paginator->settings = array(
										'conditions' => $conditions,
										'limit' => 10,
										'order' => 'User.created Desc',
										'fields' => array('id', 'username', 'email', 'is_activated', 'is_deleted'),
										'contain' => array(
											'UserProfile' => array(
												'fields' => array(
													'id', 'first_name' ,'last_name', 'mobile'
													)
												)
											)
									);
		$subAdmins = $this->Paginator->paginate('User');
		$this->set('subAdmins',$subAdmins);
		if ($this->request->is('ajax')) {
			$this->layout = '';
			$this->autoRender = false;
			$this->viewPath = 'Elements' . DS . 'backend' . DS . 'Admin';
			$this->render('listing');
		}
	}

/**
 * Method admin_login for admin login
 *
 * @return void
 */
	public function admin_login() {
		if ($this->Auth->loggedIn()) {
			$this->_redirectAfterLogin();
		}

		$this->layout = 'backend_login';
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {

				if ($this->Auth->user('is_activated') == Configure::read('Bollean.False') || 
					$this->Auth->user('is_deleted') == Configure::read('Bollean.True')) {
					// user is not activated or deleted

					$this->Session->setFlash(
						__('Your account is blocked by adminsitrator.'),
						'default',
						'error'
					);
					return $this->redirect($this->Auth->logout());
				}

				$userTypes = array(
					Configure::read('UserTypes.Admin'),
					Configure::read('UserTypes.SubAdmin')
				);

				if (!$this->_checkUserType($userTypes)) { // check if logged in user is not a front end user
					$this->Session->setFlash(
						__('you are not authorized to access this location.'),
						'default',
						'error'
					);
					return $this->redirect($this->Auth->logout());
				}

				if ($this->data['User']['keep_me_logged_in'] == false) {
					$this->Cookie->delete('User');
				}

				$cookie = array(
					'email' => $this->Session->read('Auth.User.email'),
					'persist_code' => $this->Session->read('Auth.User.persist_code')
				);
				$this->Cookie->httpOnly = true;
				$this->Cookie->write('User', $cookie, true, '7 Days');
				return $this->redirect($this->Auth->redirectUrl());
			}
			$this->Session->setFlash(
				__('Invalid Username or Password'),
				'default',
				'error'
			);
		}
	}

/**
 * Method admin_logout for admin logout
 *
 * @return void
 */
	public function admin_logout() {
		$this->Cookie->delete('User');
		return $this->redirect($this->Auth->logout());
	}
	
/**
 * Method admin_change_password for change admin password
 *
 * @return void
 */
	public function admin_change_password() {
		$this->request->allowMethod('post','put');
		$this->_changePassword();
	}
	
/**
 * Method admin_getUserData get data to show in the edit profile popup
 *
 * @return void
 */
	public function admin_ajaxGetUserData($UserID=null) {
		$UserID = base64_decode($UserID);
		$getUserData = $this->User->find('first',array(
				'conditions' => array(
					'User.id' => $UserID 
				),
				'fields' => array(
					'id', 'email'
				),
				'contain' => array(
					'UserProfile' => array(
						'fields' => array(
							'id', 'first_name', 'last_name', 'mobile'
						)
					)
				)
			)
		);
		$this->set(
			array(
				'response' => $getUserData,
				'_serialize' => 'response'
				)
			);
	}
	
/**
 * Method admin_edit_profile for edit admin profile
 *
 * @return void
 */
	public function admin_edit_profile() {
		$this->request->allowMethod('post','put');

		$this->User->unvalidate(array('username', 'old_password','password','confirm_password'));
		$this->UserProfile->unvalidate(array('company'));

		if ($this->User->saveAssociated($this->request->data)) {
			$this->Session->setFlash(__('Profile updated successfully.'), 'default', 'success');
			$this->redirect($this->referer());
		}
		$errors = $this->User->validationErrors;
		if (!empty($errors)) {
			$errorMsg = $this->_setSaveAssociateValidationError($errors);
		}
		$this->Session->setFlash(__('Edit profile request not completed due to following : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
		$this->redirect($this->referer());
	}
	
/**
 * Method admin_forgotPassword to send temporary password to user's email
 *
 * @return void
 */
	public function admin_forgotPassword() {
		$this->request->allowMethod('post','put');
		$this->_forgotPassword();
	}
	
/**
 * Method admin_resetPassword to reset admin user password
 *
 * @return void
 */
	public function admin_resetPassword($persistCode = null) {
		
		if ($this->request->is('post')) {
			$this->User->unvalidate(array('username', 'email', 'old_password'));
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('Your Password has been changed successfully. You can login to continue'), 'default', 'success');
				$this->redirect(array('action' => 'login', 'admin' => true));
			} else {
				$errors = $this->User->validationErrors;
            if (!empty($errors)) {
                $errorMsg = $this->_setValidaiotnError($errors);
            }
            $this->Session->setFlash(__('Password reset request not completed due to following error : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
			}
		}
		
		$this->layout = 'backend_login';
		$persistCodeEnc = urldecode(base64_decode($persistCode));
		
		$getUserID = $this->User->findByPersistCode($persistCodeEnc, array('id'));
		if (empty($getUserID)) {
			$this->redirect(array('action' => 'login', 'admin' => true));
		}
		$this->set('userID',$getUserID['User']['id']);
	}
	
/**
 * Method admin_add_subadmin for add new sub admin
 *
 * @return void
 */
	public function admin_addSubadmin() {
		$this->request->allowMethod('post','put');

		$this->User->unvalidate(array('old_password'));
		$this->UserProfile->unvalidate(array('profile_pic','company'));
		$this->request->data['User']['user_type_id'] = configure::read('UserTypes.SubAdmin');

		app::uses('String','Utility');
		$this->request->data['User']['activation_key'] = String::uuid();

		App::uses('CakeTime', 'Utility');
		$this->request->data['User']['persist_code'] = CakeTime::fromString(date('Y-m-d'));

		$this->request->data['UserProfile']['confirm_password'] = $this->request->data['User']['confirm_password'];
		if ($this->User->saveAssociated($this->request->data)) {
			$this->admin_addSubAdminEmail();
			$this->Session->setFlash('Sub-Admin added successfully.', 'default', 'success');
		} else {
			$errors = $this->User->validationErrors;
            if (!empty($errors)) {
                $errorMsg = $this->_setSaveAssociateValidationError($errors);
            }
			$this->Session->setFlash('Sub-Admin add request not completed due to following errors: <br/>' . $errorMsg . ' Try again!', 'default', 'error');
		}
		$this->redirect(array('action' => 'index', 'admin' => true));
	}

/**
 * Method admin_addSubAdminEmail for send registration notification to new sub admin added by admin itself
 *
 * @return void
 */
	private function admin_addSubAdminEmail() {
	
		$this->loadModel('EmailTemplate');

		$link = "<a href=" . Configure::read('ROOTURL').'admin/admins/subAdminActivateAccount/' .$this->request->data['User']['activation_key']. ">Click Here </a> to activate your account.";

	   $temp = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => Configure::read('EmailTemplateId.registration_notification'))));
	   $temp['EmailTemplate']['mail_body'] = str_replace(
	    	array('../../..', '#NAME', '#EMAIL', '#PASSWORD', '#CLICKHERE'),
	    	array(FULL_BASE_URL, $this->request->data['UserProfile']['first_name'].' '.$this->request->data['UserProfile']['last_name'], $this->request->data['User']['email'], $this->request->data['UserProfile']['confirm_password'], $link), $temp['EmailTemplate']['mail_body']);
		
		return $this->_sendEmailMessage($this->request->data['User']['email'], $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject']);
		
	}

/**
 * Method __updateUserCarDetail for send registration notification to new sub admin added by admin itself
 *
 * @return void
 */	

	private function __updateUserCarDetail($data = array()) {
		
		$userCarData = $this->UserCar->find('all',
										array('conditions' => array(
													'UserCar.user_id' => $data['UserProfile']['user_id']
											)

											)
									);
			
		foreach ($userCarData as $key => $value) {
			$this->UserCar->id = $value['UserCar']['id'];
			$this->UserCar->saveField('registeration_number',$data['UserCar']['registeration_number'][$key],False);
		}
		return true;
	}

/**
 * Method __updateUserAddressDetail for send registration notification to new sub admin added by admin itself
 *
 * @return void
 */	

	private function __updateUserAddressDetail($data = array()) {
		
		$userAddressData = $this->UserAddress->find('all',
										array('conditions' => array(
													'UserAddress.user_id' => $data['UserProfile']['user_id']
											)

											)
									);
			
		foreach ($userAddressData as $key => $value) {
			$this->UserAddress->id = $value['UserAddress']['id'];
			$this->UserAddress->saveField('address',$data['UserAddress']['address'][$key],False);
		}
		return true;
	}
/**
 * Method admin_addSubAdminEmail for send registration notification to new sub admin added by admin itself
 *
 * @return void
 */
	public function admin_saveDetails() {
		
		$this->request->allowMethod('post','put');
		
		$this->UserProfile->id = $this->request->data['UserProfile']['id']; 
		if ($this->UserProfile->save($this->request->data)) {
			$this->__updateUserCarDetail($this->request->data);
			$this->__updateUserAddressDetail($this->request->data);
			$this->Session->setFlash('User Details Edited successfully.', 'default', 'success');
			
		} else {
			$errors = $this->UserProfile->validationErrors;
            if (!empty($errors)) {
                $errorMsg = $this->_setSaveAssociateValidationError($errors);
            }
			$this->Session->setFlash('User detail edit request not completed due to following errors: <br/>' . $errorMsg . ' Try again!', 'default', 'error');
		}
		$this->redirect(array('action' => 'userListing', 'admin' => true));
	}
/**
 * Method _updateUserForRemovePicture common  function to run query for update user table so that when user wants to remove profile pic only one query can execute
 *
 * @return bool
 */
    private function __updateUserForRemovePicture($id) {
    	
    	$this->loadModel('UserProfile');
    	if (
    		$this->UserProfile->updateAll(
    			array(
    					'UserProfile.profile_pic' => null
    				),
    			array(
    					'UserProfile.user_id' => $id
    				)
    		)
    		) {
    			
    			//$this->Session->write('Auth.User.UserProfile.profile_pic', null);
    			return true;
    	}
    	return false;
    }

/**
 * Method _updateUserForRemovePicture common  function to run query for update user table so that when user wants to remove profile pic only one query can execute
 *
 * @return bool
 */
    private function __updateSpacePicture($id) {
    	
    	$this->loadModel('Space');
    	if (
    		$this->Space->updateAll(
    			array(
    					'Space.place_pic' => null
    				),
    			array(
    					'Space.id' => $id
    				)
    		)
    		) {
    			
    			//$this->Session->write('Auth.User.UserProfile.profile_pic', null);
    			return true;
    	}
    	return false;
    }

/**
 * Method __removePicture to remove user profile picture
 *
 * @return void
 */
	public function admin_removeUserImage($id = null) {

		$userInfo = $this->UserProfile->find('first',array('conditions'=> array('UserProfile.user_id' => $id)));
		if (trim($userInfo['UserProfile']['profile_pic']) != null) {

			if (!filter_var($userInfo['UserProfile']['profile_pic'], FILTER_VALIDATE_URL) === false) {
				$this->__updateUserForRemovePicture($id);

			}
			if (file_exists(WWW_ROOT . substr(Configure::read('UserImagePath'), 1) . $userInfo['UserProfile']['profile_pic'])) {
				unlink(WWW_ROOT . substr(Configure::read('UserImagePath'), 1) . $userInfo['UserProfile']['profile_pic']);
				$this->__updateUserForRemovePicture($id);
			} else {
				$this->__updateUserForRemovePicture($id);
			}
			$this->Session->setFlash(__('Profile picture has been removed successfully.'), 'default', 'success');
		} else {
			$this->Session->setFlash(__('Nothing to remove.'), 'default', 'error');
		}
		
		$this->redirect($this->referer());
	}

/**
 * Method __removeSpaceImage to remove space picture
 *
 * @return void
 */
	public function admin_removeSpaceImage($id = null) {
		
		$spaceInfo = $this->Space->find('first',array('conditions'=> array('Space.id' => $id)));
		if (trim($spaceInfo['Space']['place_pic']) != null) {

			if (!filter_var($spaceInfo['Space']['place_pic'], FILTER_VALIDATE_URL) === false) {
				$this->__updateSpacePicture($id);

			}
			if (file_exists(WWW_ROOT . substr(Configure::read('SpaceImagePath'), 1) . $spaceInfo['Space']['place_pic'])) {
				unlink(WWW_ROOT . substr(Configure::read('SpaceImagePath'), 1) . $spaceInfo['Space']['place_pic']);
				$this->__updateSpacePicture($id);
			} else {
				$this->__updateSpacePicture($id);
			}
			$this->Session->setFlash(__('Space picture has been removed successfully.'), 'default', 'success');
		} else {
			$this->Session->setFlash(__('Nothing to remove.'), 'default', 'error');
		}
		
		$this->redirect($this->referer());
	}



/**
 * Method admin_subAdminActivateAccount to activate sub admins accounts
 *
 * @param $activationKey activation key of sub admin
 * @return void
 */
	public function admin_subAdminActivateAccount($activationKey = null) {
		$checkRecord = $this->User->findByActivationKeyAndIsActivated($activationKey, '0', array('id', 'is_activated'));
		if (empty($checkRecord)) {
			$this->Session->setFlash('This link has been expired!', 'default', 'error');
			$this->redirect(array('action' => 'login', 'admin' => true));
		}
		if($this->User->activateAccount($activationKey)) {
			$this->Session->setFlash(__('Your account has been activated successfully! Please login to continue.'), 'default', 'success');
		} else {
			$this->Session->setFlash(__('Some errors occurred. Please try again.'), 'default', 'error');
		}
		$this->redirect(array('action' => 'login', 'admin' => true));
	}
	
/**
 * Method admin_changeSubAdminStatus to activate inactivate the sub admin status
 *
 * @param $userID int the user id of the sub admin
 * @return void
 */
	public function admin_changeRecordStatus($recordId = null, $model = null, $module = null) {
		$recordId = base64_decode($recordId);
		$this->loadModel($model);
		$checkRecord = $this->$model->findById($recordId, array('id', 'is_activated'));
		if (empty($checkRecord)) {
			$this->Session->setFlash(__('This Record does not exist.'), 'default', 'error');
			$this->redirect($this->referrer());
		}

		$status = $this->_changeAccountStatus($checkRecord,$model);
		$moduleName = empty($module) ? $model : $module;
		$this->Session->setFlash(__($moduleName.' has been activated successfully.'), 'default', 'success');
		if ($status == configure::read('Activate.False')) {
			$this->Session->setFlash(__($moduleName.' has been deactivated successfully.'), 'default', 'success');
		}
		$this->redirect($this->referer());
	}
	
/**
 * Method admin_deletesubAdmin to delete sub admin
 *
 * @param $userID int the user id of the sub admin
 * @return void
 */
	public function admin_delete($userID = null, $model = null) {
		$userID = base64_decode($userID);
		$this->loadModel($model);
		$checkRecord = $this->$model->findById($userID, array('id', 'is_deleted'));
		if (empty($checkRecord)) {
			$this->Session->setFlash('Record does not exist.', 'default', 'error');
			$this->redirect($this->referrer());
		}

		$status = $this->_deleteAccount($checkRecord[$model]['id'], $checkRecord[$model]['is_deleted'], $model);
		$moduleName = empty($module) ? $model : $module;
		$this->Session->setFlash(__($moduleName.' has been deleted successfully.'), 'default', 'success');
		if ($status == configure::read('Activate.False')) {
			$this->Session->setFlash(__($moduleName.' has been restored successfully.'), 'default', 'success');
		}
		$this->redirect($this->referer());
	}

/**
 * Method admin_dashboard
 */
	public function admin_dashboard() {
		$this->layout = 'backend';
		//Active front users count
		$activeUsers = $this->User->activeUserCount();

        //Approved,activated,completed and not deleted spaces count
		$parkingSpaces = $this->Space->activeParkingSpaceCount();
		
		//Total no of bookings count
		$bookings = $this->Booking->approvedBookingCount();

		//Total simply park income collected
		$amount = $this->Booking->getTotalRevenue();

		//Total simply park service tax 
		$serviceTaxTotalAmount = $this->Booking->getTotalServiceTax();


		$this->set(compact('activeUsers','parkingSpaces','bookings','amount','serviceTaxTotalAmount'));		
	}


/**
 * Method admin_changeRecordStatusByActive to activate inactivate the sub admin status
 *
 * @param $recordId int id of the table
 * @param $model string the name of the model
 * @param $$module string name of the module to show in seccess messages
 * @return void
 */
	public function admin_changeRecordStatusByActive($recordId = null, $model = null, $module = null) {
		$recordId = base64_decode($recordId);
		$this->loadModel($model);
		$checkRecord = $this->$model->findById($recordId, array('id', 'is_active'));
		if (empty($checkRecord)) {
			$this->Session->setFlash(__('This Record does not exist.'), 'default', 'error');
			$this->redirect($this->referrer());
		}

		$status = $this->_changeAccountStatusByActive($checkRecord,$model);
		$moduleName = empty($module) ? $model : $module;
		$this->Session->setFlash(__($moduleName.' has been activated successfully.'), 'default', 'success');
		if ($status == configure::read('Activate.False')) {
			$this->Session->setFlash(__($moduleName.' has been deactivated successfully.'), 'default', 'success');
		}
		$this->redirect($this->referer());
	}

	public function admin_bookings () {
		$this->layout = 'backend';
	}

	public function admin_userListing() {
		
		$this->layout = 'backend';
		$conditions = array(
			'User.user_type_id' => configure::read('UserTypes.User'),
			);
		if (isset($this->request->query) && !empty($this->request->query)) {
			$searchData = array(
				'OR' => array(
						'UserProfile.first_name LIKE' => '%'. $this->request->query['search'] .'%',
						'UserProfile.last_name LIKE' => '%'. $this->request->query['search'] .'%',
						'User.email LIKE' => '%'. $this->request->query['search'] .'%',
					)
				);
			$conditions = array_merge($conditions, $searchData);
		}
		
		$this->Paginator->settings = array(
										'conditions' => $conditions,
										'limit' => 10,
										'order' => 'User.created Desc',
										'fields' => array('id', 'email', 'is_activated', 'is_deleted', 'social_network_user'),
										'contain' => array(
											'UserProfile' => array(
												'fields' => array(
													'id', 'first_name' ,'last_name'
													)
												)
											)
									);
		$usersDatas = $this->Paginator->paginate('User');
		$this->set('usersDatas',$usersDatas);
		if ($this->request->is('ajax')) {
			$this->layout = '';
			$this->autoRender = false;
			$this->viewPath = 'Elements' . DS . 'backend' . DS . 'Admin';
			$this->render('userlisting');
		}
	}

	public function admin_ajaxGetFrontUserData($user_id = null) {
		$user_id = urldecode(base64_decode($user_id));

		$fields = array(
				'User.username',
				'User.email',
				'UserProfile.id',
				'UserProfile.first_name',
				'UserProfile.user_id',
				'UserProfile.last_name',
				'UserProfile.mobile',
				'UserProfile.company',
				'UserProfile.tan_number',
				'UserProfile.profile_pic',
				

			);
		$options['contain'] = array(
						'UserAddress' => array(
                                
                                  'State' => array(
                                  		'fields' => 
                                  			array('State.name')
                                  	),
                                  'City' => array(
                                  		'fields' => 
                                  			array('City.name')

                                  	)
                                ));
		$getUserData = $this->User->find('first',array(
				'conditions' => array(
					'User.id' => $user_id,
				),
				'fields' => $fields,
				'recursive' => 2
				)
		);
		
		$img_with_path = $this->__userProfilePicturePath($getUserData['UserProfile']['profile_pic']);
		unset($getUserData['UserProfile']['profile_pic']);
		$getUserData['UserProfile']['profile_pic'] = $img_with_path;
        
		$this->set(
				array(
					'response' => $getUserData,
					'_serialize' => 'response'
				)
			);
	}

	public function admin_addFrontUser() {
		$this->request->allowMethod('post','put');

		$this->User->unvalidate(array('old_password'));
		$this->UserProfile->unvalidate(array('profile_pic','company'));
		$this->request->data['User']['user_type_id'] = configure::read('UserTypes.User');
		$this->request->data['User']['is_activated'] = 1;

		app::uses('String','Utility');
		$this->request->data['User']['activation_key'] = String::uuid();

		App::uses('CakeTime', 'Utility');
		$this->request->data['User']['persist_code'] = CakeTime::fromString(date('Y-m-d'));

		$this->request->data['UserProfile']['confirm_password'] = $this->request->data['User']['confirm_password'];
		if ($this->User->saveAssociated($this->request->data)) {
			$this->Session->setFlash('Front User added successfully.', 'default', 'success');
		} else {
			$errors = $this->User->validationErrors;
            if (!empty($errors)) {
                $errorMsg = $this->_setSaveAssociateValidationError($errors);
            }
			$this->Session->setFlash('Front User add request not completed due to following errors: <br/>' . $errorMsg . ' Try again!', 'default', 'error');
		}
		$this->redirect(array('action' => 'userListing', 'admin' => true));
	}

	private function __userProfilePicturePath($userPic = null) {
    	if (!filter_var($userPic, FILTER_VALIDATE_URL) === false) {
    		return $userPic;
    	}

        if (!file_exists(WWW_ROOT . substr(Configure::read('UserImagePath'), 1) . $userPic)) {
            return Configure::read('UserDummyImagePath');
        }

        $profilePic = !empty($userPic) ? Configure::read('UserImagePath').$userPic : Configure::read('UserDummyImagePath');
        return $profilePic;
    }

}
