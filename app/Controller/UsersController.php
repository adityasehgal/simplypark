<?php

App::uses('AppController', 'Controller');
App::import('Vendor','Unirest', array('file' => 'unirest-php-master' . DS . 'src'.DS.'Unirest.php'));
class UsersController extends AppController {

	public $helper = array('Html', 'UserData');
	public $uses = array('User',
						'UserProfile',
						'Space',
						'SpaceFacility',
						'SmallCarSpace',
						'SpaceTimeDay',
						'SpaceDisapprovalReason',
						'SpacePricing',
						'SmallCarSpace'
					);

    public function beforeFilter() {
        parent::beforeFilter();
        if ($_SERVER["REMOTE_ADDR"] != Configure::read('Server.Local') && $_SERVER['SERVER_NAME'] == Configure::read('Server.Live')) {
			$this->Security->unlockedActions = array('addSpacePolicySave','addSpaceDetailsSave','saveSpaceData');
		}
        $this->Auth->allow('addSpace');
    }

/**
 * Method parkingSpace to list all the spaces available
 *
 * @return void
 */
public function parkingSpace() 
 {
        $this->layout = 'user';
        $msg_data = array();
        $space_data = $this->Space->find('all',array(
						'conditions' => array(
							'Space.user_id' => $this->Auth->user('id'),
							'Space.is_deleted' => 0,
							'Space.is_completed' => 1
						),
						'fields' => array(
							'Space.id',
							'Space.name',
							'Space.flat_apartment_number',
							'Space.address1',
							'Space.address2',
							'Space.post_code',
							'Space.number_slots',
							'State.name',
							'City.name',
							'Space.is_activated',
							'Space.is_approved',
							'Space.is_completed',
							'SpaceDisapprovalReason.reason'
						),
						'group' => 'Space.id',
						'order' => 'Space.created DESC'
					)
				);

        if($space_data) {
	        $path['path'] = Configure::read('ROOTURL');
			$space_data = array_merge($space_data, $path);

			if(!empty($msg_data)) {
				$space_data = array_merge($space_data, $msg_data);
			}
		}

        $this->set('data',$space_data);
    }

/**
 * Method logout to logout the user
 *
 * @return string to logout the user
 */
    public function logout(){
    	$this->Cookie->delete('User'); 
    	setcookie ("start-date", "", time() - 3600, "/", $_SERVER['SERVER_NAME'] , 1); 
    	setcookie( "end-date", "", time()- 3660, "/",$_SERVER['SERVER_NAME'], 1);
    	setcookie( "space-id", "", time()- 3660, "/",$_SERVER['SERVER_NAME'], 1);
    	setcookie( "space-park-id", "", time()- 3600, "/",$_SERVER['SERVER_NAME'], 1);
    	setcookie( "current-action", "", time()- 3600, "/",$_SERVER['SERVER_NAME'], 1);
        return $this->redirect($this->Auth->logout());
    }

/**
 * Method changePassword to open change password page
 *
 * @return void
 */
	public function changePassword() {
		$this->layout = 'user';
	}

/**
 * Method saveChangePassword for save changed user password
 *
 * @return void
 */
	public function saveChangePassword() {
		$this->request->allowMethod('post','put');
		$this->_changePassword();
	}

/**
 * Method udpateProfilePic to update user profile picture
 *
 * @return void
 */
	public function udpateProfilePic() {
		$this->request->allowMethod('post','put');
		
		if ($this->request->data['UserProfile']['upload_remove_profile_pic'] == Configure::read('Bollean.False')) {
			$this->__removePicture();
		}
		$this->UserProfile->unvalidate(array('first_name', 'last_name', 'company'));
		if ($this->UserProfile->save($this->request->data)) {

			$this->Session->setFlash(__('Your profile picture has been updated successfully.'), 'default', 'success');
			$this->redirect($this->referer());
		}
		$errors = $this->UserProfile->validationErrors;
        if (!empty($errors)) {
            $errorMsg = $this->_setValidaiotnError($errors);
        }
		$this->Session->setFlash(__('Update profile picture request not completed due to following errors : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
		$this->redirect($this->referer());
	}

public function addSpace($space_id = NULL) 
{
	//this function check at the entry point for a logged in user.
	//however, the same function is used for review space also. 
	//a user can only review the space if he is logged in and therefore
	//we are skipping the check for checkIfAdminOrUser when space_id != NULL.
	//we can implement a redirection based on space id. However, if the user is logged
	// in as user A and does a review space and then the platform asks for login info.
	// this time user enters login info of B...we are still seeing the space of A.
	// This needs investigation. However, a reasonable assumption is that a logged in user
	// can only review his/her space and the link is not available unless a user logins
	//
	if ($space_id == NULL && !$this->_checkIfAdminOrUser()) {
		$this->Session->setFlash(__('Please login or create a SimplyPark account before you can list your space.'), 'default', 'error');
		$this->redirect('/?errorlogin=1&listSpaceRedirect=1');
	}



	$this->layout = 'user';
	$data = array();

	if($space_id != NULL) {
		$space_id = base64_decode($space_id);
	}

	if($space_id == NULL) {
		$space_data = $this->Space->find('first',array(
					'conditions' => array(
						'Space.is_completed' => 0,
						'Space.user_id' => $this->Auth->user('id'),
						),
					'fields' => array(
						'Space.id'
						)
					)
				);
		if(!empty($space_data)) {
			$space_id = $space_data['Space']['id'];
		}
	}

	if($space_id != NULL) {
		$fields = array(
				'Space.id',
				'Space.name',
				'Space.description',
				'Space.place_pic',
				'Space.flat_apartment_number',
				'Space.address1',
				'Space.address2',
				'Space.state_id',
				'Space.city_id',
				'Space.lat',
				'Space.lng',
				'Space.post_code',
				'Space.tower_number',
				'Space.space_type_id',
				'Space.property_type_id',
				'Space.is_deposit_required',
				'Space.deposit_for_months',
				'Space.number_slots',
				'Space.is_same_slots',
				'Space.is_activated',
				'Space.is_completed',
				'Space.is_approved',
				'SmallCarSpace.id',
				'SmallCarSpace.small_car_slot',
				);

		$space_data = $this->Space->find('first',array(
					'conditions' => array(
						'Space.id' => $space_id,
						),
					'fields' => $fields,
					'contain' => array(
						'SpacePricing',
						'SmallCarSpace',
						'SpacePark' => array(
							'fields' => array(
								'id',
								'park_number'
								)
							)
						)
					)
				);

		//CakeLog::write('debug','In add space ...' . print_r($space_data,true));

		$data['city'] = $this->_getCityList($space_data['Space']['state_id']);
		$data['space_data'] = $space_data;
		

		if($space_data['Space']['is_approved'] == 1) {
			$msgToDisplay = "Please review your Space settings. Only few fields are available for Edit.";
			$this->Session->setFlash($msgToDisplay, 'flash_custom_space', array(), 'reviewSpaceMessage');
		}
	}

	$data['state'] = $this->_getStateList();
	$data['space_type'] = $this->_getSpaceTypeList();
	$data['facilities'] = $this->_getFacilitiesList();
	$data['property_type'] = $this->_getPropertyTypeList();
	$data['path'] = Configure::read('ROOTURL');

	if(!empty($data)) {
		$this->set('data',$data);
	} else {
		$this->Session->setFlash('Something went wrong, Try again.', 'flash_custom_space', array(), 'spaceMessage');
		//$this->Session->setFlash(__('Something went wrong, Try again.'), 'default', 'error');
		$this->redirect(array('action' => 'parkingSpace', 'users' => true));
	}
}

	public function getCities() {
		$this->autoRender = false ;
		$cities = $this->_getCityList($_GET['stateId']);
		
		return json_encode($cities);
	}

	public function addSpacePolicy($space_id = null) {
		$this->layout = 'user';
		
		$data = array();
		$space_id = base64_decode($space_id);
		if($space_id) {
			$fields = array(
					'Space.lat',
					'Space.lng',
					'Space.cancellation_policy_id',
					'Space.sign_agreement',
					'Space.booking_confirmation',
					'Space.is_completed',
					'Space.is_approved',
                                        'Space.is_activated'
				);
			$space_data = $this->Space->find('first',array(
					'conditions' => array(
						'Space.id' => $space_id,
					),
					'fields' => $fields,
					'recursive' => -1
				)
			);
			$data['space_data'] = $space_data;

			$data['lat'] = (isset($space_data['Space']['lat']) ? $space_data['Space']['lat'] : '');
			$data['long'] = (isset($space_data['Space']['lng']) ? $space_data['Space']['lng'] : '');
			$data['policies'] = $this->_getCancellationPolicy();
			$data['booking_confirmation'] = Configure::read('booking_confirmation');
			$data['space_id'] = $space_id;
			
			$this->set('data',$data);
		} else {
			return $this->redirect(
	            	array('controller' => 'users', 'action' => 'addSpacePolicy')
	        	);
		}
	}

	public function addSpaceDetails($space_id = null) {
		$this->layout = 'user';
		$space_id = base64_decode($space_id);
		if($space_id) {
			$data['space_id'] = $space_id;
			$fields = array(
                                        'Space.is_completed',
					'Space.is_activated',
					'Space.property_type_id',
					'Space.is_approved',
					'Space.is_completed',
                                        'Space.tier_pricing_support',
                                        'Space.tier_desc',
                                        'Space.tier_formatted_desc',
				);

			$space_data = $this->Space->find('first',array(
					'conditions' => array(
						'Space.id' => $data['space_id'],
					),
					'fields' => $fields,
					'contain' => array(
						 'SpacePricing' => array(
							 'fields' => array('id','vehicle_type','hourly','daily','weekely','monthly','yearly')),
						 'SpaceTimeDay' => array(
							 'fields' => array('id','mon','tue','wed','thu','fri','sat','sun','from_date','to_date','open_all_hours'))
							 )
			                   ));
			$data['space_data'] = $space_data;
			//CakeLog::write('debug','Insdie addSpaceDetails...' . print_r($space_data,true));
                        
                        if($space_data['Space']['is_approved'] == 1) {
                           $msgToDisplay = "You can review and change the price your charge for your space. Any change will be reflected in future bookings.";
			   $this->Session->setFlash($msgToDisplay, 'flash_custom_space', array(), 'reviewSpaceMessage');
                        }
			$this->set('data',$data);
		} else {
			return $this->redirect(
	            	array('controller' => 'users', 'action' => 'addSpacePolicy')
	        	);
		}
	}

public function saveSpaceData() 
{
	//CakeLog::write('debug','Inside saveSpaceData...');
	$msg = 'Your parking space updated successfully';
	$data_approve = array();
	if($this->request->data) {
                //CakeLog::write('debug','saveSpaceData...' . print_r($this->request->data,true));
		$this->SpaceTimeDay->updateAll(
				array(
					'SpaceTimeDay.mon' => Configure::read('Bollean.False'),
					'SpaceTimeDay.tue' => Configure::read('Bollean.False'),
					'SpaceTimeDay.wed' => Configure::read('Bollean.False'),
					'SpaceTimeDay.thu' => Configure::read('Bollean.False'),
					'SpaceTimeDay.fri' => Configure::read('Bollean.False'),
					'SpaceTimeDay.sat' => Configure::read('Bollean.False'),
					'SpaceTimeDay.sun' => Configure::read('Bollean.False'),
				     ),
				array(
					'SpaceTimeDay.space_id' => $this->request->data['Space']['id']
				     )
				);

		if (isset($this->request->data['SpaceTimeDay']['open_all_hours']) && 
                    $this->request->data['SpaceTimeDay']['open_all_hours']) 
                {
			$this->request->data['SpaceTimeDay']['from_date'] = $this->request->data['SpaceTimeDay']['from_date'].' 00:00:00';
			$this->request->data['SpaceTimeDay']['to_date'] = $this->request->data['SpaceTimeDay']['to_date'].' 00:00:00';
		} 
               else 
                {
			$this->request->data['SpaceTimeDay']['from_date'] = $this->request->data['SpaceTimeDay']['from_date'].' '.$this->request->data['SpaceTimeDay']['from_time'];
			$this->request->data['SpaceTimeDay']['to_date'] = $this->request->data['SpaceTimeDay']['to_date'].' '.$this->request->data['SpaceTimeDay']['to_time'];

			$this->request->data['SpaceTimeDay']['open_all_hours'] = Configure::read('Bollean.False');
		}

	        	
		if (isset($this->request->data['SpacePricing'])) 
		{
                   //CakeLog::write('debug','After...SpacePricing if condition');
		   $pricingArray = $this->request->data['SpacePricing'];
		   unset($this->request->data['SpacePricing']);
		   $this->request->data['SpacePricing'][0] = $pricingArray;	
		}	

                //CakeLog::write('debug','After...' . print_r($this->request->data,true));

		if ($this->Space->saveAssociated($this->request->data)) {
			$space_data = $this->Space->find('first',array(
						'conditions' => array(
							'Space.id' => $this->request->data['Space']['id'],
							),
						'fields' => array(
							'Space.id',
							'Space.name',
							'Space.flat_apartment_number',
							'Space.address1',
							'Space.address2',
							'Space.is_approved',
							'Space.post_code',
							'State.name',
							'City.name',
							'User.username',
							'Space.is_completed',
                                                        'Space.is_approved'
							)
						)
					);


			$space_is_approved = $space_data['Space']['is_approved'];

			$is_activated = (isset($this->request->data['SpaceTimeDay']['is_activated'])) ? 1 : 0;

			/*$data_update = array(
			  'is_completed' => 1,
			  'is_activated' => $is_activated
			  );*/

			$user_data = $this->User->find('first',array(
						'conditions' => array(
							'User.id' => $this->Auth->user('id'),
							),
						'fields' => array(
							'User.trusted_by_admin'
							)
						)
					);

                       $subject = 'Space Added';

                        if($space_data['Space']['is_approved'] == Configure::read('space_approval_states.approved'))
                         {
                             //review case
			     $data_approve = array(
						'is_approved' => Configure::read('space_approval_states.approved') //waiting is spelled incorrectly 
						 );
                             $msg = 'Parking Space settings updated. Any change in the rental price will reflect only on future bookings';
                             $subject = '[NO ACTION]Existing Space Updated';

                        }
                       else if($space_data['Space']['is_approved'] == Configure::read('space_approval_states.disapproved')) 
                       {
			      $data_approve = array(
						'is_approved' => Configure::read('space_approval_states.wating') 
						);
			      $msg = 'Your parking space settings updated successfully. Space has been sent again for approval.';
                              $subject = '[Needs Action]A disapproved space submitted for apporval';
			}
                       else 
                        {
			   if($user_data['User']['trusted_by_admin'] == 1) 
                           {
				  $data_approve = array(
						    'is_approved' => Configure::read('space_approval_states.approved') 
						  );
                                  $msg = 'Your parking space added successfully.';
                                  $subject = '[No ACTION]Space Added by a Trusted User';
                           }else {
				  $data_approve = array(
						    'is_approved' => Configure::read('space_approval_states.wating') //waiting is spelled incorrectly
						  );
                                  $msg = 'Your parking space added successfully. However, your listing is currently under review by our team. We will notify you via email once your listing is approved';
                                  $subject = '[Needs Action]Parking Space Added and needs approval';
                          } 
		        }
			

                        $this->Space->id = $this->request->data['Space']['id'];
			$this->Space->save($data_approve);


			//send mail to admin
			$address = $space_data['Space']['flat_apartment_number'].', '.$space_data['Space']['address1'].', '.$space_data['Space']['address2'];
			$address .= $space_data['City']['name'].', '.$space_data['State']['name'].', '.$space_data['Space']['post_code'];
			$space = array(
					'space_name' => $space_data['Space']['name'],
					'username' => $space_data['User']['username'],
					'address' => $address,
					'propertyType' => $this->request->data['Space']['property_type'],
					'hpricing' => isset($this->request->data['SpacePricing'][0]['hourly'])?$this->request->data['SpacePricing'][0]['hourly']:0.00,
					'dpricing' => $this->request->data['SpacePricing'][0]['daily'],
					'wpricing' => $this->request->data['SpacePricing'][0]['weekely'],
					'mpricing' => $this->request->data['SpacePricing'][0]['monthly'],
					'ypricing' => $this->request->data['SpacePricing'][0]['yearly'],
                                        'tier_pricing_support' => isset($this->request->data['Space']['tier_pricing_support'])?$this->request->data['Space']['tier_pricing_support']:0,
                                        'subject' => $subject
				      );

			$this->_sendAddSpaceEmail($space);	
			$this->Session->setFlash($msg, 'flash_custom_space', array(), 'spaceMessage');

			return $this->redirect(Configure::read('ROOTURL').'users/parkingSpace');

		} else {
			//$this->Session->setFlash(__('Your Request could not completed due to following error : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
			$this->Session->setFlash('Error : Your Request could not completed.', 'flash_custom_space', array(), 'spaceMessage');
			$this->redirect($this->referer());
		}
	} else {
		$this->Session->setFlash('Error : Your Request could not completed.', 'flash_custom_space', array(), 'spaceMessage');
		//$this->Session->setFlash(__('Your Request could not completed '), 'default', 'error');
		$this->redirect($this->referer());
	}
}

/**
 * Method __removePicture to remove user profile picture
 *
 * @return void
 */
	private function __removePicture() {
		$userInfo = $this->_getUserProfileData();
		if (trim($userInfo['UserProfile']['profile_pic']) != null) {
			if (!filter_var($userInfo['UserProfile']['profile_pic'], FILTER_VALIDATE_URL) === false) {
				$this->_updateUserForRemovePicture();
			}
			if (file_exists(WWW_ROOT . substr(Configure::read('UserImagePath'), 1) . $userInfo['UserProfile']['profile_pic'])) {
				unlink(WWW_ROOT . substr(Configure::read('UserImagePath'), 1) . $userInfo['UserProfile']['profile_pic']);
				$this->_updateUserForRemovePicture();
			} else {
				$this->_updateUserForRemovePicture();
			}
		}
		$this->redirect($this->referer());
	}

	public function setIsDeleted($userID = null, $model = null) {
		$userID = base64_decode($userID);
		$this->loadModel($model);
		$checkRecord = $this->$model->findById($userID, array('id', 'is_deleted'));
		if (empty($checkRecord)) {
			$this->Session->setFlash('Record does not exist.', 'default', 'error');
			$this->redirect($this->referrer());
		}

		$status = $this->_deleteAccount($checkRecord[$model]['id'], $checkRecord[$model]['is_deleted'], $model);
		$moduleName = empty($module) ? $model : $module;
		if($moduleName == 'Space') {
			$this->Session->setFlash($moduleName.' has been deleted successfully.', 'flash_custom_space', array(), 'spaceMessage');
		} else {
			$this->Session->setFlash(__($moduleName.' has been deleted successfully.'), 'default', 'success');
		}
		$this->redirect($this->referer());
	}

	public function setIsActivated($recordId = null, $model = null, $module = null) {
		$this->autoRender = false ;
		$recordId = base64_decode($recordId);
		$this->loadModel($model);
		$checkRecord = $this->$model->findById($recordId, array('id', 'is_activated'));
		if (empty($checkRecord)) {
			return $this->Session->setFlash(__('This Record does not exist.'), 'default', 'error');
			//$this->redirect($this->referrer());
		}

		$this->loadModel('Booking');
		$bookinsOnSpace = $this->Booking->checkSpaceBooked($recordId); //checking booking records exists for this space

		if ($bookinsOnSpace > Configure::read('Bollean.False')) {
			return $this->Session->setFlash(' Your listing cannot be deactivated at this time as there is an ongoing/pending booking on it.','flash_custom_space' ,array(), 'spaceMessage');
		}

		$status = $this->_changeAccountStatus($checkRecord,$model);
		$moduleName = empty($module) ? $model : $module;
		if($moduleName == 'Space') {
			$this->Session->setFlash($moduleName.' has been activated successfully.', 'flash_custom_space', array(), 'spaceMessage');
		} else {
			$this->Session->setFlash(__($moduleName.' has been activated successfully.'), 'default', 'success');
		}
		if ($status == configure::read('Activate.False')) {
			if($moduleName == 'Space') {
				$this->Session->setFlash($moduleName.' has been deactivated successfully.', 'flash_custom_space', array(), 'spaceMessage');
			} else {
				return $this->Session->setFlash(__($moduleName.' has been deactivated successfully.'), 'default', 'success');
			}
		}
		//$this->redirect($this->referer());
	}

	public function addSpacePolicySave() {
		$this->autoRender = false ;

		$data = array();
		$user_id = array();
		$facility = array();
		$small_car_slot = array();

		if($this->request->data) {
			$user_id['user_id'] = $this->Auth->user('id');
			$this->request->data['Space'] = array_merge($this->request->data['Space'], $user_id);
			/*if(!isset($this->request->data['Space']['is_same_slots'])) {
				$small_car_slot['is_same_slots'] = 0;
				$this->request->data['Space'] = array_merge($this->request->data['Space'], $small_car_slot);
			}*/

			$this->loadModel('SpacePark');
			if (isset($this->request->data['Space']['id'])) {
				$this->SpacePark->deleteAll(array('SpacePark.space_id' => $this->request->data['Space']['id']));
			}

			//Hanling Space park records if gated community property type is selected
			foreach ($this->request->data['SpacePark'] as $key => $value) {
				if (empty($value['park_number']) && $this->request->data['Space']['property_type_id'] == 2) {
					unset($this->request->data['SpacePark'][$key]);
					if (isset($value['id'])) {
						$this->SpacePark->delete(array('SpacePark.id' => $value['id']));
					}
				}
			}

			// Deleting all the space parks while updating space if user change the property type from gated community to anohter
			if ($this->request->data['Space']['property_type_id'] != 2 && isset($this->request->data['Space']['id'])) {
				$this->SpacePark->deleteAll(array('SpacePark.space_id' => $this->request->data['Space']['id']));
			}

                        //CakeLog::write('debug','In addSavePolicySave...' . print_r($this->request->data,true));

			if ($this->Space->saveAssociated($this->request->data)) {
				$space_id = $this->Space->id;

				$this->SpaceFacility->deleteAll(array('SpaceFacility.space_id' => $space_id));

				if(isset($this->request->data['Space']['facility_id'])) {
					foreach($this->request->data['Space']['facility_id'] as $facility_id) {
						$facility[]['SpaceFacility'] = array(
													'space_id' => $space_id, 
													'facility_id' => $facility_id
												);
					}
					$this->SpaceFacility->saveMany($facility);
				}

				/*if($this->request->data['Space']['is_same_slots'] == 0) {
					$save_data = array(
							'space_id' => $space_id,
							'small_car_slot' => $this->request->data['Space']['small_car_slot']
						);

					if(!empty($this->request->data['Space']['small_car_space'])) {
						$this->SmallCarSpace->id = $this->request->data['Space']['small_car_space'];
					}

					$this->SmallCarSpace->save($save_data);
				}*/

				if(!empty($this->request->data['Space']['hourly']) || 
					!empty($this->request->data['Space']['daily']) ||
					!empty($this->request->data['Space']['weekely']) ||
					!empty($this->request->data['Space']['monthly']) ||
					!empty($this->request->data['Space']['yearly']) ) {
					$data_to_save = array(
							'space_id' => $space_id,
							'hourly' => $this->request->data['Space']['hourly'],
							'daily' => $this->request->data['Space']['daily'],
							'weekely' => $this->request->data['Space']['weekely'],
							'monthly' => $this->request->data['Space']['monthly'],
							'yearly' => $this->request->data['Space']['yearly'],
						);

					if(!empty($this->request->data['Space']['space_pricing'])) {
						$this->SpacePricing->id = $this->request->data['Space']['space_pricing'];
					}

					$this->SpacePricing->save($data_to_save);
				}
								
				$this->redirect(Configure::read('ROOTURL').'users/addSpacePolicy/'.base64_encode($space_id));
			} else {
				$this->Session->setFlash('Error : Your Request could not completed.', 'flash_custom_space', array(), 'spaceMessage');
				//$this->Session->setFlash(__('Your Request could not completed due to following error : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
				$this->redirect($this->referer());
			}

		} else {
			return $this->redirect(
	            	array('controller' => 'users', 'action' => 'addSpace')
	        	);
		}
	}

	public function addSpaceDetailsSave() {
		$this->autoRender = false ;
		$data = array();
		
		if($this->request->data) {
                        //CakeLog::write('debug','In addSpaceDetailsSave....' . print_r($this->request->data['Space'],true));
			$user_id['user_id'] = $this->Auth->user('id');
			$this->request->data['Space'] = array_merge($this->request->data['Space'], $user_id);
			if ($this->Space->save($this->request->data)) {
				$this->redirect(Configure::read('ROOTURL').'users/addSpaceDetails/'.base64_encode($this->request->data['Space']['id']));
			}
		} else {
			return $this->redirect(
	            	array('controller' => 'users', 'action' => 'addSpacePolicy')
	        	);
		}
	}

/**
 * Method updateProfile to show update profile page
 *
 * @return void
 */
	public function updateProfile() {
		$this->layout = 'user';
		$userID = $this->Auth->user('id');
		$userData = $this->User->getUserDataToEdit($userID);
		$this->set('userData', $userData);

		$states = $this->_getStateList(); //getting state list
		$this->set('states', $states);

		if (!empty($userData['UserAddress'][0]['state_id'])) {
			$billingCities = $this->_getCityList($userData['UserAddress'][0]['state_id']); //getting cities list
			$this->set('billingCities', $billingCities);
		}

		if (!empty($userData['UserAddress'][1]['state_id'])) {
			$homeCities = $this->_getCityList($userData['UserAddress'][1]['state_id']); //getting cities list
			$this->set('homeCities', $homeCities);
		}

	}

/**
 * Method updateProfileSave to save updated profile records
 *
 * @return void
 */
	public function updateProfileSave() {
		$this->request->allowMethod('post','put');
		if (!empty($this->request->data)) {
			$this->User->contain('UserProfile');
			$userMobileNumber = $this->User->findById($this->request->data['User']['id'],array('UserProfile.mobile','UserProfile.is_mobile_verified'));
			if ($userMobileNumber['UserProfile']['mobile'] != $this->request->data['UserProfile']['mobile']) {
				$this->request->data['UserProfile']['is_mobile_verified'] = Configure::read('Bollean.false');
				
			}
			
			if (isset($this->request->data['User']['home_address']) && $this->request->data['User']['home_address'] == Configure::read('Bollean.True')) {
				$this->request->data['UserAddress'][2]['flat_apartment_number'] = $this->request->data['UserAddress'][1]['flat_apartment_number'];
				$this->request->data['UserAddress'][2]['type'] = configure::read('UserAddressType.Home');
				$this->request->data['UserAddress'][2]['address'] = $this->request->data['UserAddress'][1]['address'];
				$this->request->data['UserAddress'][2]['state_id'] = $this->request->data['UserAddress'][1]['state_id'];
				$this->request->data['UserAddress'][2]['city_id'] = $this->request->data['UserAddress'][1]['city_id'];
				$this->request->data['UserAddress'][2]['pincode'] = $this->request->data['UserAddress'][1]['pincode'];
			}
			try{


				$this->__deleteUserCars(); //soft delete all the car registration numbers before save

				foreach ($this->request->data['UserCar'] as $key => $value) {
					if (empty($value['registeration_number'])) {
						unset($this->request->data['UserCar'][$key]);
					} else { 
						$this->request->data['UserCar'][$key]['is_deleted'] = Configure::read('Bollean.False');
					}
				}

				$this->User->unvalidate(array('username', 'email', 'old_password', 'password', 'confirm_password'));
				$this->UserProfile->unvalidate(array('profile_pic'));
				
				if ($this->User->saveAssociated($this->request->data)) { 
					$this->Session->setFlash(__('Your profile has been successfully updated.'), 'default', 'success');
					$this->redirect($this->referer());
				}

				$errors = $this->User->validationErrors;
		        if (!empty($errors)) {
		            $errorMsg = $this->_setSaveAssociateValidationError($errors);
		        }
				$this->Session->setFlash(__('Update profile request not completed due to following errors : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
				$this->redirect($this->referer());
			} catch(Execption $e){
				$this->Session->setFlash(__('Some error occurred. Please try again.'), 'default', 'error');
				$this->redirect($this->referer());
			}
		}
	}

/**
 * Method __deleteUserCars to delete user cars
 *
 * @return void
 */
	private function __deleteUserCars() {
		$userID = $this->Auth->user('id');
		$this->loadModel('UserCar');
		$this->UserCar->updateAll(
										array(
												'UserCar.is_deleted' => Configure::read('Bollean.True')
											),
										array(
												'UserCar.user_id' => $userID
											)
								);
		return true;
	}


/**
 * Method sendOtpOnPhone to send OTP on phone
 * @param Mobile Number
 * @return void
 */
	public function sendOtpOnPhone($mobileNumber = null) {
		$this->autoRender = false ;
		$data = array();
		$mobileNumber = '91'.$mobileNumber;		
		$response = Unirest\Request::get(Configure::read('VerifyMobile.Url').'phone='.$mobileNumber,Configure::read('X-Mashape'));
		$data['code'] = $response->body->data->code;
		$data['message'] = $response->body->data->message;
		return json_encode($data);
	}

/**
 * Method verfifyContact to verify the mobile number
 * @param $mobileNumber (number) and $code (string)
 * @return void
 */
	public function verifyContact($code = null,$mobileNumber = null) {
		$this->autoRender = false ;
		$mobileNumber = '91'.$mobileNumber;
		$data = array();
		$response = Unirest\Request::get(Configure::read('VerifyMobile.Url').'code='.$code.'&phone='.$mobileNumber,Configure::read('X-Mashape'));		
		$data['code'] = $response->body->data->code;
		$data['message'] = $response->body->data->message;
		if($data['code'] == 200) {
			$this->UserProfile->updateAll(
				array(
						'UserProfile.is_mobile_verified' => Configure::read('Bollean.True')
				     ),
				array(
						'UserProfile.user_id' => $this->Auth->user('id')
					)
			);
		}
		return json_encode($data);
		
		
	}

}
