<?php

App::uses('AppController', 'Controller');
require_once(APP . 'Vendor' . DS . 'qrcode' . DS . 'phpqrcode' . DS . 'qrlib.php');

class ReusableTokenController extends AppController
{
	public $components = array('RequestHandler', 'Paginator');


	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('generateTokens','tokens');
                $this->RequestHandler->ext = 'json';
	}


	/******************************************************************************
	 * URI: GET https://www.simplypark.in/ReusableTokens/generateTokens/256/5
	 *
	 * ****************************************************************************/

	public function generateTokens($spaceId,$numTokens,$append = 0)
	{
		$response = array();
		$response['status'] = 1;
		$response['reason'] = 'Generated';

		if($append){
		}else{
			$prefix = "OL".$spaceId;
			$folderName = Configure::read('OfflineTokenImagePath').$spaceId;
			$record = array();

			for($itr=0;$itr < $numTokens; $itr++)
			{
				$singleRecord = array();     
				$token   = uniqid($prefix);
				$qrFileName = WWW_ROOT.$folderName.DS.$token.'.png';
				QRCode::png($token,$qrFileName);

				$singleRecord['space_id'] = $spaceId;
				$singleRecord['token']    = $token;
				$singleRecord['path']     = $qrFileName;
				$singleRecord['created']  = date("Y-m-d H:i");
				$singleRecord['modified'] = date("Y-m-d H:i");

				$record[$itr] = $singleRecord;
			}

			CakeLog::write('debug','Inside generater...' . print_r($record,true));

			if($this->ReusableToken->saveMany($record,array('atomic' => true))){
				CakeLog::write('debug','Offline Passes created');     
				$this->set('data',$response);
				$this->set('_serialize','data');
			}else{
				CakeLog::write('debug','Offline Passes creation failed');     
				$response['status'] = 0;
				$response['reason'] = 'Failed to Generate';
			}


		}

	}	


	/*******************************************************************************
	 * URI : GET https://www.simplypark.in/tokens/spaceId
	 *
	 * ****************************************************************************/   
	public function tokens($spaceId)
	{
		$response = array();
		$response['status'] = 1;
		$records = array();

		$this->loadModel('ReusableToken');
		$tokens = $this->ReusableToken->getReusablePrintTokens($spaceId);

		CakeLog::write('debug','Inside tokens....' . print_r($tokens,true));

		$this->set('data',$response);
		$this->set('_serialize','data');
	}

}
