<?php

App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');

class CancellationController extends AppController 
{



	public function beforeFilter() {
		parent::beforeFilter();
	}
 

   /******************************************************************
      Function Name : getRefund 
      Description   : gets the Refund value and the date on which
                      the refund will be processed
      input         : booking_id as integer,
                      displayText as reference to the text to display
                      refundDate as date in (d-m-Y) format
      returns       : refund value
   ******************************************************************/
/*
public function getRefund($currentDateTime, $bookingStartDateTime, $postStartCancellationDateTime,$preStartCancellationDateTime, $bookingAmount, $handlingCharges, 
                   $ownerServiceTax, $spServiceTax, $spCommission, $bookingType,&$displayText, &$refundDate)
*/

   public function getRefund($booking_id,&$displayText, &$refundDate)
   {

           /*TODO : get values from booking table using booking_id */
           $currentDateTimeObj = new DateTime(); //make sure the default_time_zone is set to Asia/Calcutta
	   $preCancellationDateTimeObj = new DateTime($preStartCancellationDateTime);
	   $postCancellationDateTimeObj = new DateTime($postStartCancellationDateTime);
	   $bookingStartDateTimeObj = new DateTime($bookingStartDateTime);


	   if ( $currentDateTimeObj < $preCancellationDateTimeObj ) 
	   {
		   $refund = $bookingAmount + $handlingCharges;
		   $displayText = "Your cancellation request has been successful. A refund of $refund will be debited to your account in 2 to 5 working days";
		   $refundDate = $currentDateTimeObj->format('d-m-Y');
		   return $refund;

	   }

	   return 0;
   }
   






}
