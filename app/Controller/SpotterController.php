<?php
App::uses('CakeTime', 'Utility');
App::uses('HttpSocket', 'Network/Http');
class SpotterController extends AppController
{
	public $helper = array('Html', 'Form', 'Js', 'Admin','Time');
	public $components = array('RequestHandler', 'Paginator');

	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('add','count');
		$this->RequestHandler->ext = 'json';
	}

/******************************************************************************
 * URI : POST https://www.simplypark.in/blob/<SpaceId>/<action>/<TimeStamp>/<deviceId>.json
 * 
 *
 * ****************************************************************************/   

 public function add($spaceId,$action,$timestamp,$deviceId)
 {
	 CakeLog::write('debug','In add..action ' . $action);
	$response = array();
	$response['status'] = 1;
        $records = array();
	
	$this->set('data',$response);
	$this->set('_serialize','data');
 }
	
/******************************************************************************
 * URI : GET https://www.simplypark.in/count 
 *
 * ****************************************************************************/   
public function count($spaceId)
{
	$response = array();
	$response['status'] = 1;
        $records = array();
	
	$this->set('data',$response);
	$this->set('_serialize','data');
 }
}
