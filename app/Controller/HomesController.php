<?php
class HomesController extends AppController
{
	public $helper = array('Html', 'Form');
	public $components = array('RequestHandler');
	public $layout = 'home';
	public $uses = array('CmsPage', 'Partner', 'Event', 'User', 'UserProfile', 'DiscountCoupon');

	public function beforeFilter() {
		parent::beforeFilter();

		$this->Auth->allow(
				'cms',
				'signup',
				'activateAccount',
				'login',
				'forgotPassword',
				'opauth_complete',
				'resendActivationEmail',
				'resetPassword',
				'ajaxGetCities',
				'contactUs',
				'saveContactUs',
				'operatorLogin',
				'operatorSpaceDetails'
			);
		   $this->_loginWithCookie();
	}

/**
 * Method index to show the landing page of the website
 *
 * @return void 
 */
	public function index() {

		$this->isAuthorized();

		$getDiscountCoupon = $this->__discountCoupon();
		$this->set('getDiscountCoupon', $getDiscountCoupon);

		//getting partners of company to show on landing page
		$getOurPartners = $this->__ourPartners();
		$this->set('getOurPartners', $getOurPartners);

		//getting popular events to show on landing page
		$getPopularEvents = $this->Event->getEvents(4, true);
		$this->set('getPopularEvents', $getPopularEvents);
	}

/**
 * Method __ourPartners to get all the activated partners of the website
 *
 * @return $partners array array of partners
 */
	private function __ourPartners() {
		$partners = $this->Partner->find('list',
				array(
						'conditions' => array(
								'Partner.is_activated' => Configure::read('Bollean.True')
							),
						'order' => 'rand()',
						'limit' => 4,
						'fields' => array(
								'id', 'logo'
							)
					)
			);
		return $partners;
	}

	private function __discountCoupon() {
		$coupon = $this->DiscountCoupon->find('first',
				array(
						'conditions' => array(
								'DiscountCoupon.is_frontend' => Configure::read('Bollean.True')
							),
						'fields' => array(
								'id', 'description'
							)
					)
			);
		return $coupon;
	}

/**
 * Method cms to show all the activated cms pages of the website
 *
 * @return void 
 */
	public function cms($cmsID) {
		$cmsID = urldecode(base64_decode($cmsID));
		$getCms = $this->CmsPage->findById($cmsID,
									array(
										'meta_title',
										'meta_description',
										'meta_keyword',
										'page_name'
										)
					);
		$this->set('getCms', $getCms);
	}

/**
 * Method signup to register new users on the website
 *
 * return void
 */
	public function signup() {
		$this->request->allowMethod('post', 'put');

		$this->User->unvalidate(array('old_password', 'confirm_password'));
		$this->UserProfile->unvalidate(array('company'));
		$this->request->data['User']['user_type_id'] = configure::read('UserTypes.User');

		app::uses('String','Utility');
		$this->request->data['User']['activation_key'] = String::uuid();

		App::uses('CakeTime', 'Utility');
		$this->request->data['User']['persist_code'] = CakeTime::fromString(date('Y-m-d H:i:s'));

		$this->request->data['UserProfile']['confirm_password'] = $this->request->data['User']['password'];

		CakeLog::write('debug','Inside a new user creation...' . print_r($this->request->data,true));
           
	         	
		if ($this->User->saveAssociated($this->request->data)) {
			$this->signUpUserEmail();

			if (isset($this->request->data['User']['booking_start_date'])) {
				if ($this->Auth->login()) {
					$this->Session->setFlash(__('You have been registered successfully. An activation email has been sent to your registered email account. Click on the activation link sent in email to activate your account. Please check your Spam folder too for the activation email. You can book space for now but you have to activate your account to login in future.'), 'default', 'success');
					$this->__redirectToBooking();
				}
			}

			$this->Session->setFlash(__('You have been registered successfully. An activation email has been sent to your registered email account. Click on the activation link sent in email to activate your account. Please check your Spam folder too for the activation email'), 'default', 'success');
			$this->redirect(array('action'=> 'index'));
		}

		$errors = $this->User->validationErrors;
        if (!empty($errors)) {
            $errorMsg = $this->_setSaveAssociateValidationError($errors);
        }
		$this->Session->setFlash(__('Registeration request not completed due to following : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
		$this->redirect('/?errorsignup=1');
	}

/**
 * Method __redirectToBooking to redirect the user to booking process page after login and registeration
 *
 * @return bool
 */
	private function __redirectToBooking() {
		return $this->redirect(
				array(
						'controller' => 'bookings',
						'action' => 'index',
						'stdate' => urlencode(base64_encode($this->request->data['User']['booking_start_date'])),
						'eddate' => urlencode(base64_encode($this->request->data['User']['booking_end_date'])),
						'space_id' => urlencode(base64_encode($this->request->data['User']['space_id'])),
						'space_park_id' => urlencode(base64_encode($this->request->data['User']['space_park_id']))
					)
				);
	}

/**
 * Method __redirectToBooking to redirect the user to booking process page after login and registeration
 *
 * @return bool
 */
	private function __redirectFbToBooking() {
		
		return $this->redirect(
				array(
						'controller' => 'bookings',
						'action' => 'index',
						'stdate' => urlencode(base64_encode(htmlspecialchars($_COOKIE["start-date"]))),
						'eddate' => urlencode(base64_encode(htmlspecialchars($_COOKIE["end-date"]))),
						'space_id' => urlencode(base64_encode(htmlspecialchars($_COOKIE["space-id"]))),
						'space_park_id' => urlencode(base64_encode(htmlspecialchars($_COOKIE["space-park-id"])))
					)
				);
	}

/**
 * Method signUpUserEmail for send registration notification to new user
 *
 * @return void
 */
	private function signUpUserEmail() {
	
		$this->loadModel('EmailTemplate');

		//$link = "<a href=" . Configure::read('ROOTURL').'homes/activateAccount/' .$this->request->data['User']['activation_key']. ">Click Here </a> to activate your account.";
		$link = Configure::read('ROOTURL').'homes/activateAccount/' .$this->request->data['User']['activation_key'];

	   $temp = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => Configure::read('EmailTemplateId.registration_notification'))));
	   $temp['EmailTemplate']['mail_body'] = str_replace(
	    	array('../../..', '#NAME', '#CLICKHERE'),
	    	array(FULL_BASE_URL, 
                      $this->request->data['UserProfile']['first_name'].' '.$this->request->data['UserProfile']['last_name'], 
                      $link), $temp['EmailTemplate']['mail_body']);

                
		return $this->_sendEmailMessage($this->request->data['User']['email'], $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject']);
		
	}

/**
 * Method activateAccount to activate user accounts
 *
 * @param $activationKey activation key of user
 * @return void
 */
	public function activateAccount($activationKey = null) {
		$checkRecord = $this->User->findByActivationKeyAndIsActivated($activationKey, '0', array('id', 'is_activated'));
		if (empty($checkRecord)) {
			$this->Session->setFlash('This link has been expired!', 'default', 'error');
			$this->redirect(array('action' => 'index'));
		}
		
		if($this->User->activateAccount($activationKey)) {
			$this->Session->setFlash(__('Your account has been activated successfully! Please login to continue.'), 'default', 'success');
		} else {
			$this->Session->setFlash(__('Some errors occurred. Please try again.'), 'default', 'error');
		}
		$this->redirect(array('action' => 'index'));
	}

/**
 * Method login to login the user at front end
 *
 * @return void
 */
	public function login() {
                CakeLog::write('debug','In login...' . print_r($this->request,true));
		$this->request->allowMethod('post', 'put');
		if (!empty($this->request->data)) {
			if ($this->Auth->login()) {

				$this->__isUserActivated(); //Check is user activated and not deleted

				$userTypes = array(
					Configure::read('UserTypes.User')
				);

				if (!$this->_checkUserType($userTypes)) { // check if logged in user is not a front end user
					$this->Session->setFlash(
				    		__('you are not authorized to access this location.'),
				    		'default',
				    		'error'
				    	);
					$this->Auth->logout();
					$this->redirect('/?errorlogin=1');
				}
				
				if (isset($this->request->data['User']['booking_start_date'])) {

					//Check space owner and the logged in user both are not same
					if (!$this->_checkSameUser($this->request->data['User']['space_id'])) {
						$this->Session->setFlash(
				    		__('Sorry, you can not book your own space.'),
				    		'default',
				    		'error'
				    	);
						$this->Auth->logout();
						$this->redirect($this->referer());
					}
					$this->_saveLastLogin();
					$this->__redirectToBooking();
				}
				$this->_saveLastLogin();
				$cookie = array(
							'email' => $this->Session->read('Auth.User.email'),
							'persist_code' => $this->Session->read('Auth.User.persist_code')
						       );
				$this->Cookie->httpOnly = true;
				$this->Cookie->write('User', $cookie, true, '7 Days');
				if ($this->request->data['User']['keep_me_logged_in'] == false) {
					$this->Cookie->delete('User');
				}
				
				if (isset($this->request->data['User']['listSpaceRedirect'])) {
					return $this->redirect(array('controller' => 'users', 'action' => 'addSpace'));
				}
				
                                if (isset($this->request->data['User']['isBookingApprovalRedirect'])) {
					return $this->redirect(array('controller' => 'bookings', 'action' => 'listing?booking-tab=2'));
				}
                                
                                if (isset($this->request->data['User']['bulkBookingRedirect'])) {
                                        //CakeLog::write('debug','in Controller .' . print_r($this->request->data['User'],true));
					return $this->redirect(array(
                                                              'controller' => 'spaces', 
                                                              'action' => 'spaceDetailBulk',
                                                               $this->request->data['User']['spaceID']));
				}
				
				return $this->redirect(array('controller' => 'dashboards', 'action' => 'dashboard'));
			}
			$this->Session->setFlash(
				__('Invalid Username or Password'),
				'default',
				'error'
			);
			if (strpos($this->referer(),'?errorlogin=1&listSpaceRedirect=1') !== false) {
			    return $this->redirect($this->referer());
			}
			
			if(strpos($this->referer(),'/spaces/spaceDetail/') !== false && strpos($this->referer(),'stdate') === false) {
				return $this->redirect($this->referer().'?errorlogin=1&stdate='.urlencode(base64_encode($this->request->data['User']['booking_start_date'])).'&edate='.urlencode(base64_encode($this->request->data['User']['booking_end_date'])).'&spaceId='.urlencode(base64_encode($this->request->data['User']['space_id'])).'&space_park_id='.urlencode(base64_encode($this->request->data['User']['space_park_id'])));
			}

			if (strpos($this->referer(),'?errorlogin=1') !== false) {						
				return $this->redirect($this->referer());
			}
			$this->redirect($this->referer().'?errorlogin=1');
		}
	}

/**
 * Method _saveLastLogin to save last login time of the user
 *
 * @return viod
 */
	protected function _saveLastLogin() {
		$this->User->id = $this->Auth->user('id'); // target correct record
        $this->User->saveField('last_login', date(DATE_ATOM)); // save login time
        return true;
	}

/**
 * Method __isUserActivated to to check user is activated and not deleted
 *
 * @return void
 */
	private function __isUserActivated() {
		if ($this->Auth->user('is_activated') == Configure::read('Bollean.False') || $this->Auth->user('is_deleted') == Configure::read('Bollean.True')) {
        // user is not activated or deleted
        	
			if ($this->Auth->user('activation_key') != null) {
				$resendLink = "<a href=" . Configure::read('ROOTURL').'homes/resendActivationEmail/' .$this->Auth->user('persist_code'). "> Resend it?</a>";
				$this->Session->setFlash(
					__("You account is awaiting confirmation from your email account. Haven't received the confirmation email.".$resendLink),
					"default",
					"error"
				);
			} else {
				$this->Session->setFlash(
					__('Your account is blocked by adminsitrator.'),
					'default',
					'error'
				);
			}
       		$this->Auth->logout();
       		$this->redirect('/?errorlogin=1');
		}
		return true;
	}

/**
 * Method forgotPassword to send temporary password to user's email
 *
 * @return void
 */
	public function forgotPassword() {
		$this->request->allowMethod('post','put');
		$this->_forgotPassword();
	}

/**
 * Method opauth_complete to get the response after social networking login
 *
 * @return void
 */
	public function opauth_complete() {

		//If user cancel the facebook login/signup process, then redirect the user to home page
		if (isset($this->request->data['error']) && !empty($this->request->data['error'])) {

			$createAccount = '<a href="#" data-toggle="modal" data-target="#signupModal">create an account</a>';

			$this->Session->setFlash(__('Permission to link your Facebook account was denied. You can try again or '.$createAccount.' without linking your Facebook account.'),'default','success');
			if (isset($_COOKIE['current-action']) && $_SERVER['SERVER_NAME'] != 'localhost') {
				return $this->redirect($_COOKIE['current-action'].'/?errorlogin=1');
			}
			$this->redirect(array('action' => 'index','?' => array('errorlogin' => 1)));
		}

		if (!empty($this->request->data)) {
			$this->request->data['User']['email'] = $this->request->data['auth']['info']['email'];
			$this->request->data['User']['is_activated'] = Configure::read('Bollean.True');
			$this->request->data['User']['password'] = $this->request->data['auth']['uid'];
			$this->request->data['User']['user_type_id'] = configure::read('UserTypes.User');
			$this->request->data['User']['social_network_user'] = Configure::read('Bollean.True');
			$this->request->data['UserProfile']['first_name'] = $this->request->data['auth']['info']['first_name'];
			$this->request->data['UserProfile']['last_name'] = $this->request->data['auth']['info']['last_name'];
			$this->request->data['UserProfile']['profile_pic'] = $this->request->data['auth']['info']['image'];

			App::uses('CakeTime', 'Utility');
			$this->request->data['User']['persist_code'] = CakeTime::fromString(date('Y-m-d'));
			
			if (!$this-> __checkIfUserExists($this->request->data['auth']['info']['email'])) {

				$this->User->unvalidate(array('old_password', 'confirm_password', 'username'));
				$this->UserProfile->unvalidate(array('company', 'profile_pic'));
				unset($this->request->data['auth']);
				unset($this->request->data['timestamp']);
				unset($this->request->data['signature']);
				unset($this->request->data['validated']);

				$this->UserProfile->Behaviors->unload('CakeAttachment.Upload'); // Detach a behavior 'CakeAttachment.Upload' from our model UserProfile
				if ($this->User->saveAssociated($this->request->data)) {
					$id = $this->User->id;
					$this->__loginFbUser($id);
				}
				$errors = $this->User->validationErrors;
		        if (!empty($errors)) {
		            $errorMsg = $this->_setSaveAssociateValidationError($errors);
		        }
				$this->Session->setFlash(__('Registeration request not completed due to following : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
				$this->redirect(array('action' => 'index'));
			}
		}
		
   	}

/**
 * Method __checkIfUserExists to check that if social networking user already exists or not
 *
 * @param string $email email of the user to check
 * @return bool false
 */
   	private function __checkIfUserExists($email) {
   		$getUser = $this->User->findByEmail($email, array('id', 'social_network_user'));
   		if (!empty($getUser)) {
   			if ($getUser['User']['social_network_user'] == Configure::read('Bollean.False')) {
   				$this->Session->setFlash(__('You are already registered on SimplyPark through manual sign up process. Please login manually to continue.'), 'default', 'error');
   				$this->redirect(array('action' => 'index'));
   			}
   			$this->__loginFbUser($email);
   		}
   		return false;
   	}

/**
 * Method __loginFbUser to login social networking user on the website
 *
 * @param string $email email of the user to login
 * @return void
 */
   	private function __loginFbUser($id = null) {
   		
   		$this->request->data['User']['username'] = $this->request->data['User']['email'];

   		if ($this->Auth->login()) {
   			$this->__isUserActivated();
   			if (isset($_COOKIE['start-date'])) {
   				$this->__redirectFbToBooking();
   			}	
   			return $this->redirect($this->Auth->redirectUrl());
   		}
   		$this->Session->setFlash(__('Some errors occurred. Please try again!'), 'default', 'error');
   		$this->redirect(array('action' => 'index'));
   	}

/**
 * Methhod resendActivationEmail to resend activation email to user who lost their activation email
 *
 * @param $persistCode to identify the user with persist code field
 * @return void
 */
   	public function resendActivationEmail($persistCode = null) {
   		$getBlockedUser = $this->User->find('first',
   											array(
   													'conditions' => array(
   														'User.persist_code' => $persistCode
   													),
   													'contain' => array(
   															'UserProfile' => array(
   																	'fields' => array(
   																			'first_name', 'last_name'
   																		)
   																)
   														),
   													'fields' => array('email', 'activation_key')
   											)
   			);

   		$this->loadModel('EmailTemplate');

		//$link = "<a href=" . Configure::read('ROOTURL').'homes/activateAccount/' .$getBlockedUser['User']['activation_key']. ">Click Here </a> to activate your account.";
		$link = Configure::read('ROOTURL').'homes/activateAccount/'.$getBlockedUser['User']['activation_key'];

	   	$temp = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => Configure::read('EmailTemplateId.resend_activation'))));
	   	$temp['EmailTemplate']['mail_body'] = str_replace(
	    	array('#NAME','#CLICKHERE'),
	    	array($getBlockedUser['UserProfile']['first_name'] . ' ' . $getBlockedUser['UserProfile']['last_name'] ,
                      $link), $temp['EmailTemplate']['mail_body']);
 
		
		if ($this->_sendEmailMessage($getBlockedUser['User']['email'], $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject'])) {
			$this->Session->setFlash(__('An activation email has been resent to your associative email account, please click on the activation link in that email to activate your account.'), 'default', 'success');
			$this->redirect('/');
		}

   	}

/**
 * Method resetPassword to reset user password
 *
 * @return void
 */
	public function resetPassword() {
		if ($this->request->is('post')) {

			$getUserID = $this->User->findByPersistCode($this->request->data['User']['persist_code'], array('id'));
			$this->request->data['User']['id'] = $getUserID['User']['id'];
			$this->User->unvalidate(array('username', 'email', 'old_password'));
			unset($this->request->data['User']['persist_code']);
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('Your Password has been changed successfully. Please login again.'), 'default', 'success');
				$this->redirect('/homes?errorlogin=1');
			}
			$errors = $this->User->validationErrors;
            if (!empty($errors)) {
                $errorMsg = $this->_setValidaiotnError($errors);
            }
            $this->Session->setFlash(__('Password reset request not completed due to following error : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
            $this->redirect('/homes/'.urlencode(base64_encode($user['User']['persist_code'])).'?resetpassword=1');
		}
	}

/**
 * Method ajaxGetCities to get cities of selected state
 *
 * @return string $cities json encoded string
 */
    public function ajaxGetCities($stateID = null) {
		$cities = $this->_getCityList($stateID);
		$this->set(
			array(
				'response' => $cities,
				'_serialize' => 'response'
				)
			);
	}

/**
 * Method contactUs to contact with the site admin
 *
 * @return void
 */
	public function contactUs() {
		$data = array();
    	$this->layout = 'home';
    	if($this->request->data) {
    		$recaptcha = $this->request->data['g-recaptcha-response'];
			if (!empty($recaptcha)) {
				$google_url = "https://www.google.com/recaptcha/api/siteverify";
				$secret = Configure::read('GoogleSiteSecretKey.SecretKey');
				$ip = $_SERVER['REMOTE_ADDR'];
				$url = $google_url."?secret=".$secret."&response=".$recaptcha."&remoteip=".$ip;
				$res = $this->_getCurlData($url);
				$res = json_decode($res, true);
				//reCaptcha success check 
				if ($res['success']) {
					$this->loadModel('ContactUs');
					if ($this->ContactUs->save($this->request->data)) {

						if ($this->__contactUsEmail($this->request->data)) {
							$this->Session->setFlash(__('Your query was successfully submitted. We will be in touch. You will now be redirected to the home page.'), 'default', 'success');
							return $this->redirect(array('controller' => 'homes', 'action' => 'contactUs'));
						}
						$this->Session->setFlash(__('Email not sent, Please try again!'), 'default', 'error');
						$data = $this->request->data;
					}
					$errors = $this->ContactUs->validationErrors;
		            if (!empty($errors)) {
		                $errorMsg = $this->_setValidaiotnError($errors);
		            }
		            $this->Session->setFlash(__('Contact us request not completed due to following error : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
		            $data = $this->request->data;
				}
				$this->Session->setFlash(__('Please re-enter your reCAPTCHA.'), 'default', 'error');
				$data = $this->request->data;
			}
			$this->Session->setFlash(__('Please re-enter your reCAPTCHA.'), 'default', 'error');
			$data = $this->request->data;
    	}
    	if(!empty($data)) {
    		$this->set('data', $data);
    	}
    }

/**
 * Method saveContactUs to save contact us information
 *
 * @return void
 */
/*
    public function saveContactUs() { 
    	$this->request->allowMethod('post', 'put');
    	//pr($this->request->data);die;
    	$recaptcha = $this->request->data['g-recaptcha-response'];
		if (!empty($recaptcha)) {
			$google_url = "https://www.google.com/recaptcha/api/siteverify";
			$secret = Configure::read('GoogleSiteSecretKey.SecretKey');
			$ip = $_SERVER['REMOTE_ADDR'];
			$url = $google_url."?secret=".$secret."&response=".$recaptcha."&remoteip=".$ip;
			$res = $this->_getCurlData($url);
			$res = json_decode($res, true);
			//reCaptcha success check 
			if ($res['success']) {
				$this->loadModel('ContactUs');
				if ($this->ContactUs->save($this->request->data)) {

					if ($this->__contactUsEmail($this->request->data)) {
						$this->Session->setFlash(__('Your query was successfully submitted. We will be in touch. You will now be redirected to the home page.'), 'default', 'success');
						$this->redirect($this->referer());
					}
					$this->Session->setFlash(__('Email not sent, Please try again!'), 'default', 'error');
					$this->redirect($this->referer());
				}
				$errors = $this->ContactUs->validationErrors;
	            if (!empty($errors)) {
	                $errorMsg = $this->_setValidaiotnError($errors);
	            }
	            $this->Session->setFlash(__('Contact us request not completed due to following error : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
	            $this->redirect($this->referer());
			}
			$this->Session->setFlash(__('Please re-enter your reCAPTCHA.'), 'default', 'error');
			$this->redirect($this->referer());

		}
		$this->Session->setFlash(__('Please re-enter your reCAPTCHA.'), 'default', 'error');
		$this->redirect($this->referer());
    }
*/
/**
 * Method _getCurlData to ensure google captha from api url
 *
 * @param $url string url to ensure captcha api
 * @return $curlData array response from google
 */
    protected function _getCurlData($url)
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
		$curlData = curl_exec($curl);
		curl_close($curl);
		return $curlData;
	}

/**
 * Methhod resendActivationEmail to resend activation email to user who lost their activation email
 *
 * @param $persistCode to identify the user with persist code field
 * @return void
 */
   	private function __contactUsEmail($userData = array()) {
   		$this->loadModel('EmailTemplate');

		$temp = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => Configure::read('EmailTemplateId.contact_us'))));
	   	$temp['EmailTemplate']['mail_body'] = str_replace(
	    	array('../../..', '#NAME', '#EMAIL', '#MESSAGE'),
	    	array(FULL_BASE_URL, $userData['ContactUs']['name'], $userData['ContactUs']['email'], $userData['ContactUs']['message']), $temp['EmailTemplate']['mail_body']);
		
		if ($this->_sendEmailMessage(Configure::read('Email.EmailAdmin'), $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject'])) {
			return true;
		}
		return false;
	}

/******************************************************************************
 *
 *            Operator APP related functions/enhacements
 *
 *******************************************************************************/

public function operatorLogin()
{
   $this->request->allowMethod('post','put');

   if(!empty($this->request->data))
   {
      //CakeLog::write('debug','Printing Operator ' . print_r($this->request,true));
      $this->request->data['User']['username'] = $this->request->data['username'];
      $this->request->data['User']['password'] = $this->request->data['password'];
      //CakeLog::write('debug','Printing Operator(AFTER) ' . print_r($this->request,true));
   
      if($this->Auth->login())
      {
	      $this->response->statusCode(200);
	      CakeLog::write('debug',"Logged in with User ID : " . CakeSession::read('Auth.User.id'));
	      $userId = CakeSession::read('Auth.User.id');
	      $this->response->header(array("UserID"=> $userId));

      }
      else{
	  CakeLog::write('debug','Inside ...else');
	  $this->response->statusCode(401);
     }


   }else{
	   $this->response->statusCode(400);
   }
   
   $this->autoRender = false;	
}

}
