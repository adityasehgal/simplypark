<?php

App::uses('AppController', 'Controller');

class WithdrawalSourcesController extends AppController {
	public $helper = array('Html');
	public $uses = array(
						'WithdrawalSources',
                        'Bank'
					);

    public function beforeFilter() {
        parent::beforeFilter();
    }

    public function index() {
    	$this->layout = 'user';
    	$data = array();
    	$msg_data = array();

        $withdrawal_source_data = $this->WithdrawalSources->find('all',array(
						'conditions' => array(
							'WithdrawalSources.user_id' => $this->Auth->user('id'),
							//'WithdrawalSources.is_deleted' => 0,
						),
						'fields' => array(
							'WithdrawalSources.id',
							'WithdrawalSources.account_first_name',
							'WithdrawalSources.account_middle_name',
							'WithdrawalSources.account_last_name',
							'WithdrawalSources.account_number',
							'WithdrawalSources.branch_code',
							'WithdrawalSources.ifsc_code',
							'WithdrawalSources.swift_code',
							'WithdrawalSources.address',
							'WithdrawalSources.postal_code',
							'WithdrawalSources.is_activated',
							'City.name',
							'State.name',
						)
					)
				);
        
        if($withdrawal_source_data) {
        	$data['data'] = $withdrawal_source_data;
        } else {
        	$data['no_data_exists'] = 'Currently no Withdrawal Sources Details have been added.';
        }

        $this->set('data',$data);
    }

    public function addWithdrawalSource($id = null) {
    	$this->layout = 'user';
    	$msg = 'save';
    	$id = base64_decode($id);

    	if($id) {
            
    		$withdrawal_source_data = $this->WithdrawalSources->find('first',array(
						'conditions' => array(
							'WithdrawalSources.id' => $id,
						),
						'fields' => array(
							'WithdrawalSources.id',
							'WithdrawalSources.account_first_name',
							'WithdrawalSources.account_middle_name',
							'WithdrawalSources.account_last_name',
							'WithdrawalSources.account_number',
							'WithdrawalSources.branch_code',
							'WithdrawalSources.ifsc_code',
							'WithdrawalSources.swift_code',
							'WithdrawalSources.address',
							'WithdrawalSources.postal_code',
							'WithdrawalSources.is_activated',
							'WithdrawalSources.city_id',
							'WithdrawalSources.state_id',
                            'WithdrawalSources.bank_name',
						)
					)
				);

    		if(!empty($withdrawal_source_data)) {
    			$data['WithdrawalSources'] = $withdrawal_source_data['WithdrawalSources'];
    			$data['city_list'] = $this->_getCityList($withdrawal_source_data['WithdrawalSources']['state_id']);
                
    		}

    	}

    	if(isset($this->request->data['WithdrawalSources'])) {

    		$this->request->data['WithdrawalSources']['user_id'] = $this->Auth->user('id');
        
            $withdrawal_source_count = $this->WithdrawalSources->find('count',array(
                                                                            'conditions' => array(
                                                                                'WithdrawalSources.user_id' => $this->Auth->user('id'),
                                                                                'WithdrawalSources.is_activated' => Configure::read('Bollean.True')
                                                                            )
                ));
            //pr($withdrawal_source_count);die;
            if(!isset($this->request->data['WithdrawalSources']['id'])) {
                $withdrawalSourceActivate = $withdrawal_source_count == 0 ? 1 : 0;
                $this->request->data['WithdrawalSources']['is_activated'] = $withdrawalSourceActivate;    
            }
            
                       // pr($this->request->data);die;
    		if ($this->WithdrawalSources->save($this->request->data)) {
    			if(isset($this->request->data['WithdrawalSources']['id'])) {
    				$msg = 'update';
    			}

                $this->Session->setFlash(__('Withdrawal Sources has been added successfully'), 'default', 'success');

                if (isset($this->request->data['WithdrawalSources']['id'])) {
                    $this->Session->setFlash(__('Withdrawal Sources has been updated successfully'), 'default', 'success');
                }
    			return $this->redirect(Configure::read('ROOTURL').'WithdrawalSources/index/');
    		}

            $errors = $this->WithdrawalSources->validationErrors;
            if (!empty($errors)) {
                $errorMsg = $this->_setValidaiotnError($errors);
            }
			$this->Session->setFlash(__('Your Request could not completed due to following error : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
			$this->redirect($this->referer());
    		
    	}

    	$data['state'] = $this->_getStateList();

		$this->set('data',$data);
    }

    public function activateWithdrawalSources() {
    	$this->WithdrawalSources->updateAll(
                array(
                    'WithdrawalSources.is_activated' => 0
                    )
                );
	    	
    	$this->WithdrawalSources->id = $this->request->data['id'];
    	if($this->WithdrawalSources->save(array('is_activated' => 1))) {
            return $this->Session->setFlash(__('Withdrawal Sources has been successfully Activated'), 'default', 'success');
        }

        $errors = $this->WithdrawalSources->validationErrors;
        if (!empty($errors)) {
            $errorMsg = $this->_setValidaiotnError($errors);
        }
        return $this->Session->setFlash(__('Your Request could not completed due to following error : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');

    	$this->autoRender = false ;
    }

    public function getBankAddress($ifsc_code = null) {
        $this->autoRender = false;
        $bankData = $this->Bank->getBankData($ifsc_code);
        return json_encode($bankData);
       
    }

    public function deleteAccount($id = null) {
        $id = base64_decode($id); 
        if ($id == null) {
           return $this->redirect(Configure::read('ROOTURL').'WithdrawalSources/index/');
        }
        $is_activated = $withdrawal_source_count = $this->WithdrawalSources->find('first',array(
                                                                            'conditions' => array(
                                                                                'WithdrawalSources.id' => $id,
                                                                                //'WithdrawalSources.is_deleted' => Configure::read('Bollean.False')
                                                                            )));

        if ($this->WithdrawalSources->delete($id)) {
            if ($is_activated['WithdrawalSources']['is_activated'] == 1) {
                $withdrawal_source = $this->WithdrawalSources->find('first',array(
                                                                            'conditions' => array(
                                                                                'WithdrawalSources.user_id' => $this->Auth->user('id'),
                                                                                //'WithdrawalSources.is_deleted' => Configure::read('Bollean.False')
                                                                            )
                ));
                if (!empty($withdrawal_source)) {
                    $this->WithdrawalSources->updateAll(
                        array(
                            'WithdrawalSources.is_activated' => 1
                            ),
                        array(
                            'WithdrawalSources.id' => $withdrawal_source['WithdrawalSources']['id']
                            )
                    );    
                }
            }
            $this->Session->setFlash(__('Account has been deleted successfully.'), 'default', 'success');
        } else {
             $this->Session->setFlash(__('Account hasn\'t been deleted.Please Try Again!!'), 'default', 'error'); 
        }
         $this->redirect(Configure::read('ROOTURL').'WithdrawalSources/index/'); 

    }

   
}