<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('HttpSocket', 'Network/Http');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $components = array(
		'Session',
      	'Cookie',
      	'DebugKit.Toolbar',
		'Auth' => array(
	        	'authenticate' => array(
	        		'Form' => array(
						'fields' => array('username' => 'username', 'password' => 'password')
					)
        	    ),
	            'authorize' => 'Controller'
			)
		);

	public $helpers = array('Html', 'Form', 'Session', 'Time');
    public $uses    = array(
            'Event',
            'EventTiming',
            'State',
            'City',
            'Facility',
            'SpaceType',
            'PropertyType',
            'CancellationPolicy'
        );

	public function beforeFilter() {
		if ($this->request->params['controller'] == 'homes') {
			setcookie ("start-date", "", time() - 3600, "/", $_SERVER['SERVER_NAME'] , 1); 
	    	setcookie( "end-date", "", time()- 3660, "/",$_SERVER['SERVER_NAME'], 1);
	    	setcookie( "space-id", "", time()- 3660, "/",$_SERVER['SERVER_NAME'], 1);
	    	setcookie( "space-park-id", "", time()- 3600, "/",$_SERVER['SERVER_NAME'], 1);
	    	setcookie( "current-action", "", time()- 3600, "/",$_SERVER['SERVER_NAME'], 1);
		}
		if ($_SERVER["REMOTE_ADDR"] != Configure::read('Server.Local') && $_SERVER['SERVER_NAME'] == Configure::read('Server.Live')) {
			$this->Security = $this->Components->load('Security');
			$this->Security->blackHoleCallback = 'forceSSL';
			$this->Security->csrfCheck = false;
			$this->Security->validatePost = false;
	        $this->Security->requireSecure();
		}
		$this->checkAdminPrefix();

		$cmsPages = $this->_getCmsPagesTitle(); //Get all the active cms pages of the website
		$this->set('cmsPages', $cmsPages);
	}

	public function forceSSL() {
        return $this->redirect('https://' . env('SERVER_NAME') . $this->here);
    }

	public function beforeRender () {  
      $this->_setErrorLayout();
    }

	function _setErrorLayout() {  
	     if ($this->name == 'CakeError') {  
	        $this->layout = 'error';  
	     }    
	}

   /* a trival space address cache. TODO: should be done properly by cache */
   public $spaceAddressCache;

/**
 * Method checkAdminPrefix to control auth for front end and admin user
 *
 * @return void
 */
	public function checkAdminPrefix() {
		if (isset($this->request->params['prefix']) && $this->request->params['prefix'] == 'admin') {
		    $this->Auth->loginAction = array(
				'controller' => 'admins',
				'action' => 'admin_login',
				'admin' => true
		    );
		    $this->Auth->logoutRedirect = array(
				'controller' => 'admins',
				'action' => 'admin_login',
				'admin' => true
		    );
		    $this->Auth->loginRedirect = array(
				'controller' => 'admins',
				'action' => 'admin_index',
				'admin' => true
		    );

			if ($this->Session->check('Auth')) {
            	$getAdminUserName = $this->_getUserProfileData();
				$this->set('getAdminUserName', $getAdminUserName);
         	}
		} else {
			$this->Auth->loginAction = array(
			'controller' => 'homes',
			'action' => 'index'
		    );
		    $this->Auth->logoutRedirect = array(
			'controller' => 'homes',
			'action' => 'index'
		    );
		    $this->Auth->loginRedirect = array(
			'controller' => 'users',
			'action' => 'parkingSpace'
		    );

		    if ($this->Session->check('Auth')) {
            	$getUserProfile = $this->_getUserProfileData();
				$this->set('getUserProfile', $getUserProfile);
         	}
		}
	}

/**
 * Method isAuthorized callback of each controller 
 * to prevent to open admin panel if frontend user is logged in and vice versa
 *
 * @return string url
 */
	public function isAuthorized() {
		if (isset($this->request->params['prefix']) && $this->request->params['prefix'] == 'admin') {
		    if ($this->Auth->user() && !in_array(
			$this->Auth->user('user_type_id'),
			array(
			    Configure::read('UserTypes.Admin'),
			    Configure::read('UserTypes.SubAdmin')
			    )
			)
			) {
			$this->Session->setFlash(
			    'You are not authorized to access this location',
			    'default',
			    'error'
			);
			return $this->redirect(array('controller' => 'users', 'action' => 'parkingSpace', 'admin' => false));
		    }
		}
		if (!isset($this->request->params['prefix'])) {
		    if ($this->Auth->user() && !in_array($this->Auth->user('user_type_id'), array(Configure::read('UserTypes.User')))) {
			$this->Session->setFlash(
			    'You are not authorized to access this location',
			    'default',
			    'error'
			);
			return $this->redirect(array('controller' => 'admins', 'action' => 'index', 'admin' => true));
		    }
		}
		return true;
	}

/**
 * Method _checkUserType checks if admin user is logging in at frontend or vice versa
 *
 * @return string url
 */
	protected function _checkUserType($userTypes = array()){
		if (in_array($this->Auth->user('user_type_id'), $userTypes)) {
		    return true;
		}
		return false;
	}

/**
 * Method _loginWithCookie to login the user with cookie
 *
 * @return void
 */
	protected function _loginWithCookie() {
		if (!$this->Auth->loggedIn() && $this->Cookie->read('User')) {
                     
		    $this->loadModel('User');
		    $user = $this->User->find('first', array(
				'conditions' => array(
				    'User.email' => $this->Cookie->read('User.email'),
				    'User.persist_code' => $this->Cookie->read('User.persist_code'),
				    'User.is_deleted' => Configure::read('Bollean.False'),
				    'User.is_activated' => Configure::read('Bollean.True')
				)
		    ));

		    if ($user && !$this->Auth->login($user['User']) || empty($user)) {
				return $this->redirect($this->Auth->logout());
		    } else {
				return $this->redirect($this->Auth->redirectUrl());
		    }
		}
	}

/**
 * Method __redirectAfterLogin to check user is admin or subadmin after logged in
 *
 * @return string url
 */
	protected function _redirectAfterLogin(){
		if (
			in_array(
				$this->Session->read('Auth.User.user_type_id'),
				array(
					Configure::read('UserTypes.Admin'),
					Configure::read('UserTypes.SubAdmin')
				)
			)
		) {
			return $this->redirect($this->Auth->redirectUrl());
		}
		return $this->redirect(array('controller' => 'admins', 'action' => 'dashboard', 'admin' => true));
	}

/**
 * Method: checkAdminType to check the logged in user is admin or sub-admin.
 * it also check that while redirecting user to add space page, if user is logged in or not.
 *
 * @return bool
 */
	protected function _checkIfAdminOrUser(){
		if ($this->Session->read('Auth.User.user_type_id') == configure::read('UserTypes.Admin')) {
		    return true;
		}

		if ($this->Session->check('Auth.User') && $this->Session->read('Auth.User.user_type_id') == configure::read('UserTypes.User')) {
			return true;
		}

		return false;
	}
	
/**
 * Method _getAdminUserName to get admin user first name to show in header bar
 *
 * @return $adminUserName array array of first name of admin user name
 */
    protected function _getUserProfileData() {
        $this->loadModel('UserProfile');
        $adminUserName = $this->UserProfile->find('first',
            array(
                'conditions' => array(
                    'UserProfile.user_id' => $this->Session->read('Auth.User.id')
                    ),
                'fields' => array('id', 'first_name', 'last_name', 'profile_pic', 'mobile'),
                'recursive' => -1
                )
            );
        return $adminUserName;
    }
    
/**
 * Method _changePassword Common function to change password functionality for admin and front end users
 *
 * @return void
 */
    protected function _changePassword(){
        $this->loadModel('User');
        $this->User->unvalidate(array('username', 'email'));
        if ($this->User->save($this->request->data)) {
            $this->Session->setFlash(__('Password has been changed successfully.'), 'default', 'success');
        } else {
            $errors = $this->User->validationErrors;
            if (!empty($errors)) {
                $errorMsg = $this->_setValidaiotnError($errors);
            }
            $this->Session->setFlash(__('Password change request not completed due to following error : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
        }
        
        $this->redirect($this->referer());
    }
    
/**
 * Method _setValidationError to make cakephp validation errors when save associate query runs for save user profile
 *
 * @param $errors array the array of errors
 * @return $str the string of errors
 */
    protected function _setSaveAssociateValidationError($errors = array()) {

        $str = null;
        $hasErrors = Hash::extract($errors, '{s}.{n}');
        if (isset($errors['UserProfile']) && !empty($errors['UserProfile'])) {
            $hasErrors = Hash::merge($hasErrors, Hash::extract($errors, 'UserProfile.{s}.{n}'));
        }
        
        if (!empty($hasErrors)) {
            $str = '<ul>';
            foreach ($hasErrors as $key => $val):
                $str .= '<li>'.$val.'</li>';
            endforeach;
            $str .= '</ul>';
        }
        return $str;
    }
    
/**
 * Method _forgotPassword Common function to forgot password functionality for admin and front end users
 *
 * @return void
 */
	protected function _forgotPassword(){
		$this->loadModel('User');
		$getUser = $this->User->find('first', array(
           'conditions' => array(
           		'OR' => array(
               			'User.username' => $this->request->data['User']['username'],
               			'User.email' => $this->request->data['User']['username']
           			)
               ),
           'fields' => array(
               'id', 'username', 'email', 'is_activated', 'persist_code', 'user_type_id'
               ),
           'contain' => array(
               'UserProfile' => array(
                   'fields' => array(
                       'first_name', 'last_name'
                       )
                   )
               )
           )
		);
		if (empty($getUser)) {
           $this->Session->setFlash(__('No such user found. Please input a valid username or email.'), 'default', 'error');
			$this->__manageRedirection();
		}
       
		if ($getUser['User']['is_activated'] == Configure::read('Bollean.False')) {
			$this->Session->setFlash(__('Your account is blocked by administrator.'), 'default', 'error');
			$this->__manageRedirection();
		}
			
		if ($this->_sendForgotPasswordEmail($getUser)) {
			$this->Session->setFlash(__('Please check your mailbox, An instruction has been sent to reset your password.'), 'default', 'success');
		} else {
			$this->Session->setFlash(__('Some error occurred. Please try again'), 'default', 'error');
		}
		$this->__manageRedirection();
	}

/**
 * Method __manageRedirection to manage redirection urls for admin and front end users
 *
 * @return void
 */
	protected function __manageRedirection() {
		if (!isset($this->request->params['prefix'])) {
			$this->redirect('/?forgotpassword=1');
		}
		$this->redirect($this->referer());
	}
    
/**
 * Method _sendForgotPasswordEmail to get email template and send email with temporary password
 *
 * @return void
 */
	protected function _sendForgotPasswordEmail($user = array()) {
		$this->loadModel('EmailTemplate');

		//$link = "<a href=" . Configure::read('ROOTURL').'admin/admins/resetPassword/' .urlencode(base64_encode($user['User']['persist_code'])). ">Click Here </a> to reset your password.";
		$link = Configure::read('ROOTURL').'admin/admins/resetPassword/' .urlencode(base64_encode($user['User']['persist_code']));

		if ($user['User']['user_type_id'] == Configure::read('UserTypes.User')) {
			//$link = "<a href=" . Configure::read('ROOTURL').'?resetpassword=1&persistcode=' .urlencode(base64_encode($user['User']['persist_code'])). ">Click Here </a> to reset your password.";
			$link = Configure::read('ROOTURL').'?resetpassword=1&persistcode=' .urlencode(base64_encode($user['User']['persist_code']));
		}

      	$temp = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => Configure::read('EmailTemplateId.forgot_password_email'))));
      	$temp['EmailTemplate']['mail_body'] = str_replace(
            array('#USERNAME', '#CLICKHERE'),
            array($user['User']['username'],$link), $temp['EmailTemplate']['mail_body']);
  
        
      	   return $this->_sendEmailMessage($user['User']['email'], $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject']);
             return 1;
	}
    
/**
 * Method: send_email_message common function to send emails
 *
 * @param $to string the user email to whom the email will be sent
 * @param $email_body the body of the email
 * @param $subject the subject of the email
 */
	public function _sendEmailMessage($to = null, $email_body = null, $subject = null, $bcc = null, $cc = null)
        {
	App::uses('CakeEmail', 'Network/Email');
      	$Email = new CakeEmail();
        $Email->config('godaddy');
      	$Email->emailFormat('both');
      	$Email->from(array(Configure::read('Email.EmailMarketing') => Configure::read('Email.EmailName')));
      	$Email->sender(Configure::read('Email.EmailMarketing'), Configure::read('Email.EmailName'));
      	$Email->to($to);
      	if ($bcc) {
      		$Email->bcc($bcc);
      	}
      	if ($cc) {
      		$Email->cc($cc);
      	}
		$Email->subject($subject);
		if ($Email->send($email_body)) {
			return true;
		}
		return false;
    }
    
/**
 * Method _setValidaiotnError to make cakephp validation errors when save associate query runs for save user profile
 *
 * @param $errors array the array of errors
 * @return $str the string of errors
 */
    protected function _setValidaiotnError($errors = array()) {

        $str = null;
        if (!empty($errors)) {
            $str = '<ul>';
            foreach ($errors as $key => $val):
                $str.= '<li>'.$val[0].'</li>';
            endforeach;
            $str .= '</ul>';
        }
        return $str;
    }
    
/**
 * Method _changeAccountStatus common function to change status
 *
 * @param $recArray array of record
 * @param $mdoel string name of the model
 * @return bool
 */
    protected function _changeAccountStatus($recArray = array(), $model =null) {
        $this->loadModel($model);
        $status = $recArray[$model]['is_activated'] == configure::read('Activate.False') ? configure::read('Activate.True') : configure::read('Activate.False');
        $this->$model->updateAll(
                array(
                    $model.'.is_activated' => $status
                    ),
                array(
                    $model.'.id' => $recArray[$model]['id']
                    )
                );
        return $status;
    }
    
/**
 * Method _deleteAccount common function to delete user
 *
 * @param $id int ID of the user in table User.
 * @return bool
 */
	protected function _deleteAccount($id = null, $deleteCheck = null, $model = null) {
		$deleteFlag = $deleteCheck == configure::read('Bollean.False') ? configure::read('Bollean.True') : configure::read('Bollean.False');
		$statusFlag = $deleteFlag == configure::read('Bollean.False') ? configure::read('Bollean.True') : configure::read('Bollean.False');
		$this->loadModel($model);
      	$this->$model->updateAll(
      			array(
                    $model.'.is_deleted' => $deleteFlag,
                    $model.'.is_activated' => $statusFlag
                    ),
               array(
                    $model.'.id' => $id
                    )
               );
		return $deleteFlag;
	}

/**
 * Method _changeAccountStatusByActive common function change status from active to inactive and vice-versa
 *
 * @param $recArray array array of status
 * @param $model string name of the model for handle dynmically
 * @return bool
 */
    protected function _changeAccountStatusByActive($recArray = array(), $model = null) {
        $this->loadModel($model);
        $status = $recArray[$model]['is_active'] == configure::read('Activate.False') ? configure::read('Activate.True') : configure::read('Activate.False');
        if ($status == Configure::read('Bollean.False') && $model == 'DiscountCoupon') {
        	if (!$this->$model->validateDiscountCouponStatus($recArray[$model]['id'])) {
        		$this->Session->setFlash(__('Please choose another discount coupon to show on frontend first.'), 'default', 'error');
        		$this->redirect($this->referer());
        	}
        }

        $this->$model->updateAll(
                array(
                    $model.'.is_active' => $status
                    ),
                array(
                    $model.'.id' => $recArray[$model]['id']
                    )
                );
        return $status;
    }

/**
 * Method _getCmsPagesTitle function used to get all the active cms pages title to be shown at footer of the website.
 *
 * @return $allCmsPages array array of cms pages
 */
	protected function _getCmsPagesTitle() {
    	$this->loadModel('CmsPage');
    	$allCmsPages = $this->CmsPage->find('list',
    						array(
    							'conditions' => array(
    													'CmsPage.is_activated' => Configure::read('Activate.True')
    												),
    							'fields' => array(
    												'id', 'page_name'
    											)
    							)
    					);
    	return $allCmsPages;
    }

    protected function _getStateList() {
    	$states = $this->State->find('list',array(
				'conditions' => array(
					'State.is_activated' => 1
				),
				'fields' => array(
					'id', 'name'
				),
				'recursive' => -1
			)
		);
		return $states;
    }

/**
 * Method _getEventSates function used to get all the states where the event will occur
 * @param $event_addresses array 
 * @return $states array 
 */
protected function _getEventStates($event_addresses = array()) 
{
        $states = array();
	foreach ($event_addresses as $key => $value) {
		$states[$value['state_id']] = $value['State']['name']; 	
	}

	return $states;

}

    protected function _getCityList($state_id) {
    	$cities = $this->City->find('list',array(
				'conditions' => array(
					'City.state_id' => $state_id,
					'City.is_activated' => 1
				),
				'order' => 'City.name ASC',
				'fields' => array(
					'name'
				),
				'recursive' => -1
			)
		);
		return $cities;
    }

    protected function _getSpaceTypeList() {
    	$space_types = $this->SpaceType->find('list',array(
				'conditions' => array(
					'SpaceType.is_activated' => 1
				),
				'fields' => array(
					'id', 'name'
				),
                                'order' => 'SpaceType.id',
				'recursive' => -1
			)
		);
		return $space_types;
    }

    protected function _getPropertyTypeList() {
    	$property_types = $this->PropertyType->find('list',array(
				'conditions' => array(
					'PropertyType.is_activated' => 1
				),
				'fields' => array(
					'id', 'name'
				),
				'recursive' => -1
			)
		);
		return $property_types;
    }
    
    protected function _getFacilitiesList() {
    	$facilities = $this->Facility->find('list',array(
				'conditions' => array(
					'Facility.is_activated' => 1
				),
				'fields' => array(
					'id', 'name'
				),
				'recursive' => -1
			)
		);
		return $facilities;
    }

    protected function _getCancellationPolicy() {
    	$policies = $this->CancellationPolicy->find('list',array(
				'fields' => array(
					'id', 'policy'
				),
				'recursive' => -1
			)
		);
		return $policies;
    }

    /**
 * Method _sendForgotPasswordEmail to get email template and send email with temporary password
 *
 * @return void
 */
protected function _sendAddSpaceEmail($space = array()) 
{
	$this->loadModel('EmailTemplate');
	$temp = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => Configure::read('EmailTemplateId.add_space'))));

	$temp['EmailTemplate']['mail_body'] = str_replace(
			array('#HPRICE','#DPRICE','#WPRICE','#MPRICE','#YPRICE','#TPRICE','#ADDRESS', '#OWNERNAME','#PTYPE'),
			array($space['hpricing'],
                              $space['dpricing'],
                              $space['wpricing'],
                              $space['mpricing'],
                              $space['ypricing'],
                              $space['tier_pricing_support'],
                              $space['address'],
                              $space['username'],
                              $space['propertyType']), $temp['EmailTemplate']['mail_body']);

	return $this->_sendEmailMessage(Configure::read('Email.EmailAdmin'), $temp['EmailTemplate']['mail_body'], $space['subject']);
}

/**
 * Method _updateUserForRemovePicture common  function to run query for update user table so that when user wants to remove profile pic only one query can execute
 *
 * @return bool
 */
    protected function _updateUserForRemovePicture() {
    	$this->loadModel('UserProfile');
    	if (
    		$this->UserProfile->updateAll(
    			array(
    					'UserProfile.profile_pic' => null
    				),
    			array(
    					'UserProfile.user_id' => $this->Auth->user('id')
    				)
    		)
    		) {
    			$this->Session->write('Auth.User.UserProfile.profile_pic', null);
    			return true;
    	}
    	return false;
    }

/**
 * Method _getRateFromTime to get space rate from time difference
 *
 * @param $numOfDays int the total days for booking
 * @return $rate float the total rate for booking space
 */
	protected function _getRateFromTime($numOfDays = null, $numOfHours = null, $spaceID = null) {

		$this->loadModel('SpacePricing');
		$getSpacePrices = $this->SpacePricing->getSpacePrices($spaceID); //getting the space prices

		$rate = '';
		switch ($numOfDays) {
			case '0':
				$rate = number_format((float)$getSpacePrices['SpacePricing']['hourly'] * $numOfHours, 2, '.', '');
				break;

			case ($numOfDays > 0 && $numOfDays < 7):
				$rate = number_format((float)$getSpacePrices['SpacePricing']['daily'] * $numOfDays, 2, '.', '');
				break;

			case ($numOfDays >= 0 && $numOfDays < Configure::read('MonthDays')):
                $numOfWeeks = $numOfDays/7;    
				$rate = number_format((float)$getSpacePrices['SpacePricing']['weekely'] * $numOfWeeks, 2, '.', '');
				break;
			
			case ($numOfDays >= Configure::read('MonthDays') && $numOfDays <= Configure::read('YearDays')):
				//Commenting below line because now we are treating this also as a long term booking
				//$numOfMonths = $numOfDays/30;
				//$rate = $getSpacePrices['SpacePricing']['monthly'] * $numOfMonths;
				$rate = $getSpacePrices['SpacePricing']['monthly'];
				break;

			default: 
				$rate = $getSpacePrices['SpacePricing']['yearly'];
				break;
		}
		return $rate;
	}

/**
 * Method _getRateFromTime to get space rate from time difference
 *
 * @param $numOfDays int the total days for booking
 * @return $rate float the total rate for booking space
 */
	protected function _getBookingTypeFromTime($numOfDays = null, $numOfHours = null, $spaceID = null) {

		$this->loadModel('SpacePricing');
		$getSpacePrices = $this->SpacePricing->getSpacePrices($spaceID); //getting the space prices

		$rate = '';
		switch ($numOfDays) {
			case '0':
				$rate = number_format((float)$getSpacePrices['SpacePricing']['hourly'] * $numOfHours, 2, '.', '');
				break;

			case ($numOfDays > 0 && $numOfDays < 7):
				$rate = number_format((float)$getSpacePrices['SpacePricing']['daily'] * $numOfDays, 2, '.', '');
				break;

			case ($numOfDays >= 0 && $numOfDays < Configure::read('MonthDays')):
                $numOfWeeks = $numOfDays/7;    
				$rate = number_format((float)$getSpacePrices['SpacePricing']['weekely'] * $numOfWeeks, 2, '.', '');
				break;
			
			case ($numOfDays >= Configure::read('MonthDays') && $numOfDays <= Configure::read('YearDays')):
				//Commenting below line because now we are treating this also as a long term booking
				//$numOfMonths = $numOfDays/30;
				//$rate = $getSpacePrices['SpacePricing']['monthly'] * $numOfMonths;
				$rate = $getSpacePrices['SpacePricing']['monthly'];
				break;

			default: 
				$rate = $getSpacePrices['SpacePricing']['yearly'];
				break;
		}
		return $rate;
	}
/**
 * Method _getGloballyCommission to get the globally set commission for all spaces
 *
 * @return $service_tax_charges_data array containing the globally set commission
 */
	protected function _getGloballyCommission() {
		$this->loadModel('ServiceTaxCharges');
		$service_tax_charges_data = $this->ServiceTaxCharges->find('first',array(
						'fields' => array(
							'ServiceTaxCharges.commission_paid_by',
							'ServiceTaxCharges.commission_percentage'
						)
					)
				);
		return $service_tax_charges_data;
	}

/**
 * Method _getNumberOfDays common function to get the number of total days months and years between two dates
 *
 * @param $startDate datetime start date time to calculate
 * @param $endDate datetime end date time to calculate
 * @return $interval json object containing total number of years and months and days
 */
	protected function _getNumberOfDays($startDate = null, $endDate = null) {
		$sDate =  new DateTime($startDate);
        $eDate =  new DateTime($endDate);
        $interval = $sDate->diff($eDate);
        if ($sDate->format('d') == '01' && $sDate->format('m') == '01' && ($eDate->format('d') == '28' || $eDate->format('d') == '29')  && $eDate->format('m') == '01') {
        	$interval->month = 'feb';
        }
        
		// if ()
		return $interval;
	}

/**
 * Method _getBookingPrice common function to get the booking Price, given 
 *
 * @param $diffInterval dateInterval object 
 * @param $operatingHours operatingHours of the space in number of hours 
 * @param $hourlyPrice price to be charged per hour 
 * @param $dailyPrice price to be charged daily 
 * @param $weeklyPrice price to be charged weekly 
 * @param $monthlyPrice price to be charged monthly 
 * @param $yearlyPrice price to be charged yearly 
 * @return bookingPrice 
 */
 function _getBookingPrice($diffInterval,$operatingHours,$hourlyPrice,$dailyPrice,$weeklyPrice,$monthlyPrice,$yearlyPrice)
 {
        $bookingPrice = 0;
	if ( $diffInterval->y )
	{
		$bookingPrice = $yearlyPrice;
	}
	else 
	{
           if ( $diffInterval->m )
           {
              $bookingPrice = ( $diffInterval->m * $monthlyPrice);
              if ( $diffInterval->d )
              {
                 $dailyPriceFromMonthly = ceil($monthlyPrice/30);
                 $bookingPrice += ($dailyPriceFromMonthly * $diffInterval->d);
              }

             if ( $diffInterval->h )
             {
                $dailyPriceFromMonthly = ceil($monthlyPrice/30);
                $hourlyPriceFromMonthly = ceil($dailyPriceFromMonthly/$operatingHours);

                $bookingPrice += ($hourlyPriceFromMonthly * $diffInterval->h);
             }
           }
          else if ( $diffInterval->d )
	  {
		  $numOfWeeks = (int)($diffInterval->d/7);
		  //echo "<br />Num of Weeks $numOfWeeks";
		  $daysRemaining = $diffInterval->d%7;
		  //echo "<br />Num of remainingdays $daysRemaining";

		  if ( $numOfWeeks )
		  {
			  $bookingPrice += ($weeklyPrice * $numOfWeeks);
                          //echo "<br />bookingPrice:: $bookingPrice";
                          $dPrice = ceil($weeklyPrice/7);
                          //echo "<br />Daily Price from weekly:: $dPrice";
			  
                          if ( $daysRemaining )
			  {
			     $bookingPrice += ($daysRemaining * ($dPrice));
                             //echo "<br />bookingPrice 2:: $bookingPrice";
			  }
             
                         if ( $diffInterval->h )
                          {
                             $hPrice = ceil($dPrice/$operatingHours);
                             //echo "<br />Hourly Price from weekly:: $hPrice";
                             $bookingPrice += ($hPrice * $diffInterval->h);
                             //echo "<br />bookingPrice 3:: $bookingPrice";
                          }
		  }
		 else 
		 {
			 $bookingPrice += ($dailyPrice * $diffInterval->d);           
                         
                        if ( $diffInterval->h )
                          {
                             $hPrice = ceil($dailyPrice/$operatingHours);
                             //echo "<br />Hourly Price from daily:: $hPrice";
                             $bookingPrice += ($hPrice * $diffInterval->h);
                          }
		 }
	  }
	else if ( $diffInterval->h )
	 {
	    $bookingPrice += $diffInterval->h * $hourlyPrice;
	 } 
       } 

   return $bookingPrice;
}
/**
 * Method _getEventCities function used to get all the cities where the event will occur
 * @param $event_addresses array 
 * @return $states array 
 */
    protected function _getEventCities($event_addresses = array()) {
 		foreach ($event_addresses as $key => $value) {
 			$cities[$value['city_id']] = $value['City']['name']; 	
 		}
 		
 		return $cities;


    }

/**
 * Method _getEventMonths function used to get all the months where the event will occur
 * @param $event_id  
 * @return $months array 
 */
protected function _getEventMonths($event_id = null) 
{
	//pr($event_id);
	$eventTimings = $this->EventTiming->find('all',
			array(
				'conditions' => array(
					'EventTiming.event_id' => $event_id
					),
				'fields' => array(
					'from',
					'to'
					)
			     )
			);

        $months = array();
	foreach ($eventTimings as $key => $value) 
	{
		$from = new DateTime($value['EventTiming']['from']);
		$to =  new DateTime($value['EventTiming']['to']);

		$start    = $from->modify('first day of this month');
		$end      = $to->modify('first day of this month');

		$interval = DateInterval::createFromDateString('1 month');
		$period   = new DatePeriod($start, $interval, $end);

		foreach ($period as $dt) 
		{
		   $months[$dt->format('m')] = $dt->format('F');
		}	
		$months[$to->format('m')] = $to->format('F');
	}

	return array_unique($months);
}


/**
 * Method _sendMessageNotification common function to send messages to user's mobile
 *
 * @param $mobileNumber int the mobile number of the user
 * @param $smsApiMobile string mobile number as requested by SMS Api
 * @return true if valid mobile recevied, false otherwise
 */
public function _validateMobile($mobileNumber,&$smsApiMobileNumber) 
{
   $mobileNumberStr = strval($mobileNumber);

   preg_match("/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/", $mobileNumberStr, $outputArr);
   //CakeLog::write('debug', 'in validateMobile...' . print_r($outputArr,true));

   if(empty($outputArr)){
      CakeLog::write('error', 'Received Mobile Number('.$mobileNumber. ') is not in valid format');
      return false;
   }

   $smsApiMobileNumber = substr($outputArr[0],-10); //only 10 digits are valid
 
   $smsApiMobileNumber = "91".$smsApiMobileNumber;

   return true;
}
/**
 * Method _sendMessageNotification common function to send messages to user's mobile
 *
 * @param $mobileNumber int the mobile number of the user
 * @param $msg text urlencoded text to send on user's mobile as message
 * @return $response obj returned from api
 */
    public function _sendMessageNotification($mobileNumber = null, $msg = null) {
    	        $request =""; //initialise the request variable
		$param['method']= "sendMessage";
		$param['send_to'] = $mobileNumber;
		$param['msg'] = $msg;
		$param['userid'] = Configure::read('SMSAPICredentials.UserID');
		$param['password'] = Configure::read('SMSAPICredentials.Password');
		$param['v'] = "1.1";
		$param['msg_type'] = "TEXT"; //Can be "FLASH”/"UNICODE_TEXT"/”BINARY”
		$param['auth_scheme'] = "PLAIN";
		//Have to URL encode the values
		foreach($param as $key=>$val) {
                        //CakeLog::write('debug','key = ' . $key . ' and value = ' . $val);
			$request.= $key."=".urlencode($val);
			//we have to urlencode the values
			$request.= "&";
			//append the ampersand (&) sign after each
			//parameter/value pair
		}
		$request = substr($request, 0, strlen($request)-1);
		//remove final (&) sign from the request
		$url = Configure::read('SMSAPICredentials.URL').$request;
		
		$socket = new HttpSocket();
                $response = $socket->post($url);
        return $response;
    }
    /**
 * Method __checkSameUser to check that the space user and the logged in user both are not same
 *
 * @param $sapceID int the id of the space
 * @return bool
 */
	protected function _checkSameUser($sapceID = null) {
		$this->loadModel('Space');
		$getSpaceUser = $this->Space->findById($sapceID,array('user_id'));
		if ($getSpaceUser['Space']['user_id'] == $this->Auth->user('id')) {
			return false;
		}
		return true;
	}

/**
 * Method notifyAllApps : This function will notify all apps about the occupancy number
 * @return void
 */
public function notifyAllApps($spaceId,$tokenList,$actionSpecificParams)
{
  $url = Configure::read('OperatorGCM.url');
  //$url = "http://localhost:3000";
  //CakeLog::write('debug','URL is ' . $url);
  $authKey = 'key='.Configure::read('OperatorGCM.serverKey');

  CakeLog::write('debug','In notifyAllApps, tokenList = ' . print_r($tokenList,true));

  $requestHeaders = array(
	           'header'=> array('Content-Type' => 'application/json',
		                     'Authorization' => $authKey,
				     'delay_while_idle' => false 
			           )
	          );




  $socket = new HttpSocket();
  CakeLog::write('debug','INside notifyAll, actionSpecicParams :: ' . print_r($actionSpecificParams,true));
  $body = array(
	  'registration_ids' => array_values($tokenList),
	  'priority' => 'high',
	  'data' => $actionSpecificParams,
            );

  $jsonData = json_encode($body);

  //CakeLog::write('debug','Printing JSON data ' . print_r($jsonData,true)); 
  $response = $socket->post($url,$jsonData,$requestHeaders);

  if($response->code == Configure::read('HTTPStatusCode.OK')){
      CakeLog::write('debug','Widgets update succeeded');
  }else{
      CakeLog::write('error','Widgets update failed with code ' . $response->code . 'and reason ' . $response->reasonPhrase);
      CakeLog::write('debug','Widgets update failed ' . print_r($response->raw,true));
  }
			                   
}

/**
 * Method _checkEmptySlot to check that is there any slot available to park at the requested date and time
 *
 * @return bool
 */
protected function _getEmptySlot($numSlotsToBook,$spaceID,$startDateStr,$endDateStr,&$spaceParkId) 
{

	$startDate = date('Y-m-d H:i:s', strtotime($startDateStr));
	$endDate = date('Y-m-d H:i:s', strtotime($endDateStr));

	$totalSlots = $this->Space->getAllSlots($spaceID);
	$getBookedSlots = $this->Booking->getBookedRecordsOfSpace($spaceID,$startDate,$endDate);
	//CakeLog::write('debug','In checkEmptySlot ...' . print_r($getBookedSlots,true));
	//CakeLog::write('debug','In checkEmptySlot TOTAL SLOTS' . print_r($totalSlots,true));
	//CakeLog::write('debug','In checkEmptySlot numSlotsTOBook ' . print_r($numSlotsToBook,true));


	if (empty($getBookedSlots)) {
		$this->loadModel('SpacePark');
		$slotID = $this->SpacePark->getSlot($spaceID);
                //CakeLog::write('debug','Slot ID ' . print_r($slotID,true));
		if(!empty($slotID)) {
			$spaceParkId = $slotID['SpacePark']['id'];
			return true;	
		} 
		return false;	
	} else {
                 $getBookedSlots = array_map("unserialize", array_unique(array_map("serialize", $getBookedSlots)));
		 //CakeLog::write('debug','after applying array_map' . print_r($getBookedSlots,true));
                
                $nextAvailableSlotId = 0;
                $numBookedSlots = 0;

                foreach($getBookedSlots as $slots)
                {
                   $numBookedSlots += $slots['Booking']['num_slots'];
                }
                $nextAvailableSlotId = $getBookedSlots[0]['Booking']['space_park_id'] + $numBookedSlots;

                //CakeLog::write('debug','Next Available SLOT ID ' . $nextAvailableSlotId); 

                if($numBookedSlots + $numSlotsToBook <= $totalSlots['Space']['number_slots'])
                {
                   $spaceParkId = $nextAvailableSlotId;
		   return true;
		}
		return false;
	}
		
}

/******************************************************************************
 * Function   : _saveBlob
 * Description: Saves the file returned by the Spotter module 
 *              The image is saved under a folder named $spaceId/$action/$device/$timestamp
 *
 * ****************************************************************************/
protected function _saveBlob($spaceId,$action,$timestamp,$device,$srcFilePath,&$savedFilePath)
{	
   $imagesFolder = Configure::read('SpotterImagePath');	
   $folderUrl = WWW_ROOT.$imagesFolder.$spaceId.DS.$action.DS.$device;

   CakeLog::write('debug','_saveBlob...FolderURL ' . $folderUrl);
   if(!is_dir($folderUrl)){
      mkdir($folderUrl,0777,true);
   }

   $fileName = $folderUrl.DS.$timestamp;
   CakeLog::write('debug','_saveBlob... FileName ' . $fileName);

   if(copy($srcFilePath,$fileName)){
	   $savedFilePath = $fileName;
	   return true;
   }else{
	   return false;
   }
}

}
