<?php

class CommandcentreController extends AppController
{
	public $helper = array('Html', 'Form', 'Js', 'Admin', 'UserData');
	public $components = array('RequestHandler', 'Paginator');
	public $uses = array('User','UserAddress','UserProfile');


	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index','login');
		$this->_loginWithCookie();
	}

	/******************************************************************************
	 *
	 * 
	 *
	 * ****************************************************************************/

	public function index()
	{
		$this->layout='command_centre_layout';
	}
	
	/******************************************************************************
	 *
	 * 
	 *
	 * ****************************************************************************/

	public function login()
	{
		CakeLog::write('debug','In CommandCentre Login ....' . print_r($this->request->data,true));
		if ($this->Auth->loggedIn()) {
			$this->redirect(array("controller" => "Commandcentre",
			                      "action" => "index"));
		}	
		$this->layout='command_centre_login_layout';

		if($this->request->is('post')){
			CakeLog::write('debug','Inside login....is Post is true');
			if($this->Auth->login()){
				if($this->Auth->user('is_activated') == Configure::read('Bollean.False') 
					||	$this->Auth->user('is_deleted') == Configure::read('Bollean.True')){
					$this->Session->setFlash(__('Your account is blocked by Administrator.'),
						'default',
						'error');
					return $this->redirect($this->Auth->logout());

				}

				$userTypes = array(Configure::read('UserTypes.Admin'),
					Configure::read('UserTypes.SubAdmin')
				);


				if(!$this->_checkUserType($userTypes)){
					$this->Session->setFlash(
						__('you are not authorized to access this location.'),
						'default',
						'error'
					);
					return $this->redirect($this->Auth->logout());
				}
				if ($this->data['User']['keep_me_logged_in'] == false) {
					$this->Cookie->delete('User');
				}
				$cookie = array(
					'email' => $this->Session->read('Auth.User.email'),
					'persist_code' => $this->Session->read('Auth.User.persist_code')
				);
				$this->Cookie->httpOnly = true;
				$this->Cookie->write('User', $cookie, true, '7 Days');
				CakeLog::write('debug','In CommandCentre...login redirectUrl ' . print_r($this->Auth->redirectUrl(),true));
			        return $this->redirect(array("controller" => "Commandcentre",
			                                     "action" => "index"));

			}
			$this->Session->setFlash(
				__('Invalid Username or Password'),
				'default',
				'error'
			);
		}
	}


}
