<?php

App::uses('AppController', 'Controller');

/**
 * Class to handle Users and its Operations.
 * @author Abha Sood Mahindra
 * @link http://www.ucodesoft.net
 */
class DashboardsController extends AppController {

	var $uses = array('Booking','Space','User');
	var $profilePercentage = '45.45';

	public function beforeFilter() {
    	parent::beforeFilter();
    }

    public function index() {
    	die('Login Sucessfull');
    }

/*
 * Method dashboard to show the user the activaity of their added spaces
 *
 * @return void
 */
    public function dashboard() {
        $this->layout = 'user';

        //Getting new booking after last zzlogin
        $getNewBookings = $this->Booking->newBookings();
        $this->set('getNewBookings',$getNewBookings);

        //Getting active parking spaces
        $getActiveParkings = $this->Space->activeSpaces();
        $this->set('getActiveParkings',$getActiveParkings);

        //Getting active parking spaces
        $confirmedAndPaidAmount = $this->Booking->confirmedAndPaidAmount();
        $this->set('confirmedAndPaidAmount',$confirmedAndPaidAmount);

        //Getting active parkinf spaces
        $getUserProfileData = $this->User->getUserDataToEdit($this->Session->read('Auth.User.id'));
   		$this->_getProfileCompletePercentage($getUserProfileData);

   		$this->set('profilePercentage',round($this->profilePercentage));

        //Getting owner income
        $getOwnerIncome = $this->Booking->ownerIncome();
        $this->set('getOwnerIncome',$getOwnerIncome);

   		//Getting booked spaces
        $getBookingsSummary = $this->Booking->getBookingSummary();
        $this->set('getBookingsSummary',$getBookingsSummary);

        //Getting current status of user spaces
        $getCurrentSpaceStatus = $this->Space->getCurrentSpaces();
        $this->set('getCurrentSpaceStatus', $getCurrentSpaceStatus);

         //Getting current status of user spaces
        $getUnderApprovalBookings = $this->Booking->getUnderApprovalBookingsCount();
        $this->set('getUnderApprovalBookings', $getUnderApprovalBookings);
    }


/**
 * Method print_ticket to print all the booking details
 *
 * @return void 
 */

    public function print_ticket($bookingId = null) {
        $this->layout = 'print_layout';
        $this->request->allowMethod('get');
        if($bookingId != null) {
            $bookingId = urldecode(base64_decode($bookingId));
            $getBookingData = $this->Booking->getBookingInfo($bookingId);
            $this->set('getBookingData',$getBookingData);
        } else {
            $this->redirect(array('controller' => 'bookings','action' => 'listing'));
        }
    }
    
/**
 * Method _getProfileCompletePercentage to calculate percentage of profile compeltion
 *
 * @param $userProfileData array contains the user profile data
 * @return void
 */
    protected function _getProfileCompletePercentage($userProfileData) {
    	if (!empty($userProfileData['UserCar'])) {
    		$this->profilePercentage += Configure::read('UserPerFieldPercentage');
    	}
    	if (isset($userProfileData['UserCar'][0])) {
    		$this->profilePercentage += Configure::read('UserPerFieldPercentage');
    	}
    	if (isset($userProfileData['UserCar'][1])) {
    		$this->profilePercentage += Configure::read('UserPerFieldPercentage');
    	}
    	if (!empty($userProfileData['UserProfile']['profile_pic'])) {
    		$this->profilePercentage += Configure::read('UserPerFieldPercentage');
    	}
    	if (!empty($userProfileData['UserProfile']['company'])) {
    		$this->profilePercentage += Configure::read('UserPerFieldPercentage');
    	}
    	if (!empty($userProfileData['UserProfile']['tan_number'])) {
    		$this->profilePercentage += Configure::read('UserPerFieldPercentage');
    	}
        if ($userProfileData['UserProfile']['is_mobile_verified'] == Configure::read('Bollean.True')) {
            $this->profilePercentage += Configure::read('UserPerFieldPercentage');
        }
        if (!empty($userProfileData['WithdrawalSources'])) {
            $this->profilePercentage += Configure::read('UserPerFieldPercentage');
        }
    	return true;
    }
}