<?php
App::uses('AppController', 'Controller');

class VisionApisController extends AppController {
	
	public $helper = array('Html', 'User','Js','Time','Front');
	public $components = array('VisionApiFactory','RequestHandler');
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('detect');
	}
	
	//public $uses = array('VisionApi');
	
	private function matchnumberPlate($annotation, $regex_format, $special_chars, &$numberPlate)
	{
		if(isset($annotation["description"])) {
			
			
			$trimmed_array= str_replace($special_chars, '', $annotation["description"]);
			$output_array =  array();

			preg_match($regex_format, $trimmed_array, $output_array);
			if(!empty($output_array)) {
				$numberPlate = str_replace("\n", '',$output_array[0]);				
			}
		
		}
	}
	
	
	private function findnumberPlate($type, $result, &$numberPlate)
	{
		if(!isset($result['textAnnotations'])) {
			return;
		}
		
		$cfg = Configure::read('NumberPlate');
		
		
		foreach ($result['textAnnotations'] as $annotation)	{
                        $numberPlate = "";
			$this->matchnumberPlate($annotation, $cfg['regex_format'], 
					$cfg['special_chars'], $numberPlate);
			if(!empty($numberPlate)) {
				break;
			}
		}

	}

	private function getVehicleType($type,$result)
	{
		$retValue = array();
		if(!isset($result['labelAnnotations'])){
			$retValue['result']    = Configure::read('DetectionResult.Failed');
                        $retValue['reason']    = "No Label Annotations returned"; 
			$retValue['score']     = 0;
			$retValue['vehicleType'] = Configure::read('AppVehicleType.NA');	
			return;
		}

		$threshold = Configure::read('MinimumLabelDetectionThreshold');
		$highestScore = 0;

		foreach ($result['labelAnnotations'] as $annotation){

			CakeLog::write('debug','Inside getVehicleType...' . print_r($annotation,true));
			$score = intval($annotation['confidence'] * 100);

			if($score > $highestScore){
			   $highestScore = $score;	
			   $reason = "No Category Found. Highest received score was of description:: " . $annotation['description'];
			   
			   $retValue['result'] = Configure::read('DetectionResult.Failed');
                           $retValue['reason']    = $annotation['description']; 
			   $retValue['score']     = $score;
			   $retValue['vehicleType'] = Configure::read('AppVehicleType.NA');	
			}


			$description = strtolower($annotation['description']);

			//first search if its a bike
			$searchTerms = Configure::read('BikeSearchTerms');


			if(in_array($description,$searchTerms)){
			   $retValue['result']      = Configure::read('DetectionResult.Success');	
                           $retValue['reason']      = $annotation['description']; 
			   $retValue['vehicleType'] = Configure::read('AppVehicleType.Bike');	
			   $retValue['score']       = $score; 
			   break;
			}

			//search if its a car
			//Google api is currently returning car for bike images also. It is therefore important to check for 
			//bike first and then car
			$searchTerms = Configure::read('CarSearchTerms');

			if(in_array($description,$searchTerms)){
			   $retValue['result']      = Configure::read('DetectionResult.Success');	
                           $retValue['reason']      = $annotation['description']; 
			   $retValue['vehicleType'] = Configure::read('AppVehicleType.Car');	
			   $retValue['score']       = $score; 
			   break;
			}
		}

		return $retValue;
		
	}
	

       	
	

	public function detect($path, $spaceId, $deviceId, $timeMs, $direction, &$result)
       	{	
		
		// expand to a canonicalized absolute path, it's way easier to
		// test for validity using the real path
		//$path = realpath('/var/www/html/simplypark/app/webroot/img/car_vision_api_test.jpg');
                Cakelog::write('debug','Inside function detect with path = ' . $path);
                Cakelog::write('debug','Inside function detect with spaceId = ' . $spaceId);
                Cakelog::write('debug','Inside function detect with DeviceId = ' . $deviceId);
                Cakelog::write('debug','Inside function detect with TimeInMs = ' . $timeMs);
                Cakelog::write('debug','Inside function detect with direction = ' . $direction);
		
		$cfg = Configure::read('VisionApiDefault.cfg');
                Cakelog::write('debug','Inside function detect with cfg = ' . print_r($cfg,true));
		$uri = "";
		$uri .= $cfg['host'].'/'.$cfg['uri'].'?key='.$cfg['KeyID'];

		$this->loadModel('Space');
		$isAnprAvailable = $this->Space->isAnprAvailable($spaceId);
		$numberPlate = "";

		CakeLog::write('debug','isAnprAvailable..' . $isAnprAvailable);

		if($isAnprAvailable){
		        CakeLog::write('debug','sending TEXT & LABEL request');
			$input = array('uri'=>$uri,'path'=>$path, 
				'features'=>array(array('type'=>'TEXT', 'maxResults'=>10),
				                  array('type'=>'LABEL', 'maxResults'=>10)));
		}else{
		        CakeLog::write('debug','sending LABEL request');
			$input = array('uri'=>$uri,'path'=>$path, 
				       'features'=>array(array('type'=>'LABEL', 'maxResults'=>10)));
		}
		$output = null;
		$type = Configure::read('VisionApiDefault.type');
		$visionApiFactoryObj = $this->Components->load('VisionApiFactory');
		$visionApiFactoryObj->detect($type, $input, $output);
               


		$retValue = $this->getVehicleType($type,$output);
		if($retValue['result'] == Configure::read('DetectionResult.Success')){	
			if($isAnprAvailable){
				$this->findnumberPlate($type, $output, $numberPlate);
				CakeLog::write('debug', 'Inside function detect, numberPlate = ' . $numberPlate);
			}
		}else{

			$reason = "No Category Found. Highest received score was of description:: " . $retValue['reason'];
			CakeLog::write('debug', 'Inside function detect, get vehicle Type failed with reason ' . $reason);
		}

		CakeLog::write('debug','Updating Tables now');
		$date = new DateTime();
		$date->setTimestamp($timeMs);
		CakeLog::write('debug','date set as ' . print_r($date,true));


		$params = array();
		$params['blobPath']              = $path;
		$params['spaceId']               = $spaceId;
		$params['deviceId']              = $deviceId;
		$params['datetime']              = $date->format('Y-m-d H:i');
		$params['direction']             = $direction;
		$params['visionApiId']           = $type; //FIXME: this needs discussion with venki.
		                                          //change it when an actual api selection is implemented
		$params['isAnprAvailable']       = $isAnprAvailable;
		$params['regNumber']             = $numberPlate;
		$params['vehicleCategoryResult'] = $retValue;

		$this->updateAnprTables($params,$result);

                CakeLog::write('error', ' IN asdfasdfsda '. print_r($result,true));
		//this function will always return true as an indicator to the threading system that the task is done
		return true;	

	}


	/****************************************************************************
	 * Function name : updateAnprTables
	 * Description   : updates anpr related tables
	 *
	 *
	 ****************************************************************************/ 

	public function updateAnprTables($params,&$detectionResult)
		                         
	{
	   CakeLog::write('debug','Inside updateAnprTables...' . print_r($params,true));	
           //start of the transaction		
	   $this->loadModel('SpotterVehicleOccupancy');
	   CakeLog::write('debug','Inside updateAnprTables...loadModel');	
           $spotterDS = $this->SpotterVehicleOccupancy->getdatasource();
	   CakeLog::write('debug','Inside updateAnprTables...before begin');	
	   $spotterDS->begin();	
	   CakeLog::write('debug','Inside updateAnprTables...spotterDS loaded');	

	   //Fill the Annotation results 
	   $spotterAnnotationResult = array();
	   $this->loadModel('SpotterAnnotationResult');

	   $result = -1;
	   CakeLog::write('debug','Inside updateAnprTables...Load Model AnnotationResult');	
	   
	   if($params['vehicleCategoryResult']['result'] ==  Configure::read('DetectionResult.Success')){
		   if($params['isAnprAvailable'] && !empty($params['regNumber'])){
			   CakeLog::write('debug','Inside updateAnprTable...result is Success');
			   $result = Configure::read('DetectionResult.Success');
		   }else{
			   CakeLog::write('debug','Inside updateAnprTable...result is RegNumber Not found');
			   $result = Configure::read('DetectionFailureReason.RegNumberNotFound');
		   }
	   }else{
		   CakeLog::write('debug','Inside updateAnprTable...result is Vehicle Category Not found');
		   $result = Configure::read('DetectionFailureReason.VehicleCategoryNotFound');
	   }

	   $spotterAnnotationId = $this->SpotterAnnotationResult->saveResult($params['visionApiId'],$result,
		                                                             $params['vehicleCategoryResult']['score'],
		                                                             $params['vehicleCategoryResult']['reason'],
									     $params['blobPath']);

	   if($spotterAnnotationId != -1){
              //now create a transaction
              $this->loadModel('SpotterTransaction');
	      $transactionId = $this->SpotterTransaction->createTransaction($params['spaceId'],$params['deviceId'],
		                                                          $spotterAnnotationId, $params['datetime'],$params['direction'],
							                  $params['vehicleCategoryResult']['vehicleType'],
									  $params['regNumber']);

	      if($transactionId != -1){
	         $this->loadModel('SpotterVehicleOccupancy');
		 $occupancyId = $this->SpotterVehicleOccupancy->updateCount($params['spaceId'],
			                                                    $params['vehicleCategoryResult']['vehicleType'],
									    $params['direction']);

		 if($occupancyId != -1){
			 $spotterDS->commit();
				 $detectionResult['result']      = $result;
				 $detectionResult['vehicleType'] = $params['vehicleCategoryResult']['vehicleType']; 
				 $detectionResult['regNumber']   = $params['regNumber']; 
                         CakeLog::write('error', 'Wrote to detectionResult 1 . ' . print_r($detectionResult,true));
			 return true;
	         }else{
			 $spotterDS->rollback();
				 $detectionResult['result']      = -1;
                         CakeLog::write('error', 'Wrote to detectionResult 2 . ' . print_r($detectionResult,true));
			 return false;
	         }		 

	      }else{
		      $spotterDS->rollback();
			      $detectionResult['result']      = -1;
                         CakeLog::write('error', 'Wrote to detectionResult 3 . ' . print_r($detectionResult,true));
		      return false;
	      }
 
	   }else{
		   $spotterDS->rollback();
			   $detectionResult['result']      = -1;
                         CakeLog::write('error', 'Wrote to detectionResult 4 . ' . print_r($detectionResult,true));
		   return false;

	   }
		                                         

	}
}
