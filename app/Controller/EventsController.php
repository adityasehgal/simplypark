<?php
class EventsController extends AppController
{
	public $helper = array('Html', 'Form','Front');
	public $components = array('Paginator', 'RequestHandler');

	public $uses = array('Event', 'State', 'City', 'EventTiming', 'EventAddress', 'Space');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow(
				'eventDetail',
				'serachEventAddresses',
				'eventParkingLocations',
				'ajaxGetEventCities',
				'ajaxSerachEventAddresses'
			);
	}

/**
 * Method admin_index to display all created events
 *
 * @return void 
 */
	public function admin_index() {
		$this->layout = 'backend';

		$conditions = array();
		$conditions = array(
				'Event.is_deleted' => 0,
			);
		if (isset($this->request->query) && !empty($this->request->query)) {
			$searchData = array(
				'OR' => array(
					'Event.event_name LIKE' => '%'. $this->request->query['search'] .'%',
					'Event.website LIKE' => '%'. $this->request->query['search'] .'%',
					'Event.tags LIKE' => '%'. $this->request->query['search'] .'%',
					)
				);
			$conditions = array_merge($conditions, $searchData);
		}

		$this->Paginator->settings = array(
										'conditions' => array($conditions),
										'limit' => 10,
										'order' => 'Event.id Asc'
									);
		$events = $this->Paginator->paginate('Event');
		$path['path'] = Configure::read('ROOTURL');
		$events = array_merge($events, $path);

		$this->set('events',$events);
		if ($this->request->is('ajax')) {
			$this->layout = '';
			$this->autoRender = false;
			$this->viewPath = 'Elements' . DS . 'backend' . DS . 'Events';
			$this->render('listing');
		}
	}

/**
 * Method admin_add to show page with add event form
 *
 * @return void
 */
	public function admin_add() {
		$states = $this->State->find('list',array(
				'conditions' => array(
					'State.is_activated' => 1
				),
				'fields' => array(
					'id', 'name'
				),
				'recursive' => -1
			)
		);
		
		$this->set('states',$states);
		$this->layout = 'backend';
	}

	public function admin_getCities() {
		$this->autoRender = false ;
		$cities = $this->City->find('list',array(
				'conditions' => array(
					'City.state_id' => $this->request->data['state_id'],
					'City.is_activated' => 1
				),
				'fields' => array(
					'id', 'name'
				),
				'recursive' => -1
			)
		);
		
		return json_encode($cities);
	}

/**
 * Method admin_addEvent to add new event in application
 *
 * @return void
 */
	public function admin_addEvent() {
		$this->request->allowMethod('post', 'put');
		if ($this->Event->save($this->request->data)) {
			$this->Session->setFlash(__('Event details have been added successfully.'), 'default', 'success');
			$this->redirect(array('action' => 'index', 'admin' => true));
		}
		$errors = $this->Event->validationErrors;
		if (!empty($errors)) {
			$errorMsg = $this->_setValidaiotnError($errors);
		}
		$this->Session->setFlash(__('Add event request not completed due to following errors: <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
		$this->redirect($this->referer());
	}

	public function admin_updateIsPopular() {
		$this->autoRender = false ;
		$data = array();
		$data['status'] = 'error';

		$check_active = $this->Event->find('first',array(
				'conditions' => array(
					'Event.id' => $this->request->data['result']['event_id'],
					'Event.is_activated !=' => 1,
				),
				'fields' => array(
					'id'
				),
				'recursive' => -1
			)
		);
		if(!empty($check_active)) {
			if(isset($check_active['Event']['id']) && 
				$this->request->data['result']['is_popular'] == 1) {
				$data['msg'] = 'Activate event to set popular.';
				return json_encode($data);
			}
		}
		
		if ($this->request->data['result']['is_popular'] == 1) {
			$event_count = $this->Event->find('count', array(
	    	    'conditions' => array(
	    	    					'Event.is_popular' => 1,
	    	    					'Event.is_deleted !=' => 1,
	    	    				)
		    ));
		    if($event_count >= 4) {
		    	$data['msg'] = 'Only 4 events can be set as popular. Kindly remove an event from the popular list before adding another.';
				return json_encode($data);
		    }
		}

		$address = $this->EventAddress->find('list',array(
				'conditions' => array(
					'EventAddress.event_id' => $this->request->data['result']['event_id'],
				),
				'fields' => array('id' ),
				'recursive' => -1
			)
		);
		
		if(empty($address)){
			$data['msg'] = 'No address exists So, Popular cannot be set.';
			return json_encode($data);
		}

		$save_data = array(
			'is_popular' => $this->request->data['result']['is_popular']
		);

		$this->Event->id = $this->request->data['result']['event_id'];
		$data['msg'] = 'Popular set unsuccessfully.';
		if($this->Event->save($save_data)) {
			$data['status'] = 'sucess';
			if ($this->request->data['result']['is_popular'] == 1) {
				$data['msg'] = 'Event set as popular successfully.';
			} else {
				$data['msg'] = 'Event removed from the popular list.';
			}
		}
		
		return json_encode($data);
	}

	public function admin_editEvent($event_id = null) {
		if($event_id) {
			$event_id = urldecode(base64_decode($event_id));
			$result = $this->Event->findById($event_id);
						
			$path['path'] = Configure::read('ROOTURL').'files/Event/';
			$result = array_merge($result, $path);
			
			$this->set('events',$result);
		} else {
			$this->Session->setFlash(__('Event details not found.'), 'default', 'error');
			$this->redirect(array('action' => 'index', 'admin' => true));
		}
		$this->layout = 'backend';
	}

	public function admin_ajaxGetEventData($event_id = null) {
		$event_id = urldecode(base64_decode($event_id));
		$getEventData = $this->Event->find('first',array(
				'conditions' => array(
					'Event.id' => $event_id,
				),
				'recursive' => 2
				)
		);
		
		$path['path'] = Configure::read('ROOTURL').'files/Event/';
		$getEventData = array_merge($getEventData, $path);
		
		$this->set(
				array(
					'response' => $getEventData,
					'_serialize' => 'response'
				)
			);
	}

	public function admin_updateEvent() {
		$this->autoRender = false ;
		
		$this->request->allowMethod('post', 'put');
		$this->Event->id = $this->request->data['Event']['id'];
		if ($this->Event->save($this->request->data)) {
			$this->Session->setFlash(__('Event details have been updated successfully.'), 'default', 'success');
			$this->redirect(array('action' => 'index', 'admin' => true));
		}
		$errors = $this->Event->validationErrors;
		if (!empty($errors)) {
			$errorMsg = $this->_setValidaiotnError($errors);
		}
		$this->Session->setFlash(__('Update event request not completed due to following errors: <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
		$this->redirect($this->referer());
	}

	public function admin_addEventAddress($event_id = null) {
		$path = array();
		$event_ids = array();
		$data = array();
		$event_ids['event_id'] = urldecode(base64_decode($event_id));
		$event_data = $this->Event->find('first',array(
				'conditions' => array(
					'Event.id' => $event_ids['event_id'],
				),
				'fields' => array(
					'event_name'
				),
				'recursive' => 2
				)
		);
		$event_ids['event_name'] = $event_data['Event']['event_name'];
		$states['state'] = $this->State->find('list',array(
				'conditions' => array(
					'State.is_activated' => 1
				),
				'fields' => array(
					'id', 'name'
				),
				'recursive' => -1
			)
		);

		$data['event_address_data'] = $this->EventAddress->findAllByEventId($event_ids['event_id']);
		$states = array_merge($states, $data);

		$states = array_merge($states, $event_ids);
		$path['path'] = Configure::read('ROOTURL');
		
		$result = array_merge($states, $path);
		$this->set(
				array(
					'response' => $result,
					'_serialize' => 'response'
				)
			);
	}

	public function admin_addEventAddressData() {
		$this->request->allowMethod('post', 'put');
		
		foreach($this->request->data['EventAddress']['time'] as $time) {
			if($time['to'] <= $time['from']) {
				$errorMsg = 'To time should be greater than From time. <br/>';
				$this->Session->setFlash(__('Add event address request not completed due to following errors: <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
				$this->redirect($this->referer());
			}
			$time = array_merge($time,array('event_id' => $this->request->data['EventAddress']['event_id']));
			$this->request->data['EventTiming'][] = $time;
		}
		unset($this->request->data['EventAddress']['time']);

		if ($this->EventAddress->saveAssociated($this->request->data)) {
			$this->Session->setFlash(__('Event address details have been added successfully.'), 'default', 'success');
			$this->redirect(array('action' => 'index', 'admin' => true));
		}
		$errors = $this->EventAddress->validationErrors;
		if (!empty($errors)) {
			$errorMsg = $this->_setValidaiotnError($errors);
		}
		$this->Session->setFlash(__('Add event address request not completed due to following errors: <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
		$this->redirect($this->referer());
	}

	public function admin_eventDetailsDelete($event_address_id, $event_id) {
		$this->EventAddress->delete(array('EventAddress.id' => $event_address_id));
		$this->EventTimings->deleteAll(array('EventTimings.event_address_id' => $event_address_id));

		$path = array();
		$event_ids = array();
		$data = array();
		$event_ids['event_id'] = $event_id;
		$states['state'] = $this->State->find('list',array(
				'conditions' => array(
					'State.is_activated' => 1
				),
				'fields' => array(
					'id', 'name'
				),
				'recursive' => -1
			)
		);

		$data['event_address_data'] = $this->EventAddress->findAllByEventId($event_ids['event_id']);
		$states = array_merge($states, $data);

		$states = array_merge($states, $event_ids);
		$path['path'] = Configure::read('ROOTURL');
		
		$result = array_merge($states, $path);
		$this->set(
				array(
					'response' => $result,
					'_serialize' => 'response'
				)
			);
	}

/**
 * Method eventDetail to get and show details of a particular event
 *
 * @return void
 */
	public function eventDetail($eventID = null) {
    	$this->layout = 'home';
    	$eventID = urldecode(base64_decode($eventID));
    	$this->Event->contain(
    					array(
    						'EventAddress' => array(
    								'fields' => array(
    										'id',
											'event_id',
											'address',
											'start_date'
    									),
    									'State' => array(
    												'name'
												),
										'City' => array(
													'name'
												),
										'EventTiming' => array(
												'conditions' => array(
														'DATE(EventTiming.to) > ' => date('Y-m-d')
													)
											)
								)
    									
    						)
    						
    					);
    	$eventDetail = $this->Event->findById($eventID,
    									array('id', 'event_name', 'event_pic', 'description')
    									//array('Event.created' => 'asc')
    					);
    
    	$this->set('eventDetail', $eventDetail);

    	$states = $this->_getEventStates($eventDetail['EventAddress']);
    	$this->set('states', $states);

  	$eventMonths = $this->_getEventMonths($eventDetail['Event']['id']);
    	$this->set('eventMonths', $eventMonths);

  		$getOtherEvents = $this->Event->getEvents(2, false, $eventID);
		$this->set('getOtherEvents', $getOtherEvents);
    }

/**
 * Method ajaxSerachEventAddresses to get the filtered address records for event detail page
 *
 * @return void
 */
    public function ajaxSerachEventAddresses() {
    	$this->autoRender = false;
    	$this->request->allowMethod('get');
    	$getAddresses = $this->EventTiming->getAddresses($this->request->query);
    	
	   	$timings = array();
    	foreach ($getAddresses as $eventTimings) {
    		$makeTimings['EventTiming'] = array();
    		$address = $eventTimings['EventAddress'];
    		$makeTimings['EventTiming'][] = $eventTimings['EventTiming']; 
    		$timings['EventAddress'][] = array_merge($address,$makeTimings);
    	}
    	$this->set('eventDetail', $timings);

    	$this->viewPath = 'Elements' . DS . 'frontend' . DS . 'Event';
    	$this->render('event_addresses');
    }

/**
 * Method ajaxGetEventCities to get the cities based on state id and event id
 * @param $eventId and $stateId
 * @return city detil array
 */
    public function ajaxGetEventCities($eventID = null, $stateId = null) {
    	$this->autoRender = false;
    	if($this->RequestHandler->isAjax()) {
    		$getAddresses = $this->EventAddress->getEventAddress($eventID,$stateId);
	    	foreach ($getAddresses as $key => $value) {
				$cityNameArray = $this->City->findById($value['EventAddress']['city_id'],
														array('id','name')

													);
				$cityDetail['city'][$key]['id'] = $cityNameArray['City']['id'];
				$cityDetail['city'][$key]['name'] = $cityNameArray['City']['name'];
				
				
			}
			return json_encode($cityDetail);
    	}
	    	
    }

/**
 * Method eventParkingLocations to get the parking spaces nearest of an event address
 *
 * @param $eventID int the id of the event
 * @param $eventAddressID int the id of event address
 * @return void
 */
    public function eventParkingLocations($eventID = null, $eventAddressID = null, $eventTimingID = null) {
    	$this->layout = 'home';
    	$eventID = urldecode(base64_decode($eventID));
    	$eventAddressID = urldecode(base64_decode($eventAddressID));
    	$eventTimingID = urldecode(base64_decode($eventTimingID));
    	$this->set('eventTimingID',$eventTimingID);

	$getEventData = $this->Event->getEventAndAddressThroughID($eventID, $eventAddressID); //getting event and address
	CakeLog::write('debug','Inside eventParkingLocations...' . print_r($getEventData,true));
		$this->set('getEventData',$getEventData);
		
		$nearestSpaces = array();
		if (!empty($getEventData['EventAddress'])) {
			$lat = $getEventData['EventAddress']['lat'];
			$long = $getEventData['EventAddress']['long'];
	    	$nearestSpaces = $this->Space->getEventNearestSpaces($lat,$long); //getting nearest spaces
	        CakeLog::write('debug','Inside eventParkingLocations..(nearestSpaces).' . print_r($nearestSpaces,true));
	    }
		$this->set('nearestSpaces',$nearestSpaces);
    }
}
