<?php

class UserdashboardController extends AppController
{
	public $components = array('RequestHandler');
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('activeusers','activespaces','occupancy');
	}



/*******************************************************************************
 * Method : activeusers 
 * Desc   : get Total registered Users
 * Input  : None
 * Output : JSON { "status" : 1,
 *                 "count" : xxxx
 *               }
 *
 *******************************************************************************/ 

public function activeusers()
{
	//CakeLog::write('debug','Inside Active Users');
	$response = array();
	$response['status']  = 1;
	$response['reason']  = "Query Successful";
	$this->loadModel('User');
	$response['count'] = $this->User->activeUserCount();
	//CakeLog::write('debug','Sending Response ' . print_r($response,true));
	$this->set('data',$response);
	$this->set('_serialize','data');

}

/*******************************************************************************
 * Method : activespaces
 * Desc   : get Total Active Spaces 
 * Input  : owner ID 
 * Output : JSON { "status" : 1,
 *                 "totalSpaces" : xxxx
 *               }
 * Note   : if owner ID is 0, we will return the count of all active spaces               
 *
 *******************************************************************************/ 

public function activespaces($ownerId = 0)
{
	//CakeLog::write('debug','Inside Active Spaces');
	$response = array();
	$response['status']  = 1;
	$response['reason']  = "Query Successful";
	$this->loadModel('Space');
	$response['count'] = $this->Space->activeSpaceCount();
	//CakeLog::write('debug','Sending Response ' . print_r($response,true));
	$this->set('data',$response);
	$this->set('_serialize','data');

}

/*******************************************************************************
 * Method : occupancy 
 * Desc   : get Occupancy of all Spaces owned by this owner 
 * Input  : owner ID 
 * Output : JSON { "status" : 1,
 *                 "occupancy" : [[11,23,34]]
 *                 "chartOptions": "string" 
 *               }
 * Note   : if owner ID is 0, we will return the count of all active spaces               
 *
 *******************************************************************************/ 

public function occupancy($ownerId = 0)
{
	CakeLog::write('debug','Inside Occupancy');
	$response = array();
	$response['status']  = 1;
	$response['reason']  = "Query Successful";

	$spaceIds = array();

	if($ownerId != 0){	
		//first find all space Ids belonging to this owner
		$this->loadModel('Space');
		$spaceIds = $this->Space->activeSpaceIds($ownerId);
	}
		
	if(empty(spaceIds) && $ownerId != 0){
	   $response['status']  = 0;
	   $response['reason']  = "No Space associated with this owner Id";
	}else{
		CakeLog::write('debug','SpaceIDs =  ' . print_r($spaceIds,true));
		//now get the occupancy
		$this->loadModel('CurrentSlotOccupancy');
		$occupancyCount = $this->CurrentSlotOccupancy->getOccupancy($spaceIds);
		CakeLog::write('debug','Occupancy Count ' . print_r($occupancyCount,true));
	}
	
	$this->set('data',$response);
	$this->set('_serialize','data');

}


/******************************************************************************
 *
 *
 *     UNUSED or Experimental API calls
 *
 *
 * *****************************************************************************

/*******************************************************************************
 * Method : coordinates 
 * Desc   : get Locations lat/lng 
 * Input  : owner ID 
 * Output : JSON { "status" : 1,
 *                 "coords" :[{lat/lng},{lat/lng}]
 *               }
 * Note   : if owner ID is 0, we will return the count of all active spaces               
 *
 *******************************************************************************/ 

public function coordinates($ownerId = 0)
{
	CakeLog::write('debug','Inside locations');
	$response = array();
	$response['status']  = 1;
	$response['reason']  = "Query Successful";
	CakeLog::write('debug','Inside locations2');
	$this->loadModel('Space');
	$coordsArray = $this->Space->getLocationsLatLng($ownerId);
	$coords = array();
	if(!empty($coordsArray)){
		$itr = 0;
		foreach($coordsArray as $coord){
			$spaceCords = $coord['Space']['lat'] . ',' . $coord['Space']['lng']; 
	                CakeLog::write('debug','SpaceCoords ' . print_r($spaceCords,true));
			$coords[$itr++] = $spaceCords;
		}
	}	
	$response['coords'] = $coords;
	//CakeLog::write('debug','Sending Response ' . print_r($response,true));
	$this->set('data',$response);
	$this->set('_serialize','data');

}

}
