<?php

App::uses('AppController', 'Controller');

class UisController extends AppController {

	public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow(
				'eventDetail'
			);
    }

    public function eventDetail() {
    	$this->layout = 'home';
    }

    public function eventLocation() {
    	$this->layout = 'home';
    }

    public function profile() {
    	$this->layout = 'user';
    }

    public function contact() {
    	$this->layout = 'home';
    }

    public function accountInfo() {
    	$this->layout = 'home';
    }

    public function congratulation() {
        $this->layout = 'home';
    }

    public function parkingDetails() {
    	$this->layout = 'home';
    }

    public function booking() {
        $this->layout = 'user';
    }

    public function errorPage() {
        $this->layout = null;
    }

     public function dashboard() {
        $this->layout = 'user';
    }

    public function print_ticket() {
        $this->layout = 'print_layout';
    }

}