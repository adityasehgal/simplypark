<?php
App::import('Vendor','Spreadsheet_Excel_Reader', array('file' => 'php-excel-reader/excel_reader2.php')); //import statement
App::import('Controller','Bookings');
App::uses('CakeTime', 'Utility');
App::uses('HttpSocket', 'Network/Http');
class SpacesController extends AppController
{
	public $helper = array('Html', 'Form', 'Js', 'Admin','Time');
	public $components = array('RequestHandler', 'Paginator');
	public $uses = array('Space', 'User', 'SpaceDisapprovalReason', 'SpaceChargeSetting', 'State', 'City', 'Booking','Bank','SpaceFacility');


	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('spaceDetail','spaceDetailBulk','getSpaceInfo','ajax_validateBookingSpaceNew', 'ajax_validateBulkBookingSpace','listSpaces','listNearBySpaces','admin_addFreeSpace','admin_activateSpace','admin_deactivateSpace','ajax_sendSpaceLocationSMS','registerWidget','deRegisterWidget','updateSlotOccupancy','refreshToken','operatorSpaceDetails', 'registerApp','getCurrentOccupancy','logOfflineEntry','logOnlineEntry','logOfflineExit','logOnlineExit','refreshAppToken','getOperatingHours','sync','getSyncId','cancelTransaction','cancelOnlineTransaction','updateTransaction','updateOnlineTransaction','isSpaceBikeOnly','isCancelUpdateEnabled','opAppGetSpaces','listNearBySpacesByLatLong');
		if ($_SERVER["REMOTE_ADDR"] != Configure::read('Server.Local') && $_SERVER['SERVER_NAME'] == Configure::read('Server.Live')) {
			$this->Security->unlockedActions = array('ajaxValidateDates');
		}
	}

/**
 * Method getAllParKingSpaces is to get all parking spaces
 *
 * @return void 
 */
	public function listSpaces() {
		$this->layout = 'home';
		$getSpaces = $this->Space->getSearchedSpaces($this->request->query['search_string']);
		$this->set(
				array(
						'response' => $getSpaces,
						'_serialize' => 'response'
					)
			);
		
		
	}

/**
 * Method listNearBySpaces to get all the spaces within 5km radius
 *
 * @param $spaceID id of the space
 * @return void
 */
public function listNearBySpaces($space_id = null) 
{
	$this->layout = 'smallhome';
	$filterResults = 0;
        CakeLog::write('debug','INside listNearBySpaces...' . print_r($this->request->data,true));
	if(isset($this->request->data['Space']) && $this->request->data['Space']['filter_results'] == 1)
        {
		$spaceID = urldecode(base64_decode($this->request->data['Space']['spaceID']));
		$filterResults =1;
                $this->set('encodedSpaceID',$this->request->data['Space']['spaceID']);
		$this->Session->write('spaceID',$this->request->data['Space']['spaceID']);
	}else if($space_id != null){
		$spaceID = urldecode(base64_decode($space_id));
		$this->set('encodedSpaceID',$space_id);
		$this->Session->write('spaceID',$space_id);
	}else{
		//CakeLog::write('debug','Empty space id');
		$encodedSpaceID = $this->Session->read('spaceID');
		$this->Session->write('spaceID',$encodedSpaceID);
		$this->set('encodedSpaceID',$encodedSpaceID);
		$spaceID = urldecode(base64_decode($encodedSpaceID));
	}

	
	//CakeLog::write('debug','++listNearBy,space:: ' . $spaceID);
	$getSpaceDetail = $this->Space->getSpaceInfo($spaceID);
	$lat = $getSpaceDetail['Space']['lat'];
	$long = $getSpaceDetail['Space']['lng'];
	$conditions = array(
			'Space.is_completed' => Configure::read('Bollean.True'),
			'Space.is_activated' => Configure::read('Bollean.True'),
			'Space.is_approved' => Configure::read('Bollean.True'),
			'Space.is_deleted' => Configure::read('Bollean.False'),
			'Space.property_type_id !=' => 2, //no need to show gated communities
			);


	//By default the list will show spaces within the radius set in config file only
	$price = null;
	$distance = Configure::read('NearestSpaceDistance');
	
	$listNearBySpaces = $this->Space->getNearestSpaces($lat,$long,$conditions,$distance);

	CakeLog::write('debug','In getList ...' . print_r($listNearBySpaces,true));
	//$this->set(compact('listNearBySpaces','getSpaceDetail','price','distance'));
	if($filterResults)
	{
	   $startDateTimeObj = new DateTime($this->request->data['Space']['start_date']);
	   $endDateTimeObj   = new DateTime($this->request->data['Space']['end_date']);
	   $this->set('encodedSpaceID',$this->request->data['Space']['spaceID']);
	   $this->set('stdate',$this->request->data['Space']['start_date']);
	   $this->set('edate',$this->request->data['Space']['end_date']);

	   //CakeLog::write('debug','NearbySpaces...' . print_r($nearbySpaces,true));
	   $bookableNearBySpaces = array();
	   $idx = 0;

	   foreach($listNearBySpaces as $space)
	    {
	       $bookingPrice = 0.0;
	       //CakeLog::write('debug','NearbySpace...' . print_r($space,true));

		if($this->isSpaceAvailable($space,
			$this->request->data['Space']['start_date'],
			$this->request->data['Space']['end_date'],
			$bookingPrice))
		{
	 	   $space['booking_price'] = $bookingPrice;   
		   $bookableNearBySpaces[$idx++] = $space;
		}

	     }


	     $this->set('listNearBySpaces',$bookableNearBySpaces);
	     $this->set('filteredResults',1);

	}else{
	   $this->set(compact('listNearBySpaces','getSpaceDetail'));
	   $this->set('filteredResults',0);
	}
	
	 $this->set('getSpaceDetail',$getSpaceDetail);

	if($this->Auth->user()) 
	{
	  $this->set('isLoggedIn', true);
	  $this->set('userId',$this->Auth->user('id'));

	  $this->loadModel('UserProfile');
	  $mobile = $this->UserProfile->getUserMobile($this->Auth->user('id'));      
	  $this->set('userMobile', $mobile);
	}else{
	  $this->set('isLoggedIn', false);
	}

	if($this->RequestHandler->isMobile()){
		$this->render('list_near_by_spaces_mobile');
	}
	
}

/**
 * Method getShortMapsURL to get a short URL 
 *
 * @return void 
 */
public function getShortMapsURL($lat,$lng) 
{
   $shortUrl = "";
   $longURL = Configure::read('GoogleShortURLKeys.LongURL').$lat.','.$lng;
   $requestHeaders = array( 
                 'header' => array(
                        'Content-Type' => 'application/json',
                )       
         );

   $socket = new HttpSocket();
   $requestParams = array('longUrl' => $longURL);
   $jsonData = json_encode($requestParams);
   $requestUrl = Configure::read('GoogleShortURLKeys.APIURL').'/?key='.Configure::read('GoogleShortURLKeys.APIKEY');
   //CakeLog::write('debug','getSHORTMAPSURL ...' . $requestUrl);
   $response = $socket->post($requestUrl,$jsonData,$requestHeaders);

   if($response->code == Configure::read('HTTPStatusCode.OK')){
      $responseData = json_decode($response->body(),true);
      $shortUrl = $responseData['id'];
   }else{
     CakeLog::write('error','API returened code..' . $response->code); 
     CakeLog::write('error','API returened code..' . $response->reasonPhrase); 
     
   }

   //CakeLog::write('debug','SHORT URL is ...' . $shortUrl);
   return $shortUrl; 

}
/**
 * Method admin_addFreeSpace to add/update informational spaces 
 *
 * @return void 
 */
public function admin_addFreeSpace() 
{
	$this->request->allowMethod('post','put');

	//add some parameters

	$this->request->data['Space']['is_activated'] = 0;
	$this->request->data['Space']['is_approved'] = 1;
	$this->request->data['Space']['is_completed'] = 1;
	$this->request->data['Space']['created'] = date("Y-m-d H:i:s");
	$this->request->data['Space']['modified'] = date("Y-m-d H:i:s");
	$this->request->data['Space']['user_id'] = 1; //admin 
	$this->request->data['Space']['is_available_with_simplypark'] = 0;
	$this->request->data['Space']['booking_confirmation'] = 2; //not available with SimplyPark

	//create the google maps short URL to driving destination also

	$this->request->data['Space']['mapsURL'] = $this->getShortMapsURL($this->request->data['Space']['lat'],
		$this->request->data['Space']['lng']);

	//CakeLog::write('debug','Inside admin free space..' . print_r($this->request->data,true));

	if(!isset($this->request->data['SpaceTimeDay']['open_all_hours']) || !$this->request->data['SpaceTimeDay']['open_all_hours']){
		$from_date = date("Y-m-d") . ' ';
		$from_date .= $this->request->data['SpaceTimeDay']['from_time'];
		//CakeLog::write('debug','From date ' . $from_date);

		$this->request->data['SpaceTimeDay']['from_date'] = $from_date;

		$to_date = date("Y-m-d",mktime(0,0,0,12,31,2030)). ' ';
		$to_date .= $this->request->data['SpaceTimeDay']['to_time'];
		//CakeLog::write('debug','To date ' . $to_date);
		$this->request->data['SpaceTimeDay']['to_date'] = $to_date;


	}

	CakeLog::write('debug','Inside addFreeSpace...adding data' . print_r($this->request->data,true));
	if (isset($this->request->data['SpacePricing'])) 
	{
		//CakeLog::write('debug','After...SpacePricing if condition');
		$pricingArray = $this->request->data['SpacePricing'];
		unset($this->request->data['SpacePricing']);
		$this->request->data['SpacePricing'][0] = $pricingArray;	
	}	
	$this->loadModel('Space');
	if($this->Space->saveAssociated($this->request->data)){
		$space_id = $this->Space->getLastInsertId();;
		//CakeLog::write('debug','Successs with space id ' . $space_id);
		if(isset($this->request->data['Space']['facility_id'])){
			foreach($this->request->data['Space']['facility_id'] as $facility_id) {
				$facility[]['SpaceFacility'] = array(
					'space_id' => $space_id, 
					'facility_id' => $facility_id
				);
			//CakeLog::write('debug','Facilities arrary ..' . print_r($facility,true));
			}
			$this->SpaceFacility->saveMany($facility);

		}
		$this->Session->setFlash(__('Space has been successfully Added'), 'default', 'success');
		$this->redirect($this->referer());
	}else{
		$errors = $this->Space->validationErrors;
		if (!empty($errors)) {
			$errorMsg = $this->_setValidaiotnError($errors);
		}

		$this->Session->setFlash(__('Your Request could not completed due to following error : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
		$this->redirect($this->referer());
	}

}

/**
 * Method admin_activateSpace to activate a space 
 *
 * @return void 
 */
public function admin_activate($spaceId) 
{
  $this->Space->admin_changeSpaceState($spaceId, Configure::read('Activate.True'));
  $this->Session->setFlash(__('Space Activated Successfully'), 'default', 'success');
  $this->redirect($this->referer());
}


/**
 * Method admin_deactivateSpace to activate a space 
 *
 * @return void 
 */
public function admin_deactivate($spaceId) 
{
  $this->Space->admin_changeSpaceState($spaceId, Configure::read('Activate.False'));
  $this->Session->setFlash(__('Space DeActivated Successfully'), 'default', 'success');
  $this->redirect($this->referer());
}

/**
 * Method admin_index get the list of all spaces added
 *
 * @return void 
 */
public function admin_index() 
{
	$this->layout = 'backend';
	$conditions = array(
			'Space.is_completed' => Configure::read('Bollean.True'),
			'Space.is_deleted' => Configure::read('Bollean.False'),
			'Space.is_available_with_simplypark' => Configure::read('Bollean.True')
			);
	if (isset($this->request->query) && !empty($this->request->query)) {
		$searchData = array(
				'OR' => array(
					'User.email LIKE' => '%'. $this->request->query['search'] .'%',
					'UserProfile.first_name LIKE' => '%'. $this->request->query['search'] .'%',
					'UserProfile.last_name LIKE' => '%'. $this->request->query['search'] .'%',
					//'Space.id' => $this->request->query['search'],
					'Space.name LIKE' => '%'. $this->request->query['search'] .'%',
					'SpaceType.name LIKE' => '%'. $this->request->query['search'] .'%',
					'PropertyType.name LIKE' => '%'. $this->request->query['search'] .'%',
					'State.name LIKE' => '%'. $this->request->query['search'] .'%',
					'City.name LIKE' => '%'. $this->request->query['search'] .'%'
					)
				);
		$conditions = array_merge($conditions, $searchData);
	}

	$options['joins'] = array(
			array('table' => 'users',
				'alias' => 'User',
				'conditions' => array(
					'User.id = Space.user_id'
					)
			     ),
			array('table' => 'user_profiles',
				'alias' => 'UserProfile',
				'conditions' => array(
					'UserProfile.user_id = User.id'
					)
			     ),
			array('table' => 'states',
				'alias' => 'State',
				'conditions' => array(
					'State.id = Space.state_id'
					)
			     ),
			array('table' => 'space_types',
				'alias' => 'SpaceType',
				'type' => 'left',
				'conditions' => array(
					'SpaceType.id = Space.space_type_id'
					)
			     ),
			array('table' => 'property_types',
					'alias' => 'PropertyType',
					'type' => 'left',
					'conditions' => array(
						'PropertyType.id = Space.property_type_id'
						)
			     ),
			array('table' => 'cities',
					'alias' => 'City',
					'type' => 'left',
					'conditions' => array(
						'City.id = Space.city_id'
						)
			     ),
			array('table' => 'space_parks',
					'alias' => 'SpacePark',
					'conditions' => array(
						'SpacePark.space_id = Space.id',
						)
			     ),
			array('table' => 'space_time_days',
					'alias' => 'SpaceTimeDay',

					'conditions' => array(
						'SpaceTimeDay.space_id = Space.id'
						)
			     )
				);
	$options['order'] = 'Space.created Desc'; 
	$options['contain'] = array('SpacePark.park_number'); 
	$options['limit'] = 10;
	$options['group'] = 'Space.id';
	$options['recursive'] = -1;
	$options['fields'] = array(
			'Space.id',
			'Space.name',
			'Space.flat_apartment_number',
			'Space.tower_number',
			'Space.address1',
			'Space.address2',
			'Space.is_activated',
			'Space.is_approved',
			'Space.property_type_id',
			'User.id',
			'User.email',
			'UserProfile.first_name',
			'UserProfile.last_name',
			'SpaceType.name',
			'PropertyType.name',
			'State.name',
			'City.name',
			'SpaceTimeDay.from_date',
			'SpaceTimeDay.to_date',	
			'SpaceTimeDay.open_all_hours',
			);

	$options['conditions'] = $conditions;

	$this->Paginator->settings = $options;
	$spaces = $this->Paginator->paginate('Space');
	$this->set('spaces',$spaces);
        
        //CakeLog::write('debug','Inside spacesController '. print_r($spaces,true));
	if ($this->request->is('ajax')) {
		$this->layout = '';
		$this->autoRender = false;
		$this->viewPath = 'Elements' . DS . 'backend' . DS . 'Space';
		$this->render('listing');
	}
}



/**
 * Method admin_parkingSpaces get the list of all spaces added
 *
 * @return void 
 */
public function admin_free_parking_spaces() 
{
	$this->layout = 'backend';
	$conditions = array(
			'Space.is_completed' => Configure::read('Bollean.True'),
			'Space.is_deleted' => Configure::read('Bollean.False'),
			'Space.is_available_with_simplypark' => Configure::read('Bollean.False')
			);
	if (isset($this->request->query) && !empty($this->request->query)) {
		$searchData = array(
				'OR' => array(
					//'Space.id' => $this->request->query['search'],
					'Space.name LIKE' => '%'. $this->request->query['search'] .'%',
					'SpaceType.name LIKE' => '%'. $this->request->query['search'] .'%',
					'PropertyType.name LIKE' => '%'. $this->request->query['search'] .'%',
					'State.name LIKE' => '%'. $this->request->query['search'] .'%',
					'City.name LIKE' => '%'. $this->request->query['search'] .'%'
					)
				);
		$conditions = array_merge($conditions, $searchData);
	}

	$options['joins'] = array(
			array('table' => 'states',
				'alias' => 'State',
				'conditions' => array(
					'State.id = Space.state_id'
					)
			     ),
			array('table' => 'space_types',
				'alias' => 'SpaceType',
				'type' => 'left',
				'conditions' => array(
					'SpaceType.id = Space.space_type_id'
					)
			     ),
			array('table' => 'property_types',
					'alias' => 'PropertyType',
					'type' => 'left',
					'conditions' => array(
						'PropertyType.id = Space.property_type_id'
						)
			     ),
			array('table' => 'cities',
					'alias' => 'City',
					'type' => 'left',
					'conditions' => array(
						'City.id = Space.city_id'
						)
			     ),
			array('table' => 'space_time_days',
					'alias' => 'SpaceTimeDay',

					'conditions' => array(
						'SpaceTimeDay.space_id = Space.id'
						)
			     )
				);
	$options['order'] = 'Space.created Desc'; 
	$options['limit'] = 10;
	$options['group'] = 'Space.id';
	$options['recursive'] = -1;
	$options['fields'] = array(
			'Space.id',
			'Space.name',
			'Space.flat_apartment_number',
			'Space.tower_number',
			'Space.address1',
			'Space.address2',
                        'Space.lat',
                        'Space.lng',
			'Space.is_activated',
			'Space.is_approved',
			'Space.property_type_id',
			'SpaceType.name',
			'PropertyType.name',
			'State.name',
			'City.name',
			'SpaceTimeDay.from_date',
			'SpaceTimeDay.to_date',	
			'SpaceTimeDay.open_all_hours',

			);
	$options['conditions'] = $conditions;

	$this->Paginator->settings = $options;
	$spaces = $this->Paginator->paginate('Space');
	$this->set('spaces',$spaces);


       //Get all the facilities
        $this->loadModel('Facility');
        $allFacilities = $this->Facility->find('list',
                                               array('conditions' => array('is_activated' => 1)));
        $this->set('facilities', $allFacilities);
       
       //Get all the States 
        $this->loadModel('State');
        $allStates = $this->State->find('list',
                                               array('conditions' => array('is_activated' => 1)));
        $this->set('statesArr', $allStates);
       
        //Get all the Cities 
        $this->loadModel('City');
        $allCities = $this->City->find('list',
                                               array('conditions' => array('is_activated' => 1)));
        $this->set('citiesArr', $allCities);
        
        //Get all the space Types 
        $this->loadModel('SpaceType');
        $allSpaceType = $this->SpaceType->find('list',
                                               array('conditions' => array('is_activated' => 1)));
        $this->set('spaceTypeArr', $allSpaceType);
        
        //Get all the Property Types 
        $this->loadModel('PropertyType');
        $allPropertyType = $this->PropertyType->find('list',
                                               array('conditions' => array('is_activated' => 1)));
        $this->set('propertyTypeArr', $allPropertyType);
	
       if ($this->request->is('ajax')) {
		$this->layout = '';
		$this->autoRender = false;
		$this->viewPath = 'Elements' . DS . 'backend' . DS . 'Space';
		$this->render('freeSpaceLsting');
	}

       //CakeLog::write('debug','Inside admin_free_spaces...' . print_r($spaces,true));
}
/**
 * Method admin_approveSpace to approve the user spaces added from frontend
 *
 * @param $spaceID the id of the space
 * @return void
 */
	public function admin_ajaxApproveSpace($spaceID = null, $userID = null) {
		$spaceID = base64_decode($spaceID);
		$userID = base64_decode($userID);
		
		$updateArray = array('Space.is_approved' => Configure::read('Bollean.True'));
		$conditionsArray = array('Space.id' => $spaceID);

		$this->__dbTableUpdate('Space', $updateArray, $conditionsArray);
		$status = array('status' => 'error');
		if ($this->__sendSpaceApprovedDisapprovedEmail($spaceID, Configure::read('EmailTemplateId.space_approved'))) {
			$checkUserTrusted = $this->User->findById($userID, array('trusted_by_admin'));
			$status['status'] = 'success';
			$status['trusted'] = $checkUserTrusted['User']['trusted_by_admin'];
		}
		$this->set(
				array(
					'response' => $status,
					'_serialize' => 'response'
				)
			);
	}

/**
 * Method makeUserTrusted to make user trusted to that from next time the user spaces will remain approved
 *
 * @param $userID int the id of the user
 * @return void
 */
	public function admin_makeUserTrusted($userID = null) {
		$userID = base64_decode($userID);

		$updateUserArray = array('User.trusted_by_admin' => Configure::read('Bollean.True'));
		$conditionsUserArray = array('User.id' => $userID);
		
		if ($this->__dbTableUpdate('User', $updateUserArray, $conditionsUserArray)) {
			$this->Session->setFlash(__('Space has been approved successfully.'), 'default', 'success');
		}
		$this->redirect($this->referer());
	}

/**
 * Method __sendSpaceApprovedDisapprovedEmail common function to send space approved or disapproved emails
 *
 * @param $spaceId int id of the space to get space data
 * @param $emailTemplate string email template to be used
 * @param $reason string reason of disapproval
 * @return void
 */
	private function __sendSpaceApprovedDisapprovedEmail($spaceID = null, $emailTemplateID = null, $reason = null) {

		$spaceData = $this->Space->getSpaceUser($spaceID);
		$this->loadModel('EmailTemplate');

		$temp = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => $emailTemplateID)));
		$currentState = $spaceData['Space']['is_activated'] == true ? 'Activated':'Deactivated';
	   	$temp['EmailTemplate']['mail_body'] = str_replace(
	    	array('../../..', '#NAME', '#SPACENAME', '#ADDRESS','#CURRSTATE','#AVAILABLEDATES'),
	    	array(
	    			FULL_BASE_URL,
	    			$spaceData['User']['UserProfile']['first_name'] . ' ' . $spaceData['User']['UserProfile']['last_name'],
	    			$spaceData['Space']['name'],
	    			$spaceData['Space']['address1'],
	    			$currentState,
	    			CakeTime::format($spaceData['SpaceTimeDay']['from_date'], '%B %e, %Y').' - '.CakeTime::format($spaceData['SpaceTimeDay']['to_date'], '%B %e, %Y')
	    		),
	    	$temp['EmailTemplate']['mail_body']);

		if (!empty($reason)) {
			$temp['EmailTemplate']['mail_body'] = str_replace(
	    	array('../../..', '#NAME', '#SPACENAME', '#REASON'),
	    	array(
	    			FULL_BASE_URL,
	    			$spaceData['User']['UserProfile']['first_name'] . ' ' . $spaceData['User']['UserProfile']['last_name'],
	    			$spaceData['Space']['name'],
	    			$reason
	    		),
	    	$temp['EmailTemplate']['mail_body']);
		}    	
		
		if ($this->_sendEmailMessage($spaceData['User']['email'], $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject'])) {
			
			if (!empty($reason)) {
				$this->Session->setFlash(__('Space has been disapproved successfully.'), 'default', 'success');
				$this->redirect($this->referer());
			}
			return true;
		}

	}

/**
 * Method admin_disapproveSpace to disapprove user space added from frontend
 *
 * @return void
 */
	public function admin_disApproveSpace() {
		$this->request->allowMethod('post','put');
		
		$updateArray = array('Space.is_approved' => Configure::read('SpaceStatus.NotApproved'));
		$conditionsArray = array('Space.id' => $this->request->data['SpaceDisapprovalReason']['space_id']);

		if ($this->__dbTableUpdate('Space', $updateArray, $conditionsArray) && $this->__checkSpaceDisapproveReasonExists()) {
			if ($this->SpaceDisapprovalReason->save($this->request->data)) {

				//update user to untrusted by admin
				$updateUserArray = array('User.trusted_by_admin' => Configure::read('Bollean.False'));
				$conditionsUserArray = array('User.id' => $this->request->data['SpaceDisapprovalReason']['user_id']);
				$this->__dbTableUpdate('User', $updateUserArray, $conditionsUserArray);

				//change status of space to inactivate
				$updateSpaceArray = array('Space.is_activated' => Configure::read('Bollean.False'));
				$conditionsSpaceArray = array('Space.id' => $this->request->data['SpaceDisapprovalReason']['space_id']);
				$this->__dbTableUpdate('Space', $updateSpaceArray, $conditionsSpaceArray);

				$this->__sendSpaceApprovedDisapprovedEmail(
							$this->request->data['SpaceDisapprovalReason']['space_id'],
							Configure::read('EmailTemplateId.space_disapproved'),
							$this->request->data['SpaceDisapprovalReason']['reason']
						);
			}
			$errors = $this->SpaceDisapprovalReason->validationErrors;
            if (!empty($errors)) {
                $errorMsg = $this->_setValidaiotnError($errors);
            }
            $this->Session->setFlash(__('Space disapproval request not completed due to following error : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
            $this->redirect($this->referer());
		}
	}

/**
 * Method __checkSpaceDisapproveReasonExists to check is there any disapproval reason exists for space already, if exists 
 * then make request data according to update query, otherwise remain it as it is
 *
 * @return bool
 */
	private function __checkSpaceDisapproveReasonExists() {
		$checkExistReason = $this->SpaceDisapprovalReason->findBySpaceId($this->request->data['SpaceDisapprovalReason']['space_id'], array('id'));
		if (!empty($checkExistReason)) {
			$this->request->data['SpaceDisapprovalReason']['id'] = $checkExistReason['SpaceDisapprovalReason']['id'];
		}
		return true;
	}

/**
 * Method __dbTableUpdate common function to execute update query on table
 *
 * @param $model string the name of the model to update
 * @param $updateArray array contains the values to update in spaces table
 * @param $conditionsArray array contains the conditions value to update in spaces table
 * @return bool
 */
	private function __dbTableUpdate($model = null, $updateArray = array(), $conditionsArray = array()) {
		if (
				$this->$model->updateAll(
						$updateArray,
						$conditionsArray
					)
			) {
			return true;
		}
		return false;
	}


/**
 * Method getSpaceInfo to get the space from space id
 *
 * @param $spaceID the id of the particular space
 * @return void
 */
	public function getSpaceInfo($spaceID = null) {
		$spaceID = urldecode(base64_decode($spaceID));
		$spaceInfo = $this->Space->getSpaceInfo($spaceID);
		$this->set(
			array(
				'response' => $spaceInfo,
				'_serialize' => 'response'
				)
			);
	}

/**
 * Method admin_ajaxGetSpaceInfo to get the space from space id
 *
 * @param $spaceID the id of the particular space
 * @return void
 */
	public function admin_ajaxGetSpaceInfo($spaceID = null) {
		$spaceID = urldecode(base64_decode($spaceID));
		$spaceInfo = $this->Space->getSpaceInfo($spaceID);
		$this->set(
			array(
				'response' => $spaceInfo,
				'_serialize' => 'response'
				)
			);
	}



public function admin_setChargesSpace() 
{

	if(isset($this->request->data['SpaceChargeSetting'])) 
        {
           //CakeLog::write('debug','In admin_setChargeSpace() ' . print_r($this->request->data['SpaceChargeSetting'],true));

		if(isset($this->request->data['SpaceChargeSetting']['charge_service_tax_to_owner']) 
                  && $this->request->data['SpaceChargeSetting']['charge_service_tax_to_owner'] == 'on') 
                 {
		    $this->request->data['SpaceChargeSetting']['charge_service_tax_to_owner'] = Configure::read('Bollean.True');
		 }else{
		   $this->request->data['SpaceChargeSetting']['charge_service_tax_to_owner'] = Configure::read('Bollean.False');
		 }

                if(isset($this->request->data['SpaceChargeSetting']['one_time_charge']) &&
                   $this->request->data['SpaceChargeSetting']['one_time_charge'] == 'on')
                {
                   $this->request->data['SpaceChargeSetting']['one_time_charge'] = Configure::read('Bollean.True');
                }else{
                   $this->request->data['SpaceChargeSetting']['one_time_charge'] = Configure::read('Bollean.False');
                }


		if ($this->SpaceChargeSetting->save($this->request->data)) {

			$this->Session->setFlash(__('Details has been added successfully'), 'default', 'success');

			if (isset($this->request->data['SpaceChargeSetting']['id'])) {
				$this->Session->setFlash(__('Details has been updated successfully'), 'default', 'success');
			}
			$this->redirect($this->referer());
		} 
		$errors = $this->SpaceChargeSetting->validationErrors;
		if (!empty($errors)) {
			$errorMsg = $this->_setValidaiotnError($errors);
		}
		$this->Session->setFlash(__('Your Request could not completed due to following error : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
		$this->redirect($this->referer());
	}
}

	public function admin_ajaxSpaceChargeData($space_id = null) {
		$data = array();
		$space_id = urldecode(base64_decode($space_id));
		
		$getSpaceChargeData = $this->SpaceChargeSetting->findBySpaceId($space_id);
                //CakeLog::write('debug','GetSpaceChargeData ' . print_r($getSpaceChargeData,true));

		if(!empty($getSpaceChargeData)){
			$getSpaceChargeData = $getSpaceChargeData['SpaceChargeSetting'];
		} else {
			$getSpaceChargeData = $this->_getGloballyCommission(); //getting the globally set commission
			$getSpaceChargeData = $getSpaceChargeData['ServiceTaxCharges'];
		}
		$this->set(
				array(
					'response' => $getSpaceChargeData,
					'_serialize' => 'response'
				)
			);
	}


/**
 * Method spaceDetail to get the particular space details
 *
 * @param $spaceID id of the space
 * @return void
 */
public function spaceDetail($spaceID = null, $eventTimingID = null) 
{
	$this->layout = 'home';
	$spaceID = urldecode(base64_decode($spaceID));
        CakeLog::write('debug','++spaceDetail,spaceID ' . $spaceID . ' User ' . $this->Auth->user('id'));
	$getSpaceDetail = $this->Space->getSpaceInfo($spaceID);
	$this->set('getSpaceDetail',$getSpaceDetail);

	$getEventTime = array();
	if (!empty($eventTimingID)) {
		$eventTimingID = base64_decode(urldecode($eventTimingID));
		$this->loadModel('EventTiming');
		$getEventTime = $this->EventTiming->getEventDates($eventTimingID);
	}
	//CakeLog::write('debug','IN spaceDetail...' . print_r($getEventTime,true));
	$this->set('getEventTime',$getEventTime);

}

/**
 * Method SpaceDetailBulk to get the particular space details
 *
 * @param $spaceID id of the space
 * @return void
 */
public function spaceDetailBulk($spaceID = null) 
{
        if(!$this->_checkIfAdminOrUser())
        {
	   $this->Session->setFlash(__('Please login or create a SimplyPark account before proceeding with the booking.'), 'default', 'error');
	   $this->redirect('/?errorlogin=1&bulkBookingRedirect=1&spaceId='.$spaceID);
        }

	$this->layout = 'home';
	$spaceID = urldecode(base64_decode($spaceID));
	$getSpaceDetail = $this->Space->getSpaceInfo($spaceID);

        if($getSpaceDetail['Space']['allow_bulk_bookings'] == 0 || $getSpaceDetail['Space']['number_slots'] == 1)
        {
            $this->Session->setFlash(__('Listed Space does not support Bulk Bookings. Kindly contact SimplyPark at customercare@simplypark.in to resolve this'), 'default', 'error');
       	    $this->redirect($this->referer());
        }

        //CakeLog::write('debug','In spaceDetailBulk' . print_r($getSpaceDetail,true));
        $slotOptions = array();
        
       for ($i = 1; $i <= $getSpaceDetail['Space']['number_slots']; $i++) 
       {
          $slotOptions[$i] = $i; 
       }

        $this->loadModel('BulkBookingSetting');
        $getBulkBookingSettings = $this->BulkBookingSetting->getSettings($spaceID);
        //CakeLog::write('debug','After BulkBookingSetting :: ' . print_r($getBulkBookingSettings,true));
           
	$this->set('slotOptions',$slotOptions);
        $this->set('maxSlots', $getSpaceDetail['Space']['number_slots']);
        $this->set('bulkBookingSettings', $getBulkBookingSettings);
	$this->set('getSpaceDetail',$getSpaceDetail);
}

	public function admin_ajaxGetAdminSpaceData($space_id = null) {
		$data = array();
		$space_id = urldecode(base64_decode($space_id));
		$space_data = null;
		$space_data = $this->Space->find('first',array(
						'conditions' => array(
							'Space.id' => $space_id,
						),
						'fields' => array(
							'Space.id',
							'Space.name',
							'Space.description',
							'Space.place_pic',
							'Space.flat_apartment_number',
							'Space.admin_description',
							'Space.address1',
							'Space.address2',
							'Space.post_code',
							'State.name',
							'City.name',
							'Space.search_tags',
							'Space.search_string',
							'Space.lat',
							'Space.lng',
                                                        'Space.tower_number'
						),
					)
				);
		$this->set(
				array(
					'response' => $space_data,
					'_serialize' => 'response'
				)
			);
	}

public function admin_saveUpdateAdminSpaceData() 
{
	
                $this->request->data['Space']['mapsURL'] = $this->getShortMapsURL($this->request->data['Space']['lat'],
                                                                                  $this->request->data['Space']['lng']);   
   
                //CakeLog::write('debug','Inside admin saveUpdateAdminSpaceData..' . print_r($this->request->data,true));
		if($this->Space->save($this->request->data)) {
                     $this->Session->setFlash(__('Space has been successfully Updated'), 'default', 'success');

        	$this->redirect($this->referer());
        }

        $errors = $this->Space->validationErrors;
        if (!empty($errors)) {
            $errorMsg = $this->_setValidaiotnError($errors);
        }
        $this->Session->setFlash(__('Your Request could not completed due to following error : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
        $this->redirect($this->referer());
}

/**
 * Method ajax_sendSpaceLocation to send the space location as SMS
 *
 * @retun void
 */

public function ajax_sendSpaceLocationSMS()
{
   //CakeLog::write('debug','Inside sendSpaceLocation ...' . print_r($this->request->data,true));
   $spaceID = $this->request->data['space_id'];
   $userID = $this->request->data['user_id'];
   $mobile = $this->request->data['mobile_no'];
   $shortURL = $this->request->data['short_url'];

   if($this->_validateMobile($mobile,$mobileNumStr) == false){
           $sms['status'] = -1; //to denote invalid mobile number
           $sms['addedToDb'] = false;
   }else{
	   //CakeLog::write('debug','Inside SMS2...' . $mobileNumStr);

	   $this->loadModel('Message');
	   //getting message body
	   $messageContent = $this->Message->getMessage(Configure::read('MessageId.free_space_location')); 
	   //CakeLog::write('debug','Inside messageContent' . print_r($messageContent,true));
	   $smsText = str_replace('#SHORTURL',$shortURL,$messageContent['Message']['body']);

	   $messageResponse = $this->_sendMessageNotification($mobileNumStr,$smsText);


	   if ($messageResponse->code == Configure::read('HTTPStatusCode.OK')) 
	   {
		   //CakeLog::write('debug','SMS sent' . $messageResponse->body()); 
		   $sms['status'] = 1;
	   }else{
		   CakeLog::write('error','SMS send failed with ' . $messageResponse->reasonPhrase); 
		   $sms['status'] = 0;
	   }
           $this->loadModel('PotentialUser');

           $this->PotentialUser->addPotentialUser($spaceID,$userID,$mobile);
           $sms['addedToDb'] = true;
   }


   $this->set(
                array( 'response' => $sms,
                       '_serialize' => 'response'
                     )
             );

}



	public function ajax_validateBookingSpace() {
		
		$spaceID = $this->request->data['Space']['space_id'];


		$startDate = strtotime($this->request->data['Space']['start_date']);
	    $endDate = strtotime($this->request->data['Space']['end_date']);
	    /*$numOfDays = round(abs($startDate-$endDate)/86400);*/

           
      	// Aditya Changes for bug 1197 --Start--
	    $interval = $this->_getNumberOfDays($this->request->data['Space']['start_date'], $this->request->data['Space']['end_date']);
		//CakeLog::write('debug', print_r($interval->d, true));

        $numOfDays = $interval->days;
        $numOfHours = $interval->h;
        /* Add 1 to numOfDays as 3rd Jan - 1st Jan is 3 days and not 2. We need 
         * numOfDays inclusive of endDate 
         */
        if ( $numOfDays > 0 ) {
           $numOfDays = $numOfDays + 1;
           $numOfHours = 0; /* we don't care about hours if days > 1 */
        }
        //CakeLog::write('debug', print_r($numOfDays, true));
       	/* Aditya Changes for bug 1197 --End--*/ 

       	// if days are greater than two months, then take advance deposit of one month
	    if ($numOfDays > Configure::read('MonthDays')) {
	    	//$spaceDepositDetails = $this->Space->getDepositInfo($spaceID);
	    	$getBokkingStatus['monthrefund'] = 1;
	    }

	    $getBokkingStatus['type'] = 'success';
            /* Aditya Changes for bug 1197 --Start Multiply Rate with numOfDays to show correct rate--*/
	    $getBokkingStatus['rate'] = $this->_getRateFromTime($numOfDays,$numOfHours,$spaceID);
            /* Aditya Changes for bug 1197 --End-*/ 
	    $getBokkingStatus['months'] = $interval->m;
	    $getBokkingStatus['days'] = $interval->d;
	    $getBokkingStatus['years'] = $interval->y;

	    //code to calculate long term booking prices
	    if (($interval->m > 0 && $interval->d > 0) || $interval->m > 1 || $interval->y > 0) {
		    
		    if ($interval->y > 0) {
		    	$totalPayRate = $getBokkingStatus['rate'];
		    	$monthlyInstallments = $getBokkingStatus['rate']/12;
		    } elseif ($interval->m > 0) {
		    	$totalPayRate = ($getBokkingStatus['rate']*$interval->m) + (($getBokkingStatus['rate']/30)*$interval->d);
		    	$monthlyInstallments = $getBokkingStatus['rate'];
		    }
		    $totalPayRightNow = $monthlyInstallments+$monthlyInstallments;
		    
		    $yealyMsg = 'Total booking amount for your parking duration is Rs.'.round($totalPayRate,2).'.';
		    $yealyMsg .= ' Collected in monthly instalments of Rs. '.round($monthlyInstallments,2).'.';
		    $yealyMsg .= ' A Refundable deposit of Rs. '.round($monthlyInstallments,2).' is also collected at the time of booking.';
		    $yealyMsg .= ' Total to Pay right now : Rs.'.round($totalPayRightNow,2).' /-';
		    $yealyMsg .= '(A small convenience fees will also be charged by SimplyPark)';
		    $getBokkingStatus['message'] = $yealyMsg;
	    }

	    if (!$this->_checkEmptySlot()) {
	    	$getBokkingStatus['type'] = 'error';
	    	$getBokkingStatus['message'] = 'Space is not available for the selected date and time.';
	    } else {
	    	$getBokkingStatus['space_park_id'] = $this->request->data['Space']['space_park_id'];
		    $unavailableDays = $this->__getWeekDays($numOfDays,$startDate,$spaceID);
		    if (!empty($unavailableDays)) {
		    	$getBokkingStatus['type'] = 'error';
		    	$getBokkingStatus['message'] = 'Space is not available on '. implode(',', $unavailableDays) . ' . Please select other dates.';
		    }

		    if ($getBokkingStatus['rate'] <= Configure::read('SpaceUnavailabelPrice') && $getBokkingStatus['type'] != 'error') {
		    	$getBokkingStatus['type'] = 'error';
		    	$getBokkingStatus['message'] = __('Space price for selected dates is not available, so the space can not be book on these dates.');
		    }
	    }
	    
		$this->set(
				array(
						'response' => $getBokkingStatus,
						'_serialize' => 'response'
					)
			);
	}

/**
 * Method isSpaceAvailable to calculate booking Price and spaceAvailability 
 *
 * @retun void
 */
public function isSpaceAvailable($spaceDetailObj,$startDateTimeStr,$endDateTimeStr,&$bookingPrice)
{
   CakeLog::write('debug','Inside isSpaceAvailable..ssadf..' . print_r($spaceDetailObj,true));
    $spaceID = $spaceDetailObj['Space']['id'];
    //CakeLog::write('debug','Inside isSpaceAvailable ..' . $spaceID);	

   //CakeLog::write('debug','in isSpaceAvailable....' . print_r($spaceDetailObj,true)); 
   //is the booking time within the space operating hours
   if(!isset($spaceDetailObj['SpaceTimeDay']['open_all_hours']) || $spaceDetailObj['SpaceTimeDay']['open_all_hours'] == 0)
   {
      $openingTime = date("H:i", strtotime($spaceDetailObj['SpaceTimeDay']['from_date'])); 
      $closingTime = date("H:i", strtotime($spaceDetailObj['SpaceTimeDay']['to_date'])); 

      //CakeLog::write('debug','OpeningTime ' . $openingTime);
      //CakeLog::write('debug','ClosingTime ' . $closingTime);


      $bookingStartTime = date("H:i", strtotime($startDateTimeStr));
      $bookingEndTime   = date("H:i", strtotime($endDateTimeStr));

      //CakeLog::write('debug','BookingStartTime ' . $bookingStartTime);
      //CakeLog::write('debug','BookingClosingTime ' . $bookingEndTime);
      //CakeLog::write('debug','OpeningTime ' . $openingTime);
      //CakeLog::write('debug','ClosingTime ' . $closingTime);
      
      $openingDateTimeObj  = new DateTime($spaceDetailObj['SpaceTimeDay']['from_date']);
      $closingDateTimeObj = new DateTime($spaceDetailObj['SpaceTimeDay']['to_date']);  
      $openingHour = intval($openingDateTimeObj->format('H'));
      $closingHour = intval($closingDateTimeObj->format('H'));

      $bookingStartTimeInt = (int)substr($bookingStartTime,0,2);
      $bookingEndTimeInt   = (int)substr($bookingEndTime,0,2);
      $openingTimeInt      = (int)substr($openingTime,0,2);
      $closingTimeInt      = (int)substr($closingTime,0,2);
      
      //CakeLog::write('debug','BookingStartTimeInt ' . $bookingStartTimeInt);
      //CakeLog::write('debug','BookingClosingTimeInt ' . $bookingEndTimeInt);
      //CakeLog::write('debug','OpeningTimeInt ' . $openingTimeInt);
      //CakeLog::write('debug','ClosingTimeInt ' . $closingTimeInt);

      if($openingHour < $closingHour)
      {
	      if(!($bookingStartTimeInt >= $openingTimeInt && $bookingStartTimeInt <= $closingTimeInt && 
		      $bookingEndTimeInt >= $openingTimeInt && $bookingEndTimeInt <= $closingTimeInt))
	      {

		      $bookingPrice = 0;
		      //CakeLog::write('debug','return false 1');
		      return false;
	      }
      }else{
	      $bookingStartObj = new DateTime($startDateTimeStr);
              $bookingEndObj   = new DateTime($endDateTimeStr);
              /*
              $diffInterval = $bookingStartObj->diff($bookingEndObj);

	      if($diffInterval->y || $diffInterval->m || $diffInterval->d)
	      {
		      if($bookingStartTimeInt < $openingTimeInt || $bookingEndTimeInt > $closingTimeInt)
		      {
		              CakeLog::write('debug','return false 1.1');
			      return false;
		      }  
	      }else{
		      //hourly booking
		      if($bookingStartTimeInt < $openingTimeInt)
		       {
		               CakeLog::write('debug','return false 1.2');
			       return false;
		       }
		      
	      }
	       */
	      if($bookingStartTimeInt > $closingTimeInt && $bookingStartTimeInt < $openingTimeInt)
	      {
	              //CakeLog::write('debug','return false 1.1');
		      return false;
	      }

	      if($bookingEndTimeInt > $closingTimeInt && $bookingEndTimeInt < $closingTimeInt)
	      { 
	              //CakeLog::write('debug','return false 1.2');
		      return false;
	      }

      }

   }
 
   //is the space available to be booked for the booking duration
   $bookingStartDateTimeObj = new DateTime($startDateTimeStr);
   $bookingEndDateTimeObj   = new DateTime($endDateTimeStr);


   $isTierPricingEnabled = ($spaceDetailObj['Space']['tier_pricing_support'])?1:0;
   $bookingType = 0; /* 0 is NA, 1 is hourly, 2 is daily, 3 is weekly, 4 is monthly, 5 is yearly */
   $isOpen24Hours = false;
   
   if ($spaceDetailObj['SpaceTimeDay']['open_all_hours']) 
   {
     $isOpen24Hours = true;
     $openHours = 24; 
    }
   else
    {
       $openingDateTimeObj  = new DateTime($spaceDetailObj['SpaceTimeDay']['from_date']);
       $closingDateTimeObj = new DateTime($spaceDetailObj['SpaceTimeDay']['to_date']);  
       $openingHour = intval($openingDateTimeObj->format('H'));
       $closingHour = intval($closingDateTimeObj->format('H'));
       $openingMinutes = intval($openingDateTimeObj->format('i'));
       $closingMinutes = intval($closingDateTimeObj->format('i'));

       if ( $openingMinutes > 0 )
       {
          $openingHour = $openingHour + 1;
       }

       if ( $closingHour > 0 )
       {
          $closingHour = $closingHour - 1;
       }

       if($closingHour < $openingHour)
       {
          $openHour = $closingHour + 24 - $openingHour;
       }else{	  
	  $openHours = $closingHour - $openingHour;
       }
     }

     $bookingEndHour   = intval($bookingEndDateTimeObj->format('H'));
     $bookingStartHour = intval($bookingStartDateTimeObj->format('H'));

     if ($bookingEndHour < $bookingStartHour AND !$isOpen24Hours)
     {
        $hoursToAdd = $openHours - ($bookingStartHour - $bookingEndHour); 
        $bookingStartDateTimeObj->setTime(23,0);
        $bookingEndDateTimeObj->setTime($hoursToAdd-1,0);
     }
   else if ($bookingEndHour > $bookingStartHour AND (($bookingEndHour - $bookingStartHour) == $openHours) AND !$isOpen24Hours)
    {
       $bookingEndDateTimeObj->modify("+1 day");
       $bookingEndDateTimeObj->setTime($bookingStartHour,0);
     }

    $diffInterval = $bookingStartDateTimeObj->diff($bookingEndDateTimeObj);

   if($diffInterval->y)
   { 
     if($spaceDetailObj['SpacePricing'][0]['yearly'] == 0.00)
     {
        $bookingPrice = 0;
        //CakeLog::write('debug','return false 2');
        return false;
     }else{
        $bookingType = 5;
     }
   }else if($diffInterval->y ==0 && $diffInterval->m)
   {
      if($spaceDetailObj['SpacePricing'][0]['monthly'] == 0.00)
      {
         $bookingPrice = 0;
         //CakeLog::write('debug','return false 3');
         return false;
      }else{
        $bookingType = 4;
      }
   }else if($diffInterval->y == 0 && $diffInterval->m == 0 && $diffInterval->days >= 7)
   {
      if($spaceDetailObj['SpacePricing'][0]['weekely'] == 0.00)
      {
         $bookingPrice = 0;
         //CakeLog::write('debug','return false 4');
         return false;
      }else{
         $bookingType = 3;
      }
   }else if($diffInterval->y == 0 && $diffInterval->m == 00 && $diffInterval->days && $diffInterval->days < 7) 
   {

      if($spaceDetailObj['SpacePricing'][0]['daily'] == 0.00)
      {
         $bookingPrice = 0;
         //CakeLog::write('debug','return false 5');
         return false;
      }else{
         $bookingType = 2;
      }
   }else if($diffInterval->y ==0 && $diffInterval->m == 0 && $diffInterval->d == 0 && $diffInterval->h) 
   {
      if($spaceDetailObj['SpacePricing'][0]['hourly'] == 0.00 && $isTierPricingEnabled == 0)
      {
         $bookingPrice = 0;
         //CakeLog::write('debug','return false 6');
         return false;
      }else{
         $bookingType = 1;
      }
   }else{
     CakeLog::write('error', 'ERROR while calculating booking TYPE');
     $bookingPrice = 0;
         //CakeLog::write('debug','return false 7');
     return false;
   }


  
   //is the space open during the booking days 

   $this->loadModel('SpaceTimeDay');

   $spaceOpenDays = $this->SpaceTimeDay->getSpaceDays($spaceID);
   $openDaysBitmap = 0;

   if ($spaceOpenDays['SpaceTimeDay']['mon'])
	 $openDaysBitmap |= (1 << 0);
   if ($spaceOpenDays['SpaceTimeDay']['tue'])
	 $openDaysBitmap |= (1 << 1);
   if ($spaceOpenDays['SpaceTimeDay']['wed'])
	 $openDaysBitmap |= (1 << 2);
   if ($spaceOpenDays['SpaceTimeDay']['thu'])
	 $openDaysBitmap |= (1 << 3);
   if ($spaceOpenDays['SpaceTimeDay']['fri'])
	 $openDaysBitmap |= (1 << 4);
   if ($spaceOpenDays['SpaceTimeDay']['sat'])
	 $openDaysBitmap |= (1 << 5);
   if ($spaceOpenDays['SpaceTimeDay']['sun'])
	 $openDaysBitmap |= (1 << 6);
 
   $dayBitPosArray = array (
			 'Mon' => 0,
			 'Tue' => 1,
			 'Wed' => 2,
			 'Thu' => 3,
			 'Fri' => 4,
			 'Sat' => 5,
			 'Sun' => 6
			 );
  
   $startDay = $bookingStartDateTimeObj->format('D');
   $index    = $dayBitPosArray[$startDay];

   $numOfDays = ($bookingType == 1)?1:($diffInterval->d > 7)?7:$diffInterval->d;
   for($itr = 0; $itr < $numOfDays; $itr++)
   {
      if ( !($openDaysBitmap & (1 << $index ) ) )
      {
         $bookingPrice = 0;
         return false;
      } 

      $index = ($index + 1)%7;
   }
 

   if($spaceDetailObj['Space']['is_available_with_simplypark'])
   { 
	   //is their any slot available during the booking days 

	   $startDate = date('Y-m-d H:i:s', strtotime($startDateTimeStr));
	   $endDate = date('Y-m-d H:i:s', strtotime($endDateTimeStr));


	   $totalSlots     = $this->Space->getAllSlots($spaceID);
	   $getBookedSlots = $this->Booking->getBookedRecordsOfSpace($spaceID,$startDate,$endDate);

	   if(!empty($getBookedSlots))
	   {
		   $getBookedSlots = array_map("unserialize", array_unique(array_map("serialize", $getBookedSlots)));
		   //CakeLog::write('debug','after applying array_map' . print_r($getBookedSlots,true));

		   $nextAvailableSlotId = 0;
		   $numBookedSlots = 0;
		   $numSlotsToBook = 1;

		   foreach($getBookedSlots as $slots)
		   {
			   $numBookedSlots += $slots['Booking']['num_slots'];
		   }

		   if($numBookedSlots + $numSlotsToBook > $totalSlots['Space']['number_slots'])
		   {
			   $bookingPrice = 0;
			   return false;
		   }
	   }


	   if($isTierPricingEnabled && $bookingType == 1) 
	   {
		   $this->loadModel('SpaceTierPricing');
		   $bookingPrice = $this->SpaceTierPricing->getTierHourlyPrice($spaceID,$diffInterval->h);
	   }else{

		   $bookingPrice = $this->_getBookingPrice($diffInterval,$openHours,$spaceDetailObj['SpacePricing'][0]['hourly'],
			   $spaceDetailObj['SpacePricing'][0]['daily'],
			   $spaceDetailObj['SpacePricing'][0]['weekely'],
			   $spaceDetailObj['SpacePricing'][0]['monthly'],
			   $spaceDetailObj['SpacePricing'][0]['yearly']);
	   }
   } 

  //CakeLog::write('debug','Returning from isSpaceAvailable as TRUE');
  return true;

} 
/**
 * Method validateBookingSpace to validate booking space form before submitting
 *
 * @retun void
 */
  public function ajax_validateBookingSpaceNew() 
  {

	  $spaceId = $this->request->data['Space']['space_id'];
	  $isEvent = $this->request->data['Space']['is_Event'];

          CakeLog::write('debug','Received SpaceID' . $spaceId);
          //CakeLog::write('debug','AdvanceYearlyPaymentSpaceId' . Configure::read('AdvanceYearlyPaymentSpaceID'));


	  $startDateTimeObj = new DateTime($this->request->data['Space']['start_date']); 
	  $endDateTimeObj   = new DateTime($this->request->data['Space']['end_date']); 
	  $tempEndDateTimeObj   = clone $endDateTimeObj;
	  $tempStartDateTimeObj = clone $startDateTimeObj;
          CakeLog::write('debug','++AjaxRequestForSpaceID ' . $spaceId . 'User ' . $this->Auth->user('id'));

	  $openHours = 0;
	  $isOpen24Hours = false;
          $error = 0;
          //this function also if deposit is to be collected 
	  $spaceInfo = $this->Space->getSpacePricingAndOperatingHours($spaceId);

	  CakeLog::write('debug','ajax-----' . print_r($spaceInfo,true));
          
          $dailyPrice   = $spaceInfo['SpacePricing'][0]['daily'];
          $weeklyPrice  = $spaceInfo['SpacePricing'][0]['weekely'];
          $monthlyPrice = $spaceInfo['SpacePricing'][0]['monthly'];
          $yearlyPrice  = $spaceInfo['SpacePricing'][0]['yearly'];
          $hourlyPrice  = 0.00;
          
	  
	  $isYearlyPaidInAdvance = 0;
	  
	  if($spaceInfo['Space']['is_yearly_payment_collected_in_advance']){
	     $isYearlyPaidInAdvance = 1;
	  }

          //CakeLog::write('debug','IN ajax_validateBookingSpaceNew...' . print_r($spaceInfo,true));
          $isTierPricingEnabled = 0;

          if($spaceInfo['Space']['tier_pricing_support']){
             $isTierPricingEnabled = 1;
          }else{
             $hourlyPrice  = $spaceInfo['SpacePricing'][0]['hourly'];
          }

          $spaceOpenBitMap = 0;
          $unAvailStr = "";



	  if ( $spaceInfo['SpaceTimeDay']['open_all_hours'] ) 
	  {
		  $isOpen24Hours = true;
		  $openHours = 24; 
	  }
	  else
	  {
             $openingDateTimeObj  = new DateTime($spaceInfo['SpaceTimeDay']['from_date']);
             $closingDateTimeObj = new DateTime($spaceInfo['SpaceTimeDay']['to_date']);  
             $openingHour = intval($openingDateTimeObj->format('H'));
             $closingHour = intval($closingDateTimeObj->format('H'));
             $openingMinutes = intval($openingDateTimeObj->format('i'));
             $closingMinutes = intval($closingDateTimeObj->format('i'));

             if ( $openingMinutes > 0 )
             {
                $openingHour = $openingHour + 1;
             }

             if ( $closingHour > 0 )
             {
                $closingHour = $closingHour - 1;
             }

	     if($closingHour < $openingHour)
	     {
	        $openHours = $closingHour + 24 - $openingHour;
             }else{		
	        $openHours = $closingHour - $openingHour;
	     }
	  }

	  $bookingEndHour   = intval($endDateTimeObj->format('H'));
	  $bookingStartHour = intval($startDateTimeObj->format('H'));

	  if ( $bookingEndHour < $bookingStartHour AND !$isOpen24Hours )
	  {
		  $hoursToAdd = $openHours - ($bookingStartHour - $bookingEndHour); 
		  $tempStartDateTimeObj->setTime(23,0);
		  $tempEndDateTimeObj->setTime($hoursToAdd-1,0);
	  }
	  else if ( $bookingEndHour > $bookingStartHour AND (($bookingEndHour - $bookingStartHour) > $openHours) AND !$isOpen24Hours )
	  {
		  $tempEndDateTimeObj->modify("+1 day");
		  $tempEndDateTimeObj->setTime($bookingStartHour,0);
	  }

	  $diffInterval = $tempStartDateTimeObj->diff($tempEndDateTimeObj);
          //CakeLog::write('debug',"TempStartDateTimeObj" . print_r($tempStartDateTimeObj,true));
          //CakeLog::write('debug',"TempEndDateTimeObj" . print_r($tempEndDateTimeObj,true));
          //CakeLog::write('debug',"bookingEndHour". $bookingEndHour);
          //CakeLog::write('debug',"bookingStartHour". $bookingStartHour);
          //CakeLog::write('debug',"openHours". $openHours);
          //CakeLog::write('debug','DiFFINTERVal...' . print_r($diffInterval,true));
	  
          if ( $diffInterval->y && $yearlyPrice == 0.00 )
	  {
		  $getBookingStatus['type'] = 'error';
		  $getBookingStatus['message'] = __('The current space is not available to be booked on yearly basis.');
                  $error = 1;
	  }
	  else if ( $diffInterval->m && $monthlyPrice == 0.00 )
	  {
		  $getBookingStatus['type'] = 'error';
		  $getBookingStatus['message'] = __('The current space is not available to be booked on monthly basis.');
                  $error = 1;
	  }
	  else if ( $diffInterval->y == 0 && $diffInterval->m == 0 && $diffInterval->days >= 7 && 
                    $dailyPrice == 0.00 && $weeklyPrice == 0.00 )
	  {
		  $getBookingStatus['type'] = 'error';
		  $getBookingStatus['message'] = __('The current space is not available to be booked on weekly basis.');
                  $error = 1;
	  }
	  else if ( $diffInterval->y == 0 && $diffInterval->m == 0 && $diffInterval->days && $diffInterval->days < 7 && 
                    $dailyPrice == 0.00 )
	  {
		  $getBookingStatus['type'] = 'error';
		  $getBookingStatus['message'] = __('The current space is not available to be booked on daily basis.');
                  $error = 1;
	  }
	  else if ( $diffInterval->y ==0 && $diffInterval->m == 0 && $diffInterval->d == 0 && $diffInterval->h && 
                    $hourlyPrice == 0.00 && $isTierPricingEnabled == 0)
	  {
		  $getBookingStatus['type'] = 'error';
		  $getBookingStatus['message'] = __('The current space is not available to be booked on hourly basis.');
                  $error = 1;
	  }


	  if (!$this->_checkEmptySlot()) 
	  {
		  $getBookingStatus['type'] = 'error';
		  $getBookingStatus['message'] = 'The current space is booked for the selected date and time.';
                  $error = 1;
	  } 
	  else 
	  {
		  $getBookingStatus['space_park_id'] = $this->request->data['Space']['space_park_id'];
                  //CakeLog::write('debug','Setting space_park_id as ' . $getBookingStatus['space_park_id']);
                  $spaceOpenBitMap = $this->__NewgetWeekDays($diffInterval->days,$startDateTimeObj,$spaceId,$unAvailStr);

		  if (!empty($unAvailStr)) 
		  {
		     $getBookingStatus['type'] = 'error';
		     $getBookingStatus['message'] = 'Space is not available on '. $unAvailStr;
                     
                    if($isEvent)
                     {
                        $getBookingStatus['message'] .= ' . Please go back and choose another space.';
                     }
                    else
                     {
                        $getBookingStatus['message'] .=' . Please select other dates.';
                     }

                     $error = 1;
		  }

	  }

          if ( !$error )
	  {
                  if($isTierPricingEnabled && $diffInterval->y == 0 && $diffInterval->m == 0 && $diffInterval->d == 0) {
                    $this->loadModel('SpaceTierPricing');
                    //CakeLog::write('debug','Number of HOURS ..' . $diffInterval->h);
                    $bookingPrice = $this->SpaceTierPricing->getTierHourlyPrice($spaceId,$diffInterval->h);

                  }else{
		      $bookingPrice = $this->_getBookingPrice($diffInterval,$openHours,$hourlyPrice,$dailyPrice,$weeklyPrice,
				                              $monthlyPrice,$yearlyPrice);
                 }


                  //CakeLog::write('debug','diffInterval ' . print_r($diffInterval,true));
		  //TODO: what if the booking is for 1 month + few hours. Please test this 
		  if ( ($diffInterval->y) || ($diffInterval->m > 1) || ($diffInterval->m == 1 && $diffInterval->d ) )
		  {
			  $getBookingStatus['deposit'] = ($spaceInfo['Space']['is_deposit_required']?1:0);
			  $bookingMsg = '';
			  $isBookingYearly = 0;

			  if ( $diffInterval->y )
			  {
				  //CakeLog::write('debug','Is yearly');
				  $isBookingYearly = 1;
			          if($isYearlyPaidInAdvance == 0)
			          {		 
				     $monthlyInstallments  = ($bookingPrice/12);
				     $numberOfInstallments = 11;
				     $monthlyPrice = ceil($monthlyInstallments);
				  }
                                  
			  }
			  else if ( $diffInterval->m > 0 )
			  {
                                  if ( $diffInterval->m == 1 )
                                  {
                                     $numberOfInstallments = 1;
                                     $monthlyInstallments  = $monthlyPrice; 
                                  }
                                  else
                                  {
				     $monthlyInstallments = ($bookingPrice/$diffInterval->m);
				     $numberOfInstallments = $diffInterval->m - 1;
                                  }
                                  $bookingMsg = 'Total Booking Amount for ' . $diffInterval->m . ' month(s)';
                                  if ( $diffInterval->d )
                                  {
                                     $bookingMsg .=' and ' . $diffInterval->d . ' day(s)';
                                  }
			  }
			  
			  $installmentMsg = '<div align=center>';
			  
			  if($isBookingYearly && $isYearlyPaidInAdvance)
			  {
                             $installmentMsg .= 'The discounted yearly price is available ONLY when paid in advance.<br />Please be aware this booking is NON cancellable';
			  }else{ 
			     $installmentMsg = '<div align=center>';
                             $installmentMsg .= 'The amount will be collected in monthly installments<br />';

                             if($spaceInfo['Space']['is_deposit_required']){
                                $installmentMsg .= 'To help process cancellations, a refundable deposit of Rs.' . round($monthlyPrice,2) . '<br />will also be collected with this booking';
                             }

                          }

			  $installmentMsg .= '</div>';
			  //$installmentMsg .= '<table id=longtermbooking>';
			  //$installmentMsg .= '<tr><td id=longtermbookingcol1>' . $bookingMsg . '</td> <td id=longtermbookingcol2><b>Rs.'. round($bookingPrice,2) . '</b></td></tr>';
			  //$installmentMsg .= '<tr><td id=longtermbookingcol1>Number of Installments</td> <td id=longtermbookingcol2><b>'. $numberOfInstallments.'</b></td></tr>';
			  //$installmentMsg .= '<tr><td id=longtermbookingcol1>Refundable deposit</td><td id=longtermbookingcol2><b>Rs.'. round($monthlyInstallments,2).'</b></td></tr>';
			  //$installmentsg .= '<tr><td id=longtermbookingcol1>Current Booking Amount</td><td id=longtermbookingcol2><b>Rs.'. round($totalPayRightNow,2) . '*</b></td></tr>';
			  //$installmentsg .= '<tr><td colspan=2><small>*SimplyPark will charge a small convenience fee. <br/>Service Tax may also be levied</small></td></tr>';
			  //$installmentMsg .= '</table>';
			  //$installmentMsg .= '</div>';

                          $getBookingStatus['installmentMsg'] = $installmentMsg;

		  }

		  $getBookingStatus['type'] = 'success';
		  $getBookingStatus['bookingPrice'] = $bookingPrice;

                  if(!$isEvent)
		  {
			  //now and try and suggest a different date to decrease the cost for the driver
			  $endYear    = intval($tempEndDateTimeObj->format('Y'));
			  $startYear  = intval($tempStartDateTimeObj->format('Y'));
			  $endMonth   = intval($tempEndDateTimeObj->format('m'));
			  $startMonth = intval($tempStartDateTimeObj->format('m'));
			  $endDay     = intval($tempEndDateTimeObj->format('d'));
			  $startDay   = intval($tempStartDateTimeObj->format('d'));
			  $daysToMonth = 0;
			  $tempBookingPrice = $bookingPrice;
			  $addSingleDay = 0;
			  $timeModString = ''; 
			  $daysToWeek = 7 - ($diffInterval->d % 7);

			  if ( $diffInterval->days > 360 )
			  {
				  $tempBookingPrice = $yearlyPrice;
				  $daysToYear = abs($startDay  - $endDay);

				  if ( $diffInterval->h ) 
				  {
					  $endDateTimeObj->setTime($bookingStartHour,0);
				  }
				  $endDateTimeObj->modify("+$daysToYear day");
				  $bookingSuggestion = "Change your End Date to " . $endDateTimeObj->format('d-m-Y H:i');
			  }
			  else if ( $startYear == $endYear AND $startMonth == $endMonth AND $diffInterval->d >= 27 )
			  {

				  switch($endMonth)
				  {
					  case 1:
					  case 3:
					  case 5:
					  case 7:
					  case 8:
					  case 10:
					  case 12:
						  $daysToMonth = 31 - $endDay;
						  break;
					  case 2:
						  if ( ($endYear % 4 == 0) AND ($endYear % 100 == 0 ) AND ($endYear % 400 == 0 ) )
						  {
							  $daysToMonth = 29 - $endDay;
						  }
						  else
						  {
							  $daysToMonth = 28 - $endDay;
						  }
						  break;
					  default:
						  $daysToMonth = 30 - $endDay;
				  }
				  $daysToMonth +=1;
				  $timeModString = 'P'.$daysToMonth.'D';

				  if ( $diffInterval->h ) 
				  {
					  //echo "bookingStartHour:: $bookingStartHour";
					  $endDateTimeObj->setTime($bookingStartHour,0);
				  }
				  $bookingSuggestion = "Change your End Date to ";
			  }
			  else if ( $startMonth != $endMonth AND $startDay >= $endDay AND ($startDay - $endDay) < 3 )
			  {
				  $daysToMonth = $startDay - $endDay;
				  $timeModString = 'P'.$daysToMonth.'D';

				  if ( $diffInterval->h ) 
				  {
					  $endDateTimeObj->setTime($bookingStartHour,0);
				  }
				  $bookingSuggestion = "Change your End Date to ";
			  }
			  else if ( $diffInterval->y == 0 AND $diffInterval->m == 0 AND $daysToWeek < 4)
			  {
				  // try and calculate the weekly price 
				  $timeModString = 'P'.$daysToWeek.'D';

				  if ( $diffInterval->h ) 
				  {
					  $endDateTimeObj->setTime($bookingStartHour,0);
				  }
				  $bookingSuggestion = "Change your End Date to ";
			  }
			  else if ( $diffInterval->h >= ((80 * $openHours)/100) ) //80% of a day
			  {
				  $hoursToAdd = $openHours - $diffInterval->h;
				  if ( $isOpen24Hours )
				  {
					  $timeModString = 'PT'.$hoursToAdd.'H';
				  }
				  else
				  {
					  $addSingleDay = 1;
				  }

				  $bookingSuggestion = "Change your End Date to ";
			  }



			  if ( !empty($timeModString)  OR $addSingleDay )
			  {
				  if ( !empty($timeModString) )
				  {
					  //echo "<br />TimeModString:: $timeModString";
					  $endDateTimeObj->add(new DateInterval($timeModString));
					  //echo "<br />endDateTimeObj " . print_r($endDateTimeObj,true);
				  }

				  if ( $addSingleDay )
				  {
					  $endDateTimeObj->modify('+1 day');
					  $endDateTimeObj->setTime($bookingStartHour,0);
				  }
				  $bookingSuggestion .= $endDateTimeObj->format('d-m-Y H:i');

				  $tempDiffInterval = $startDateTimeObj->diff($endDateTimeObj); 
				  //echo "<br />";
				  //echo print_r($tempDiffInterval,true);
				  $tempBookingPrice = $this->_getBookingPrice($tempDiffInterval,$openHours,$hourlyPrice,$dailyPrice,$weeklyPrice,$monthlyPrice,$yearlyPrice);
			  }



			  if ( $tempBookingPrice < $bookingPrice )
			  {
				  if ( $this->_isSpaceAvailableOnThisDate($spaceOpenBitMap,$endDateTimeObj) )
				  {
					  $diff = $bookingPrice - $tempBookingPrice;
					  $bookingSuggestion .= " and pay Rs. $diff less.";
					  $getBookingStatus['bookingSuggestion'] = $bookingSuggestion;
				  }
			  }
		  }
	  }

	  CakeLog::write('debug',".....BookingResponse..." . print_r($getBookingStatus,true));


	  $this->set(
			  array(
				  'response' => $getBookingStatus,
				  '_serialize' => 'response'
			       )
		    );
  }


/**
 * Method validateBulkBookingSpace to validate booking space form before submitting
 *
 * @retun void
 */
  public function ajax_validateBulkBookingSpace() 
  {

	  $spaceId = $this->request->data['Space']['space_id'];
          $numSlots = $this->request->data['Space']['num_slots'];

          //CakeLog::write('debug','Inside validateBulkBookingSpace' . print_r($this->request,true));

	  $startDateTimeObj = new DateTime($this->request->data['Space']['start_date']); 
	  $endDateTimeObj   = new DateTime($this->request->data['Space']['end_date']); 
	  $tempEndDateTimeObj   = clone $endDateTimeObj;
	  $tempStartDateTimeObj = clone $startDateTimeObj;

	  $openHours = 0;
	  $isOpen24Hours = false;
          $error = 0;

	  $spaceInfo = $this->Space->getSpaceOperatingHours($spaceId); 
          $this->loadModel('BulkBookingSetting');
          $bulkBookingSettings = $this->BulkBookingSetting->getSettings($spaceId);

          $hourlyPrice  = $bulkBookingSettings['BulkBookingSetting']['hourly_price'];
          $dailyPrice   = $bulkBookingSettings['BulkBookingSetting']['daily_price'];
          $weeklyPrice  = $bulkBookingSettings['BulkBookingSetting']['weekly_price'];
          $monthlyPrice = $bulkBookingSettings['BulkBookingSetting']['monthly_price'];
          $yearlyPrice  = $bulkBookingSettings['BulkBookingSetting']['yearly_price'];
          $spaceOpenBitMap = 0;
          $unAvailStr = "";



	  if ( $spaceInfo['SpaceTimeDay']['open_all_hours'] ) 
	  {
		  $isOpen24Hours = true;
		  $openHours = 24; 
	  }
	  else
	  {
             $openingDateTimeObj  = new DateTime($spaceInfo['SpaceTimeDay']['from_date']);
             $closingDateTimeObj = new DateTime($spaceInfo['SpaceTimeDay']['to_date']);  
             $openingHour = intval($openingDateTimeObj->format('H'));
             $closingHour = intval($closingDateTimeObj->format('H'));
             $openingMinutes = intval($openingDateTimeObj->format('i'));
             $closingMinutes = intval($closingDateTimeObj->format('i'));

             if ( $openingMinutes > 0 )
             {
                $openingHour = $openingHour + 1;
             }

             if ( $closingHour > 0 )
             {
                $closingHour = $closingHour - 1;
             }

             $openHours = $closingHour - $openingHour;
	  }

	  $bookingEndHour   = intval($endDateTimeObj->format('H'));
	  $bookingStartHour = intval($startDateTimeObj->format('H'));

	  if ( $bookingEndHour < $bookingStartHour AND !$isOpen24Hours )
	  {
		  $hoursToAdd = $openHours - ($bookingStartHour - $bookingEndHour); 
		  $tempStartDateTimeObj->setTime(23,0);
		  $tempEndDateTimeObj->setTime($hoursToAdd-1,0);
	  }
	  else if ( $bookingEndHour > $bookingStartHour AND (($bookingEndHour - $bookingStartHour) == $openHours) AND !$isOpen24Hours )
	  {
		  $tempEndDateTimeObj->modify("+1 day");
		  $tempEndDateTimeObj->setTime($bookingStartHour,0);
	  }

	  $diffInterval = $tempStartDateTimeObj->diff($tempEndDateTimeObj);
	  
          if ( $diffInterval->y && $yearlyPrice == 0.00 )
	  {
		  $getBookingStatus['type'] = 'error';
		  $getBookingStatus['message'] = __('The current space is not available to be booked on yearly basis.');
                  $error = 1;
	  }
	  else if ( $diffInterval->m && $monthlyPrice == 0.00 )
	  {
		  $getBookingStatus['type'] = 'error';
		  $getBookingStatus['message'] = __('The current space is not available to be booked on monthly basis.');
                  $error = 1;
	  }
	  else if ( $diffInterval->y == 0 && $diffInterval->m == 0 && $diffInterval->days >= 7 && 
                    $dailyPrice == 0.00 && $weeklyPrice == 0.00 )
	  {
		  $getBookingStatus['type'] = 'error';
		  $getBookingStatus['message'] = __('The current space is not available to be booked on weekly basis.');
                  $error = 1;
	  }
	  else if ( $diffInterval->y == 0 && $diffInterval->m == 0 && $diffInterval->days && $diffInterval->days < 7 && 
                    $dailyPrice == 0.00 )
	  {
		  $getBookingStatus['type'] = 'error';
		  $getBookingStatus['message'] = __('The current space is not available to be booked on daily basis.');
                  $error = 1;
	  }
	  else if ( $diffInterval->y ==0 && $diffInterval->m == 0 && $diffInterval->d == 0 && $diffInterval->h && 
                    $hourlyPrice == 0.00 )
	  {
		  $getBookingStatus['type'] = 'error';
		  $getBookingStatus['message'] = __('The current space is not available to be booked on hourly basis.');
                  $error = 1;
	  }


	  if (!$this->_checkEmptySlot($numSlots)) 
	  {
		  $getBookingStatus['type'] = 'error';
		  $getBookingStatus['message'] = 'The current space is not available for the selected date and time.';
                  $error = 1;
	  } 
	  else 
	  {
		  $getBookingStatus['space_park_id'] = $this->request->data['Space']['space_park_id'];
                  $spaceOpenBitMap = $this->__NewgetWeekDays($diffInterval->days,$startDateTimeObj,$spaceId,$unAvailStr);

		  if (!empty($unAvailStr)) 
		  {
		     $getBookingStatus['type'] = 'error';
		     $getBookingStatus['message'] = 'Space is not available on '. $unAvailStr;
                     $getBookingStatus['message'] .=' . Please select other dates.';

                     $error = 1;
		  }

	  }

          if ( !$error )
	  {

		  $bookingPrice = $this->_getBookingPrice($diffInterval,$openHours,$hourlyPrice,$dailyPrice,$weeklyPrice,
				                          $monthlyPrice,$yearlyPrice);

                  $numberOfInstallments = 0;

		  //TODO: what if the booking is for 1 month + few hours. Please test this 
		  if ( ($diffInterval->y) || ($diffInterval->m > 1) || ($diffInterval->m == 1 && $diffInterval->d ) )
		  {
			  $getBookingStatus['deposit'] = 1;
                          $bookingMsg = '';

			  if ( $diffInterval->y )
			  {
				  $monthlyInstallments  = ($bookingPrice/12);
				  $numberOfInstallments = 11;
                                  $bookingMsg = 'Total Booking Amount for the year';
                                  $monthlyPrice = ceil($monthlyInstallments);
                                  
			  }
			  else if ( $diffInterval->m > 0 )
			  {
                                  if ( $diffInterval->m == 1 )
                                  {
                                     $numberOfInstallments = 1;
                                     $monthlyInstallments  = $monthlyPrice; 
                                  }
                                  else
                                  {
				     $monthlyInstallments = ($bookingPrice/$diffInterval->m);
				     $numberOfInstallments = $diffInterval->m - 1;
                                  }
                                  $bookingMsg = 'Total Booking Amount for ' . $diffInterval->m . ' month(s)';
                                  if ( $diffInterval->d )
                                  {
                                     $bookingMsg .=' and ' . $diffInterval->d . ' day(s)';
                                  }
			  }
                           
			  $installmentMsg = '<div align=center>';
                          $installmentMsg .= 'The amount will be collected in monthly installments<br />';

                          if($bulkBookingSettings['BulkBookingSetting']['deposit'] > 0.00)
                          {
                             $installmentMsg .='A refundable deposit of ' . $bulkBookingSettings['BulkBookingSetting']['deposit']. 'will also be collected with this booking<br />';
                          }
      

			  $installmentMsg .= '</div>';
                          $getBookingStatus['installmentMsg'] = $installmentMsg;

		  }
                  $total =  number_format((float)($bookingPrice * $numSlots),2,'.',',');
		  $getBookingStatus['type'] = 'success';
		  $getBookingStatus['bookingPrice'] = $bookingPrice;
		  $getBookingStatus['total'] = $total;

                  //CakeLog::write('debug','CommissionPercentage :: ' . is_numeric($bulkBookingSettings['BulkBookingSetting']['commission_percentage']));

                  
                  if($bulkBookingSettings['BulkBookingSetting']['commission_paid_by'] == 1 &&
                      $bulkBookingSettings['BulkBookingSetting']['commission_percentage'] > 0.00)
                  {
                     $getBookingStatus['spCommission'] = 1;
                  }else {
                     $getBookingStatus['spCommission'] = 0;
                  } 
                  
                  $getBookingStatus['numInstallments'] = $numberOfInstallments;

          }


	  $this->set(
			  array(
				  'response' => $getBookingStatus,
				  '_serialize' => 'response'
			       )
		    );
  }



/**
 * Method _checkEmptySlot to check that is there any slot available to park at the requested date and time
 *
 * @return bool
 */
protected function _checkEmptySlot($numSlotsToBook=1) 
{

	$spaceID = $this->request->data['Space']['space_id'];
	$startDate = date('Y-m-d H:i:s', strtotime($this->request->data['Space']['start_date']));
	$endDate = date('Y-m-d H:i:s', strtotime($this->request->data['Space']['end_date']));

	$totalSlots = $this->Space->getAllSlots($spaceID);
	$getBookedSlots = $this->Booking->getBookedRecordsOfSpace($spaceID,$startDate,$endDate);
	//CakeLog::write('debug','In checkEmptySlot ...' . print_r($getBookedSlots,true));
	//CakeLog::write('debug','In checkEmptySlot TOTAL SLOTS' . print_r($totalSlots,true));
	//CakeLog::write('debug','In checkEmptySlot numSlotsTOBook ' . print_r($numSlotsToBook,true));


	if (empty($getBookedSlots)) {
		$this->loadModel('SpacePark');
		$slotID = $this->SpacePark->getSlot($spaceID);
                //CakeLog::write('debug','Slot ID ' . print_r($slotID,true));
		if(!empty($slotID)) {
			$this->request->data['Space']['space_park_id'] = $slotID['SpacePark']['id'];
			return true;	
		} 
		return false;	
	} else {
                 $getBookedSlots = array_map("unserialize", array_unique(array_map("serialize", $getBookedSlots)));
		 //CakeLog::write('debug','after applying array_map' . print_r($getBookedSlots,true));
                
                $nextAvailableSlotId = 0;
                $numBookedSlots = 0;

                foreach($getBookedSlots as $slots)
                {
                   $numBookedSlots += $slots['Booking']['num_slots'];
                }
                $nextAvailableSlotId = $getBookedSlots[0]['Booking']['space_park_id'] + $numBookedSlots;

                //CakeLog::write('debug','Next Available SLOT ID ' . $nextAvailableSlotId); 

                if($numBookedSlots + $numSlotsToBook <= $totalSlots['Space']['number_slots'])
                {
                   $this->request->data['Space']['space_park_id'] = $nextAvailableSlotId;
		   return true;
		}
		return false;
	}
		
}



/**
 * Method __getWeekDays to get week days names from two dates and validate on which days, the space is available
 *
 * @param $numOfDays int the total number of days from start and end dates
 * @param $startDate datetime the start date
 * @param $spaceID int id of the space
 * @return $unavailableDay array containing unavailable days to show as an error message
 */
 private function __NewgetWeekDays($numOfDays = null, $startDateTimeObj = null, $spaceID = null,&$unAvailDaysStr)  
 {
	 $this->loadModel('SpaceTimeDay');
	 $spaceOpenDays = $this->SpaceTimeDay->getSpaceDays($spaceID);
	 $openDaysBitmap = 0;

	 if ( $spaceOpenDays['SpaceTimeDay']['mon'] )
		 $openDaysBitmap |= (1 << 0);
	 if ( $spaceOpenDays['SpaceTimeDay']['tue'] )
		 $openDaysBitmap |= (1 << 1);
	 if ( $spaceOpenDays['SpaceTimeDay']['wed'] )
		 $openDaysBitmap |= (1 << 2);
	 if ( $spaceOpenDays['SpaceTimeDay']['thu'] )
		 $openDaysBitmap |= (1 << 3);
	 if ( $spaceOpenDays['SpaceTimeDay']['fri'] )
		 $openDaysBitmap |= (1 << 4);
	 if ( $spaceOpenDays['SpaceTimeDay']['sat'] )
		 $openDaysBitmap |= (1 << 5);
	 if ( $spaceOpenDays['SpaceTimeDay']['sun'] )
		 $openDaysBitmap |= (1 << 6);

	 $dayBitPosArray = array (
			 'Mon' => 0,
			 'Tue' => 1,
			 'Wed' => 2,
			 'Thu' => 3,
			 'Fri' => 4,
			 'Sat' => 5,
			 'Sun' => 6
			 );

	 $indexToDayNameArray = array (
			 0 => 'Monday',
			 1 => 'Tuesday',
			 2 => 'Wednesday',
			 3 => 'Thursday',
			 4 => 'Friday',
			 5 => 'Saturday',
			 6 => 'Sunday'
			 );


	 $startDay = $startDateTimeObj->format('D');

	 $spaceDayBitmap = 0; 
	 $index = $dayBitPosArray[$startDay];
	 $unAvailDaysStr = '';
	 if ( $numOfDays > 7 )
		 $numOfDays = 7;

	 for($itr = 0; $itr < $numOfDays; $itr++)
	 {
		 if ( !($openDaysBitmap & (1 << $index ) ) )
		 {
			 $unAvailDaysStr .= ' ';
			 $unAvailDaysStr .= $indexToDayNameArray[$index];  
		 } 

		 $index = ($index + 1)%7;
	 }

    return $openDaysBitmap;
 }

/**
 * Method _isSpaceAvailableOnThisDate to check if the space is available on this date 
 *
 * @param $startDateObj datetime the start date
 * @param $openBitMap bitmap which is returned by getWeekDays 
 * @return true or false 
 */
 private function _isSpaceAvailableOnThisDate($openDayBitmap,$dateTimeObj)  
 {
	 $startDay = $dateTimeObj->format('D');
	 $dayBitPosArray = array (
			 'Mon' => 0,
			 'Tue' => 1,
			 'Wed' => 2,
			 'Thu' => 3,
			 'Fri' => 4,
			 'Sat' => 5,
			 'Sun' => 6
			 );
         $index = $dayBitPosArray[$startDay];

         if ( $openDayBitmap & (1 << $index) )
            return true;
         
   return false;

 }
/**
 * Method __getWeekDays to get week days names from two dates and validate on which days, the space is available
 *
 * @param $numOfDays int the total number of days from start and end dates
 * @param $startDate datetime the start date
 * @param $spaceID int id of the space
 * @return $unavailableDay array containing unavailable days to show as an error message
 */
	private function __getWeekDays($numOfDays = null, $startDate = null, $spaceID = null) {
		$datearray = array(date('D', $startDate));
                /*Aditya - changes for bug 1197. Run the loop till numofDays - 1 as 
                 * the start date is already added to date array */
   		for ($i = 1;$i <= $numOfDays - 1; $i++) {
     		$startDate = $startDate+86400;
     		$datearray = array_merge($datearray,array(date('D', $startDate)));
   		}
   		$datearray = array_map('strtolower', $datearray);
                
                
   		$this->loadModel('SpaceTimeDay');
   		$getSpaceDays = $this->SpaceTimeDay->getSpaceDays($spaceID);

   		$unavailableDays = array();
   		foreach ($getSpaceDays['SpaceTimeDay'] as $key => $value) {
   			if (!$value) {
   				if (in_array($key, $datearray)) {
   					$unavailableDays[] = Configure::read('weeklongdays.'.$key);
   				}
   			}
   		}
		return $unavailableDays;
	}

/**
 * Method ajaxValidateDates to get validate start date and date while listing space
 *  if a user gives an yearly rate but then selects the Until date as less than a year, it should throw up an error + More such validations
 *
 * @return void
 */
	public function ajaxValidateDates() {
		
		$spaceID = $this->request->data['space_id'];
		$startDate = $this->request->data['start_date'];
		$endDate = $this->request->data['end_date'];

		$interval = $this->_getNumberOfDays($startDate, $endDate);


		$this->set(
					array(
							'response' => $interval,
							'_serialize' => 'response'
						)
				);

		/*$this->loadModel('SpacePricing');
		$spacePricings = $this->SpacePricing->getSpacePrices($this->request->data['space_id']);
		//pr($spacePricings);die;
		$status = array(
						'Type' => Configure::read('StatusType.Success')
					);

		foreach ($spacePricings as $priceSlots) {
			if ($interval->y > Configure::read('Bollean.False')) {
				break;
			} elseif (!empty($priceSlots['yearly']) && $priceSlots['yearly'] != Configure::read('SpaceUnavailabelPrice') && $interval->y == Configure::read('Bollean.False')) {
				$status = array(
						'Type' => Configure::read('StatusType.Error'),
						'Message' => 'Please select atleast one year as you entered the yearly price for your space.'
					);
				break;
			} elseif (!empty($priceSlots['monthly']) && $priceSlots['monthly'] != Configure::read('SpaceUnavailabelPrice') && $interval->m == Configure::read('Bollean.False')) {
				$status = array(
							'Type' => Configure::read('StatusType.Error'),
							'Message' => 'Please select atleast one month as you entered the monthly price for your space.'
						);
				break;
			} elseif (!empty($priceSlots['weekely']) && $priceSlots['weekely'] != Configure::read('SpaceUnavailabelPrice') && $interval->d < 7) {
				$status = array(
							'Type' => Configure::read('StatusType.Error'),
							'Message' => 'Please select atleast one week as you entered the weekly price for your space.'
						);
				break;
			} elseif (!empty($priceSlots['daily']) && $priceSlots['daily'] != Configure::read('SpaceUnavailabelPrice') && $interval->d == Configure::read('Bollean.False')) {
				$status = array(
							'Type' => Configure::read('StatusType.Error'),
							'Message' => 'Please select atleast two days as you entered the daily price for your space.'
						);
				break;
			}
		}

		$this->set(
					array(
							'response' => $status,
							'_serialize' => 'response'
						)
				);*/
	}
	
/**
 * Method updateSlotOccupancy : This function will log an OFFLINE car entry or exit . 
 *                          This function would be called by the android widget
 *      @jsonData space_id
 *      @jsonData action
 * @return void
 */
public function updateSlotOccupancy($spaceId,$action,$id)
{ 
   $currentSlots = 0;
   $data = array();
   $sendNotifications = 1;

   //CakeLog::write('debug','In updateSlotOccupancy....spaceID ' . $spaceId . ',action ' . $action . ',id = ' . $id); 

   if($action == 1)
   { 
      CakeLog::write('debug','Loggin Offline Entry');
      $this->loadModel('CurrentSlotOccupancy');
      $currentSlots = $this->CurrentSlotOccupancy->incrementSlotInUse($spaceId);
   }else if($action == 2){
      CakeLog::write('debug','Loggin Offline Exit');
      $this->loadModel('CurrentSlotOccupancy');
      $currentSlots = $this->CurrentSlotOccupancy->decrementSlotInUse($spaceId);

      if($currentSlots == - 1) //happens in case of error
      {
         $currentSlots = 0;
      }
   }else{
	$this->loadModel('CurrentSlotOccupancy');
        $numParkedCars  = 0;
	$numParkedBikes = 0;
	$currentSlots = $this->CurrentSlotOccupancy->getSlotsInUse($spaceId,$numParkedCars,$numParkedBikes);
	$sendNotifications = 0;
   } 

   $data['spaceId'] = $spaceId;
   $data['numOccupied'] = $currentSlots;
   
   //CakeLog::write('debug','updateSlotOccupancy...' . print_r($data,true));
   //$jsonData = json_encode($data);
   $this->set('data',$data);
   $this->set('_serialize','data');

   if($sendNotifications){
	   //notify other widgets 
	   $this->loadModel('WidgetToken');
	   $tokenList = $this->WidgetToken->getWidgetTokens($spaceId,$id);
	   
	   if(!empty($tokenList)){
	     $this->notifyAllWidgets($spaceId,$currentSlots,$tokenList); 
	   }
   }
}


/**
 * Method notifyAllWidgets : This function will notify all widgets about the occupancy number
 * @return void
 */
public function notifyAllWidgets($spaceId,$currentSlots,$tokenList)
{
  $url = Configure::read('GCM.url');
  //$url = "http://localhost:3000";
  //CakeLog::write('debug','URL is ' . $url);
  $authKey = 'key='.Configure::read('GCM.serverKey');

  //CakeLog::write('debug','In notifyAllWidgets, tokenList = ' . print_r($tokenList,true));

  $requestHeaders = array(
	           'header'=> array('Content-Type' => 'application/json',
	           'Authorization' => $authKey)
                   );

  $socket = new HttpSocket();
  $requestParams = array('numOccupied' => $currentSlots,
                         'spaceId' => $spaceId);
  $body = array(
	  'registration_ids' => array_values($tokenList),
	  'priority' => 'high',
	  'data' => $requestParams,
            );

  $jsonData = json_encode($body);

  //CakeLog::write('debug','Printing JSON data ' . print_r($jsonData,true)); 
  $response = $socket->post($url,$jsonData,$requestHeaders);

  if($response->code == Configure::read('HTTPStatusCode.OK')){
      CakeLog::write('debug','Widgets update succeeded');
  }else{
      CakeLog::write('error','Widgets update failed with code ' . $response->code . 'and reason ' . $response->reasonPhrase);
      CakeLog::write('debug','Widgets update failed ' . print_r($response->raw,true));
  }
			                   
}

/**
 * Method registerWidget : This function will registerWidget when it is installed 
 *                         This will be useful later to send Notifications update to all the installed widgets 
 *      @jsonData google register token 
 * @return void
 */
public function registerWidget($spaceId)
{
	
   CakeLog::write('debug','Inside registerWidget with spaceID ' . $spaceId);

   
   $currentSlots = 0;
   $id = -1;
   //CakeLog::write('debug','Get Current Count');

   $response = array();

   //first check if the space exits
   if($this->Space->isSpaceConfigured($spaceId)){
	$numParkedCars  = 0;
        $numParkedBikes = 0;
        $this->loadModel('CurrentSlotOccupancy');
        $currentSlots = $this->CurrentSlotOccupancy->getSlotsInUse($spaceId,$numParkedCars,$numParkedBikes);
	
	//now register the widget
        $jsonData = $this->request->input('json_decode');
        //CakeLog::write('debug','After decoding jsonData' . print_r($jsonData,true));

        $token = $jsonData->{'token'};
        //CakeLog::write('debug','Token is ' . $token);
	
	$this->loadModel('WidgetToken');
	$id = $this->WidgetToken->registerWidget($spaceId,$token);
	//CakeLog::write('debug','ID for the registgerWidget...' . $id);
    }else{
	 $currentSlots = -1;
    }
   
    $response['spaceId'] = $spaceId;
    $response['numOccupied'] = $currentSlots;
    $response['id'] = $id;
    
    $this->set('data',$response);
    $this->set('_serialize','data');
    
}

/**
 * Method deRegisterWidget : This function will deRegisterWidget when it is un-installed 
 *      @jsonData google register token 
 * @return void
 */
public function deRegisterWidget()
{
	
   //CakeLog::write('debug','Inside deregisterWidget');

   $response = array();

	
    //now register the widget
    $jsonData = $this->request->input('json_decode');
    //CakeLog::write('debug','After decoding jsonData' . print_r($jsonData,true));

    $token = $jsonData->{'token'};
    //CakeLog::write('debug','Token is ' . $token);
	
    $this->loadModel('WidgetToken');
    $this->WidgetToken->deRegisterWidget($token);
   
    $response['tokenDeleted'] = 1;
    
    $this->set('data',$response);
    $this->set('_serialize','data');
}

/**
 * Method refreshToken : This function will refresh a given token 
 *      @jsonData google register token 
 * @return void
 */
public function refreshToken()
{
	
   CakeLog::write('debug','Inside refreshToken');

   $response = array();

	
    $jsonData = $this->request->input('json_decode');
    CakeLog::write('debug','After decoding jsonData' . print_r($jsonData,true));

    $oldToken = $jsonData->{'oldtoken'};
    $newToken = $jsonData->{'newtoken'};

    CakeLog::write('debug','Old Token is ' . $oldToken);
    CakeLog::write('debug','New Token is ' . $newToken);
	
    $this->loadModel('WidgetToken');
    $this->WidgetToken->refreshToken($oldToken,$newToken);
   
    $response['tokenRefreshed'] = 1;
    
    $this->set('data',$response);
    $this->set('_serialize','data');
}

/******************************************************************************
 *
 *     Operator App Specific functions
 *
 * ****************************************************************************/

public function operatorSpaceDetails($spaceOwnerId, $spaceId =0, $operatorId = -1,$isRequestRenewal=0)
{
	//find the first space that is listed by this operator
	CakeLog::write('debug','User Id ' . $spaceOwnerId);     
	$this->loadModel('Space');	
	$activeSpace = $this->Space->opAppGetSpaceDetails($spaceOwnerId, $spaceId);
	$response = array();
	$response['status'] = 1;
	$response['reason'] = "Space Found";
	
	CakeLog::write('debug','Active Space for this user ' . print_r($activeSpace,true));


	if(!empty($activeSpace))
	{
		//generate an operator ID for this operator

		$this->loadModel('Operator');
		if($isRequestRenewal == 0){
			$operatorId = $this->Operator->getNextOperatorId($activeSpace['Space']['id']);
		}

		if($operatorId < 0 && !$isRequestRenewal){
			$response['status'] = 0;
			$response['reason'] = "Maximum Operator Limit reached. Please contact SimplyPark.in";
		}else{

			//Hack to fix a bug in the app.
			if($isRequestRenewal && $spaceId == 277 && $operatorId == 0){
				$operatorId = 3;
			}
			//CakeLog::write('debug','Active Space for this user ' . print_r($activeSpace,true));
			$response['operatorId'] = $operatorId;

			$response['name']             = $activeSpace['Space']['name'];
			$response['spaceId']         = $activeSpace['Space']['id'];
			$response['slots']            = $activeSpace['Space']['number_slots'];
			$response['spaceDefaultVehicleType']= $activeSpace['Space']['default_parking'];
			$response['dailyPrice']       = isset($activeSpace['SpacePricing'][0]['daily'])?$activeSpace['SpacePricing'][0]['daily']:0.00;
			$response['weeklyPrice']      = isset($activeSpace['SpacePricing'][0]['weekely'])?$activeSpace['SpacePricing'][0]['weekely']:0.00;
			$response['monthlyPrice']     = isset($activeSpace['SpacePricing'][0]['monthly'])?$activeSpace['SpacePricing'][0]['monthly']:0.00;
			$response['yearlyPrice']      = isset($activeSpace['SpacePricing'][0]['yearly'])?$activeSpace['SpacePricing'][0]['yearly']:0.00;
			$response['bikeMonthlyPrice'] = isset($activeSpace['SpacePricing']['bike_monthly_price'])?$activeSpace['SpacePricing']['bike_monthly_price']:0.00;
			$response['is24HoursOpen']    = $activeSpace['SpaceTimeDay']['open_all_hours'];
			$response['bikePricingRatio'] = $activeSpace['Space']['bike_pricing_ratio'];
			$response['isMultiDayParkingPossible'] = $activeSpace['Space']['is_multi_day_parking_possible'];
			$response['isParkingPrepaid'] = $activeSpace['Space']['is_offline_parking_prepaid'];
			$response['numPrepaidHours']  = $activeSpace['Space']['num_hours_prepaid'];
			$response['nightCharge'] = $activeSpace['Space']['night_charge'];
			$response['isPassActivationRequired'] = $activeSpace['Space']['is_pass_activation_required'];
			$response['isCancelUpdateAllowed'] = $activeSpace['Space']['is_cancel_update_allowed'];
			$response['isSlotOccupiedAssumed'] = $activeSpace['Space']['is_slot_occupancy_assumed']; 
			$response['lostTicketCharge'] = $activeSpace['Space']['lost_ticket_charge']; 
			$response['spaceOwnerName']   = $activeSpace['User']['UserProfile']['first_name'] . ' ' . $activeSpace['User']['UserProfile']['last_name']; 
			$response['spaceOwnerMobile'] = $activeSpace['User']['UserProfile']['mobile']; 
			$response['reusableTokensUsed'] = isset($activeSpace['Space']['reusable_tokens_used'])?$activeSpace['Space']['reusable_tokens_used']:0;

			if(isset($activeSpace['CurrentSlotOccupancy']['num_slots_in_use'])){
				$response['currentOccupancy'] = $activeSpace['CurrentSlotOccupancy']['num_slots_in_use'];
				$response['numParkedCars']    = $activeSpace['CurrentSlotOccupancy']['num_parked_cars'];
				$response['numParkedBikes']   = $activeSpace['CurrentSlotOccupancy']['num_parked_bikes'];
				$response['numBikesInSingleSlot']  = $activeSpace['CurrentSlotOccupancy']['num_bikes_in_single_slot'];
			}else{
				$response['currentOccupancy'] = 0;
				$response['numParkedCars']    = 0; 
				$response['numParkedBikes']   = 0;
				$response['numBikesInSingleSlot']  = 0;
			}


			if(isset($activeSpace['Space']['apply_custom_ticket_format'])){
				$this->loadModel('TicketFormat');
				$customFormats = $this->TicketFormat->getCustomTicketFormats($response['spaceId']);
				CakeLog::write('debug','Inside Custom Formats...' . print_r($customFormats,true));
				//$customFormats = preg_replace("/[\r\n]/","/[\n]/",$customFormats);
				//CakeLog::write('debug','Inside Custom Formats...' . print_r($customFormats,true));
				if(!empty($customFormats)){
					$response['applyCustomTicketFormat'] = true;
					CakeLog::write('debug','Inside Custom Formats...' . print_r($customFormats[0]['TicketFormat'],true));
					foreach ($customFormats[0]['TicketFormat'] as &$arr) {
						//$arr = preg_replace("/[\r\n]/","/[\n]/",$arr);
						$arr = str_replace("\r\n","\n",$arr);
					}
					$response['ticketFormat'] = $customFormats;
				}else{
					$response['applyCustomTicketFormat'] = false;
				}
			}

			$response['tierPricingSupport'] = $activeSpace['Space']['tier_pricing_support'];
			$response['isParkAndRideAvailable'] = $activeSpace['Space']['is_park_and_ride_available'];
			$response['numBikesInSingleSlot']   = $activeSpace['Space']['num_bikes_in_single_slot'];
			$response['openingDateTime'] = $activeSpace['SpaceTimeDay']['from_date'];
			$response['closingDateTime'] = $activeSpace['SpaceTimeDay']['to_date'];
			$response['isSpaceBikeOnly'] = $activeSpace['Space']['is_space_bike_only'];
			$response['isGateWiseReportsRequired'] = $activeSpace['Space']['is_gatewise_reports_required'];

			if($response['spaceDefaultVehicleType']==""){
				if($response['isSpaceBikeOnly']){
					$response['spaceDefaultVehicleType']="Bike";
				}else{
					$response['spaceDefaultVehicleType']="Car";
				}
			}

			if($response['spaceDefaultVehicleType']==""){
				if($response['isSpaceBikeOnly']){
					$response['spaceDefaultVehicleType']="Bike";
				}else{
					$response['spaceDefaultVehicleType']="Car";
				}
			}


			if($response['tierPricingSupport']){
				$this->loadModel('SpaceTierPricing');
				$tierPricing = $this->SpaceTierPricing->find('list',
					array(
						'conditions' => array('SpaceTierPricing.space_id' => $response['spaceId']),
						'fields' => array('price','hours','vehicle_type')
					)
				);
				CakeLog::write('debug','TierPricingSupport...' . print_r($tierPricing,true));
				if($activeSpace['Space']['is_space_bike_only']){
					$response['tierPricing'] = $tierPricing[2];
				}else{ 
					$response['tierPricing'] = $tierPricing[1];
				} 

				//issue95 changes
				foreach($activeSpace['SpacePricing'] as &$pricing){
					CakeLog::write('debug','Pricing ...' . print_r($pricing,true));
					if(array_key_exists($pricing['vehicle_type'],$tierPricing))
					{
						$pricing['tierPricingSupport'] = true;
						$pricing['tierPricing'] = $tierPricing[$pricing['vehicle_type']];
					}else{
						$pricing['tierPricingSupport'] = false;
					}
				}
			}else{
				$response['hourlyPrice']       = isset($activeSpace['SpacePricing'][0]['hourly'])?$activeSpace['SpacePricing'][0]['hourly']:0.00;
			} 
			if(isset($activeSpace['ExtraNightCharges'])){

				//setup extra night charges if any
				foreach($activeSpace['SpacePricing'] as &$pricing){
					$found = false;
					foreach($activeSpace['ExtraNightCharges'] as $nightCharge){
						if($nightCharge['vehicle_type'] == $pricing['vehicle_type']){
							$pricing['isNightChargeAvailable']        = true;
							$pricing['nightCharge']['startHour']     = $nightCharge['start_hour'];
							$pricing['nightCharge']['endHour']       = $nightCharge['end_hour'];
							$pricing['nightCharge']['dailyCharge']   = $nightCharge['daily_charge'];
							$pricing['nightCharge']['monthlyCharge'] = $nightCharge['monthly_charge'];
							$found = true;
							break;
						}
					}
					if($found == false){
						$pricing['isNightChargeAvailable']        = false;
					}
				} 
			}

			if(isset($activeSpace['PenaltyCharges'])){

				//setup extra night charges if any
				foreach($activeSpace['SpacePricing'] as &$pricing){
					$found = false;
					foreach($activeSpace['PenaltyCharges'] as $penaltyCharge){
						if($penaltyCharge['vehicle_type'] == $pricing['vehicle_type']){
							$pricing['isPenaltyChargeAvailable']        = true;
							$pricing['penaltyCharge']['chargeCondition']   = $penaltyCharge['charge_condition'];
							$pricing['penaltyCharge']['hourlyCharge']      = $penaltyCharge['hourly_charge'];
							$found = true;
							break;
						}
					}
					if($found == false){
						$pricing['isPenaltyChargeAvailable']        = false;
						$pricing['chargePenaltyOnMonthlyPasses']    = false;      
					}
				} 
			}

			$response['pricingPerVehicleType'] = $activeSpace['SpacePricing'];
			if($response['reusableTokensUsed']){
				$this->loadModel('ReusableToken');
				$response['tokens'] = $this->ReusableToken->getReusablePrintTokens($response['spaceId']);
			}

		}


	}
	else{
		$response['status'] = 0;
		$response['reason'] = "No Parking space associated with this account. Please contact SimplyPark.in";
	}	
	
	$this->set('data',$response);
        $this->set('_serialize','data');

}	


/**
 * Method registerApp : This function will registerApp when it is installed 
 *                         This will be useful later to send Notifications update to all the installed apps/widgetrs 
 *      @jsonData google register token 
 * @return void
 */
public function registerApp($spaceId)
{
	
   CakeLog::write('debug','Inside registerApp with spaceID ' . $spaceId);

   
   $currentSlots = 0;
   $numParkedCars = 0;
   $numParkedBikes = 0;
   $id = -1;
   CakeLog::write('debug','Get Current Count');

   $response = array();

   //first check if the space exits
   if($this->Space->isSpaceConfigured($spaceId)){
        $this->loadModel('CurrentSlotOccupancy');
	$currentSlots = $this->CurrentSlotOccupancy->getSlotsInUse($spaceId,$numParkedCars,$numParkedBikes);

	$this->loadModel('SyncUpdate');
	$syncId = $this->SyncUpdate->getCurrentSyncId($spaceId);
	
	//now register the app 
        $jsonData = $this->request->input('json_decode');
        //CakeLog::write('debug','After decoding jsonData' . print_r($jsonData,true));

	$token = $jsonData->{'token'};
	$operatorId = $jsonData->{'operator_id'};
	$operatorDesc = $jsonData->{'operator_desc'};
	

        CakeLog::write('debug','Token is ' . $token);
        CakeLog::write('debug','Operator Desc is ' . $operatorDesc);
	
	$this->loadModel('WidgetToken');
	$id = $this->WidgetToken->registerWidget($spaceId,$token,1,$operatorId);
	CakeLog::write('debug','ID for the registgerWidget...' . $id);

	$this->loadModel('Operator');
	$this->Operator->updateOperatorDesc($operatorId,$spaceId,$operatorDesc);
    }else{
	 $currentSlots = -1;
    }
   
    $response['spaceId'] = $spaceId;

    $response['numOccupied']    = $currentSlots;
    $response['numParkedCars']  = $numParkedCars;
    $response['numParkedBikes'] = $numParkedBikes;
    $response['syncId'] = $syncId;

    $response['id'] = $id;

    //CakeLog::write('debug','IN registerApp..., printing response' . print_r($response,true));
    
    $this->set('data',$response);
    $this->set('_serialize','data');
    
}


/******************************************************************************
 *  Get Current Space Occupancy
 *
 * ****************************************************************************/

public function getCurrentOccupancy($spaceId)
{
   $numParkedCars  = 0;
   $numParkedBikes = 0;   
   $this->loadModel('CurrentSlotOccupancy');
   $currentSlots = $this->CurrentSlotOccupancy->getSlotsInUse($spaceId,$numParkedCars,$numParkedBikes);

   $data['spaceId']        = $spaceId;
   $data['numOccupied']    = $currentSlots;  
   $data['numParkedCars']  = $numParkedCars;
   $data['numParkedBikes'] = $numParkedBikes;  
   
   $this->set('data',$data);
   $this->set('_serialize','data');

   CakeLog::write('debug','IN getCurrentOccupancy, occupany for Space Id:: ' . $spaceId . " is " . $currentSlots);
} 

/******************************************************************************
 *  Log Entry 
 *
 * ****************************************************************************/

public function logOfflineEntry($spaceId)
{
	$response = array();
	$response['status'] = 1;
	$response['reason']  = "Entry Recorded";
	
	$currentOccupancy = array();

	$jsonData = $this->request->input('json_decode');

	$operatorId     = -1;
        if(isset($jsonData->{'operatorId'})){
	   $operatorId     = $jsonData->{'operatorId'};
        }else{
	   $operatorId     = $jsonData->{'operator_id'};
        }
	$carReg         = $jsonData->{'carReg'};
	$entryTime      = $jsonData->{'entryTime'};
	$entryTimeStamp = $jsonData->{'entryTimeStamp'};
	$parkingId      = $jsonData->{'parkingId'};
	$isParkAndRide  = $jsonData->{'isParkAndRide'};
	$gcmIdAtServer  = $jsonData->{'gcmIdAtServer'};
	$vehicleType     = $jsonData->{'vehicleType'};

	$reusableToken = "";

	if(isset($jsonData->{'reusableToken'})){
	   $reusableToken     = $jsonData->{'reusableToken'};
	}


	CakeLog::write('debug','Inside LogOfflineEntry: OperatorId ' . $operatorId . ',carReg = ' . $carReg . ',spaceId = ' . $spaceId);


	//$entryDateTime = new DateTime('@' . $entryTime);

	//$entryDateTimeStr = $entryDateTime->format("Y-m-d H:i:s");
	CakeLog::write('debug','Inside LogOfflineEntry: currentOccupancy ' . print_r($currentOccupancy,true));
	CakeLog::write('debug','Inside LogOfflineEntry: entryTime ' . $entryTime . ',parkingId = ' . $parkingId);
	
	//issue113: fix
	$this->loadModel('SyncUpdate');
        $syncTableDS = $this->SyncUpdate->getdatasource();
	$syncTableDS->begin();

	$incrementSlotInUse = 0;
	$this->loadModel('OfflineEntryExitTime');
	$offlineEntryExitid = $this->OfflineEntryExitTime->logEntry($spaceId,
		                                                    $operatorId,
							            $carReg,
							            $parkingId,
							            $entryTime,
							            $isParkAndRide,
								    $vehicleType,
								    $reusableToken,
								    $incrementSlotInUse);

	if($offlineEntryExitid != -1){

		if($incrementSlotInUse){
			$this->loadModel('CurrentSlotOccupancy');
			$currentOccupancy = $this->CurrentSlotOccupancy->incrementSlotInUse($spaceId,$vehicleType);
		}else{
			$occupancy = 0;
			$numParkedCars = 0;
			$numParkedBikes = 0;
			$this->loadModel('CurrentSlotOccupancy');
			$numSlots = $this->CurrentSlotOccupancy->getSlotsInUse($spaceId, $numParkedCars, $numParkedBikes);
			$currentOccupancy['slotOccupancy']  = $numSlots;
			$currentOccupancy['numParkedCars']  = $numParkedCars;
			$currentOccupancy['numParkedBikes'] = $numParkedBikes;
		}

		//update the sync table
		$this->loadModel('SyncUpdate');
		$syncId = $this->SyncUpdate->generateSyncId($spaceId,$offlineEntryExitid);

		if($syncId != -1){
                        //issue113
		        $syncTableDS->commit();

			CakeLog::write('debug','Inside LogOfflineEntry:: ' . print_r($currentOccupancy,true));	

			$response['spaceId']        = $spaceId;
			$response['numOccupied']    = $currentOccupancy['slotOccupancy'];
			$response['numParkedCars']  = $currentOccupancy['numParkedCars']; 	
			$response['numParkedBikes'] = $currentOccupancy['numParkedBikes'];
			$response['syncId']         = $syncId;	

			CakeLog::write('debug','INside logEntry:::' . print_r($response,true));	
			$this->set('data',$response);
			$this->set('_serialize','data');

			//notify other app 
			$this->loadModel('WidgetToken');
			$tokenList = $this->WidgetToken->getAppTokens($spaceId,$gcmIdAtServer);

			if(!empty($tokenList)){
				$actionSpecificParams['action']        = Configure::read('Action.entry');
				$actionSpecificParams['isParkAndRide'] = $isParkAndRide; 
				$actionSpecificParams['reg']           = $carReg; 
				$actionSpecificParams['parkingId']     = $parkingId;
				$actionSpecificParams['vehicleType']   = $vehicleType;
				$actionSpecificParams['numParkedCars']  = $response['numParkedCars'];
				$actionSpecificParams['numParkedBikes'] = $response['numParkedBikes'];
				$actionSpecificParams['numOccupied']    = $response['numOccupied'];
				$actionSpecificParams['spaceId']        = $spaceId;
				$actionSpecificParams['syncId']         = $response['syncId'];
				$actionSpecificParams['reusableToken']  = $reusableToken;

				$this->loadModel('Operator');
				$operatorDesc = $this->Operator->getOperatorDesc($operatorId,$spaceId);
				$actionSpecificParams['operatorDesc']   = $operatorDesc;
				$actionSpecificParams['operatorId'] = $operatorId;


				CakeLog::write('debug','Inside entryTimeStamp...' . $entryTimeStamp);

				$actionSpecificParams['entryTime']   = $entryTimeStamp;  

				$this->notifyAllApps($spaceId,$tokenList,$actionSpecificParams);
			}
		}else{
                    //issue113
		    $syncTableDS->rollback();
	            $response['status'] = 0;
	            $response['reason'] = "Sync ID Generation failed";
		    
		    CakeLog::write('debug','INside logEntry:::' . print_r($response,true));	
		    $this->set('data',$response);
		    $this->set('_serialize','data');
		}
	}else{
		$syncTableDS->rollback();
		$response['status'] = 0;
		$response['status'] = 0;
		$response['reason']  = "Database addition failed";

		CakeLog::write('debug','INside logEntry:::' . print_r($response,true));	
		$this->set('data',$response);
		$this->set('_serialize','data');
	}
}


/******************************************************************************
 *  Log Exit 
 *
 * ****************************************************************************/

public function logOfflineExit($spaceId,$parkingId)
{
	$response = array();
	$response['status'] = 1;
	$response['reason']  = "Exit Recorded";

	$currentOccupancy = array();
	$currentOccupancy['slotOccupancy']  = 0;
	$currentOccupancy['numParkedCars']  = 0;
	$currentOccupancy['numParkedBikes'] = 0;

	$jsonData = $this->request->input('json_decode');

	$operatorId     = -1;
        if(isset($jsonData->{'operatorId'})){
	   $operatorId     = $jsonData->{'operatorId'};
        }else{
	   $operatorId     = $jsonData->{'operator_id'};
        }
	

	$exitTime      = $jsonData->{'exitTime'};
	$exitTimeStamp = $jsonData->{'exitTimeStamp'};
	$gcmIdAtServer = $jsonData->{'gcmIdAtServer'};
	$parkingFees   = $jsonData->{'parkingFees'};
	$vehicleType   = $jsonData->{'vehicleType'};
	$vehicleReg = "";

	if(array_key_exists('vehicleReg',$jsonData))
	{
	   $vehicleReg = $jsonData->{'vehicleReg'};	
	}
       $entryTime = "0000-00-00 00:00:00";
       $entryTimeStamp = 0;
	
       if(array_key_exists('entryTime',$jsonData))
	{
	   $entryTime = $jsonData->{'entryTime'};	
	}
       
       if(array_key_exists('entryTimeStamp',$jsonData))
	{
	   $entryTimeStamp = $jsonData->{'entryTimeStamp'};	
	}
       
        $reusableToken = "";

	if(isset($jsonData->{'reusableToken'})){
	   $reusableToken     = $jsonData->{'reusableToken'};
	}

	CakeLog::write('debug','Inside LogOfflineExit: spaceId ' . $spaceId . ', parkingId = ' . $parkingId);

	//issue113: fix
	$this->loadModel('SyncUpdate');
        $syncTableDS = $this->SyncUpdate->getdatasource();
	$syncTableDS->begin();
	
	$this->loadModel('OfflineEntryExitTime');
	$decrementSlotInUse = 1;
	$offlineEntryExitid = $this->OfflineEntryExitTime->logExit($spaceId,
		                                                   $operatorId,
								   $parkingId,
                                                                   $entryTime,
								   $exitTime,
								   $parkingFees,
								   $vehicleReg,
								   $vehicleType,
								   $reusableToken,
							           $decrementSlotInUse);

	if($offlineEntryExitid != -1)
	{
		$this->loadModel('CurrentSlotOccupancy');
		if($decrementSlotInUse){
			$currentOccupancy = $this->CurrentSlotOccupancy->decrementSlotInUse($spaceId,$vehicleType);
		}else{
			$occupancy = 0;
			$numParkedCars = 0;
			$numParkedBikes = 0;
			$currentOccupancy['slotOccupancy'] = $this->CurrentSlotOccupancy->getSlotsInUse($spaceId,
				                                                                        $numParkedCars,
				                                                                        $numParkedBikes);
	                $currentOccupancy['numParkedCars']  = $numParkedCars;
	                $currentOccupancy['numParkedBikes'] = $numParkedBikes;
		}

		//update the sync table
		$this->loadModel('SyncUpdate');
		$syncId = $this->SyncUpdate->generateSyncId($spaceId,$offlineEntryExitid);

		if($syncId != -1){
	                //issue113: fix
	                $syncTableDS->commit();

			$response['spaceId']        = $spaceId;
			$response['numOccupied']    = $currentOccupancy['slotOccupancy'];   
			$response['numParkedCars']  = $currentOccupancy['numParkedCars'];   
			$response['numParkedBikes'] = $currentOccupancy['numParkedBikes'];   
			$response['syncId']         = $syncId;

			$this->set('data',$response);
			$this->set('_serialize','data');

			//notify other app 
			$this->loadModel('WidgetToken');
			$tokenList = $this->WidgetToken->getAppTokens($spaceId,$gcmIdAtServer);

			if(!empty($tokenList)){
				$actionSpecificParams['action']        = Configure::read('Action.exit');
				$actionSpecificParams['parkingId']     = $parkingId;
				$actionSpecificParams['parkingFees']   = $parkingFees;
				$actionSpecificParams['entryTime']     = $entryTimeStamp;  
				$actionSpecificParams['exitTime']      = $exitTimeStamp;  
				$actionSpecificParams['vehicleType']   = $vehicleType;
				$actionSpecificParams['numParkedCars']  = $response['numParkedCars'];
				$actionSpecificParams['numParkedBikes'] = $response['numParkedBikes'];
				$actionSpecificParams['numOccupied']    = $response['numOccupied'];
				$actionSpecificParams['spaceId']        = $spaceId;
				$actionSpecificParams['syncId']         = $response['syncId'];
				$actionSpecificParams['reusableToken']  = $reusableToken;
				if($vehicleReg != "")
				{
					$actionSpecificParams['vehicleReg']   = $vehicleReg;
				}

				$this->loadModel('Operator');
				$operatorDesc = $this->Operator->getOperatorDesc($operatorId,$spaceId);
				$actionSpecificParams['operatorDesc'] = $operatorDesc;
				$actionSpecificParams['operatorId']   = $operatorId;

				$this->notifyAllApps($spaceId,$tokenList,$actionSpecificParams); 
			}
		}else{
	                //issue113: fix
	                $syncTableDS->rollback();
			$response['status']  = 0;
			$response['reason']  = 'SyncID creation failed';

			$this->set('data',$response);
			$this->set('_serialize','data');
		}
	}else{
		//issue113: fix
		$syncTableDS->rollback();
		$response['status']  = 0;
		$response['reason']  = 'DB entry failed';

		$this->set('data',$response);
		$this->set('_serialize','data');
	}
}

/******************************************************************************
 *  Cancel & Update transactions
 *
 * ****************************************************************************/
public function cancelTransaction($spaceId)
{
	$response = array();
	$jsonData = $this->request->input('json_decode');

	$operatorId = $jsonData->{'operator_id'};
	$parkingId  = $jsonData->{'parking_id'};
	$gcmIdAtServer = $jsonData->{'gcm_id'};

        $entryDateTimeForDb = "0000-00-00 00:00:00";
        $entryDateTimeForGcm = 0;
        $vehicleReg = "";
        $vehicleType = 0; 
 
        $optionalParameters = 0;
        
        if(isset($jsonData->{'entry_date_time'})){
           $entryDateTimeForDb = $jsonData->{'entry_date_time'};
        }
        if(isset($jsonData->{'entry_date_time_stamp'})){
           $entryDateTimeForGcm = $jsonData->{'entry_date_time_stamp'};
        }
        if(isset($jsonData->{'vehicle_reg'})){
           $vehicleReg = $jsonData->{'vehicle_reg'};
        }
        if(isset($jsonData->{'vehicle_type'})){
           $vehicleType = $jsonData->{'vehicle_type'};
        }
	
	$reusableToken = "";

	if(isset($jsonData->{'reusableToken'})){
	   $reusableToken     = $jsonData->{'reusableToken'};
	}
	
	//issue113: fix
	$this->loadModel('SyncUpdate');
        $syncTableDS = $this->SyncUpdate->getdatasource();
	$syncTableDS->begin();

	$this->loadModel('OfflineEntryExitTime');
        $vehicleTypeReadFromDb = 0;
	$id = $this->OfflineEntryExitTime->cancelTransaction($spaceId,$parkingId,$operatorId,
                                                             $vehicleReg,$vehicleType,$entryDateTimeForDb,$reusableToken,$vehicleTypeReadFromDb);

	if($id >= 0)
	{
	   $this->loadModel('CurrentSlotOccupancy');
	   $this->CurrentSlotOccupancy->decrementSlotInUse($spaceId,$vehicleTypeReadFromDb);
	 	
	   //update the sync table
	   $this->loadModel('SyncUpdate');
	   $syncId = $this->SyncUpdate->generateSyncId($spaceId,$id);
	   if($syncId != -1){
		   //issue113
		   $syncTableDS->commit();

		   $response['status'] = 1;
		   $response['reason'] = "Cancelled";
		   $response['syncId'] = $syncId;
		   $this->set('data',$response);
		   $this->set('_serialize','data');


		   //notify other app 
		   $this->loadModel('WidgetToken');
		   $tokenList = $this->WidgetToken->getAppTokens($spaceId,$gcmIdAtServer);

		   if(!empty($tokenList)){
			   $actionSpecificParams['action']         = Configure::read('Action.cancel');
			   $actionSpecificParams['parkingId']      = $parkingId;
			   $actionSpecificParams['vehicleReg']     = $vehicleReg;
			   $actionSpecificParams['vehicleType']    = $vehicleTypeReadFromDb;
			   $actionSpecificParams['entryDateTime']  = $entryDateTimeForGcm;
			   $actionSpecificParams['operatorId']     = $operatorId;
			   $this->loadModel('Operator');
			   $actionSpecificParams['operatorDesc']   = $this->Operator->getOperatorDesc($operatorId,$spaceId);
			   $actionSpecificParams['syncId']         = $syncId;
			   $actionSpecificParams['reusableToken']  = $reusableToken;

			   $this->notifyAllApps($spaceId,$tokenList,$actionSpecificParams); 
		   }
	   }else{
		   //issue113
		   $syncTableDS->rollback();
		   $response['status'] = 0;
		   $response['reason'] = "Sync ID generation failed";	  
		   $this->set('data',$response);
		   $this->set('_serialize','data');
	   }
	
	}else{
		//issue113
		$syncTableDS->rollback();
		$response['status'] = 0;
		$response['reason'] = "No Such Transaction";	  
		$this->set('data',$response);
		$this->set('_serialize','data');
	}

}

public function updateTransaction($spaceId)
{
	$response = array();
	$jsonData = $this->request->input('json_decode');

	$operatorId = $jsonData->{'operator_id'};
	$parkingId  = $jsonData->{'parking_id'};
	$parkingStatus = $jsonData->{'parking_status'};
	$parkingFees   = $jsonData->{'parking_fees'};
	$gcmIdAtServer = $jsonData->{'gcm_id'};
        
        $entryDateTimeForDb = "0000-00-00 00:00:00";
        $entryDateTimeForGcm = 0;
        $vehicleReg = "";
        $vehicleType = 0; 
        
        if(isset($jsonData->{'entry_date_time'})){
           $entryDateTimeForDb = $jsonData->{'entry_date_time'};
        }
        if(isset($jsonData->{'entry_date_time_stamp'})){
           $entryDateTimeForGcm = $jsonData->{'entry_date_time_stamp'};
        }
        if(isset($jsonData->{'vehicle_reg'})){
           $vehicleReg = $jsonData->{'vehicle_reg'};
        }
        if(isset($jsonData->{'vehicle_type'})){
           $vehicleType = $jsonData->{'vehicle_type'};
        }
	
	$reusableToken = "";

	if(isset($jsonData->{'reusableToken'})){
	   $reusableToken     = $jsonData->{'reusableToken'};
	}
	
	//issue113: fix
	$this->loadModel('SyncUpdate');
        $syncTableDS = $this->SyncUpdate->getdatasource();
	$syncTableDS->begin();

	$this->loadModel('OfflineEntryExitTime');
        $vehicleTypeReadFromDb = 0;

	$id = $this->OfflineEntryExitTime->updateTransaction($spaceId,
                                                             $parkingId,$parkingStatus,$parkingFees,
                                                             $operatorId,$vehicleReg,$vehicleType,$entryDateTimeForDb,$reusableToken,
                                                             $vehicleTypeReadFromDb);

	if($id >= 0)
	{
	   $this->loadModel('CurrentSlotOccupancy');
	   $this->CurrentSlotOccupancy->incrementSlotInUse($spaceId,$vehicleTypeReadFromDb);
	 	
	   //update the sync table
	   $this->loadModel('SyncUpdate');
	   $syncId = $this->SyncUpdate->generateSyncId($spaceId,$id);

	   if($syncId != -1){
		   $syncTableDS->commit();
		   $response['status'] = 1;
		   $response['reason'] = "Updated";
		   $response['syncId'] = $syncId;
		   $this->set('data',$response);
		   $this->set('_serialize','data');

		   //notify other app 
		   $this->loadModel('WidgetToken');
		   $tokenList = $this->WidgetToken->getAppTokens($spaceId,$gcmIdAtServer);

		   if(!empty($tokenList)){
			   $actionSpecificParams['action']        = Configure::read('Action.update');
			   $actionSpecificParams['parkingId']     = $parkingId;
			   $actionSpecificParams['status']        = $parkingStatus;
			   $actionSpecificParams['parkingFees']   = $parkingFees;
			   $actionSpecificParams['vehicleReg']    = $vehicleReg;
			   $actionSpecificParams['vehicleType']   = $vehicleTypeReadFromDb;
			   $actionSpecificParams['entryDateTime'] = $entryDateTimeForGcm;
			   $actionSpecificParams['operatorId']    = $operatorId;
			   $this->loadModel('Operator');
			   $actionSpecificParams['operatorDesc']  = $this->Operator->getOperatorDesc($operatorId,$spaceId);
			   $actionSpecificParams['syncId']        = $syncId;
			   $actionSpecificParams['reusableToken'] = $reusableToken;

			   $this->notifyAllApps($spaceId,$tokenList,$actionSpecificParams); 
		   }
	   }else{
		   $syncTableDS->rollback();
		   
		   $response['status'] = 0;
		   $response['reason'] = "SyncUpdate Failed";	  
		   
		   $this->set('data',$response);
		   $this->set('_serialize','data');
	   }
	
	}else{
		$syncTableDS->rollback();
		$response['status'] = 0;
		$response['reason'] = "No Such Transaction";	  
		$this->set('data',$response);
		$this->set('_serialize','data');
	}

}


/******************************************************************************
 *         Get Updates From Server
 *
 * ****************************************************************************/

public function sync($spaceId = 0 , $syncId = -1)
{
	$resposne = array();
	$isPull = 1;
	$gcmIdAtServer = -1;
	if($syncId != -1)
	{
	   //sync type pull from server
	   CakeLog::write('debug','In SYNC, pull from Server called');
	   $response = $this->pullFromServer($spaceId,$syncId);

	}else{
            		
	   //push from server
	   $isPull = 0;
	   CakeLog::write('debug','In SYNC, push to Server called');
	   $response = $this->pushToServer($spaceId,$syncId,$gcmIdAtServer);		
	}
	
	$this->set('data',$response);
	$this->set('_serialize','data');

	//notify other apps to sync with server
	if($isPull == 0){
	   $this->loadModel('WidgetToken');
	   $tokenList = $this->WidgetToken->getAppTokens($spaceId,$gcmIdAtServer);
	   if(!empty($tokenList)){
		   $actionSpecificParams['action']  = Configure::read('Action.sync');
		   $this->notifyAllApps($spaceId,$tokenList,$actionSpecificParams);
	   }
	}
}

public function pushToServer($spaceId,$syncId,&$gcmIdAtServer)
{
        $response['status'] = 1;
	$response['reason'] = "Updated";
	$offlineResultCode = "";
	$onlineResultCode = "";
	$opAppResultCode = "";
	$opAppBookingModificationResultCode = "";
	
	$jsonDataArray = $this->request->input('json_decode',true);
	//CakeLog::write('debug','INside pushToServer...'. print_r($jsonDataArray,true));

	$numUpdates = $jsonDataArray['numUpdates'];
	$operatorId = $jsonDataArray['operatorId'];
	$gcmIdAtServer = $jsonDataArray['gcmId'];
	
	CakeLog::write('debug','INside pushToServer...with numupdates' . $numUpdates);
	CakeLog::write('debug','INside pushToServer...with operartoId' . $operatorId);
	$bikesToSlotRatio = $this->Space->getBikesToSlotRatio($spaceId);
	CakeLog::write('debug','INside pushToServer...bikesToSlotRatio' . $bikesToSlotRatio);


	$updatesArray = $jsonDataArray['updates'];
	$syncId = 0;
	$numOccupied      = 0;
	$numCarsOccupied  = 0;
	$numBikesOccupied = 0;
	$isOfflineUpdate  = 0;
	$isOnlineUpdate   = 0;
	$isOpAppBookingUpdate = 0;


	for($itr=0; $itr<$numUpdates; $itr++)
	{
		//issue113: fix
	        $this->loadModel('SyncUpdate');
		$syncTableDS = $this->SyncUpdate->getdatasource();
		$syncTableDS->begin();

	        $isOfflineUpdate  = 0;
	        $isOnlineUpdate   = 0;
	        $isOpAppBookingUpdate = 0;
                $isBookingModificationUpdate = 0;
                $updateSlotsInUse = 0;
		
		$transaction = $updatesArray[$itr];
		//CakeLog::write('debug','Inside pushToServer...' . print_r($transaction,true));	
		$isStale = 0;

		$updateType = $transaction['msg_type'];
      
                if($updateType == Configure::read('PushToServerUpdateType.OfflineEntryExitUpdate'))
		{ 
	           $this->loadModel('OfflineEntryExitTime');
		   $lastInsertId = $this->OfflineEntryExitTime->addUpdateRecord($transaction,
		                                                                $spaceId,$operatorId,
										$isStale,$updateSlotsInUse);
		   $isOfflineUpdate = 1;
	        }else if($updateType == Configure::read('PushToServerUpdateType.OnlineEntryExitUpdate')){
	           $this->loadModel('EntryExitTime');
		   $lastInsertId = $this->EntryExitTime->addUpdateRecord($transaction,
		                                                         $spaceId,$operatorId,
                                                                         $isStale,$updateSlotsInUse);
                   $isOnlineUpdate = 1;
	        }else if($updateType == Configure::read('PushToServerUpdateType.opAppBookingModification')){
		   $modifiedBookingsController = new BookingsController();
                   $lastInsertId = $modifiedBookingsController->handleModifyBookingPushedRecord($transaction,$spaceId, 
                                                                                                $operatorId,$isStale);
                   $isBookingModificationUpdate = 1;
                  
		}else{
			//user specific params
			$name    = $transaction['name'];
			$email   = $transaction['email'];
			$mobile  = $transaction['mobile'];

			//booking specific params
			$bookingStatus        = $transaction['status'];
			$bookingStartDateTime = $transaction['start_date']; //this is date in the format YYYY-MM-DD HH:mm
			$bookingEndDateTime   = $transaction['end_date'];   //this is date in the format YYYY-MM-DD HH:mm
			$passIssueDate        = $transaction['created']; //this is date in the format YYYY-MM-DD
			$serialNumber         = $transaction['serial_num'];
                        if(isset($transaction['is_pass_for_staff'])){
                           $isPassForStaff = $transaction['is_pass_for_staff'];
                        }else{
                           $isPassForStaff = 0;
                       }
                        //in the newer app version, we send the booking ID & encodedPassCode
                        //explictly. In order to remain backward comp, we set the serial number as
                        //encodedPassCode & bookingId  
                        if(isset($transaction['encode_pass_code'])){
                           $encodedPassCode = $transaction['encode_pass_code'];
                        }else{
                           $encodedPassCode = $serialNumber;
                        }
                        
                        if(isset($transaction['booking_id'])){
                           $bookingId = $transaction['booking_id'];
                        }else{
                           $bookingId = $serialNumber;
                        }
			
                        $vehicleReg           = $transaction['vehicle_reg'];
			$vehicleType          = $transaction['vehicle_type'];
			$numBookedSlots       = $transaction['num_slots'];
			$parkingFees          = $transaction['booking_price'];
                        $isRenewal            = false;
			$parentBookingId      = "";
			$paymentMode          = Configure::read('PaymentMode.Cash');
			$invoiceId            = "";
			$invoicePaymentId     = "";

                        if(isset($transaction['is_renewal']))
                        {
                           $isRenewal = $transaction['is_renewal'];
                        }

                        if(isset($transaction['parent_booking_id']))
                        {
                           $parentBookingId = $transaction['parent_booking_id'];
                        }   
			
			if(isset($transaction['payment_mode']))
                        {
                           $paymentMode = $transaction['payment_mode'];
                        }   
			
			if(isset($transaction['invoice_id']))
                        {
                           $invoiceId = $transaction['invoice_id'];
                        }   
			
			if(isset($transaction['payment_id']))
                        {
                           $invoicePaymentId = $transaction['payment_id'];
                        }   

			$isOpAppBookingUpdate = 1;

			$bookingController = new BookingsController();

			$lastInsertId = -1;
                        $spaceOwnerId = 0;
			$bookingController->_createBookingCommonSteps($spaceId,$name,$email,$mobile,$bookingStatus,
				                                      $bookingStartDateTime,$bookingEndDateTime,$passIssueDate,
								      $bookingId,$encodedPassCode,$serialNumber,
                                                                      $vehicleReg,$vehicleType,$numBookedSlots,
								      $parkingFees,0,$isRenewal,$parentBookingId,
								      $paymentMode,$invoiceId,$invoicePaymentId,$operatorId,$isPassForStaff,
                                                                      $lastInsertId,$spaceOwnerId);
		}
                
		if($lastInsertId != -1){
		   CakeLog::write('debug','Insert succeeded...');	
		   $this->loadModel('SyncUpdate');
		   
		   if($isOfflineUpdate)
		   {
		      $updateType = Configure::read('UpdateType.offline_entry_exit_update');
		   }else if($isOnlineUpdate){
		      $updateType = Configure::read('UpdateType.online_entry_exit_update');
		   }else if($isBookingModificationUpdate){
		      $updateType = Configure::read('UpdateType.operatorApp_booking_modification');
                   }
                   else{
		      $updateType = Configure::read('UpdateType.operatorApp_booking_update');
		   }

		   if($this->SyncUpdate->generateSyncId($spaceId,$lastInsertId,$updateType) != -1){
			   //issue113
			   $syncTableDS->commit();
			   
			   if($updateSlotsInUse){
				   if($transaction['status'] == Configure::read('BookingEntryOrExit.entry')){
					   if($transaction['vehicle_type'] == 1){
						   $numCarsOccupied++;
						   $numOccupied++;
					   }else{
						   $numBikesOccupied++;
						   if($numBikesOccupied % $bikesToSlotRatio == 1)
						   {
							   $numOccupied++;
						   }	      
					   }
					   //CakeLog::write('debug','ENTRY...');
				   }else{
					   if($transaction['vehicle_type'] == 1){
						   $numCarsOccupied--;
						   $numOccupied--;
					   }else{
						   $numBikesOccupied--;
						   if($numBikesOccupied % $bikesToSlotRatio == 0)
						   {
							   $numOccupied--;
						   }	      
					   }

				   }
				   $this->loadModel('CurrentSlotOccupancy');
				   $this->CurrentSlotOccupancy->updateSlotsInUse($spaceId,$numOccupied,$numCarsOccupied,$numBikesOccupied);
			   }

			   if($isOfflineUpdate){
				   $offlineResultCode .= Configure::read('PushToServerResults.success');
			   }else if($isOnlineUpdate){
				   $onlineResultCode .= Configure::read('PushToServerResults.success');
			   }else if($isBookingModificationUpdate){
	                           $opAppBookingModificationResultCode = Configure::read('PushToServerResults.success');
                           }else{
				   $opAppResultCode .= Configure::read('PushToServerResults.success');	   
			   }
		   }else{
			   if($isOfflineUpdate){
				   $offlineResultCode .= Configure::read('PushToServerResults.failed');
			   }else if($isOnlineUpdate){
				   $onlineResultCode .= Configure::read('PushToServerResults.failed');
			   }else if($isBookingModificationUpdate){
	                           $opAppBookingModificationResultCode = Configure::read('PushToServerResults.failed');
                           } else{
				   $opAppResultCode .= Configure::read('PushToServerResults.failed');	   
			   }
			   //issue113
			   $syncTableDS->rollback();
		   }

		}else{
			//issue113
			$syncTableDS->rollback();
			CakeLog::write('debug','Update ' . $itr . ' failed to push with stale value ' . $isStale);	
			if($isStale)
			{
				if($isOfflineUpdate){
					$offlineResultCode .= Configure::read('PushToServerResults.stale');
				}else if($isOnlineUpdate){
					$onlineResultCode .= Configure::read('PushToServerResults.stale');
				}else if($isBookingModificationUpdate){
					$opAppBookingModificationResultCode .= Configure::read('PushToServerResults.stale');
				} else{
					$opAppResultCode .= Configure::read('PushToServerResults.failed');	   
				}

			}else{
				if($isOfflineUpdate){
					$offlineResultCode .= Configure::read('PushToServerResults.failed');
				}else if($isOnlineUpdate){
					$onlineResultCode .= Configure::read('PushToServerResults.failed');
				}else if($isBookingModificationUpdate){
					$opAppBookingModificationResultCode .= Configure::read('PushToServerResults.failed');
				} else{
					$opAppResultCode .= Configure::read('PushToServerResults.failed');	   
				}
			}
			$response['status'] = 2; //partial success
			$response['reason'] = "Partial Success.Some rows could not be upated";
		}
		$syncTableDS = null;
	}


	$response['onlineResultCode']                   = $onlineResultCode;
	$response['offlineResultCode']                  = $offlineResultCode;
	$response['opAppBookingResultCode']             = $opAppResultCode;
	$response['opAppBookingModificationResultCode'] = $opAppBookingModificationResultCode;
	
	$this->loadModel('SyncUpdate');
	$response['syncId'] = $this->SyncUpdate->getCurrentSyncId($spaceId);

	CakeLog::write('debug','After PUSH..syncID sent by Server is:: ' . $response['syncId']);
	CakeLog::write('debug','offlineResultCode:: ' . $offlineResultCode);

	return $response;
	
}


public function flattenBookingsArray($array)
{
   $flattenArray['opAppBooking'] = $array['Booking'];
   $flattenArray['opAppBooking']['vehicle_reg']  = $array['UserCar']['registeration_number'];

   $bikeCarType = Configure::read('VehicleType.Bike');
   if($array['UserCar']['car_type'] == Configure::read('VehicleType.Bike')){
      $flattenArray['opAppBooking']['vehicle_type'] = Configure::read('AppVehicleType.Bike');
   }else if($array['UserCar']['car_type'] == Configure::read('VehicleType.MiniBus')){
      $flattenArray['opAppBooking']['vehicle_type'] = Configure::read('AppVehicleType.MiniBus');
   }else if($array['UserCar']['car_type'] == Configure::read('VehicleType.Bus')){
      $flattenArray['opAppBooking']['vehicle_type'] = Configure::read('AppVehicleType.Bus');
   }else{
      $flattenArray['opAppBooking']['vehicle_type'] = Configure::read('AppVehicleType.Car');
   }

   $flattenArray['opAppBooking']['booking_id']        = $array['Transaction']['payment_id'];
   $flattenArray['opAppBooking']['is_renewal']        = $array['Transaction']['is_renewal'];
   $flattenArray['opAppBooking']['parent_booking_id'] = $array['Transaction']['parent_transaction_id']; 
   $flattenArray['opAppBooking']['payment_mode']         = $array['Transaction']['payment_mode']; 
   $flattenArray['opAppBooking']['invoice_id']           = $array['Transaction']['invoice_id']; 
   $flattenArray['opAppBooking']['invoice_payment_id']   = $array['Transaction']['invoice_payment_id']; 


   return $flattenArray; 	   
}

public function pullFromServer($spaceId,$syncId)
{
   $response = array();
   $response['status'] = 1;
   $response['reason'] = "Updates Follow";
   
   $this->loadModel('SyncUpdate');
   $updateIdArray = $this->SyncUpdate->getUpdateIdsToSync($spaceId,$syncId);
   //CakeLog::write('debug','In PullFromServer, updateIdArray:: ' . print_r($updateIdArray,true));
   //issue113: set the serverSyncID equal to the size of the updateIdArray
   //$serverSyncId = $this->SyncUpdate->getCurrentSyncId($spaceId);	   
   $serverSyncId = $syncId;
   $sendPullCommand = false;

   if(!empty($updateIdArray)){

      $updateTypes = array_keys($updateIdArray);
      //CakeLog::write('debug','In PullFromServer, updateTypes:: ' . print_r($updateIdArray,true));
      $numUpdates = 0;
      $recordsToPush = array();
 

      foreach($updateIdArray as $updateType => $value)
      {
        //CakeLog::write('debug', 'key is ' . $updateType);
        //CakeLog::write('debug', 'value is ' . print_r($value,true));
        $numRecords = count($value); 
        $serverSyncId += $numRecords;
        /*CakeLog::write('debug', ' count value is ' . $numRecords);
        if($numRecords == Configure::read('maxPullResponse')){
          $sendPullCommand = true;
        }*/
 
        if($updateType == Configure::read('UpdateType.offline_entry_exit_update'))
	{
		$offlineEntryExitArray = array();
		$uniqueIdsArray = array_unique(array_values($value));
                //CakeLog::write('debug','In PullFromServer, uniqueIdsArray:: ' . print_r($uniqueIdsArray,true));
		$this->loadModel('OfflineEntryExitTime');
                if($syncId == 0){
                   //if this is the first pull, then only push the latest records.
		   $offlineEntryExitArray = $this->OfflineEntryExitTime->getLatestRecordsToPushToClient($uniqueIdsArray);
                }else{
		   $offlineEntryExitArray = $this->OfflineEntryExitTime->getRecordsToPushToClient($uniqueIdsArray);
                }
                
	        //$log = $this->OfflineEntryExitTime->getDataSource()->getLog(false, false);
	        //CakeLog::write('debug',print_r($log,true));
                
		$recordsToPush = array_merge($recordsToPush,$offlineEntryExitArray);
                $offlineEntryExitArray = null;
                $uniqueIdsArray = null;
	}else if ($updateType == Configure::read('UpdateType.simplypark_booking_update')){
		$simplyParkBookingsIdArray = array_values($value);
		$this->loadModel('Booking');
		$simplyParkBookingsArray = $this->Booking->getRecordsToPushToClient($simplyParkBookingsIdArray);
               
                $recordsToPush = array_merge($recordsToPush,$simplyParkBookingsArray);
                $simplyParkBookingsArray = null;
	}else if ($updateType == Configure::read('UpdateType.online_entry_exit_update')){
               $simplyParkEntryExitUpdateIdArray = array_values($value);
               $this->loadModel('EntryExitTime');
               $simplyParkTransactionsArray = $this->EntryExitTime->getRecordsToPushToClient($simplyParkEntryExitUpdateIdArray);
               
               $recordsToPush = array_merge($recordsToPush,$simplyParkTransactionsArray);
               $simplyParkEntryExitUpdateIdArray = null;
               $simplyParkTransactionsArray = null;

	}else if ($updateType == Configure::read('UpdateType.operatorApp_booking_update')){
	      $opAppBookingIdsArray = array_values($value);
	      $this->loadModel('Booking');
	      $opAppBookingsArray = $this->Booking->getOpAppRecordsToPushToClient($opAppBookingIdsArray,$spaceId);
	      
	      
              $flattenArray = array_map(array($this,'flattenBookingsArray'),$opAppBookingsArray);
              $opAppBookingsArray = null;
	      //CakeLog::write('debug','RecordsToPush before merge ' . print_r($recordsToPush,true));
              $recordsToPush = array_merge($recordsToPush,$flattenArray); 
	      //CakeLog::write('debug','RecordsToPush afteR merge ' . print_r($recordsToPush,true));
              $flattenArray = null;
	}else if($updateType == Configure::read('UpdateType.operatorApp_booking_modification')){
              $opAppModBookingIdsArray = array_values($value);
              $this->loadModel('LostPass');
              $opAppModBookingsArray = $this->LostPass->getOpAppRecordsToPushToClient($opAppModBookingIdsArray);
              CakeLog::write('debug','In ModBookingsArray...' . print_r($opAppModBookingsArray,true));
              $recordsToPush = array_merge($recordsToPush,$opAppModBookingsArray);
              CakeLog::write('debug','After Merge...' . print_r($recordsToPush,true));
              $opAppModBookingsArray = null;
        }

      }

      //sent an array of all operators with their descriptions
      $this->loadModel('Operator');
      $response['operators'] = $this->Operator->getAllOperators($spaceId);

      //CakeLog::write('debug', 'Records To Push' . print_r($recordsToPush,true));
      $response['numUpdates'] = count($recordsToPush);
      $response['update']     = $recordsToPush;
      $response['syncId']     = $serverSyncId;
   }else{
      $response['status'] = 2;//we are not treating it as error.
      $response['reason'] = "No updates to sync";
      if($syncId != $serverSyncId){
	      CakeLog::write('error','No data to update, yet client and server sync ids are different. Client Sync ID :: ' . $syncId . ',Server Sync Id :: ' . $serverSyncId . ' SpaceID:: ' . $spaceId);
              $response['syncId'] = $syncId;  
      }else{
	      $response['syncId'] = $serverSyncId;  
      }
   }
   
   CakeLog::write('debug','Pull From Server Called, syncId sent by server:: ' . $response['syncId'] . '. Client sync id ' . $syncId . '. Space ID: ' . $spaceId);

   return $response;

}





/******************************************************************************
 *         Get Sync Id 
 *
 * ****************************************************************************/

public function getSyncId($spaceId)
{
   $this->loadModel('SyncUpdate');
   $currentSyncId = $this->SyncUpdate->getCurrentSyncId($spaceId);

   $this->loadModel('CurrentSlotOccupancy');
   $occupiedSlots = 0;
   $numParkedCars = 0;
   $numParkedBikes = 0;

   //$occupiedSlots = $this->CurrentSlotOccupancy->getSlotsInUse($spaceId,$numParkedCars,$numParkedBikes);
	   
   
   $response['status'] = 1;
   $response['reason'] = "Request Successful";
   $response['syncId'] = $currentSyncId;
   //$response['slotsOccupied'] = $occupiedSlots;
   //$response['numParkedCars'] = $numParkedCars;
   //$response['numParkedBikes'] = $numParkedBikes;
   
   $this->set('data',$response);
   $this->set('_serialize','data');
}

/**
 * Method refreshAppToken : This function will refresh a given token 
 *      @jsonData google register token 
 * @return void
 */
public function refreshAppToken($spaceId,$gcmIdAtServer)
{
	
   CakeLog::write('debug','Inside refreshToken');

   $response = array();

	
    $jsonData = $this->request->input('json_decode');
    CakeLog::write('debug','After decoding jsonData' . print_r($jsonData,true));

    $oldToken = $jsonData->{'oldtoken'};
    $newToken = $jsonData->{'newtoken'};

    CakeLog::write('debug','Old Token is ' . $oldToken);
    CakeLog::write('debug','New Token is ' . $newToken);
	
    $this->loadModel('WidgetToken');
    $this->WidgetToken->refreshAppToken($gcmIdAtServer,$oldToken,$newToken);
   
    $response['tokenRefreshed'] = 1;
    
    $this->set('data',$response);
    $this->set('_serialize','data');
}

/**
 * Method getOperatingHours : This cunction will return the operating hours 
 *                            of a given space
 *
 * */
public function getOperatingHours($spaceId){

	$operatingHours = $this->Space->opAppGetSpaceOperatingHours($spaceId);
	$response = array();
	$response['status'] = 1;
	$response['reason'] = "Space Found";

	if(!empty($operatingHours)){
              $response['openingDateTime'] = $operatingHours['SpaceTimeDay']['from_date'];
              $response['closingDateTime'] = $operatingHours['SpaceTimeDay']['to_date'];
	      $response['is24HoursOpen']   = $operatingHours['SpaceTimeDay']['open_all_hours'];
	}else{
		$response['status'] = 0;
		$response['reason'] = "Invalid Space ID or Space Timings are not set";
	}
	$this->set('data',$response);
        $this->set('_serialize','data');
}





/******************************************************************************
 *
 *   Online Bookings related functions
 *
 * ****************************************************************************/

public function logOnlineEntry($spaceId)
{
	$response = array();
	$response['status'] = 1;
	$response['reason']  = "Entry Recorded";
	
	$currentOccupancy = array();
	
        $jsonData = $this->request->input('json_decode');

	$operatorId      = $jsonData->{'operatorId'};
	$vehicleReg      = $jsonData->{'vehicleReg'};
	$entryTime       = $jsonData->{'entryTime'};
	$entryTimeStamp  = $jsonData->{'entryTimeStamp'};
	$isParkAndRide   = $jsonData->{'isParkAndRide'};
	$gcmIdAtServer   = $jsonData->{'gcmIdAtServer'};
	$vehicleType     = $jsonData->{'vehicleType'};
	$bookingType     = $jsonData->{'bookingType'};
	$parkingId       = $jsonData->{'parkingId'};
        $parentBookingId = $jsonData->{'bookingId'};
	
       CakeLog::write('debug','Inside LogOnlineEntry: OperatorId ' . $operatorId . ',vehicleReg = ' . $vehicleReg . ', vehicleType = ' . $vehicleType);
       CakeLog::write('debug','Inside LogOnlineEntry: EntryTime ' . $entryTime . 'spaceId:' . $spaceId);

       $this->loadModel('EntryExitTime');
       $entryId = 0; 
       $incrementSlotInUse = 1;
       
       //issue113: fix
       $this->loadModel('SyncUpdate');
       $syncTableDS = $this->SyncUpdate->getdatasource();
       $syncTableDS->begin();
       
       $entryLogged = $this->EntryExitTime->logEntry($spaceId,$operatorId,$parentBookingId,$parkingId,
	                                             $bookingType,
                                                     $vehicleReg,$vehicleType,
                                                     $entryTime,$isParkAndRide,
                                                     $entryId,$incrementSlotInUse);

       if($entryLogged >= 0)
       {
	       if($incrementSlotInUse)
	       {
	          //increment the Slot Occupancy Table
	          $this->loadModel('CurrentSlotOccupancy');
	          $currentOccupancy = $this->CurrentSlotOccupancy->incrementSlotInUse($spaceId,$vehicleType);
	       }else{
		  $occupancy = 0;
		  $numParkedCars = 0;
		  $numParkedBikes = 0;
	          $this->loadModel('CurrentSlotOccupancy');
		  $currentOccupancy['slotOccupancy'] = $this->CurrentSlotOccupancy->getSlotsInUse($spaceId,
			                                                                          $numParkedCars,
			                                                                          $numParkedBikes);
		  $currentOccupancy['numParkedCars']  = $numParkedCars;
		  $currentOccupancy['numParkedBikes'] = $numParkedBikes;
	       } 


	       //update the sync table
	       $this->loadModel('SyncUpdate');
	       $updateType = Configure::read('UpdateType.online_entry_exit_update');
	       $syncId = $this->SyncUpdate->generateSyncId($spaceId,$entryId,$updateType);

	       if($syncId != -1){
                       //issue113
		       $syncTableDS->commit();

		       $response['spaceId']        = $spaceId;
		       $response['numOccupied']    = $currentOccupancy['slotOccupancy'];
		       $response['numParkedCars']  = $currentOccupancy['numParkedCars']; 	
		       $response['numParkedBikes'] = $currentOccupancy['numParkedBikes'];
		       $response['syncId']         = $syncId;	

		       CakeLog::write('debug','INside SimplyParklogEntry:::' . print_r($response,true));	
		       $this->set('data',$response);
		       $this->set('_serialize','data');

		       //notify other app 
		       $this->loadModel('WidgetToken');
		       $tokenList = $this->WidgetToken->getAppTokens($spaceId,$gcmIdAtServer);

		       if(!empty($tokenList)){
			       $actionSpecificParams['action']        = Configure::read('Action.simplypark_online_entry');
			       $actionSpecificParams['isParkAndRide'] = $isParkAndRide;
			       $actionSpecificParams['bookingId']     = $parentBookingId; 
			       $actionSpecificParams['reg']           = $vehicleReg; 
			       $actionSpecificParams['parkingId']     = $parkingId;
			       $actionSpecificParams['vehicleType']   = $vehicleType;
			       $actionSpecificParams['bookingType']   = $bookingType;
			       $actionSpecificParams['numParkedCars']  = $response['numParkedCars'];
			       $actionSpecificParams['numParkedBikes'] = $response['numParkedBikes'];
			       $actionSpecificParams['numOccupied']    = $response['numOccupied'];
			       $actionSpecificParams['spaceId']        = $spaceId;
			       $actionSpecificParams['syncId']         = $response['syncId'];

			       $this->loadModel('Operator');
			       $operatorDesc = $this->Operator->getOperatorDesc($operatorId,$spaceId);
			       $actionSpecificParams['operatorDesc'] = $operatorDesc;
			       $actionSpecificParams['operatorId']   = $operatorId;


			       CakeLog::write('debug','Inside entryTimeStamp...' . $entryTimeStamp);

			       $actionSpecificParams['entryTime']   = $entryTimeStamp;  

			       $this->notifyAllApps($spaceId,$tokenList,$actionSpecificParams);
		       }
	       }else{
                       //issue113
		       $syncTableDS->rollback();
	               $response['status'] = 0;
		       $response['reason']  = "Entry Recorded failed due to db issue in SyncTable";
                       CakeLog::write('debug','INside SimplyParklogEntry:::' . print_r($response,true));	
	               $this->set('data',$response);
	               $this->set('_serialize','data');
	       }
       }else{
	       $response['status'] = 0;
	       if($entryId == -1){
		       $response['reason']  = "Entry Recorded failed due to db issue";
	       }else{
		       $response['reason']  = "Stale Update Received";
               }
	       
               CakeLog::write('debug','INside SimplyParklogEntry:::' . print_r($response,true));	
	       $this->set('data',$response);
	       $this->set('_serialize','data');
       }



}

public function logOnlineExit($spaceId,$parkingId)
{
	$response = array();
	$response['status'] = 1;
	$response['reason']  = "Exit Recorded";

	$currentOccupancy = array();
	$currentOccupancy['slotOccupancy']  = 0;
	$currentOccupancy['numParkedCars']  = 0;
	$currentOccupancy['numParkedBikes'] = 0;

	$jsonData = $this->request->input('json_decode');
	
        $operatorId      = $jsonData->{'operatorId'};
	$vehicleReg      = $jsonData->{'vehicleReg'};
	$exitTime        = $jsonData->{'exitTime'};
	$exitTimeStamp   = $jsonData->{'exitTimeStamp'};
	$gcmIdAtServer   = $jsonData->{'gcmIdAtServer'};
	$vehicleType     = $jsonData->{'vehicleType'};
	$parkingFees     = $jsonData->{'parkingFees'};
        $parentBookingId = $jsonData->{'bookingId'};
	
       CakeLog::write('debug','Inside LogOnlineExit: OperatorId ' . $operatorId . ',vehicleReg = ' . $vehicleReg);
	
       //issue113: fix
	$this->loadModel('SyncUpdate');
       $syncTableDS = $this->SyncUpdate->getdatasource();
       $syncTableDS->begin();

       $this->loadModel('EntryExitTime');
       $exitId = 0;
       $decrementSlotInUse = 1;
       
       $exitLogged = $this->EntryExitTime->logExit($spaceId,$operatorId,$parentBookingId,$parkingId,
	                                           $parkingFees,
	                                           $vehicleReg,$vehicleType,
                                                   $exitTime,
						   $exitId,$decrementSlotInUse);

       if($exitLogged >= 0)
       {
          if($decrementSlotInUse){
		$this->loadModel('CurrentSlotOccupancy');
		$currentOccupancy = $this->CurrentSlotOccupancy->decrementSlotInUse($spaceId,$vehicleType);
	  }else{		  
	        $occupancy = 0;
	        $numParkedCars = 0;
		$numParkedBikes = 0;

		$this->loadModel('CurrentSlotOccupancy');
	        $currentOccupancy['slotOccupancy'] = $this->CurrentSlotOccupancy->getSlotsInUse($spaceId,
			                                                                        $numParkedCars,
			                                                                        $numParkedBikes);
		$currentOccupancy['numParkedCars']  = $numParkedCars;
		$currentOccupancy['numParkedBikes'] = $numParkedBikes;
	  }
	   //update the sync table
	   $this->loadModel('SyncUpdate');
           $updateType = Configure::read('UpdateType.online_entry_exit_update');
	   $syncId = $this->SyncUpdate->generateSyncId($spaceId,$exitId,$updateType);

	   if($syncId != -1){
                   //issue113: fix
                   $syncTableDS->commit();
		   
		   $response['spaceId']        = $spaceId;
		   $response['numOccupied']    = $currentOccupancy['slotOccupancy'];   
		   $response['numParkedCars']  = $currentOccupancy['numParkedCars'];   
		   $response['numParkedBikes'] = $currentOccupancy['numParkedBikes'];   
		   $response['syncId']         = $syncId;

		   $this->set('data',$response);
		   $this->set('_serialize','data');

		   //notify other app 
		   $this->loadModel('WidgetToken');
		   $tokenList = $this->WidgetToken->getAppTokens($spaceId,$gcmIdAtServer);

		   if(!empty($tokenList)){
			   $actionSpecificParams['action']         = Configure::read('Action.simplypark_online_exit');
			   $actionSpecificParams['parkingId']      = $parkingId;
			   $actionSpecificParams['bookingId']      = $parentBookingId;
			   $actionSpecificParams['parkingFees']    = $parkingFees;
			   $actionSpecificParams['exitTime']       = $exitTimeStamp;  
			   $actionSpecificParams['vehicleType']    = $vehicleType;
			   $actionSpecificParams['parkingFees']    = $parkingFees;
			   $actionSpecificParams['numParkedCars']  = $response['numParkedCars'];
			   $actionSpecificParams['numParkedBikes'] = $response['numParkedBikes'];
			   $actionSpecificParams['numOccupied']    = $response['numOccupied'];
			   $actionSpecificParams['spaceId']        = $spaceId;
			   $actionSpecificParams['syncId']         = $response['syncId'];

			   $this->loadModel('Operator');
			   $operatorDesc = $this->Operator->getOperatorDesc($operatorId,$spaceId);
			   $actionSpecificParams['operatorDesc'] = $operatorDesc;
			   $actionSpecificParams['operatorId']   = $operatorId;

			   $this->notifyAllApps($spaceId,$tokenList,$actionSpecificParams); 
		   }
	   }else{
		   //issue113: fix
		   $syncTableDS->rollback();
		   $response['status'] = 0;
		   $response['reason']  = "Entry Recorded failed due to db issue";
		   
		   CakeLog::write('debug','INside SimplyParklogExit:::' . print_r($response,true));	
		   $this->set('data',$response);
		   $this->set('_serialize','data');
	   }
       }else{
	       $response['status'] = 0;
	       if($exitId == - 2)
	       {
	          $response['reason']  = "Stale Entry";
	       }else{
		  $response['reason']  = "Entry Recorded failed due to db issue";
	       }
	  
	  CakeLog::write('debug','INside SimplyParklogExit:::' . print_r($response,true));	
	  $this->set('data',$response);
	  $this->set('_serialize','data');
       
       }

}

/******************************************************************************
 *  CancelOnlineTransaction : An entry/exit on the operatorApp is cancelled.
 *  Please do not confuse it with actual cancellation of a booking which happens
 *  in the booking controller
 * ****************************************************************************/
public function cancelOnlineTransaction($spaceId)
{
	$response = array();
	$jsonData = $this->request->input('json_decode');

	$operatorId = $jsonData->{'operatorId'};
	$parkingId  = $jsonData->{'parkingId'};
	$gcmIdAtServer = $jsonData->{'gcmId'};
        
        $entryDateTimeForDb = "0000-00-00 00:00:00";
        $entryDateTimeForGcm = 0;
        $vehicleReg = "";
        $vehicleType = 0;
        $bookingId = "";
        $bookingType = 0; 
 
        $optionalParameters = 0;
        
        if(isset($jsonData->{'entry_date_time'})){
           $entryDateTimeForDb = $jsonData->{'entry_date_time'};
        }
        if(isset($jsonData->{'entry_date_time_stamp'})){
           $entryDateTimeForGcm = $jsonData->{'entry_date_time_stamp'};
        }
        if(isset($jsonData->{'vehicle_reg'})){
           $vehicleReg = $jsonData->{'vehicle_reg'};
        }
        if(isset($jsonData->{'vehicle_type'})){
           $vehicleType = $jsonData->{'vehicle_type'};
        }
        if(isset($jsonData->{'booking_id'})){
           $bookingId = $jsonData->{'booking_id'};
        }
        if(isset($jsonData->{'booking_type'})){
           $bookingType = $jsonData->{'booking_type'};
        }
        CakeLog::write('debug','In cancelOnlineTransaction with parkingID  = '. $parkingId);
	
	//issue113: fix
	$this->loadModel('SyncUpdate');
        $syncTableDS = $this->SyncUpdate->getdatasource();
	$syncTableDS->begin();
	
        $this->loadModel('EntryExitTime');
	$vehicleTypReadFromDb = 0;
	
        $cancelId = $this->EntryExitTime->cancelTransaction($spaceId,$parkingId,$operatorId,
                                                            $vehicleReg,$vehicleType,$entryDateTimeForDb,$bookingId,$bookingType,
                                                            $vehicleTypReadFromDb);

	if($cancelId >= 0)
	{
	   $this->loadModel('CurrentSlotOccupancy');
	   $this->CurrentSlotOccupancy->decrementSlotInUse($spaceId,$vehicleType);
	   
	   //update the sync table
	   $this->loadModel('SyncUpdate');
	   $updateType = Configure::read('UpdateType.online_entry_exit_update');
	   $syncId = $this->SyncUpdate->generateSyncId($spaceId,$cancelId,$updateType);

	   if($syncId != -1){
		   //issue113
		   $syncTableDS->commit();

		   $response['status'] = 1;
		   $response['reason'] = "Cancelled";
		   $response['syncId'] = $syncId;
		   $this->set('data',$response);
		   $this->set('_serialize','data');


		   //notify other app 
		   $this->loadModel('WidgetToken');
		   $tokenList = $this->WidgetToken->getAppTokens($spaceId,$gcmIdAtServer);

		   if(!empty($tokenList)){
			   $actionSpecificParams['action']        = Configure::read('Action.simplypark_online_cancel');
			   $actionSpecificParams['parkingId']     = $parkingId;
			   $actionSpecificParams['operatorId']    = $operatorId;
			   $actionSpecificParams['vehicleReg']     = $vehicleReg;
			   $actionSpecificParams['vehicleType']    = $vehicleTypeReadFromDb;
			   $actionSpecificParams['entryDateTime']  = $entryDateTimeForGcm;
			   $actionSpecificParams['bookingId']      = $bookingId;
			   $actionSpecificParams['bookingType']    = $bookingType;
			   $this->loadModel('Operator');
			   $actionSpecificParams['operatorDesc'] = $this->Operator->getOperatorDesc($operatorId,$spaceId);
			   $actionSpecificParams['syncId']        = $syncId;

			   $this->notifyAllApps($spaceId,$tokenList,$actionSpecificParams); 
		   }
	   }else{
		   //issue113
		   $syncTableDS->rollback();
		   
		   $response['status'] = 0;
		   $response['reason'] = "SyncID generation failed";	  
		   $this->set('data',$response);
		   $this->set('_serialize','data');
	   }

	   
	}else{
		$response['status'] = 0;
		$response['reason'] = "No Such Transaction";	  
		$this->set('data',$response);
		$this->set('_serialize','data');
	}
	

}


public function updateOnlineTransaction($spaceId)
{
	$response = array();
	$jsonData = $this->request->input('json_decode');

	$operatorId = $jsonData->{'operatorId'};
	$parkingId  = $jsonData->{'parkingId'};
	$parkingStatus = $jsonData->{'parkingStatus'};
	$parkingFees   = $jsonData->{'parkingFees'};
	$gcmIdAtServer = $jsonData->{'gcmId'};
        $entryDateTimeForDb = "0000-00-00 00:00:00";
        $entryDateTimeForGcm = 0;
        $exitDateTimeForDb = "0000-00-00 00:00:00";
        $exitDateTimeForGcm = 0;
        $vehicleReg = "";
        $vehicleType = 0;
        $bookingId = "";
        $bookingType = 0; 
        
        if(isset($jsonData->{'entry_date_time'})){
           $entryDateTimeForDb = $jsonData->{'entry_date_time'};
        }
        if(isset($jsonData->{'entry_date_time_stamp'})){
           $entryDateTimeForGcm = $jsonData->{'entry_date_time_stamp'};
        }
        if(isset($jsonData->{'exit_date_time'})){
           $exitDateTimeForDb = $jsonData->{'exit_date_time'};
        }
        if(isset($jsonData->{'exit_date_time_stamp'})){
           $exitDateTimeForGcm = $jsonData->{'exit_date_time_stamp'};
        }
        if(isset($jsonData->{'vehicle_reg'})){
           $vehicleReg = $jsonData->{'vehicle_reg'};
        }
        if(isset($jsonData->{'vehicle_type'})){
           $vehicleType = $jsonData->{'vehicle_type'};
        }
        if(isset($jsonData->{'booking_id'})){
           $bookingId = $jsonData->{'booking_id'};
        }
        if(isset($jsonData->{'booking_type'})){
           $bookingType = $jsonData->{'booking_type'};
        }
	
	//issue113: fix
	$this->loadModel('SyncUpdate');
        $syncTableDS = $this->SyncUpdate->getdatasource();
	$syncTableDS->begin();

	$this->loadModel('EntryExitTime');
        $vehicleTypeReadFromDb = 0;

        CakeLog::write('debug','Inside updateOnlineTransaction ...parkingID is ' . $parkingId);
	$updateId = $this->EntryExitTime->updateTransaction($spaceId,
                                                            $parkingId,$parkingStatus,$parkingFees,
                                                            $operatorId,$vehicleReg,$vehicleType,$entryDateTimeForDb,$exitDateTimeForDb,
                                                            $bookingId,$bookingType,$vehicleTypeReadFromDb);

	if($updateId >= 0)
	{
	   $this->loadModel('CurrentSlotOccupancy');
	   $this->CurrentSlotOccupancy->incrementSlotInUse($spaceId,$vehicleTypeReadFromDb);
	 	
	   //update the sync table
	   $this->loadModel('SyncUpdate');
	   $updateType = Configure::read('UpdateType.online_entry_exit_update');
	   $syncId = $this->SyncUpdate->generateSyncId($spaceId,$updateId,$updateTypeReadFromDb);

	   if($syncId != -1){
		   //issue113: fix
		   $syncTableDS->commit();

		   $response['status'] = 1;
		   $response['reason'] = "Updated";
		   $response['syncId'] = $syncId;
		   $this->set('data',$response);
		   $this->set('_serialize','data');

		   //notify other app 
		   $this->loadModel('WidgetToken');
		   $tokenList = $this->WidgetToken->getAppTokens($spaceId,$gcmIdAtServer);

		   if(!empty($tokenList)){
			   $actionSpecificParams['action']        = Configure::read('Action.simplypark_online_update');
			   $actionSpecificParams['parkingId']     = $parkingId;
			   $actionSpecificParams['status']        = $parkingStatus;
			   $actionSpecificParams['parkingFees']   = $parkingFees;
			   $actionSpecificParams['vehicleReg']     = $vehicleReg;
			   $actionSpecificParams['vehicleType']    = $vehicleTypeReadFromDb;
			   $actionSpecificParams['entryDateTime']  = $entryDateTimeForGcm;
			   $actionSpecificParams['exitDateTime']   = $exitDateTimeForGcm;
			   $actionSpecificParams['bookingId']      = $bookingId;
			   $actionSpecificParams['bookingType']    = $bookingType;
			   $actionSpecificParams['operatorId']    = $operatorId;
			   $this->loadModel('Operator');
			   $actionSpecificParams['operatorDesc'] = $this->Operator->getOperatorDesc($operatorId,$spaceId);
			   $actionSpecificParams['syncId']        = $syncId;

			   $this->notifyAllApps($spaceId,$tokenList,$actionSpecificParams); 
		   }
	   }else{
		   //issue113: fix
		   $syncTableDS->rollback();
		   $this->set('data',$response);
		   $this->set('_serialize','data');
	   }
	
	}else{
		//issue113: fix
		$syncTableDS->rollback();
		$response['status'] = 0;
		$response['reason'] = "No Such Transaction";	  
		$this->set('data',$response);
		$this->set('_serialize','data');
	}

}


/******************************************************************************
 *
 *  Misc functions
 *
 ******************************************************************************/

public function isSpaceBikeOnly($spaceId)
{
   $response = array();
   $response['status'] = 1;

   $spaceInfo = $this->Space->isSpaceBikeOnly($spaceId);

   if(!empty($spaceInfo))
   {
      $response['isBikeOnly'] = $spaceInfo['Space']['is_space_bike_only'];
   }else{
      $response['status'] = 0;
   } 

   $this->set('data',$response);
   $this->set('_serialize','data');

}

public function isCancelUpdateEnabled($spaceId)
{
   $response = array();
   $response['status'] = 1;

   $response['isCancelUpdateAllowed'] = $this->Space->isCancelUpdateEnabled($spaceId);

   $this->set('data',$response);
   $this->set('_serialize','data');

}
  /************************************************************************************
*
*  Added this unction to split to provide the functionality of selecting space from multiple  user to
*
************************************************************************************/
public function opAppGetSpaces($spaceOwnerId)
{
	CakeLog::write('debug','User Id ' . $spaceOwnerId);     
	$this->loadModel('Space');
	$activeSpace = $this->Space->opAppGetSpaces($spaceOwnerId);
	// 			  CakeLog::write('debug','Active Space for this user ' . print_r($activeSpace,true));

	$response = array();
	$response['status'] = 1;

	if(!empty($activeSpace)){
		$response['status'] = 1;
		$response['reason'] = "Spaces Found";
		$spaceArray = array();
		$spaceCount = 0;
		CakeLog::write('debug','Before acSpace ');     
		foreach ( $activeSpace as $acSpace ){
			$acSpaceB= array();
			$acSpaceB['name']             = $acSpace['Space']['name'];
			$acSpaceB['spaceId']          = $acSpace['Space']['id'];
			$acSpaceB['slots']            = $acSpace['Space']['number_slots'];
			$acSpaceB['address']          = $acSpace['Space']['flat_apartment_number'];
			$spaceArray[$spaceCount] =$acSpaceB;
			$spaceCount++;
		}
		$response['spaces'] = $spaceArray;
		$response['spaceCount'] = $spaceCount;

	}else{
		$response['status'] = 0;
		$response['reason'] = "No Parking space associated with this account. Please contact SimplyPark.in";
	}
	$this->set('data',$response);
	$this->set('_serialize','data');

}


public function cancelNdmcTransactions($spaceId){

	$response = array();
	$response['status'] = 1;
        $records = array();

	$this->loadModel('OfflineEntryExitTime');
	$records = $this->OfflineEntryExitTime->cancelNdmcTransactions();

	foreach($records as $record){

		$vehicleTypeReadFromDb = 0;
                $parkingId = $record['OfflineEntryExitTime']['parking_id'];
                $operatorId = 1;
                $vehicleReg = $record['OfflineEntryExitTime']['car_reg'];
                $vehicleType = $record['OfflineEntryExitTime']['vehicle_type'];
                $entryDateTimeForDb = $record['OfflineEntryExitTime']['entry_time'];

		$id = $this->OfflineEntryExitTime->cancelTransaction($spaceId,$parkingId,$operatorId,
				$vehicleReg,$vehicleType,$entryDateTimeForDb,$vehicleTypeReadFromDb);

		if($id >= 0)
		{
			//update the sync table
			$this->loadModel('SyncUpdate');
			$syncId = $this->SyncUpdate->generateSyncId($spaceId,$id);
			$response['status'] = 1;
			$response['reason'] = "Cancelled";
			$response['syncId'] = $syncId;

			//notify other app 
			$this->loadModel('WidgetToken');
			$tokenList = $this->WidgetToken->getAllAppTokens($spaceId);
                        $entryDateTimeForGcm = strtotime($entryDateTimeForDb) * 1000;
                        CakeLog::write('debug','Entry DateTime for GCM' . $entryDateTimeForGcm); 

			if(!empty($tokenList)){
				$actionSpecificParams['action']         = Configure::read('Action.cancel');
				$actionSpecificParams['parkingId']      = $parkingId;
				$actionSpecificParams['vehicleReg']     = $vehicleReg;
				$actionSpecificParams['vehicleType']    = $vehicleTypeReadFromDb;
				$actionSpecificParams['entryDateTime']  = $entryDateTimeForGcm;
				$actionSpecificParams['operatorId']     = $operatorId;
				$this->loadModel('Operator');
				$actionSpecificParams['operatorDesc']   = $this->Operator->getOperatorDesc($operatorId,$spaceId);
				$actionSpecificParams['syncId']         = $syncId;

				$this->notifyAllApps($spaceId,$tokenList,$actionSpecificParams); 
			}
		}
	}

	$this->set('data',$response);
	$this->set('_serialize','data');
}

/**
 * Method listNearBySpacesByLatLong to get all the spaces within 5km radius
 *
 * @param $spaceID id of the space
 * @return void
 */
public function listNearBySpacesByLatLong($latitude,$longitude) 
{
	
  //      CakeLog::write('debug','getSpaceDetail listNearBySpaces...' . print_r($latitude,true));
	
	$lat = $latitude;
	$long = $longitude;
	$conditions = array(
			'Space.is_completed' => Configure::read('Bollean.True'),
			'Space.is_activated' => Configure::read('Bollean.True'),
			'Space.is_approved' => Configure::read('Bollean.True'),
			'Space.is_deleted' => Configure::read('Bollean.False'),
			'Space.property_type_id !=' => 2, //no need to show gated communities
			);


	//By default the list will show spaces within the radius set in config file only
	$price = null;
	$distance = Configure::read('NearestSpaceDistance');
	
	$listNearBySpaces = $this->Space->getNearestSpaces($lat,$long,$conditions,$distance);
	
//	CakeLog::write('debug','In getList ...' . print_r($listNearBySpaces,true));
//	CakeLog::write('debug','In size of listNearBySpaces array ...' . print_r(sizeOf($listNearBySpaces),true));
	
	if(sizeOf($listNearBySpaces)==0){
			/*Call the google API*/
			  $socket = new HttpSocket();
			// array query
				$latLongString = $lat.",".$long;
				$results = $socket->get('https://maps.googleapis.com/maps/api/place/nearbysearch/json', array('location'=>$latLongString,'radius'=>'5000','sensor'=>'true','types'=>'parking','rankBy'=>'distance','key'=>'AIzaSyAdshU0J-37QdcgZXAuobZ0xQEewOFJxhg'));
				CakeLog::write('debug','In after getting result ...'.print_r($results,true) );
				$array = json_decode( $results->body, true );
				
				$places = $array['results'];
//				CakeLog::write('debug','In after getting places ...'.print_r($places,true) );
				$resultArray = array();
				$spaceCount = 0;
				foreach($places as $key => $place) { 
						$itemArray = array();
						$spaceArray = array();
						$spaceArray["name"] = $place["name"];
						$spaceArray["description"] = $place["name"];
						$spaceArray["space_type_id"] = 24;
						$spaceArray["property_type_id"] = 3;
						$spaceArray["flat_apartment_number"] = 3;
						$spaceArray["address1"] = $place["vicinity"];
						$spaceArray["address2"] = "";
						$spaceArray["state_id"] = 3;
						$spaceArray["city_id"] = 3;
						$spaceArray["post_code"] = 3;
						$spaceArray["lat"] = $place["geometry"]["location"]["lat"];
						$spaceArray["lng"] = $place["geometry"]["location"]["lng"];
						$spaceArray["number_slots"] = 100;
						$spaceArray["is_parking_free"] = 0;
						$spaceArray["tier_pricing_support"] = 0;
						$spaceArray["tier_desc"] = "";
						$spaceArray["is_activated"] = 1;
						$spaceArray["is_approved"] = 1;
						$spaceArray["is_completed"] = 1;
						//$spaceArray["created"] = "";
						//$spaceArray["modified"] = "";
						$spaceArray["user_id"] = 1;
						$spaceArray["icon"] = "https://www.simplypark.in/files/SpacePic/place_pic.images.jpg";
						//$spaceArray["place_pic"] = "image.jpg";
						$spaceArray["is_available_with_simplypark"] = 0;
						$spaceArray["booking_confirmation"] = 1;
						$spaceArray["google_place_id"] = $place["place_id"];
						
//						$spaceArray["mapsURL"] = $this->getShortMapsURL($spaceArray["lat"],$spaceArray['lng']);
						$itemArray["Space"] = $spaceArray;
						
						$cityArray = array();
#						$cityArray["id"]="114";
#						$cityArray["state_id"]="10";
#						$cityArray["name"]="New Delhi";
#						$cityArray["is_activated"]=true;
#						$cityArray["created"]="2015-05-22 07:09:56";
#						$cityArray["modified"]="2015-05-22 07:09:56";
#						$itemArray["City"] = $cityArray;
				
						$SpaceTimeDayArray = array();
#						$SpaceTimeDayArray["mon"]=1;
#						$SpaceTimeDayArray["tuw"]=1;
#						$SpaceTimeDayArray["wed"]=1;
#						$SpaceTimeDayArray["thu"]=1;
#						$SpaceTimeDayArray["fri"]=1;
#						$SpaceTimeDayArray["sat"]=1;
#						$SpaceTimeDayArray["sun"]=1;
#						$SpaceTimeDayArray["from_time"]=1;
#						$SpaceTimeDayArray["to_time"]=1;
#						$SpaceTimeDayArray["from_date"]=1;
#						$SpaceTimeDayArray["to_date"]=1;
#						$SpaceTimeDayArray["open_all_hours"]=1;
						$itemArray["SpaceTimeDay"]= $SpaceTimeDayArray;
						CakeLog::write('debug','In size of google-2 listNearBySpaces Saved ...'.$itemArray["Space"]["address1"]);
				/*		if($this->Space->save($itemArray)){
										CakeLog::write('debug','In size of google-2 listNearBySpaces Saved ...'.$itemArray["Space"]["address1"]);

						}else{				CakeLog::write('debug','In size of google-3 listNearBySpaces not  saved...');
						}
						*/
						$resultArray[$spaceCount++]=$itemArray;
				}
				$this->Space->saveMany($resultArray);
				$listNearBySpaces	= $resultArray;	
				#$listNearBySpaces	= $array;	
//				CakeLog::write('debug','In size of google-4 listNearBySpaces array ...' . print_r($resultArray,true));
				
		}else{
//			CakeLog::write('debug','In size of google-5 listNearBySpaces array ...');
			
			for($i=0;$i<sizeOf($listNearBySpaces);$i++) { 
			    if($listNearBySpaces[$i]["Space"]["is_available_with_simplypark"]==1){
					$listNearBySpaces[$i]["Space"]["encoded_id"]=base64_encode($listNearBySpaces[$i]["Space"]["id"]);
					}
				if($listNearBySpaces[$i]["Space"]["place_pic"]!= Null){
					if (filter_var($listNearBySpaces[$i]["Space"]["place_pic"], FILTER_VALIDATE_URL)) { 
							$listNearBySpaces[$i]["Space"]["icon"]=$listNearBySpaces[$i]["Space"]["place_pic"];
						}else{
							$listNearBySpaces[$i]["Space"]["icon"]="https://www.simplypark.in/files/SpacePic/place_pic.".$listNearBySpaces[$i]["Space"]["place_pic"];
						}
				}else{$listNearBySpaces[$i]["Space"]["icon"]="https://www.simplypark.in/files/SpacePic/place_pic.images.jpg";}
			}

		}
//	CakeLog::write('debug','In size of google-6 listNearBySpaces array ...'.print_r($listNearBySpaces,true));

	$response["listNearBySpaces"] = $listNearBySpaces; 
	$this->response->header('Access-Control-Allow-Origin', '*');
	$this->set('data',$response);
    $this->set('_serialize','data');
	
}
}
