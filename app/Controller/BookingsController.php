<?php

App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');

//loading qr code generater library 
require_once(APP . 'Vendor' . DS . 'qrcode' . DS . 'phpqrcode' . DS . 'qrlib.php');

class BookingsController extends AppController {

	public $helper = array('Html', 'User','Js','Time','Front');
	public $components = array('RequestHandler','Paginator');

	public $uses = array('Space', 'Booking', 'UserCar','BookingInstallment','SpacePark','CouponApply','Message',
                             'UserProfile','EmailTemplate','Transaction');

    //space booking global variables
    public $spaceID = '';
    public $startDate = '';
    public $endDate = '';
    public $spaceParkId = null;

    public $bookingRate = '';
    public $spacePriceWithTaxCommission = '';
    public $spacePriceWithCommission = '';
    public $spaceCommissionAmount = '';
    public $spaceTaxAmount = '';
    public $ownerIncome = '';
    public $simplyParkServiceIncome = '';
    public $serviceCommissionPercentage = '';
    public $serviceTaxPercentage = '';
    public $ownerServiceTax = '';
    public $simplyParkServiceTax = '';
    public $commissionPaidBy = '';
    public $serviceTaxPaidByOwner = '';
    public $serviceTaxPaidBySimplyPark = '';
    public $internetHandlingCharges = '';

    public $chargeServiceTaxSimplyPark = '';
    public $chargeServiceTaxOwner = '';

    //Discount coupon global variables
    public $couponPercentage = '';
    public $couponMaxAmount = '';
    public $couponFlatAmount = '';
    public $discountAmount = '';

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('payInstallment', 'installmentSave', 'cancelBookingDueToPaymentDefault',
                           'approveBookingFromEmail','ajaxManageDiscountCoupon','log_entry_exit',
                           'createBooking','createInvoice','modifyBooking');
    }

public function admin_search_bookings() {
    $this->layout = 'backend';
    $conditions =array('Transaction.payment_id' => '');  
    if (isset($this->request->query) && !empty($this->request->query)) {

        $search = str_replace('SP_', 'pay_', $this->request->query['search']);
        $conditions = array('Transaction.payment_id' =>$search);  
    }
    $transactionData = $this->Transaction->find('first',array('conditions' => $conditions));
    $bookingData = $this->Booking->findById($transactionData['Transaction']['booking_id']);
    $bookingType = !empty($bookingData['BookingInstallment']) ? 'Long Term':'Short Term';
    $this->set(compact('bookingData','bookingType'));
}

/**
 * Method admin_admin_pay_to_owner get the list   *
 * @return void 
 */
public function admin_pay_to_owner() {
    $this->layout = 'backend';
    $conditions = array(
            "DATE(Booking.start_date)" => date('Y-m-d', strtotime('-'.Configure::read('PayToOwnerDueDate'). ' days')),
            'Booking.paid_to_owner' => Configure::read('Bollean.False'),
            'Booking.'.Configure::read('BookingTableStatusField') => array(
                                        Configure::read('BookingStatus.Approved'),
                                        Configure::read('BookingStatus.Cancelled'),
                                        Configure::read('BookingStatus.CancellationRequested')
                        )   
            );
    
    $searchData = array();  
    if (isset($this->request->query) && !empty($this->request->query)) {
        $search = str_replace('SP_', 'pay_', $this->request->query['search']);
        $searchData = array(
            'OR' => array(
                'OwnerDetail.first_name LIKE' => '%'. $this->request->query['search'] .'%',
                'OwnerDetail.last_name LIKE' => '%'. $this->request->query['search'] .'%',
                'OwnerId.email LIKE' => '%'. $this->request->query['search'] .'%',
                'Booking.email LIKE' => '%'. $this->request->query['search'] .'%',
                'Booking.first_name LIKE' => '%'. $this->request->query['search'] .'%',
                'Booking.last_name LIKE' => '%'. $this->request->query['search'] .'%',
                'Transaction.payment_id' => $search, 
            )
        );
    }
    $conditions = array_merge($searchData,$conditions);
    $options['joins'] = array(
                            array('table' => 'spaces',
                                'alias' => 'OwnerSpace',
                                //'type' => 'inner',
                                'conditions' => array(
                                    'OwnerSpace.id = Booking.space_id',
                                )
                            ),
                            array('table' => 'users',
                                'alias' => 'OwnerId',
                                //'type' => 'inner',
                                'conditions' => array(
                                    'OwnerId.id = OwnerSpace.user_id',
                                )
                            ),
                            array('table' => 'user_profiles',
                                'alias' => 'OwnerDetail',
                                //'type' => 'inner',
                                'conditions' => array(
                                    'OwnerDetail.user_id = OwnerId.id',
                                )
                            )
                        );
    $options['contain'] = array(                                                             
                 'BookingInstallment'=>array(
                       'conditions'=>array(
                            "DATE_ADD(BookingInstallment.date_pay,INTERVAL 48 HOUR) <=" => date('Y-m-d H:i:s'),
                            'BookingInstallment.status' => Configure::read('Bollean.True'),
                            'BookingInstallment.is_deleted !=' => Configure::read('Bollean.True') 
                        )
                  ),
                'Transaction',
                 );
    $options['order'] = 'Booking.created ASC';
    $options['limit'] = 10;
    $options['recursive'] = -1;
    $options['fields'] = '*';
    $options['conditions'] = $conditions; 
    $this->Paginator->settings = $options;
    $bookingsData = $this->Paginator->paginate('Booking');
    $this->set('bookingsData',$bookingsData);
     if ($this->request->is('ajax')) {
        $this->layout = '';
        $this->autoRender = false;
        $this->viewPath = 'Elements' . DS . 'backend' . DS . 'Booking';
        $this->render('pay_to_owner_booking_list');
    }
}


/**
 * Method admin_admin_pay_to_owner get the list   *
 * @return void 
 */
public function admin_changeBookingPaidStatus() {
    $this->request->allowMethod('post', 'put');
    $this->request->data['Booking']['id'] = base64_decode(urldecode($this->request->data['Booking']['id']));
    if($this->Booking->save($this->request->data)) {
        $this->Session->setFlash(__('Booking paid status has been changed successfully'),'default','success');
        $this->redirect($this->referer());
    } else {
        $this->Session->setFlash(__('Request Not Completed.Please Try again!!'),'default','error');
        $this->redirect($this->referer());
    }
}

public function admin_change_booking_refund_status() {
    $this->request->allowMethod('post', 'put');
    if (!empty($this->request->data['Booking']['id'])) {
        $ref_code = $this->request->data['Booking']['bank_reference_code_refundable_deposit'];
        foreach ($this->request->data['Booking']['id'] as $key => $value) {
            $this->Booking->id = $value;
            $data['Booking']['is_deposit_refunded'] = 1;
            $data['Booking']['bank_reference_code_refundable_deposit'] = $ref_code;
            $this->Booking->save($data);
            
        }
        $this->Session->setFlash(__('Status changed successfully'),'default','success');
        $this->redirect($this->referer());  
            
    } else {
        $this->Session->setFlash(__('Please select a booking first!!'),'default','error');
        $this->redirect($this->referer());   
    }

}
/**
 * Method admin_index get the list of all spaces added
 *
 * @return void 
 */
public function admin_index() 
{
        $this->layout = 'backend';
        if (isset($this->request->query) && !empty($this->request->query)) {
            $searchData = array(
                'OR' => array(
                    'Booking.email LIKE' => '%'. $this->request->query['search'] .'%',
                    'Booking.first_name LIKE' => '%'. $this->request->query['search'] .'%',
                    'Booking.last_name LIKE' => '%'. $this->request->query['search'] .'%',
                    'Booking.name LIKE' => '%'. $this->request->query['search'] .'%',
                    
                    )
                );
        }

        $options['order'] = 'Booking.created Desc';
        $options['limit'] = 10;
        $options['conditions'] = $conditions;
        $this->Paginator->settings = $options;
        $bookingsData = $this->Paginator->paginate('Booking');
        $this->set('bookingsData',$bookingsData);
    }


/**
 * Method bookingInfo to get the space info which is currently being booked
 *
 * @return void
 */
public function index($checkToManageCoupon = null, $couponDetails = null) 
{
        //CakeLog::write('debug','in index ..this->request->data ' . print_r($this->request,true));
	$this->layout = 'home';
	$userID = $this->Auth->user('id');

	if (empty($checkToManageCoupon)) 
        {
                //CakeLog::write('debug','Empty checkToManageCoupon');
		$this->_getPutRequestedParams();
		if (!$this->_checkSameUser($this->spaceID)) {
			$this->Session->setFlash(
					__('Sorry, you can not book your own space.'),
					'default',
					'error'
					);
			$this->redirect($this->referer());
		}
	}

        CakeLog::write('debug','++bookingIndex,spaceID '.$this->spaceID . 'user ' . $userID . ' sdate ' . $this->startDate . ' edate '. $this->endDate);  
	$startDateTimeObj = new DateTime($this->startDate);
	$endDateTimeObj   = new DateTime($this->endDate);
        $lastInstallmentDateObj = clone $startDateTimeObj;
        $lastInstallmentDate = '';
        $hourlyPrice  = 0.0;
        $dailyPrice   = 0.0;
        $weeklyPrice  = 0.0;
        $monthlyPrice = 0.0;
        $yearlyPrice  = 0.0;

	$openHours = 0;
	$isOpen24Hours = false;
	$error = 0;


        
        $this->loadModel('Space');
       
        //this function also returns if deposit is required 
        $spaceInfo = $this->Space->getSpacePricingAndOperatingHours($this->spaceID);
	
        $dailyPrice   = $spaceInfo['SpacePricing'][0]['daily'];
	$weeklyPrice  = $spaceInfo['SpacePricing'][0]['weekely'];
	$monthlyPrice = $spaceInfo['SpacePricing'][0]['monthly'];
	$yearlyPrice  = $spaceInfo['SpacePricing'][0]['yearly'];
	
	//enhancement 60
	
	$isYearlyPaidInAdvance = 0;
	
	if($spaceInfo['Space']['is_yearly_payment_collected_in_advance']){
	     $isYearlyPaidInAdvance = 1;
	 }
	
	$isTierPricingEnabled = 0;
 
        if($spaceInfo['Space']['tier_pricing_support']) {
           $isTierPricingEnabled = 1;
        }else{
	   $hourlyPrice  = $spaceInfo['SpacePricing'][0]['hourly'];
        }

	if ( $spaceInfo['SpaceTimeDay']['open_all_hours'] ) 
	{
		$isOpen24Hours = true;
		$openHours = 24; 
	}
	else
	{
		$openingDateTimeObj  = new DateTime($spaceInfo['SpaceTimeDay']['from_date']);
		$closingDateTimeObj = new DateTime($spaceInfo['SpaceTimeDay']['to_date']);  
		$openingHour = intval($openingDateTimeObj->format('H'));
		$closingHour = intval($closingDateTimeObj->format('H'));
		$openingMinutes = intval($openingDateTimeObj->format('i'));
		$closingMinutes = intval($closingDateTimeObj->format('i'));

		if ( $openingMinutes > 0 )
		{
			$openingHour = $openingHour + 1;
		}

		if ( $closingHour > 0 )
		{
			$closingHour = $closingHour - 1;
		}

		$openHours = $closingHour - $openingHour;
	}
	$bookingEndHour   = intval($endDateTimeObj->format('H'));
	$bookingStartHour = intval($startDateTimeObj->format('H'));

	if ( $bookingEndHour < $bookingStartHour AND !$isOpen24Hours )
	{
		$hoursToAdd = $openHours - ($bookingStartHour - $bookingEndHour); 
		$startDateTimeObj->setTime(23,0);
		$endDateTimeObj->setTime($hoursToAdd-1,0);
	}
	else if ( $bookingEndHour > $bookingStartHour AND (($bookingEndHour - $bookingStartHour) == $openHours) AND !$isOpen24Hours )
	{
		$endDateTimeObj->modify("+1 day");
		$endDateTimeObj->setTime($bookingStartHour,0);
	}

	$diffInterval = $startDateTimeObj->diff($endDateTimeObj);

        if($isTierPricingEnabled && $diffInterval->y == 0 && $diffInterval->m == 0 && $diffInterval->d == 0) {
           $this->loadModel('SpaceTierPricing');
           //CakeLog::write('debug','Number of HOURS ..' . $diffInterval->h);
           $bookingPrice = $this->SpaceTierPricing->getTierHourlyPrice($this->spaceID,$diffInterval->h);

        }else{
	   $bookingPrice = $this->_getBookingPrice($diffInterval,$openHours,$hourlyPrice,$dailyPrice,$weeklyPrice,
	 	                                    $monthlyPrice,$yearlyPrice);
        }


        //CakeLog::write('debug','Booking Price:: ' . $bookingPrice);

        
        $numberOfInstallments  = 0;
        $monthlyInstallment    = 0;
        $spServiceTaxForFirstInstallment = 0; 
        $lastInstallmentAmount = 0;
        $deposit = 0;
	$monthlyHandlingCharge   = 0;
        $ownerServiceTax = 0.0;
        $simplyParkServiceTax = 0.0;
        $commission = 0.0;	
        $isServiceTaxPaidByOwner = 0;
        $isCommissionOneOff = false;
       
        $commission = $this->_getSimplyParkCommission($this->spaceID,$bookingPrice, //these are passed to operate on
                                                         $paidBy,$isServiceTaxPaidByOwner,$isCommissionOneOff); //this is a result paramemter.
        
        //CakeLog::write('debug','SimplyPark Commission:: ' . $commission);

        $getUserCars = $this->UserCar->getUserCars($userID);
	$userCars = $this->__organizeUserCars($getUserCars);
        
        $serviceTaxPercentage = 0.0;
        
        $this->_getServiceTax($this->spaceID,$bookingPrice,$commission,$isServiceTaxPaidByOwner,
                                  $ownerServiceTax,$simplyParkServiceTax,$serviceTaxPercentage);


        $discount = 0.0;


        $couponFlatAmount   = 0;
        $discountPercentage = 0;
        $maxDiscountAmount  = 0;
        $discount           = 0;
        

        $handlingCharges = $commission + $ownerServiceTax + $simplyParkServiceTax;
        

        if ( $paidBy == Configure::read('CommissionPaidBy.Driver') )
        {
           $ownerIncome = $bookingPrice;
        }
       else
        {
           $ownerIncome = ($bookingPrice - ($commission + $simplyParkServiceTax));
        }

        $totalPrice = $bookingPrice + $handlingCharges;
        //CakeLog::write('debug','TotalPrice:: ' . $totalPrice);
        $firstInstallmentAmount = 0.0;
        $ownerIncomeForLastInstallment = 0;
        $ownerIncomePerInstallment     = 0;
        $ownerServiceTaxPerInstallment = 0;
        $ownerServiceTaxForLastInstallment = 0;
        $simplyParkIncomePerInstallment = 0;
        $simplyParkIncomeForLastInstallment = 0;
        $simplyParkServiceTaxPerInstallment = 0;
        $simplyParkServiceTaxForLastInstallment = 0; 
        $lastMonthHandlingCharge = 0;
        /* lets calculate the installment amount */
	if ( $diffInterval->y ) 
	{
		if(!$isYearlyPaidInAdvance)
		{
			$numberOfInstallments    = 12;

			$deposit                 = ($spaceInfo['Space']['is_deposit_required']?ceil($yearlyPrice/12):0); 
			$lDate                   = $lastInstallmentDateObj->add(new DateInterval('P11M'));
			$lastInstallmentDate     = $lDate->format('d-m-Y');
                        if($isCommissionOneOff == false)
			{
				$monthlyHandlingCharge   = ceil(($handlingCharges)/12);
				$lastMonthHandlingCharge = $monthlyHandlingCharge;
				$monthlyInstallment      = ceil($yearlyPrice/12 + $monthlyHandlingCharge);
                                $firstInstallmentAmount  = $monthlyInstallment;
				$lastInstallmentAmount   = $monthlyInstallment;

				$ownerIncomePerInstallment     = $monthlyInstallment - $monthlyHandlingCharge;
				$ownerIncomeForLastInstallment = $ownerIncomePerInstallment; 
				$ownerServiceTaxPerInstallment     = $ownerServiceTax/12;
				$ownerServiceTaxForLastInstallment = $ownerServiceTaxPerInstallment;

				$simplyParkIncomePerInstallment     = $commission/12;
				$simplyParkIncomeForLastInstallment = $simplyParkIncomePerInstallment;
				$simplyParkServiceTaxPerInstallment = $simplyParkServiceTax/12;
				$simplyParkServiceTaxForLastInstallment = $simplyParkServiceTaxPerInstallment;
                        }else{
				$monthlyInstallment      = ceil($yearlyPrice/12);
				$lastInstallmentAmount   = $monthlyInstallment;
                                $firstInstallmentAmount  = $monthlyInstallment + $handlingCharges;

				$ownerIncomePerInstallment     = $monthlyInstallment;
				$ownerIncomeForLastInstallment = $ownerIncomePerInstallment; 
				$ownerServiceTaxPerInstallment     = $ownerServiceTax/12;
				$ownerServiceTaxForLastInstallment = $ownerServiceTaxPerInstallment;
                        }
		}
        
                
	}
       else if ( ($diffInterval->m ==1 AND $diffInterval->d ) OR ($diffInterval->m > 1) )
       { 
	       $numberOfInstallments = $diffInterval->m;
	       $numberOfInstallments = $diffInterval->d?$numberOfInstallments+1:$numberOfInstallments;


	       $ratio = 0;
	       $lastMonthHandlingCharge = 0;

               if(!$isCommissionOneOff)
	       {
		       if ( $diffInterval->d )
		       {
			       $ratio                    = ($diffInterval->m * 30) + $diffInterval->d;
			       $lastMonthHandlingCharge  = ($handlingCharges * $diffInterval->d)/$ratio;
			       $monthlyHandlingCharge    = ($handlingCharges - $lastMonthHandlingCharge)/($diffInterval->m);

			       $ownerServiceTaxForLastInstallment = ($ownerServiceTax * $diffInterval->d)/$ratio;
			       $ownerServiceTaxPerInstallment     = ($ownerServiceTax - $ownerServiceTaxForLastInstallment)/$diffInterval->m;

			       $simplyParkIncomeForLastInstallment   = ($commission * $diffInterval->d)/$ratio;
			       $simplyParkIncomePerInstallment       = ($commission - $simplyParkIncomeForLastInstallment)/$diffInterval->m;

			       $simplyParkServiceTaxForLastInstallment = ($simplyParkServiceTax * $diffInterval->d)/$ratio;
			       $simplyParkServiceTaxPerInstallment      = ($simplyParkServiceTax - $simplyParkServiceTaxForLastInstallment)/$diffInterval->m;
		       }
		       else
		       {
			       $ratio                   = $diffInterval->m;
			       $lastMonthHandlingCharge = $handlingCharges/$ratio;
			       $monthlyHandlingCharge   = $lastMonthHandlingCharge;

			       $ownerServiceTaxForLastInstallment = ($ownerServiceTax)/$ratio;
			       $ownerServiceTaxPerInstallment     =  $ownerServiceTaxForLastInstallment;

			       $simplyParkIncomeForLastInstallment   = ($commission)/$ratio;
			       $simplyParkIncomePerInstallment       = $simplyParkIncomeForLastInstallment;

			       $simplyParkServiceTaxForLastInstallment = ($simplyParkServiceTax)/$ratio;
			       $simplyParkServiceTaxPerInstallment     = $simplyParkServiceTaxForLastInstallment;
		       }
	       }else{
		       $lastMonthHandlingCharge  = 0.0;
		       $monthlyHandlingCharge    = 0.0;
		       $simplyParkIncomeForLastInstallment   = 0.0; 
		       $simplyParkIncomePerInstallment       = 0.0;

		       $simplyParkServiceTaxForLastInstallment = 0.0;
		       $simplyParkServiceTaxPerInstallment      = 0.0;
		       
                      if ($diffInterval->d)
		       {
			       $ratio                    = ($diffInterval->m * 30) + $diffInterval->d;

			       $ownerServiceTaxForLastInstallment = ($ownerServiceTax * $diffInterval->d)/$ratio;
			       $ownerServiceTaxPerInstallment     = ($ownerServiceTax - $ownerServiceTaxForLastInstallment)/$diffInterval->m;
		       }
		       else
		       {
			       $ratio                   = $diffInterval->m;

			       $ownerServiceTaxForLastInstallment = ($ownerServiceTax)/$ratio;
			       $ownerServiceTaxPerInstallment     =  $ownerServiceTaxForLastInstallment;

		       }

               }     

	       $monthlyInstallment       = $monthlyPrice + $monthlyHandlingCharge;
               //CakeLog::write('debug','Monthly Installment ...' . $monthlyInstallment);
               if(!$isCommissionOneOff)
	       {
	          $firstInstallmentAmount   = $monthlyInstallment;
	          $lastInstallmentAmount    = $totalPrice - ($monthlyInstallment * ($numberOfInstallments - 1));
                       
	       }else{
	          $firstInstallmentAmount   = $monthlyPrice + $handlingCharges;
                  if($numberOfInstallments > 2){
	             $lastInstallmentAmount    = $totalPrice - $firstInstallmentAmount - ($monthlyInstallment * ($numberOfInstallments - 2));
                  }else{
	             $lastInstallmentAmount    = $totalPrice - $firstInstallmentAmount;
                  }
               }
               //CakeLog::write('debug','First Installment ...' . $firstInstallmentAmount);
	       $ownerIncomePerInstallment     = $monthlyInstallment - $monthlyHandlingCharge;
	       $ownerIncomeForLastInstallment = $lastInstallmentAmount - $lastMonthHandlingCharge; 

	       $deposit    = ($spaceInfo['Space']['is_deposit_required']?$monthlyPrice:0);

	       $dateModString       = 'P'.$diffInterval->m.'M';
	       $lDate               = $lastInstallmentDateObj->add(new DateInterval($dateModString));
	       $lastInstallmentDate = $lDate->format('d-m-Y');


       }

       
       
       $status = array();
        
       if ($checkToManageCoupon) 
       {
	       $couponFlatAmount = $couponDetails['DiscountCoupon']['flat_amount'];
	       $discountPercentage = 0.0;
	       $maxDiscountAmount  = 0.0;

	       if ($couponDetails['DiscountCoupon']['discount_type'] == Configure::read('Bollean.True')) 
	       {
		       $discountPercentage = $couponDetails['DiscountCoupon']['discount_percentage'];
		       $maxDiscountAmount  = $couponDetails['DiscountCoupon']['max_amount_discount'];
	       }


	       if ( $couponFlatAmount )
	       {
		       $discount = $couponFlatAmount;
	       }
	       else if ( $discountPercentage )
	       {
		       if ( $monthlyInstallment )
		       {
			       $amountToDiscount = $monthlyInstallment - $simplyParkServiceTaxPerInstallment;
		       }
		       else
		       { 
			       $amountToDiscount = $totalPrice - $simplyParkServiceTax;
		       }


		       $discount = (($discountPercentage * $amountToDiscount)/100); 
		       $discount = ( $discount > $maxDiscountAmount) ? $maxDiscountAmount : $discount;
	       }


	       if($monthlyInstallment)
	       { 
                       if($isCommissionOneOff)
                       {
                          $spIncomeForFirstInstallment = $commission - $discount;
                       }else{ 
		          $spIncomeForFirstInstallment     = $simplyParkIncomePerInstallment - $discount;
                       }

		       if ( $spIncomeForFirstInstallment > 0 && $simplyParkServiceTax > 0.0)
		       {
			       $spServiceTaxForFirstInstallment = $this->calculateServiceTax($spIncomeForFirstInstallment,
					       $serviceTaxPercentage); 
		       }

		       $handlingChargesForFirstInstallment = $ownerServiceTaxPerInstallment + 
			       $spIncomeForFirstInstallment + 
			       $spServiceTaxForFirstInstallment;

		       $firstInstallmentAmount = $ownerIncomePerInstallment +
			       $handlingChargesForFirstInstallment;
		       $status = array(
				       'Type' => Configure::read('StatusType.Success'),
				       'Message' => __('Coupon applied sucessfully.'),
				       'discountCouponId' => $couponDetails['DiscountCoupon']['id'],
				       'discountAmount' => round($discount,2),
				       'isShortTermBooking' => 0,
				       'firstInstallment' => round($firstInstallmentAmount,2),
				       'spIncomeForFirstInstallment' => round($spIncomeForFirstInstallment,2),
				       'spServiceTaxForFirstInstallment' => round($spServiceTaxForFirstInstallment,2),
				       'handlingChargesForFirstInstallment' => round($handlingChargesForFirstInstallment,2),
				       'refundableDeposit' => round($deposit,2),
				       'payNow' => round($deposit+$firstInstallmentAmount,2),
				       'isBulk' => 0,
                                       'isCommissionOneOff' => $isCommissionOneOff
				      );

	       }
	       else
	       { 
		       $commission = $commission - $discount;

		       if ( $commission > 0 && $simplyParkServiceTax > 0.0)
		       {
			       $simplyParkServiceTax = $this->calculateServiceTax($commission, $serviceTaxPercentage);
		       }
		       else
		       {
			       $simplyParkServiceTax = 0;
		       } 

		       $handlingCharges = $ownerServiceTax + $commission + $simplyParkServiceTax;
		       $totalPrice = $bookingPrice + $handlingCharges;

		       $status = array(
				       'Type' => Configure::read('StatusType.Success'),
				       'Message' => __('Coupon applied sucessfully.'),
				       'discountCouponId' => $couponDetails['DiscountCoupon']['id'],
				       'discountAmount' => round($discount,2),
				       'isShortTermBooking' => 1,
				       'totalPrice' => $totalPrice,
				       'handlingCharges' => $handlingCharges,
				       'simplyParkIncome' => $commission,
				       'simplyParkServiceTax' => $simplyParkServiceTax,
				       'isBulk' => 0
				      );
	       }
       }
      else
      {
	      $spaceDetail = $this->Space->bookingSpaceInfo($this->spaceID);
	      // space related parameters 
	      $this->set('spaceDetail',$spaceDetail);
	      $this->set('spaceID',$this->spaceID);
	      $this->set('userCars',$userCars);
	      $this->set('isBulk',0);
	      //for non bulk booking, space_park_id actually refers to the slot.
              //for bulk booking, space_park_id refers to starting slot number. 
              //CakeLog::write('debug','space park id' . $this->spaceParkId);
              $this->set('space_park_id',$this->spaceParkId);

	      // booking start,end date,cars related params
	      $this->set('startDate',$this->startDate);
	      $this->set('startDate',$this->startDate);
	      $this->set('endDate',$this->endDate);


	      //installment related params
	      $this->set('monthlyInstallment',round($monthlyInstallment,2));
	      $this->set('refundableDeposit',round($deposit,2));
              $this->set('firstInstallmentAmount',round($firstInstallmentAmount,2));
	      $this->set('lastInstallmentAmount',round($lastInstallmentAmount,2));
	      $this->set('numberOfInstallments',$numberOfInstallments);
	      $this->set('lastInstallmentDate', $lastInstallmentDate);
	      $this->set('monthlyHandlingCharge',round($monthlyHandlingCharge,2));
	      $this->set('lastMonthHandlingCharge',round($lastMonthHandlingCharge,2));

              //CakeLog::write('debug','LastInstallmentAmount ' . $lastInstallmentAmount);
	      // total price to pay + handlingcharges
	      $this->set('totalPrice',$totalPrice); //does not include deposit 
	      $this->set('handlingCharges',$handlingCharges);
	      $this->set('isYearlyPaidInAdvance',$isYearlyPaidInAdvance);
              $this->set('isCommissionOneOff',$isCommissionOneOff);

	      //commission,service tax related params
	      $this->set('commissionPaidBy',$paidBy);
	      $this->set('ownerServiceTax',$ownerServiceTax);
	      $this->set('ownerServiceTaxPerInstallment',$ownerServiceTaxPerInstallment);
	      $this->set('ownerServiceTaxForLastInstallment',$ownerServiceTaxForLastInstallment);
	      $this->set('simplyParkServiceTax',$simplyParkServiceTax);
	      $this->set('simplyParkServiceTaxPerInstallment',$simplyParkServiceTaxPerInstallment);
	      $this->set('simplyParkServiceTaxForLastInstallment',$simplyParkServiceTaxForLastInstallment);
	      $this->set('ownerIncome',$ownerIncome);
	      $this->set('ownerIncomePerInstallment',$ownerIncomePerInstallment);
	      $this->set('ownerIncomeForLastInstallment',$ownerIncomeForLastInstallment);
	      $this->set('internetHandlingCharges',$handlingCharges);
	      $this->set('simplyParkServiceIncome',$commission);
	      $this->set('simplyParkIncomePerInstallment',$simplyParkIncomePerInstallment);
	      $this->set('simplyParkIncomeForLastInstallment',$simplyParkIncomeForLastInstallment);
	      $this->set('bookingPriceBeforeHandlingCharges', $bookingPrice);
	      $this->set('discount',$discount);
	      $this->set('serviceTaxPercentage',$serviceTaxPercentage);
	      $this->set('numSlots',$this->numSlots);
             
      }



    return $status;

}

/**
 * Method bulkIndex : seperate implementation for bulk booking 
 *
 * @return void
 */
public function bulkIndex($checkToManageCoupon = null, $couponDetails = null) 
{
        //CakeLog::write('debug','in bulkIndex ..');
	$this->layout = 'home';
	$userID = $this->Auth->user('id');

	if (empty($checkToManageCoupon)) 
        {
                //CakeLog::write('debug','Empty checkToManageCoupon');
		$this->_getPutRequestedParams(1);
		if (!$this->_checkSameUser($this->spaceID)) {
			$this->Session->setFlash(
					__('Sorry, you can not book your own space.'),
					'default',
					'error'
					);
			$this->redirect($this->referer());
		}
	}

        
	$startDateTimeObj = new DateTime($this->startDate);
	$endDateTimeObj   = new DateTime($this->endDate);
        $lastInstallmentDateObj = clone $startDateTimeObj;
        $lastInstallmentDate = '';
        $hourlyPrice  = 0.0;
        $dailyPrice   = 0.0;
        $weeklyPrice  = 0.0;
        $monthlyPrice = 0.0;
        $yearlyPrice  = 0.0;

	$openHours = 0;
	$isOpen24Hours = false;
	$error = 0;
        
        $this->loadModel('Space');
	$spaceInfo = $this->Space->getSpaceOperatingHours($this->spaceID);
        $this->loadModel('BulkBookingSetting');
        
        $bulkBookingSettings = $this->BulkBookingSetting->getSettings($this->spaceID);
	$hourlyPrice  = $bulkBookingSettings['BulkBookingSetting']['hourly_price'] * $this->numSlots;
	$dailyPrice   = $bulkBookingSettings['BulkBookingSetting']['daily_price'] * $this->numSlots;
	$weeklyPrice  = $bulkBookingSettings['BulkBookingSetting']['weekly_price'] * $this->numSlots;
	$monthlyPrice = $bulkBookingSettings['BulkBookingSetting']['monthly_price'] * $this->numSlots;
	$yearlyPrice  = $bulkBookingSettings['BulkBookingSetting']['yearly_price'] * $this->numSlots;

	if ( $spaceInfo['SpaceTimeDay']['open_all_hours'] ) 
	{
		$isOpen24Hours = true;
		$openHours = 24; 
	}
	else
	{
		$openingDateTimeObj  = new DateTime($spaceInfo['SpaceTimeDay']['from_date']);
		$closingDateTimeObj = new DateTime($spaceInfo['SpaceTimeDay']['to_date']);  
		$openingHour = intval($openingDateTimeObj->format('H'));
		$closingHour = intval($closingDateTimeObj->format('H'));
		$openingMinutes = intval($openingDateTimeObj->format('i'));
		$closingMinutes = intval($closingDateTimeObj->format('i'));

		if ( $openingMinutes > 0 )
		{
			$openingHour = $openingHour + 1;
		}

		if ( $closingHour > 0 )
		{
			$closingHour = $closingHour - 1;
		}

		$openHours = $closingHour - $openingHour;
	}
	$bookingEndHour   = intval($endDateTimeObj->format('H'));
	$bookingStartHour = intval($startDateTimeObj->format('H'));

	if ( $bookingEndHour < $bookingStartHour AND !$isOpen24Hours )
	{
		$hoursToAdd = $openHours - ($bookingStartHour - $bookingEndHour); 
		$startDateTimeObj->setTime(23,0);
		$endDateTimeObj->setTime($hoursToAdd-1,0);
	}
	else if ( $bookingEndHour > $bookingStartHour AND (($bookingEndHour - $bookingStartHour) == $openHours) AND !$isOpen24Hours )
	{
		$endDateTimeObj->modify("+1 day");
		$endDateTimeObj->setTime($bookingStartHour,0);
	}

	$diffInterval = $startDateTimeObj->diff($endDateTimeObj);


	$bookingPrice = $this->_getBookingPrice($diffInterval,$openHours,$hourlyPrice,$dailyPrice,$weeklyPrice,
		                                $monthlyPrice,$yearlyPrice);


        //CakeLog::write('debug','Booking Price:: ' . $bookingPrice);

        
        $numberOfInstallments  = 0;
        $monthlyInstallment    = 0;
        $firstInstallmentAmount          = 0;
        $spServiceTaxForFirstInstallment = 0; 
        $lastInstallmentAmount = 0;
        $deposit = 0;
	$monthlyHandlingCharge   = 0;
        $ownerServiceTax = 0.0;
        $simplyParkServiceTax = 0.0;
        $commission = 0.0;	
        $isServiceTaxPaidByOwner = 0;
       
       $commission = $this->_getSimplyParkCommissionForBulkBookings($this->spaceID,$bookingPrice,$bulkBookingSettings, //these are passed to operate on
                                                                        $paidBy,$isServiceTaxPaidByOwner); //this is a result paramemter.

       $serviceTaxPercentage = 0.0;
       
       $this->_getServiceTaxForBulkBookings($this->spaceID,$bookingPrice,$commission,$bulkBookingSettings,
                                                $isServiceTaxPaidByOwner,$ownerServiceTax,
                                                $simplyParkServiceTax,$serviceTaxPercentage);
       $bulkUserCarId = Configure::read('BulkCarId');

	
        $discount = 0.0;


        $couponFlatAmount   = 0;
        $discountPercentage = 0;
        $maxDiscountAmount  = 0;
        $discount           = 0;
        

        $handlingCharges = $commission + $ownerServiceTax + $simplyParkServiceTax;
        

        if ( $paidBy == Configure::read('CommissionPaidBy.Driver') )
        {
           $ownerIncome = $bookingPrice;
        }
       else
        {
           $ownerIncome = ($bookingPrice - ($commission + $simplyParkServiceTax));
        }

        $totalPrice = $bookingPrice + $handlingCharges;
        //CakeLog::write('debug','TotalPrice:: ' . $totalPrice);

        $ownerIncomeForLastInstallment = 0;
        $ownerIncomePerInstallment     = 0;
        $ownerServiceTaxPerInstallment = 0;
        $ownerServiceTaxForLastInstallment = 0;
        $simplyParkIncomePerInstallment = 0;
        $simplyParkIncomeForLastInstallment = 0;
        $simplyParkServiceTaxPerInstallment = 0;
        $simplyParkServiceTaxForLastInstallment = 0; 
        $lastMonthHandlingCharge = 0;
        /* lets calculate the installment amount */
	if ( $diffInterval->y ) 
	{
		$numberOfInstallments    = 12;

		$deposit                 = $bulkBookingSettings['BulkBookingSetting']['deposit']; 
		$lDate                   = $lastInstallmentDateObj->add(new DateInterval('P11M'));
		$lastInstallmentDate     = $lDate->format('d-m-Y');
		$monthlyHandlingCharge   = ceil(($handlingCharges)/12);
                $lastMonthHandlingCharge = $monthlyHandlingCharge;
		$monthlyInstallment      = ceil($yearlyPrice/12 + $monthlyHandlingCharge);
		$lastInstallmentAmount   = $monthlyInstallment;
                
                $ownerIncomePerInstallment     = $monthlyInstallment - $monthlyHandlingCharge;
                $ownerIncomeForLastInstallment = $ownerIncomePerInstallment; 
                $ownerServiceTaxPerInstallment     = $ownerServiceTax/12;
                $ownerServiceTaxForLastInstallment = $ownerServiceTaxPerInstallment;

                $simplyParkIncomePerInstallment     = $commission/12;
                $simplyParkIncomeForLastInstallment = $simplyParkIncomePerInstallment;
                $simplyParkServiceTaxPerInstallment = $simplyParkServiceTax/12;
                $simplyParkServiceTaxForLastInstallment = $simplyParkServiceTaxPerInstallment;
        
                
	}
       else if ( ($diffInterval->m ==1 AND $diffInterval->d ) OR ($diffInterval->m > 1) )
       { 
	       $numberOfInstallments = $diffInterval->m;
	       $numberOfInstallments = $diffInterval->d?$numberOfInstallments+1:$numberOfInstallments;


               $ratio = 0;
               $lastMonthHandlingCharge = 0;

               if ( $diffInterval->d )
               {
                  $ratio                    = ($diffInterval->m * 30) + $diffInterval->d;
                  $lastMonthHandlingCharge  = ($handlingCharges * $diffInterval->d)/$ratio;
	          $monthlyHandlingCharge    = ($handlingCharges - $lastMonthHandlingCharge)/($diffInterval->m);

                  $ownerServiceTaxForLastInstallment = ($ownerServiceTax * $diffInterval->d)/$ratio;
                  $ownerServiceTaxPerInstallment     = ($ownerServiceTax - $ownerServiceTaxForLastInstallment)/$diffInterval->m;

                  $simplyParkIncomeForLastInstallment   = ($commission * $diffInterval->d)/$ratio;
                  $simplyParkIncomePerInstallment       = ($commission - $simplyParkIncomeForLastInstallment)/$diffInterval->m;

                  $simplyParkServiceTaxForLastInstallment = ($simplyParkServiceTax * $diffInterval->d)/$ratio;
                  $simplyParkServiceTaxPerInstallment      = ($simplyParkServiceTax - $simplyParkServiceTaxForLastInstallment)/$diffInterval->m;
               }
              else
               {
                  $ratio                   = $diffInterval->m;
                  $lastMonthHandlingCharge = $handlingCharges/$ratio;
                  $monthlyHandlingCharge   = $lastMonthHandlingCharge;
                  
                  $ownerServiceTaxForLastInstallment = ($ownerServiceTax)/$ratio;
                  $ownerServiceTaxPerInstallment     =  $ownerServiceTaxForLastInstallment;

                  $simplyParkIncomeForLastInstallment   = ($commission)/$ratio;
                  $simplyParkIncomePerInstallment       = $simplyParkIncomeForLastInstallment;

                  $simplyParkServiceTaxForLastInstallment = ($simplyParkServiceTax)/$ratio;
                  $simplyParkServiceTaxPerInstallment     = $simplyParkServiceTaxForLastInstallment;
               }     
        
 
	       $monthlyInstallment       = $monthlyPrice + $monthlyHandlingCharge;
	       $lastInstallmentAmount    = $totalPrice - ($monthlyInstallment * ($numberOfInstallments - 1));
	       
	       $deposit    = $bulkBookingSettings['BulkBookingSetting']['deposit']; 

	       $dateModString       = 'P'.$diffInterval->m.'M';
	       $lDate               = $lastInstallmentDateObj->add(new DateInterval($dateModString));
	       $lastInstallmentDate = $lDate->format('d-m-Y');
               
               $ownerIncomePerInstallment     = $monthlyInstallment - $monthlyHandlingCharge;
               $ownerIncomeForLastInstallment = $lastInstallmentAmount - $lastMonthHandlingCharge; 
                
       }

       
       
       $status = array();
        
       if ($checkToManageCoupon) 
       {
	       $couponFlatAmount = $couponDetails['DiscountCoupon']['flat_amount'];
	       $discountPercentage = 0.0;
	       $maxDiscountAmount  = 0.0;

	       if ($couponDetails['DiscountCoupon']['discount_type'] == Configure::read('Bollean.True')) 
	       {
		       $discountPercentage = $couponDetails['DiscountCoupon']['discount_percentage'];
		       $maxDiscountAmount  = $couponDetails['DiscountCoupon']['max_amount_discount'];
	       }

                  //CakeLog::write('debug','monthly installment ' . $monthlyInstallment);
                  if($monthlyInstallment)
                  {
                     $status = $this->__calculateDiscountForLongTermBulkBooking($couponDetails,$couponFlatAmount,$discountPercentage,$maxDiscountAmount,
                                                                                $this->numSlots, 
                                                                                $numberOfInstallments,$monthlyInstallment,$lastInstallmentAmount,
                                                                                $deposit,$ownerIncomePerInstallment,$ownerIncomeForLastInstallment,
                                                                                $ownerServiceTaxPerInstallment,$ownerServiceTaxForLastInstallment,
                                                                                $simplyParkIncomePerInstallment,$simplyParkIncomeForLastInstallment,
                                                                                $simplyParkServiceTaxPerInstallment,$simplyParkServiceTaxForLastInstallment);
                  }else{
			$status = array(
					'Type' => Configure::read('StatusType.Error'),
					'Message' => __('This voucher cannot be applied on this booking.')
                                       );
                  }
      }
      else
      {
	      $spaceDetail = $this->Space->bookingSpaceInfo($this->spaceID);
	      // space related parameters 
	      $this->set('spaceDetail',$spaceDetail);
	      $this->set('spaceID',$this->spaceID);
	      $this->set('bulkUserCarId',$bulkUserCarId);
	      $this->set('isBulk',1);
	       
              //for non bulk booking, space_park_id actually refers to the slot.
              //for bulk booking, space_park_id refers to starting slot number. 
              //CakeLog::write('debug','space park id' . $this->spaceParkId);
              $this->set('space_park_id',$this->spaceParkId);

	      // booking start,end date,cars related params
	      $this->set('startDate',$this->startDate);
	      $this->set('startDate',$this->startDate);
	      $this->set('endDate',$this->endDate);

	      //installment related params
	      $this->set('monthlyInstallment',round($monthlyInstallment,2));
	      $this->set('refundableDeposit',round($deposit,2));
	      $this->set('lastInstallmentAmount',round($lastInstallmentAmount,2));
              //CakeLog::write('debug','In bulk booking...numberOfInstallments ' . $numberOfInstallments);
	      $this->set('numberOfInstallments',$numberOfInstallments);
	      $this->set('lastInstallmentDate', $lastInstallmentDate);
	      $this->set('monthlyHandlingCharge',round($monthlyHandlingCharge,2));
	      $this->set('lastMonthHandlingCharge',round($lastMonthHandlingCharge,2));

              //CakeLog::write('debug','LastInstallmentAmount ' . $lastInstallmentAmount);
	      // total price to pay + handlingcharges
	      $this->set('totalPrice',$totalPrice); //does not include deposit 
	      $this->set('handlingCharges',$handlingCharges);

	      //commission,service tax related params
	      $this->set('commissionPaidBy',$paidBy);
	      $this->set('ownerServiceTax',$ownerServiceTax);
	      $this->set('ownerServiceTaxPerInstallment',$ownerServiceTaxPerInstallment);
	      $this->set('ownerServiceTaxForLastInstallment',$ownerServiceTaxForLastInstallment);
	      $this->set('simplyParkServiceTax',$simplyParkServiceTax);
	      $this->set('simplyParkServiceTaxPerInstallment',$simplyParkServiceTaxPerInstallment);
	      $this->set('simplyParkServiceTaxForLastInstallment',$simplyParkServiceTaxForLastInstallment);
	      $this->set('ownerIncome',$ownerIncome);
	      $this->set('ownerIncomePerInstallment',$ownerIncomePerInstallment);
	      $this->set('ownerIncomeForLastInstallment',$ownerIncomeForLastInstallment);
	      $this->set('internetHandlingCharges',$handlingCharges);
	      $this->set('simplyParkServiceIncome',$commission);
	      $this->set('simplyParkIncomePerInstallment',$simplyParkIncomePerInstallment);
	      $this->set('simplyParkIncomeForLastInstallment',$simplyParkIncomeForLastInstallment);
	      $this->set('bookingPriceBeforeHandlingCharges', $bookingPrice);
	      $this->set('discount',$discount);
	      $this->set('serviceTaxPercentage',$serviceTaxPercentage);
	      $this->set('numSlots',$this->numSlots);
             
             $this->render('bulkindex');
      }



    return $status;

}

/**
 * Method _getPutRequestedParams to set the named and request data parametters
 *
 * @reutn bool true
 */
    protected function _getPutRequestedParams($isBulk = 0) {
        if (isset($this->request->data['Space']) && !empty($this->request->data['Space'])) {
            $this->spaceID = $this->request->data['Space']['space_id'];
            $this->startDate = $this->request->data['Space']['start_date'];
            $this->endDate = $this->request->data['Space']['end_date'];
            if($isBulk){
               $this->numSlots = $this->request->data['Space']['num_slots'];
            }else{
               $this->numSlots = 1;
            }
            $this->spaceParkId = $this->request->data['Space']['space_park_id'];
        } elseif (isset($this->request->params['named']) && !empty($this->request->params['named'])) {
            $this->spaceID = base64_decode(urldecode($this->request->params['named']['space_id']));
            $this->startDate = base64_decode(urldecode($this->request->params['named']['stdate']));
            $this->endDate = base64_decode(urldecode($this->request->params['named']['eddate']));
            if($isBulk){
               $this->numSlots = base64_decode(urldecode($this->request->params['named']['numSlots']));
            }else{
               $this->numSlots = 1;
            }
            $this->spaceParkId = base64_decode(urldecode($this->request->params['named']['space_park_id']));
        } else {
            $this->Session->setFlash(__('Server timed out. Please try again!'),'default','error');
            $this->redirect(array('controller' => 'homes', 'action' => 'index'));
        }
        return true;
    }

/**
 * Method __organizeUserCars to format the user cars array to show it in user cars drop down
 *
 * @param $userCars array containing the unformatted user cars array
 * @return $formattedCars array containing the formatted user cars array
 */
    private function __organizeUserCars($userCars = array()) {
    	$formattedCars = array();
    	if (!empty($userCars)) {
    		$formattedCars = Hash::combine($userCars, '{n}.UserCar.id', '{n}.UserCar.registeration_number');
    	}
    	return $formattedCars;
    }

/**
 * Method addCar to add user car while booking space
 *
 * @return void
 */
    public function addCar() {
    	$this->request->data['UserCar']['user_id'] = $this->Auth->user('id');
    	
    	$this->loadModel('UserCar');
    	if ($this->UserCar->save($this->request->data)) {
    		$lastID = $this->UserCar->getLastInsertId();
    		$status = array(
    					'status' => 'success',
    					'data' => $this->request->data['UserCar']['registeration_number'],
    					'carID' => $lastID
    				);
    	} else {
    		$errors = $this->UserCar->validationErrors;
			if (!empty($errors)) {
				$status = array(
    				'status' => 'error',
    				'message' => $this->_setValidationError($errors)
    			);
			}
    	}

    	$this->set(
    		array(
    			'response' => $status,
    			'_serialize' => 'response'
    		)
    	);
    }

/**
 * Method admin_ajaxGetBookingInfo to get the Booking from booking id
 *
 * @param $bookingID the id of the particular space
 * @return void
 */
    public function admin_ajaxGetBookingInfo($bookingID = null) {
        $bookingID = urldecode(base64_decode($bookingID));
        if ($this->RequestHandler->isAjax()) {
            $bookingInfo = $this->Booking->getBookingInfo($bookingID);
            $this->set('bookingData', $bookingInfo);
            $this->set('_serialize', array('bookingData'));
        }    
    }

/**
 * Method admin_changeBookingInstallmentPaidStatus to get the Booking from booking id
 *
 * @param $bookingID the id of the particular space
 * @return void
 */
    public function admin_changeBookingInstallmentPaidStatus($dataID = null, $branchCode = null) {
        $this->autoRender = false;
        if ($this->RequestHandler->isAjax()) { 
           $this->BookingInstallment->id = $dataID;
           $data['BookingInstallment']['bank_reference_code'] = $branchCode;
           $data['BookingInstallment']['paid_to_owner'] = 1;
           if($this->BookingInstallment->save($data['BookingInstallment'])) {
            
                $response = 'Already Paid';
                 
           } else {

                $response = 'Something Went Wrong'; 
           }
           
           echo json_encode($response);
        }  
    }


/**
 * Method _getServiceTaxForBulkBookings to calculate the owner and driver service tax given booking price and commission 
 * @param spaceId
 * @param bookingPrice
 * @param commission
 * @param serviceTaxPaidByOwner 
 * @param reference to ownerServiceTax
 * @param reference to simplyParkServiceTax 
 * @return true
 * @notes : this function also sets the serviceTaxPercentage global variable
 */
 protected function _getServiceTaxForBulkBookings($spaceId,$bookingPrice,$commission,$bulkBookingSettings,$serviceTaxPaidByOwner,
                                                  &$ownerServiceTax,&$simplyParkServiceTax,&$serviceTaxPercentage) 
 {
      
        $serviceTaxPercentage = $bulkBookingSettings['BulkBookingSetting']['service_tax_percentage'];
        $simplyParkServiceTax = 0.0;
        $ownerServiceTax = 0.0;


	$this->chargeServiceTaxSimplyPark = Configure::read('Bollean.True');

        if ( $commission > 0 && $bulkBookingSettings['BulkBookingSetting']['service_tax_paid_by_simplypark'])
        {
           $simplyParkServiceTax = $this->calculateServiceTax($commission,$serviceTaxPercentage);
        }
 
        if ( $serviceTaxPaidByOwner )
        {
           $ownerServiceTax = $this->calculateServiceTax($bookingPrice,$serviceTaxPercentage);
        }

	return true;
}



/**
 * Method _getServiceTax to calculate the owner and driver service tax given booking price and commission 
 * @param spaceId
 * @param bookingPrice
 * @param commission
 * @param serviceTaxPaidByOwner 
 * @param reference to ownerServiceTax
 * @param reference to simplyParkServiceTax 
 * @return true
 * @notes : this function also sets the serviceTaxPercentage global variable
 */
 protected function _getServiceTax($spaceId,$bookingPrice,$commission,$serviceTaxPaidByOwner,
                                   &$ownerServiceTax,&$simplyParkServiceTax,&$serviceTaxPercentage) 
 {
        $this->loadModel('ServiceTaxCharge');
	$serviceTax = $this->ServiceTaxCharge->getGlobalSpaceTaxAndPaidBy();

	$globalTaxPaidBySimplyPark = $serviceTax['ServiceTaxCharge']['service_tax_paid_by_simplypark'];
        $serviceTaxPercentage = $serviceTax['ServiceTaxCharge']['service_tax_percentage'];
        $simplyParkServiceTax = 0.0;
        $ownerServiceTax = 0.0;


	$this->chargeServiceTaxSimplyPark = Configure::read('Bollean.True');

        if ( $globalTaxPaidBySimplyPark )
        {
           if ( $commission > 0 )
           {
              $simplyParkServiceTax = $this->calculateServiceTax($commission,$serviceTaxPercentage);
           }
        }
 
        if ( $serviceTaxPaidByOwner )
        {
           $ownerServiceTax = $this->calculateServiceTax($bookingPrice,$serviceTaxPercentage);
        }

	return true;
}


/**
 * Method _getSimplyParkCommissionForBulkBookings to calculate the simplypark commission 
 *
 * @return bool sp commission 
 * @notes : this function also sets the paidBy variable and sets the serviceCommissionPercentage for this sesson
 */
 protected function _getSimplyParkCommissionForBulkBookings($spaceId,$bookingPrice,$bulkBookingSettings,
                                                            &$paidBy,&$ownerPaysServiceTax) 
 {
        $commission = 0.0;
        $ownerPaysServiceTax = 0;

        $commissionPercentage = 0; 

        
        $commission           = $this->_getCommission($bulkBookingSettings['BulkBookingSetting']['commission_percentage'],
                                                             $bookingPrice);
        $paidBy               = $bulkBookingSettings['BulkBookingSetting']['commission_paid_by'];
        $ownerPaysServiceTax  = $bulkBookingSettings['BulkBookingSetting']['service_tax_paid_by_owner'];
        
	
        $this->set('commissionPercentage',$commissionPercentage);
	return ($commission);
}

/**
 * Method _getSimplyParkCommission to calculate the simplypark commission 
 *
 * @return bool sp commission 
 * @notes : this function also sets the paidBy variable and sets the serviceCommissionPercentage for this sesson
 */
 protected function _getSimplyParkCommission($spaceId,$bookingPrice,&$paidBy,&$ownerPaysServiceTax,&$isCommissionOneOff) 
 {
	$this->loadModel('SpaceChargeSetting');
	$perSpaceSettings = $this->SpaceChargeSetting->getSpaceChargeSetting($spaceId);

        $commission = 0.0;
        $ownerPaysServiceTax = 0;

        $commissionPercentage = 0; 
        $isCommissionOneOff = false;
        $maxCommissionValue = 0;

	if (empty($perSpaceSettings)) 
	{
	        $this->loadModel('ServiceTaxCharge');
                $globalSpaceCharges   = $this->ServiceTaxCharge->getGlobalSpaceCharges();
                //global settings apply
		$commission           = $this->_getCommission($globalSpaceCharges['ServiceTaxCharge']['commission_percentage'],
                                                              $bookingPrice);
                $paidBy               = $globalSpaceCharges['ServiceTaxCharge']['commission_paid_by'];
                $commissionPercentage = $globalSpaceCharges['ServiceTaxCharge']['commission_percentage'];
	} 
	else 
        {
		$commission           = $this->_getCommission($perSpaceSettings['SpaceChargeSetting']['commission_percentage'],
                                                              $bookingPrice);
                $paidBy               = $perSpaceSettings['SpaceChargeSetting']['commission_paid_by'];
                $ownerPaysServiceTax  = $perSpaceSettings['SpaceChargeSetting']['charge_service_tax_to_owner'];
                $commissionPercentage = $perSpaceSettings['SpaceChargeSetting']['commission_percentage'];
                $isCommissionOneOff   = $perSpaceSettings['SpaceChargeSetting']['is_one_time_charge'];
                $maxCommissionValue   = $perSpaceSettings['SpaceChargeSetting']['max_commission_value'];
        } 

        if($isCommissionOneOff && $commission > $maxCommissionValue)
        {
           $commission = $maxCommissionValue;
        }
	
        $this->set('commissionPercentage',$commissionPercentage);
	return ($commission);
}


/**
 * Method _getCommission to add commission to the space booking price
 *
 * @param $commissionPercentage the total commission to add to the space price
 * @return bool true
 */
protected function _getCommission($commissionPercentage = null,$bookingPrice) 
{
        $commission = ceil(($commissionPercentage/100) * $bookingPrice);

	return $commission;
}

 
 /* Method _addServiceTax to add service tax to the space price
  *
  * @param $paidBy int contains the value to determine that the tax will be paid by owner or by simplypark itself
  * @param $commission is simplypark commission on which service tax might be charged
  * @param $bookingPrice is the booking Price 
  *
 */
protected function calculateServiceTax($preTaxAmount, $serviceTaxPercentage) 
{
   return ceil( ($preTaxAmount * $serviceTaxPercentage) /100);
}


/**
 * Method saveBooking to save booking records
 *
 * @return void
 */
public function saveBooking() 
{ 
	$this->request->allowMethod('post', 'put');
	$this->_captureAmount(); //Capture amount from razor pay is booking space does not required manual approval
        $isBulk   = 0;
        $passInfo = array();

	$this->_getPrePostAndType();
	CakeLog::write('debug','Inside saveBOoking...' . print_r($this->request->data,true));
        //generate the qr code filename
        if(isset($this->request->data['Booking']['num_slots']) && $this->request->data['Booking']['num_slots'] > 1){
          //Get the pass information
          $this->loadModel('LongTermBookingPass');
          $passInfo = $this->LongTermBookingPass->getNextAvailablePass($this->request->data['Booking']['space_id']);

          if(!empty($passInfo)){
             $this->request->data['Booking']['qrcodefilename']    = $passInfo['LongTermBookingPass']['qr_file_name'];
             $this->request->data['Booking']['serial_num']        = $passInfo['LongTermBookingPass']['serial_num'];
             $this->request->data['Booking']['encode_pass_code']  = $passInfo['LongTermBookingPass']['encode_pass_code'];
             $isBulk = 1;

          }
	}else{
		$this->request->data['Booking']['qrcodefilename'] = md5($this->request->data['Transaction']['payment_id'].
                                                                        $this->request->data['Booking']['space_id'].
									$this->request->data['Booking']['user_id']).'.png';

		$this->request->data['Booking']['num_slots'] = 1;
		$transId = str_replace("pay_","SP_",$this->request->data['Transaction']['payment_id']);
                $this->request->data['Booking']['serial_num']       = $transId;
                $this->request->data['Booking']['encode_pass_code'] = $transId; 
	}
        
        CakeLog::write('debug','Inside saveBOoking..printing request' . print_r($this->request->data,true));

	$spaceType = array();
	$spaceTypeDetailsAvailable = false;
        //if booking type is HOURLY, lets set the park live apply algorithm as true
        if($this->request->data['Booking']['booking_type'] == 1){ //hourly
           $this->request->data['Booking']['park_live_apply_algorithm'] = 1;
           $this->request->data['Booking']['park_live_checked'] = 0;
        }else{
            $this->loadModel('Space');
            $spaceType = $this->Space->getSpaceType($this->request->data['Booking']['space_id']);
	    $spaceTypeDetailsAvailable = true;

	    //CakeLog::write('debug','spaceType ...' . print_r($spaceType,true));
            if($spaceType['Space']['number_slots'] > 1 && !($spaceType['Space']['property_type_id'] == Configure::read('PropertyType.PrivateResidence')
                 || $spaceType['Space']['property_type_id'] == Configure::read('PropertyType.GatedCommunity')))
            {
               $this->request->data['Booking']['park_live_apply_algorithm'] = 1;
               $this->request->data['Booking']['park_live_checked'] = 0;
            }
         } 
        	
        if ($this->Booking->saveAssociated($this->request->data)) {
		
		$getLastInsertId = $this->Booking->getLastInsertId();

		if(!$spaceTypeDetailsAvailable){
                    $this->loadModel('Space');
                    $spaceType = $this->Space->getSpaceType($this->request->data['Booking']['space_id']);
		}

                //generate a sync ID and send a GCM update 
                if(!($spaceType['Space']['property_type_id'] == Configure::read('PropertyType.PrivateResidence')  || 
                   $spaceType['Space']['property_type_id'] == Configure::read('PropertyType.GatedCommunity')))
                {
                   $this->loadModel('SyncUpdate');
                   $syncId = $this->SyncUpdate->generateSyncId($this->request->data['Booking']['space_id'],
			                                       $getLastInsertId,
			                                       Configure::read('UpdateType.simplypark_booking_update'));  
		   //notify other app 
		   $spaceId = $this->request->data['Booking']['space_id'];
	           $this->loadModel('WidgetToken');
	           $tokenList = $this->WidgetToken->getAllAppTokens($spaceId);
	   
		   if(!empty($tokenList)){
		      $actionSpecificParams['action']         = Configure::read('Action.simplypark_booking_update');
		      $actionSpecificParams['serialNumber']   = $this->request->data['Booking']['serial_num']; 
		      $actionSpecificParams['encodedPassCode'] = $this->request->data['Booking']['encode_pass_code']; 
		      $actionSpecificParams['customer']       = $this->request->data['Booking']['first_name']. ' ' . $this->request->data['Booking']['last_name'];
                      $actionSpecificParams['bookingType']    = $this->request->data['Booking']['booking_type'];    
		      $actionSpecificParams['startTime']      = $this->request->data['Booking']['start_date'];
		      $actionSpecificParams['endTime']        = $this->request->data['Booking']['end_date'];
		      $actionSpecificParams['numBookedSlots'] = $this->request->data['Booking']['num_slots'];
		      $actionSpecificParams['syncId']         = $syncId;
		      $actionSpecificParams['spaceOwnerId']   = $this->request->data['Booking']['space_user_id'];
		      $actionSpecificParams['status']         = Configure::read('BookingStatus.Approved');

		     $this->notifyAllApps($spaceId,$tokenList,$actionSpecificParams);
		   }
                }
                $transId = str_replace('pay_','SP_',$this->request->data['Transaction']['payment_id']);
       
		if (isset($this->request->data['Booking'][Configure::read('BookingTableStatusField')])) {
			$this->_alertBookingEmail('automatic_booking_approval_email_to_owner',
                                                   $transId,
                                                   $this->request->data['Booking']['owner_income'],
                                                   $this->request->data['Booking']['booking_type'],
                                                   $this->request->data['Booking']['email'],
                                                   $this->request->data['Booking']['space_user_id']);
			$this->_confirmBookingEmail($this->request->data['Booking']['qrcodefilename']);
		} else {
			if (Configure::read('SMSNotifications')) {
				//send message to owner to approve/disapprove booking
				$this->__smsOwnerToApproveDisapproveBooking($this->request->data['Booking']['space_user_id']);

				//send message to driver to pending approval booking
				$this->__smsDriverToPendingApproval($this->request->data['Booking']['user_id']);
			}
			$this->_alertBookingEmail('alert_booking',
                                                   $transId,
                                                   $this->request->data['Booking']['owner_income'],
                                                   $this->request->data['Booking']['booking_type'],
                                                   $this->request->data['Booking']['email'],
                                                   $this->request->data['Booking']['space_user_id']);
		}


		$getLastInsertId = urlencode(base64_encode($getLastInsertId));
		$this->redirect(array('action' => 'congratulation/'.$getLastInsertId));
	}
}

/**
 * Method __smsOwnerToApproveDisapproveBooking to send message to owner mobile to approve or disapprove the booking
 * if booked space required approval from owner
 *
 * @param $ownerUserID int the user id of the owner to get mobile number
 * @return bool
 */
    private function __smsOwnerToApproveDisapproveBooking($ownerUserID = null) {
        $ownerMobileNumber = $this->UserProfile->getUserName($ownerUserID); //getting user mobile number

        //getting message body
        $messageContent = $this->Message->getMessage(Configure::read('MessageId.approve_disapprove_booking'));

        $messageResponse = $this->_sendMessageNotification($ownerMobileNumber['UserProfile']['mobile'],$messageContent['Message']['body']);

        if ($messageResponse->code == Configure::read('HTTPStatusCode.OK')) {
            return true;
        }
        return false;
    }

/**
 * Method __smsDriverToPendingApproval to send message to driver mobile to intimate him/her that
 * your booking is in pending approval mode
 *
 * @param $userID int the user id of the driver to get mobile number
 * @return bool
 */
    private function __smsDriverToPendingApproval($userID = null) {
        $driverMobileNumber = $this->UserProfile->getUserName($userID); //getting user mobile number

        //getting message body
        $messageContent = $this->Message->getMessage(Configure::read('MessageId.booking_request_pending'));

        $messageResponse = $this->_sendMessageNotification($driverMobileNumber['UserProfile']['mobile'],$messageContent['Message']['body']);

        if ($messageResponse->code == Configure::read('HTTPStatusCode.OK')) {
            return true;
        }
        return false;
    }

/**
 * Method _getPrePostAndType to get pre start cancel date and
 * post start cancel date and booking type to save in bookings table.
 *
 * @return bool true
 */
    protected function _getPrePostAndType() {
        $preCancelDate = $this->_calculatePreStartCancellationDeadline($this->request->data['Booking']['start_date']);
        $bookingType = $this->_getBookingType($this->request->data['Booking']['start_date'],$this->request->data['Booking']['end_date']);
        $postCancelDate = $this->_calculatePostStartCancellationDeadline($preCancelDate,$this->request->data['Booking']['end_date'],$bookingType);
        $this->_calculateEntryThresholds($this->request->data['Booking']['start_date'],
                                         $this->request->data['Booking']['end_date'],
                                         $preEntryThreshold,$postEntryThreshold);
        
        $this->request->data['Booking']['pre_start_cancellation_deadline'] = $preCancelDate;
        $this->request->data['Booking']['post_start_cancellation_deadline'] = $postCancelDate;
        $this->request->data['Booking']['booking_type'] = $bookingType;

        $this->request->data['Booking']['pre_entry_datetime'] = $preEntryThreshold;
        $this->request->data['Booking']['post_entry_datetime'] = $postEntryThreshold;
        return true;
    }

/**
 * Method _captureAmount to capture amount if booking space does not require manual approval
 *
 * @return bool
 */
    protected function _captureAmount() {
        if (isset($this->request->data['Booking'][Configure::read('BookingTableStatusField')]) && $this->request->data['Booking']['booking_price'] > 0) {
            $paymentID = $this->request->data['Transaction']['payment_id'];

            CakeLog::write('debug','++++++++Amount to capture ' . $this->request->data['Booking']['amount_to_capture']);
            if ($this->_hitToCaptureAmount($paymentID, $this->request->data['Booking']['amount_to_capture'])) {
                $this->request->data['Booking'][Configure::read('BookingTableStatusField')] = Configure::read('Bollean.True');
            } else {
                $this->Session->setFlash(__('Your payment didn\'t proceed successfully. Please try again!'),'default','error');
                $this->redirect(
                array(
                        'action' => 'index',
                        'stdate' => urlencode(base64_encode($this->request->data['Booking']['start_date'])),
                        'eddate' => urlencode(base64_encode($this->request->data['Booking']['end_date'])),
                        'space_id' => urlencode(base64_encode($this->request->data['Booking']['space_id']))
                    )
                );
            }
        }
        return true;
    }

/**
 * Method _hitToCaptureAmount common function to hit razor pay server to capture booking amount to merchant's account
 *
 * @param $paymentID string the payment id for which the razor pay will transfer the amount
 * @param $amount float total amount to capture
 * @return bool
 */
    protected function _hitToCaptureAmount($paymentID = null, $amount = null) {
        $amount = array('amount' => $amount*100);

        $socket = new HttpSocket();
        $socket->configAuth('Basic', Configure::read('RazorPayKeys.KeyID'), Configure::read('RazorPayKeys.KeySecret'));
        $payment = $socket->post(Configure::read('RazorPayKeys.APIURL').$paymentID.'/capture',$amount);
        if ($payment->code == Configure::read('HTTPStatusCode.OK')) {
            return true;
        }
       else{
               CakeLog::write('error','In hitToCaptureAmount:: Payment for payment ID' . $paymentID .' not successful with code::' . $payment->code);
               
        }
        return false;
    }

/**
 * Method congratulation to show user after successfully booking space
 *
 * @param $bookingID int the id of the booking
 */
    public function congratulation($bookingID = null) 
    {
        $this->layout = 'home';
        $bookingID = base64_decode(urldecode($bookingID));
        $getBookingRecord = $this->Booking->getBookingRecordCongratulationPage($bookingID);

	if($getBookingRecord['Booking']['num_slots'] > 1){
		//qr codes are pre generated
	}else{
		//CakeLog::write('debug','Inside congratulation ..' . print_r($getBookingRecord,true));

		//generate the QR code here
		$this->_generateQrCode($getBookingRecord,$bookingID);
	}
        $this->set('getBookingRecord', $getBookingRecord);
    }

/**
 * Method _generateQrCode to generate QR code based on the following parameters
 *      - User Name
 *      - Booking Start Date Time
 *      - Booking End Date Time
 *      - Booking ID
 *      - Space Name
 *      - Car Registration Number
 * @return void
 */
private function _generateQrCode($bookingRecord,$bookingID) 
{
   /*
   $qrCodeContents  = "Name: " . $bookingRecord['Booking']['first_name'] . " " . $bookingRecord['Booking']['last_name'] . "\r\n";
   $qrCodeContents .= "Booking Time: " . $bookingRecord['Booking']['start_date'] . " to " . $bookingRecord['Booking']['end_date'] . "\r\n";
   $transId = str_replace('pay_','SP_',$bookingRecord['Transaction']['payment_id']);

   $qrCodeContents .= "BookingID: " . $transId . "\r\n";
   $qrCodeContents .= "Venue: " . $bookingRecord['Space']['name'] . "\r\n";

   if($bookingRecord['Booking']['num_slots'] == 1) 
   {
      $qrCodeContents .= "Car: " . $bookingRecord['UserCar']['registeration_number'];
   }
   */
   $qrCodeContents = $this->getQrCodeContent($bookingID,$bookingRecord['Transaction']['payment_id']);


   //CakeLog::write('debug','Inside _generate QrCode ' . print_r($bookingRecord,true));
   $absoluteFileName = Configure::read('QrImagePath').$bookingRecord['Booking']['qrcodefilename'];

   //CakeLog::write('debug','File name of QR Code Reader' . $absoluteFileName);

   QRcode::png($qrCodeContents, $absoluteFileName);


}

public function getQrCodeContent($bookingId,$paymentId)
{
   $transId = str_replace('pay_','SP_',$paymentId);
   $qrCodeContents = $bookingId . "/" . $transId;

   return $qrCodeContents;
}


/**
 * Method log_entry_exit : This function will log a car entry or exit . 
 *                          This function would be called by the scan
 *                          of the booking pass
 *      @param space_id
 *      @param- booking_id
 *      @param trans_id
 * @return void
 */
public function log_entry_exit($lang="eng",$spaceId,$bookingId,$transId,$allowEarlyEntry=0) 
{
   Configure::write('Config.language', $lang);
   $status = Configure::read('BookingValidationStatus.validBooking');
   $this->layout = 'print_layout';
   $this->set('spaceId',$spaceId);
   $this->set('bookingId',$bookingId);
   $this->set('transId',$transId);
   $this->set('lang',$lang);

   //CakeLog::write('debug','Inside log_entry_exit..' . $lang . ' ' . $spaceId . ' ' . $bookingId . '  ' . $transId . ' ' . $allowEarlyEntry);
  
   /*get the space Address if it already exists
   $spaceAddress = $this->spaceAddressCache[$spaceId];
   CakeLog::write('debug','$spaceAddress' . $spaceAddress); 

   if(empty($spaceAddress)){
     CakeLog::write('debug','Reteriving space address from DB'); 
     $this->loadModel('Space');
     $spaceAddress = $this->Space->getFormattedSpaceAddress($spaceId); 
     $this->spaceAddressCache[$spaceId] = $spaceAddress;
   }

   CakeLog::write('debug','$spaceAddress' . $spaceAddress); 
   */
   if($transId == "OFFLINEUSERENTRY") 
   {
      $this->loadModel('CurrentSlotOccupancy');
      $this->CurrentSlotOccupancy->incrementSlotInUse($spaceId);

      $this->set('entryOrExit',Configure::read('BookingEntryOrExit.entry'));
      $this->set('offlineUser',1);
      $this->set('result', Configure::read('BookingValidationStatus.validBooking'));
      //$this->set('spaceAddress',$spaceAddress);
      $this->set('time', date("j-M g:i a"));

   }else if($transId == "OFFLINEUSEREXIT") {
      $this->loadModel('CurrentSlotOccupancy');
      if($this->CurrentSlotOccupancy->decrementSlotInUse($spaceId) >= 0) {
         $this->set('entryOrExit', Configure::read('BookingEntryOrExit.exit'));
         $this->set('result', Configure::read('BookingValidationStatus.validBooking'));
      }else {
         $this->set('entryOrExit', Configure::read('BookingEntryOrExit.exitWithoutEntry'));
         $this->set('result', Configure::read('BookingValidationStatus.exitWithoutEntry'));
      }
      $this->set('time', date("j-M g:i a"));
      $this->set('offlineUser', 1);
      //$this->set('spaceAddress',$spaceAddress);
   }
   else {
           $this->set('offlineUser', 0);
           $this->set('isHourlyBooking', true);
	   $this->set('transId', $transId);

           $carReg  = "";
           
           /* step 1: first see if this is entry or exit */
           $this->loadModel('EntryExitTime');
           $isEntry = $this->EntryExitTime->isEntryOrExit($bookingId,$spaceId,$transId);
           //CakeLog::write('debug','After isEntryOrExit..' . $isEntry);

           if($isEntry == Configure::read('BookingEntryOrExit.entry')){ 
              // step 1.1 : if entry, validate the booking
	      if($this->Booking->validateBooking($spaceId,$bookingId,$transId,$status,$bookingInfo)) 
               {
	         if($status == Configure::read('BookingValidationStatus.validBooking') ||
                    ($status == Configure::read('BookingValidationStatus.tooEarly') && $allowEarlyEntry))		  
                  {
                     //CakeLog::write('debug','After isEntryOrExit..' . $isEntry);
		     $this->loadModel('EntryExitTime');
                     $isEarlyEntry = 0;
                     if($status == Configure::read('BookingValidationStatus.tooEarly')){
                        $isEarlyEntry = 1;
                        $status = Configure::read('BookingValidationStatus.validBooking');
                     }

		     $this->EntryExitTime->logEntry($bookingId,$spaceId,$transId,$bookingInfo['Booking']['booking_type'],$isEarlyEntry);

                     if($bookingInfo['Booking']['booking_type'] != 1) {//not hourly booking
                         $this->set('isHourlyBooking', false);
                         $this->set('longBookingDisplayString', 'Please issue a parking sticker/pass to the User');
                     }else{
		        $this->loadModel('CurrentSlotOccupancy');
		        $this->CurrentSlotOccupancy->incrementSlotInUse($spaceId);
                     }

		     $carReg = $bookingInfo['UserCar']['registeration_number']; //TODO: look for bulk booking CAR REG number
      
                     $this->set('time', date("j-M g:i a"));
		   }
	       }
              $this->set('result',$status); 
              $this->set('entryOrExit', Configure::read('BookingEntryOrExit.entry'));
            }
          else if($isEntry == Configure::read('BookingEntryOrExit.exit')) {
               $bookingInfo = array();
               
               $this->loadModel('EntryExitTime');
               if($this->EntryExitTime->logExit($bookingId,$spaceId,$transId,$bookingInfo) == Configure::read('BookingValidationStatus.validBooking'))
               {
                  if($bookingInfo['Booking']['booking_type'] == 1){
		     $this->loadModel('CurrentSlotOccupancy');
		     $this->CurrentSlotOccupancy->decrementSlotInUse($spaceId);
                   }
		  /* find the time difference between original booking */
                   
                   $bookingStartTimeObj = new DateTime($bookingInfo['Booking']['start_date']); 
                   $bookingEndDateTimeObj = new DateTime($bookingInfo['Booking']['end_date']);
                   $nowDateTimeObj        = new DateTime();
                   $nowDateTimeObj->setTime($nowDateTimeObj->format("H"),$nowDateTimeObj->format("i"),0);
                   
                   if($bookingInfo['EntryExitTime']['is_entry_early'])
                   {
                      $bookingDuration = $bookingStartTimeObj->diff($bookingEndDateTimeObj);
                      //Cakelog::write('debug','User Entered early(DURATION)' . print_r($bookingDuration,true));
                      $bookingStartTimeObj = new DateTime($bookingInfo['EntryExitTime']['entry_time']);
                      //Cakelog::write('debug','User Entered early....' . print_r($bookingStartTimeObj,true));
                      $bookingEndDateTimeObj = clone $bookingStartTimeObj;
                      $bookingEndDateTimeObj->add($bookingDuration);
                      //Cakelog::write('debug','User Entered early....' . print_r($bookingStartTimeObj,true) . ' ' . print_r($bookingEndDateTimeObj,true));
                   }    
                   
                   if($nowDateTimeObj > $bookingEndDateTimeObj)
                    {
                      $displayString = "User overstayed for ";
                      $comma = '';
                      $diffInterval = $bookingEndDateTimeObj->diff($nowDateTimeObj);

                      //CakeLog::write('debug','DiffInterval ..' . print_r($diffInterval,true));

                      $diffBitMap = 0;
                      $yearlyBit = 1;
                      $monthlyBit = 2;
                      $dailyBit = 4;
                      $hourlyBit = 8;
                      $minuteBit = 16;

                      if($diffInterval->y) {
                         $displayString .= $diffInterval->y . ' years';
                         $comma = ' , ';
                         $diffBitMap = $yearlyBit;
                      }
                      
                      if($diffInterval->m) {
                         $displayString .= $comma . $diffInterval->m . ' months';
                         $comma = ' , ';
                         $diffBitMap |= $monthlyBit;
                      }
                      
                     if($diffInterval->d) {
                         $displayString .= $comma . $diffInterval->d . ' days';
                         $comma = ' , ';
                         $diffBitMap |= $dailyBit;
                      }
                     
                     if($diffInterval->h) {
                         $displayString .= $comma . $diffInterval->h . ' hours';
                         $comma = ' , ';
                         $diffBitMap |= $hourlyBit;
                      }
                     
                     if($diffInterval->i) {
                         $displayString .= $comma . $diffInterval->i . ' minutes';
                         $comma = ' , ';
                         $diffBitMap |= $minuteBit;
                     }
                     //CakeLog::write('debug','OverDue Payment ..' . print_r($bookingInfo,true));
                     $paymentDue = 0;
                     if($bookingInfo['EntryExitTime']['booking_type'] == 1) 
                     { //we only calculate overdue payment for hourly bookings

                          $paymentDue = $this->getPaymentDue($bookingInfo,$diffInterval,$nowDateTimeObj);

                          if($paymentDue == 0 ){
                            //this is possible in case of tier pricing where 0-4 hours charge is 20 
                            //and the user booked for 2 hours stayed 1 hour extra. no payment is due
                            $status = Configure::read('BookingValidationStatus.validBooking');
                          }else{
                            $status = Configure::read('BookingValidationStatus.paymentDue');
                            $this->loadModel('EntryExitTime');
                            $this->EntryExitTime->updatePaymentInfo($bookingInfo['EntryExitTime']['id'],$paymentDue,$displayString);
                          }
                     }
                        
                     $this->set('overstayString', $displayString);
                     $this->set('paymentDue', $paymentDue);
 
                  }/*user overstayed */
		   

              }/*record Exit */
                 
                $this->set('time', date("j-M g:i a"));
                $this->set('result', $status);
                $this->set('entryOrExit', Configure::read('BookingEntryOrExit.exit'));
                $this->set('isHourlyBooking', false);
              
           }else if($isEntry == Configure::read('BookingEntryOrExit.reEntry')){
               // validate rentry
                if($this->Booking->validateReEntry($spaceId,$bookingId,$transId,$status,$bookingInfo) &&
	           ($status == Configure::read('BookingValidationStatus.validBooking'))) 
                {
		     $this->loadModel('EntryExitTime');
		     $this->EntryExitTime->updateEntry($bookingId,$spaceId,$transId,Configure::read('BookingEntryOrExit.entry'));
                     
                     //as per design, we will not increment the slot usage as long term bookings are considered as RESERVED. 
                     //The slot usage is already incremented till the time the booking ends

		     $carReg = $bookingInfo['UserCar']['registeration_number']; //TODO: look for bulk booking CAR REG number

                 }
              
                //CakeLog::write('debug','status after reEntry ' . $status); 
                $this->set('result',$status); 
                $this->set('entryOrExit',Configure::read('BookingEntryOrExit.reEntry'));
                $this->set('isHourlyBooking' ,false);
                $this->set('longBookingDisplayString',"Please confirm the parking sticker/pass issued to the User");
                $this->set('time', date("j-M g:i a"));

           }else if($isEntry == Configure::read('BookingEntryOrExit.alreadyUsed')) {
                $this->loadModel('EntryExitTime');
                $bookingInfo = $this->EntryExitTime->getBookingInfo($bookingId,$spaceId,$transId);

                $this->set('result', Configure::read('BookingValidationStatus.exitAlreadyRecorded'));
                $this->set('entryTime',date("j-M g:i a",strtotime($bookingInfo['EntryExitTime']['entry_time'])));
                $this->set('exitTime',date("j-M g:i a",strtotime($bookingInfo['EntryExitTime']['entry_time'])));
                $carReg = $bookingInfo['Booking']['UserCar']['registeration_number'];

           }

      $preEntryDateTime = "";
      $postEntryDateTime = "";

      if(isset($bookingInfo['Booking']['pre_entry_datetime'])){
         $preEntryDateTime = date("j-M g:i a",strtotime($bookingInfo['Booking']['pre_entry_datetime']));
      }
      
      if(isset($bookingInfo['Booking']['post_entry_datetime'])){
         $postEntryDateTime = date("j-M g:i a",strtotime($bookingInfo['Booking']['post_entry_datetime']));
      }

       //$preEntryDateTime = isset($bookingInfo['Booking']['pre_entry_datetime'])?date("j-M-y g:i",strtotime(bookingInfo['Booking']['pre_entry_datetime'])):"";
       //$postEntryDateTime = isset($bookingInfo['Booking']['post_entry_datetime'])?date("j-M-y g:i",strtotime($bookingInfo['Booking']['post_entry_datetime'])):"";
    
       $this->set('carReg', $carReg); 
       $this->set('preEntryTime',$preEntryDateTime);
       $this->set('postEntryTime',$postEntryDateTime);
       //$this->set('spaceAddress',$spaceAddress);
   }//handling of online user
       
}

/**
 * Method getPaymentDue : to calculate payment due at the end of the booking
 * @param bookingInfo : with the pricing information
 * @param diffInterval : with the overstay information
 * @return void
 */
protected function getPaymentDue($bookingInfo,$diffInterval,$nowDateTimeObj) 
{
   if($bookingInfo['Space']['tier_pricing_support']){

       $startDateTimeObj = new DateTime($bookingInfo['Booking']['start_date']);

       $diffIntervalFromBookingStartTime = $startDateTimeObj->diff($nowDateTimeObj);
       
       $numHours = $diffIntervalFromBookingStartTime->h + ($diffIntervalFromBookingStartTime->i?1:0);

       //CakeLog::write('debug','getPaymentDue ..Num Hours ' . $numHours);
       //CakeLog::write('debug','StartDateTimeoBJ ' . print_r($diffIntervalFromBookingStartTime,true));
       
       $this->loadModel('SpaceTierPricing');
       $priceToCharge = $this->SpaceTierPricing->getTierHourlyPrice($bookingInfo['Space']['id'],$numHours);
       //CakeLog::write('debug','PriceToCharge ..' . $priceToCharge);
       return ($priceToCharge - $bookingInfo['Booking']['space_price']);

   }else{
       /*TODO: Please be extra careful if we start supporting Half Hour bookings.
               In the current scenario, our bookings are done at 1 hour mark. Therefore,
               if the user goes even a little bit up, we charge the user for the whole hour.
               This logic might have to be re-thought when/if we start supporting half hour bookings
      */
       $numExtraHours = $diffInterval->h + ($diffInterval->i?1:0);
       //CakeLog::write('debug','Number of Extra Hours :: ' . $numExtraHours);
       return ($numExtraHours * $bookingInfo['Space']['SpacePricing']['hourly']);
   }
 
}

/**
 * Method _alertBookingEmail to send email after booking if space is immediate booking type
 *
 * @return void
 */
protected function _alertBookingEmail($emailTemplateID = null,$transId = null,$ownerIncome = null,$bookingType = null,$email = null,$ownerId = null) 
{

        $this->loadModel('UserProfile');
        $userDetail = $this->UserProfile->getUserName($this->Auth->user('id'));
	$spaceOwnerDetail = $this->UserProfile->getUserName($ownerId);
	$numSlotsTitleStr = "";
	$numSlotsStr = "";

        $numSlots = 1;
        if(isset($this->request->data['Booking']['num_slots'])){
         $numSlots =  $this->request->data['Booking']['num_slots'];
        }
        
        if($numSlots == 1){ 
           $this->loadModel('UserCar');
           $userCarDetail = $this->UserCar->getCarRegisterationNumber($this->request->data['Booking']['user_car_id']);
           $carRegNumber = $userCarDetail['UserCar']['registeration_number'];
           $carTypes     = Configure::read('CarTypes.'.$userCarDetail['UserCar']['car_type']);
        }else{
           $carRegNumber = "Multiple Cars will be parked"; 
	   $carTypes     = "Hatchback/SUV/MPVs";
	   $numSlotsTitleStr = "Slots booked";
	   $numSlotsStr      = $numSlots;
        }

        $spaceDetail = $this->Space->confirmBookingSpaceInfo($this->request->data['Booking']['space_id']);

        $infoMsg = '';

        //CakeLog::write('debug','Received space id ' . $this->request->data['Booking']['space_id']);
        //CakeLog::write('debug','Configured space id ' . Configure::read('TempSpaceID'));

        $spaceParkData = $this->SpacePark->findById($this->request->data['Booking']['space_park_id'],array('park_number'));
        $spaceParkNumber = !empty($spaceParkData) ? $spaceParkData['SpacePark']['park_number'] : 'N/A'; 
        $address2 = '';
        $hashString = '#PARKNUMBER';
        $hashStringReplace = 'N/A';
        
        $this->loadModel('EmailTemplate');
        
        $temp = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => Configure::read('EmailTemplateId.'.$emailTemplateID))));

      $paymentDate = new DateTime($this->request->data['Booking']['start_date']);
      $paymentDate->add(new DateInterval('P2D'));

      if($bookingType >= 4){
         $infoMsg .= 'The rental income above only reflects the first installment amount';
      }
      
      if(($this->request->data['Booking']['space_id'] == Configure::read('TempSpaceID')) ||
          ($this->request->data['Booking']['space_id'] == Configure::read('TempSpaceID1'))){
          $ownerIncome -= Configure::read('TempCommission');
          $infoMsg = '<small>A commission of Rs. '. Configure::read('TempCommission') . ' is already adjusted in this income</small>';
      }else if($this->request->data['Booking']['space_id'] == Configure::read('TempSpaceIDPercent')){
          $ownerIncome -= ceil(($ownerIncome* Configure::read('TempPerCentCommission'))/100);
          $infoMsg = '<small>A commission of '. Configure::read('TempPerCentCommission') . '% is already adjusted in this income</small>';
      }

       $temp['EmailTemplate']['mail_body'] = str_replace(
	       array('#NAME','#TRANSID', '#SPACENAME', '#STARTTIME', '#ENDTIME', '#USERNAME','#EMAIL','#CARREG', '#CARTYPE','#OWNERINCOME','#PAYMENTDATE','#INFOMSG',
                     '#NUMSLOTSTITLE','#NUMSLOTS'),
            array(
                    $spaceOwnerDetail['UserProfile']['first_name'].' '.$spaceOwnerDetail['UserProfile']['last_name'],
                    $transId,
                    $spaceDetail['Space']['name'] . ' - ' . $spaceDetail['City']['name'],
                    date('d/m/Y H:i', strtotime($this->request->data['Booking']['start_date'])),
                    date('d/m/Y H:i', strtotime($this->request->data['Booking']['end_date'])),
                    $userDetail['UserProfile']['first_name'].' '.$userDetail['UserProfile']['last_name'],
                    $email,
                    $carRegNumber,
                    $carTypes,
                    $ownerIncome,
                    $paymentDate->format('d/m/Y'),
		    $infoMsg,
		    $numSlotsTitleStr,
		    $numSlotsStr
	    ), $temp['EmailTemplate']['mail_body']);

        return $this->_sendEmailMessage($spaceDetail['User']['email'], $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject'], Configure::read('Email.EmailAdmin'));
    }

/**
 * Method _confirmBookingEmail to send email after booking is space is immediate booking type
 *
 * @return void
 */
protected function _confirmBookingEmail($qrCodeFileName=null)
 {
        $this->loadModel('UserProfile');
        $userDetail = $this->UserProfile->getUserName($this->Auth->user('id'));

        $carRegNumber = "";
        $carType      = "";
	$numSlots = 1;
	$numSlotsTitleStr = "";
	$numSlotsStr = "";
	
      
        if(isset($this->request->data['Booking']['num_slots'])){
           $numSlots = $this->request->data['Booking']['num_slots'];
	}

	$fileName = "";
      
        if($numSlots == 1){
           $this->loadModel('UserCar');
           $userCarDetail = $this->UserCar->getCarRegisterationNumber($this->request->data['Booking']['user_car_id']);
           $carRegNumber = $userCarDetail['UserCar']['registeration_number'];
	   $carType      = Configure::read('CarTypes.'.$userCarDetail['UserCar']['car_type']);
           $fileName = "https://www.simplypark.in/".Configure::read('QrImagePath').$qrCodeFileName;
        }else{
           $carRegNumber = "Multiple Cars will be parked";
           $carType      = "Hatchback/SUV/MPVs";
	   $fileName = "https://www.simplypark.in/".Configure::read('QrImagePath').'/'.$this->request->data['Booking']['space_id'].'/'.$qrCodeFileName;
	   $numSlotsTitleStr = "Number of Slots booked";
	   $numSlotsStr     = $numSlots;

        }

        $spaceDetail = $this->Space->confirmBookingSpaceInfo($this->request->data['Booking']['space_id']);
        $spaceParkData = $this->SpacePark->findById($this->request->data['Booking']['space_park_id'],array('park_number'));
        $spaceParkNumber = !empty($spaceParkData) ? $spaceParkData['SpacePark']['park_number'] : 'N/A'; 
        $address2 = '';
        $spaceAddress = '';
        if ($spaceDetail['Space']['property_type_id'] == 2 && $spaceDetail['Space']['booking_confirmation'] == Configure::read('Bollean.True')) {
            $spaceAddress = 'Car Park : <b> ' . $spaceParkNumber . '</b><br />';
        }
       
        if (!empty($spaceDetail['Space']['address2'])) {
            $address2 = $spaceDetail['Space']['address2'];
        }
        
        $spaceAddress .=  $spaceDetail['Space']['flat_apartment_number'].', '.$spaceDetail['Space']['address1'].  
                           ', ' . $address2 . ', Post Code: ' .  $spaceDetail['Space']['post_code'];

        $transId = str_replace('pay_','SP_',$this->request->data['Transaction']['payment_id']);

 
        $discount              = 0.00;
        $handlingCharges       = 0.00;
        $beforeCommissionPrice = 0.00;
        $total                 = 0.00;
       /* 
        if(isset($this->request->data['Booking']['discount_amount']))
        {
           $discount = $this->request->data['Booking']['discount_amount'];
           $handlingCharges = $this->request->data['Booking']['internet_handling_charges'];
          
           // we calculate what the handling charges were before discount 
           if($discount > $handlingCharges)
           {
              if($handlingCharges > 0)
              {
                 $handlingCharges = $discount - $handlingCharges;
              }else{
                 $handlingCharges += $discount;
              }
            }else{
               $handlingCharges += $discount;
            } 
           
           $serviceTax = 0.00;
           if($this->request->data['Booking']['service_tax_by_simplypark'] == 1)
           {
              $serviceTax = ceil(($handlingCharges * $this->request->data['Booking']['service_tax'])/100);
           }
              
           $beforeCommissionPrice = $this->request->data['Booking']['space_price'];
           $handlingCharges += $serviceTax;
           $discount = $beforeCommissionPrice + $handlingCharges - $this->request->data['Booking']['booking_price'];
        
        }else {*/
           $handlingCharges = $this->request->data['Booking']['internet_handling_charges'];
           $beforeCommissionPrice = $this->request->data['Booking']['space_price'];
        //}
 
        $total = $beforeCommissionPrice + $handlingCharges - $discount;
 
        $depositText = '&nbsp;';
        $deposit     = 0;
        $dueOnText   = '&nbsp;';
        $dueDate     = '';
        $infoMsg     = '';
        $installment = 0;
        $nextPaymentAmountText = '';

        if(isset($this->request->data['Booking']['refundable_deposit']))
        {
           $depositText = 'Deposit:';
           $deposit     = $this->request->data['Booking']['refundable_deposit'];
           $dueOnText   = 'Next Payment due: ';
           $dueDate     = $this->request->data['BookingInstallment'][1]['date_pay'];
           $infoMsg     = 'Login to see the full payment schedule';
           $installment =  $this->request->data['BookingInstallment'][1]['amount'];
           $nextPaymentAmountText = 'Amount';
        }

        $this->loadModel('EmailTemplate');
        $temp = $this->EmailTemplate->find('first', 
                                           array(
                                              'conditions' => array('EmailTemplate.id' => Configure::read('EmailTemplateId.booking_confirmation'))
                                           ));

        //CakeLog::write('debug', 'Sending confirmation email with qr image as ...' . $fileName);

        $depositAmount = ($deposit !=0)?number_format((float)$deposit,2,'.', ''):'';
	$installmentAmount = ($installment !=0)?number_format((float)$installment,2,'.', ''):'';


		$temp['EmailTemplate']['mail_body'] = str_replace(
			array('#NAME','#PRICE','#HCHARGES','#TOTAL','#TRANSID',
			'#RDEPOSIT','#DEPOSITAMOUNT','#DUEON','#DUEDATE','#NEXTPAYMENT','#INSTALLMENT','#INFOMSG',
			'#ADDRESS', '#OWNERNAME','#OWNEREMAIL','#STARTTIME', '#ENDTIME', '#CARREG','#TYPE','#QRCODESRC',
		        '#NUMSLOTSTITLE','#NUMSLOTS'),
			array(
				$userDetail['UserProfile']['first_name'].' '.$userDetail['UserProfile']['last_name'],
				number_format((float)$beforeCommissionPrice, 2, '.', ''),
				number_format((float)$handlingCharges,2,'.', ''),
				number_format((float)$total,2,'.', ''),
				$transId,
				$depositText,
				$depositAmount,
				$dueOnText,
				$dueDate,
				$nextPaymentAmountText,
				$installmentAmount,
				$infoMsg,
				$spaceAddress,          
				$spaceDetail['User']['UserProfile']['first_name'] .' '.$spaceDetail['User']['UserProfile']['last_name'] ,
				$spaceDetail['User']['email'],
				date('d/m/Y H:i', strtotime($this->request->data['Booking']['start_date'])),
				date('d/m/Y H:i', strtotime($this->request->data['Booking']['end_date'])),
				$carRegNumber,
				$carType,
				$fileName,
				$numSlotsTitleStr,
				$numSlotsStr
			), $temp['EmailTemplate']['mail_body']);

           //CakeLog::write('debug', 'Printing body ..');
           //CakeLog::write('debug', print_r($temp['EmailTemplate']['mail_body'],true));
        return $this->_sendEmailMessage($this->Auth->user('email'), $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject'], Configure::read('Email.EmailAdmin'));
    }

/**
 * Method ajaxApplyCoupon to apply discount coupon on space booking price
 *
 * @return void
 */
 public function ajaxManageDiscountCoupon() 
{
	$this->spaceID = $this->request->data['spaceID'];
	$this->startDate = $this->request->data['startDate'];
	$this->endDate = $this->request->data['endDate'];
	$this->numSlots = $this->request->data['numSlots'];
	$isBulk = $this->request->data['isBulk'];
	$propertyId = $this->request->data['propertyTypeId'];
        //CakeLog::write('debug','inside ..discount ' . $propertyId);
 


	$userID = $this->Auth->user('id');

	$this->loadModel('DiscountCoupon');
	$couponDetails = $this->DiscountCoupon->getCouponDetails($this->request->data['coupon_code']);

	$status = array(
			'Type' => Configure::read('StatusType.Error'),
			'Message' => __('Invalid coupon code. Please try again!')
		       );


        //CakeLog::write('debug','inside ajax...' . print_r($couponDetails,true));
	if (!empty($couponDetails)) 
        {
                //CakeLog::write('debug','Ajax_manage coupon ' . print_r($couponDetails,true));

		if($couponDetails['DiscountCoupon']['is_for_bulk_booking'] == 1 && $isBulk == 0 ||
		     $couponDetails['DiscountCoupon']['is_for_bulk_booking'] == 0 && $isBulk == 1) 
                {
			$status = array(
					'Type' => Configure::read('StatusType.Error'),
					'Message' => __('This voucher cannot be applied on this booking.')
                                       );
		}
              else if ($couponDetails['DiscountCoupon']['property_type_id'] != 0 &&
                       $couponDetails['DiscountCoupon']['property_type_id'] != $propertyId)
                {
                    $propertyTypeList = $this->_getPropertyTypeList();
			$status = array(
					'Type' => Configure::read('StatusType.Error'),
					'Message' => __('Voucher cannot be applied. This voucher is ONLY applicable for ' . $propertyTypeList[$couponDetails['DiscountCoupon']['property_type_id']])
                                       );
                }
                //issue 44 : temp fix. for discount code SPM300..do not allow short term bookings 
		else if ($this->request->data['coupon_code'] == 'SPM300'){
			$startDateTimeObj = new DateTime($this->startDate);
			$endDateTimeObj = new DateTime($this->endDate);

			$diffInterval = $startDateTimeObj->diff($endDateTimeObj);

			//CakeLog::write('debug','Inside discount..printing diffInterval' . print_r($diffInterval,true));

			if(($diffInterval->m == 0 && $diffInterval->y == 0)){
				$status = array(
						'Type' => Configure::read('StatusType.Error'),
						'Message' => __('This voucher can only be applied on month long or year long bookings.')
					       );
			}else{
				$status = array(
						'Type' => Configure::read('StatusType.Error'),
						'Message' => __('You can not apply same coupon code again.')
					       );

				$this->loadModel('CouponApply');
				if ($this->CouponApply->checkAlreadyApplied($userID, $couponDetails['DiscountCoupon']['id']) == Configure::read('Bollean.False')) 
				{

					$status = array(
							'Type' => Configure::read('StatusType.Error'),
							'Message' => __('Some error occurred. Please try again!')
						       );

					if($isBulk){
						$status = $this->bulkIndex(1,$couponDetails);

					}else{
						$status = $this->index(1,$couponDetails);
					}

				}

			}
		}
              else{

		         $status = array(
			 	       'Type' => Configure::read('StatusType.Error'),
				       'Message' => __('You can not apply same coupon code again.')
			       );

		         $this->loadModel('CouponApply');
		         if ($this->CouponApply->checkAlreadyApplied($userID, $couponDetails['DiscountCoupon']['id']) == Configure::read('Bollean.False')) 
                         {
                                 
			         $status = array(
					         'Type' => Configure::read('StatusType.Error'),
					         'Message' => __('Some error occurred. Please try again!')
				                );
                             
                                 if($isBulk){
                                    $status = $this->bulkIndex(1,$couponDetails);

                                 }else{
	 		            $status = $this->index(1,$couponDetails);
                                 }

		         }
              }
	}

	$this->set(
			array(
				'response' => $status,
				'_serialize' => 'response'
			     )
		  );
    if($isBulk)
    {
       $this->render('bulkindex');
    }
}


/**
 * Method approveBookingFromEmail
 *
 * @return void
 */
 public function approveBookingFromEmail() 
 {
    $this->layout = 'user';
    if (!$this->_checkIfAdminOrUser()) 
    {
       $this->Session->setFlash(__('Please login before your can approve/disapprove the booking request.'), 'default', 'error');
       $this->redirect('/?errorlogin=1&isBookingApprovalRedirect=1');
    }
    $this->redirect('listing?booking-tab=2');
 }


/**
 * Method listing to show all the booking made and booking received
 *
 * @return void
 */
 public function listing() 
 {
	$this->layout = 'user';

	$bookingMadeReceived = $this->Booking->getBookingMadeReceived();
	/* get the status of "Request for Cancellation button" */
        
	 
    $bookingsWithCancellationIndex = $this->_addCancelBookingChecks($bookingMadeReceived);

	if ( empty($bookingsWithCancellationIndex['expiredBookings']) ) { 
        $this->Booking->updateBookingRecords($bookingsWithCancellationIndex['expiredBookings'],Configure::read('BookingStatus.Expired')); 
	}

	$this->set('bookingMadeReceived', $bookingsWithCancellationIndex['allBookings']);
	//pr($bookingMadeReceived);die;
 }

/**
 * Method _addCancelBookingChecks to add the cancel booking check for each booking
 *
 * @param $bookingsMadeReceived array all made and received bookings
 * @return array containg the updated made and received arrays
 */
    protected function _addCancelBookingChecks($bookingsMadeReceived = array()) {
        $expiredBookings = array();
        $allBookings = array();
 
        foreach( $bookingsMadeReceived as $booking ) {

            $customMAnageBookings = $this->_customAddCancelAndHoverText($booking);            
            $allBookings[] = $customMAnageBookings['Booking'];
            $expiredBookings[] = $customMAnageBookings['expiredBookings'];
        }
        return array(
                'expiredBookings' => $expiredBookings,
                'allBookings' => $allBookings
            );
    }

/**
 * Method _customAddCancelAndHoverText to add custom is cancel allowed status and hover text to all bookings
 *
 * @param $booking array containg the booking detail
 * @return array containing expired booking and booking with cancel and hover text information
 */
    protected function _customAddCancelAndHoverText($booking = array()) {
        $hoverText = "";
        $expiredBookings = array();

        if ($booking['Booking'][Configure::read('BookingTableStatusField')] == Configure::read('BookingStatus.Cancelled') ) { //cancelled
            $isCancelAllowed = 0;
            $hoverText = "Booking Cancelled";
        } elseif ($booking['Booking'][Configure::read('BookingTableStatusField')] == Configure::read('BookingStatus.CancellationRequested') ) { //cancelled but in notice period
            $isCancelAllowed = 0; 
            $hoverText = "Booking Cancelled. Notice Period Ongoing";
        } elseif ($booking['Booking'][Configure::read('BookingTableStatusField')] == Configure::read('BookingStatus.Expired') ) { //expired
            $isCancelAllowed = 0;
            $hoverText = "Booking Expired"; 
        } elseif ($booking['Booking'][Configure::read('BookingTableStatusField')] == Configure::read('BookingStatus.DisApproved') ) { //disapproved
            $isCancelAllowed = 0;
            $hoverText = "Booking Disapproved"; 
        } /*elseif ($booking['Booking'][Configure::read('BookingTableStatusField')] == Configure::read('BookingStatus.Waiting') ) { // waiting for approval

            $isCancelAllowed = 1;
            $hoverText = ""; 
        } */
        elseif ($booking['Booking'][Configure::read('BookingTableStatusField')] == Configure::read('BookingStatus.CancelledThroughPaymentDefault') ) { //paymentdefault
            $isCancelAllowed = 0;
            $hoverText = "Booking Cancelled"; 
        }
       else if ($booking['Booking']['booking_type'] > 3 && !$booking['Booking']['refundable_deposit']) {
            $isCancelAllowed = 0;
            $hoverText = "Online Cancellation Not possible"; 
        }
       else {
            $isExpired = 0;
            $bookingStatusText = '';
            $isCancelAllowed = $this->isCancellationAllowed($booking['Booking']['pre_start_cancellation_deadline'],
                   $booking['Booking']['post_start_cancellation_deadline'],
                   $booking['Booking']['start_date'],
                   $booking['Booking']['end_date'],
                   $booking['Booking']['booking_type'],
                   $booking['Booking'][Configure::read('BookingTableStatusField')],
                   $hoverText,
                   $isExpired,
                   $bookingStatusText,
                   $booking['Booking']['num_slots']);

            if ( $isExpired ) {
                $booking['Booking'][Configure::read('BookingTableStatusField')] = Configure::read('BookingStatus.Expired'); //update is_approved as this is now a stale value
                $expiredBookings[] = $booking['Booking']['id'];
            }else{
                $booking = array_merge($booking, array('booking_status' => $bookingStatusText));
            }
        }
        $booking = array_merge($booking, array('is_cancel_allowed' => $isCancelAllowed), array('hover_text' => $hoverText));

        return array(
                'expiredBookings' => $expiredBookings,
                'Booking' => $booking
            );

    }


/**
 * Method ajaxBookingsMade to get all the bookings made records for search functionality
 *
 * @return bool
 */
    public function ajaxBookingsMade() {
        $this->autoRender = false;
        $this->layout = '';
        $this->request->allowMethod('get');
        
        $bookingStartDateMade = $this->request->query["booking-start-date-made"];
        $bookingEndDateMade = $this->request->query["booking-end-date-made"];
        
        $bookingMade = $this->Booking->getBookingMade( $bookingStartDateMade,$bookingEndDateMade);

        $bookingsWithCancellationIndex = $this->_addCancelBookingChecks($bookingMade);

        $this->set('bookingMadeReceived',$bookingsWithCancellationIndex['allBookings']);
        $this->set(compact('bookingStartDateMade','bookingEndDateMade'));

        $this->viewPath = 'Elements' . DS . 'frontend' . DS . 'BookingSpace';
        $this->render('booking_made');

    }

/**
 * Method ajaxBookingsMade to get all the bookings made records for search functionality
 *
 * @return bool
 */
    public function ajaxBookingsReceived() {
        $this->autoRender = false;
        $this->layout = '';
        $this->request->allowMethod('get');
        
        $bookingStartDateReceived = $this->request->query["booking-start-date-recieved"];
        $bookingEndDateReceived = $this->request->query["booking-end-date-recieved"];
        
        $bookingReceived = $this->Booking->getBookingReceived( $bookingStartDateReceived,$bookingEndDateReceived);

        $bookingsWithCancellationIndex = $this->_addCancelBookingChecks($bookingReceived);

        $this->set('bookingMadeReceived',$bookingsWithCancellationIndex['allBookings']);

        $this->set(compact('bookingStartDateReceived','bookingEndDateReceived'));

        $this->viewPath = 'Elements' . DS . 'frontend' . DS . 'BookingSpace';
        $this->render('booking_received');

    }

/**
 * Method print_ticket to print all the booking details
 *
 * @return void 
 */

    public function print_ticket($bookingId = null) {
        $this->layout = 'print_layout';
        $this->request->allowMethod('get');
        if($bookingId != null) {
            $bookingId = urldecode(base64_decode($bookingId));
            $getBookingData = $this->Booking->getBookingInfo($bookingId);
            $this->set('getBookingData',$getBookingData);
        } else {
            $this->redirect(array('controller' => 'bookings','action' => 'listing'));
        }
    }

    

/**
 * Method listing to show all the booking that requires approval
 *
 * @return void
 */
    public function admin_required_approval_bookings() {
        $this->layout = 'backend';
        $searchData = array();
        if (isset($this->request->query) && !empty($this->request->query)) {
            $searchData = array(
                'OR' => array(
                    'Booking.email LIKE' => '%'. $this->request->query['search'] .'%',
                    'Booking.first_name LIKE' => '%'. $this->request->query['search'] .'%',
                    'Booking.last_name LIKE' => '%'. $this->request->query['search'] .'%',
                   
                    )
                );
        }

        $conditions = array('Booking.'.Configure::read('BookingTableStatusField') => 0);
        $conditions = array_merge($conditions,$searchData);
        $this->Paginator->settings = array(
                                        'conditions' => $conditions,
                                        'limit' => 10,
                                        'order' => 'User.created Desc',
                                        
                                    );
        $bookingsData = $this->Paginator->paginate('Booking');
        $this->set('bookingsData', $bookingsData);
        if ($this->request->is('ajax')) {
            $this->layout = '';
            $this->autoRender = false;
            $this->viewPath = 'Elements' . DS . 'backend' . DS . 'Booking';
            $this->render('required_approval_bookings_list');
        }
        //pr($bookingMadeReceived);die;
    }
/**
 * Method listing to show all the booking that are approved
 *
 * @return void
 */
    public function admin_approved_bookings() {
        $this->layout = 'backend';
        $searchData = array();
        if (isset($this->request->query) && !empty($this->request->query)) {
            $searchData = array(
                'OR' => array(
                    'Booking.email LIKE' => '%'. $this->request->query['search'] .'%',
                    'Booking.first_name LIKE' => '%'. $this->request->query['search'] .'%',
                    'Booking.last_name LIKE' => '%'. $this->request->query['search'] .'%',
                   
                    )
                );
        }

        $conditions = array('Booking.'.Configure::read('BookingTableStatusField') => 1);
        $conditions = array_merge($conditions,$searchData);
        $this->Paginator->settings = array(
                                        'conditions' => $conditions,
                                        //'limit' => 10,
                                        'order' => 'Booking.created Desc',
                                        
                                    );
        $bookingsData = $this->Paginator->paginate('Booking');
        
        $this->set('bookingsData', $bookingsData);
        if ($this->request->is('ajax')) {
            $this->layout = '';
            $this->autoRender = false;
            $this->viewPath = 'Elements' . DS . 'backend' . DS . 'Booking';
            $this->render('approved_bookings_list');
        }
    }

/**
 * Method listing to show all the booking that are disapproved
 *
 * @return void
 */
    public function admin_disapproved_bookings() {
        $this->layout = 'backend';
        $searchData = array();
        if (isset($this->request->query) && !empty($this->request->query)) {
            $searchData = array(
                'OR' => array(
                    'Booking.email LIKE' => '%'. $this->request->query['search'] .'%',
                    'Booking.first_name LIKE' => '%'. $this->request->query['search'] .'%',
                    'Booking.last_name LIKE' => '%'. $this->request->query['search'] .'%',
                   
                    )
                );
        }
        $conditions = array('Booking.'.Configure::read('BookingTableStatusField') => 2);
        $conditions = array_merge($conditions,$searchData);
        $this->Paginator->settings = array(
                                        'conditions' => $conditions,
                                        'limit' => 10,
                                        'order' => 'User.created Desc',
                                        
                                    );
        $bookingsData = $this->Paginator->paginate('Booking');
        $this->set('bookingsData', $bookingsData);
        if ($this->request->is('ajax')) {
            $this->layout = '';
            $this->autoRender = false;
            $this->viewPath = 'Elements' . DS . 'backend' . DS . 'Booking';
            $this->render('disapproved_bookings_list');
        }
    }

/**
 * Method listing to show all the booking that are cancelled
 *
 * @return void
 */
    public function admin_cancelled_bookings() {
        $this->layout = 'backend';
        $searchData = array();
        if (isset($this->request->query) && !empty($this->request->query)) {
            $searchData = array(
                'OR' => array(
                    'Booking.email LIKE' => '%'. $this->request->query['search'] .'%',
                    'Booking.first_name LIKE' => '%'. $this->request->query['search'] .'%',
                    'Booking.last_name LIKE' => '%'. $this->request->query['search'] .'%',
                   
                    )
                );
        }
        $conditions = array('Booking.'.Configure::read('BookingTableStatusField') => array(Configure::read('BookingStatus.Cancelled'),Configure::read('BookingStatus.CancellationRequested')),
                'Booking.last_date_after_cancellation !=' => null,
                'DATE_ADD(Booking. last_date_after_cancellation,INTERVAL 24 HOUR) <=' => date('Y-m-d H:i:s'),
                'Booking.is_deposit_refunded' => 0,
                
                );
        $conditions = array_merge($conditions,$searchData);
        $this->Paginator->settings = array(
                                        'conditions' => $conditions,
                                        'limit' => 10,
                                        'order' => 'Booking.created Desc',
                                        
                                    );
        
        
        $bookingsData = $this->Paginator->paginate('Booking');
        $this->set(compact('bookingsData'));
        if ($this->request->is('ajax')) {
            $this->layout = '';
            $this->autoRender = false;
            $this->viewPath = 'Elements' . DS . 'backend' . DS . 'Booking';
            $this->render('cancelled_bookiings');
        }
    }

/**
 * Method ajaxApproveSpace to approve booking space by owner
 *
 * @return bool
 */
    public function ajaxApproveSpace() {
        $status = array(
                        'Type' => Configure::read('StatusType.Error'),
                        'Message' => __('Approval failed. Please try again!')
                    );
        $razorCaptureStatus = 1;
        
        
        
        if ($this->request->data['payment_id'] != 'pay_FREE') 
        {
            $razorCaptureStatus = $this->_hitToCaptureAmount($this->request->data['payment_id'],$this->request->data['payment_amount']);
        }

        if ($razorCaptureStatus) {
            $bookingID = urldecode(base64_decode($this->request->data['booking_id']));
            if (
                    $this->Booking->updateAll(
                                array(
                                        'Booking.'.Configure::read('BookingTableStatusField') => Configure::read('SaveBookingStatus.Approved')
                                ),
                                array(
                                        'Booking.id' => $bookingID
                                    )
                                )
                ) {
                $bookingDetail = $this->Booking->getBookingDetailApprovalEmail($bookingID);

                $customBookingHandle = $this->_customAddCancelAndHoverText($bookingDetail);
                $this->_sendApprovalDisApprovalEmail($bookingDetail,$this->request->data['payment_id']);

                if (Configure::read('SMSNotifications')) {
                    //message intimation to driver that their space has been approved at simply park
                    $this->__smsDriverApproveBooking($bookingDetail['User']['id']);
                }

                $status = array(
                        'Type' => Configure::read('StatusType.Success'),
                        'BookingID' => $bookingID,
                        'StartDate' => date('d F', strtotime($bookingDetail['Booking']['start_date'])),
                        'is_cancel_allowed' => $customBookingHandle['Booking']['is_cancel_allowed'],
                        'hover_text' => $customBookingHandle['Booking']['hover_text'],
                        'Message' => __('Space approved!')
                    );
            }
           else {
                CakeLog::write('error','updateAll Retured false');
            }
        }
        $this->set(
                array(
                        'response' => $status,
                        '_serialize' => 'response'
                    )
            );
    }

/**
 * Method __smsDriverApproveBooking to send message to driver's mobile that
 * his booking has been approved by owner
 *
 * @param $ownerUserID int the user id of the owner to get mobile number
 * @return bool
 */
    private function __smsDriverApproveBooking($userID = null) {
        $ownerMobileNumber = $this->UserProfile->getUserName($userID); //getting user mobile number

        //getting message body
        $messageContent = $this->Message->getMessage(Configure::read('MessageId.booking_confirmed'));

        $messageResponse = $this->_sendMessageNotification($ownerMobileNumber['UserProfile']['mobile'],$messageContent['Message']['body']);

        if ($messageResponse->code == Configure::read('HTTPStatusCode.OK')) {
            return true;
        }
        return false;
    }

/**
 * Method _sendApprovalEmail to send approval email to the user who booked the space
 *
 * @return bool
 */
    protected function _sendApprovalDisApprovalEmail($bookingDetail = array(), $transactionId, $disApprove = false) {

        $address2 = '';
        $transId = "Not Applicable";

        if (!empty($bookingDetail['Space']['address2'])) {
            $address2 = $bookingDetail['Space']['address2'].', ';
        }

        $this->loadModel('EmailTemplate');
        $transId = str_replace('pay_','SP_',$transactionId); 
        
	if ($disApprove) {
		$emailTemplateID = Configure::read('EmailTemplateId.disapprove_booking_email');
		$temp = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => $emailTemplateID)));
		$infoMsg = '';

		if($bookingDetail['Booking']['refundable_deposit'])
		{
			$infoMsg = 'excludes refundable deposit of  ' . $bookingDetail['Booking']['refundable_deposit'];
		}

		$temp['EmailTemplate']['mail_body'] = str_replace(
				array('#NAME', '#SPACENAME', '#STARTTIME', '#ENDTIME','#TRANSID',
					'#BOOKINGAMOUNT','#INFOMSG'),
				array(
					$bookingDetail['User']['UserProfile']['first_name'].' '.$bookingDetail['User']['UserProfile']['last_name'],
					$bookingDetail['Space']['name'] . ' - ' . $bookingDetail['Space']['City']['name'],
					date('m/d/Y H:i', strtotime($bookingDetail['Booking']['start_date'])),
					date('m/d/Y H:i', strtotime($bookingDetail['Booking']['end_date'])),
					$transId,
					$bookingDetail['Booking']['booking_price'],
					$infoMsg,
				     ), $temp['EmailTemplate']['mail_body']);
	}else{
		$emailTemplateID = Configure::read('EmailTemplateId.approve_booking_email');
		$spaceParkNumber = !empty($bookingDetail['SpacePark']['park_number']) ? $bookingDetail['SpacePark']['park_number'] : 'N/A';
		$spaceAddress = '';

		if ($bookingDetail['Space']['property_type_id'] == 2 && 
				$bookingDetail['Space']['booking_confirmation'] == Configure::read('Bollean.false')) 
		{
			$spaceAddress = 'Car Park: '. '<b>'.$spaceParkNumber. '</b>' . '<br />';  
		}

		$spaceAddress .= $bookingDetail['Space']['flat_apartment_number'].', '.
			$bookingDetail['Space']['address1'].', '.
			$address2.
			$bookingDetail['Space']['post_code'];

		$temp = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => $emailTemplateID)));
		$infoMsg = '';

		if($bookingDetail['Booking']['refundable_deposit'])
		{
			$infoMsg = 'A refundable deposit of ' . $bookingDetail['Booking']['refundable_deposit'] . ' is also collected with this booking';
		}

		$temp['EmailTemplate']['mail_body'] = str_replace(
				array('#NAME', '#SPACENAME', '#ADDRESS', '#STARTTIME', '#ENDTIME', '#CARREG','#CARTYPE','#TRANSID',
                                      '#BOOKINGAMOUNT','#INFOMSG','#OWNERNAME','#OWNEREMAIL'),
				array(
					$bookingDetail['User']['UserProfile']['first_name'].' '.$bookingDetail['User']['UserProfile']['last_name'],
					$bookingDetail['Space']['name'] . ' - ' . $bookingDetail['Space']['City']['name'],
					$spaceAddress,
					date('m/d/Y H:i', strtotime($bookingDetail['Booking']['start_date'])),
					date('m/d/Y H:i', strtotime($bookingDetail['Booking']['end_date'])),
					$bookingDetail['UserCar']['registeration_number'],
					Configure::read('CarTypes.'.$bookingDetail['UserCar']['car_type']),
					$transId,
					$bookingDetail['Booking']['booking_price'],
					$infoMsg,
					$bookingDetail['Space']['User']['UserProfile']['first_name'] . ' ' . 
                                        $bookingDetail['Space']['User']['UserProfile']['last_name'],  
					$bookingDetail['Space']['User']['email']  
				     ), $temp['EmailTemplate']['mail_body']);
	}
        
        return $this->_sendEmailMessage($bookingDetail['User']['email'], $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject'], Configure::read('Email.EmailAdmin'));
    }

/**
 * Method ajaxDisApproveSpace to disapprove booking space by owner
 *
 * @return bool
 */
    public function ajaxDisApproveSpace() {
        $status = array(
                        'Type' => Configure::read('StatusType.Error'),
                        'Message' => __('Disapproval failed. Please try again!')
                    );
       
        $bookingID = urldecode(base64_decode($this->request->data['booking_id']));

        $dataSource = $this->Booking->getDataSource();

        try{

            $dataSource->begin();

            $checkBookings = $this->Booking->updateAll(
                                array(
                                        'Booking.'.Configure::read('BookingTableStatusField') => Configure::read('SaveBookingStatus.DisApproved')
                                ),
                                array(
                                        'Booking.id' => $bookingID
                                    )
                                );

            if (!$checkBookings) {
                throw new Exception;
            }

            $checkInstallments = $this->BookingInstallment->updateAll(
                                array(
                                        'BookingInstallment.is_deleted' => Configure::read('Bollean.True')
                                ),
                                array(
                                        'BookingInstallment.booking_id' => $bookingID
                                    )
                                );

            if (!$checkInstallments) {
                throw new Exception;
            }
            $dataSource->commit();
            $bookingDetail = $this->Booking->getBookingDetailApprovalEmail($bookingID);
            $this->_sendApprovalDisApprovalEmail($bookingDetail,$this->request->data['payment_id'],true);

            if (Configure::read('SMSNotifications')) {
                //message intimation to driver that their space has been approved at simply park
                $this->__smsDriverDisApproveBooking($bookingDetail['User']['id']);
            }
            $status = array(
                    'Type' => Configure::read('StatusType.Success'),
                    'Message' => __('Space disapproved!')
                );

        } catch (Exception $e) {

            $status = array(
                'Type' => Configure::read('StatusType.Error'),
                'Message' => __('Space is not disapproved! Please try again.')
            );

            $dataSource->rollback();
        }

        $this->set(
                array(
                        'response' => $status,
                        '_serialize' => 'response'
                    )
            );
    }

/**
 * Method __smsDriverDisApproveBooking to send message to driver's mobile that
 * his booking has been disapproved by owner
 *
 * @param $userID int the user id of the owner to get mobile number
 * @return bool
 */
    private function __smsDriverDisApproveBooking($userID = null) {
        $ownerMobileNumber = $this->UserProfile->getUserName($userID); //getting user mobile number

        //getting message body
        $messageContent = $this->Message->getMessage(Configure::read('MessageId.booking_not_confirmed'));

        $messageResponse = $this->_sendMessageNotification($ownerMobileNumber['UserProfile']['mobile'],$messageContent['Message']['body']);

        if ($messageResponse->code == Configure::read('HTTPStatusCode.OK')) {
            return true;
        }
        return false;
    }

/**
 * Method _hitToRefundAmount common function to hit razor pay server to refund booking amount to user's account
 *
 * @param $paymentID string the payment id for which the razor pay will transfer the amount
 * @param $amount float total amount to capture
 * @return bool
 */
    protected function _hitToRefundAmount($paymentID = null, $amount = null) {
        $amount = array('amount' => $amount*100);

        $socket = new HttpSocket();
        $socket->configAuth('Basic', Configure::read('RazorPayKeys.KeyID'), Configure::read('RazorPayKeys.KeySecret'));
        $payment = $socket->post(Configure::read('RazorPayKeys.APIURL').$paymentID.'/refund',$amount);
        if ($payment->code == Configure::read('HTTPStatusCode.OK')) {
            return true;
        }
        return false;
    }
   /******************************************************************
      Function Name : isCancellationAllowed 
      Description   : Given a booking id, return whether the 
                      "Request For Cancellation" button in the view
                       is enabled or disabled
      input         : $booking_id, reference to hoverText to fill
      returns       : bool true ->enable the link, disable otherwise 
      notes         : this function is being overriden to also print
                      booking status (starts on, ends on ..etc)Issue 25
   ******************************************************************/
 private function isCancellationAllowed($preStartCancellationDateTime,$postStartCancellationDateTime,$bookingStartDateTime,$bookingEndDateTime,
                                        $bookingType,$bookingStatus,
                                        &$hoverText,&$isExpired,&$bookingStatusText,$numSlots=1)
{
	$currentDateTimeObj = new DateTime(); //make sure the default_time_zone is set to Asia/Calcutta
	$preCancellationDateTimeObj = new DateTime($preStartCancellationDateTime);
	$postCancellationDateTimeObj = new DateTime($postStartCancellationDateTime);
	$endDateTimeObj = new DateTime($bookingEndDateTime);
	$bookingStartDateTimeObj = new DateTime($bookingStartDateTime);

        $isExpired = 0;

        if($currentDateTimeObj <= $bookingStartDateTimeObj)
        {
           $bookingStatusText = "Starts on " . $bookingStartDateTimeObj->format('j-M-y H:i');
        }else if($currentDateTimeObj > $endDateTimeObj){
           $bookingStatusText = "Started. Ends on " . $endDateTimeObj->format('j-M-y H:i');
        }else{
           $bookingStatusText = "Expired on " . $endDateTimeObj->format('j-M-y H:i');
        }
        

	if ( $currentDateTimeObj > $endDateTimeObj )
	{
		$hoverText = "Booking Expired. No cancellation Possible";
                $isExpired = 1;
		return 0;
	}
        else if ($numSlots > 1)
        {
                $hoverText = "Online Cancellation not possible";
                $isExpired = 0;
                return 0;
        }
	else if ( $currentDateTimeObj < $preCancellationDateTimeObj ) 
	{
		$hoverText = "Cancellation Allowed with Full Refund";
		return 1;

	}
	else if ( $currentDateTimeObj <= $bookingStartDateTimeObj )
	{
                if ($bookingStatus == Configure::read('BookingStatus.Waiting'))
                {
                       $hoverText = "Cancellation Not Possible";
                       return 0;
                }
		else if ( $bookingType < 4 ) /* short */
		{
			$hoverText = "Cancellation Not Possible";
			return 0;
		}
		else /* monthly or yearly */
		{
                        if ( $postCancellationDateTimeObj->format('Y-m-d') == $bookingStartDateTimeObj->format('Y-m-d') ) //booking is just for one month
                        {
			   $hoverText = "Cancellation Not Possible";
			   return 0;
                        }
                       else
                        {
			   $hoverText = "Cancellation Possible but Notice Period applies";
			   return 1;
                        }
		}
	}/* $currentDateTimeObj <=....*/
	else if ( $currentDateTimeObj <= $postCancellationDateTimeObj )
	{
		if ( $bookingType < 4 ) /* short */
		{
			$hoverText = "Cancellation Not Possible";
			return 0;
		}
		else /* monthly or yearly */
		{
			$hoverText = "Cancellation Possible but Notice Period applies";
			return 1;
		}
	}

	$hoverText = "No cancellation Possible";
	return 0;

}



   /******************************************************************
      Function Name : calculatePreStartCancellationDeadline
      Description   : Given a Booking Start Date in string, returns
                      the datetime object representing the datetime 
                      before which a booking can be cancelled with
                      full refund
      input         : datetime in string (YYYY-m-d H:i)
      returns       : datetime in string (YYYY-m-d H:i)
      to be used    : at the time of booking
   ******************************************************************/

     protected function _calculatePreStartCancellationDeadline($bookingStartDateTime)
     {
	     //echo "Received Start Date as " .$bookingStartDateTime ."<br />";
	     $bookingStartDateTimeObj = new DateTime($bookingStartDateTime);
	     $bookingStartDateTimeObj->sub(new DateInterval('PT48H'));

	     $preStartCancellationDateTime = $bookingStartDateTimeObj->format('Y-m-d H:i:s');

	     return $preStartCancellationDateTime;
      }  

   /******************************************************************
       Function Name : getBookingType
       Description   : Given a Booking Start Date, Booking End Date (both
                       in string), returns the booking type to be stored
                       in DB
                   
       input         : bookingStartDateTime in string (YYYY-m-d H:i)
                       bookingEndDateTime (YYYY-m-d H:i) in string
       returns       : integer representing the type of booking
                       0 -> Invalid
                       1 -> Hourly
                       2 -> Daily
                       3 -> Weekly
                       4 -> Monthly
                       5 -> Yearly
       to be used    : at the time of booking
    ******************************************************************/

  protected function _getBookingType($bookingStartDateTime,$bookingEndDateTime)
  {
	  //echo "$bookingStartDateTime ::: $bookingEndDateTime";

	  $bookingEndDateTimeObj = new DateTime($bookingEndDateTime);
	  $bookingStartDateTimeObj = new DateTime($bookingStartDateTime);

	  $interval = $bookingStartDateTimeObj->diff($bookingEndDateTimeObj);

	  $diffInDays = $interval->days;

          if ( $interval->y == 1 )
          {
             $bookingType = 5;
          }
         else if ( $interval->m >= 1 )
          {
             $bookingType = 4;
          }
	 else
	  {
		  if ( $diffInDays == 0 )
		  {
			  $bookingType = 1;
		  } 
		  else if ( $diffInDays > 0 && $diffInDays < 7 )
		  {
			  $bookingType = 2;
		  }
		  else
		  {
			  $bookingType = 3;
		  }
	  }
   

	  //echo "Number of days = $diffInDays <br />";
/*
	  if ( $diffInDays > 29 )
	  {
		  if ( $diffInDays < 365 )
		  {
			  $bookingType = 4;
		  }
		  else
		  {
			  $bookingType = 5;
		  }
	  } 
	  else
	  {
		  if ( $diffInDays == 0 )
		  {
			  $bookingType = 1;
		  } 
		  else if ( $diffInDays > 0 && $diffInDays < 7 )
		  {
			  $bookingType = 2;
		  }
		  else
		  {
			  $bookingType = 3;
		  }


	  }
*/

	  return $bookingType;
  }

   /******************************************************************
      Function Name : calculatePostStartCancellationDeadline
      Description   : Once the booking starts, the date/time returned
                      by this function is the deadline for cancellation
      input         : datetime in string (YYYY-m-d H:i:s),
                      datetime in string (YYYY-m-d H:i:s)
                      bookingType as integer
      returns       : dateTime in string (YYYY-m-d H:i:s)
      Prerequisite  : call getBookingType to get the booking type
      to be used    : at the time of booking
   ******************************************************************/

   protected function _calculatePostStartCancellationDeadline($preStartCancellationDateTime,
                                                              $bookingEndDateTime,
                                                              $bookingType)
   {

	   if ( $bookingType <  4 ) // short
	   {
		   $postStartCancellationDateTime = $preStartCancellationDateTime;

	   }
	   else 
	   {
		   $bookingEndDateTimeObj = new DateTime($bookingEndDateTime);

		   if ( $bookingType == 4 ) // monthly
		   {
			   $noticePeriod = Configure::read('MonthlyNoticePeriod');
		   }
		   else 
		   {
			   $noticePeriod = Configure::read('YearlyNoticePeriod');
		   }

		   //$bookingEndDateTimeObj->sub(new DateInterval('P' .$noticePeriod. 'D'));
		    $bookingEndDateTimeObj->sub(new DateInterval('P1M'));

		   // set Hours and minutes as 0
		   $bookingEndDateTimeObj->setTime(0,0,0); 

		   $postStartCancellationDateTime = $bookingEndDateTimeObj->format('Y-m-d H:i:s');
	   }


	   return $postStartCancellationDateTime; 


   }
   
    /******************************************************************
      Function Name : calculateEntryThresholds
      Description   : Calculates two thresholds
                       -> preEntryThreshold : time before the start time 
                          entry would be allowed. This means if the user
                          reaches the parking location earlier than his/her
                          booking and is within the preEntryThreshold, entry 
                          will be allowed
                       -> postEntryThreshold : time after which the entry won't
                          be allowed. This means if the user reaches the parking
                          location later than his/her booking is within the 
                          postEntryThreshold, entry won't be allowed
 
      input         : start datetime in string (YYYY-m-d H:i:s),
                      end datetime in string (YYYY-m-d H:i:s)
                      reference to preEntryThreshold
                      reference to postEntryThreshold
      returns       : NONE 
      Notes         : for v1, postEntryThreshold will be equal to 
                      the end datetime. This might be changed later
   ******************************************************************/

   protected function _calculateEntryThresholds($startDateTimeStr,$endDateTimeStr,
                                                &$preEntryThresholdStr,
                                                &$postEntryThresholdStr)
   {
      $preEntryThresholdObj = new DateTime($startDateTimeStr);
      $diffString = 'PT'.Configure::read('EntryThresholds.preEntryThreshold').'M';

      $preEntryThresholdObj->sub(new DateInterval($diffString));
      $preEntryThresholdStr = $preEntryThresholdObj->format('Y-m-d H:i:s');
  
      //in version 1, the post Entry threshold is the endDateTime
      $postEntryThresholdStr = $endDateTimeStr;  

   }
   
   /******************************************************************
      Function Name : cancelBooking 
      Description   : Given a booking id, return the 
                      refund amount and update the database
                      with updated
                         a) owner income & service tax
                         b) simplypark income & service tax
                         c) cancellation fees (if any)
                         d) last date of parking
      input         : $booking_id
                      $requestedBy : who requested the cancellation , 1 -> Driver, 2->Owner
      returns       : displayText : informational text to be displayed in the View
      Assumption    : cancelBooking is called only when isCancelledAllowed is True 
   ******************************************************************/
public function cancelBooking($base64BookingId,$requestedBy)
{
	$currentDateTimeObj = new DateTime(); //make sure the default_time_zone is set to Asia/Calcutta

	$bookingId = base64_decode(urldecode($base64BookingId));
	$bookingRecord = $this->Booking->getBookingRecordForCancellation($bookingId);

	$displayText="";

	//var_dump($bookingRecord);

	$preCancellationDateTimeObj = new DateTime($bookingRecord['Booking']['pre_start_cancellation_deadline']);
	$postCancellationDateTimeObj = new DateTime($bookingRecord['Booking']['post_start_cancellation_deadline']);
	$bookingStartDateTimeObj = new DateTime($bookingRecord['Booking']['start_date']);
	$bookingType = $bookingRecord['Booking']['booking_type'];
	$status = $bookingRecord['Booking'][Configure::read('BookingTableStatusField')];

        //CakeLog::write('debug','In cancelBooking ...currentDateTimeObj :: ' . print_r($currentDateTimeObj,true));
        //CakeLog::write('debug','In cancelBooking ...preCancellationDateTimeObj :: ' . print_r($preCancellationDateTimeObj,true));

	if ( $currentDateTimeObj < $preCancellationDateTimeObj ) 
	{
		$refund = $bookingRecord['Booking']['booking_price'];
		$displayText = "<p align=\"middle\">Your cancellation request has been successful. <br />"; 
		$deposit = 0;       
		$refundDate = $currentDateTimeObj->format('d-m-Y');
		$currentDateTimeObj->setTime(23,59,0);


		if ( $bookingType > 3 ) 
		{
			$deposit = $bookingRecord['Booking']['refundable_deposit'];
		}

		if ( $requestedBy == 1 ) //driver
		{
			$displayText .= "A refund of Rs.$refund ";

			if ( $deposit != 0 ) //will be 0 (in case of monthly) in case booking is for exact 1 month
			{
				$displayText .= "plus Refundable deposit of Rs.$deposit";
			}

			$displayText .= " will be debited to your account in 2 to 5 working days</p>";
		} 
		else
		{
			$displayText .= "The driver will be refunded the booking amount plus any deposit if collected </p>";
		}


		$totalRefundPaid = $refund;


		$lastDateAfterCancellation = $refundDate;
		$ownerIncomeAfterCancellation = 0;
		$ownerServiceTaxAfterCancellation = 0;
		$spIncomeAfterCancellation = 0;
		$spServiceTaxAfterCancellation = 0;
		$cancellationFees = 0;
		$lastParkingDateInString = $currentDateTimeObj->format('Y-m-d h:i:s');

		$amountToRefund = $refund + $deposit;

		if ( ($status == Configure::read('BookingStatus.Waiting')) || $amountToRefund == 0 ) //if under approval, then no need to call the refund api
		{
			$refundApiStatus = true;
		}
		else 
		{
			/* call the razor pay refund api */
			$refundApiStatus = $this->initiateRefund( $bookingRecord['Transaction']['payment_id'],$amountToRefund );
		}  

		if ( $refundApiStatus )
		{
			$oldStatus = $status;
			$status = Configure::read('BookingStatus.Cancelled'); //cancelled
			$refundStatus = 2; //paid
                        
                        $is_deposit_refunded = 1; //true
                        
                       if ( $bookingType > 3 and $deposit != 0 ) 
			{	
				//TODO: this should be part of a transaction or done using saveAll()
				$this->loadModel('BookingInstallment');
				$this->BookingInstallment->cancelAllInstallments($bookingId); 
			}
			//update the db
			$this->Booking->updateBookingRecordAfterCancellation($bookingId,
					$totalRefundPaid,$refundStatus,$status,$lastParkingDateInString,
                                        $is_deposit_refunded,$deposit,
					$ownerIncomeAfterCancellation,$ownerServiceTaxAfterCancellation,
					$spIncomeAfterCancellation, $spServiceTaxAfterCancellation,
					$cancellationFees);
 
                        /*Based on RAZORPAY docs, the credit card is charged but not transferred to merchant's account
                          From the user's point of view, manual and automatica bookings are same
			//if the booking was under approval, override the displayText completely.
			if ( $oldStatus == Configure::read('BookingStatus.Waiting') )
			{
				$displayText  = "<p align=\"middle\">Your cancellation request has been successful. <br />"; 
				$displayText .= "Since your booking request was under approval, your credit/debit card was not charged.<br/>";
				$displayText .= "No refund is due on this booking";
			}
                       */

			//send Email
	                 $this->sendCancellationEmail($bookingRecord,$oldStatus,$cancellationFees,$requestedBy);

			$this->Session->setFlash($displayText,'default','success');
		}
		else
		{
			//something has gone wrong. We shouldn't be here
			$displayText = "Unfortunately, your cancellation request could not be processed at this time. The administrator has been notified. Please try again later";

			//TODO: maybe do var_dump of start dates etc.

			$refund = -1; // an error value to check in view
			$this->Session->setFlash($displayText,'default','error');
		}
	}
        elseif ( $status == Configure::read('BookingStatus.Waiting') ) 
	{
		//something has gone wrong. We shouldn't be here
		$displayText = "Unfortunately, your cancellation request could not be processed at this time. The administrator has been notified. Please try again later";

		//TODO: maybe do var_dump of start dates etc.

		$refund = -1; // an error value to check in view
		$this->Session->setFlash($displayText,'default','error');

	}
	elseif ( $bookingType > 3 ) //if $currentDate > preCancellationDate, only long term bookings can be cancelled
	{
		$bookingEndDateTimeObj = new DateTime($bookingRecord['Booking']['end_date']);

		if ( $bookingType == 4 ) //Monthly
		{
			$noticePeriod = Configure::read('MonthlyNoticePeriod');

		}
		else
		{
			$noticePeriod = Configure::read('YearlyNoticePeriod');
		}
		$status = $this->cancelOnGoingLongBooking($bookingId,$currentDateTimeObj,$bookingStartDateTimeObj,$bookingEndDateTimeObj,
				$noticePeriod,$bookingRecord,$bookingType,
				$requestedBy, $displayText);


		if ( $status ) 
		{
			$this->Session->setFlash($displayText,'default','success');
		}
		else
		{
			$this->Session->setFlash($displayText,'default','error');
		} 

	}
	else
	{

		//something has gone wrong. We shouldn't be here
		$displayText = "Unfortunately, your cancellation request could not be processed at this time. The administrator has been notified. Please try again later";

		//TODO: maybe do var_dump of start dates etc.

		$refund = -1; // an error value to check in view
		$this->Session->setFlash($displayText,'default','error');
	} 
	$this->redirect(
           array('controller' => 'bookings', 'action' => 'listing','?' => array('booking-tab' => $requestedBy))
        );
}

 
   /******************************************************************
      Function Name : cancelOnGoingLongBooking 
      Description   :  
                      
      input         : 
      returns       : true if successful, false otherwise
      assumption    : driver pays the commission
      TODO          : implementation when owner pays the commission
   ******************************************************************/
 private function cancelOnGoingLongBooking($bookingId,$currentDateTimeObj,$bookingStartDateTimeObj,$bookingEndDateTimeObj,
                                           $noticePeriod,$bookingRecord,$bookingType,$requestedBy,&$displayText)
 {

    /*step 1: First find if the current installment has been paid*/
    $currentDate = $currentDateTimeObj->format('Y-m-d');

     $this->loadModel('BookingInstallment');
     $unPaid = $this->BookingInstallment->isAnyPreviousInstallmentsUnPaid($bookingId,$currentDate);

     if ( !empty($unPaid) )
     {
        $amount = $unPaid[0]['BookingInstallment']['amount'];
        $displayText="Our records indicate that your last/current Installment of Rs. $amount is still due.<br />
                      Cancellation can only be processed when all payments have been paid. <br />
                      Please settle all due payments first before cancelling a booking";

        return false;
     }

    /*step 2: Find the number of days difference between currentDate and next Installment date */
    $nextInstallmentDateArray = $this->BookingInstallment->getNextInstallmentDate($bookingId); //maybe passing the current date is not required

    if ( empty($nextInstallmentDateArray) )
    {
       //no installments due. we can here only if we are in the last month of the booking.
       $displayText="This booking will expire at the end of the current collection cycle and therefore cannot be cancelled.";
       return false;
    }

    $nextInstallmentDateObj = new DateTime($nextInstallmentDateArray['BookingInstallment']['date_pay']);
    $nextInstallmentDateObj->setTime(23,59,59);
    
    if ( $nextInstallmentDateObj < $currentDateTimeObj ) //something wrong
    {
	
        $displayText = "Unfortunately, your cancellation request could not be processed at this time. <br />The administrator has been notified. Please try again later";

       return false;

    }

    $diffDateTimeObj =  $currentDateTimeObj->diff($nextInstallmentDateObj);
    $daysTillNextInstallment = $diffDateTimeObj->days;
    
    $cancellationFees = 0;
    $ownerIncomeAfterCancellation = $bookingRecord['Booking']['owner_income'];
    $ownerServiceTaxAfterCancellation = $bookingRecord['Booking']['owner_service_tax_amount'];
    $spIncomeAfterCancellation = $bookingRecord['Booking']['simplypark_income'];
    $spServiceTaxAfterCancellation = $bookingRecord['Booking']['simplypark_service_tax_amount'];
    $deposit = $bookingRecord['Booking']['refundable_deposit']; 
    $depositToRefund = $deposit;

  

    if ( $daysTillNextInstallment >= $noticePeriod ) 
    {
	    //cancellation will happen at the end of the current collection cycle
	    $refundDateTimeObj = $nextInstallmentDateObj;
	    $refundDateTimeObj->setTime($bookingEndDateTimeObj->format('H'),$bookingEndDateTimeObj->format('i'),$bookingEndDateTimeObj->format('s')); 
	    $refundDateTime = $refundDateTimeObj->format('Y-m-d H:i:s');
	    $cancellationFees = 0;
	    $dateTimeToDisplay = $refundDateTimeObj->format('d-m-Y H:i');
            
            $driverEmailDisplay = "Your cancellation request has been successful.
			           Your booking will automatically cancel at the end of your payment cycle on $dateTimeToDisplay.
			           No further payments will be due on this booking. 
			           After $dateTimeToDisplay, Your deposit of Rs. $deposit will be debited to your account in 2 to 5 working days";
            $ownerEmailDisplay = "Your cancellation request has been successful. 
			          Your space will still be occupied till $dateTimeToDisplay.
			          No further payments will be received for this booking.";

	    if ( $requestedBy == 1 ) //driver
	    {
		    $displayText = "<p align=\"middle\">";
                    $displayText .= "Your cancellation request has been successful. <br/>
			             Your booking will automatically cancel at the end of your payment cycle on $dateTimeToDisplay.
			             No further payments will be due on this booking. <br />
			             After $dateTimeToDisplay, Your deposit of Rs. $deposit will be debited to your account in 2 to 5 working days";

                    $displayText .="</p>";
                    
	    }
	    else
	    {
		    $displayText  = "<p align=\"middle\">";
                    $displayText .= "Your cancellation request has been successful. <br/>
			             Your space will still be occupied till $dateTimeToDisplay.
			             No further payments will be received for this booking.";
                    $displayText .= "</p>";
	    }




	    //cancel all installments after current Date
	    $this->BookingInstallment->cancelAllInstallmentsAfterDate($bookingId,$currentDate); //maybe passing the current date is not required

	    //update the booking db
	    $this->loadModel('Booking'); 
	    $status = Configure::read('BookingStatus.CancellationRequested'); //cancellation requested in notice period
	    $refundStatus = 1; //unpaid
	    $totalRefundPaid = 0; //only deposit needs to be refunded
	    $this->Booking->updateBookingRecordAfterCancellation($bookingId,
			    $totalRefundPaid,$refundStatus,$status,$refundDateTime,
			    0,$depositToRefund,
			    $ownerIncomeAfterCancellation,$ownerServiceTaxAfterCancellation,
			    $spIncomeAfterCancellation, $spServiceTaxAfterCancellation,
			    $cancellationFees);
	    //send Email
	    $this->sendLongBookingCancellationEmail($bookingRecord,$bookingId,
			                            $currentDateTimeObj,$dateTimeToDisplay,
			                            $deposit,$requestedBy,$driverEmailDisplay,$ownerEmailDisplay,0,0);       

	    return true;
    }
   else
    {
       $daysInMonth = 30; //maybe read from Configuration file 

       
       //last day of parking will be in the next installment cycle
       $daysUsedInNextMonth = $noticePeriod - $daysTillNextInstallment; 
       $nextInstallmentDate = $nextInstallmentDateObj->format('Y-m-d'); 
       $lastParkingDateTimeObj = new DateTime("+$daysUsedInNextMonth day $nextInstallmentDate");

       $lastParkingDateTimeObj->setTime($bookingEndDateTimeObj->format('H'),$bookingEndDateTimeObj->format('i'),$bookingEndDateTimeObj->format('s'));
       
       $lastParkingDateTime = $lastParkingDateTimeObj->format('Y-m-d H:i:s');
       $dateTimeToDisplay = $lastParkingDateTimeObj->format('d-m-Y H:i');
       

       $handlingCharges = $bookingRecord['Booking']['internet_handling_charges'];
       
       /* get amount paid service tax etc */ 
       $spacePrice = $bookingRecord['Booking']['space_price'];
  
       $parkingCostForUsedDays = round((($spacePrice/$daysInMonth) * $daysUsedInNextMonth),2);
       $ownerServiceTaxForUsedDays = 0;
       $ownerPaysServiceTax = $bookingRecord['Booking']['service_tax_by_owner'];
       $serviceTax = $bookingRecord['Booking']['service_tax'];

       if ( $ownerPaysServiceTax )
       {
          $ownerServiceTaxForUsedDays = round((($parkingCostForUsedDays * $serviceTax)/100),2);
       }

       $ownerIncomeAfterCancellation += $parkingCostForUsedDays;
       $ownerServiceTaxAfterCancellation += $ownerServiceTaxForUsedDays;



       if ( $bookingType == 4 ) //monthly
       {
	       /* This cost will come from the deposit */
               $nextInstallmentAmount = $parkingCostForUsedDays + $ownerServiceTaxForUsedDays;
	       $depositToRefund = $deposit - $nextInstallmentAmount;
               $driverEmailDisplay = "Your cancellation request has been successful. 
		                 Your booking will automatically cancel on ($dateTimeToDisplay).
		                 No further payments are due now. Your deposit has been adjusted to meet our minimum notice period policy.
		                 The adjusted deposit of Rs.$depositToRefund will be debited to your account in 2-5 working days";

               $ownerEmailDisplay = "Your cancellation request has been successful.
		                 Your space will still be occupied till ($dateTimeToDisplay).
		                 You will receive a further payment of $daysUsedInNextMonth days.";

               if ( $requestedBy == 1 ) // driver
               {
	           $displayText = "<p align=\"middle\">";
                   $displayText .= "Your cancellation request has been successful. <br/>
		                    Your booking will automatically cancel on ($dateTimeToDisplay).
		                    No further payments are due now. <br />Your deposit has been adjusted to meet our minimum notice period policy.
		                    <br />The adjusted deposit of Rs.$depositToRefund will be debited to your account in 2-5 working days";
;
                   $displayText .= "</p>";
               }
             else
              {
	           $displayText = "<p align=\"middle\">";
                   $displayText .= "Your cancellation request has been successful. <br/>
		                    Your booking will automatically cancel on ($dateTimeToDisplay).
		                    No further payments are due now. <br />Your deposit has been adjusted to meet our minimum notice period policy.
		                    <br />The adjusted deposit of Rs.$depositToRefund will be debited to your account in 2-5 working days";
                   $displayText .= "</p>";
              }
              
	       
               $internetHandlingCharges = $ownerServiceTaxForUsedDays;
	       $spIncomeForUsedDays = 0;
	       $spServiceTaxForUsedDays = 0;

	       //cancel all installments after current Date
	       $this->BookingInstallment->cancelAllInstallmentsAfterDate($bookingId,$currentDate); 

	       //update the booking db
	       $this->loadModel('Booking'); 
	       $status = Configure::read('BookingStatus.CancellationRequested'); //cancellation requested in notice period
	       $refundStatus = 1; //unpaid
	       $totalRefundPaid = 0; //only deposit needs to be refunded
	       $this->Booking->updateBookingRecordAfterCancellation($bookingId,
			                                            $totalRefundPaid,$refundStatus,$status,$lastParkingDateTime,
			                                            0,$depositToRefund,
			                                            $ownerIncomeAfterCancellation,$ownerServiceTaxAfterCancellation,
			                                            $spIncomeAfterCancellation, $spServiceTaxAfterCancellation,
			                                            $cancellationFees);
	    //send Email
	    $this->sendLongBookingCancellationEmail($bookingRecord,$bookingId,
			                            $currentDateTimeObj,$dateTimeToDisplay,
			                            $depositToRefund,
			                            $requestedBy,$driverEmailDisplay,$ownerEmailDisplay,$nextInstallmentAmount,0,$dateTimeToDisplay);       
       }
      else //for yearly, we also charge simplypark commission 
      {
	      $spCommission = $bookingRecord['Booking']['commission'];
	      $paidBy = $bookingRecord['Booking']['commission_paid_by'];

	      $spIncomeForUsedDays = round((($parkingCostForUsedDays * $spCommission)/100),2);

	      $spPaysServiceTax = $bookingRecord['Booking']['service_tax_by_simplypark'];
	      $spServiceTaxForUsedDays = 0;

	      if ( $spPaysServiceTax )
	      {
		      $spServiceTaxForUsedDays  = round((($spIncomeForUsedDays * $bookingRecord['Booking']['simplypark_service_tax_amount'])/100),2);
	      }


	      if ( $paidBy == 1 ) //driver
	      {
		      $ownerIncome = $parkingCostForUsedDays;
		      $internetHandlingCharges = $ownerServiceTaxForUsedDays + $spIncomeForUsedDays + $spServiceTaxForUsedDays;
	      }
	      else
	      {
		      $ownerIncome = $parkingCostForUsedDays - ($spIncomeForUsedDays + $spServiceTaxForUsedDays);
		      //todo: does it impact his service tax also
		      $internetHandlingCharges = 0;
	      }
              
              $installmentAmount = $ownerIncome + $internetHandlingCharges;




	      //update nextInstallment with new details
	      $this->BookingInstallment->updateInstallment($bookingId,$nextInstallmentDate,
			                                   $spIncomeForUsedDays,$spServiceTaxForUsedDays, //simplypark income & commission
			                                   $ownerIncome, $ownerServiceTaxForUsedDays,
			                                   $internetHandlingCharges,$installmentAmount);
	      //cancel all installments after current Date
	      $this->BookingInstallment->cancelAllInstallmentsAfterDate($bookingId,$nextInstallmentDate); 

	      //update the booking db
	      $this->loadModel('Booking'); 
	      $status = Configure::read('BookingStatus.CancellationRequested'); //cancellation requested in notice period
	      $refundStatus = 1; //unpaid
	      $totalRefundPaid = 0; //only deposit needs to be refunded

              $ownerIncomeAfterCancellation += $ownerIncome;
              $ownerServiceTaxAfterCancellation += $ownerServiceTaxForUsedDays;
              $spIncomeAfterCancellation += $spIncomeForUsedDays;
              $spServiceTaxAfterCancellation += $spServiceTaxForUsedDays;
              $cancellationFees = 0; //TODO

	      $this->Booking->updateBookingRecordAfterCancellation($bookingId,
			                                           $totalRefundPaid,$refundStatus,$status,$lastParkingDateTime,
			                                           0,$depositToRefund,
			                                           $ownerIncomeAfterCancellation,$ownerServiceTaxAfterCancellation,
			                                           $spIncomeAfterCancellation, $spServiceTaxAfterCancellation,
			                                           $cancellationFees);

              $driverEmailDisplay = "Your cancellation request has been successful. <br/>
		                Your booking will automatically cancel on $dateTimeToDisplay.
		                One payment of Rs. $installmentAmount is due on your next collection cycle.<br /> 
		                After $dateTimeToDisplay, Your deposit of Rs. $depositToRefund will be debited to your account in 2 to 5 working days";

              $ownerEmailDisplay  = "";
              if ( $requestedBy == 1 )// driver
              {
	          $displayText  = "<p align=\"middle\">";
                  $displayText .= "Your cancellation request has been successful. <br/>
		                   Your booking will automatically cancel on $dateTimeToDisplay.
		                   One payment of Rs. $installmentAmount is due on your next collection cycle.<br /> 
		                   After $dateTimeToDisplay, Your deposit of Rs. $depositToRefund will be debited to your account in 2 to 5 working days";
                  $displayText .= "</p>";
              }
             else
              {
                  //show installmentAmount - the internet handling charges
                  $ownerInstallmentIncome = $installmentAmount - $internetHandlingCharges;

                  $ownerEmailDisplay = "Your cancellation request has been successful. <br/>
		                Your space will still be occupied till $dateTimeToDisplay.
		                One payment of Rs. $ownerInstallmentIncome will be collected in the next collection cycle.";

	         //update nextInstallment with new details
	          $displayText  = "<p align=\"middle\">";
                  $displayText .= "Your cancellation request has been successful. <br/>
		                   Your space will still be occupied till $dateTimeToDisplay.
		                   One payment of Rs. $ownerInstallmentIncome will be collected in the next collection cycle.";
                  $displayText .= "</p>";
              }
	      
              //send Email
	      $this->sendLongBookingCancellationEmail($bookingRecord,$bookingId,
			                              $currentDateTimeObj,$dateTimeToDisplay,
			                              $depositToRefund,
			                              $requestedBy,$driverEmailDisplay,$ownerEmailDisplay,
                                                      $installmentAmount, $internetHandlingCharges,$nextInstallmentDate);       
      }
     
  
       return true;
    }   
    

 }



   /******************************************************************
      Function Name : cancelBookingDueToPaymentDefault 
      Description   : cancels a booking due to default on payment
                      
      input         : 
      returns       : true if successful, false otherwise
      assumption    : to be called ONLY for long term bookings and 
                      when all attempts for payments have expired 
   ******************************************************************/

   public function cancelBookingDueToPaymentDefault($bookingId,$datePay)
   {
      /*step 1: Get the booking record */
      $this->loadModel('Booking'); 
      $bookingRecord = $this->Booking->getBookingRecordForCancellation($bookingId);

      /*added by ravish due to the "PHP Fatal error:  Call to a member function format()" error through cron hit*/
      $currentDateTimeObj = new DateTime(); //make sure the default_time_zone is set to Asia/Calcutta
      /*added by ravish*/
      
      $currentDate = $currentDateTimeObj->format('Y-m-d');
      
      //Nothing to refund. We will take the whole deposit and pass it to her owner 

     $cancellationFees = 0;
     $ownerIncomeAfterCancellation = $bookingRecord['Booking']['owner_income'];
     $ownerServiceTaxAfterCancellation = $bookingRecord['Booking']['owner_service_tax_amount'];
     $spIncomeAfterCancellation = $bookingRecord['Booking']['simplypark_income'];
     $spServiceTaxAfterCancellation = $bookingRecord['Booking']['simplypark_service_tax_amount'];
     $deposit = $bookingRecord['Booking']['refundable_deposit']; 
     $depositToRefund = 0;

     //Add the deposit to owner Income */
     $ownerIncomeAfterCancellation += $deposit; //we are passing deposit as cancellation fees and therefore not charging service tax

     $refundDateTimeObj = $currentDateTimeObj;
     $refundDateTime = $refundDateTimeObj->format('Y-m-d H:i:s');

      $this->loadModel('BookingInstallment'); 
      $this->BookingInstallment->cancelAllInstallmentsAfterDateInclusive($bookingId,$datePay);
      

     //update the booking db 
     //update the booking db
      $this->loadModel('Booking'); 
      $status = Configure::read('BookingStatus.CancelledPaymentDefault'); //cancelled due to default
      $refundStatus = 0; //no refund due
      $totalRefundPaid = 0; //only deposit needs to be refunded
      $this->Booking->updateBookingRecordAfterCancellation($bookingId,
					                    $totalRefundPaid,$refundStatus,$status,$refundDateTime,
					                    1,$depositToRefund,
                                                            $ownerIncomeAfterCancellation,$ownerServiceTaxAfterCancellation,
					                    $spIncomeAfterCancellation, $spServiceTaxAfterCancellation,
					                    $cancellationFees);

     //send relevent Emails
     $this->sendLongBookingDefaultCancellationEmail($bookingId,
                                                    $bookingRecord['Booking']['space_id'],
                                                    $bookingRecord['Booking']['user_id'],$bookingRecord['Booking']['space_user_id'],
                                                    $bookingRecord['Booking']['start_date'],$bookingRecord['Booking']['end_date'],
                                                    $deposit,$ownerIncomeAfterCancellation);
                                  
       
      return true;
   }  

   /******************************************************************
      Function Name : initiateRefund 
      Description   : Given a payment id & amount, call the payment
                      gateway refund api
      input         : $booking_id, amount
      returns       : true if successful, false otherwise
   ******************************************************************/
 private function initiateRefund($paymentId,$refund)
  {
     $amountInPaisa= array('amount' => $refund * 100); //Amount should be in paisa.
     /*CakeLog::write('debug',print_r('Initiating Refund of '));
     CakeLog::write('debug',print_r($amountInPaisa));*/

     $socket = new HttpSocket();
     $socket->configAuth('Basic', Configure::read('RazorPayKeys.KeyID'), Configure::read('RazorPayKeys.KeySecret'));
     $payment = $socket->post(Configure::read('RazorPayKeys.APIURL').$paymentId.'/refund',$refund); 

     //check return object for errors 
     if ( $payment->code == Configure::read('HTTPStatusCode.OK')) {
        //CakeLog::write('debug',print_r('Refund Pay returned OK'),true);
        return true;
     } 
    else if ( $payment->code == Configure::read('HTTPStatusCode.BadRequest')) {
       CakeLog::write('error','Refund Pay returned Bad Request for payment id' . $paymentId);
       CakeLog::write('error', 'Reason:: ' . print_r($payment,true));
       return false;
     }
    else if ( $payment->code == Configure::read('HTTPStatusCode.NotAuthorized')) {
       CakeLog::write('error','Refund Pay returned UNAUTHORIZED for payment id' . $paymentId);
       CakeLog::write('error', 'Reason:: ' . print_r($payment,true));
       return false;
     }
  }
   
  /******************************************************************
      Function Name : sendCancellationEmail 
      Description   : 
      input         : 
      returns       : true if successful, false otherwise
   ******************************************************************/
private function sendCancellationEmail($bookingRecord,$oldStatus,$cancellationFees,$requestedBy)
{
	//CakeLog::write('debug','sendCancellationEmail called with bookingRecord as ' . print_r($bookingRecord,true)); 
      
        $spTransId = str_replace("pay","SP",$bookingRecord['Transaction']['payment_id']);

	$this->loadModel('EmailTemplate');

	//shouldn't this be in the model
	$cancelEmailTemplate = $this->EmailTemplate->find('first',
			array('conditions' => array('EmailTemplate.id' => Configure::read('EmailTemplateId.cancel_booking_email_to_driver'))));

	$ownerEmailTemplate = $this->EmailTemplate->find('first',
			array('conditions' => array('EmailTemplate.id' => Configure::read('EmailTemplateId.cancel_booking_email_to_owner'))));

         $driverDisplayText ='';
         $ownerDisplayText ='';
         $infoMsg = '';
         $refundInfoMsg = 'The refund will be credited to your debit/card card in 2-5 working days.';

        //first send email to driver
	 if( $requestedBy == 1 ) //driver
	 {
	    $driverDisplayText = "Your cancellation Request has been successful.";
	    $ownerDisplayText = "Unfortunately, the driver has cancelled the booking."; 
            
            if($oldStatus ==  Configure::read('BookingStatus.Waiting'))
            {
              $ownerDisplayText .= "No further action is required from you";
              $infoMsg = "The total amount will be refunded in 2-5 working days";
              $refundInfoMsg = '';
            }
        
  	 }
	else //owner cancelled
	 {
	    $driverDisplayText = "Unfortunately, the owner has cancelled your booking.";
	    $ownerDisplayText = "Your cancellation request has been successful. The whole booking amount will be refunded to the user"; 
            
            if($oldStatus ==  Configure::read('BookingStatus.Waiting'))
            {
              $infoMsg = "The total amount will be refunded in 5 working days.";
              $refundInfoMsg = '';
            }
	 }
        

	
	$address = $bookingRecord['Space']['flat_apartment_number'].', '
		. $bookingRecord['Space']['address1'].', '
		. $bookingRecord['Space']['address2'].' PinCode: ' . $bookingRecord['Space']['post_code'];

        $deposit = 0.0;
        if(isset($bookingRecord['Booking']['refundable_deposit']))
        {
           $deposit =  $bookingRecord['Booking']['refundable_deposit'];
        }

        $refund =  $bookingRecord['Booking']['space_price'] + $bookingRecord['Booking']['internet_handling_charges'] + 
                   $deposit - $cancellationFees;

        $startDateStr = date('d/m/Y H:i', strtotime($bookingRecord['Booking']['start_date']));
        $endDateStr = date('d/m/Y H:i', strtotime($bookingRecord['Booking']['end_date']));


	$cancelEmailTemplate['EmailTemplate']['mail_body'] = str_replace(
			array('#NAME','#CANCELMESSAGE','#TRANSID','#PRICE','#HCHARGES','#RAMOUNT','#CFEES','#TOTAL','#ADDRESS','#STARTTIME','#ENDTIME','#INFOMSG','#REFUNDMSG'),
			array(
				$bookingRecord['User']['UserProfile']['first_name'].' '.$bookingRecord['User']['UserProfile']['last_name'],
                                $driverDisplayText,
				$spTransId, 
                                $bookingRecord['Booking']['space_price'],
                                $bookingRecord['Booking']['internet_handling_charges'],
				number_format((float)$deposit,2,'.',''),
				number_format((float)$cancellationFees,2,'.',''),
				number_format((float)$refund,2,'.',''),
				$address,
                                $startDateStr,
                                $endDateStr,
                                $infoMsg,
                                $refundInfoMsg
			     ),$cancelEmailTemplate['EmailTemplate']['mail_body']);

	//send an email to driver 
	$this->_sendEmailMessage($bookingRecord['User']['email'], $cancelEmailTemplate['EmailTemplate']['mail_body'], 
			                $cancelEmailTemplate['EmailTemplate']['subject'], Configure::read('Email.EmailAdmin'));

	$ownerEmailTemplate['EmailTemplate']['mail_body'] = str_replace(
			array('#NAME','#CANCELMESSAGE','#TRANSID','#ADDRESS','#USERNAME','#USEREMAIL','#STARTTIME','#ENDTIME'),
			array(
				$bookingRecord['Space']['User']['UserProfile']['first_name'].' '.$bookingRecord['Space']['User']['UserProfile']['last_name'],
				$ownerDisplayText,
				$spTransId, 
                                $address,
				$bookingRecord['User']['UserProfile']['first_name'].' '.$bookingRecord['User']['UserProfile']['last_name'],
                                $bookingRecord['User']['email'],
				$startDateStr,
				$endDateStr,
			     ),$ownerEmailTemplate['EmailTemplate']['mail_body']);

	return $this->_sendEmailMessage($bookingRecord['Space']['User']['email'], $ownerEmailTemplate['EmailTemplate']['mail_body'], 
			$ownerEmailTemplate['EmailTemplate']['subject'], Configure::read('Email.EmailAdmin'));
       

}
  /******************************************************************
      Function Name : sendLongBookingCancellationEmail 
      Description   : 
      input         : 
      returns       : true if successful, false otherwise
   ******************************************************************/
 private function sendLongBookingCancellationEmail($bookingRecord,$bookingId,
                                                   $currentDateTimeObj, $lastParkingDate,
                                                   $deposit,
                                                   $requestedBy,$driverEmailDisplay,$ownerEmailDisplay,
                                                   $nextInstallmentAmount, $installmentHandlingCharge,$nextInstallmentDate="")
{
        //CakeLog::write('debug','In sendLongBookingCancellationEmail ...' . print_r($bookingRecord,true));
        $this->loadModel('BookingInstallment');
        $amountBookingRecord = $this->BookingInstallment->getTotalBookingAmount($bookingId);
        
        $totalAmount                         = $amountBookingRecord[0]['BookingInstallment']['total_booking_amount'];
        $totalHandlingCharges                = $amountBookingRecord[0]['BookingInstallment']['total_handling_charges'];
        $bookingAmountWithoutHandlingCharges = $totalAmount - $totalHandlingCharges;

        //CakeLog::write('debug','Total Booking Amount:: ' . $totalAmount);

        $dueAndPaid = $this->BookingInstallment->getTotalDueAndPaidInstallments($bookingId);
        //CakeLog::write('debug','Total Due and Paid:: ' . $dueAndPaid);

        $refundableDepositText = "Deposit";
        $infoMsg = "Your deposit will be debited to your account in 2-5 working days";
     

        $spTransId = str_replace("pay","SP",$bookingRecord['Transaction']['payment_id']);
	
        $this->loadModel('EmailTemplate');

	$driverEmailTemplate = $this->EmailTemplate->find('first',
			                                   array('conditions' => array('EmailTemplate.id' => Configure::read('EmailTemplateId.cancel_long_term_booking_email_to_driver')))
			                                 );
	
        $ownerEmailTemplate = $this->EmailTemplate->find('first',
			                                   array('conditions' => array('EmailTemplate.id' => Configure::read('EmailTemplateId.cancel_long_term_booking_email_to_owner')))
			                                 );

        if ( $requestedBy == 2 ) //owner
        {
           $driverEmailDisplay = "Unfortunately, the space owner has cancelled your booking. As per our policy, you can still use the parking space till $lastParkingDate. All Relevant information enclosed";
        }
    
	$address =  $bookingRecord['Space']['name'] .' - '. $bookingRecord['Space']['City']['name'];
	$address .= " ".$bookingRecord['Space']['flat_apartment_number'].', '.$bookingRecord['Space']['address1'].', '.$bookingRecord['Space']['address2'].' ' . 
                        $bookingRecord['Space']['State']['name'] .
                       ' PinCode: ' . $bookingRecord['Space']['post_code'];

        $dueOnText = "";
        $dueOnDate ="";


        if($nextInstallmentAmount != 0)
        {
           $dueOnText = "Due On";
           $dueOnDate = $nextInstallmentDate;
           $installmentForOwner = $nextInstallmentAmount - $installmentHandlingCharge;
        }
 
          

        
	$driverEmailTemplate['EmailTemplate']['mail_body'] = str_replace(
			array('#NAME','#CANCELMESSAGE','#TRANSID','#LASTPARKINGDATE',
                              '#RENT','#DPAID','#NPAYMENT','#DUEON','#DUEDATE','#RDEPOSIT','#RAMOUNT','#INFOMSG',
                              '#ADDRESS','#OWNERNAME','#OWNEREMAIL','#STARTTIME','#ENDTIME','#CARREG','#TYPE' ),
			array(
				$bookingRecord['User']['UserProfile']['first_name'].' '.$bookingRecord['User']['UserProfile']['last_name'],
                                $driverEmailDisplay,
                                $spTransId,
                                $lastParkingDate,
                                number_format((float)$totalAmount,2,'.',''),
                                number_format((float)$dueAndPaid,2,'.',''),
                                number_format((float)$nextInstallmentAmount,2,'.',''),
                                $dueOnText,
                                $dueOnDate,
                                $refundableDepositText,
                                number_format((float)$deposit,2,'.',''),
                                $infoMsg,      
                                $address,
				$bookingRecord['Space']['User']['UserProfile']['first_name'].' '.$bookingRecord['Space']['User']['UserProfile']['last_name'], //owner name
				$bookingRecord['Space']['User']['email'], //owner email 
                                date('d/m/Y H:i', strtotime($bookingRecord['Booking']['start_date'])),
                                date('d/m/Y H:i',strtotime($bookingRecord['Booking']['end_date'])),
                                $bookingRecord['UserCar']['registeration_number'],
                                Configure::read('CarTypes.'.$bookingRecord['UserCar']['car_type'])
			     ),$driverEmailTemplate['EmailTemplate']['mail_body']);

	//send an email to driver 
	$this->_sendEmailMessage($bookingRecord['User']['email'], $driverEmailTemplate['EmailTemplate']['mail_body'], 
			$driverEmailTemplate['EmailTemplate']['subject'], Configure::read('Email.EmailAdmin'));

        $deposit = "NA"; //currently we do not show the deposit information to the owner
        //$displayText .= "<br />Total Income earned from this booking $ownerIncomeAfterCancellation.";
        if ( $requestedBy == 1 ) //driver
        {
           $ownerEmailDisplay = "Unfortunately, the driver has cancelled the booking on your space. To help protect your income, as per our policy, a notice period applies and the driver has to use your parking space till $lastParkingDate.<br />We have enclosed all relevant information below";
        }
        $infoMsg = '';

        $totalRent = $totalAmount - $totalHandlingCharges;
        

	$ownerEmailTemplate['EmailTemplate']['mail_body'] = str_replace(
			array('#NAME','#CANCELMESSAGE','#TRANSID','#LASTPARKINGDATE',
                              '#RENT','#DPAID','#NPAYMENT','#DUEON','#DUEDATE','#INFOMSG',
                              '#ADDRESS','#USERNAME','#USEREMAIL','#STARTTIME','#ENDTIME','#CARREG','#TYPE'),
			array(
				$bookingRecord['User']['UserProfile']['first_name'].' '.$bookingRecord['User']['UserProfile']['last_name'],
                                $ownerEmailDisplay,
                                $spTransId, 
				$lastParkingDate,
                                number_format((float)$totalRent,2,'.',''),
                                number_format((float)$dueAndPaid,2,'.',''),
                                number_format((float)$installmentForOwner,2,'.',''),
                                $dueOnText,
                                $dueOnDate,
                                $infoMsg,
                                $address,
				$bookingRecord['User']['UserProfile']['first_name'].' '.$bookingRecord['User']['UserProfile']['last_name'],
                                $bookingRecord['User']['email'],
                                date('d/m/Y H:i', strtotime($bookingRecord['Booking']['start_date'])),
                                date('d/m/Y H:i', strtotime($bookingRecord['Booking']['end_date'])),
                                $bookingRecord['UserCar']['registeration_number'],
                                Configure::read('CarTypes.'.$bookingRecord['UserCar']['car_type'])
			     ),$ownerEmailTemplate['EmailTemplate']['mail_body']);

	return $this->_sendEmailMessage($bookingRecord['User']['email'], $ownerEmailTemplate['EmailTemplate']['mail_body'], 
			$ownerEmailTemplate['EmailTemplate']['subject'], Configure::read('Email.EmailAdmin'));

}

  /******************************************************************
      Function Name : sendLongCancellationEmail 
      Description   : 
      input         : 
      returns       : true if successful, false otherwise
   ******************************************************************/
 private function sendLongBookingDefaultCancellationEmail($bookingId,$spaceId,$driverId,$ownerId,
                                                          $currentDate, $startDateTime,$endDateTime,
                                                          $cancellationFees,$ownerIncomeAfterCancellation)
                                                          
{
	
        $this->loadModel('User');
	$driverDetail = $this->User->getUserDetails($driverId);
	$ownerDetail = $this->User->getUserDetails($ownerId);
        
	$spaceDetail = $this->Space->bookingSpaceInfo($spaceId);

	$this->loadModel('EmailTemplate');

	//shouldn't this be in the model
	$cancelEmailTemplate = $this->EmailTemplate->find('first',
			array('conditions' => array('EmailTemplate.id' => Configure::read('EmailTemplateId.cancel_long_booking_default')))
			);

        $ownerEmailTemplate = $cancelEmailTemplate;

        $cancelMsg = "Due to non-payment of pending dues, your booking has been cancelled with immediate effect. <br />The deposit of Rs.$cancellationFees paid at the time of booking has been passed on to the owner as cancellation Fees. ";
        $paymentDefaultMsg = "Cancellation Fees: ";
        
	$cancelEmailTemplate['EmailTemplate']['mail_body'] = str_replace(
			array('#NAME','#CANCELMESSAGE','#SPACENAME','#ADDRESS','#STARTTIME','#ENDTIME','#PAYMENTDEFAULTMSG','#CANCELLATIONFEES'),
			array(
				$driverDetail['UserProfile']['first_name'].' '.$driverDetail['UserProfile']['last_name'],
				$cancelMsg,
				$spaceDetail['Space']['name'] .' - '. $spaceDetail['City']['name'],
				$spaceDetail['Space']['flat_apartment_number'].', '.$spaceDetail['Space']['address1'].', '.$spaceDetail['Space']['address2'].$spaceDetail['Space']['post_code'],
                                $startDateTime,
                                $endDateTime,
				$paymentDefaultMsg,
				$cancellationFees
			     ),$cancelEmailTemplate['EmailTemplate']['mail_body']);

	//send an email to driver 
	$this->_sendEmailMessage($driverDetail['User']['email'], $cancelEmailTemplate['EmailTemplate']['mail_body'], 
			$cancelEmailTemplate['EmailTemplate']['subject'], Configure::read('Email.EmailAdmin'));

        $deposit = "NA"; //currently we do not show the deposit information to the owner
        
        $cancelMsg = "Due to non-payment of pending dues, the booking on your listed space has been cancelled with immediate effect. <br />The deposit of Rs.$cancellationFees collected at the time of booking has been passed on to the you as cancellation Fees. Please find the relevant details below. <br /> Total income from this booking : Rs. $ownerIncomeAfterCancellation";
        $email = $driverDetail['User']['email'];
        $mobile = $driverDetail['UserProfile']['mobile'];
        $cancelMsg .= "<br /> The driver of the car parked at your space can be reached at $email and/or $mobile<br />";
        
        $paymentDefaultMsg = "Cancellation Fees: ";

	$ownerEmailTemplate['EmailTemplate']['mail_body'] = str_replace(
			array('#NAME','#CANCELMESSAGE','#SPACENAME','#ADDRESS','#STARTTIME','#ENDTIME','#PAYMENTDEFAULTMSG','#CANCELLATIONFEES'),
			array(
				$ownerDetail['UserProfile']['first_name'].' '.$ownerDetail['UserProfile']['last_name'],
				$cancelMsg,
				$spaceDetail['Space']['name'] .' - '. $spaceDetail['City']['name'],
				$spaceDetail['Space']['flat_apartment_number'].', '.$spaceDetail['Space']['address1'].', '.$spaceDetail['Space']['address2'].$spaceDetail['Space']['post_code'],
                                $startDateTime,
                                $endDateTime,
                                $paymentDefaultMsg,
                                $cancellationFees
			     ),$ownerEmailTemplate['EmailTemplate']['mail_body']);

	return $this->_sendEmailMessage($ownerDetail['User']['email'], $ownerEmailTemplate['EmailTemplate']['mail_body'], 
			$ownerEmailTemplate['EmailTemplate']['subject'], Configure::read('Email.EmailAdmin'));

}


/**
 * Method payInstallment to pay installment of long term bookings
 *
 * @param $bookingInstallmentID int the id of the booking installment
 * @return void
 */
    public function payInstallment($bookingInstallmentID = null) {
        
        $this->layout = 'home';
        if (empty($bookingInstallmentID)) {
            $this->Session->setFlash(__('Invalid URL'),'default','error');
            $this->redirect(array('controller' => 'homes', 'action' => 'index'));
        }

        $bookingInstallmentID = base64_decode(urldecode($bookingInstallmentID));
        $bookingInstallmentRecord = $this->BookingInstallment->getBookingIntallment($bookingInstallmentID);
        if (empty($bookingInstallmentRecord)) {
            $this->Session->setFlash(__('This link has been expired.'),'default','error');
            $this->redirect(array('controller' => 'homes', 'action' => 'index'));
        }

        $this->set('installmentInfo',$bookingInstallmentRecord);
    }

/**
 * Method installmentSave to save the payment of the monthly installment
 *
 * @return void
 */
    public function installmentSave() {
        
	    $this->request->allowMethod('post','put');
	    
        CakeLog::write('debug','Inside installmentSave....' . print_r($this->request->data,true));
        if ($this->Booking->saveAssociated($this->request->data)) {
		$this->_emailInstallmentPaid($this->request->data['BookingInstallment'][0]['id']);

            /*if (Configure::read('SMSNotifications')) {
                //send message to owner to approve/disapprove booking
                $this->__smsOwnerDriverToReceivedPayment($this->request->data['Booking']['space_user_id'],'payment_received');
                $this->__smsOwnerDriverToReceivedPayment($this->request->data['Booking']['user_id'],'amount_successfully_received');

                //send message to driver to pending approval booking
                $this->__smsAdminToReceivedPayment($this->request->data['Booking']['user_id']);
	    }*/

            $this->Session->setFlash(__('You have successfully made payment for your installment.'),'default','success');
            $this->redirect(array('controller' => 'homes', 'action' => 'index'));
        }
        $this->_hitToRefundAmount($this->request->data['Transaction']['payment_id'], $this->request->data['Booking']['booking_price']);
        $this->Session->setFlash(__('Sorry, your transaction did not proceed successfully, your money will be refunded to your account in two or three working days.'),'default','error');
        $this->redirect(array('controller' => 'homes', 'action' => 'index'));
    }

/**
 * Method __smsOwnerDriverToReceivedPayment to send message to owner and driver mobile to intimate him that
 * payment for installment has been received
 *
 * @param $ownerUserID int the user id of the owner to get mobile number
 * @return bool
 */
    private function __smsOwnerDriverToReceivedPayment($userID = null, $messageID) {
        $ownerMobileNumber = $this->UserProfile->getUserName($userID); //getting user mobile number

        //getting message body
        $messageContent = $this->Message->getMessage(Configure::read('MessageId.'.$messageID));

        $messageContent['Message']['body'] = str_replace(
            array('#USER','#AMOUNT'),
            array(
                    $ownerMobileNumber['UserProfile']['first_name'].' '.$ownerMobileNumber['UserProfile']['last_name'],
                    $this->request->data['Booking']['booking_price']
                ), $messageContent['Message']['body']);


        $messageResponse = $this->_sendMessageNotification($ownerMobileNumber['UserProfile']['mobile'],$messageContent['Message']['body']);

        if ($messageResponse->code == Configure::read('HTTPStatusCode.OK')) {
            return true;
        }
        return false;
    }

/**
 * Method __smsAdminToReceivedPayment to send message to admin mobile to intimate him that
 * we have recieved user's installment payment
 *
 * @param $userID int the user id of the driver to get mobile number
 * @return bool
 */
    private function __smsAdminToReceivedPayment() {
        $adminMobileNumber = $this->UserProfile->getUserName(Configure::read('AdminUserID')); //getting admin mobile number

        //getting message body
        $messageContent = $this->Message->getMessage(Configure::read('MessageId.payment_received'));

        $messageContent['Message']['body'] = str_replace(
            array('#USER'),
            array(
                    'Admin'
                ), $messageContent['Message']['body']);


        $messageResponse = $this->_sendMessageNotification($adminMobileNumber['UserProfile']['mobile'],$messageContent['Message']['body']);

        if ($messageResponse->code == Configure::read('HTTPStatusCode.OK')) {
            return true;
        }
        return false;
    }

/**
 * Method _emailInstallmentPaid to send email to user for received payment
 *
 * @param $bookingInstallmentID int the id of the booking installment
 * @return bool
 */
protected function _emailInstallmentPaid($bookingInstallmentID = null) 
{
	$bookingInstallmentRecord = $this->BookingInstallment->emailBookingIntallmentRecord($bookingInstallmentID);

	$temp = $this->EmailTemplate->find('first', 
		                           array('conditions' => array('EmailTemplate.id' => Configure::read('EmailTemplateId.installment_amount_paid'))));
	$temp['EmailTemplate']['mail_body'] = str_replace(
		array(
			'../../..',
			'#NAME',
			'#SPACENAME',
			'#AMOUNT',
			'#DATE',
		),
		array(
			FULL_BASE_URL,
			$bookingInstallmentRecord['Booking']['User']['UserProfile']['first_name'].' '.$bookingInstallmentRecord['Booking']['User']['UserProfile']['last_name'],
			$bookingInstallmentRecord['Booking']['Space']['name'],
			$bookingInstallmentRecord['BookingInstallment']['amount'],
			date("jS-M-y")
		),
		$temp['EmailTemplate']['mail_body']
	);

	$emailTo = $bookingInstallmentRecord['Booking']['User']['email'];
	$bccTo = array(
		//$bookingInstallmentRecord['Booking']['Space']['User']['email'],
		Configure::read('Email.EmailAdmin')
	);
	//CakeLog::write('debug','Email To ' . $emailTo);
	//CakeLog::write('debug','Email To ' . print_r($ccTo,true));
	//CakeLog::write('debug','MailBody' . $temp['EmailTemplate']['mail_body']);
	
   return $this->_sendEmailMessage($emailTo, $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject'],$bccTo,null);
}
/**
 * Method _emailInstallmentPaid to send email to user for received payment
 *
 * @param $bookingInstallmentID int the id of the booking installment
 * @return bool
 */
private function __calculateDiscountForLongTermBulkBooking($couponDetails,$couponFlatAmount,$discountPercentage,$maxDiscountAmount,$numSlots,
                                                           $numberOfInstallments,$monthlyInstallment,$lastInstallment,$deposit,
                                                           $ownerIncomePerInstallment,$ownerIncomeForLastInstallment,
                                                           $ownerServiceTaxPerInstallment,$ownerServiceTaxForLastInstallment,
                                                           $simplyParkIncomePerInstallment,$simplyParkIncomeForLastInstallment,
                                                           $simplyParkServiceTaxPerInstallment,$simplyParkServiceTaxForLastInstallment)
{
	$perInstallmentDiscount  = 0.0;
	$lastInstallmentDiscount = 0.0;

	if($couponFlatAmount)
	{
		$perInstallmentDiscount  = $couponFlatAmount * $numSlots;
		$lastInstallmentDiscount = $perInstallmentDiscount;
	} 
	else if($discountPercentage)
	{
		$installmentAmount     = $monthlyInstallment -  $simplyParkServiceTaxPerInstallment;
		$lastInstallmentAmount = $lastInstallment - $simplyParkServiceTaxForLastInstallment;

		$perInstallmentDiscount = (($discountPercentage * $installmentAmount)/100); 
		$perInstallmentDiscount = ( $perInstallmentDiscount > $maxDiscountAmount) ? $maxDiscountAmount : $perInstallmentDiscount;

		$lastInstallmentDiscount = (($discountPercentage * $lastInstallmentAmount)/100); 
		$lastInstallmentDiscount = ( $lastInstallmentDiscount > $maxDiscountAmount) ? $maxDiscountAmount : $lastInstallmentDiscount;
	}



	$numDiscountedInstallments = $couponDetails['DiscountCoupon']['num_discounted_installments'];

	if($numDiscountedInstallments == 0)
	{
		$numDiscountedInstallments = $numberOfInstallments;
		$loopEnd =  $numDiscountedInstallments - 1;
	}else{
		$loopEnd =  $numDiscountedInstallments;
	}

	$installmentAmount            = array();
	$spIncomePerInstallment       = array();
        $spServiceTaxPerInstallment   = array();
	$handlingChargePerInstallment = array(); 

	for($itr=0; $itr<$loopEnd;$itr++)
	{
		$spIncomePerInstallment[$itr] = $simplyParkIncomePerInstallment - $perInstallmentDiscount;
		$spServiceTaxPerInstallment[$itr] = 0.0;

		if($spIncomePerInstallment[$itr] > 0 && $simplyParkServiceTax > 0)
		{
			$spServiceTaxPerInstallment[$itr] =  $this->calculateServiceTax($spIncomePerInstallment[$itr],
					$serviceTaxPercentage);   
		}

		$handlingChargePerInstallment[$itr] = round($ownerServiceTaxPerInstallment +
			                                $spIncomePerInstallment[$itr] +
			                                $spServiceTaxPerInstallment[$itr],2);

		$installmentAmount[$itr] = round($ownerIncomePerInstallment + $handlingChargePerInstallment[$itr],2); 
	}

	if($numDiscountedInstallments == $numberOfInstallments)
	{
		$spIncomePerInstallment[$itr] = $simplyParkIncomeForLastInstallment - $lastInstallmentDiscount;
		$spServiceTaxPerInstallment[$itr] = 0.0;

		if($spIncomePerInstallment[$itr] > 0 && $simplyParkServiceTax > 0)
		{
			$spServiceTaxPerInstallment[$itr] =  $this->calculateServiceTax($spIncomePerInstallment[$itr],
					$serviceTaxPercentage);   
		}

		$handlingChargePerInstallment[$itr] = round($ownerServiceTaxForLastInstallment +
			$spIncomePerInstallment[$itr] +
			$spServiceTaxPerInstallment[$itr],2);

		$installmentAmount[$itr] = round($ownerIncomeForLastInstallment + $handlingChargePerInstallment[$itr],2); 


	}else 
	{
		for( ; $itr<$numberOfInstallments-1;$itr++)
		{
			$spIncomePerInstallment[$itr] = $simplyParkIncomePerInstallment;
                        $spServiceTaxPerInstallment[$itr] = $simplyParkServiceTaxPerInstallment;

			$handlingChargePerInstallment[$itr] = round($ownerServiceTaxPerInstallment +
				                                $spIncomePerInstallment[$itr] +
				                                $spServiceTaxPerInstallment[$itr],2);

			$installmentAmount[$itr] = round($ownerIncomePerInstallment + $handlingChargePerInstallment[$itr],2); 

		}

		/* last installment */
		$spIncomePerInstallment[$itr] = $simplyParkIncomeForLastInstallment;
               $spServiceTaxPerInstallment[$itr] = $simplyParkServiceTaxForLastInstallment;

		$handlingChargePerInstallment[$itr] = round($ownerServiceTaxForLastInstallment +
			                              $spIncomePerInstallment[$itr] +
		                             	      $spServiceTaxPerInstallment[$itr],2);

		$installmentAmount[$itr] = round($ownerIncomeForLastInstallment + $handlingChargePerInstallment[$itr],2); 

	}

        //CakeLog::write('debug','in discount controller..installmentamount ' . $installmentAmount[0] . ' deposit ' . $deposit); 
        $payNow = $installmentAmount[0] + $deposit;
	
        $status = array(
			'Type' => Configure::read('StatusType.Success'),
			'Message' => __('Coupon applied sucessfully.'),
			'discountCouponId' => $couponDetails['DiscountCoupon']['id'],
			'discountAmount' => round($perInstallmentDiscount,2),
			'isShortTermBooking' => 0,
                        'numInstallments' => $numberOfInstallments,
			'installmentArray' => $installmentAmount,
			'spIncomeArray' => $spIncomePerInstallment,
                        'spServiceTaxArray' => $spServiceTaxPerInstallment,
			'handlingChargeArray' => $handlingChargePerInstallment,
                        'numDiscountedInstallments' => $numDiscountedInstallments,
                        'payNow' => $payNow,
                        'refundableDeposit' => $deposit,
                        'isBulk' => 1
		       );

     return $status;
}

function getParkingId($date,$operatorId)
{
	echo "In getParkingID, Time " . $entryTime . "<br />";
	$maxPrintableChar = 36;
	$maxParkIdLen = 6;
	$encodeArray = array('0','1','2','3','4','5','6','7','8','9',
		'a','b','c','d','e','f','g','h','i','j',
		'k','l','m','n','o','p','q','r','s','t',
		'u','v','w','y','z');
	$parkingId = array();

	$numberIndexStart = 0;
	$alphabetIndexStart = 10;
	$baseYear = 2016;
	$radix    = 36;

	$epochTimeObj = new DateTime("2016-01-01 00:00:00");
	$epochTime = $epochTimeObj->getTimestamp(); 
	echo "EpochTime in MS " . $epochTime ."<br />";

	$currentTimeObj = new DateTime($entryTime);
	$currentTime = $currentTimeObj->getTimestamp();
	echo "Current(Entry) Time in MS " . $currentTime ."<br />";

	$timeInMs = intval(($currentTime - $epochTime));

	$yearInByte = 0;

	//we only have 5 bits out of possible 8 to encode operatorId and year.
	//as 2^5 is 32 which is withing the range of 36 printable ascii characters
	//we have
	$operatorByte = $operatorId;
	echo "Operator ID " . $operatorId . "<br />";
	echo "OperatorByte " . $operatorByte . "<br />";
	$operatorByte <<=1;

	$operatorByte |= $yearInByte;		  
	echo "OperatorByte(after year in Byte) " . $operatorByte . "<br />";
	$parkingId[0] = $encodeArray[$operatorByte];

	$itr = 1;

	echo "Time in MS " . $timeInMs . "<br />";
	while($timeInMs >0)
	{

		$parkingId[$itr++] = $encodeArray[(int)($timeInMs % $radix)];
		$timeInMs = intval($timeInMs / $radix);
		echo "Time in MS " . $timeInMs . "<br />";
	}

	return $parkingId;

}

/******************************************************************************
 *  functions which work on bookings created within the app
 *
 ******************************************************************************/

public function createBooking($spaceId){
	$response = array();
	$jsonData = $this->request->input('json_decode');

	$operatorId = $jsonData->{'operatorId'};
	$appGeneratedBookingId  = $jsonData->{'bookingId'};
	$gcmIdAtServer = $jsonData->{'gcmId'};

	//user specific params
	$name    = $jsonData->{'name'};
	$email   =  $jsonData->{'email'};
	$mobile  = $jsonData->{'mobile'};

	CakeLog::write('debug','In createBooking, appgenerated booking iD:: ' . $appGeneratedBookingId);
	CakeLog::write('debug','In createBooking, serialNumber:: ' . $jsonData->{'serialNumber'});

	//booking specific params
	$bookingStatus        = $jsonData->{'bookingStatus'};
	//$bookingType          = $jsonData->{'bookingType'};
	
	$isPassForStaff = 0;
        if(isset($jsonData->{'isPassForStaff'}))
        {
          $isPassForStaff = $jsonData->{'isPassForStaff'};
        }

	
	$bookingStartDateTime = $jsonData->{'startDateTime'}; //this is date in the format YYYY-MM-DD HH:mm
	$bookingEndDateTime   = $jsonData->{'endDateTime'};   //this is date in the format YYYY-MM-DD HH:mm
	$passIssueDate        = $jsonData->{'passIssueDate'}; //this is date in the format YYYY-MM-DD
        if(isset($jsonData->{'encodedPassCode'}))
        {
	   $encodedPassCode = $jsonData->{'encodedPassCode'};
        }else{
           $encodedPassCode = $appGeneratedBookingId;
	}
	if(isset($jsonData->{'serialNumber'})){
	   $serialNumber  = $jsonData->{'serialNumber'};
	}else{
	   $serialNumber = "";
        }	   
	$vehicleReg           = $jsonData->{'vehicleReg'};
	$vehicleType          = $jsonData->{'vehicleType'};
	$numBookedSlots       = $jsonData->{'numBookedSlots'};
	$parkingFees          = $jsonData->{'parkingFees'};
	$discount             = $jsonData->{'discount'};
        $isRenewal            = false;
	$parentBookingId      = "";
	$paymentMode          = Configure::read('PaymentMode.Cash');
	$invoiceId            = "";
	$invoicePaymentId     = "";

        if(isset($jsonData->{'isRenewal'}))
        {
           $isRenewal = $jsonData->{'isRenewal'};
        }

        if(isset($jsonData->{'parentBookingId'}))
        {
           $parentBookingId = $jsonData->{'parentBookingId'};
        }   
	
	if(isset($jsonData->{'paymentMode'}))
        {
           $paymentMode = $jsonData->{'paymentMode'};
        }   
	
	if(isset($jsonData->{'invoiceId'}))
        {
           $invoiceId = $jsonData->{'invoiceId'};
        }   
	
	if(isset($jsonData->{'paymentId'}))
        {
           $invoicePaymentId = $jsonData->{'paymentId'};
        }   

	$bookingId = 0;
	$spaceOwnerId = 0;
	
	//issue113: fix
	$this->loadModel('SyncUpdate');
        $syncTableDS = $this->SyncUpdate->getdatasource();
	$syncTableDS->begin();

	
	//TODO: this function has way too many parameters. needs refactoring
        if($this->_createBookingCommonSteps($spaceId,$name,$email,$mobile,$bookingStatus,$bookingStartDateTime,$bookingEndDateTime,
		$passIssueDate,$appGeneratedBookingId,$encodedPassCode,$serialNumber,$vehicleReg,$vehicleType,$numBookedSlots,$parkingFees,$discount,
                $isRenewal,$parentBookingId,$paymentMode,$invoiceId,$invoicePaymentId,$operatorId,$isPassForStaff,$bookingId,$spaceOwnerId))
	{
	   $this->loadModel('SyncUpdate');
	   $updateType = Configure::read('UpdateType.operatorApp_booking_update');

	   $syncId = $this->SyncUpdate->generateSyncId($spaceId,$bookingId,$updateType);

	   if($syncId != -1){

                   $syncTableDS->commit();

		   $response['status'] = 1;
		   $response['reason'] = "Booking Recorded"; 
		   $response['syncId'] = $syncId; 

		   $this->set('data',$response);
		   $this->set('_serialize','data');

		   //Step.5 Send an update to other apps
		   $this->loadModel('WidgetToken');
		   $tokenList = $this->WidgetToken->getAppTokens($spaceId,$gcmIdAtServer);
		   if(!empty($tokenList)){
			   $actionSpecificParams['action']                  = Configure::read('Action.operator_app_generated_booking_update');
			   $actionSpecificParams['bookingId']               = $appGeneratedBookingId;
			   $actionSpecificParams['name']                    = $name;
			   $actionSpecificParams['email']                   = $email;
			   $actionSpecificParams['mobile']                  = $mobile;
			   $actionSpecificParams['bookingStatus']           = $bookingStatus;
			   $actionSpecificParams['bookingStartDateTime']    = $bookingStartDateTime;
			   $actionSpecificParams['bookingEndDateTime']      = $bookingEndDateTime;
			   $actionSpecificParams['passIssueDate']           = $passIssueDate;
			   $actionSpecificParams['encodedPassCode']         = $encodedPassCode;
			   $actionSpecificParams['serialNumber']            = $serialNumber;
			   $actionSpecificParams['vehicleReg']              = $vehicleReg;
			   $actionSpecificParams['vehicleType']             = $vehicleType;
			   $actionSpecificParams['numBookedSlots']          = $numBookedSlots;
			   $actionSpecificParams['parkingFees']             = $parkingFees;
			   $actionSpecificParams['discount']                = $discount;
			   $actionSpecificParams['spaceOwnerId']            = $spaceOwnerId;
			   $actionSpecificParams['syncId']                  = $syncId;
			   $actionSpecificParams['isRenewal']               = $isRenewal;
			   $actionSpecificParams['parentBookingId']         = $parentBookingId;
			   $actionSpecificParams['paymentMode']             = $paymentMode;
			   $actionSpecificParams['invoiceId']               = $invoiceId;
			   $actionSpecificParams['paymentId']               = $invoicePaymentId;
			   $actionSpecificParams['operatorId']              = $operatorId;
			   $this->loadModel('Operator');
			   $actionSpecificParams['operatorDesc'] = $this->Operator->getOperatorDesc($operatorId,$spaceId);
			   $actionSpecificParams['isPassForStaff']  = $isPassForStaff;

			   CakeLog::write('debug','GCM update in createBooking ' . print_r($actionSpecificParams,true)); 
			   $this->notifyAllApps($spaceId,$tokenList,$actionSpecificParams); 
		   }
	   }else{
		   //issue113
		   $syncTableDS->rollback();
		   $response['status'] = 0;
		   $response['reason'] = "Booking Save Failed(SyncTable Generation Failed)"; 
		   $this->set('data',$response);
		   $this->set('_serialize','data');
	   }
	}
	else
	{
		//issue113
		$syncTableDS->rollback();
		$response['status'] = 0;
		$response['reason'] = "Booking Save Failed"; 
		$this->set('data',$response);
		$this->set('_serialize','data');
	}
}

function _createBookingCommonSteps($spaceId,$name,$email,$mobile,$bookingStatus,
	                           $bookingStartDateTime,$bookingEndDateTime,$passIssueDate,
			           $appGeneratedBookingId,$encodedPassCode,$serialNumber,$vehicleReg,$vehicleType,
				   $numBookedSlots,$parkingFees,$discount,$isRenewal,$parentBookingId,
				   $paymentMode,$invoiceId,$invoicePaymentId,$operatorId = 0,$isPassForStaff = 0,
                                   &$bookingId,&$spaceOwnerId)
{
	//Step.1 Find or create the User
	$this->loadModel('User');
	$userInfo = $this->User->getUserInfo($email);

	$userId = 0;
	$namesArray = explode(" ",$name);
        
        if(count($namesArray) == 1){
              $namesArray[1] = " ";
        }

        //Step 1. Find the User if exists, otherwise create one
        if(empty($userInfo))
	{
           //create a new user
            $userId = $this->createUser($namesArray[0],$namesArray[1],$email,$mobile);
	}else{
	   $userId = $userInfo['User']['id'];
           CakeLog::write('debug','User Found...' . print_r($userInfo,true));
	}

	//Step.2 Find an empty slot
	$spaceParkId = 0;
	if(!$this->_getEmptySlot($numBookedSlots,$spaceId,$bookingStartDateTime,$bookingEndDateTime,$spaceParkId)){
		CakeLog::write('debug','createBooking. Could not find an empty slot for the booking received from App');
	}

	//Step.3 setup the vehicle Type
	
	if($vehicleType == Configure::read('AppVehicleType.Car')){
		$carType = Configure::read('VehicleType.Hatchback');
	}else if($vehicleType == Configure::read('AppVehicleType.Bike')){
		$carType = Configure::read('VehicleType.Bike');
	}else if($vehicleType == Configure::read('AppVehicleType.MiniBus')){
		$carType = Configure::read('VehicleType.MiniBus');
	}else if($vehicleType == Configure::read('AppVehicleType.Bus')){
		$carType = Configure::read('VehicleType.Bus');
	}

        //Step.4 Find the pass to be activated based on encodedPassCode 
        $isPassActivated = false;
	$this->loadModel('LongTermBookingPass'); 
	CakeLog::write('debug','LongTermBookingPass...' . $encodedPassCode);
	$passInfo = $this->LongTermBookingPass->getPassInfo($spaceId,$encodedPassCode);
	//$log = $this->LongTermBookingPass->getDataSource()->getLog(false, false);
	//CakeLog::write('debug',print_r($log,true));

        if(!empty($passInfo)){
              $isPassActivated = true;
	      $bookingRecord['Booking']['qrcodefilename']    = $passInfo['LongTermBookingPass']['qr_file_name']; 
	      $bookingRecord['Booking']['serial_num']        = $passInfo['LongTermBookingPass']['serial_num'];
	      $bookingRecord['Booking']['encode_pass_code']  = $encodedPassCode;
              $this->LongTermBookingPass->setPassAsUsed($encodedPassCode);
        }else{
	      $bookingRecord['Booking']['qrcodefilename']    = ""; 
	      $bookingRecord['Booking']['serial_num']        = $serialNumber;
	      $bookingRecord['Booking']['encode_pass_code']  = $encodedPassCode;

        } 

	//Step.5 Save the booking
        $spaceOwnerId = $this->getSpaceOwnerId($spaceId);
	$bookingRecord['Booking']['user_id']                                      = $userId; 
	$bookingRecord['Booking']['space_user_id']                                = $spaceOwnerId;
	$bookingRecord['Booking']['space_id']                                     = $spaceId; 
	$bookingRecord['Booking']['first_name']                                   = $namesArray[0] ; 		
	$bookingRecord['Booking']['last_name']                                    = $namesArray[1]; 		
	$bookingRecord['Booking']['mobile']                                       = $mobile; 		
	$bookingRecord['Booking']['email']                                        = $email; 		
	$bookingRecord['Booking']['user_car_id']                                  = $this->getOrCreateUserCarId($userId,$vehicleReg,$carType); 		
	$bookingRecord['Booking']['start_date']                                   = $bookingStartDateTime; 		
	$bookingRecord['Booking']['end_date']                                     = $bookingEndDateTime; 
	$bookingRecord['Booking']['space_park_id']                                = $spaceParkId; 
	$bookingRecord['Booking']['num_slots']                                    = $numBookedSlots; 		
	$bookingRecord['Booking']['booking_type']                                 = $this->_getBookingType($bookingStartDateTime,$bookingEndDateTime); 
	$bookingRecord['Booking']['space_price']                                  = $parkingFees + $discount;
	$bookingRecord['Booking']['booking_price']                                = $parkingFees; 		
	$bookingRecord['Booking']['owner_income']                                 = $parkingFees; 		
	$bookingRecord['Booking']['total_installments']                           = 0; 		
	$bookingRecord['Booking']['operator_id']                                  = $operatorId; 		
	$bookingRecord['Booking']['status']                                       = Configure::read('BookingStatus1.Approved'); 		
	$bookingRecord['Booking']['created']                                      = $passIssueDate; 		
	$bookingRecord['Booking']['modified']                                     = date("Y-m-d H:i"); 		
    $bookingRecord['Booking']['is_pass_for_staff']   						  = $isPassForStaff; 

        /*Transactions*/	
	$bookingRecord['Transaction']['payment_id']         = $appGeneratedBookingId;
	$bookingRecord['Transaction']['payment_mode']       = $paymentMode;
	$bookingRecord['Transaction']['invoice_id']         = $invoiceId;
	$bookingRecord['Transaction']['invoice_payment_id'] = $invoicePaymentId;


        if($isRenewal)
        {
	   $bookingRecord['Transaction']['is_renewal'] = 1;
	   $bookingRecord['Transaction']['parent_transaction_id'] = $parentBookingId;
        }else{
	   $bookingRecord['Transaction']['is_renewal'] = 0;
        }

        CakeLog::write('debug','Going to save Booking ...' . print_r($bookingRecord,true));


	if($this->Booking->saveAll($bookingRecord)){
		$bookingId = $this->Booking->getLastInsertId();
		return true;
	}else{
		$bookingId = -1;
		return false;
	}


}
function createUser($firstName, $lastName, $email, $mobile)
{
   $newUser = array();
   $newUser['User']['user_type_id'] = Configure::read('UserTypes.User');
   
   app::uses('String','Utility');
   $newUser['User']['activation_key'] = String::uuid();
   
   app::uses('CakeTime', 'Utility');
   $newUser['User']['persist_code'] = CakeTime::fromString(date('Y-m-d H:i:s'));
   $newUser['User']['is_activated'] = 1; //we activate the user by default

   //$newUser['User']['password'] = openssl_random_pseudo_bytes(10);
   $newUser['User']['password'] = "simplypark";
   $newUser['User']['email']    = $email;
   $newUser['User']['username'] = $email;

   $newUser['UserProfile']['first_name'] = $firstName;
   $newUser['UserProfile']['last_name']  = $lastName;
   $newUser['UserProfile']['mobile']     = $mobile;
   $newUser['UserProfile']['confirm_password'] = $newUser['User']['password'];
   $userId = 0;
   
   $this->loadModel('User');
   $this->User->unvalidate(array('old_password', 'confirm_password'));
   $this->UserProfile->unvalidate(array('company'));
   
   CakeLog::write('debug','createUser...' . print_r($newUser,true)); 
   if($this->User->saveAssociated($newUser)){
      //send email to the user regarding the monthly pass 
      $userId = $this->User->getLastInsertId();
   }else{
	$errors = $this->User->validationErrors;
        CakeLog::write('debug','createUser...ERRORS' . print_r($errors,true)); 
   }
 
   return $userId;

}

function getSpaceOwnerId($spaceId)
{
   $this->loadModel('Space');
   $spaceInfo = $this->Space->getSpaceOwner($spaceId);

   if(!empty($spaceInfo))
   {
      //CakeLog::write('debug','In getSpaceOwnerId ...' . print_r($spaceInfo,true));
      return $spaceInfo['Space']['user_id'];
   }else{
      CakeLog::write('error','Cound not find any user id for space ' . $spaceId);
      return 0;
   }

}

function getOrCreateUserCarId($userId,$vehicleReg,$vehicleType)
{
   $this->loadModel('UserCar');

   return $this->UserCar->addUpdateCar($userId,$vehicleReg,$vehicleType);

}


public function createInvoice($spaceId)
{
   $response = array();
   $jsonData = $this->request->input('json_decode');
   $amount  = $jsonData->{'amount'};
   $invoiceArgs = array(
               'customer' => array('name' => $jsonData->{'name'},
                                     'contact' => $jsonData->{'contact'},
                                     'email'  => $jsonData->{'email'}
                                    ), 
               'line_items' => array(array('amount' => $amount * 100,
                                     'name' => $jsonData->{'productName'},
                                     'description' => $jsonData->{'productDescription'},
                                     'quantity' => 1)
                                    ),
                'date' => time(),
                'currency' =>  "INR",
                'type' =>  "ecod",
                'view_less' => "1",
                'sms_notify'=> "1"
               );
   $failureReason = "";	
   $responseBody = $this->_createInvoice($invoiceArgs,$failureReason);

   //CakeLog::write('debug','createInvoiceForECod...' . print_r($response,true));

   if($responseBody != null)
   {
     $jsonResponse = json_decode($responseBody);
     $response['status'] = 0;
     $response['reason'] = 'Invoice created';
     CakeLog::write('debug','createInvoiceForECod...' . print_r($responseBody,true));
     $response['invoiceId'] = $jsonResponse->{'id'};
   }else{
     $response['status'] = 1;
     $response['reason'] = $reasonPhrase; 
   }
   
   $this->set('data',$response);
   $this->set('_serialize','data');
 
}

private function _createInvoice($data,&$reason)
{
  $socket = new HttpSocket();
  $socket->configAuth('Basic', Configure::read('RazorPayInvoiceKeys.KeyID'), Configure::read('RazorPayInvoiceKeys.KeySecret'));
  $uri = Configure::read('RazorPayKeys.INVOICEAPIURL');
  $jsonData = json_encode($data);
  //CakeLog::write('debug','_createInvoice...' . print_r($jsonData,true));
  $request = array(
        'header' => array('Content-Type' => 'application/json',
        ),
    );
  $response = $socket->post($uri,$jsonData,$request);
   
  //CakeLog::write('debug','_createInvoice...' . print_r($response,true));

  if($response->code == Configure::read('HTTPStatusCode.OK'))
  {
     return $response->body();   	
  }else{
     $reason = $response->reasonPhrase;
     return null;
  }
}


/****************************************************************************** 
 *Modifies booking created via the operator app
*******************************************************************************/
public function modifyBooking($spaceId)
{
	$response = array();
	$response['status'] = 1;
	$response['reason']  = "Booking Modified";

	$jsonData = $this->request->input('json_decode');

	$operatorId = $jsonData->{'operator_id'};
	$appGeneratedBookingId  = $jsonData->{'booking_id'};
	$gcmIdAtServer = $jsonData->{'gcm_id'};

        $oldEncodedPassCode = $jsonData->{'old_encoded_pass_code'};
        $newEncodedPassCode = $jsonData->{'new_encoded_pass_code'};
        $oldSerialNumber    = $jsonData->{'old_serial_number'};
        $newSerialNumber    = $jsonData->{'new_serial_number'};
        
        $oldPassIssueDateForDb  = $jsonData->{'old_pass_issue_date'};
        $oldPassIssueDateForGcm = $jsonData->{'old_pass_issue_date_timestamp'}; 
        
        $newPassIssueDateForDb  = $jsonData->{'new_pass_issue_date'};
        $newPassIssueDateForGcm = $jsonData->{'new_pass_issue_date_timestamp'}; 
	
        $paymentMode  = $jsonData->{'payment_mode'};
        $lostPassFees = $jsonData->{'lost_pass_fees'};

        //first get booking id from the appGeneratedBookingId
        $this->loadModel('Transaction');
        $bookingId = $this->Transaction->getBookingId($appGeneratedBookingId);
        
	$notifyOthers = false;
        
        if($bookingId != -1)
	{
		//issue113: fix
		$this->loadModel('SyncUpdate');
		$syncTableDS = $this->SyncUpdate->getdatasource();
		$syncTableDS->begin();
		
                $retVal = $this->Booking->modifyBooking($spaceId,$bookingId,
				$newEncodedPassCode,$newSerialNumber);


		if($retVal == Configure::read('ReturnTypes.OK'))
		{
			$updateType = Configure::read('UpdateType.operatorApp_booking_modification');

			//add a new LostPass Record 
			$recordId = -1;
			$this->loadModel('LostPass');
			$retVal =  $this->LostPass->addRecord($spaceId,$operatorId,$appGeneratedBookingId,
					$oldEncodedPassCode,$newEncodedPassCode,
					$oldPassIssueDateForDb,$newPassIssueDateForDb,
					$oldSerialNumber,$newSerialNumber,$lostPassFees,
					$paymentMode,$recordId);

			if($retVal == Configure::read('ReturnTypes.OK'))
			{
				//generateSyncId
				$this->loadModel('SyncUpdate');
				$syncId = $this->SyncUpdate->generateSyncId($spaceId,$recordId,$updateType);

				if($syncId != -1){
                                        $syncTableDS->commit();
			                $notifyOthers = true;
					CakeLog::write('debug','Inside ModifiedBooking, SyncID generated is  ' . $syncId);	
					$response['syncId'] = $syncId;
				}else{
					$syncTableDS->rollback();
					$response['status'] = 0;
					$response['reason'] = 'Sync ID could not be genearted';
				}
			}else{
			        $syncTableDS->rollback();
				$response['status'] = 0;
				$response['reason'] = 'Add Record - Returned error';
			}
		}else if($retVal == Configure::read('ReturnTypes.Stale')){
                        $syncTableDS->rollback();
			//get Current SyncId
			$this->loadModel('SyncUpdate');
			$syncId = $this->SyncUpdate->getCurrentSyncId($spaceId);
			CakeLog::write('debug','Inside getCurrentSyncId  ' . $syncId);	
			$response['syncId'] = $syncId;
			$response['status'] = 2;
			$response['reason'] = 'Stale';
		}else{
                        $syncTableDS->rollback();
			$response['status'] = 0;
			if($retVal == Configure::read('ReturnTypes.NoSuchRecord')){
				$response['substatus'] = 1;
				$response['reason'] = 'No such booking';
			}else{
				$response['reason'] = 'Unknown error';
			}
		}
	}else{
           $syncTableDS->rollback();
	   $response['status'] = 0;
	   $response['substatus'] = 1;
	   $response['reason'] = 'No such booking';
        }
        
        CakeLog::write('debug','Inside Modify Booking :::' . print_r($response,true));	
	$this->set('data',$response);
        $this->set('_serialize','data');

        if($notifyOthers){
	   $this->loadModel('WidgetToken');
	   $tokenList = $this->WidgetToken->getAppTokens($spaceId,$gcmIdAtServer);
	   if(!empty($tokenList)){
		   $actionSpecificParams['action']             = Configure::read('Action.operator_app_generated_booking_modification');
		   $actionSpecificParams['bookingId']          = $bookingId;
		   $actionSpecificParams['oldEncodedPassCode'] = $oldEncodedPassCode;  
		   $actionSpecificParams['newEncodedPassCode'] = $newEncodedPassCode;
		   $actionSpecificParams['oldPassIssueDate']   = $oldPassIssueDateForGcm;
		   $actionSpecificParams['newPassIssueDate']   = $newPassIssueDateForGcm;
		   $actionSpecificParams['oldSerialNumber']    = $oldSerialNumber;
		   $actionSpecificParams['newSerialNumber']    = $newSerialNumber;
		   $actionSpecificParams['paymentMode']        = $paymentMode;
		   $actionSpecificParams['lostPassFees']       = $lostPassFees;
		   $actionSpecificParams['spaceId']            = $spaceId;
		   $actionSpecificParams['syncId']             = $response['syncId'];
		   
		   $this->loadModel('Operator');
                   $operatorDesc = $this->Operator->getOperatorDesc($operatorId,$spaceId);
		   $actionSpecificParams['operatorDesc'] = $operatorDesc;
		   $actionSpecificParams['operatorId']   = $operatorId;
		   
		   $this->notifyAllApps($spaceId,$tokenList,$actionSpecificParams); 
        }

      }
}


public function handleModifyBookingPushedRecord($transaction,$spaceId,$operatorId,&$isStale)
{
	$appGeneratedBookingId  = $transaction['booking_id'];

	$oldEncodedPassCode = $transaction['old_encoded_pass_code'];
	$newEncodedPassCode = $transaction['new_encoded_pass_code'];
	$oldSerialNumber    = $transaction['old_serial_number'];
	$newSerialNumber    = $transaction['new_serial_number'];

	$oldPassIssueDateForDb  = $transaction['old_pass_issue_date'];
	$newPassIssueDateForDb  = $transaction{'new_pass_issue_date'};

	$paymentMode  = $transaction['payment_mode'];
	$lostPassFees = $transaction['lost_pass_fees'];

	//first get booking id from the appGeneratedBookingId
	$this->loadModel('Transaction');
	$bookingId = $this->Transaction->getBookingId($appGeneratedBookingId);

	if($bookingId != -1)
	{
		$retVal = $this->Booking->modifyBooking($spaceId,$bookingId,
			$newEncodedPassCode,$newSerialNumber);


		if($retVal == Configure::read('ReturnTypes.OK'))
		{
			$updateType = Configure::read('UpdateType.operatorApp_booking_modification');

			//add a new LostPass Record 
			$recordId = -1;
			$this->loadModel('LostPass');
			$retVal =  $this->LostPass->addRecord($spaceId,$operatorId,$appGeneratedBookingId,
				$oldEncodedPassCode,$newEncodedPassCode,
				$oldPassIssueDateForDb,$newPassIssueDateForDb,
				$oldSerialNumber,$newSerialNumber,$lostPassFees,
				$paymentMode,$recordId);

			if($retVal == Configure::read('ReturnTypes.OK'))
			{
				return $recordId;
			}else{
				return -1;
			}
		}else if($retVal == Configure::read('ReturnTypes.Stale')){
			$isStale = 1;
			return -1;
		}else{
			return -1;
		}
	}else{
		return -1;
	}


}



}
