<?php
class EmailTemplatesController extends AppController
{
	public $helper = array('Html', 'Form','Fck');
	public $components = array('Paginator');
	public $uses = array('EmailTemplate');

	public function beforeFilter() {
		parent::beforeFilter();
	}

/**
 * Method admin_index to display all categories
 *
 * @return void 
 */
	public function admin_index() {
		$this->layout = 'backend';		
		$this->Paginator->settings = array(
										'conditions' => array(
											'EmailTemplate.is_deleted' => 0
											),
										'limit' => 10,
										'order' => 'EmailTemplate.id Asc'
									);
		$emailTemplates = $this->Paginator->paginate('EmailTemplate');
		$this->set('emailTemplates',$emailTemplates);
	}

/**
 * Method admin_editTemplate to save upadted records
 *
 * @return void 
 */
	public function admin_editTemplate($templateID=null) {
		if(isset($this->request->data) && !empty($this->request->data)) {
			if ($this->EmailTemplate->save($this->request->data)) {
				$this->Session->setFlash('Email template updated successfully.', 'default', 'success');
				$this->redirect(array('action' => 'index', 'admin' => true));
			} else {
				$errors = $this->EmailTemplate->validationErrors;
	            if (!empty($errors)) {
	                $errorMsg = $this->_setValidaiotnError($errors);
	            }
				$this->Session->setFlash(__('Email template update request not completed due to following : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
			}
			$this->redirect($this->referer());
		}
		$this->layout = 'backend';
		$templateID = base64_decode($templateID);
		$template = $this->EmailTemplate->findById($templateID, array('id', 'template_used_for', 'subject' ,'mail_body'));
		$this->set('template',$template);
	}
}
