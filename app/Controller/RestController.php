<?php

App::import('Controller','VisionApis');
class RestController extends AppController
{
	public $components = array('RequestHandler', 'Paginator');


	public function beforeFilter() {
		//parent::beforeFilter();
		$this->Auth->authenticate = array(
			                     'Basic' => array('fields' => array('username' => 'username', 'password' => 'password')));
		//$this->Auth->allow('add','count','detect','parkings');
                $this->RequestHandler->ext = 'json';
	}


	/******************************************************************************
	 * URI: POST https://www.simplypark.in/blob/<spaceId>/<action>/<timestamp>/<device>
	 *
	 * ****************************************************************************/

	public function add($spaceId,$action,$timestamp,$device)
	{
		$response = array();
		$response['status'] = 1;

		//CakeLog::write('debug',print_r($this->request,true));
		CakeLog::write('debug','File Name ' . $this->request->params['form']['image']['tmp_name']);

		$savedFile = "";
		if($this->_saveBlob($spaceId,$action,$timestamp,$device,
			            $this->request->params['form']['image']['tmp_name'],
		                    $savedFile)){
		  $this->set('data',$response);
		  $this->set('_serialize','data');

		  CakeLog::write('debug','In function add, returned TRUE');

		  //Post a job to the worker thread 
		  $params = array();
		  $params['spaceId' ]  = $spaceId;
		  $params['action']    = $action;
		  $params['timestamp'] = $timestamp;
		  $params['device']    = $device;
		  $params['blob']      = $savedFile;

		  $this->loadModel('Queue.QueuedTask');
		  if($this->QueuedTask->createJob('Annotate',$params)){
		     CakeLog::write('debug','RestController - ADD, task creation successful');	  
		  }else{
		     CakeLog::write('debug','RestController - FAIL, task creation successful');	  
		  }
		}else{
		  throw new BadRequestException('Server Error. Please try again later', 501);
		}
	}	
	
	/******************************************************************************
	 * 
	 * URI: POST https://www.simplypark.in/annotate/<spaceId>/<parkingId>/<action>/<timestamp>/<device>
	 *
	 * ****************************************************************************/

	public function detect($spaceId,$device,$action,$parkingid,$timestamp)
	{
		$response = array();
		$response['status'] = 1;

		//CakeLog::write('error',print_r($this->request,true));
		CakeLog::write('debug','File Name ' . $this->request->params['form']['image']['tmp_name']);

		$savedFile = "";
		if($this->_saveBlob($spaceId,$action,$timestamp,$device,
			            $this->request->params['form']['image']['tmp_name'],
		                    $savedFile)){

		  CakeLog::write('debug','In function add, returned TRUE');

		  //Post a job to the worker thread 
		  $params = array();
		  $params['spaceId' ]  = $spaceId;
		  $params['action']    = $action;
		  $params['timestamp'] = $timestamp;
		  $params['device']    = $device;
		  $params['blob']      = $savedFile;

		  $result = array();
		  
		  $controller = new VisionApisController();
		  //this function is also called as part of post request and therefore
		  //always returns true to indicate the task is done. Therefore, we are 
		  //passing an additional parameter result which will contain the result
		  //of the detection
		  $controller->detect($savedFile,$spaceId,$device,$timestamp,$action,$result);
		
                  CakeLog::write('error','Result ' . print_r($result,true));
           
		  if($result['result'] != -1){
			  if($result['result'] != Configure::read('DetectionResult.Success')){
				  $response['status']      = 0;
			          $response['reason']      = $result['result']; 
	                  }
			  $response['vehicleType']     = $result['vehicleType'];
			  $response['regNumber']       = $result['regNumber'];
			  $response['parkingId']       = $result['parkingid'];
			  
			  $this->set('data',$response);
			  $this->set('_serialize','data');
		  }else{
			  throw new BadRequestException('Server Error. Please try again later', 501);
		  }
	      }else{
			  throw new BadRequestException('Server Error. Please try again later', 501);
	      }
	}



	/*******************************************************************************
	 * URI : GET https://www.simplypark.in/count 
	 *
	 * ****************************************************************************/   
	public function count($spaceId)
	{
		$response = array();
		$response['status'] = 1;
		$records = array();

		$this->set('data',$response);
		$this->set('_serialize','data');
	}
	
	
	/******************************************************************************
	 *
	 *   PARKING API  - SIMPLYPARK v1
	 *
	 *
	 *
	 * *****************************************************************************/
	
	/*******************************************************************************
	 * URI : GET https://www.simplypark.in/v1/parkings?lat=LAT&lng=LNG&radius=XX (in meteres)
	 *
	 * ****************************************************************************/   
	public function parkings($id=null)
	{
		$response = array();
		$response['status'] = 0;
		$records = array();


                CakeLog::write('debug','Inside parkings, id = ' . $id);
		if($id != null){
			//detail code goes here
			CakeLog::write('debug','Inside detail');
	                $this->loadModel('Parking');
			$detail = $this->Parking->detail($id);

			if(empty($detail)){
				$response['status'] = 1;
				$response['reason'] = 'Invaid Parking ID';
			}else{
                                $response['detail'] = $detail['Parking'];
			}
		}else{

			$lat    = $this->request->query('lat');	
			$lng    = $this->request->query('lng');	
			$radius = $this->request->query('radius');

			if ($lat == null or $lng == null){
				$lat    = Configure::read('DefaultLat');
				$lng    = Configure::read('DefaultLng');
			}

			if ($radius == null){
				$radius = Configure::read('radius');	  
			}

			CakeLog::write('debug','Inside parkings...distance is ' . $radius);
			CakeLog::write('debug','Inside parkings...lat is ' . $lat);
			CakeLog::write('debug','Inside parkings...lng is ' . $lng);

	                $this->loadModel('Parking');
			$parkings = $this->Parking->parkings($lat,$lng,$radius);

			if(empty($parkings)){
				$response['status'] = 1;
				$response['reason'] = 'No Parkings Found or your license does not cover this area.';
			}else{
				$response['count'] = count($parkings);
				$response['parkings'] = array() ;
				foreach($parkings as $parking){
					$filter = array();
					
					$filter['id']          = $parking['Parking']['id'];
					$filter['short_name']  = $parking['Parking']['short_name'];
					$filter['lat']         = $parking['Parking']['lat'];
					$filter['lng']         = $parking['Parking']['lng'];
					$filter['distance']    = $parking['0']['distance'] * 1000;
					$response['parkings'][] = $filter;
					//CakeLog::write('debug','Inside loop...' . print_r($parking,true));
				}

			}
		}

		$this->set('data',$response);
		$this->set('_serialize','data');
	}

}
