<?php

/**
 * This file gets all the users who are registered in the current date
 * and an email containg the count of total registered users
 * and containing every user's small information, goes to the administrator on daily basis
 *
*/

App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');
class NewRegisterationShell extends AppShell {
    public $uses = array('User', 'EmailTemplate');

	public function main() {
		$newUsers = $this->User->getNewRegisteredUsers();
//		pr($newUsers);die;
		if (!empty($newUsers)) {
			$this->_emailTotalUsers($newUsers);
		}
		return true;
	}

/**
 * Method _emailTotalUsers to send email containg the information for total count of new users
 * and every user's small information
 *
 * @param $allUsers array containing all user's details
 * @return bool
 */
	protected function _emailTotalUsers($allUsers) {

		$users = '';
		foreach ($allUsers as $setUsers) {
			$users .= '<div><b>Name: </b>'.$setUsers['UserProfile']['first_name'].' '.$setUsers['UserProfile']['last_name'].'</br>';
			$users .= '<b>Email: </b>'.$setUsers['User']['email'].'</br></div><hr></hr>';
		}

	   	$temp = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => Configure::read('EmailTemplateId.newly_registered_users'))));
	   	$temp['EmailTemplate']['mail_body'] = str_replace(
										    	array(
										    			'../../..',
										    			'#DATE',
										    			'#COUNTUSER',
										    			'#DETAIL'
										    		),
										    	array(
										    			FULL_BASE_URL,
										    			date('Y-m-d'),
										    			count($allUsers),
										    			$users
										    		),
										    	$temp['EmailTemplate']['mail_body']
										    );
		$emailTo = Configure::read('Email.EmailAdmin');
		
	   	$this->Apps = new AppController(new CakeRequest());
		
		return $this->Apps->_sendEmailMessage($emailTo, $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject']);
	}
}