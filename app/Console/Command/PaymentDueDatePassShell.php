<?php
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('HttpSocket', 'Network/Http');
App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');
class PaymentDueDatePassShell extends AppShell {
    public $uses = array('BookingInstallment', 'UserProfile', 'Message');

	public function main() {
		$dueInstallments = $this->BookingInstallment->getPassedInstallments();
		array_map(array($this, '_sendPassDateReminder'), $dueInstallments);
		return true;
	}

/**
 * Method _sendPassDateReminder to send installment reminder to user who booked parking space for long temr parkings
 *
 * @param $bookingDetail array containing booking installment details and booking details
 * @return bool
 */
	protected function _sendPassDateReminder($bookingDetail) {
		
		if (Configure::read('SMSNotifications')) {
			$this->__smsForDueDatePassed($bookingDetail['Booking']['user_id'],'installment_payment_overdue_to_driver');
			$this->__smsForDueDatePassed($bookingDetail['Booking']['space_user_id'],'installment_payment_overdue_to_owner');
			$this->__smsForDueDatePassedToAdmin();
		}

	}

/**
 * Method __smsForDueDatePassed send message to user, owner for pay installment due date has been passed.
 *
 * @param $userID int the user id of the user to get mobile number
 * @return bool
 */
    private function __smsForDueDatePassed($userID = null, $messageID = null) {
        $userMobileNumber = $this->UserProfile->getUserName($userID); //getting user mobile number

        //getting message body
        $messageContent = $this->Message->getMessage(Configure::read('MessageId.'.$messageID));


		$messageContent['Message']['body'] = str_replace(
            array('#USER'),
            array(
                    $userMobileNumber['UserProfile']['first_name'].' '.$userMobileNumber['UserProfile']['last_name']
                ), $messageContent['Message']['body']);

        $this->Apps = new AppController(new CakeRequest());
        $messageResponse =$this->Apps->_sendMessageNotification($userMobileNumber['UserProfile']['mobile'],$messageContent['Message']['body']);

        if ($messageResponse->code == Configure::read('HTTPStatusCode.OK')) {
            return true;
        }
        return false;
    }

/**
 * Method __smsForDueDatePassed send message to admin for pay installment due date has been passed.
 *
 * @param $userID int the user id of the user to get mobile number
 * @return bool
 */
    private function __smsForDueDatePassedToAdmin() {
        $userMobileNumber = $this->UserProfile->getUserName(Configure::read('AdminUserID')); //getting user mobile number

        //getting message body
        $messageContent = $this->Message->getMessage(Configure::read('MessageId.installment_payment_overdue_to_admin'));

        $this->Apps = new AppController(new CakeRequest());
        $messageResponse =$this->Apps->_sendMessageNotification($userMobileNumber['UserProfile']['mobile'],$messageContent['Message']['body']);

        if ($messageResponse->code == Configure::read('HTTPStatusCode.OK')) {
            return true;
        }
        return false;
    }
}