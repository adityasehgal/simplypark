<?php

/**
 * This shell is used to get the due on simplypark to pay to owner after 48 hours of booking start date
 * And email to admin for each booking
 *
 */


App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');

class DueOnSimplyParkShell extends AppShell {
    public $uses = array('Booking','EmailTemplate');

	public function main() {
		$getDues = $this->Booking->getDueOnSimplyPark();
        array_map(array($this, '_sendDueEamilToAdmin'), $getDues);
		return true;
	}

/**
 * Method _deactivateEvents to deactivate all the events which are not in the non expired list of event ids
 *
 * @param $nonExpiredEvents array containing the list of no expired event ids
 * @return bool
 */
	protected function _sendDueEamilToAdmin($Bookings = array()) {
        
        $temp = $this->EmailTemplate->find(
                                        'first',
                                        array
                                        (
                                            'conditions' => array
                                                                (
                                                                    'EmailTemplate.id' => Configure::read(
                                                                                            'EmailTemplateId.pay_to_owner'
                                                                                            )
                                                                )
                                        )
                                    );
        $temp['EmailTemplate']['mail_body'] = str_replace(
                                                array(
                                                        '../../..',
                                                        '#SPACENAME',
                                                        '#NAME',
                                                        '#EMAIL',
                                                        '#STARTTIME',
                                                        '#ENDTIME',
                                                        '#PAYTOOWNER'
                                                    ),
                                                array(
                                                        FULL_BASE_URL,
                                                        $Bookings['Space']['name'],
                                                        $Bookings['Space']['User']['UserProfile']['first_name'].' '.$Bookings['Space']['User']['UserProfile']['last_name'],
                                                        $Bookings['Space']['User']['email'],
                                                        $Bookings['Booking']['start_date'],
                                                        $Bookings['Booking']['end_date'],
                                                        $Bookings[0]['pay_to_owner'],
                                                    ),
                                                $temp['EmailTemplate']['mail_body']
                                            );

        $this->Apps = new AppController(new CakeRequest());
        
        return $this->Apps->_sendEmailMessage(Configure::read('Email.EmailAdmin'), $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject']);
	}
}