<?php
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('HttpSocket', 'Network/Http');
App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');
class BookingExpireShell extends AppShell {
    public $uses = array('Booking', 'UserProfile', 'Message');

	public function main() {
		$expiredBookings = $this->Booking->getExipedBooking();
        array_map(array($this, '_sendExpiredBookingReminder'), $expiredBookings);
		return true;
	}

/**
 * Method _sendExpiredBookingReminder to send booking expired messages
 *
 * @param $bookingDetail array containing booking installment details and booking details
 * @return bool
 */
	protected function _sendExpiredBookingReminder($bookingDetail) {

        $this->Booking->updateAll(
                            array(
                                    'Booking.'.Configure::read('BookingTableStatusField') => Configure::read('BookingStatus.Expired')
                                ),
                            array(
                                    'Booking.id' => $bookingDetail['Booking']['id']
                                )
                        );
		return true;
		if (Configure::read('SMSNotifications')) {
			$this->__smsForExpiredBooking($bookingDetail['Booking']['user_id'],'booking_expire_for_listed_space_to_driver');
			$this->__smsForExpiredBooking($bookingDetail['Booking']['space_user_id'],'booking_expire_for_listed_space_to_owner');
		}
	}

/**
 * Method __smsForExpiredBooking send message to user, owner for booking expired.
 *
 * @param $userID int the user id of the user to get mobile number
 * @return bool
 */
    private function __smsForExpiredBooking($userID = null, $messageID = null) {
        $userMobileNumber = $this->UserProfile->getUserName($userID); //getting user mobile number

        //getting message body
        $messageContent = $this->Message->getMessage(Configure::read('MessageId.'.$messageID));

		$messageContent['Message']['body'] = str_replace(
            array('#USER'),
            array(
                    $userMobileNumber['UserProfile']['first_name'].' '.$userMobileNumber['UserProfile']['last_name']
                ), $messageContent['Message']['body']);

        $this->Apps = new AppController(new CakeRequest());

        $messageResponse =$this->Apps->_sendMessageNotification($userMobileNumber['UserProfile']['mobile'],$messageContent['Message']['body']);

        if ($messageResponse->code == Configure::read('HTTPStatusCode.OK')) {
            return true;
        }
        return false;
    }
}