<?php
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('HttpSocket', 'Network/Http');
App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');
class ReminderDueInstallmentShell extends AppShell {
    public $uses = array('BookingInstallment', 'EmailTemplate', 'UserProfile', 'Message');

    public function main() {
	        //CakeLog::write('debug','Inside main in shell');
		$dueInstallments = $this->BookingInstallment->getDueInstallments();
		//		pr($dueInstallments);die;
		//CakeLog::write('debug',print_r($dueInstallments,true));
		//$log = $this->BookingInstallment->getDataSource()->getLog(false, false);
                //debug($log);
		array_map(array($this, '_sendReminder'), $dueInstallments);
		return true;
	}

/**
 * Method _sendReminder to send installment reminder to user who booked parking space for long temr parkings
 *
 * @param $bookingDetail array containing booking installment details and booking details
 * @return bool
 */
protected function _sendReminder($bookingDetail) 
{
        //CakeLog::write('debug','Inside _sendReminder..' . print_r($bookingDetail,true));
 	$address2 = '';
	if (!empty($bookingDetail['Booking']['Space']['address2'])) {
		$address2 = $bookingDetail['Booking']['Space']['address2'].', ';
	}
 
	$allInstallments = $this->BookingInstallment->getAllInstallmentsForBookingId($bookingDetail['Booking']['id']);
 
	$paymentSchedule = "";
	$monthData = "<tr>";
	$paymentData ="<tr align=\"center\">";
	//make the th 
	foreach($allInstallments as $installment)
	{
           $currentInstallmentFound = 0;
	     
	   //CakeLog::write('debug','Payment schedule ' . print_r($installment,true));
           if($installment['BookingInstallment']['status']){
             $monthData .= "<th bgcolor=\"#C0C0C0\">";
	     $monthData .= date("jS-M",strtotime($installment['BookingInstallment']['date_pay']));
	     $monthData .="</th>";

	     $paymentData .="<td bgcolor=\"#C0C0C0\">" . $installment['BookingInstallment']['amount'] ."</td>";
	   }else{  
	     
	     if($currentInstallmentFound == 0 && 
	        (strcmp($installment['BookingInstallment']['date_pay'],$bookingDetail['BookingInstallment']['date_pay']) == 0))
	     {
                $monthData .= "<th bgcolor=\"#66FF66\">";
	        $monthData .= date("jS-M",strtotime($installment['BookingInstallment']['date_pay']));
                $monthData .= "*";
	        $paymentData .="<td bgcolor=\"#66FF66\">" . $installment['BookingInstallment']['amount'] ."</td>";
		$currentInstallmentFound = 1;
	     }else{
                $monthData .= "<th>";
	        $monthData .= date("jS-M",strtotime($installment['BookingInstallment']['date_pay']));
	        $paymentData .="<td>" . $installment['BookingInstallment']['amount'] ."</td>";
	     }
	     $monthData .="</th>";
	   }
	}
	$monthData .="</tr>";
	$paymentData .="</tr>";

        $paymentSchedule = $monthData . $paymentData;
	
	$link = Configure::read('ROOTURL').'bookings/payInstallment/' .urlencode(base64_encode($bookingDetail['BookingInstallment']['id']));

	$temp = $this->EmailTemplate->find('first', 
		                           array('conditions' => 
					            array('EmailTemplate.id' => Configure::read('EmailTemplateId.installment_remidmer'))));

	$temp['EmailTemplate']['mail_body'] = str_replace(
		array(
			'../../..',
			'#LASTDATE',
			'#NAME',
			'#SPACENAME',
			'#ADDRESS',
			'#STARTTIME',
			'#ENDTIME',
			'#AMOUNT',
			'#CLICKHERE',
			'#SCHEDULE'
		),
		array(
			FULL_BASE_URL,
			date("jS-M-y",strtotime($bookingDetail['BookingInstallment']['date_pay'])),
			$bookingDetail['Booking']['User']['UserProfile']['first_name'].' '.$bookingDetail['Booking']['User']['UserProfile']['last_name'],
			$bookingDetail['Booking']['Space']['name'],
			$bookingDetail['Booking']['Space']['flat_apartment_number'].', '.$bookingDetail['Booking']['Space']['address1'].', '.$address2.$bookingDetail['Booking']['Space']['post_code'],
			$bookingDetail['Booking']['start_date'],
			$bookingDetail['Booking']['end_date'],
			$bookingDetail['BookingInstallment']['amount'],
			$link,
			$paymentSchedule
		),
		$temp['EmailTemplate']['mail_body']
	);
	$this->Apps = new AppController(new CakeRequest());

	$emailTo = $bookingDetail['Booking']['User']['email'];
	$ccTo = array(
		$bookingDetail['Booking']['Space']['User']['email'],
		Configure::read('Email.EmailAdmin')
	);
	$this->Apps->_sendEmailMessage($emailTo, $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject'],null,$ccTo);

	//if ($bookingDetail['BookingInstallment']['date_pay'] == date('Y-m-d',strtotime('+'.Configure::read('ReminderInstallmentInterval').' days')) && Configure::read('SMSNotifications')) {
	if(Configure::read('SMSNotifications'))
	{
	   $this->__smsUserToDuePayment($bookingDetail['Booking']['user_id']);
	}

}

/**
 * Method __smsUserToDuePaymentto send message to user for pay installment reminder only once.
 *
 * @param $userID int the user id of the user to get mobile number
 * @return bool
 */
private function __smsUserToDuePayment($userID = null) 
{
	$userMobileNumber = $this->UserProfile->getUserName($userID); //getting user mobile number
	//CakeLog::write('debug','UserMobile Number ' . print_r($userMobileNumber,true));

	//getting message body
	$messageContent = $this->Message->getMessage(Configure::read('MessageId.due_installment'));

/*
	$messageContent['Message']['body'] = str_replace(
		array('#USER'),
		array(
			$userMobileNumber['UserProfile']['first_name'].' '.$userMobileNumber['UserProfile']['last_name']
		), $messageContent['Message']['body']);
 */
	$messageContent['Message']['body'] = str_replace(
		array('#USER'),
		array(
		      "User"	
		), $messageContent['Message']['body']);

	//CakeLog::write('debug','message Content ' . print_r($messageContent,true));

	$this->Apps = new AppController(new CakeRequest());
	$messageResponse =$this->Apps->_sendMessageNotification($userMobileNumber['UserProfile']['mobile'],$messageContent['Message']['body']);

	if ($messageResponse->code == Configure::read('HTTPStatusCode.OK')) {
		return true;
	}
	return false;
}
}
