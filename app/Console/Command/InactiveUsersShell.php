<?php

/**
 * This file gets all the users who are registered but have not activated their emails
 * and an sends the activation link to remind them to click again 
 *
*/

App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');
class InactiveUsersShell extends AppShell {
    public $uses = array('User', 'EmailTemplate');

	public function main() {
		$inactiveUsers = $this->User->getInactiveUsers();
                //CakeLog::write('debug', 'Inside inactiveUsers shell...');
		if (!empty($inactiveUsers)) {
			$this->_emailTotalUsers($inactiveUsers);
		}
		return true;
	}

/**
 * Method _emailTotalUsers to send email containg the information for total count of new users
 * and every user's small information
 *
 * @param $allUsers array containing all user's details
 * @return bool
 */
	protected function _emailTotalUsers($allUsers) {

		$users = '';
	   	$temp = $this->EmailTemplate->find('first', 
                                                   array('conditions' => array('EmailTemplate.id' => Configure::read('EmailTemplateId.registration_notification'))));
                $subject = '[REMINDER]Activate your SimplyPark account';
		
                foreach ($allUsers as $setUsers) 
                {
                    $perUserEmail = $temp;
		    $link = Configure::read('ROOTURL').'homes/activateAccount/' .$setUsers['User']['activation_key'];
	            $perUserEmail['EmailTemplate']['mail_body'] = str_replace(
	            	         array('../../..', '#NAME', '#CLICKHERE'),
	    	                 array(FULL_BASE_URL, 
                                 $setUsers['UserProfile']['first_name'].' '.$setUsers['UserProfile']['last_name'], 
                                 $link), $temp['EmailTemplate']['mail_body']);
	   	    $this->Apps = new AppController(new CakeRequest());
		    $this->Apps->_sendEmailMessage($setUsers['User']['email'], $perUserEmail['EmailTemplate']['mail_body'], $subject);
		}
	    return 1;	
	}
}
