<?php

/**
 * This shell is used to make all the expired events deactivate and unpopular.
 * This shell will execute once in 12 hours
 *
 */

class EventDisapproveShell extends AppShell {
    public $uses = array('Event','EventTiming');

	public function main() {
		$nonExpiredEvents = $this->EventTiming->getNonExpiredEvent();
		$this->_deactivateEvents($nonExpiredEvents);
		return true;
	}

/**
 * Method _deactivateEvents to deactivate all the events which are not in the non expired list of event ids
 *
 * @param $nonExpiredEvents array containing the list of no expired event ids
 * @return bool
 */
	protected function _deactivateEvents($nonExpiredEvents = array()) {
        $this->Event->updateAll(
        					array(
        							'Event.is_activated' => Configure::read('Bollean.False'),
        							'Event.is_popular' => Configure::read('Bollean.False')
        						),
        					array(
        							'NOT' => array(
        									'Event.id' => $nonExpiredEvents
        								)
        						)
        				);
        return true;
	}
}