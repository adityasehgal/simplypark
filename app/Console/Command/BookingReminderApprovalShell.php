<?php
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');
Class BookingReminderApprovalShell extends AppShell {
	public $uses = array('Booking', 'EmailTemplate');

	public function main() {
		$underApprovalBookings = $this->Booking->getUnderApprovalBookings();
		array_map(array($this, '_reminderApproval'), $underApprovalBookings);
		return true;
	}

/**
 * Method: _reminderApproval to check for send remider to space owner to approve booking space
 *
 * @param $pendingBookings array containing the detail about the booking space
 * @return bool
 */
protected function _reminderApproval($pendingBookings) 
{
	//if current date is less than booking start date then send remoder
	if (date('Y-m-d') < date('Y-m-d', strtotime($pendingBookings['Booking']['start_date']))) {
		$this->_sendReminderToApproveDisapproveEmail($pendingBookings,
                                                             Configure::read('EmailTemplateId.reminder_approval'),
                                                             1);
	} 
      elseif (date('Y-m-d') >= date('Y-m-d', strtotime($pendingBookings['Booking']['start_date']))) 
        {//if current date is equal or > to booking start date then auto disapprove space
		$this->_autoDisapprove($pendingBookings);
	}
	
        return true;
}

/**
 * Method: _sendReminderToApproveDisapproveEmail to send remider email to space owner to take decission on booking
 *
 * @param $bookingDetail array containing the detail about the booking space
 * @param $emailTemplate int the id of the email tamplate
 * @return bool
 */
protected function _sendReminderToApproveDisapproveEmail($bookingDetail = array(), $emailTemplate = null, $emailUser = null) 
{

        //CakeLog::write('debug','Inside _sendReminderToApproveDisapproveEmail:: ' . print_r($bookingDetail,true));
        $infoMsg = '';
        $transId = str_replace('pay_','SP_',$bookingDetail['Transaction']['payment_id']);
        
        if($bookingDetail['Booking']['refundable_deposit'])
        {
           $infoMsg = 'The rental income above only reflects the first installment amount';
        }
        
        
        $paymentDate = new DateTime($bookingDetail['Booking']['start_date']);
        $paymentDate->add(new DateInterval('P2D'));
        
	$temp = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => $emailTemplate)));
	$temp['EmailTemplate']['mail_body'] = str_replace(
			array(
				'#NAME',
				'#SPACENAME',
                                '#TRANSID',
				'#STARTTIME',
				'#ENDTIME',
                                '#OWNERINCOME',
                                '#PAYMENTDATE',
                                '#INFOMSG',
                                '#USERNAME',
                                '#EMAIL',
                                '#CARREG',
                                '#CARTYPE'
			     ),
			array(
				$bookingDetail['Space']['User']['UserProfile']['first_name'].' '.$bookingDetail['Space']['User']['UserProfile']['last_name'],
				$bookingDetail['Space']['name'],
                                $transId,
				$bookingDetail['Booking']['start_date'],
				$bookingDetail['Booking']['end_date'],
                                $bookingDetail['Booking']['owner_income'],
                                $paymentDate->format('d/m/Y'),
                                $infoMsg,
                                $bookingDetail['User']['UserProfile']['first_name'] .' ' .$bookingDetail['User']['UserProfile']['last_name'],
                                $bookingDetail['User']['email'],
                                $bookingDetail['UserCar']['registeration_number'],
                                Configure::read('CarTypes.'.$bookingDetail['UserCar']['car_type'])
			     ),
			$temp['EmailTemplate']['mail_body']
			);
	$this->Apps = new AppController(new CakeRequest());

	$emailTo = $bookingDetail['User']['email'];
	if (!empty($emailUser)) {
		$emailTo = $bookingDetail['Space']['User']['email'];
	}

	return $this->Apps->_sendEmailMessage($emailTo, $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject']);
}

/**
 * Method: _sendDisapproveEmail to send remider email to space owner to take decission on booking
 *
 * @param $bookingDetail array containing the detail about the booking space
 * @param $emailTemplate int the id of the email tamplate
 * @return bool
 */
protected function _sendDisapproveEmail($bookingDetail = array(), $emailTemplate = null, $emailUser = null) 
{

        //CakeLog::write('debug','Inside _sendReminderDisapproveEmail:: ' . print_r($bookingDetail,true));
        $infoMsg = '';
        $transId = str_replace('pay_','SP_',$bookingDetail['Transaction']['payment_id']);
        
        if($bookingDetail['Booking']['refundable_deposit'])
        {
           $infoMsg = 'plus a refundable deposit of ' . $bookingDetail['Booking']['refundable_deposit'] . '<br />';
        }

        $infoMsg = 'Your credit/debit card will NOT be charged';
       
	$temp = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => $emailTemplate)));
	$temp['EmailTemplate']['mail_body'] = str_replace(
			array(
				'#NAME',
				'#SPACENAME',
                                '#TRANSID',
				'#STARTTIME',
				'#ENDTIME',
                                '#BOOKINGAMOUNT',
                                '#INFOMSG'
			     ),
			array(
				$bookingDetail['Space']['User']['UserProfile']['first_name'].' '.$bookingDetail['Space']['User']['UserProfile']['last_name'],
				$bookingDetail['Space']['name'],
                                $transId,
				$bookingDetail['Booking']['start_date'],
				$bookingDetail['Booking']['end_date'],
                                $bookingDetail['Booking']['booking_price'],
                                $infoMsg
			     ),
			$temp['EmailTemplate']['mail_body']
			);
	$this->Apps = new AppController(new CakeRequest());

	$emailTo = $bookingDetail['User']['email'];
	if (!empty($emailUser)) {
		$emailTo = $bookingDetail['Space']['User']['email'];
	}

	return $this->Apps->_sendEmailMessage($emailTo, $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject']);
}

/**
 * Method _autoDisapprove to auto disapprove booking if current date is the start date
 * and booking is in the under approval mode
 *
 * @param $bookingDetail array containing the detail about the booking space
 * @return bool
 */
    protected function _autoDisapprove($bookingDetail = array()) {
    	$this->Booking->updateAll(
    			array(
    					'Booking.'.Configure::read('BookingTableStatusField') => Configure::read('BookingStatus.DisApproved')
    				),
    			array(
    					'Booking.id' => $bookingDetail['Booking']['id']
    				)
    		);
    	$this->_sendDisapproveEmail($bookingDetail,Configure::read('EmailTemplateId.auto_disapprove'));
    	return true;
    }
}	
