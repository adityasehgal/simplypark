<?php

/**
 * This file gets total number of bookigs in the current date
 * and an email containg the count of total bookings goes to the administrator on daily basis
 *
*/

App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');
class TotalBookingsShell extends AppShell {
    public $uses = array('Booking', 'EmailTemplate');

	public function main() {
		$totalBookings = $this->Booking->getNewBookings();

		if (!empty($totalBookings)) {
			$this->_emailTotalBookings($totalBookings);
		}
		return true;
	}

/**
 * Method _emailTotalUsers to send email containg the information for total count of new users
 * and every user's small information
 *
 * @param $allUsers array containing all user's details
 * @return bool
 */
	protected function _emailTotalBookings($numBookings) {

	   	$temp = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => Configure::read('EmailTemplateId.newly_made_bookings'))));
	   	$temp['EmailTemplate']['mail_body'] = str_replace(
										    	array(
										    			'../../..',
										    			'#DATE',
										    			'#COUNTBOOKINGS'
										    		),
										    	array(
										    			FULL_BASE_URL,
										    			date('Y-m-d'),
										    			count($numBookings)
										    		),
										    	$temp['EmailTemplate']['mail_body']
										    );
	   	
		$emailTo = Configure::read('Email.EmailAdmin');
		
	   	$this->Apps = new AppController(new CakeRequest());
		
		return $this->Apps->_sendEmailMessage($emailTo, $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject']);
	}
}