<?php
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('HttpSocket', 'Network/Http');
App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');

class ConfirmRejectBookingShell extends AppShell {
    public $uses = array('Booking', 'UserProfile', 'Message');

	public function main() 
        {
                /*All bookings which are
                  (park_live_apply_algorithm == TRUE)  && 
                  (park_live_checked == FALSE) && 
                   booking_type == HOURLY &&  
                   (now() + n >= StartDate && now() + n < endDate)
                 */
		$qualifyingBookings = $this->Booking->getQualifiyingBookingsForParkLive();

                if(!empty($qualifyingBookings)) {
                   $this->loadModel('CurrentSlotOccupancy');
                   
                   $spaceSlotsArray = array();
                   $spaceFullArray = array();
                   
                   foreach($qualifyingBookings as $booking)
                   {
                      $spaceId = $qualifyingBookings['Booking']['space_id'];
		      if($spaceFullArray[$spaceId] == 0 ) {
			      $numSlots = (isset($spaceSlotsArray[$spaceId])?$spaceSlotsArray[$spaceId]:$this->CurrentSlotOccupancy->getSlotsInUse($qualifyingBookings['Booking']['space_id']));
			      if($numSlots + 1 > $qualifyingBookings['Bookings']['space_threshold_level']){
				     //reject the booking
				     //send the sms
				     $spaceFullArray[$spaceId] = 1;
			       }else{
				      $spaceSlotsArray[$spaceId] = $numSlots + 1;
			       }
			}else{
			     //reject the booking
			     //send the sms
                       }
 
                   }

                }
	   return true;
	}
   /******************************************************************
      Function Name : rejectBooking 
      Description   : Given a booking id,reject the booking with all 
                      money refunded 
      input         : $booking_id
      Assumption    : This function is called when a real time booking
                      is cancelled and is CURRENTLY ONLY CALLED FOR
                      SHORT TERM BOOKINGS
   ******************************************************************/
public function rejectBooking($bookingId)
{
   $bookingRecord = $this->Booking->getBookingRecordForRejection($bookingId);

   $refund = $bookingRecord['Booking']['booking_price'];

   /* call the razor pay refund api */
   //TODO: the booking controller and this cron job should call the same function to initiateRefund 
   $refundApiStatus = $this->_initiateRefund($bookingRecord['Transaction']['payment_id'],$amountToRefund);
   
   if($refundApiStatus){
        $status       = Configure::read('BookingStatus.CancelledNoSpaceAvailable'); //cancelled as no space is available
        $refundStatus = 2; //paid
	$lastParkingDateInString = $currentDateTimeObj->format('Y-m-d h:i:s');
        $is_deposit_refunded = 1;
        $deposit = 0;
	$ownerIncomeAfterCancellation = 0;
	$ownerServiceTaxAfterCancellation = 0;
	$spIncomeAfterCancellation = 0;
	$spServiceTaxAfterCancellation = 0;
        $cancellationFees = 0;
	
        //update the db
	$this->Booking->updateBookingRecordAfterCancellation($bookingId,
				$refund,$refundStatus,$status,$lastParkingDateInString,
                                $is_deposit_refunded,$deposit,
			        $ownerIncomeAfterCancellation,$ownerServiceTaxAfterCancellation,
				$spIncomeAfterCancellation, $spServiceTaxAfterCancellation,
				$cancellationFees);

        //send Email

        //send SMS
   }
}
   /******************************************************************
      Function Name : initiateRefund 
      Description   : Given a payment id & amount, call the payment
                      gateway refund api
      input         : $booking_id, amount
      returns       : true if successful, false otherwise
   ******************************************************************/
private function _initiateRefund($paymentId,$refund)
  {
     $amountInPaisa= array('amount' => $refund * 100); //Amount should be in paisa.

     $socket = new HttpSocket();
     $socket->configAuth('Basic', Configure::read('RazorPayKeys.KeyID'), Configure::read('RazorPayKeys.KeySecret'));
     $payment = $socket->post(Configure::read('RazorPayKeys.APIURL').$paymentId.'/refund',$refund); 

     //check return object for errors 
     if ( $payment->code == Configure::read('HTTPStatusCode.OK')) {
        /*CakeLog::write('debug',print_r('Refund Pay returned OK'),true);*/
        return true;
     } 
    else if ( $payment->code == Configure::read('HTTPStatusCode.BadRequest')) {
       CakeLog::write('error','Refund Pay returned Bad Request for payment id' . $paymentId);
       CakeLog::write('error', 'Reason:: ' . print_r($payment,true));
       return false;
     }
    else if ( $payment->code == Configure::read('HTTPStatusCode.NotAuthorized')) {
       CakeLog::write('error','Refund Pay returned UNAUTHORIZED for payment id' . $paymentId);
       CakeLog::write('error', 'Reason:: ' . print_r($payment,true));
       return false;
     }
  }

}
