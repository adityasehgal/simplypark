<?php
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('HttpSocket', 'Network/Http');
App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');

//loading qr code generater library 
require_once(APP . 'Vendor' . DS . 'qrcode' . DS . 'phpqrcode' . DS . 'qrlib.php');

class GeneratePassesShell extends AppShell {
   public $uses = array('Space','LongTermBookingPass');	

   public function main()
   {
      $spaces = $this->Space->getSpacesForPassesGeneration();
      
      if(!empty($spaces)){
          $this->generatePasses($spaces);
      }
   }


   
   protected function generatePasses($spaces)
   {
       foreach($spaces as $space)
       {
	  $totalPassesToPrint = $space['Space']['num_passes'];
          $spaceId    = $space['Space']['id'];
	  $ownerId    = $space['User']['id'];
	  $record     = array();
	  $folderName = Configure::read('QrImagePath') .$spaceId;
	  CakeLog::write('debug','FolderName : ' . $folderName);
	  $folder     =  new Folder(WWW_ROOT.DS.$folderName, true, 0755);
	  CakeLog::write('debug','Folder : ' . print_r($folder,true));
	  


	  for($passId = 0; $passId < $totalPassesToPrint; $passId++)
	  {
             $singleRecord = array();		  
	     $qrString  = "SP_".$spaceId . "_" . $ownerId . "_" . dechex($passId);
	     $passIdStr = (string)$passId;
	     $customerId = $passId + 1;
	     $serialNumber = "SP_".$spaceId."_CUST".$customerId."_";

	     $paddedPassId = str_pad($passIdStr,5-strlen($passIdStr),"0",STR_PAD_LEFT);
	     $serialNumber .= $paddedPassId;

	     $absoluteFileName = WWW_ROOT.DS.$folderName.DS.$serialNumber.'.png';
	     //CakeLog::write('debug','Absolue FilesName ' . $absoluteFileName);
	     //CakeLog::write('debug','SerialNumber ' . $serialNumber);
	     //CakeLog::write('debug','paddedSlotId ' . $serialNumber);

	     QRCode::png($qrString,$absoluteFileName);

	     $singleRecord['space_id'] = $spaceId;
	     $singleRecord['encode_pass_code'] = $qrString;
	     $singleRecord['serial_num'] = $serialNumber;
	     $singleRecord['qr_file_name'] = $serialNumber.'.png';
	     $singleRecord['created'] = date("Y-m-d H:i");
	     $singleRecord['modified'] = date("Y-m-d H:i");
	     
	     $record[$passId] = $singleRecord;
	  }

	  if($this->LongTermBookingPass->saveMany($record,array('atomic' => true))){
		  $space['Space']['booking_passes_created'] = 1;
		  $this->Space->save($space);
	  }
       }


   }
    



}
