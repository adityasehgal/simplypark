<?php

App::uses('QueueTask', 'Queue.Console/Command/Task');
App::import('Controller','Bookings');
App::import('Controller','Spaces');
App::import('Controller','VisionApis');
App::import('Controller','Tests');

/**
 * A Simple QueueTask example that runs for a while.
 *
 */
class QueueAnnotateTask extends QueueTask {

/**
 * ZendStudio Codecomplete Hint
 *
 * @var QueuedTask
 */
	public $QueuedTask;

/**
 * Timeout for run, after which the Task is reassigned to a new worker.
 *
 * @var int
 */
	public $timeout = 120;

/**
 * Number of times a failed instance of this task should be restarted before giving up.
 *
 * @var int
 */
	public $retries = 1;

/**
 * Stores any failure messages triggered during run()
 *
 * @var string
 */
	public $failureMessage = '';

/**
 * Example add functionality.
 * Will create one example job in the queue, which later will be executed using run();
 *
 * @return void
 */
	public function add() {
		$this->out('CakePHP Queue Annotate task.');
		$this->hr();
		$this->out('This is a very simple but long running example of a QueueTask.');
		$this->out('I will now add the Job into the Queue.');
		$this->out('This job will need at least 2 minutes to complete.');
		$this->out(' ');
		$this->out('To run a Worker use:');
		$this->out('	cake Queue.Queue runworker');
		$this->out(' ');
		$this->out('You can find the sourcecode of this task in:');
		$this->out(__FILE__);
		$this->out(' ');
		/*
		 * Adding a task of type 'example' with no additionally passed data
		 */
		if ($this->QueuedTask->createJob('LongExample', 1 * MINUTE)) {
			$this->out('OK, job created, now run the worker');
		} else {
			$this->err('Could not create Job');
		}
	}

/**
 * Example run function.
 * This function is executed, when a worker is executing a task.
 * The return parameter will determine, if the task will be marked completed, or be requeued.
 *
 * @param array $data The array passed to QueuedTask->createJob()
 * @param int $id The id of the QueuedTask
 * @return bool Success
 * @throws RuntimeException when seconds are 0;
 */
	public function run($data, $id = null) {
		$this->hr();
		$this->out('CakePHP Queue Annotate task.');
		
		$spaceId   = $data['spaceId'];
		$action    = $data['action'];
		$timestamp = $data['timestamp'];
		$device    = $data['device'];
		$blob      = $data['blob'];
		
		$this->out('SpaceId ' . $spaceId);
		$this->out('Action ' . $action);
		$this->out('Timestamp ' . $timestamp);
		$this->out('Device ' . $device);
		$this->out('Blob ' . $blob);
		$this->hr();
		
		$this->out('Going to call Controller function');
		$controller = new VisionApisController();
		$controller->detect($blob,$spaceId,$device,$timestamp,$action,null);
		//$controller = new BookingsController();
		//$controller->test($spaceId);
		$this->out('Controller function returned');

		//$spaceController = new SpacesController();
		//$spaceController->test($spaceId);
		
		//$visionController = new VisionApisController();
		//$visionController->detect($blob,$spaceId,$device,$timestamp,$direction);
		//$testController = new TestsController();
		//$testController->increment();

		
		$this->out(' ->Success, the QueueAnnotation Job was run.<-');
		$this->out(' ');
		$this->out(' ');
		$this->hr();
		return true;
	}

}
