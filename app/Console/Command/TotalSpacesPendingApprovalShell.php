<?php

/**
 * This file gets total number of spaces which need approval or disapproval decision from admin end
 * and an email containg the count of total spaces and some detail about each space goes to the administrator on daily basis
 *
*/

App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');
class TotalSpacesPendingApprovalShell extends AppShell {
    public $uses = array('Space', 'EmailTemplate');

	public function main() {
		$totalPendingSpaces = $this->Space->getPendingApprovalSpaces();
//pr($totalPendingSpaces);die;
		if (!empty($totalPendingSpaces)) {
			$this->_emailTotalPendingSpaces($totalPendingSpaces);
		}
		return true;
	}

/**
 * Method _emailTotalPendingSpaces to send email containg the information for total count of pending space
 * and every space's small information
 *
 * @param $pendingSpaces array containing all space's details
 * @return bool
 */
	protected function _emailTotalPendingSpaces($pendingSpaces) {

		$spaces = '';
		foreach ($pendingSpaces as $setSpaces) {
			$spaces .= '<div><b>Title: </b>'.$setSpaces['Space']['name'].'</br>';
			$spaces .= '<b>Address: </b>'.$setSpaces['Space']['flat_apartment_number']. ' ' .$setSpaces['Space']['address1'] .' '. $setSpaces['Space']['address2'].'</br></div><hr></hr>';
		}

	   	$temp = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => Configure::read('EmailTemplateId.pending_spaces'))));
	   	$temp['EmailTemplate']['mail_body'] = str_replace(
										    	array(
										    			'../../..',
										    			'#DATE',
										    			'#COUNTSPACES',
										    			'#DETAIL'
										    		),
										    	array(
										    			FULL_BASE_URL,
										    			date('Y-m-d'),
										    			count($pendingSpaces),
										    			$spaces
										    		),
										    	$temp['EmailTemplate']['mail_body']
										    );
	   	
		$emailTo = Configure::read('Email.EmailAdmin');
		
	   	$this->Apps = new AppController(new CakeRequest());
		
		return $this->Apps->_sendEmailMessage($emailTo, $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject']);
	}
}