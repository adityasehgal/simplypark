<?php
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('Controller', 'Controller');
App::uses('BookingsController', 'Controller');
class AutoCancelBookingShell extends AppShell {
    public $uses = array('BookingInstallment');

	public function main() {
		$bookinigToCancel = $this->BookingInstallment->getBookingsToCancel();

		//array_map(array($this, '_cancelBooking'), $bookinigToCancel);
		return true;
	}

/**
 * Method _cancelBooking to auto cancel booking for which the installment payment's last date is passed
 *
 * @param $bookingDetail array containing booking installment details and booking details
 * @return bool
 */
	protected function _cancelBooking($bookingDetail) {

        $this->Bookings = new BookingsController(new CakeRequest());

		$this->Bookings->cancelBookingDueToPaymentDefault(
                            $bookingDetail['BookingInstallment']['booking_id'],
                            $bookingDetail['BookingInstallment']['date_pay']
                        );

	}
}
