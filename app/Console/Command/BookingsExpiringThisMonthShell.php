<?php
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('HttpSocket', 'Network/Http');
App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');
class BookingsExpiringThisMonthShell extends AppShell {
    public $uses = array('Booking', 'EmailTemplate', 'UserProfile', 'Message');

    public function main() {
	        //CakeLog::write('debug','Inside main in shell');
		$expiringBookings = $this->Booking->getBookingsExpiringThisMonth();
		//		pr($dueInstallments);die;
		//CakeLog::write('debug',print_r($expiringBookings,true));
		//$log = $this->Booking->getDataSource()->getLog(false, false);
                //debug($log);
		array_map(array($this, '_sendEmailToAdmin'), $expiringBookings);
		return true;
	}

/**
 * Method _sendReminder to send installment reminder to user who booked parking space for long temr parkings
 *
 * @param $bookingDetail array containing booking installment details and booking details
 * @return bool
 */
protected function _sendEmailToAdmin($expiringBookings) 
{

	$temp = $this->EmailTemplate->find('first', 
		                           array('conditions' => 
					   array('EmailTemplate.id' => Configure::read('EmailTemplateId.expiring_bookings_this_month'))));

	$textToEmail = "<th><td>ID</td><td>Space Id</td><td>Name</td><td>Duration</td><td>Owner Details</td>";
	
	for($itr=0; $itr < count($expiringBookings); $itr++){
		$booking = $expiringBookings[$itr];
		CakeLog::write('debug','Inside loop ..' . print_r($booking,true));
		$name = $booking['Booking']['first_name'] + ' ' + $booking['Booking']['last_name'] + "<br />" +
			"Mob: " + $booking['Booking']['mobile'] + "<br />"+"Email:" + $booking['Booking']['email'];
		$duration  = $booking['Booking']['start_date'] + "-" + $booking['Booking']['end_date'];

		$ownerDetails = $booking['User']['UserProfile']['first_name'] + " " +  $booking['User']['UserProfile']['last_name'] + "<br/>"
			       + $booking['User']['email'];
		
		$textToEmail  += "<tr><td>" + $booking['Booking']['id'] + "</td>";
		$textToEmail  += "<td>" + $name + "</td>";
		$textToEmail  += "<td>" + $duration + "</td>";
		$textToEmail  += "<td>" + $ownerDetails + "</td>";
		$textToEmail  += "</tr>";

		break;


	}
	$temp['EmailTemplate']['mail_body'] = str_replace(
		array(
			'#EXPIRINGBOOKINGS',
		),
		array(
			$textToEmail,
		),
		$temp['EmailTemplate']['mail_body']
	);
	$this->Apps = new AppController(new CakeRequest());

	$emailTo = Configure::read('Email.EmailAdmin');
	$ccTo = "";
	//$this->Apps->_sendEmailMessage($emailTo, $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject'],null,$ccTo);
}

}
