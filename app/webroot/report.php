<?php
    // Outputs all POST parameters to a text file. The file name is the date_time of the report reception
    $fileName = date('Y-m-d_H-i-s').'.txt';
    $file = fopen(__DIR__ . '/files/applogs/'.$fileName,'w') or die('Could not create report file: ' . $fileName);
	fwrite($file,sizeof($_POST));
	foreach($_POST as $key => $value) {
    $reportLine = $key." = ".$value."\n";
		fwrite($file, $reportLine) or die ('Could not write to report file ' . $reportLine);
    }
    fclose($file);
?>
