$(function () {
      var viewUserData = function(url) {
        $.ajax({
            url: url,
            success:function( data ) {
                var popupTemplate = $("#viewUserField").html();
                resultData = _.template(popupTemplate, {data: data});
                $('#userDataContainer').html(resultData);
            }
        });
    };

    var addFrontUserValidate = function(ele){
        $(ele).validate({
            rules: {
                "data[User][username]": {
                    required: true
                },
                "data[User][email]": {
                    required: true,
                    email: true
                },
                "data[User][password]": {
                    required: true,
                    minlength: 6
                },
                "data[User][confirm_password]": {
                    required: false,
                    equalTo: '#user-new-password'
                },
                "data[UserProfile][first_name]": {
                    required: true,
                    alphaberspecialcharsonly: true
                },
                "data[UserProfile][last_name]": {
                    required: true,
                    alphaberspecialcharsonly: true
                },
                "data[UserProfile][mobile]": {
                    required: true,
                    number: true
                }
            },
            messages: {
                "data[UserProfile][first_name]": {
                    alphaberspecialcharsonly: 'Invalid characters in First Name'
                },
                "data[UserProfile][last_name]": {
                    alphaberspecialcharsonly: 'Invalid characters in Last Name'
                }
            }
        });
    };

	$(document).ready(function(){
        $(document).on('click', '.view_user_data', function(){
            var url = $(this).attr('data-url');
            viewUserData(url);
        });
        addFrontUserValidate('#add-user');
    });
});