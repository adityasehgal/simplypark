$(function () {
    var addSubAdminValidate = function(ele){
		$(ele).validate({
			rules: {
                "data[User][username]": {
                    required: true
                },
				"data[User][email]": {
                    required: true,
                    email: true,
                    pattern: /.+@simplypark\.in$/
                },
                "data[User][password]": {
                    required: true,
                    minlength: 6
                },
                "data[User][confirm_password]": {
                    required: true,
                    equalTo: '#subadmin-new-password'
                },
                "data[UserProfile][first_name]": {
                    required: true,
                    alphaberspecialcharsonly: true
                },
                "data[UserProfile][last_name]": {
                    required: true,
                    alphaberspecialcharsonly: true
                },
                "data[UserProfile][mobile]": {
                    number: true,
                    required: true,
                }
			},
            messages: {
                "data[UserProfile][first_name]": {
                    alphaberspecialcharsonly: 'Invalid characters in First Name'
                },
                "data[UserProfile][last_name]": {
                    alphaberspecialcharsonly: 'Invalid characters in Last Name'
                }
            }
		});
	};
	$(document).ready(function(){
		addSubAdminValidate('#add-sub-admin');
	});

});
