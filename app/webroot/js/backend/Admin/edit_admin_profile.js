$(function () {
    var editAdminProfileData = function(url) {
        $.ajax({
            url: url,
            success:function( data ) {
            		var popupTemplate = $("#editProfileData").html();
						resultData = _.template(popupTemplate, {data: data});
                  $('#profileDataContainer').html(resultData);
            }
        });
    };
	var editAdminProfileValidate = function(ele){
		$(ele).validate({
			rules: {
				"data[User][email]": {
				    required: true,
                    email: true
                },
                "data[UserProfile][first_name]": {
                	required: true,
                    alphaberspecialcharsonly: true
                },
                "data[UserProfile][last_name]": {
                	required: true,
                    alphaberspecialcharsonly: true
                },
                "data[UserProfile][mobile]": {
                    required: true,
                	number: true
               	}
			},
            messages: {
                "data[UserProfile][first_name]": {
                    alphaberspecialcharsonly: 'Invalid characters in First Name'
                },
                "data[UserProfile][last_name]": {
                    alphaberspecialcharsonly: 'Invalid characters in Last Name'
                }
            }
		});
	};
	$(document).ready(function(){
        $(document).on('click', '.edit_profile_data', function(){
            var url = $(this).attr('data-url');
            editAdminProfileData(url);
        });
		editAdminProfileValidate('#edit-admin-profile');
	});

});
