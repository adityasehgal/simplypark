 $(function () {
    var bookingData = function(booking_id) {
  
 		var url = base_url+'admin/Bookings/ajaxGetBookingInfo/'+booking_id;
 		$.getJSON(url, function(data) {
 			
 			var bookingInfoTemplate = $("#bookingInfoContainer").html();
           	$('#bookingTemplate').html(_.template(bookingInfoTemplate, {booking_data: data.bookingData }));
            $('#viewBookingDetailModal').modal();
            $('#busy-indicator').fadeOut();
 		});
    };

    var payInstallments = function(dataId,branchCode,formClass) {
        var  url =  base_url+'admin/Bookings/changeBookingInstallmentPaidStatus/'+dataId+'/'+branchCode;
        $.getJSON( url, function( data ) {
            // console.log(data);
            // // $('.'+formClass).parents().find('table.installmentTable').removeClass('table table-bordered').find('tr td:nth-child(1)').html('');
            // $('.'+formClass).parent().next('td').html('Paid');
            // $('.'+formClass).parent().html('');
            location.reload();
        }) .fail(function() {
                alert( "Something Went Wrong!! Please try again" );
        });
    }

    

	$(document).ready(function(){
        $('.magniPopup').click(function() {
            var bookingId = $(this).attr('data-id');
            $('.booking-id').val(bookingId);
        })

        $(document).on('click', '.view_booking_data', function(){
        	$('#busy-indicator').fadeIn();
            var bookingId = $(this).attr('data-booking-id');
            bookingData(bookingId);
        });

        $(document).on('click','.pay',function() {
            var dataId = $(this).attr('data-id');
            var formClass = $(this).parents('tr').find('td:nth-child(even)').find('form').attr('class');
            var formObj = $('.'+formClass).serializeArray();
            var branchCode = formObj[0].value;
            payInstallments(dataId,branchCode,formClass);
        });   
    }); 

});
function hasEmptyArrays(obj) {
    var emptyArrayMembers = _.filter(obj, function(member) { 
        return _.isArray(member) && _.isEmpty(member)
    });
    return emptyArrayMembers.length > 0;
}
