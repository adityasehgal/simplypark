$(function () {
    
    var spaceData = function(space_id) {
        $.ajax({
            url: base_url+'spaces/getSpaceInfo/'+space_id+'.json',
            success:function( data ) {
                //console.log(data);
                console.log(data.SpaceFacility);
                var spaceInfoTemplate = $("#spaceInfoModal").html();
                resultData = _.template(spaceInfoTemplate, {data: data});
                //$('#list-city').html(resultData);
            }
        });
    };
    
	$(document).ready(function(){
        $(document).on('click',".space-details", function(event){
            var spaceID = $(this).attr('data-id');
            spaceData(spaceID);
        });
    });
});