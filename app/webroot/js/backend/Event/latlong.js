$(document).ready(function(){
	$(document).on('click', '.latlong', function(){
		if ($('#address').val() != '') {
			var geocoder = new google.maps.Geocoder();
			
			var state_name = $('#state-list').find(":selected").text();
			var city_name = $('#city-list').find(":selected").text();

			var address = $('#address').val()+' '+state_name+' '+city_name+' '+$('#pin-code').val();
			
			geocoder.geocode( { 'address': address}, function(results, status) {

				if (status == google.maps.GeocoderStatus.OK) {
				    var latitude = results[0].geometry.location.lat();
					var longitude = results[0].geometry.location.lng();
				    document.getElementById('latitude').value = latitude.toFixed(6);
		            document.getElementById('longitude').value = longitude.toFixed(6);
			    } else {
			    	alert('Unable to get lat log of Address Filled.');
			    }
			}); 
		}
		document.getElementById('latitude').value = '';
		document.getElementById('longitude').value = '';
		return false;
	});

	$(document).on('blur',"#pinCode",function() {
		setLatLong();
	});

	// $('#add-space').on('submit', function() {
	// $("#get-lat-long").click(function (event) {
	// 	// alert('abha');
	// 	// return false;
	// 	// event.preventDefault();
	// 	// alert('vhjb');
	// 	setLatLong();
	// 	// alert('abha');
	// 	$("#add-space").submit();
	// 	// window.location.href("http://localhost/simply/users/addSpacePolicySave");
	// });

	function setLatLong() {
		if ($('#address1').val() != '') {
			var geocoder = new google.maps.Geocoder();

			var state_name = $('#state').find(":selected").text();
			var city_name = $('#city').find(":selected").text();

			var address = $('#address1').val()+' '+$('#address2').val()+' '+city_name+' '+state_name+' '+$('#pinCode').val();
			//console.log(address);

			geocoder.geocode( { 'address': address}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					console.log(results);
				    var latitude = results[0].geometry.location.lat();
					var longitude = results[0].geometry.location.lng();
					console.log(latitude+'-----------'+longitude);
				    document.getElementById('lat').value = latitude.toFixed(6);
		            document.getElementById('long').value = longitude.toFixed(6);
			    }
			});
			return true;
		}
		document.getElementById('lat').value = '';
		document.getElementById('long').value = '';
		return false;
	}
});
