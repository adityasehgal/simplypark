$(function () {
    var addEventValidate = function(ele){
		$(ele).validate({
			rules: {
                "data[EventAddress][address]": {
                    required: true
                },
                "data[EventAddress][state_id]": {
                    required: true
                },
                "data[EventAddress][post_code]": {
                    required: true,
                    number: true
                },
                "data[EventAddress][lat]": {
                    required: true,
                    number: true
                },
                "data[EventAddress][long]": {
                    required: true,
                    number: true
                },
			}
		});
	};

    var addCity = function(state_id, url) {
        $.ajax({
            url: url,
            type: 'POST',
            dataType: "json",
            data: {state_id : state_id},
            success:function( data ) {
                var addCityTemplate = $("#addCity").html();
                resultData = _.template(addCityTemplate, {data: data});
                $('#list-city').html(resultData);
            }
        });
    };

    var deleteAddresses = function(url) {
        $.ajax({
            url: url,
            type: 'POST',
            dataType: "json",
            success:function( data ) {
                var deleteEventAddresses = $("#deleteEventAddresses").html();
                resultData = _.template(deleteEventAddresses, {data: data});
                $('#event-addresses-table').html(resultData);
            }
        });
    };

    var getTemplateOfTimeSlots = function(count) {
        var timeSlotTemplate = $("#addTimeSlots").html();
        resultData = _.template(timeSlotTemplate, {count: count});
        $('.time-slots').append(resultData);
    };

	$(document).ready(function(){
        window.count = 0;
		addEventValidate('#add-event-address');

        $(document).on('change', '#state-list', function(){
            var state_id = this.value;
            var url = $(this).attr('data-url');
            addCity(state_id, url);
        });

        $(document).on('click',".remove-button", function(event){
            $(this).closest(".added-time-slots").remove();
        });

        $(document).on('click', '#time-slots', function(event){
            // window.count = parseInt($('.count').val());
            window.count += 1;
            event.preventDefault();
            getTemplateOfTimeSlots(window.count);
        });

        $(document).on('click',".delete-address", function(event){
            var url = $(this).attr('data-url');
            deleteAddresses(url);
        });
    });

    $(document).on('click',".date-pick",function(){
        var start_date_id,end_date_id;
        var id = $(this).closest('div').attr('id');
        var from_date = moment().format();
        var split_in_array = id.split('_');
        if (split_in_array.indexOf('start') == 0) {
            start_date_id = 'start_date_' + split_in_array[2];
            end_date_id = 'end_date_' + (parseInt(split_in_array[2]) + 1);    
        }  else {
            start_date_id = 'start_date_' + (parseInt(split_in_array[2]) - 1);
            end_date_id = 'end_date_' + split_in_array[2];
        }
        
        $('#' + start_date_id).datetimepicker({
            minDate: from_date,
            format : "YYYY-MM-DD HH:mm",

        });
        // .on("dp.change",function (e) {
        //     $('#' + end_date_id).find('input').val(moment($(this).find('input').val()).add(30, 'minutes').format("YYYY-MM-DD HH:mm"));
        // });
        $('#' + end_date_id).datetimepicker({
            minDate: moment(from_date).add(30, 'minutes').format("YYYY-MM-DD HH:mm"),
            format : "YYYY-MM-DD HH:mm"
        });
    });
});