$(function () {
    var addEventValidate = function(ele){
		$(ele).validate({
			rules: {
				"data[Event][event_name]": {
                    required: true
                },
                "data[Event][website]": {
                    // url: true
                    urlCheck: true
                },
                "data[Event][tags]": {
                    required: true
                },
                "data[Event][event_pic]": {
                    required: true,
                    fileType: true
                }
			}
		});

        jQuery.validator.addMethod("fileType", function() {
            var ext = $('.upload-pic').val().split('.').pop().toLowerCase();
            var check = parseInt($('#edit-upload-pic').val());
            if(check == 1) {
                return true;
            }
            
            if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
                return false;
            }
            return true;
        }, "File Type not supported.");

         jQuery.validator.addMethod("urlCheck", function() {
            var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
            var regex = new RegExp(expression);
            var t = $('input[name="data[Event][website]"]').val();
            if (t.match(regex) )
            {
               return true;
            }
            return false;

        }, "Enter valid URL.");
	};

    var updateEventValidate = function(ele){
        $(ele).validate({
            rules: {
                "data[Event][event_name]": {
                    required: true
                },
                "data[Event][website]": {
                    urlCheck: true
                },
                "data[Event][tags]": {
                    required: true
                },
                "data[Event][event_pic]": {
                    fileType: true
                }
            }
        });

        jQuery.validator.addMethod("fileType", function() {
            var ext = $('.upload-pic').val().split('.').pop().toLowerCase();
            var check = parseInt($('#edit-upload-pic').val());
            if(check == 1) {
                return true;
            }
            
            if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
                return false;
            }
            return true;
        }, "File Type not supported.");

         jQuery.validator.addMethod("urlCheck", function() {
            var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
            var regex = new RegExp(expression);
            var t = $('input[name="data[Event][website]"]').val();
            if (t.match(regex) )
            {
               return true;
            }
            return false;

        }, "Enter valid URL.");
    };

    var addCity = function(state_id, url) {
        $.ajax({
            url: url,
            type: 'POST',
            dataType: "json",
            data: {state_id : state_id},
            success:function( data ) {
                var addCityTemplate = $("#addCity").html();
                resultData = _.template(addCityTemplate, {data: data});
                $('#list-city').html(resultData);
            }
        });
    };

    var getTemplateOfTimeSlots = function(count) {
        var timeSlotTemplate = $("#addTimeSlots").html();
        resultData = _.template(timeSlotTemplate, {count: count});
        $('.time-slots').append(resultData);
    };

    var viewEventData = function(url) {
        $.ajax({
            url: url,
            success:function( data ) {
                var popupTemplate = $("#viewEventField").html();
                resultData = _.template(popupTemplate, {data: data});
                $('#eventDataContainer').html(resultData);
            }
        });
    };

	$(document).ready(function(){
        window.count = 0;
        addEventValidate('#add-event');
		updateEventValidate('#update-event');
        
        $('#add-event .input-daterange').datepicker({
            format: "yyyy-mm-dd", 
            autoclose: true,
            todayHighlight: true,
        })
        .on('changeDate', function(e){
            var start_date = $('#datepicker').find("[name='data[Event][start_date]']").val();
            var end_date = $('#datepicker').find("[name='data[Event][end_date]']").val();
            if(start_date > end_date) {
                $('.datepicker').datepicker('update', start_date);
            }
        });
    
        $('#datetimepicker3').datetimepicker({
            format: 'LT'
        });

        $('#datetimepicker4').datetimepicker({
            format: 'LT'
        });

        $(document).on('focus',".date-pick",function(){
           $(this).datetimepicker({
            format: 'LT'
           });          
        });

        $(document).on('click',".remove-button", function(event){
            $(this).closest(".added-time-slots").remove();
        });

        $(document).on('change', '#state-list', function(){
            var state_id = this.value;
            var url = $(this).attr('data-url');
            addCity(state_id, url);
        });

        $(document).on('click', '#time-slots', function(event){
            window.count = parseInt($('.count').val());
            window.count += 1;
            event.preventDefault();
            getTemplateOfTimeSlots(window.count);
        });

        $(document).on('click', '.view_event_data', function(){
            var url = $(this).attr('data-url');
            viewEventData(url);
        });
    });
});