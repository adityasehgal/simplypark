$(function () {
    var viewEventData = function(url) {
        $.ajax({
            url: url,
            success:function( data ) {
                var popupTemplate = $("#viewEventField").html();
                resultData = _.template(popupTemplate, {data: data});
                $('#eventDataContainer').html(resultData);
            }
        });
    };

    var addEventAddressData = function(url) {
        $.ajax({
            url: url,
            success:function( data ) {
                var popupTemplate = $("#addEventAddress").html();
                resultData = _.template(popupTemplate, {data: data});
                $('.eventAddressDataContainer').html(resultData);
            }
        });
    };

	$(document).ready(function(){
        $(document).on('click', '.view_event_data', function(){
            var url = $(this).attr('data-url');
            viewEventData(url);
        });

        $(document).on('click', '.add_address_data', function(){
            var url = $(this).attr('data-url');
            addEventAddressData(url);
        });
    });
});