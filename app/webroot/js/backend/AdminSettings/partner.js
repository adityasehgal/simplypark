$(function () {
	var PartnerValidate = function(ele){
		$(ele).validate({
			rules: {
				"data[Partner][name]": {
					required: true
                },
                "data[Partner][logo]": {
					required: true,
                    fileType: true
                }
			}
		});
        jQuery.validator.addMethod("fileType", function() {
            var ext = $('.upload-logo').val().split('.').pop().toLowerCase();
            if(empty(ext)) {
                var ext = $('.edit-upload-logo').val().split('.').pop().toLowerCase();
            }
            if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
                return false;
            }
            return true;
        }, "File Type not supported.");
	};

	// Add
 	$(document).ready(function(){
		PartnerValidate('#add-partner');
	});

	// Edit
    var editPartnerData = function(url) {
        $.ajax({
            url: url,
            success:function( data ) {
        		var popupTemplate = $("#editPartnerField").html();
				resultData = _.template(popupTemplate, {data: data});
                $('#partnerDataContainer').html(resultData);
            }
        });
    };

	$(document).ready(function(){
        $(document).on('click', '.edit_partner_data', function(){
            var url = $(this).attr('data-url');
            editPartnerData(url);
        });
		PartnerValidate('#edit-partner');
	});
});
