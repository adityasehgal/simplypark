$(function () {
    var disapproveFormValidate = function(ele){
		$(ele).validate({
			rules: {
                "data[SpaceDisapprovalReason][reason]": {
                    required: true
                }
			},
            messages: {
                "data[SpaceDisapprovalReason][reason]": {
                    required: 'Please specify reason.'
                }  
            }
		});
	};


    //common function at frontend to get cities according to the state selected
    var approveSpace = function(space_id, user_id, element) {
        $.ajax({
            url: base_url+'admin/spaces/ajaxApproveSpace/' + space_id + '/' + user_id + '.json',
            success:function( data ) {
                if(data.status == 'success' && data.trusted == 0){
                    $('#busy-indicator').fadeOut();
                    var url = element.attr('data-url');
                    $('.usertrustedsure').attr('href',url);
                    jQuery.magnificPopup.open({
                        items: {src: '#approveSpace'},type: 'inline'}, 0);
                    element.hide();
                    element.parent().parent().removeClass('space-row-color-not-approve');
                    element.parent().parent().addClass('space-row-color-approve');
                    if (element.siblings().length == 0) {
                        var anchorDisapprove = '<a href="#" class="btn btn-ok disapprovalspace" title="Disapprove Space" data-toggle="modal" data-target="#disApproveSpace" data-space-id="'+space_id+'" data-user-id="'+user_id+'"><i class="glyphicon glyphicon-remove"></i></a>';
                        element.parent().html(anchorDisapprove);
                    }
                } else {
                    element.parent().parent().addClass('space-row-color-not-approve');
                    $('#busy-indicator').fadeOut();
                    alert('Space approved successfully.');
                }
            }
        });
    };

    var spaceData = function(space_id) {
        $.ajax({
            url: base_url+'admin/spaces/ajaxGetSpaceInfo/'+space_id+'.json',
            success:function( data ) {
                var spaceInfoTemplate = $("#spaceInfoContainer").html();
                resultData = _.template(spaceInfoTemplate, {data: data});
                $('#spaceTemplate').html(resultData);
                $('#spaceInfoModal').modal();
                $('#busy-indicator').fadeOut();
            }
        });
    };

    var editSpaceChargesData = function(url) {
        $.ajax({
            url: url,
            success:function( data ) {
                var popupTemplate = $("#editChargesSpaceField").html();
                resultData = _.template(popupTemplate, {data: data});
                $('#chargesSpaceDataContainer').html(resultData);
            }
        });
    };

    var editAdminSpaceData = function(url) {
        $.ajax({
            url: url,
            success:function( data ) {
                var popupTemplate = $("#editAdminSpaceField").html();
                resultData = _.template(popupTemplate, {data: data});
                $('#spaceAdminDataContainer').html(resultData);
            }
        });
    };

	$(document).ready(function(){
		disapproveFormValidate('#disapprove-space');

        $(document).on('click', '.disapprovalspace', function(){
            var spaceId = $(this).attr('data-space-id');
            var userId = $(this).attr('data-user-id');
            $('#spaceID').val(spaceId);
            $('#userID').val(userId);
        });


        $(document).on('click', '.makeusertrusted', function(){
            $('#busy-indicator').fadeIn();
            var spaceID = $(this).attr('data-space-id');
            var userID = $(this).attr('data-user-id');
            var element = $(this);
            approveSpace(spaceID,userID,element);
        });

        $(document).on('click',".space-details", function(event){
            $('#busy-indicator').fadeIn();
            var spaceID = $(this).attr('data-space-id');
            spaceData(spaceID);
        });

        $(document).on('click', '.space-charges-settings', function(){
            var spaceId = $(this).attr('data-space-id');
            var userId = $(this).attr('data-user-id');
            $('#space-id').val(spaceId);
            $('#user-id').val(userId);

        });

        $(document).on('click', '.space-charges-settings', function(){
            var url = $(this).attr('data-url');
            editSpaceChargesData(url);
        });

        $(document).on('click', '.edit_admin_space_data', function(){
            var url = $(this).attr('data-url');
            editAdminSpaceData(url);
        });
    });
});
function countChar(val) {
    var len = val.value.length;
    if (len >= 1000) {
      val.value = val.value.substring(0, 1000);
    } else {
      $('#charNum').text(1000 - len);
    }

}

function fileExists(url) {
    if(url){
        var req = new XMLHttpRequest();
        req.open('GET', url, false);
        req.send();
        return req.status==200;
    } else {
        return false;
    }
}
