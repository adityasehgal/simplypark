(function () {

	var formGroup = ".form-group",
		hasSuccess = "has-success",
		hasError = "has-error",
		includeClass = "valid",
		addCarFormId = "#addCar";
        accountInformationFormId = "#account-information";
        agreeTermFormId = "#agree-booking-terms";



	/* ****************************************************************
	  It is to validate add car form
	******************************************************************* */
	var ValidatorForm = function (fromId){
		$(fromId).validate({
			rules: {
			    'data[UserCar][registeration_number]': {
                    required: true
                }
		  	},
		  	messages: {
		  		'data[UserCar][registeration_number]': {
                    required: 'Please enter car number.'
                }
		  	},
			highlight : function(element){
	    		highlightFunc(element);
	    	},

		    success : function(element) {
		     	successFunc(element);
		    }
		});
	};

    /* ****************************************************************
      It is to validate account information form
    ******************************************************************* */
    var accountInfomationValidate = function (fromId){
        $(fromId).validate({
            rules: {
                'data[Booking][first_name]': {
                    required: true
                },
                'data[Booking][last_name]': {
                    required: true
                },
                'data[Booking][mobile]': {
                    required: true,
                    minlength:10,
                    maxlength:10,
                    number: true
                },
                'data[Booking][email]': {
                    required: true,
                    email: true
                },
                'data[Booking][user_car]': {
                    required: true
                }
            },
            highlight : function(element){
                highlightFunc(element);
            },
            success : function(element) {
                successFunc(element);
            },
            submitHandler: function(form) {
                $('#collapseTwo').collapse('show');
                $('#collapseOne').collapse('hide');

                $('<input>').attr({
                    type: 'text',
                    name: 'data[Booking][first_name]',
                    value: $('#first_name').val(),
                }).appendTo('#booking-space-save');

                $('<input>').attr({
                    type: 'text',
                    name: 'data[Booking][last_name]',
                    value: $('#last_name').val(),
                }).appendTo('#booking-space-save');

                $('<input>').attr({
                    type: 'text',
                    name: 'data[Booking][mobile]',
                    value: $('#mobile').val(),
                }).appendTo('#booking-space-save');

                $('<input>').attr({
                    type: 'text',
                    name: 'data[Booking][email]',
                    value: $('#email').val(),
                }).appendTo('#booking-space-save');

                $('<input>').attr({
                    type: 'text',
                    name: 'data[Booking][user_car_id]',
                    value: $('#user-cars').val(),
                }).appendTo('#booking-space-save');

                return false;  // block the default submit action
            }
        });
    };

    /* ****************************************************************
      It is to validate agree terms and conditions form
    ******************************************************************* */
    var agreeTermsValidate = function (fromId){
        $(fromId).validate({
            rules: {
                'data[AgreeBookingTerms][agree_booking_terms_conditions]': {
                    required: true
                }
            },
            highlight : function(element){
                highlightFunc(element);
            },
            success : function(element) {
                successFunc(element);
            },
            errorPlacement: function(error, element) {
               $('.help-block').append(error);
            },
            submitHandler: function(form) {
                if (options.amount > 0) {

                    options.prefill.contact = $('#mobile').val();
                    options.prefill.email = $('#email').val();
                    options.prefill.name = $('#first_name').val() + ' ' + $('#last_name').val();

                    razorPayObj();
                    return false;  // block the default submit action
                }
                $('<input>').attr({
                    type: 'text',
                    name: 'data[Transaction][payment_id]',
                    value: 'pay_FREE'
                }).appendTo('#booking-space-save');
                $('#overlay').show();
                $('#booking-space-save').submit();
            }
        });
    };

	var highlightFunc = function(ele){
    	$(ele).closest(formGroup).removeClass(hasSuccess).addClass(hasError);
    }

    var successFunc = function(ele){
    	$(ele).addClass(includeClass).closest(formGroup).removeClass(hasError).addClass(hasSuccess);
    }

    var saveUserCar = function(element) {
        $.ajax({
            type: 'post',
            url: element.attr('action')+'.json',
            data: element.serialize(),
            success:function( data ) {
                if (data.status == 'success') {
                    $('#user-cars').append('<option value="'+data.carID+'">'+data.data+'</option>');
                    $('#user-cars').val(data.carID); 
                    $('#addCarModal').modal('hide');
                    $('.error-box').addClass('hide');
                } else {
                    $('#show-car-error').html(data.message);
                    $('.error-box').removeClass('hide');
                }
            }
        });
    }

    var options = {
        "key": razorPayKeyID.KeyID,
        "amount": totalAmount,
        "name": spaceDetail.Space.name + ' - ' + spaceDetail.City.name,
        "description": "SimplyPark Space Booking Payment",
        // "image": "/your_logo.png",
        "handler": function (response){
            
            $('<input>').attr({
                type: 'text',
                name: 'data[Transaction][payment_id]',
                value: response.razorpay_payment_id
            }).appendTo('#booking-space-save');
            $('#overlay').show();
            $('#booking-space-save').submit();
        },
        "prefill": {
            "name": $('#first_name').val() + ' ' + $('#last_name').val(),
            "email": $('#email').val(),
            "contact": $('#mobile').val()
        }/*,
        "notes": {
            "address": "Hello World"
        }*/
    };


    var razorPayObj = function(e) {
        var rzp1 = new Razorpay(options);
        rzp1.open();
    }

    var applyCoupon = function(couponVal,isBulk,numSlots,propertyTypeId) {
        $.ajax({
            type: 'post',
            url: base_url+'bookings/ajaxManageDiscountCoupon.json',
            dataType: "json",
            data: {coupon_code : couponVal, startDate : startDate, endDate : endDate, spaceID : spaceID, isBulk: isBulk, numSlots: numSlots, propertyTypeId: propertyTypeId},
            success:function( data ) {
                if (data.Type == 1) {
                    $('#show-error').removeClass('text-danger');
                    $('#show-error').addClass('text-success');
                    if(data.isBulk == 0) {
                       updatePageDisplayAfterDiscount(data);
                       updateHiddenFormAfterDiscount(data);
                 
                       $('#show-error').html(data.Message);
                       if ( data.isShortTermBooking )
                       {
                           options.amount = data.totalPrice*100;
                       }
                      else
                       {
                          //options.amount = (data.firstInstallment + data.refundableDeposit)*100;
                          options.amount = (data.payNow)*100;
                       }
                     }else {
                       $('#show-error').html(data.Message);
                       updateBulkBookingPageDisplayAfterDiscount(data);
                       updateBulkBookingHiddenFormAfterDiscount(data);

                       options.amount = data.payNow * 100;


                     }


                    $('.apply-coupon').addClass('disabled');
                } else {
                    $('#show-error').removeClass('text-success');
                    $('#show-error').addClass('text-danger');
                    $('#show-error').html(data.Message);
                }
                
            }
        });
    }

    $(document).ready(function(){
        ValidatorForm(addCarFormId);
        accountInfomationValidate(accountInformationFormId);
        agreeTermsValidate(agreeTermFormId);

        $(document).on('submit', '#addCar', function(e) {
            e.preventDefault();
            saveUserCar($(this));
        });

        $(document).on('click', '.apply-coupon', function(){
            var couponCode = $('.coupon-input').val();
            if (couponCode != '') {
                var isBulk = $('.isBulk').text();
                var numSlots = $('.numSlotsForBulkBooking').text();
                var propertyTypeId = $('.propertyTypeId').text();
                var $btn = $(this).button('loading');
                applyCoupon(couponCode,isBulk,numSlots,propertyTypeId);
                $btn.button('reset');
            }
        });

	});
})();


function updatePageDisplayAfterDiscount(data)
{
    if ( data.isShortTermBooking)
    {
	    $('.st-parking-price-strikethrough').css('text-decoration','line-through');
	    $('#after-discount-price').removeClass('hide');
	    var htmlToReplace = '<i class=\'fa fa-inr\'><i>' + data.totalPrice;
	    $('#after-discount-price').html(htmlToReplace);
	    $('.st-parking-price-display-2').html(data.totalPrice);
	    if ( data.totalPrice == 0 )
	    {
		    $('.st-handling-charge-display-1').html('<br /><small><font color=gray>Yey, this booking is free. Just agree to our Terms,Press the Pay button and you are done.</font></small>');
	    }
	    else
	    {
                    if ( data.handlingCharges > 0 )
                    {
		       $('.st-handling-charge-display-1').html('(includes a handling charge of Rs. ' + data.handlingCharges + ')');
                    }
                   else
                    {
		       $('.st-handling-charge-display-1').addClass('hide');
                    }


	    }
            
    }
   else
    {
	    $('.lt-parking-price-info').removeClass('hide');
            if(data.isCommissionOneOff)
            {
	       $('.lt-parking-price-info').html('<font color=green>Discount Applied to First Installment</font>');
	    }else{
	       $('.lt-parking-price-info').html('<font color=green>First Installment discounted to </font>');
	       $('#after-discount-price').removeClass('hide');
	       var htmlToReplace = '<i class=\'fa fa-inr\'><i>' + data.firstInstallment;
	       console.log(data.firstInstallment);
	       console.log(htmlToReplace);
	       $('#after-discount-price').html(htmlToReplace);
	       }
            
            //var payNow = data.refundableDeposit + data.firstInstallment;
            htmlToReplace = data.payNow + '*';
	    $('#paynowAmount').html(htmlToReplace);
            
	    $('#lt-discount-info').removeClass('hide');
            htmlToReplace = '<dt>Discounted Installment</dt>' +
                            '<dd class=total-amount-pay> Rs. ' +
                             data.firstInstallment;

	    $('#lt-final-pay').html(data.payNow);

            if ( data.firstInstallment )
            {
               if ( data.handlingChargesForFirstInstallment > 0 )
               {
                  htmlToReplace += ' (includes a handling charge of Rs. ' + data.handlingChargesForFirstInstallment +')';
               }
            }
           else
            {
               if(data.refundableDeposit) {
                  htmlToReplace += ('<small><font color=gray>&nbsp;(Yey, the first installment is free.However, we still have to collect a refundable deposit)</font></small>');
               }else{
                  htmlToReplace += ('<small><font color=gray>&nbsp;(Yey, the first installment is free.)</font></small>');
               }

	    }
	    htmlToReplace += '</dd>';
 
            $('#lt-discount-info').html(htmlToReplace);
      
           
    }
}

function updateBulkBookingPageDisplayAfterDiscount(data)
{

   $('.lt-parking-price-info').removeClass('hide');
   $('.lt-parking-price-info').html('<font color=green>Installment discounted to </font>');
   $('#after-discount-price').removeClass('hide');
   var htmlToReplace = '<i class=\'fa fa-inr\'><i>' + data.installmentArray[0];
   if(data.numDiscountedInstallments == data.numInstallments)
   {
      $('.parking-price').css('text-decoration','line-through');
      htmlToReplace += ' /month';
   }else{
      htmlToReplace += ' for ' + data.numDiscountedInstallments + ' month';
      if(data.numDiscountedInstallments > 1) {
         htmlToReplace += 's';
      }
   }
  
   $('#after-discount-price').html(htmlToReplace);

   //var payNow = data.refundableDeposit + data.firstInstallment;
   htmlToReplace = data.payNow+' ';
   $('#paynowAmount').html(htmlToReplace);
   
   for(itr=1;itr<data.numInstallments-1;itr++)
   {
      var installmentStr = '#installment'+itr;
      $(installmentStr).html(data.installmentArray[itr]);
   }
   
    $('#lastInstallment').html(data.installmentArray[itr]);

   $('#lt-discount-info').removeClass('hide');
   htmlToReplace = '<dt>Discounted Installment</dt>' +
   	'<dd class=total-amount-pay> Rs. ' +
   	data.installmentArray[0];
   
   if(data.numDiscountedInstallments == data.numInstallments)
   {
      htmlToReplace += ' per month';
   }else{
      htmlToReplace += ' for ' + data.numDiscountedInstallments + ' month';
      if(data.numDiscountedInstallments > 1) {
         htmlToReplace += 's';
      }
   }

   if(data.refundableDeposit <= 0)
   {
      $('#rdeposit').addClass('hide');
      $('#rdepositVal').addClass('hide');
   }

   $('#lt-final-pay').html(data.payNow);


   if(data.handlingChargeArray[0] > 0)
   {
      htmlToReplace += ' (includes a handling charge of Rs. ' + data.handlingChargeArray[0] +')';
   }
   
   htmlToReplace += '</dd>';
   $('#lt-discount-info').html(htmlToReplace);
}

function updateHiddenFormAfterDiscount(data)
{
   if ( data.isShortTermBooking)
   {
       if ($('.amount_to_capture').length > 0) 
       {
          $('.amount_to_capture').attr('value',data.totalPrice);
       }

       $('#booking-price').attr('value',data.totalPrice);
       $('#internet-handling-charges').attr('value',data.handlingCharges);
       $('#simplypark-tax-amount').attr('value',data.simplyParkServiceTax);
       $('#simplypark-income').attr('value',data.simplyParkIncome);
       
       $('<input>').attr({
              type: 'text',
              name: 'data[Booking][discount_coupon_id]',
              value: data.discountCouponId
       }).appendTo('#booking-space-save');
      $('<input>').attr({
             type: 'text',
             name: 'data[Booking][discount_amount]',
             value: data.discountAmount
        }).appendTo('#booking-space-save');

      $('<input>').attr({
             type: 'text',
             name: 'data[CouponApply][user_id]',
             value: $('#BookingUserId').val()
       }).appendTo('#booking-space-save');
     
     $('<input>').attr({
         type: 'text',
         name: 'data[CouponApply][discount_coupon_id]',
         value: data.discountCouponId
     }).appendTo('#booking-space-save');


   }
  else
   {
       var payNow = data.refundableDeposit + data.firstInstallment;
       $('.amount_to_capture').attr('value',data.payNow);
      
       $('#booking-price, .installment_pay0').attr('value',data.firstInstallment);
       $('#internet-handling-charges, .installment_handling_charges0').attr('value',data.handlingChargesForFirstInstallment);
       $('#simplypark-tax-amount, .installment_sp_tax_amount0').attr('value',data.spServiceTaxForFirstInstallment);
       $('#simplypark-income, .installment_sp_income0').attr('value',data.spIncomeForFirstInstallment);
       
       $('<input>').attr({
              type: 'text',
              name: 'data[Booking][discount_coupon_id]',
              value: data.discountCouponId
       }).appendTo('#booking-space-save');
      $('<input>').attr({
             type: 'text',
             name: 'data[Booking][discount_amount]',
             value: data.discountAmount
        }).appendTo('#booking-space-save');

      $('<input>').attr({
             type: 'text',
             name: 'data[CouponApply][user_id]',
             value: $('#BookingUserId').val()
       }).appendTo('#booking-space-save');
     
     $('<input>').attr({
         type: 'text',
         name: 'data[CouponApply][discount_coupon_id]',
         value: data.discountCouponId
     }).appendTo('#booking-space-save');




   }

}

function updateBulkBookingHiddenFormAfterDiscount(data)
{
	var payNow = data.refundableDeposit + data.installmentArray[0];
	$('.amount_to_capture').attr('value',data.payNow);
       
        $('#booking-price').attr('value',data.installmentArray[0]);
        $('#internet-handling-charges').attr('value',data.handlingChargeArray[0]);
        $('#simplypark-tax-amount').attr('value',data.spServiceTaxArray[0]);
        $('#simplypark-income').attr('value',data.spIncomeArray[0]);
        
        for(itr=0;itr<data.numInstallments;itr++)
        {
           var installmentStr    = '#BookingInstallment'+itr+'Amount';
           var handlingChargeStr = '#BookingInstallment'+itr+'InternetHandlingCharges';
           var spTax             = '#BookingInstallment'+itr+'SimplyparkServiceTaxAmount';
           var spIncome          = '#BookingInstallment'+itr+'SimplyParkIncome';

	   $(installmentStr).attr('value',data.installmentArray[itr]);
	   $(handlingChargeStr).attr('value',data.handlingChargeArray[itr]);
	   $(spTax).attr('value',data.spServiceTaxArray[itr]);
	   $(spIncome).attr('value',data.spIncomeArray[itr]);
        }

	$('<input>').attr({
                    type: 'text',
                    name: 'data[Booking][discount_coupon_id]',
                    value: data.discountCouponId
                    }).appendTo('#booking-space-save');
                    $('<input>').attr({
                    type: 'text',
                    name: 'data[Booking][discount_amount]',
                    value: data.discountAmount
                    }).appendTo('#booking-space-save');

       $('<input>').attr({
                  type: 'text',
                  name: 'data[CouponApply][user_id]',
                  value: $('#BookingUserId').val()
                  }).appendTo('#booking-space-save');

       $('<input>').attr({
                 type: 'text',
                 name: 'data[CouponApply][discount_coupon_id]',
                 value: data.discountCouponId
                 }).appendTo('#booking-space-save');
}


function manageDiscountCoupon(discountAmountarr) {

    $('#booking-price, .installment_pay0').attr('value', discountAmountarr.TotalPriceToPay);
    $('#internet-handling-charges, .installment_handling_charges0').attr('value', discountAmountarr.InternetHandlingCharges);
    $('#simplypark-tax-amount, .installment_sp_tax_amount0').attr('value', discountAmountarr.SimplyParkTax);
    $('#simplypark-income, .installment_sp_income0').attr('value', discountAmountarr.SimplyParkIncome);

    $('.interner-handling-amount').html(discountAmountarr.InternetHandlingCharges+' INR');

    if ($('.amount_to_capture').length > 0) {
        $('.amount_to_capture').attr('value',discountAmountarr.TotalPriceToPay);
    }

    if ($('.refundable_amount').length > 0) {

        var totalAmountToShowAfterDiscount = parseFloat(discountAmountarr.TotalPriceToPay) + parseFloat($('.refundable_amount').val());
        
        if ($('.amount_to_capture').length > 0) {
            $('.amount_to_capture').attr('value',totalAmountToShowAfterDiscount);
        }

        $('.total-amount-pay').html(totalAmountToShowAfterDiscount +' INR');
        $('.space-price').html(totalAmountToShowAfterDiscount);
    } else {
        $('.total-amount-pay').html(discountAmountarr.TotalPriceToPay+' INR');
        $('.space-price').html(discountAmountarr.TotalPriceToPay);
    }

    $('<input>').attr({
        type: 'text',
        name: 'data[Booking][discount_coupon_id]',
        value: discountAmountarr.discountCouponId
    }).appendTo('#booking-space-save');

    $('<input>').attr({
        type: 'text',
        name: 'data[Booking][discount_amount]',
        value: discountAmountarr.discountAmount
    }).appendTo('#booking-space-save');

    $('<input>').attr({
        type: 'text',
        name: 'data[CouponApply][user_id]',
        value: $('#BookingUserId').val()
    }).appendTo('#booking-space-save');

    $('<input>').attr({
        type: 'text',
        name: 'data[CouponApply][discount_coupon_id]',
        value: discountAmountarr.discountCouponId
    }).appendTo('#booking-space-save');
}
