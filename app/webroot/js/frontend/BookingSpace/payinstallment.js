(function () {

    var options = {
        "key": razorPayKeyID.KeyID,
        "amount": BookingDetail.BookingInstallment.amount*100,
        "name": BookingDetail.Booking.Space.name + ' - ' + BookingDetail.Booking.Space.City.name,
        "description": "SimplyPark Space Booking Installment Payment",
        // "image": "/your_logo.png",
        "handler": function (response){
            $('<input>').attr({
                type: 'text',
                name: 'data[Transaction][payment_id]',
                value: response.razorpay_payment_id
            }).appendTo('#booking-installment-pay');
            $('#booking-installment-pay').submit();
        },
        "prefill": {
            "name": BookingDetail.Booking.first_name + ' ' + BookingDetail.Booking.last_name,
            "email": BookingDetail.Booking.email,
            "contact": BookingDetail.Booking.mobile
        }/*,
        "notes": {
            "address": "Hello World"
        }*/
    };


    var razorPayObj = function(e) {
        var rzp1 = new Razorpay(options);
        rzp1.open();
        //e.preventDefault();
    }

    $(document).ready(function(){
        $(document).on('click','.pay_intallment',function(){
            razorPayObj();
        });
	});
})();