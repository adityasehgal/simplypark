
(function () {

    var bookingID = '';
    var paymentID = '';
    var paymentAmount = '';
    var approvedElement = '';
    var disapprovedElement = '';

    var approveBooking = function() {
        $('#myModalapprove').modal('hide');
        $.ajax({
            type: 'post',
            url: base_url+'bookings/ajaxApproveSpace.json',
            dataType: "json",
            data: {booking_id : bookingID, payment_id : paymentID, payment_amount : paymentAmount},
            success:function( data ) {
                if (data.Type == 1) {
                    var approvedSpace = $("#approveSpace").html();
                    resultData = _.template(approvedSpace, {data: data});
                    approvedElement.parents('td').html(resultData);
                    $('#busy-indicator').fadeOut();
                } else {
                    $('#busy-indicator').fadeOut();
                    alert('Approve booking failed');
                }
            }
        });
    }

    var disApproveBooking = function() {
        $('#myModaldisapprove').modal('hide');
        $.ajax({
            type: 'post',
            url: base_url+'bookings/ajaxDisApproveSpace.json',
            dataType: "json",
            data: {booking_id : bookingID, payment_id : paymentID},
            success:function( data ) {
                if (data.Type == 1) {
                    var disapprovedSpace = $("#disapproveSpace").html();
                    disapprovedElement.parents('td').html(disapprovedSpace);
                    $('#busy-indicator').fadeOut();
                } else {
                    $('#busy-indicator').fadeOut();
                    alert('Disapprove booking failed');
                }
            }
        });
    }

    var searchBookingsMade = function(element) {
        var postData = element.serializeArray();
        var formURL = element.attr("action");

        $.ajax({
            type: 'get',
            url: formURL,
            data: postData,
            success:function( data ) {
                $('#bookingMade').html(data);
                $('#busy-indicator').fadeOut();
                $('.start-date, .end-date').datetimepicker({
                    format:'Y-m-d',
                    timepicker: false
                });
            }
        });
    }

    var searchBookingsReceived = function(element) {
        var postData = element.serializeArray();
        var formURL = element.attr("action");

        $.ajax({
            type: 'get',
            url: formURL,
            data: postData,
            success:function( data ) {
                $('#bookingReceived').html(data);
                $('#busy-indicator').fadeOut();
                $('.start-date-receive, .end-date-receive').datetimepicker({
                    format:'Y-m-d',
                    timepicker: false
                });
            }
        });
    }

    $(document).ready(function(){
        
        $('.start-date, .end-date, .start-date-receive, .end-date-receive').datetimepicker({
            format:'Y-m-d',
            timepicker: false
        });

        $(document).on('click', '.approve-booking', function() {
            approvedElement = $(this);
            bookingID = $(this).attr('data-id');
            paymentID = $(this).attr('data-payment-id');
            paymentAmount = $(this).attr('data-payment-amount');
            $('#myModalapprove').modal();
        });

        $(document).on('click', '.access-approval', function() {
            $('#busy-indicator').fadeIn();
            approveBooking();
        });

        $(document).on('click', '.disapprove-booking', function() {
            disapprovedElement = $(this);
            bookingID = $(this).attr('data-id');
            paymentID = $(this).attr('data-payment-id');
            $('#myModaldisapprove').modal();
        });

       $(document).on('click','.cancel-request',function() {
             var link = $(this).attr('data-link');
             $('#cancel-request-link').attr('href',link);
        });
       
       $(document).on('click','#cancel-request-link',function(ev) {
             $(this).html('In Progress...');
             $(this).addClass('disabled');
             $('.noButton').addClass('disabled');
        });
       
       $(document).on('click','.cancel-request-by-owner',function() {
             var link = $(this).attr('data-link');
             $('#cancel-request-by-owner-link').attr('href',link);
        });
       
       $(document).on('click','#cancel-request-by-owner-link',function(ev) {
             $(this).html('In Progress...');
             $(this).addClass('disabled');
             $('.noButtonOwner').addClass('disabled');
        });


        $(document).on('click', '.access-disapproval', function() {
            $('#busy-indicator').fadeIn();
            disApproveBooking();
        });

        $(document).on('submit','#filter-bookings-made',function(e){
            e.preventDefault(); //STOP default action
            //e.unbind(); //unbind. to stop multiple form submit.
            $('#busy-indicator').fadeIn();
            searchBookingsMade($(this));
        });

        $(document).on('submit','#filter-bookings-received',function(e){
            e.preventDefault(); //STOP default action
            //e.unbind(); //unbind. to stop multiple form submit.
            $('#busy-indicator').fadeIn();
            searchBookingsReceived($(this));
        });

	});
})();
