(function () {

	var formGroup = ".form-group",
		hasSuccess = "has-success",
		hasError = "has-error",
		includeClass = "valid",
		loginFormId = "#loginForm",
		forgotPassFormId = "#forgotPass",
		resetPassFormId = "#reset-password",
		signupFormId = "#signupForm";
		changePasswordFormId = "#changePasswordForm";
		uploadProfilePicFormId = "#updateProfilePicForm";
		searchTagFormId = "#searchTags";



	/* ****************************************************************
	  Use addClient function for validate AddClient modal box fields
	******************************************************************* */
	var ValidatorForm = function (fromId){
		$(fromId).validate({
			rules: {
			    'data[User][email]': {
                    required: true,
                    minlength: 3,
                    email: true
                },
                'data[User][password]': {
                    required: true,
                    minlength: 6
                },
                'data[UserProfile][first_name]': {
                	required: true,
                	alphaberspecialcharsonly: true
                },
                'data[UserProfile][last_name]': {
                	required: true,
                	alphaberspecialcharsonly: true
                },
                'data[UserProfile][username]': {
                	required: true,
                },
                'data[UserProfile][mobile]': {
					required: true,
					minlength:10,
					maxlength:10,
					number: true
                },
                'data[User][old_password]': {
                    required: true
                },
                'data[User][password]': {
                    required: true,
                    minlength: 6
                },
                'data[User][confirm_password]': {
                    equalTo: '#new-password'
                },
                'data[UserProfile][profile_pic]': {
                    required: true
                },
                "data[Search][email]": {
                    required: true,
                    email: true
                },
                "data[Search][keyword]": {
                    required: true
                }

		  	},
		  	messages: {
		  		"data[UserProfile][first_name]": {
                    alphaberspecialcharsonly: 'Invalid characters in First Name'
                },
                "data[UserProfile][last_name]": {
                    alphaberspecialcharsonly: 'Invalid characters in Last Name'
                }
		  	},
			highlight : function(element){
	    		highlightFunc(element);
	    	},

		    success : function(element) {
		     	successFunc(element);
		    }
		});
	};

	var highlightFunc = function(ele){
    	$(ele).closest(formGroup).removeClass(hasSuccess).addClass(hasError);
    }

    var successFunc = function(ele){
    	$(ele).addClass(includeClass).closest(formGroup).removeClass(hasError).addClass(hasSuccess);
    }

    //common function at frontend to get cities according to the state selected
    var getCities = function(state_id) {
        $.ajax({
            url: base_url+'homes/ajaxGetCities/' + state_id + '.json',
            async: false,
            success:function( data ) {
                var addCityTemplate = $("#cityData").html();
                resultData = _.template(addCityTemplate, {data: data});
                $('#cityContainer').html(resultData);
            }
        });
    };

    //common function at frontend to get cities according to the state selected for home address area at update profile page
    var getCitiesHome = function(state_id) {
        $.ajax({
            url: base_url+'homes/ajaxGetCities/' + state_id + '.json',
            async: false,
            success:function( data ) {
                var addCityTemplate = $("#cityDataHome").html();
                resultData = _.template(addCityTemplate, {data: data});
                $('#cityContainerHome').html(resultData);
            }
        });
    };

    $(document).ready(function(){
		ValidatorForm(loginFormId);
		ValidatorForm(forgotPassFormId);
		ValidatorForm(signupFormId);
		ValidatorForm(changePasswordFormId);
		ValidatorForm(uploadProfilePicFormId);
		ValidatorForm(resetPassFormId);
		ValidatorForm(searchTagFormId);
		
		$('.modal').bind('hidden.bs.modal', function () {
			// $("body").css({"padding-right" : "0px", "overflow" : "visible"});
		});

		$('#signupModal').on('shown.bs.modal', function () {
			$("body").css("overflow", "hidden");
		});
		
		/* login modal hide when click forgot password link */
		$("#callForgotPass, #callLoginModal, #callSignupModal").click(function(){
	       	$('#loginModal, #signupModal').modal('hide');
	    });

		$('#new-password').pStrength({
		    'changeBackground'          : false,
		    'onPasswordStrengthChanged' : function(passwordStrength, strengthPercentage) {
		        if ($(this).val()) {
		            $.fn.pStrength('changeBackground', $(this), passwordStrength);
		        } else {
		            $.fn.pStrength('resetStyle', $(this));
		        }
	        	var strenghtType = '';
	            if(strengthPercentage > 0 && strengthPercentage <= 50) {
	                strenghtType = 'Weak';
	            }

	            if(strengthPercentage > 50 && strengthPercentage <= 80) {
	                strenghtType = 'Moderate';
	            }

	            if(strengthPercentage > 80) {
	                strenghtType = 'Strong';
	            }
		        $('#' + $(this).data('display'))
		            .html('Your password strength is ' + strenghtType);
		            //.html('Your password strength is ' + strengthPercentage + '%');
		    },
		    'onValidatePassword': function(strengthPercentage) {
		        $('#' + $(this).data('display')).html(
		            $('#' + $(this).data('display')).html()
		        );
		    }
		});

		// var showGatedSpace = function(url) {
	 //        $.ajax({
	 //            url: url,
	 //            success:function( data ) {

	 //            	alert(data);
	 //                var popupTemplate = $("#showGatedSpaceField").html();
	 //                resultData = _.template(popupTemplate, {data: data});
	 //                $('#showGatedSpaceContainer').html(resultData);
	 //            }
	 //        });
	 //    };

		$(document).on('change', '#state-list', function(){
			$('#empty-city').removeClass('hide');
            var state_id = this.value;
            getCities(state_id);
        });

        $(document).on('change', '#state-list-home', function(){
			var state_id = this.value;
            getCitiesHome(state_id);
        });

		//unbind validate upload profile pic form if remove profile pic radio button is selected
        $(document).on('click', '.removepic', function(){
        	$(uploadProfilePicFormId).data('validator', null);
			$(uploadProfilePicFormId).unbind('validate');
        });

        //bind validate upload profile pic form if upload profile pic radio button is selected
        $(document).on('click', '.uploadpic', function(){
        	ValidatorForm(uploadProfilePicFormId);
        });

        $(document).on('click', '.search-not-found', function(){
        	$('.searched_keyword').val($('.search-box').val());
            $('#searchTagEmail').modal();
        });

        $(document).on('click', '.search-gated-space', function(){
        	var url = $(this).attr('data-url');
        	console.log(url);
            showGatedSpace(url);
          //  $('#searchGatedSpace').modal();
        });
	    
	});

	
})();