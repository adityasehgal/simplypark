var addCity = function(eventId,stateId) {
    $('#valid-cities').val('');
    $('#busy-indicator').fadeIn();
    var eventId = Base64.encode(eventId);
    var stateId = Base64.encode(stateId);
    var url = base_url+'events/ajaxGetEventCities/'+eventId+'/'+stateId;
    $.getJSON(url, function(data) {
        $('#busy-indicator').fadeOut();
        $('#empty-city').removeClass('hide');

        var addCityTemplate = $("#cityDataEvent").html();
        resultData = _.template(addCityTemplate, {data: data.city});
        $('#city-list-all').html(resultData);
    })
    .fail(function() {
        $('#empty-city').addClass('hide');
    });
    
};

$("#search_address_form").submit(function(e)
{
    var postData = $(this).serializeArray();
    var formURL = $(this).attr("action");
    $.ajax(
    {
        url : formURL,
        type: "GET",
        data : postData,
        success:function(data, textStatus, jqXHR) 
        {
            $('#event-addresses').html(data);
            $('#busy-indicator').fadeOut();
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            $('#busy-indicator').fadeOut();
            alert('Some errors occurred. Please try again!');
        }
    });
    e.preventDefault(); //STOP default action
    //e.unbind(); //unbind. to stop multiple form submit.
});

$(document).ready(function(){
    
    $(document).on('change', '.search_address', function(){
        if($(this).attr("id") == 'state-event-list') {

            var eventId = $('#event-id').val();
            var stateId = $(this).find('option:selected').val();
            addCity(eventId,stateId); 
        }
        $('#busy-indicator').fadeIn();
        $("#search_address_form").submit();
    });
});
