$(document).ready(function() {
    initialize();
}); 

var map;
locations = {};
locationsDetail = {};
infowindow =  new google.maps.InfoWindow({
  content: ''
});
var bounds = new google.maps.LatLngBounds();
var selectedIcon = {
              url: base_url+"/img/selected_map_marker.png",
              size: new google.maps.Size(47, 56)
            };
var unSelectedIcon = {
              url: base_url+"/img/unselected_map_marker.png",
              size: new google.maps.Size(47, 56)
            };
var eventAddressIcon = {
              url: base_url+"/img/event_address_marker.png",
              size: new google.maps.Size(47, 56)
            };

function initialize() {
    var mapOptions = {
        mapTypeId: 'roadmap'
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);
        
    // Multiple Markers
    var markers = [];

    createEventAddressMarker();

    for( i = 0; i < nearestSpacesForMap.length; i++ ) {
        markers.push([
            nearestSpacesForMap[i].Space.address1+', '+nearestSpacesForMap[i].Space.address2+', '+nearestSpacesForMap[i].City.name, 
            parseFloat(nearestSpacesForMap[i].Space.lat),
            parseFloat(nearestSpacesForMap[i].Space.lng)
        ]);
    }
    
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var location = nearestSpacesForMap[i];
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);

        var icon = {
              url: base_url+"/img/unselected_map_marker.png",
              size: new google.maps.Size(47, 56)
            };

        marker = new google.maps.Marker({
            position: position,
            map: map,
            icon:icon,
            title: markers[i][0]
        });
        
        locations[location.Space.id]=marker;
        locationsDetail[location.Space.id]=location;

        // add click event
        bindInfoWindow(marker, map, infowindow, location, i);

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(14);
        google.maps.event.removeListener(boundsListener);
    });
    
}

function createEventAddressMarker() {

    var latlng = new google.maps.LatLng(eventAddresForMap.EventAddress.lat,eventAddresForMap.EventAddress.long);
    
    bounds.extend(latlng);

    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        icon: eventAddressIcon
    });

    var eventAddresshtml = [];
    eventAddresshtml.push("<div class='info_content'><b>Event Info: </b><h3 class='text-primary'>");
    eventAddresshtml.push(eventAddresForMap.Event.event_name+' - '+eventAddresForMap.EventAddress.City.name+"</h3>");
    eventAddresshtml.push("<b>Address: </b>");
    eventAddresshtml.push(eventAddresForMap.EventAddress.address+', '+eventAddresForMap.EventAddress.State.name+', '+eventAddresForMap.EventAddress.City.name+'<br>');
    eventAddresshtml.push("</div>");
    eventAddresshtml = eventAddresshtml.join("");
    google.maps.event.addListener(marker, 'click', (function (marker) {
        return function () {

            $(".location-name").removeClass("active");
            $('.space-title').removeClass('park-map-marker');

            for( var k = 0; k < Object.keys(locations).length ; k++ )
            {
                space_id = nearestSpacesForMap[k].Space.id;
                locations[space_id].setIcon(unSelectedIcon);
            }
            
            if (infowindow) infowindow.close();
            infowindow.setContent(eventAddresshtml);
            infowindow.open(map, marker);
        }
    })(marker));

    // Extend the bounds with the last marker position
    map.fitBounds(bounds);
    
}

function showSelectedSpace(spaceID)
{
    $(".location-name").removeClass("active");
    $("#SPACEID-" + spaceID).addClass("active");
    $('.space-title').removeClass('park-map-marker');
    $("#SPACEID-" + spaceID).find('.space-title').addClass('park-map-marker');

    for( var k = 0; k < Object.keys(locations).length ; k++ )
    {
        space_id = nearestSpacesForMap[k].Space.id;
        locations[space_id].setIcon(unSelectedIcon);
    }

    locations[spaceID].setIcon(selectedIcon);
    // infowindow
    marker = locations[spaceID];
    infowindow.setContent(showDetail(locationsDetail[spaceID]));
    infowindow.open(map, marker);
}

function getRates (hr,day,week,month,year) 
{
    var Rate;
    if (hr !== null && hr > 0) {
        Rate = hr + ' /hr';
    } else if (day !== null && day > 0) {
        Rate = day + ' /day';
    } else if (week !== null && week > 0) {
        Rate = week + ' /week';
    } else if (month !== null && month > 0) {
        Rate = month + ' /month';
    } else if (year !== null && year > 0) {
        Rate = year + ' /annum';
    } else {
        Rate = 'N/A';
    }
    return Rate;
}

function showDetail(location)
{
    var numberSlots = (location.Space.number_slots==null) ? parseInt('0') : parseInt(location.Space.number_slots);
    var html = [];
    html.push("<div class='info_content'><h3 class='text-primary'>");
    html.push(location.Space.name+"</h3>");
    html.push("<b>Price: </b> &#8377 ");
    html.push(getRates(location.SpacePricing.hourly,location.SpacePricing.daily,location.SpacePricing.weekely,location.SpacePricing.monthly,location.SpacePricing.yearly) + '<br>');
    html.push("<b>Distance: </b>");
    html.push(Math.round(location[0].distance)+" km(s)<br>");
    html.push("<b>Available Slots: </b>");
    html.push(numberSlots+"<br></div>");
    return html.join("");
}

function bindInfoWindow(marker, map, infowindow, location, i) {
    google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
            var infoContent=showDetail(location); 
            infowindow.setContent(infoContent); 

            
            for( var k = 0; k < Object.keys(locations).length ; k++ )
            {
                space_id = nearestSpacesForMap[k].Space.id;
                locations[space_id].setIcon(unSelectedIcon);
            }
            marker.setIcon(selectedIcon);
            
            infowindow.open(map, marker);

            $(".location-name").removeClass("active");
            $("#SPACEID-" + location.Space.id).addClass("active");
            $('.space-title').removeClass('park-map-marker');
            $("#SPACEID-" + location.Space.id).find('.space-title').addClass('park-map-marker');

        }
    })(marker, i));
} 