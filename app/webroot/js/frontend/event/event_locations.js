$(function () {
    
    var spaceData = function(space_id) {
        $.ajax({
            url: base_url+'spaces/getSpaceInfo/'+space_id+'.json',
            success:function( data ) {
                //console.log(data.SpaceFacility[1].Facility.name);
                var spaceInfoTemplate = $("#spaceInfoContainer").html();
                resultData = _.template(spaceInfoTemplate, {data: data});
                
                $('#spaceTemplate').html(resultData);
                $('#spaceInfoModal').modal();
            }
        });
    };
    
	$(document).ready(function(){
        $(document).on('click',".space-details", function(event){
            var spaceID = $(this).attr('data-id');
            spaceData(spaceID);
        });
    });
});
function countChar(val) {
    var len = val.value.length;
    if (len >= 1000) {
      val.value = val.value.substring(0, 1000);
    } else {
      $('#charNum').text(1000 - len);
    }
};
function fileExists(url) {
    if(url){
        var req = new XMLHttpRequest();
        req.open('GET', url, false);
        req.send();
        return req.status==200;
    } else {
        return false;
    }
}
