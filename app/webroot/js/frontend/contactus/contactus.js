(function () {

	var formGroup = ".form-group",
		hasSuccess = "has-success",
		hasError = "has-error",
		includeClass = "valid",
		contactUsFormId = "#contact-us";



	/* ****************************************************************
	  It is to validate contact us form
	******************************************************************* */
	var ValidatorForm = function (fromId){
		$(fromId).validate({
			rules: {
			    'data[ContactUs][email]': {
                    required: true,
                    minlength: 3,
                    email: true
                },
                'data[ContactUs][name]': {
                    required: true,
                    letterswithbasicpunc: true
                },
                'data[ContactUs][message]': {
                	required: true
                }
		  	},
		  	messages: {
		  		'data[ContactUs][email]': {
                    required: 'Please enter email.'
                },
                'data[ContactUs][name]': {
                    required: 'Please enter name.'
                },
                'data[ContactUs][message]': {
                	required: 'Please enter message.'
                }
		  	},
			highlight : function(element){
	    		highlightFunc(element);
	    	},

		    success : function(element) {
		     	successFunc(element);
		    }
		});
	};

	var highlightFunc = function(ele){
    	$(ele).closest(formGroup).removeClass(hasSuccess).addClass(hasError);
    }

    var successFunc = function(ele){
    	$(ele).addClass(includeClass).closest(formGroup).removeClass(hasError).addClass(hasSuccess);
    }

    $(document).ready(function(){
		ValidatorForm(contactUsFormId);
	});
})();

function countChar(val) {
    var len = val.value.length;
    if (len >= 1000) {
      val.value = val.value.substring(0, 1000);
    } else {
      $('#charNum').text(1000 - len);
    }
  };