$(function () {
        var addWithdrawalSourceValidate = function(ele){
        $(ele).validate({
            rules: {
                "data[WithdrawalSources][account_first_name]": {
                    required: true
                },
                "data[WithdrawalSources][account_number]": {
                    required: true,
                    number: true,
                    minlength: 9,
                },
                "data[WithdrawalSources][branch_code]": {
                    required: true,
                },
                "data[WithdrawalSources][ifsc_code]": {
                    required: true,
                    minlength: 11,
                    maxlength: 11,
                    regx: /[A-Z|a-z]{4}[0][\d]{6}$/   
                },
                // "data[WithdrawalSources][swift_code]": {
                //     required: true,
                // },
                "data[WithdrawalSources][address]": {
                    required: true,
                },
                "data[WithdrawalSources][postal_code]": {
                    required: true,
                    number: true,
                },
                "data[WithdrawalSources][state_id]": {
                    required: true,
                },
                "data[WithdrawalSources][bank_name]": {
                    required: true,
                }
            },
            messages: {
                "data[WithdrawalSources][account_first_name]": {
                    required: 'Please enter First Name.'
                },
                "data[WithdrawalSources][account_number]": {
                    required: 'Please enter Account Number.'
                },
                "data[WithdrawalSources][branch_code]": {
                    required: 'Please enter Branch Code.'
                },
                "data[WithdrawalSources][ifsc_code]": {
                    required: 'Please enter IFSC Code.',
                  
                },
                // "data[WithdrawalSources][swift_code]": {
                //     required: 'Please enter Swift Code.'
                // },
                "data[WithdrawalSources][address]": {
                    required: 'Please enter Address.'
                },
                "data[WithdrawalSources][postal_code]": {
                    required: 'Please enter Postal code.',
                },
                "data[WithdrawalSources][state_id]": {
                    required: 'Please Select State.'
                },
                "data[WithdrawalSources][bank_name]": {
                    required: 'Please enter Bank Name.'
                }
            }
        });
    };

    var addCity = function(state_id, url) {
        $.ajax({
            url: url,
            type: 'POST',
            dataType: "json",
            data: {state_id : state_id},
            success:function( data ) {
                var addCityTemplate = $("#addCity").html();
                resultData = _.template(addCityTemplate, {data: data});
                $('#list-city').html(resultData);
            }
        });
    };

    var activateWithdrawalSource = function(url, id) {
        $.ajax({
            url: url,
            type: 'POST',
            dataType: "json",
            data: {id : id},
            // success:function( data ) {

            // }
            complete: function(){
                location.reload(true);
            }
        });
    };

    $(document).ready(function(){
        $.validator.addMethod("regx", function(value, element, regexpr) {          
            return regexpr.test(value);
        }, "Please enter a valid IFSC Code.");

        addWithdrawalSourceValidate('#add-withdrawal-source');

        $(document).on('change', '#state', function(){
            var state_id = this.value;
            var url = $(this).attr('data-url');
            addCity(state_id, url);
        });
        $(document).on('keyup','#ifsc-code',function(){
            var ifscCode = $(this).val();
            var ifscRegex = /[A-Z|a-z]{4}[0][\d]{6}$/g;

            var urlLoc = base_url + 'withdrawal_sources/getBankAddress/' + ifscCode;
            if(ifscCode.length == 11 && ifscRegex.test(ifscCode) == true ) {
                var parent = $(this).parent().parent().attr('id');
                // if($('#'+parent).find('div#loading')) {
                //     $('#loading').remove();
                // }
                
                //$('#'+parent +' div:nth-child(2)').after('<div id="loading" class="pull-left"><i class="fa fa-refresh fa-spin"></i></div>');
                $.getJSON( urlLoc, function( data ) {
                    var address = $('#WithdrawalSourcesAddress');
                    var bankName = $('#bank-name');
                     if($('#ifscCode').find('label.not-found')) {
                            $('.not-found').remove();
                        }
                    if (!$.isEmptyObject(data)) {
                        address.val(data.Bank.address).attr('readonly',true);
                        bankName.val(data.Bank.bank); 
                    } else {
                        
                        $('#ifsc-code').after('<label for="ifsc-code" class="pull-right not-found">Sorry!! We could not find a branch address for the given IFSC code. Kindly enter your branch details manually or try again.</label>');    
                        address.val('').attr('readonly',false);
                        bankName.val('');
                    } 
                    
                }).fail(function(err) {                 
                });
            }
        });

        $('input:radio[name="data[WithdrawalSources][is_activated]"]').change(function(){
            if ($(this).is(':checked')) {
                var id = $(this).val();
                var url = $(this).attr('data-url');
                activateWithdrawalSource(url, id);
            }
        });
    });
});