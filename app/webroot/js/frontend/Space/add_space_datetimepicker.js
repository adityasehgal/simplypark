$(function () {

        //alert(isEditAfterApproval);
	var startDate = new Date();
	var selectedStartDate = '';

	var curDate = getDateFormated(startDate);
	
	var maxStartDate = new Date().getFullYear()+'/'+(new Date().getMonth()+4)+'/'+new Date().getDate(); //Last maximum start date upto three months from current date
	var curTime = startDate.getHours() + ":" + startDate.getMinutes();

	var endDate = '';
	var endMinDate = '';
	var endMaxDate = '';
	var selectedEndDate = '';

	if ($('#end-date').val() != '') {
		endDate = new Date($('#start-date').val());
		endMinDate = getDateFormated(endDate);

                //alert(typeof endDate);

		endMaxDate = (endDate.getFullYear()+1)+'/'+(endDate.getMonth()+1)+'/'+endDate.getDate();

		selectedStartDate = new Date($('#start-date').val());
		selectedStartDate = selectedStartDate.setHours(selectedStartDate.getHours()+1);

		selectedStartTime = new Date($('#start-date').val()+' '+$('#start-time').val());
		selectedStartTime = selectedStartTime.getHours()+1 + ':00';
	}

	var validateDates = function() {
		$.ajax({
            type: 'post',
            url: base_url+'spaces/ajaxValidateDates.json',
            data: {start_date :  new Date($('#start-date').val()), end_date :  new Date($('#end-date').val()), space_id : $('#space-id').val()},
            success:function( data ) {
            
	    if (data.m == 0 && data.y == 0 && !(data.month)  && $('#property_type_id').val() == 2) {
	       $('.dates-error').removeClass('hide');
               $('.end-date-info').addClass('hide');
	       $('.dates-error').html('Spaces in a Gated Community are booked minimum for a month. Please choose at least a month.');
	       $('#submit-space-details').attr('disabled',true);
	    } else {
			$('.dates-error').addClass('hide');
                        $('.end-date-info').addClass('hide');
                	$('#submit-space-details').attr('disabled',false);
                	$('.dates-error').html('');
	    }

            }
        });
	}

	var manageEndDate = function(check) {
		$('#end-date').datetimepicker({
			format:'Y-m-d',
			minDate: endMinDate,
			timepicker:false,
			onSelectDate:function(ct,$i) {
				selectedEndDate = new Date(ct);
				validateDates();
				priceBox();
			},
		});
	}

	var startTime = function() {
		//$('#start-time').prop('readonly',true);
		$('#start-time').datetimepicker({
		  	datepicker:false,
		  	format:'H:i',
		  	allowTimes:[
			  '00:00', '00:30', '01:00', '01:30', '02:00', '02:30', '03:00', '03:30', '04:00', '04:30', '05:00', '05:30', '06:00', '06:30', '07:00', '07:30', '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30',
			  '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30', '21:00', '21:30', '22:00', '22:30', '23:00'
			],
                        step:30,
			//onShow:startTimeLogic,
			onSelectTime:function(ct,$i){
				selectedStartTime = ct.getHours()+1 + ':00';
				endTime();
				priceBox();
			}
		});
	}

	var startTimeLogic = function( currentDateTime ){
		if (endDate > new Date()) {
			this.setOptions({
		  		minTime: false,
		  		maxTime: false
		  	});
		} else {
			this.setOptions({
		  		minTime: curTime,
		  		maxTime: false
		  	});
		}
	};

	var endTime = function() {
                //alert(selectedStartTime);    
		$('#end-time').prop('readonly',false);
		$('#end-time').datetimepicker({
		  	datepicker:false,
		  	format:'H:i',
		  	allowTimes:[
			  '00:30', '01:00', '01:30', '02:00', '02:30', '03:00', '03:30', '04:00', '04:30', '05:00', '05:30', '06:00', '06:30', '07:00', '07:30', '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30',
			  '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30', '21:00', '21:30', '22:00', '22:30', '23:00', '23:30'
			],
                        step:30,
			onShow: endTimeLogic,
			defaultTime: selectedStartTime,
			onSelectTime:function(ct,$i){
				priceBox();
			}
		});
	}

	var endTimeLogic = function(currentDateTime) {
		
		/*if (getDateFormated(selectedEndDate) === getDateFormated(endDate)) {*/
			this.setOptions({
		  		minTime: selectedStartTime,
		  		maxTime: false
		  	});
		/*} else if (selectedEndDate > new Date()) {
			this.setOptions({
		  		minTime: false,
		  		maxTime: false
		  	});
		} else {
			endMinTime = currentDateTime.getHours()+1 + ':00';
			this.setOptions({
		  		minTime: endMinTime,
		  		maxTime: false
		  	});
		}*/
	}

	var enableDisableTime = function(element) {
		
		if (element.is(':checked')) {
			$('#start-time').val('');
			$('#start-time').datetimepicker('destroy');
			$('#start-time').prop('readonly',true);
			$('#end-time').val('');
			$('#end-time').datetimepicker('destroy');
			$('#end-time').prop('readonly',true);
                        if (isEditAfterApproval) {
                           element.prop('readonly',true);
                        }
		} else {
			$('#start-time').prop('readonly',false);
			$('#end-time').prop('readonly',true);

                        if (isEditAfterApproval) {
                           element.prop('readonly',true);
                        }
			startTime();

		}
	}

	$(document).ready(function(){
                //startTime();
		
               if (!isEditAfterApproval) {
	          enableDisableTime($('.24-hours-open'));
               
                
		  //$('#end-time').prop('readonly',true);
		  $('#end-date').prop('readonly',true);
		  $('#start-date').datetimepicker({
		  	            format:'Y-m-d',
			            minDate: curDate,
			            maxDate: maxStartDate,
			            timepicker:false,
			 
                                  onSelectDate:function(ct,$i) {
			 	          $('#end-date').prop('readonly',false);
				          endDate = new Date(ct);
                                          endMinDate = getDateFormated(endDate);
                                          endMaxDate = endDate;
                                          endMaxDate.setFullYear(endMaxDate.getFullYear() + 5);
                                          dateVal = getDateFormatedByHypen(endDate);
				          $('#end-date').val(dateVal);
				          manageEndDate(0);
				          priceBox();
			                  $('.end-date-info').removeClass('hide');
                                          $('.end-date-info').html('To maximise your revenue, we recommend keeping the End Date as long as possible.<br />We have automatically choosen an End Date for you');	
			          }
		  });

		  $(document).on('click', '#time1', function(){
		 	$('#start-time').datetimepicker('show');
		  });

		  $(document).on('click', '#time2', function(){
		 	$('#end-time').datetimepicker('show');
		  });

		  $(document).on('click', '#date1', function(){
		        $('#start-date').datetimepicker('show'); //support hide,show and destroy command
		  });

		 $(document).on('click', '#date2', function(){
		       $('#end-date').datetimepicker('show'); //support hide,show and destroy command
		  });

		
                 if ($('#end-date').val() != '') {
			manageEndDate(1);
			startTime();
			endTime();
			if ($('.24-hours-open').is(':checked')) {
				enableDisableTime($('.24-hours-open'));
			}
		}

		
                $(document).on('click', '.24-hours-open', function(){
			enableDisableTime($(this));
		});

	}
     });
});

function getDateFormated(date) {
	var month = date.getMonth()+1;
	var day = date.getDate();

	var output = date.getFullYear() + '/' +
	    (month<10 ? '0' : '') + month + '/' +
	    (day<10 ? '0' : '') + day;
	return output;
}

function getDateFormatedByHypen(date) {
	var month = date.getMonth()+1;
	var day = date.getDate();

	var output = date.getFullYear() + '-' +
	    (month<10 ? '0' : '') + month + '-' +
	    (day<10 ? '0' : '') + day;
	return output;
}
