$(document).ready(function() {
    initialize();

   $( "#mobileInput" ).focus(function() {
      $("#smsSendStatus").text('').addClass('hide');
   });

   $(".smsButton").click(function() {
     $("#smsSendStatus"+this.id).addClass('hide');
     if($("#mobile"+this.id).is(":visible")){
         var mobile = $("#mobileInput"+this.id).val();
          if(mobile.length == 10){
             $("#"+this.id).attr('disabled','disabled');
             $("#"+this.id).text("Sending....");
             
             $("#mobileInput"+this.id).attr('disabled','disabled');
             
             var userId = $("#SMSLocation"+this.id+"User-id").val();
             var spaceId = $("#SMSLocation"+this.id+"Space-id").val();
             var shortURL = $("#SMSLocation"+this.id+"Short-url").val();
             smsLocation(this.id,userId,spaceId,mobile,shortURL);
          }else{
             $("#smsSendStatus"+this.id).removeClass('hide');
             $("#smsSendStatus"+this.id).addClass('show');
             $("#smsSendStatus"+this.id).html("<span class=\"text-danger driving\">Invalid Mobile Number.</span>");
             
          }
     }else{
        $("#mobile"+this.id).removeClass('hide');
        $("#"+this.id).text("Send SMS");
     }
  });
});

var map;
locations = {};
locationsDetail = {};
infowindow =  new google.maps.InfoWindow({
  content: ''
});
var selectedIcon = { 
              url: base_url+"/img/selected_map_marker.png",
              size: new google.maps.Size(47, 56)
            };
var unSelectedIcon = {
              url: base_url+"/img/unselected_map_marker.png",
              size: new google.maps.Size(47, 56)
            };

function initialize() {
    if(nearestSpacesForMap.length!=0){ 
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);

        
    // Multiple Markers
    var markers = [];
      for( i = 0; i < nearestSpacesForMap.length; i++ ) {
         markers.push([
            nearestSpacesForMap[i].Space.address1+', '+nearestSpacesForMap[i].Space.address2+', '+nearestSpacesForMap[i].City.name, 
            parseFloat(nearestSpacesForMap[i].Space.lat),
            parseFloat(nearestSpacesForMap[i].Space.lng)
        ]);
      }
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var location = nearestSpacesForMap[i];
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);

        var icon = {
              url: base_url+"/img/unselected_map_marker.png",
              size: new google.maps.Size(47, 56)
            };

        marker = new google.maps.Marker({
            position: position,
            map: map,
            icon:icon,
            title: markers[i][0]
        });
        
        locations[location.Space.id]=marker;
        locationsDetail[location.Space.id]=location;

        // add click event
        bindInfoWindow(marker, map, infowindow, location, i);

        // Automatically center the map fitting all markers on the screen
         map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(15); /* 1 world, 5 landmass, 10 city, 15 streets, 20 buildings */
	if(!isMobileAndTablet()){
          this.panBy(-200,50);
	}
        google.maps.event.removeListener(boundsListener);
    });
    }else{
	// Create a map object and specify the DOM element for display.
        var map = new google.maps.Map(document.getElementById('map_canvas'), {
              center: {lat: 28.6289332, lng: 77.2065107},
              scrollwheel: false,
               zoom: 15 
           });
    }
    
}

function isMobileAndTablet()
{
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
}


function showSelectedSpace(spaceID,smsDivId)
{
    //resetSmsDiv(smsDivId);
    $(".location-name").removeClass("active");
    $("#SPACEID-" + spaceID).addClass("active");
    $('.space-title').removeClass('park-map-marker');
    $("#SPACEID-" + spaceID).find('.space-title').addClass('park-map-marker');

    for( var k = 0; k < Object.keys(locations).length ; k++ )
    {
        space_id = nearestSpacesForMap[k].Space.id;
        locations[space_id].setIcon(unSelectedIcon);
    }

    locations[spaceID].setIcon(selectedIcon);
    // infowindow
    marker = locations[spaceID];
    infowindow.setContent(showDetail(locationsDetail[spaceID]));
    infowindow.open(map, marker);
}

function getRates (hr,day,week,month,year) 
{
    var Rate;
    if (hr !== null && hr > 0) {
        Rate = hr + ' /hr';
    } else if (day !== null && day > 0) {
        Rate = day + ' /day';
    } else if (week !== null && week > 0) {
        Rate = week + ' /week';
    } else if (month !== null && month > 0) {
        Rate = month + ' /month';
    } else if (year !== null && year > 0) {
        Rate = year + ' /annum';
    } else {
        Rate = 'N/A';
    }
    return Rate;
}

function showDetail(location)
{
    //reset the SMS location button if shown 
    var numberSlots = (location.Space.number_slots==null) ? parseInt('0') : parseInt(location.Space.number_slots);
    var html = [];
    html.push("<div class='info_content'><h3 class='text-primary'>");
    html.push(location.Space.name+"</h3>");
    if(location.Space.is_available_with_simplypark == false && location.Space.is_parking_free){
    }else{
      if(location.Space.tier_pricing_support == "1"){
	      //html.push("<table><tr><td>");
              html.push("<b>Price: </b> </td>");
	      //html.push("<td>");
	      html.push(location.Space.tier_formatted_desc);
	      //html.push("</td></tr></table>");
      }else{
              html.push("<b>Price: </b> &#8377 ");
              html.push(getRates(location.SpacePricing.hourly,location.SpacePricing.daily,
				 location.SpacePricing.weekely,location.SpacePricing.monthly,location.SpacePricing.yearly) + '<br>');
      }
    }
    html.push("<b>Distance: </b>");
    html.push(Math.round(location[0].distance)+" km(s)<br>");
    html.push("<b>Available Slots: </b>");
    html.push(numberSlots+"<br></div>");
    return html.join("");
}

function bindInfoWindow(marker, map, infowindow, location, i) {
    google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
            var infoContent=showDetail(location); 
            infowindow.setContent(infoContent); 

            
            for( var k = 0; k < Object.keys(locations).length ; k++ )
            {
                space_id = nearestSpacesForMap[k].Space.id;
                locations[space_id].setIcon(unSelectedIcon);
            }
            marker.setIcon(selectedIcon);
            
            infowindow.open(map, marker);

            $(".location-name").removeClass("active");
            $("#SPACEID-" + location.Space.id).addClass("active");
            $('.space-title').removeClass('park-map-marker');
            $("#SPACEID-" + location.Space.id).find('.space-title').addClass('park-map-marker');

        }
    })(marker, i));
} 
    
var smsLocation = function(id,userId,spaceId,mobile,shortURL) {
        $.ajax({
            type: 'post',
            url: base_url+'spaces/ajax_SendSpaceLocationSms.json',
            dataType: "json",
            data: {user_id: userId, space_id: spaceId , mobile_no: mobile,short_url: shortURL},
            success:function( data ) {
                if (data.status == 1) {
                   $("#smsSendStatus"+id).removeClass('hide');
                   $("#smsSendStatus"+id).addClass('show');
                   $("#smsSendStatus"+id).html("<span class=\"text-primary driving\">Location sent</span>");
                }else if(data.status == -1){
                   $("#smsSendStatus"+id).removeClass('hide');
                   $("#smsSendStatus"+id).addClass('show');
                   $("#smsSendStatus"+id).html("<span class=\"text-danger driving\">Invalid Mobile Number. Please try again.</span>");
                }else{
                   $("#smsSendStatus"+id).removeClass('hide');
                   $("#smsSendStatus"+id).addClass('show');
                   $("#smsSendStatus"+id).html("<span class=\"text-danger driving\">Oops. Looks like we couldn't send the location. Please try again.</span>");
                }
                $("#mobileInput"+id).removeAttr('disabled');
                $("#"+id).removeAttr('disabled');
                $("#"+id).text("Send SMS");

            }
        });
    }
