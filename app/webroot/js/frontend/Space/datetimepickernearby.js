$(function () {
    
         /* Requirements of this function 
         * 1. At load time, From date picker should be enabled, To date picker should be disabled
         * 2. From date picker, the dates should not be greater than 3 months. If possible, year combo box should also be capped.
         * 3. To date picker, the dates should not be greater than From date + 1 year. If possible ,year combo box should also be capped.
         * 4. Please take care that the from_date and to_date should not go beyond the from_date and end_date of the space
         * 4. BookNow function should be called when From Date, From Time, To Date, To Time is selected (various combinations should be tested)
         */

	$(document).ready(function(){
		var currentDateObj        = new Date();
                var currentTimeInMs       = currentDateObj.getTime();
                var minStartDateObj       = new Date(currentTimeInMs);
                var maxStartDateObj       = new Date(currentTimeInMs);
               
                maxStartDateObj.addMonths(3);

                var maxStartDateInMs = maxStartDateObj.getTime();

               //set start-date & end-date as read only. 
               $('#start-date').prop('readonly',true);
               $('#end-date').prop('readonly',true);

               /* minimum time calculation */
               var hours = currentDateObj.getHours();
               if(currentDateObj.getMinutes() > 0){
                  hours++;
               }
               
              var defaultStartTimeStr = (hours<10?'0'+hours:hours) + ":00";
              var minStartTimeStr = defaultStartTimeStr; 
              var maxEndTimeStr = false;
              var startPickerMonthStart = currentDateObj.getMonth();
              var startPickerYearStart  = currentDateObj.getYear();
              
              var minDateWithoutTimeObj = new Date(currentTimeInMs);
              minDateWithoutTimeObj.setHours(0);
              minDateWithoutTimeObj.setMinutes(0);
              minDateWithoutTimeObj.setSeconds(0); 
              minDateWithoutTimeObj.setMilliseconds(0); 
                
              var minDateWithoutTimeInMs = minDateWithoutTimeObj.getTime();




               $('#start-date').datetimepicker({
    			        lang:'en',
    			        format:'d.m.Y H:i',
                                closeOnWithouthClick:true,
    			        minDate: new Date(currentTimeInMs),
                                startDate: new Date(currentTimeInMs),
                                maxDate: new Date(maxStartDateInMs),
                                minTime: defaultStartTimeStr, 
                                //maxTime: false,
                                defaultTime: defaultStartTimeStr,
                                monthStart: startPickerMonthStart,
                                monthEnd: -1, //setting an invaild month to stop the month scrolling
                                yearStart: startPickerYearStart,
                                yearEnd: -1, //setting an invalid year to stop the year scrolling 
                                roundTime: 'ceil',
                                scrollMonth:false,
                                showOtherMonths:false,
                                defaultSelect:false,
                                //defaultDate:new Date(minStartDateInMs),
                                onSelectTime:
                                function(ct,$i){
   			        },
                                onShow:
                                function(current_time, $input) {
                                     var dateObj = new Date(current_time);
                                     setNextPrevButtonForStartPicker.call(this,dateObj,minStartDateObj,maxStartDateObj);
                                 },
                                onChangeMonth: 
                                function(current_time, $input) {
                                     var dateObj = new Date(current_time);
                                     setNextPrevButtonForStartPicker.call(this,dateObj,minStartDateObj,maxStartDateObj);
                                 },
                                onChangeYear:
                                function(current_time, $input) {
                                     var dateObj = new Date(current_time);
                                     setNextPrevButtonForStartPicker.call(this,dateObj,minStartDateObj,maxStartDateObj);
                                 },
                                onSelectDate:
                                function(current_time, $input) {
                                   var dateObj = new Date(current_time);
                                   startPickerDateSelected(dateObj);
                                },
                                 
                                onChangeDateTime:
                                function(current_time, $input) {
                                   var dateObj = new Date(current_time);
                                   dateObj.setHours(0);
                                   dateObj.setMinutes(0);
                                   dateObj.setSeconds(0);
                                   dateObj.setMilliseconds(0);
                                   var dateObjInMs = dateObj.getTime();
                                   if(dateObjInMs != minDateWithoutTimeInMs)
                                   { 
	                              this.setOptions({
                                                   minTime: false 
                                      });
                                   }else {
	                                 this.setOptions({
                                                   minTime: minStartTimeStr 
                                         });
                                   }
                                   
                                   var currentDateObj = new Date(current_time);
                                   startPickerDateSelected(currentDateObj);
                                   

                                },

                               
              });
            
	      
              $('#date1').click(function(){
		  $('#start-date').datetimepicker('show'); //support hide,show and destroy command
	      });

	      $('#date2').click(function(){
		  $('#end-date').datetimepicker('show'); //support hide,show and destroy command
	      });

        });
});


function setNextPrevButtonForStartPicker(dateObj, minStartDateObj,maxStartDateObj)
{
   if(dateObj.getMonth() == minStartDateObj.getMonth() && 
      dateObj.getFullYear() == minStartDateObj.getFullYear())
   {
      this.setOptions({
            prevButton: false
       });
   }else {
	this.setOptions({
            prevButton: true 
      });
   }

  if(dateObj.getMonth() == maxStartDateObj.getMonth() && 
    dateObj.getFullYear() <= maxStartDateObj.getFullYear())
  {
	this.setOptions({
              nextButton: false
        });
  } else {
	this.setOptions({
             nextButton: true 
        });

  }


}


function startPickerDateSelected(dateObj)
{
   $('#end-date').removeAttr('readonly');
   $('#end-date').val('');
   $('#end-date').datetimepicker('destroy');

   var minEndDateObj  = new Date(dateObj.getTime());
   minEndDateObj = minEndDateObj.addHours(1);
   var minEndDateInMs = minEndDateObj.getTime();

   var hours = minEndDateObj.getHours();
   var defaultTimeStr = (hours<10?'0'+hours:hours) + ":00"; //TODO:change this if we start supporting half hour intervals
   var minEndTimeStr = defaultTimeStr;
   var maxEndTimeStr = false;
   

   var maxEndDateObj = new Date(dateObj.getTime());
   maxEndDateObj = maxEndDateObj.addMonths(12);
   var maxEndDateInMs = maxEndDateObj.getTime();
   
   var endPickerYearStart = minEndDateObj.getFullYear();
   var endPickerYearEnd   = maxEndDateObj.getFullYear();

   var fromSelectedDateObj = new Date(dateObj.getTime());
   fromSelectedDateObj.setHours(0);
   fromSelectedDateObj.setMinutes(0);
   fromSelectedDateObj.setSeconds(0);
   fromSelectedDateObj.setMilliseconds(0);

   var fromSelectedDateInMs = fromSelectedDateObj.getTime();
   
    $('#end-date').datetimepicker({
                           format:'d.m.Y H:i',
                           lang:'en',
                           closeOnWithouthClick:true,
                           minDate: new Date(minEndDateInMs), 
                           maxDate: new Date(maxEndDateInMs),
                           startDate: new Date(minEndDateInMs),
                           yearStart: endPickerYearStart,
                           yearEnd: endPickerYearEnd,
                           todayButton: false,
                           scrollMonth: false,
                           roundTime: 'ceil', 
                           minTime: minEndTimeStr,
                           maxTime: maxEndTimeStr,
                           defaultTime: defaultTimeStr,
                          
                          onChangeDateTime:
                          function(current_time, $input) {
                                 var selectedDateObj = new Date(current_time);
                                 selectedDateObj.setHours(0);
                                 selectedDateObj.setMinutes(0);
                                 selectedDateObj.setSeconds(0);
                                 selectedDateObj.setMilliseconds(0);
                                 var dateObjInMs = selectedDateObj.getTime();
                                 //console.log('inside enddate...current_time ' + current_time);
                                   
                                 if(dateObjInMs != fromSelectedDateInMs)
                                 { 
                                    //console.log('inside enddate...setting minTime as ' + minStartTimeStr);
	                            this.setOptions({
					    minTime: "00:00"
					     
                                    });
                                 }else {
	                              this.setOptions({
                                           minTime: defaultTimeStr 
                                      });

                                  }
                                 }
                            });


}


function getDateFormated(date) {
  var month = date.getMonth()+1;
  var day = date.getDate();

  var output = date.getFullYear() + '/' +
      (month<10 ? '0' : '') + month + '/' +
      (day<10 ? '0' : '') + day;
  return output;
}

Date.isLeapYear = function (year) { 
    return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0)); 
};

Date.getDaysInMonth = function (year, month) {
    return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
};

Date.prototype.isLeapYear = function () { 
    return Date.isLeapYear(this.getFullYear()); 
};

Date.prototype.getDaysInMonth = function () { 
    return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
};

Date.prototype.addMonths = function (value) {
    var n = this.getDate();
    this.setDate(1);
    this.setMonth(this.getMonth() + value);
    this.setDate(Math.min(n, this.getDaysInMonth()));
    return this;
};

Date.prototype.addDays = function(days)
{
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}

Date.prototype.addHours = function(h) {    
   this.setTime(this.getTime() + (h*60*60*1000)); 
   return this;   
}

Date.prototype.subtractHours = function(h) {    
   this.setTime(this.getTime() - (h*60*60*1000)); 
   return this;   
}

