$(function () {
    
         /* Requirements of this function 
         * 1. At load time, From date picker should be enabled, To date picker should be disabled
         * 2. From date picker, the dates should not be greater than 3 months. If possible, year combo box should also be capped.
         * 3. To date picker, the dates should not be greater than From date + 1 year. If possible ,year combo box should also be capped.
         * 4. Please take care that the from_date and to_date should not go beyond the from_date and end_date of the space
         * 4. BookNow function should be called when From Date, From Time, To Date, To Time is selected (various combinations should be tested)
         */

	$(document).ready(function(){
		var currentDateObj        = new Date();
                var minStartTimeStr; //this is the space opening time
                var currentMinStartTimeStr;//this is the minimum time we would set when the start date picker is loaded 
                                           //in most cases, when the start date picker is first loaded, minimum time is equal to default time.
                                           //however, if the space requires approval, we add one day. in that case minimum time would be equal to
                                           //space opening hour whearas default time we are still keeping as current time (not day).
                var setMinTimeToDefault = true; //in most cases, when the start date picker is first loaded, minimumtime would be equal to default time.
               
                var spaceAvailFromDateObj      = new Date(spacesForMap.SpaceTimeDay.from_date.replace(/\-/g, '/'));  
                var spaceAvailFromDateInMs     = spaceAvailFromDateObj.getTime();

               var spaceAvailToDateObj      = new Date(spacesForMap.SpaceTimeDay.to_date.replace(/\-/g, '/'));
               var currentTimeStr = '0';
               var spaceToTimeStr = '1';
               
               if(spacesForMap.SpaceTimeDay.open_all_hours)
               {
                   //we current store 24 hours as 00:00. 
                   spaceAvailToDateObj.setHours(23); 
                   spaceAvailToDateObj.setMinutes(59); 
                   spaceAvailToDateObj.setSeconds(59); 
	       }
             
               //check to handle the case where the currentTime > space closing Hour 
	       var spaceToHours = spaceAvailToDateObj.getHours();
	       spaceToTimeStr = (spaceToHours<10?'0'+spaceToHours:spaceToHours) + ':00';
	       
               var spaceFromHours = spaceAvailFromDateObj.getHours();
               if(spaceAvailFromDateObj.getMinutes() > 0){
                  spaceFromHours++;
               }

	       var spaceTimingOverFlow = false;
	       var allowTimesArrayOnCurrentDate = new Array();     //use this on current date
	       var allowTimesArrayOnDateChange  = new Array();  //use this when the date/time changes on the picker 
	       
	       /*we assume that the hours are set correctly. This is to handle cases where space operating hours
		* are 09:00 to 08:00 or 09:00 to 00:00
		*/

	       if(spaceToHours < spaceFromHours){
		   spaceTimingOverFlow = true;
	       }
	       spaceFromTimeStr = (spaceFromHours<10?'0'+spaceFromHours:spaceFromHours) + ':00';
               
               var hours   = currentDateObj.getHours();
               var minutes = currentDateObj.getMinutes();
	       currentTimeStr = (hours<10?'0'+hours:hours) + ':' +
			            (minutes<10?'0'+minutes:minutes);
               
	        
                var spaceAvailToDateInMs     = spaceAvailToDateObj.getTime();

                /* add a day in case the property requires manual confirmation from the user */
                if ((spacesForMap.Space.booking_confirmation == "0") || 
                     (spaceTimingOverFlow == false && currentTimeStr >= spaceToTimeStr) || 
		     (spaceTimingOverFlow == false && currentTimeStr < spaceFromTimeStr))
                {
                   currentDateObj = currentDateObj.addDays(1);
                   if(!spacesForMap.SpaceTimeDay.open_all_hours){
                      currentDateObj.setHours(spaceFromHours);
                   }
                   setMinTimeToDefault = false;
                   
                   currentDateObj.setMinutes(0);
                   currentDateObj.setSeconds(0);
                   currentDateObj.setMilliseconds(0);
                }
 
                var currentDateInMs = currentDateObj.getTime();
                

                var maxStartTimeStr;
                var defaultStartTimeStr;

                var startPickerYearStart;
                var startPickerYearEnd;

               //set start-date & end-date as read only. 
               $('#start-date').prop('readonly',true);
               $('#end-date').prop('readonly',true);
               

		
               //find the minimumTimePeriod the space can be rented out 
               var minimumRentalPeriod;
               if (parseInt(bulkBookingSettings.BulkBookingSetting.hourly_price))
               { 
                  minimumRentalPeriod = 1;
               }else if (parseInt(bulkBookingSettings.BulkBookingSetting.daily_price)){
                  minimumRentalPeriod = 2;
               }else if (parseInt(bulkBookingSettings.BulkBookingSetting.weekly_price)){
                  minimumRentalPeriod = 3;
               }else if (parseInt(bulkBookingSettings.BulkBookingSetting.monthly_price)){
                  minimumRentalPeriod = 4;
               }else if (parseInt(bulkBookingSettings.BulkBookingSetting.yearly_price)){
                  minimumRentalPeriod = 5;
               }
    
               
               //currentDateStr = getDateFormated(startDateObj);    

               //minimum date : should be max(current date, spaceAvailFromDateObj)

               var minStartDateObj;

               if((currentDateInMs >= spaceAvailFromDateInMs) &&
                   (currentDateInMs <= spaceAvailToDateInMs) )
               {
                  minStartDateObj = new Date(currentDateInMs);
                  
                  if (spacesForMap.Space.booking_confirmation //if manual confirmation, we have already added one day.The next day can start with current Hour
                                    && 
                      (currentTimeStr < spaceToTimeStr) ||
                      (currentTimeStr > spaceFromTimeStr) //if current time is outside the space operating hours, no need to add an hour
                     )
                   { 
                     minStartDateObj.addHours(1);
                   }
                  
                  minStartDateObj.setMinutes(0);
                  minStartDateObj.setSeconds(0);
               }
              else if ((spaceAvailFromDateInMs > currentDateInMs) &&
                       ( currentDateInMs <= spaceAvailToDateInMs) )
               {
                  minStartDateObj = new Date(spaceAvailFromDateInMs);
                  minStartDateObj.setMinutes(0);
                  minStartDateObj.setSeconds(0);
                  setMinTimeToDefault = false;
               }
              else
               {
                  console.log('This is an error');
                  displayError('This Space is temporarily disabled by Owner. Please go back and choose another space');   
                  return;
               }

               var minStartDateInMs = minStartDateObj.getTime();

               //maximum date: should be min(minStartDate + 3 months, spaceAvailToDateObj)
               var maxStartDateObj = new Date(minStartDateInMs);
               maxStartDateObj.addMonths(3);
               var maxStartDateInMs = maxStartDateObj.getTime();

               if(maxStartDateInMs >= spaceAvailToDateInMs)
               {
                  maxStartDateObj  = new Date(spaceAvailToDateInMs);
                  maxStartDateInMs = spaceAvailToDateInMs;
               }
              /* 
              if (spacesForMap.Space.booking_confirmation){ 
               defaultStartTimeStr = currentDateObj.getHours()+1 + ':00';
              }else{
               defaultStartTimeStr = currentDateObj.getHours() + ':00';
              }*/

              var hours = minStartDateObj.getHours();
              var maxEndTimeStr;

              defaultStartTimeStr = (hours<10?'0'+hours:hours) + ':00';

              //set minTime
              if(spacesForMap.SpaceTimeDay.open_all_hours)
              {
                 minStartTimeStr = false;
                 maxStartTimeStr = '23:10';
                 maxEndTimeStr   = false;
              }
             else
              {
                 /*
                 hours = spaceAvailFromDateObj.getHours();
                 if(spaceAvailFromDateObj.getMinutes() > 0)
                 {
                    hours++;
                 }
                 
                 minStartTimeStr = (hours<10?'0'+hours:hours) + ':00';
                 */
                 
                 
                 hours = spaceAvailToDateObj.getHours();
                 //don't care about space timing being 11:30 or 11:00 as in both cases, maxStartTime is 11
                 maxEndTimeStr = (hours<10?'0'+hours:hours) + ':10'; //we add a dummy 10 mins because for maxTime of 22:00, the time scroller stops at 21:00. the condition is < maxTime*/

 
                 minStartTimeStr = spaceFromTimeStr;
                 maxStartTimeStr = spaceToTimeStr; 
              }


               startPickerYearStart = minStartDateObj.getFullYear();
               startPickerYearEnd   = maxStartDateObj.getFullYear();
               startPickerMonthStart = minStartDateObj.getMonth();
               startPickerMonthEnd   = maxStartDateObj.getMonth();
               
                var minDateWithoutTimeObj = new Date(minStartDateInMs);
                minDateWithoutTimeObj.setHours(0);
                minDateWithoutTimeObj.setMinutes(0);
                minDateWithoutTimeObj.setSeconds(0); 
                minDateWithoutTimeObj.setMilliseconds(0); 
                
                var minDateWithoutTimeInMs = minDateWithoutTimeObj.getTime();

               if(setMinTimeToDefault){
                  currentMinStartTimeStr = defaultStartTimeStr;
               }else {
                  currentMinStartTimeStr = minStartTimeStr;
               }
               //console.log(minStartDateObj);
               //console.log(maxStartDateObj);
               //console.log(minStartTimeStr);
               //console.log(currentMinStartTimeStr);
               //console.log(maxStartTimeStr);
               //console.log(defaultStartTimeStr);


               var stdate = $('#start-date').val();
               var edate = $('#end-date').val();

	       if(stdate && edate)
	       {
                  validateBookingSpace($('#book-space'),null);
	       } 

	       if(spaceTimingOverFlow == false){
		   
		       $('#start-date').datetimepicker({
			       lang:'en',
			       format:'d.m.Y H:i',
			       closeOnWithouthClick:true,
			       minDate: new Date(minStartDateInMs),
			       startDate: new Date(minStartDateInMs),
			       maxDate: new Date(maxStartDateInMs),
			       minTime: currentMinStartTimeStr, 
			       maxTime: maxStartTimeStr,
			       defaultTime: defaultStartTimeStr,
			       monthStart: startPickerMonthStart,
			       monthEnd: -1, //setting an invaild month to stop the month scrolling
			       yearStart: startPickerYearStart,
			       yearEnd: -1, //setting an invalid year to stop the year scrolling 
			       roundTime: 'ceil',
			       scrollMonth:false,
			       showOtherMonths:false,
			       defaultSelect:false,
			       //defaultDate:new Date(minStartDateInMs),
			       onSelectTime:
			       function(ct,$i){
			       },
			       onShow:
				       function(current_time, $input) {
					       var dateObj = new Date(current_time);
					       setNextPrevButtonForStartPicker.call(this,dateObj,minStartDateObj,maxStartDateObj);
				       },
			       onChangeMonth: 
				       function(current_time, $input) {
					       var dateObj = new Date(current_time);
					       setNextPrevButtonForStartPicker.call(this,dateObj,minStartDateObj,maxStartDateObj);
				       },
			       onChangeYear:
				       function(current_time, $input) {
					       var dateObj = new Date(current_time);
					       setNextPrevButtonForStartPicker.call(this,dateObj,minStartDateObj,maxStartDateObj);
				       },
			       onSelectDate:
				       function(current_time, $input) {
					       var dateObj = new Date(current_time);
					       startPickerDateSelected(dateObj,spaceAvailToDateInMs,
							       minStartTimeStr,maxEndTimeStr, 
							       minimumRentalPeriod,false);
				       },

			       onChangeDateTime:
				       function(current_time, $input) {
					       var dateObj = new Date(current_time);
					       dateObj.setHours(0);
					       dateObj.setMinutes(0);
					       dateObj.setSeconds(0);
					       dateObj.setMilliseconds(0);
					       var dateObjInMs = dateObj.getTime();
					       if(dateObjInMs != minDateWithoutTimeInMs)
					       { 
						       this.setOptions({
							       minTime: minStartTimeStr 
						       });
					       }else {
						       if(setMinTimeToDefault){
							       this.setOptions({
								       minTime: defaultStartTimeStr 
							       });
						       }else{
							       this.setOptions({
								       minTime: minStartTimeStr 
							       });
						       }
					       }

					       var currentDateObj = new Date(current_time);
					       startPickerDateSelected(currentDateObj,spaceAvailToDateInMs,
							       minStartTimeStr,maxEndTimeStr,
							       minimumRentalPeriod,false);


				       },


		       });
	       }else{
		       var idx = 0;
		       var fromHours = parseInt(currentMinStartTimeStr.substring(0,2));

		       for(i=fromHours ; i < 24; i++)
		       {
			       var hour = i<10?'0'+i.toString():i.toString();	   
			       allowTimesArrayOnCurrentDate.push(hour + ":00");	   
		       }
		       
		       var spaceToHours = parseInt(spaceToTimeStr.substring(0,2));
		       var spaceFromHours = parseInt(spaceFromTimeStr.substring(0,2)); 

		       for(i=spaceFromHours; i < 24 + spaceToHours; i++)
		       {
		          var idx = i % 24;
			  hour = idx<10?'0'+idx.toString():idx.toString();	   
			  allowTimesArrayOnDateChange.push(hour + ":00");
		       }

		       allowTimesArrayOnDateChange.sort();


		       $('#start-date').datetimepicker({
			       lang:'en',
			       format:'d.m.Y H:i',
			       closeOnWithouthClick:true,
			       minDate: new Date(minStartDateInMs),
			       startDate: new Date(minStartDateInMs),
			       maxDate: new Date(maxStartDateInMs),
			       allowTimes: allowTimesArrayOnCurrentDate, 
			       defaultTime: defaultStartTimeStr,
			       monthStart: startPickerMonthStart,
			       monthEnd: -1, //setting an invaild month to stop the month scrolling
			       yearStart: startPickerYearStart,
			       yearEnd: -1, //setting an invalid year to stop the year scrolling 
			       roundTime: 'ceil',
			       scrollMonth:false,
			       showOtherMonths:false,
			       defaultSelect:false,
			       //defaultDate:new Date(minStartDateInMs),
			       onSelectTime:
			       function(ct,$i){
			       },
			       onShow:
				       function(current_time, $input) {
					       var dateObj = new Date(current_time);
					       setNextPrevButtonForStartPicker.call(this,dateObj,minStartDateObj,maxStartDateObj);
				       },
			       onChangeMonth: 
				       function(current_time, $input) {
					       var dateObj = new Date(current_time);
					       setNextPrevButtonForStartPicker.call(this,dateObj,minStartDateObj,maxStartDateObj);
				       },
			       onChangeYear:
				       function(current_time, $input) {
					       var dateObj = new Date(current_time);
					       setNextPrevButtonForStartPicker.call(this,dateObj,minStartDateObj,maxStartDateObj);
				       },
			       onSelectDate:
				       function(current_time, $input) {
					       var dateObj = new Date(current_time);
					       startPickerDateSelected(dateObj,spaceAvailToDateInMs,
							       minStartTimeStr,maxEndTimeStr, 
							       minimumRentalPeriod,true);
				       },

			       onChangeDateTime:
				       function(current_time, $input) {
					       var dateObj = new Date(current_time);
					       dateObj.setHours(0);
					       dateObj.setMinutes(0);
					       dateObj.setSeconds(0);
					       dateObj.setMilliseconds(0);
					       var dateObjInMs = dateObj.getTime();
					       if(dateObjInMs != minDateWithoutTimeInMs)
					       { 
						       this.setOptions({
							       //minTime: minStartTimeStr 
							       allowTimes: allowTimesArrayOnDateChange
						       });
					       }else {
						       if(setMinTimeToDefault){
							       this.setOptions({
								       allowTimes: allowTimesArrayOnCurrentDate 
							       });
						       }else{
							       this.setOptions({
								       allowTimes: allowTimesArrayOnDateChange 
							       });
						       }
					       }

					       var currentDateObj = new Date(current_time);
					       startPickerDateSelected(currentDateObj,spaceAvailToDateInMs,
							       minStartTimeStr,maxEndTimeStr,
							       minimumRentalPeriod,true);


				       },


		       });
	       }

	       
            
	      
              $('#date1').click(function(){
		  $('#start-date').datetimepicker('show'); //support hide,show and destroy command
	      });

	      $('#date2').click(function(){
		  $('#end-date').datetimepicker('show'); //support hide,show and destroy command
	      });

              $( "#numSlots" ).change(function() {
                 $('.space-price').addClass('hide');
                 $('.sp-commission').addClass('hide');
                 $('.deposit-content').addClass('hide');
                 $('.booking-tip').addClass('hide');
                 $('#show-error').addClass('hide');
		 $('#start-date').val(''); //support hide,show and destroy command
   
                 $('#end-date').val('');
                 $('#end-date').datetimepicker('destroy');
                 $('#end-date').prop('readonly','true');
              });

        });
});


function setNextPrevButtonForStartPicker(dateObj, minStartDateObj,maxStartDateObj,isSpaceTimeOverFlow)
{
   if(dateObj.getMonth() == minStartDateObj.getMonth() && 
      dateObj.getFullYear() == minStartDateObj.getFullYear())
   {
      this.setOptions({
            prevButton: false
       });
   }else {
	this.setOptions({
            prevButton: true 
      });
   }

  if(dateObj.getMonth() == maxStartDateObj.getMonth() && 
    dateObj.getFullYear() <= maxStartDateObj.getFullYear())
  {
	this.setOptions({
              nextButton: false
        });
  } else {
	this.setOptions({
             nextButton: true 
        });

  }


}


function startPickerDateSelected(dateObj,spaceAvailToDateInMs,minStartTimeStr,maxEndTimeStr,minimumRentalPeriod,isSpaceTimeOverFlow)
{
   $('.space-price').addClass('hide');
   $('.sp-commission').addClass('hide');
   $('.deposit-content').addClass('hide');
   $('.booking-tip').addClass('hide');
   $('#show-error').addClass('hide');
   
   $('#end-date').removeAttr('readonly');
   $('#end-date').val('');
   $('#end-date').datetimepicker('destroy');

   //console.log('Entered startPickerDateSelected');
   //console.log(dateObj);
   //console.log('minStartTimeStr ' + minStartTimeStr);
   //console.log('maxStartTimeStr ' + maxStartTimeStr);


   //the starting point for the minimum date is the From Date Choosen.
   //However, after that we will add the minimumTimePeriod for which this space is available
   //example: if minimum hourly is allwoed, minimum date is the From date
   //         if minimum monthly is allowed, minimum date is the From date + a month and so on
   //all these additions will be capped till the ToDate 
   var minEndDateObj;
 
   minEndDateObj = new Date(dateObj.getTime());
   var defaultTimeStr = minStartTimeStr; //dateObj.getHours() + ":00"; //TODO:change this if we start supporting half hour intervals
   var minEndTimeStr ='';
   var minEndDateInMs;
   var error = false;
 
   if(minimumRentalPeriod == 1)//hourly
   {
       //do nothing. maybe see if we add one hour do we go outside the operating hours of the space
       minEndDateObj = minEndDateObj.addHours(1);

       var hours = minEndDateObj.getHours();
       defaultTimeStr = (hours<10?'0'+hours:hours) + ":00"; //TODO:change this if we start supporting half hour intervals

       if(maxEndTimeStr != false) 
       {
          if(defaultTimeStr > maxEndTimeStr)
          {
	     if(!isSpaceTimeOverFlow)
	     {	     
		     minEndDateObj = minEndDateObj.addDays(1);
		     defaultTimeStr = minStartTimeStr;
	     }
          }
       } 
      minEndDateInMs = minEndDateObj.getTime();
      minEndTimeStr  = defaultTimeStr;
   }else if(minimumRentalPeriod == 2) {//daily
      minEndDateObj = minEndDateObj.addDays(1);
   }else if (minimumRentalPeriod == 3){ //weekly
      minEndDateObj = minEndDateObj.addDays(7);
   }else if (minimumRentalPeriod == 4){ //monthly
      minEndDateObj = minEndDateObj.addMonths(1);
   }else if (minimumRentalPeriod == 5){ //yearly
      minEndDateObj = minEndDateObj.addMonths(12);
   }else {
      console.log('Prices for this space have been temporarily disabled by the Owner');
      displayError('Prices for this space have been temporarily disabled by the Owner.');
      return;
   }

   if(minEndTimeStr == ''){
      var hours = minEndDateObj.getHours();
      minEndTimeStr = (hours<10?'0'+hours:hours) + ':00';
   }
   
   minEndDateInMs = minEndDateObj.getTime();
      
   if(minEndDateInMs > spaceAvailToDateInMs)
   {
         console.log('This is an error');
         displayError('This Space is temporarily disabled by Owner. Please go back and choose another space');
         return;
   }
         
   
   var minEndDateWithoutTimeObj = new Date(minEndDateObj.getTime());
   minEndDateWithoutTimeObj.setHours(0);
   minEndDateWithoutTimeObj.setMinutes(0);
   minEndDateWithoutTimeObj.setSeconds(0); 
   minEndDateWithoutTimeObj.setMilliseconds(0); 
   
   var minEndDateWithoutTimeInMs = minEndDateWithoutTimeObj.getTime();
  
   var maxEndDateObj = new Date(dateObj.getTime());
   maxEndDateObj = maxEndDateObj.addMonths(12);
   var maxEndDateInMs = maxEndDateObj.getTime();

   if(maxEndDateInMs > spaceAvailToDateInMs)
   {
      maxEndDateInMs = spaceAvailToDateInMs;
      maxEndDateObj = new Date(maxEndDateInMs);
   }
   
   var endPickerYearStart = minEndDateObj.getFullYear();
   var endPickerYearEnd   = maxEndDateObj.getFullYear();

   //console.log(minEndDateObj);
   //console.log(maxEndDateObj);
   //console.log(defaultTimeStr);
   //console.log(maxEndTimeStr);

   var allowTimesArrayOnCurrentDate = new Array();
   var allowTimesArrayOnDateChange = new Array();

   if(isSpaceTimeOverFlow == true)
   {
	   var spaceFromHour = parseInt(minStartTimeStr.substring(0,2));
	   var spaceToHour   = parseInt(maxEndTimeStr.substring(0,2));
	   var minEndHour    = parseInt(minEndTimeStr.substring(0,2));
	   var idx = 0;
	   var toHour = 24;

	   for(i=minEndHour ; i < toHour; i++)
	   {
		   var hour = i % 24;
		   hour = i<10?'0'+i.toString():i.toString();	   
		   allowTimesArrayOnCurrentDate.push(hour + ":00");	   
	   }
 
	   toHour = spaceToHour + 24;
	   for(i=spaceFromHour; i < toHour; i++)
           {
	     var idx = i % 24;
	     hour = idx<10?'0'+idx.toString():idx.toString();	   
	     allowTimesArrayOnDateChange.push(hour + ":00");	
	   }

	   allowTimesArrayOnDateChange.sort();



	   $('#end-date').datetimepicker({
		   format:'d.m.Y H:i',
		   lang:'en',
		   closeOnWithouthClick:true,
		   minDate: new Date(minEndDateInMs), 
		   maxDate: new Date(maxEndDateInMs),
		   startDate: new Date(minEndDateInMs),
		   yearStart: endPickerYearStart,
		   yearEnd: endPickerYearEnd,
		   todayButton: false,
		   scrollMonth: false,
		   roundTime: 'ceil', 
		   allowTimes: allowTimesArrayOnCurrentDate,
		   defaultTime: defaultTimeStr,
		   //defaultDate: new Date(minEndDateInMs), 
		   onShow:
		   function(current_time, $input) {
			   var dateObj = new Date(current_time);
			   setNextPrevButtonForEndPicker.call(this,dateObj,minEndDateObj,maxEndDateObj);
		   },
		   onChangeMonth: 
			   function(current_time, $input) {
				   var dateObj = new Date(current_time);
				   setNextPrevButtonForEndPicker.call(this,dateObj,minEndDateObj,maxEndDateObj);
			   },
		   onChangeYear:
			   function(current_time, $input) {
				   var dateObj = new Date(current_time);
				   setNextPrevButtonForEndPicker.call(this,dateObj,minEndDateObj,maxEndDateObj);
			   },

		   onChangeDateTime:
			   function(current_time, $input) {
				   var dateObj = new Date(current_time);
				   dateObj.setHours(0);
				   dateObj.setMinutes(0);
				   dateObj.setSeconds(0);
				   dateObj.setMilliseconds(0);
				   var dateObjInMs = dateObj.getTime();
				   //console.log('inside enddate...current_time ' + current_time);

				   if(dateObjInMs != minEndDateWithoutTimeInMs)
				   { 
					   //console.log('inside enddate...setting minTime as ' + minStartTimeStr);
					   this.setOptions({
						   //minTime: minStartTimeStr 
						   allowTimes: allowTimesArrayOnDateChange
					   });
				   }else {
					   //console.log('inside enddate...setting minTime as ' + defaultTimeStr);
					  /* if(minimumRentalPeriod == 1){
						   this.setOptions({
						       allowTimes: allowTimesArrayOnCurrentDate
						   });
					   }else{
						   var hours      = minEndDateObj.getHours();
						   var minTimeStr = (hours<10?'0'+hours:hours) + ':00'; 
						   this.setOptions({
							   minTime: minTimeStr 
						   });

					   }*/
					   this.setOptions({
						   allowTimes: allowTimesArrayOnCurrentDate
					   });
				   }
			   },

		   onSelectDate:
			   function(current_time,$input) {
				   validateBookingSpace($('#book-space'),null);
			   },
		   onSelectTime: 
			   function(current_time,$input) {
				   validateBookingSpace($('#book-space'),null);
			   },
	   });
   }else{
	   $('#end-date').datetimepicker({
		   format:'d.m.Y H:i',
		   lang:'en',
		   closeOnWithouthClick:true,
		   minDate: new Date(minEndDateInMs), 
		   maxDate: new Date(maxEndDateInMs),
		   startDate: new Date(minEndDateInMs),
		   yearStart: endPickerYearStart,
		   yearEnd: endPickerYearEnd,
		   todayButton: false,
		   scrollMonth: false,
		   roundTime: 'ceil', 
		   minTime: minEndTimeStr,
		   maxTime: maxEndTimeStr,
		   defaultTime: defaultTimeStr,
		   //defaultDate: new Date(minEndDateInMs), 
		   onShow:
		   function(current_time, $input) {
			   var dateObj = new Date(current_time);
			   setNextPrevButtonForEndPicker.call(this,dateObj,minEndDateObj,maxEndDateObj);
		   },
		   onChangeMonth: 
			   function(current_time, $input) {
				   var dateObj = new Date(current_time);
				   setNextPrevButtonForEndPicker.call(this,dateObj,minEndDateObj,maxEndDateObj);
			   },
		   onChangeYear:
			   function(current_time, $input) {
				   var dateObj = new Date(current_time);
				   setNextPrevButtonForEndPicker.call(this,dateObj,minEndDateObj,maxEndDateObj);
			   },

		   onChangeDateTime:
			   function(current_time, $input) {
				   var dateObj = new Date(current_time);
				   dateObj.setHours(0);
				   dateObj.setMinutes(0);
				   dateObj.setSeconds(0);
				   dateObj.setMilliseconds(0);
				   var dateObjInMs = dateObj.getTime();
				   //console.log('inside enddate...current_time ' + current_time);

				   if(dateObjInMs != minEndDateWithoutTimeInMs)
				   { 
					   //console.log('inside enddate...setting minTime as ' + minStartTimeStr);
					   this.setOptions({
						   minTime: minStartTimeStr 
					   });
				   }else {
					   //console.log('inside enddate...setting minTime as ' + defaultTimeStr);
					   if(minimumRentalPeriod == 1){
						   this.setOptions({
							   minTime: defaultTimeStr 
						   });
					   }else{
						   var hours      = minEndDateObj.getHours();
						   var minTimeStr = (hours<10?'0'+hours:hours) + ':00'; 
						   this.setOptions({
							   minTime: minTimeStr 
						   });

					   }
				   }
			   },

		   onSelectDate:
			   function(current_time,$input) {
				   validateBookingSpace($('#book-space'),null);
			   },
		   onSelectTime: 
			   function(current_time,$input) {
				   validateBookingSpace($('#book-space'),null);
			   },
	   });
   }



}


function displayError(htmlMessage)
{
	$('#show-error').removeClass('hide');
	$('#show-error').val('');
	$('#start-date').val('').css('background-color','#eee');
	$('#end-date').val('').css('background-color','#eee');
	$('#show-error').html(htmlMessage);

	$('#end-date').prop('readonly',true);
	$('#end-date').val('');
	$('#end-date').datetimepicker('destroy');

	$('#start-date').prop('readonly',true);
	$('#start-date').val('');
	$('#start-date').datetimepicker('destroy');


}
function setNextPrevButtonForEndPicker(dateObj, minEndDateObj,maxEndDateObj)
{
   if(dateObj.getMonth() == minEndDateObj.getMonth() && 
      dateObj.getFullYear() == minEndDateObj.getFullYear())
   {
      this.setOptions({
            prevButton: false
       });
   }else {
	this.setOptions({
            prevButton: true 
      });
   }

  if(dateObj.getMonth() == maxEndDateObj.getMonth() && 
    dateObj.getFullYear() == maxEndDateObj.getFullYear())
  {
	this.setOptions({
              nextButton: false
        });
  } else {
	this.setOptions({
             nextButton: true 
        });

  }


}

function validateBookingSpace(element,eventSpace) 
{
      $('.book-now-button').attr('value','Book Now...');
      $('.book-now-button').attr('disabled',true);
     

        $.ajax({
            type: 'post',
            url: base_url+'spaces/ajax_validateBulkBookingSpace.json',
            data: element.serialize(),
            success:function( data ) {
              $('#book-space').find('.space-park-id').remove();
              $('<input>').attr({
              type: 'hidden',
              name: 'data[Space][space_park_id]',
              value: data.space_park_id,
              class: 'space-park-id'
          }).appendTo('#book-space');
                 if (data.type == 'error') {
                    $('.space-price').addClass('hide');
                    $('.space-price').html('');
                    $('.deposit-content').addClass('hide');
                    $('.deposit-content').html('');
                    $('.booking-tip').addClass('hide');
                    $('.booking-tip').html('');
                    $('.sp-commission').addClass('hide');
                    $('.sp-commission').html('');
                    $('.book-now-button').attr('disabled',true);
                    $('.book-now-button').attr('value','Book Now');
                    $('#show-error').removeClass('hide');
                    $('#show-error').html(data.message);
                }
		 else if (data.type == 'success') { 
			 var priceToDisplay = data.bookingPrice + '/slot';
			 $('.space-price').html(priceToDisplay);
			 $('.space-price').removeClass('hide');
                         var commissionMsg;
                         
                         if(data.spCommission == 1)
                         {
 
			    commissionMsg = '*a small commission will be charged by SimplyPark';
                         }
                        else
                         {
			       commissionMsg = '*Total to pay for all slots Rs.' + data.total;
                         }

                            
			 $('.sp-commission').removeClass('hide');
			 $('.sp-commission').html(commissionMsg);

			 $('.book-now-button').attr('disabled',false);
			 $('.book-now-button').attr('value','Book Now');
			 $('#show-error').html('');
			 
                        if ( data.deposit ) 
			 {
				 $('.deposit-content').html(data.installmentMsg);
				 $('.deposit-content').removeClass('hide');
			 }
			 else
			 {
				 $('.deposit-content').addClass('hide');
			 }
			

			 if ( data.bookingSuggestion ) {
				 var htmlToReplace = '<img src=/img/frontend/tip.png height=25px/>' + '<font color=blue>'+'<b>' + data.bookingSuggestion + '</b>' + '</font>';
				 $('.booking-tip').html(htmlToReplace);
				 $('.booking-tip').removeClass('hide');
			 }
			 else
			 {
				 $('.booking-tip').addClass('hide');
			 }

		 }
             else {
                    $('#show-error').html(data.message);
                    $('.space-price').addClass('hide');
                    $('.space-price').html('');
                    $('.deposit-content').addClass('hide');
                    $('.deposit-content').html('');
                    $('.booking-tip').addClass('hide');
                    $('.booking-tip').html('');
                    $('.sp-commission').addClass('hide');
                    $('.sp-commission').html('');
                    $('.book-now-button').attr('disabled',true);
                    $('.book-now-button').attr('value','Book Now');
                } 
            },
            error:function() {
              $('#show-error').html('An unexpected error occurred. Please try again later!');
                $('.space-price').addClass('hide');
                $('.space-price').html('');
                $('.fa-inr').addClass('hide');
                $('.fa-inr').html('');
                $('.fa').addClass('hide');
                $('.fa').html('');
                $('.yearly-content').addClass('hide');
                $('.yearly-content').html('');
                $('.book-now-button').attr('disabled',true);
                $('.book-now-button').attr('value','Book Now');
            }
        });
}


function getDateFormated(date) {
  var month = date.getMonth()+1;
  var day = date.getDate();

  var output = date.getFullYear() + '/' +
      (month<10 ? '0' : '') + month + '/' +
      (day<10 ? '0' : '') + day;
  return output;
}

function getEventDateFormated(date) {
  var eventMonth = date.getMonth()+1;
  var eventDay = date.getDate();
  var output = (eventDay<10 ? '0' : '') + eventDay + '.' + (eventMonth<10 ? '0' : '') + eventMonth + '.' +
       + date.getFullYear()+ ' ' + (date.getHours()<10 ? '0' : '') + date.getHours() + ':' + (date.getMinutes()<10 ? '0' : '') + date.getMinutes() ;

  return output;
}

Date.isLeapYear = function (year) { 
    return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0)); 
};

Date.getDaysInMonth = function (year, month) {
    return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
};

Date.prototype.isLeapYear = function () { 
    return Date.isLeapYear(this.getFullYear()); 
};

Date.prototype.getDaysInMonth = function () { 
    return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
};

Date.prototype.addMonths = function (value) {
    var n = this.getDate();
    this.setDate(1);
    this.setMonth(this.getMonth() + value);
    this.setDate(Math.min(n, this.getDaysInMonth()));
    return this;
};

Date.prototype.addDays = function(days)
{
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}

Date.prototype.addHours = function(h) {    
   this.setTime(this.getTime() + (h*60*60*1000)); 
   return this;   
}

Date.prototype.subtractHours = function(h) {    
   this.setTime(this.getTime() - (h*60*60*1000)); 
   return this;   
}

