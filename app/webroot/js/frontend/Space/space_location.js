$(function () {

    var formGroup = ".form-group",
        hasSuccess = "has-success",
        hasError = "has-error",
        includeClass = "valid",
        bookingFormId = "#book-space";

    var ValidatorForm = function (fromId){
        $(fromId).validate({
            rules: {
                'data[Space][start_date]': {
                    required: true
                },
                'data[Space][end_date]': {
                    required: true
                }
            },
            messages: {
                'data[Space][start_date]': {
                    required: 'Please select start date and time.'
                },
                'data[Space][end_date]': {
                    required: 'Please select end date and time.'
                }
            },
            highlight : function(element){
                highlightFunc(element);
            },

            success : function(element) {
                successFunc(element);
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    var highlightFunc = function(ele){
        $(ele).closest(formGroup).removeClass(hasSuccess).addClass(hasError);
    }

    var successFunc = function(ele){
        $(ele).addClass(includeClass).closest(formGroup).removeClass(hasError).addClass(hasSuccess);
    }

    $(document).ready(function(){
        

        ValidatorForm(bookingFormId);

        $(document).on('submit', '#book-space', function(e) {

            if ($('#spaceDescModal').length > 0 && typeof e.isTrigger === 'undefined') {
                $('#spaceDescModal').modal();
                return false;
            }

            if (!$('.book-now-button').attr('data-user')) {
                e.preventDefault();
                
                var start_date = $('#start-date').val();
                var end_date = $('#end-date').val();
                var space_id = $('.hidden-space-id').val()
                var space_park_id = $('.space-park-id').val()

                //setting cookie when the user logins with facebook
                var domain_string = "; domain=" + serverName;
                document.cookie = "start-date=" + start_date + "; max-age=" + 60*60*24*1 + "; path=/" + domain_string ;
                document.cookie = "end-date=" + end_date + "; max-age=" + 60*60*24*1 + "; path=/" + domain_string ;
                document.cookie = "space-id=" + space_id + "; max-age=" + 60*60*24*1 + "; path=/" + domain_string ;
                document.cookie = "space-park-id=" + space_park_id + "; max-age=" + 60*60*24*1 + "; path=/" + domain_string ;
                document.cookie = "current-action=" + currentAction + "; max-age=" + 60*60*24*1 + "; path=/" + domain_string ;
                
                $('<input>').attr({
                    type: 'hidden',
                    name: 'data[User][booking_start_date]',
                    value: start_date
                }).appendTo('#loginForm, #signupForm');

                $('<input>').attr({
                    type: 'hidden',
                    name: 'data[User][booking_end_date]',
                    value: end_date
                }).appendTo('#loginForm, #signupForm');

                $('<input>').attr({
                    type: 'hidden',
                    name: 'data[User][space_id]',
                    value: space_id
                }).appendTo('#loginForm, #signupForm');

                $('<input>').attr({
                    type: 'hidden',
                    name: 'data[User][space_park_id]',
                    value: space_park_id
                }).appendTo('#loginForm, #signupForm');


                $('#loginModal').modal();
            }
        });

        $(document).on('click', '.booking-ok', function() {
            $('#spaceDescModal').modal('hide');
            $('#book-space').submit();
        });

        $(document).on('click', '.more-info', function(){
            $(this).addClass('less-info').removeClass('more-info').html('Less');
            $('.show-more-info').removeClass('hide');
        });
        $(document).on('click', '.less-info', function(){
            $(this).addClass('more-info').removeClass('less-info').html('More');
            $('.show-more-info').addClass('hide');
        });
    });
});

function initialize() {
    var myLatlng = new google.maps.LatLng(spacesForMap.Space.lat,spacesForMap.Space.lng);
    var mapOptions = {
        zoom: 15,
        center: myLatlng
    }
    var map = new google.maps.Map(document.getElementById('space-on-map'), mapOptions);
    var slots = spacesForMap.Space.number_slots != null ? parseInt(spacesForMap.Space.number_slots) : 0;
    //var hourlyRate = spacesForMap.SpacePricing.hourly > 0 ? ' &#8377 '+spacesForMap.SpacePricing.hourly+'/hr' : 'N/A';

    var Rate;
    if(spacesForMap.Space.tier_pricing_support == 1) {
	    Rate = spacesForMap.Space.tier_formatted_desc;
    }else if (spacesForMap.SpacePricing.hourly !== null && spacesForMap.SpacePricing.hourly > 0) {
        Rate = spacesForMap.SpacePricing.hourly + ' /hr';
    } else if (spacesForMap.SpacePricing.daily !== null && spacesForMap.SpacePricing.daily > 0) {
        Rate = spacesForMap.SpacePricing.daily + ' /day';
    } else if (spacesForMap.SpacePricing.weekely !== null && spacesForMap.SpacePricing.weekely > 0) {
        Rate = spacesForMap.SpacePricing.weekely + ' /week';
    } else if (spacesForMap.SpacePricing.monthly !== null && spacesForMap.SpacePricing.monthly > 0) {
        Rate = spacesForMap.SpacePricing.monthly + ' /month';
    } else if (spacesForMap.SpacePricing.yearly !== null && spacesForMap.SpacePricing.yearly > 0) {
        Rate = spacesForMap.SpacePricing.yearly + ' /annum';
    } else {
        Rate = 'N/A';
    }

    var contentString = "";
    if(spacesForMap.Space.tier_pricing_support == 1){
       contentString = '<div class="info_content text-center"><h3 class="text-primary infowindowh3">'+spacesForMap.Space.name+'</h3>' +
            '<b class="text-center">Price: </b>'+Rate+'<br>'+
            '<b class="text-center">Available Slots: </b>'+slots+'</div>';
    }else{
       contentString = '<div class="info_content text-center"><h3 class="text-primary infowindowh3">'+spacesForMap.Space.name+'</h3>' +
            '<b class="text-center">Price: </b>&#8377 '+Rate+
            '<b class="text-center">Available Slots: </b>'+slots+'</div>';
    }

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    var icon = {
              url: base_url+"/img/unselected_map_marker.png",
              size: new google.maps.Size(47, 56)
            };

    var marker = new google.maps.Marker({
        position: myLatlng,
        icon: icon,
        map: map
    });

    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map,marker);
    });

    infowindow.open(map,marker);
}
google.maps.event.addDomListener(window, 'load', initialize);
