$.widget( "custom.catcomplete", $.ui.autocomplete, {
    _renderMenu: function( ul, items ) {
        ul.addClass('search-box');
        var that = this;
        var k = 0;
        var i = 0;
        var j = 0;
        var spaceCount = 0;
    
        $.each( items, function( index, item ) {
            var li;
           
            if ( item.category == "Gated Community" && k == 0 ) {
                ul.append( "<h4><i class ='fa fa-building-o fa-fw'></i> Gated Space </h4>" );
                k++;
            }

            if( item.category == "Spaces" ) {
                spaceCount++;
            }

            if ( item.category == "Spaces" && i == 0) {
                ul.append('<h4><i class="fa fa-car fa-fw"></i> Parking Space</h4>');
                i++;
            }

            if ( item.category == "Events" && j == 0 ) {

                if (i > 0 && spaceCount >= 5) 
                { 
                  ul.append('<a href="'+base_url+'Spaces/listSpaces?search_string='+$("#search_new").val()+'" class="see-result text-right">See All Result</a>');
                }
                ul.append('<h4><i class="fa fa-calendar"></i> Venues</h4>');
                j++
            }
            li = that._renderItemData( ul, item );

        });
    }
});
  
$(function() {
    $( "#search_new" ).catcomplete({
        delay: 0,
        //source: data
        source: function( request, response ) {
            $.ajax({
                url: base_url + "searches/index.json",
                dataType: "json",
                data: {term: request.term},
                success: function(data) {
                    var count = 0;
                    response($.map(data, function(item) {
                   //console.log(item);
                       if(item == 'error') {
                             return {
                                label: "No records Found",
                                value: "<article class='no-search search-not-found'>No records found. <b>Click here</b> to get notification for your keyword.</article>",
                                cateogry: "NoResults"
                                }
                        }
                        if ( item.Space ) {
                            var space_body = '';
                            var city = (item.City.name != null) ? '- '+item.City.name : '';
                            var state = (item.State.name != null) ? item.State.name : '';
                            var flat_apartment_number = (item.Space.flat_apartment_number != null) ? item.Space.flat_apartment_number : '';
                            var address1 = (item.Space.address1 != null) ? ', '+item.Space.address1 : '';
                            var address2 = (item.Space.address2 != null) ? ','+item.Space.address2 : '';

                            space_body = space_body + "<article id=Space"+item.Space.id+"><strong><a class='search-name-link' href="+base_url+"spaces/listNearBySpaces/"+encodeURIComponent(Base64.encode(item.Space.id))+">"+item.Space.name+"</a>  "+city+"</strong></br>"+flat_apartment_number+" "+address1+" "+address2+" "+city+" "+state+"</article>";
                            var space_label = item.Space.name + city +" "+ flat_apartment_number + " "+ address1+ " "+address2 + " " + city + " "+state;
                            count++;
                            if (count <= 5) {
                                var obj1 = {
                                    label: space_label,
                                    value: space_body,
                                    category: "Spaces"
                                }

                            } 
                            
                                
                        } 
                        if ( item.gatedData ) {
                            //console.log(item.gatedData);
                             var gatedData_body = '';
                                gatedData_body = gatedData_body + "<article class='no-search' id=Gated-"+randString(item.gatedData.count)+"><strong>"+item.gatedData.count + " Space(s) Found within community : <a class='search-gated-space' data-url='"+base_url + "searches/ajaxGetGatedSpace/"+item.gatedData.search_string+"' href='#'>"+item.gatedData.search_string+"</a></strong></article>";
                                var gatedData_label = item.gatedData.count + " Space(s) found";
                                var obj2 = {
                                    label: gatedData_label,
                                    value: gatedData_body,
                                    category: "Gated Community"
                                }
                                
                        } 
                        if ( item.Event )
                        {
                            var event_body = '';
                            event_body = event_body + "<article id=Event"+item.Event.id+"><strong><a class='search-name-link' href="+base_url+"events/eventDetail/"+encodeURIComponent(Base64.encode(item.Event.id))+">"+item.Event.event_name+"</a></strong></br>"+item.EventAddress.address+" "+item.City.name+"</article>";
                            var event_label = item.Event.event_name+" "+item.EventAddress.address;

                            var obj3 = {
                            label: event_label,
                            value: event_body,
                            category: "Events"
                            }
                        }
                        var obj = jQuery.extend(obj1,obj2,obj3);
                        return obj;
                    }));
                }
            });
        },
        focus: function( event, ui ) {
            var location;
            check_if_id_exists(ui);
            location = searchOnFocusAndSelect(ui);
            $('#search-btn').prop('href',location);  
            return false;
        },
        select: function(event,ui) {
            var location,url;
            var ele = $.parseHTML(ui.item.value);

            if ( ui.item.category == 'Gated Community' ) {
                url = $('#' + ele[0].id).find('a').attr('data-url');
                $.getJSON( url, function( data ) {
                    $('.badge').html(data.count);
                    var gatedSpaceTemplate = $("#showGatedSpaceField").html();
                    resultData = _.template(gatedSpaceTemplate, {data: data});
                    $('#showGatedSpaceContainer').html(resultData);
                    $('#searchGatedSpace').modal('show');
                }).fail(function() {
                    $('#searchGatedSpace').modal('hide');
                });
            } else {
                check_if_id_exists(ui);
                location = searchOnFocusAndSelect(ui);
                window.location.href = location;    
            }
            
            return false;
        },
        create: function (e) {
            $(this).prev('.ui-helper-hidden-accessible').remove();
        }      
    });
});


$.ui.autocomplete.prototype._renderItem = function(ul, item) {
    return $("<a></a>")
        .append(item.value)
        .appendTo(ul);
}

//Random string generator
function randString(x){
    var s = "";
    while(s.length<x&&x>0){
        var r = Math.random();
        s+= (r<0.1?Math.floor(r*100):String.fromCharCode(Math.floor(r*26) + (r>0.5?97:65)));
    }
    return s;
}

// check_if_id_exists functions checks for the element id  
// return void

var check_if_id_exists = function(ui) {
    var ele = $.parseHTML(ui.item.value);
    if(ele[0].id.length>0) //check if not an empty string
    {
        
        $('#search_new').val(ui.item.label);
        $('.ui-menu-item').find('article').removeClass('focus-autocomplete-item');
        $('#'+ele[0].id).addClass('focus-autocomplete-item'); 

        
    } 
    else 
    {
        var search_string = $("#search_new").val();
        $( "#search_new" ).val(search_string);
    }
       
}

var searchOnFocusAndSelect = function(ui) {
    if ( $('.search').find('div#element-container') || $('.search-spot').find('div#element-container')) {
        $('#element-container').remove();
    }

    //for user and home header 
    $('.search').append('<div class="hide" id="element-container"></div>');

    //for main landing page
    $('.search-spot').append('<div class="hide" id="element-container"></div>');

    $('#element-container').append($.parseHTML(ui.item.value));
    href = $('#element-container').find('a').attr('href');
    return href;
}



