$(function () {
    var map, geocoder, marker, infowindow, initialLocation = document.getElementById('location');

    var initialize = function() {
        var lat = 1.10,
        lng = 1.10,
        zoom = 2;

        if (document.getElementById('latitude').value != '' && document.getElementById('longitude').value != '') {
            lat = document.getElementById('latitude').value;
            lng = document.getElementById('longitude').value;
            zoom = 16;
        }

        var latlng = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));

        var myOptions = {
            zoom: zoom,
            center: latlng,
            panControl: true,
            scrollwheel: false,
            scaleControl: true,
            overviewMapControl: true,
            overviewMapControlOptions: { opened: true }
        };
        
        map = new google.maps.Map(initialLocation,
                myOptions);
        geocoder = new google.maps.Geocoder();
        marker = new google.maps.Marker({
            draggable: isMapDraggable,
            position: latlng,
            map: map
        });

        //map.streetViewControl = false;
        infowindow = new google.maps.InfoWindow({
            content:  "(" + parseFloat(lat) + ", " +
                        +parseFloat(lng)+ ")"
        });

        google.maps.event.addListener(map, 'click', function(event) {
            marker.setPosition(event.latLng);

            var yeri = event.latLng;

            var latlongi = "(" + yeri.lat().toFixed(6) + ", " +yeri.lng().toFixed(6) + ")";

            infowindow.setContent(latlongi);

            document.getElementById('latitude').value = yeri.lat().toFixed(6);
            document.getElementById('longitude').value = yeri.lng().toFixed(6);

            geocoder.geocode({'latLng': event.latLng}, function(results, status) {
                  if (status === google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        marker.setPosition(event.latLng);
                        infowindow.open(map, marker);
                    }
                  }
                }
            );
        });

        google.maps.event.addListener(marker, 'dragend', function (event) {
            document.getElementById("latitude").value = this.getPosition().lat().toFixed(6);
            document.getElementById("longitude").value = this.getPosition().lng().toFixed(6);
        });
    };

    $(document).ready(function(){
        initialize();
    });
});
