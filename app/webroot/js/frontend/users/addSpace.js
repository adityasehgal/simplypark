$(function () {

    var park_number_index = 0;
    var totalSlots = $('#total-slots');
    var smallCarSlots = $('#small-cars');
    var gatedPark = $('.gated-park');
    var smallCarCheckBox = $('#small-car-slot');

    var addSpaceValidate = function(ele){
		$(ele).validate({
			rules: {
                "data[Space][name]": {
                    required: true
                },
                "data[Space][flat_apartment_number]": {
                    required: true
                },
                "data[Space][address1]": {
                    required: true,
                },
                "data[Space][address2]": {
                    required: true,
                },
                "data[Space][state_id]": {
                    required: true,
                },
                "data[Space][post_code]": {
                    required: true,
                    number: true
                },
                "data[Space][tower_number]": {
                    required: function(element){
                        return $("#property_type_id").val() == 2;
                    }
                },
                "data[SpacePark][0][park_number]": {
                    required: function(element){
                        return $("#property_type_id").val() == 2;
                    }
                },
                "data[Space][place_pic]": {
                    fileType: true
                },
                "data[Space][number_slots]": {
                    required: true,
                    digits: true,
                    min: 1
                }
			},
            messages: {
                "data[Space][name]": {
                    required: 'Please enter Space Name.'
                },
                "data[Space][flat_apartment_number]": {
                    required: 'Please enter Flat/Apartment Number.'
                },
                "data[Space][address]": {
                    required: 'Please enter Address.'
                },
                "data[Space][state_id]": {
                    required: 'Please Select State.'
                },
                "data[Space][post_code]": {
                    required: 'Please enter Postal code.',
                    number: 'Please enter valid Postal Code.'
                },
                "data[Space][tower_number]": {
                    required: 'Please enter tower number.',
                }
            },
            groups: {
                priceGroup: 'data[Space][hourly] data[Space][daily] data[Space][monthly] data[Space][yearly] data[Space][weekely]',
            },
            errorPlacement: function(error, element) {
               if (error.attr('for') === "priceGroup"){
                    error.insertBefore('#price-text').wrap("<div class='col-sm-offset-3 col-xs-8 group-error'></div>")
                    //error.insertBefore('.text-info').wrap("<div></div>");
                   $('.group-error').append(error);
               } else if (error.attr('for') === "total-slots") {
                    error.insertAfter('#total-slots').wrap("<div class='total-slots-error'></div>")
                    $('.total-slots-error').append(error);
               } else {
                   element.after(error);
               }
            }
		});
        jQuery.validator.addMethod("fileType", function() {
            var ext = $('.place_pic').val().split('.').pop().toLowerCase();
            
            if (ext == '') {
                return true;
            }

            if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
                return false;
            }
            return true;
        }, "File Type not supported.");
	};

    var addSpacePolicyValidate = function(ele){
        $(ele).validate({
            rules: {
                "data[Space][TandC]": {
                    required: true,
                },
            },
            messages: {
                "data[Space][TandC]": {
                    required: 'Agree to terms and conditions.'
                }
            },
            errorPlacement: function(error, element) {
                if (error.attr('for') === "data[Space][TandC]"){
                    error.insertAfter('.term-condition > label').wrap("<div></div>");
                }
                else{
                    element.after(error);
                }
            },
        });
    };

    var addSpaceDetailsValidate = function(ele){
        $(ele).validate({
            rules: {
                "data[SpaceTimeDay][from_date]": {
                    required: true
                },
                "data[SpaceTimeDay][from_time]": {
                    required: '.24-hours-open:unchecked'
                },
                "data[SpaceTimeDay][to_date]": {
                    required: true,
                },
                "data[SpaceTimeDay][to_time]": {
                    required: '.24-hours-open:unchecked'
                },
                "data[SpaceTimeDay][mon]": {
                    dayCheck: true
                },
                "data[SpaceTimeDay][tue]": {
                    dayCheck: true
                },
                "data[SpaceTimeDay][wed]": {
                    dayCheck: true
                },
                "data[SpaceTimeDay][thu]": {
                    dayCheck: true
                },
                "data[SpaceTimeDay][fri]": {
                    dayCheck: true
                },
                "data[SpaceTimeDay][sat]": {
                    dayCheck: true
                },
                "data[SpaceTimeDay][sun]": {
                    dayCheck: true
                },
                "data[SpaceTimeDay][weekdays]": {
                    dayCheck: true
                },
                "data[SpaceTimeDay][weekends]": {
                    dayCheck: true
                },
                "data[SpaceTimeDay][whole_week]": {
                    dayCheck: true
                },
                "data[SpacePricing][hourly]": {
                    number: true,
                    priceCheck: true,
                    min: 0
                },
                "data[tier_desc]": {
                    priceCheck: true,
                },
                "data[SpacePricing][daily]": {
                    number: true,
                    priceCheck: true,
                    min: 0
                },
                "data[SpacePricing][weekely]": {
                    number: true,
                    priceCheck: true,
                    min: 0
                },
                "data[SpacePricing][monthly]": {
                    number: true,
                    priceCheck: true,
                    min: 0
                },
                "data[SpacePricing][yearly]": {
                    number: true,
                    priceCheck: true,
                    min: 0
                },
            },
            messages: {
                "data[SpaceTimeDay][from_date]": {
                    required: 'Please enter start date.'
                },
                "data[SpaceTimeDay][from_time]": {
                    required: 'Please enter start time.'
                },
                "data[SpaceTimeDay][to_date]": {
                    required: 'Please enter end date.'
                },
                "data[SpaceTimeDay][to_time]": {
                    required: 'Please enter end time.'
                },
                "data[SpacePricing][hourly]": {
                    number: 'Please enter valid Price.',
                    min: 'Negative value is not allowed'
                },
                "data[SpacePricing][daily]": {
                    number: 'Please enter valid Price.',
                    min: 'Negative value is not allowed'
                },
                "data[SpacePricing][monthly]": {
                    number: 'Please enter valid Price.',
                    min: 'Negative value is not allowed'
                },
                "data[SpacePricing][yearly]": {
                    number: 'Please enter valid Price.',
                    min: 'Negative value is not allowed'
                },
                "data[SpacePricing][weekely]": {
                    number: 'Please enter valid Price.',
                    min: 'Negative value is not allowed'
                }
            },
            groups: {
                dayGroup: 'data[SpaceTimeDay][mon] data[SpaceTimeDay][tue] data[SpaceTimeDay][wed] data[SpaceTimeDay][thu] data[SpaceTimeDay][fri] data[SpaceTimeDay][sat] data[SpaceTimeDay][sun] data[SpaceTimeDay][weekends] data[SpaceTimeDay][weekdays] data[SpaceTimeDay][whole_week]',
                priceGroup: 'data[SpacePricing][hourly] date[SpacePricing][tierHourly] data[SpacePricing][daily] data[SpacePricing][monthly] data[SpacePricing][yearly] data[SpacePricing][weekely]'
            },
            errorPlacement: function(error, element) {
                if (error.attr('for') === "dayGroup"){
                    $('.week-box-inline div[class="col-sm-8"]').append(error);
                    error.wrap("<div></div>");
                } else if (error.attr('for') === "priceGroup"){
                    error.insertBefore('#price-text').wrap("<div class='col-sm-offset-3 col-xs-8 group-error'></div>")
                    //error.insertBefore('.text-info').wrap("<div></div>");
                   $('.group-error').append(error);
                } else if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
        jQuery.validator.addMethod("dayCheck", function() {
                    if ( $('input[name="data[SpaceTimeDay][mon]"]').is(":checked") ) {
                        return true;
                    }

                    if ( $('input[name="data[SpaceTimeDay][tue]"]').is(":checked") ) {
                        return true;
                    }

                    if ( $('input[name="data[SpaceTimeDay][wed]"]').is(":checked") ) {
                        return true;
                    }

                    if ( $('input[name="data[SpaceTimeDay][thu]"]').is(":checked") ) {
                        return true;
                    }

                    if ( $('input[name="data[SpaceTimeDay][fri]"]').is(":checked") ) {
                        return true;
                    }

                    if ( $('input[name="data[SpaceTimeDay][sat]"]').is(":checked") ) {
                        return true;
                    }

                    if ( $('input[name="data[SpaceTimeDay][sun]"]').is(":checked") ) {
                        return true;
                    }

                    if ( $('input[name="data[SpaceTimeDay][weekdays]"]').is(":checked") ) {
                        return true;
                    }

                    if ( $('input[name="data[SpaceTimeDay][weekends]"]').is(":checked") ) {
                        return true;
                    }
                    if ( $('input[name="data[SpaceTimeDay][whole_week]"]').is(":checked") ) {
                        return true;
                    }

                    return false;

                }, "Please select atleast a Day.");
        jQuery.validator.addMethod("priceCheck", function() {
            if ($('input[name="data[SpacePricing][hourly]"]').val() > 0 ||
                    $.trim($('#tier_desc').val())  ||
                    $('input[name="data[SpacePricing][daily]"]').val() > 0 ||
                    $('input[name="data[SpacePricing][weekely]"]').val() > 0 ||
                    $('input[name="data[SpacePricing][monthly]"]').val() > 0 ||
                    $('input[name="data[SpacePricing][yearly]"]').val() > 0
                    ) {
                return true;
            }
            return false;

        }, "Please enter atleast one price.");
    };

    var showSmallCarSlots = function(){
        if($('#small-car-slot:checked').length) {
            $("#small-car-slot-input").hide();
        } else {
            $("#small-car-slot-input").show();
        }
    };

    var showDepositAmount = function(){
        if($('#is-deposit:checked').length) {
            $("#deposit-price-input").show();
        } else {
            $("#deposit-price-input").hide();
        }
    };

    var addCity = function(state_id, url) {
        $.ajax({
            url: url,
            type: 'get',
            dataType: "json",
            data: {stateId : state_id},
            success:function( data ) {
                var addCityTemplate = $("#addCity").html();
                resultData = _.template(addCityTemplate, {data: data});
                $('#list-city').html(resultData);
            }
        });
    };

    var showHideHourlyPrice = function() {
        var property_type_id = parseInt($( "#property_type_id" ).val());

        
        if (property_type_id == 2) {
            $('.whole-week,.check-days').prop('checked',true).prop('disabled',true); // Mark the checkbox whole week ON by default and DO not allow it to be changed. [ONLY FOR GATED COMMUNITY]
            $('.weekdays,.weekends').prop('disabled',true);
            $('.24-hours-open').prop('checked',true).prop('disabled',true);;// Mark the 24 hours open checkbox by default and DO not allow it to be changed. [ ONLY FOR GATED COMMUNITY]


            $('.price-basis').html('monthly and yearly');
            $('.price-single').html('monthly');
            $('.price-single-cap').html('Monthly');
            $('#hourly, #daily, #weekely').val('');
            $('.hourly-input, .daily-input, .weekly-input').removeClass('show').addClass('hide');

            if ($('#yearly').val() == '') {
                $('#yearly').addClass('hide');
            }

            $('.gated-tower').addClass('show');
            gatedPark.addClass('show');
        } else {
            $('.price-basis').html('hourly, daily, weekly, monthly and yearly');
            $('.price-single').html('hourly');
            $('.price-single-cap').html('Hourly');
            $('.hourly-input, .daily-input, .weekly-input').addClass('show');
            $('.gated-tower').find('input').val('');
            gatedPark.find('input').val('');
            $('.gated-tower').removeClass('show').addClass('hide');
            gatedPark.removeClass('show').addClass('hide');
        }
        priceBox();
    };

    var manageCarParkNumber = function() {
        var totalParks = parseInt(totalSlots.val());

        /*if (!smallCarCheckBox.is(':checked') && smallCarSlots.val() != '') {
            var totalParks = parseInt(totalSlots.val())+parseInt(smallCarSlots.val());            
        }*/

        if (totalParks > 0) {
            if (gatedPark.find('.form-group').length == totalParks) {
                return false;
            }

            if (gatedPark.children('.form-group').length > 0) {
                var id_attr = gatedPark.children(".form-group:last-child").attr('id');
                if (typeof id_attr !== typeof undefined && id_attr !== false) {
                    park_index = parseInt(gatedPark.children(".form-group:last-child").attr('id'));
                    park_number_index = park_index+1;                    
                } else {
                    park_number_index++;
                }
            } else {
                park_number_index++;
            }

            gatedPark.append('<div class="form-group">'+
                    '<div class="col-sm-4 col-sm-offset-3">'+
                        '<input required name="data[SpacePark]['+park_number_index+'][park_number]" class="form-control" placeholder="Gated Community Park Number" type="text">'+
                    '</div>'+
                '</div>'
            ); //add input box
        }
    };

    var removeCarParkNumber = function(element) {

        var slotsLength = totalSlots.val();
        var gatedParksLength = gatedPark.children('.form-group').length;

        /*if (smallCarSlots.val() != '' && !smallCarCheckBox.is(':checked')) {
            var slotsLength = parseInt(totalSlots.val())+parseInt(smallCarSlots.val());            
        }*/

        if (gatedParksLength < slotsLength) {
            for (var i = 1; i < slotsLength; i++) {
                manageCarParkNumber();
            }
        }

        if (slotsLength != '' && slotsLength > 0) {
            if (gatedParksLength > slotsLength) {
                var removeParks = gatedParksLength - slotsLength;
                $('.gated-park > .form-group').slice(-removeParks).remove();
            }
        }
    };

    var makeSmallCarSlotEmpty = function() {
        smallCarSlots.val('');
        removeCarParkNumber();
    }

    var checkWeekDaysDisabled = function() {
        if ($('.wdays:checked').length == 5) {
            $('.weekdays').prop('checked',true);
        } else {
            $('.weekdays').prop('checked',false);
        }
    };

    var checkWeekEndsDisabled = function () {
        if ($('.wend:checked').length == 2) {
            $('.weekends').prop('checked',true);
        } else {
            $('.weekends').prop('checked',false);
        }
    };

    var manageConfirmation = function(element) {
        if (element.val() == 0) {
            $('.manually-confirm-note').removeClass('hide');
        } else {
            $('.manually-confirm-note').addClass('hide');
        }
    }

    var manageWholeWeek = function(element) {
        if (element.is(':checked')) {
            $('.wdays, .wend').prop('checked',true);
            $('.weekends, .weekdays').prop('checked',false);
            $('.weekends, .weekdays').prop('disabled',true);
        } else {
            $('.wdays, .wend').prop('checked',false);
            $('.weekends, .weekdays').prop('checked',false);
            $('.weekends, .weekdays').prop('disabled',false);
        }
    }

    var addSlots = function(element) {
        
        if (element.val() > 0) {
            $('.hidden-slots').remove();
            for (var i = 0; i < element.val(); i++) {
                $('<input>').attr({
                    type: 'hidden',
                    name: 'data[SpacePark]['+i+'][park_number]',
                    value: 'slot'+i,
                    class: 'hidden-slots'
                }).appendTo('#add-space');
            }
        }
    }

	$(document).ready(function(){
        addSpaceValidate('#add-space');
        addSpacePolicyValidate('#add-space-policy');
		addSpaceDetailsValidate('#add-space-details');
        showSmallCarSlots();
        showHideHourlyPrice();
        showDepositAmount();

        if ($('#SpaceTierPricingSupport').is(':checked')) {
               $("#showTierPricingTextArea").addClass('show');
               $('#hourly').prop('disabled',true);
        }else {
               $("#showTierPricingTextArea").removeClass('show');
               $("#showTierPricingTextArea").addClass('hide');
               $('#hourly').prop('disabled',false);
       }
        $('#SpaceTierPricingSupport').click(function() {
            if(this.checked){
               $("#showTierPricingTextArea").addClass('show');
               $('#hourly').prop('disabled',true);
            }else{
               $("#showTierPricingTextArea").removeClass('show');
               $("#showTierPricingTextArea").addClass('hide');
               $('#hourly').prop('disabled',false);
            }
         });
  

        //commenting out activate-checkbox as the space will always be active by default
        $(".activate-checkbox").bootstrapSwitch();

        $(document).on('change', '#state', function(){
            var state_id = this.value;
            var url = $(this).attr('data-url');
            addCity(state_id, url);
        });

        $(document).on('click', '#small-car-slot', function(){
            showSmallCarSlots();
        });

        $(document).on('click', '#is-deposit', function(){
            showDepositAmount();
        });

        //$("[name='data[Space][is_activated]']").bootstrapSwitch();

        $('.activate-checkbox').on('switchChange.bootstrapSwitch', function (event, state) { 
            var is_activated;
            var serverUrl = $(this).attr("data-serverUrl");
            if(state == true) {
                is_activated = 1;
            } else {
                is_activated = 0;
            }
            var request ={'is_activated': is_activated};
            $.ajax({
                url: serverUrl,
                type: 'POST',
                dataType: "json",
                data: {result: request},
                // success:function( data ) {
                // },
                complete: function(){
                    location.reload(true);
                }
            });
        });

        $(document).on('change', '#property_type_id', function(){

            if ($(this).val() == 2) {
                $('#add-space').find('.hidden-slots').remove();
            } else {
                addSlots($('#total-slots'));
            }

            showHideHourlyPrice();
            manageCarParkNumber();
            removeCarParkNumber();
        });

        $(".weekdays").click(function() {
          if(this.checked) {
                $('.wdays').each(function() {
                    this.checked = true;
                });
                $('.wend').each(function() {
                    this.disabled = true;
                });
                $('.weekends').attr('disabled',true);
                $('.weekends').prop('checked',false);

                $('.whole-week').attr('disabled',true);
                $('.whole-week').prop('checked',false);

                $('.wend').attr('checked', false);
          } else {
                $('.wdays').each(function() {
                    this.checked = false;
                });
                $('.wend').each(function() {
                    this.disabled = false;
                });
                $('.weekends').attr('disabled',false);
                $('.whole-week').attr('disabled',false);
          }

       });

       $(".weekends").click(function() {
          if(this.checked) {
                $('.wend').each(function() {
                    this.checked = true;
                });
                $('.wdays').each(function() {
                    this.disabled = true;
                });
                $('.weekdays').attr('disabled',true);
                $('.weekdays').prop('checked',false);

                $('.whole-week').attr('disabled',true);
                $('.whole-week').prop('checked',false);

                $('.wdays').attr('checked', false);

          } else {
                $('.wend').each(function() {
                    this.checked = false;
                });
                $('.wdays').each(function() {
                    this.disabled = false;
                });
                $('.weekdays').attr('disabled',false);
                $('.whole-week').attr('disabled',false);
          }

        });

        $('.whole-week').click(function() {
            manageWholeWeek($(this));
        });

        $(document).on('click','.wdays', function(){
            checkWeekDaysDisabled();
        });

        $(document).on('click','.wend', function(){
            checkWeekEndsDisabled();
        });

        $(document).on('blur', '#total-slots, #small-cars', function(){
            
            if ($('#property_type_id').val() == 2) {
                removeCarParkNumber($(this));
            } else {
                addSlots($(this));
            }
        });

        $(document).on('click', '#small-car-slot', function(){
            makeSmallCarSlotEmpty();
        });

        $(document).on('click','.how-confirm',function() {
            manageConfirmation($(this));
        });

        $('input[name="data[Space][is_activated]"]').on('switchChange.bootstrapSwitch', function(event, state) {
            if (state) {
                $('.space-state-text').html('Your listing is now activated.');
            } else {
               $('.space-state-text').html('Your listing is currently disabled.');
            }
        });


        $(document).on('click', '.check-days, .whole-week, .weekdays, .weekends', function(){
            priceBox();
        });

        $(document).on('click','.24-hours-open', function() {
            priceBox();
        });
        $(document).on('submit','#add-space-details', function(e) {
            
            if ($('#property_type_id').val() != 2 && typeof e.isTrigger === 'undefined') {
                if (
                        $('.price input[type="text"]').filter(function() {
                            return !!this.value;
                        }).length == 1) {
                    $('#priceAlertModal').modal();
                    return false;
                }
            }
        });

        $(document).on('click', '#price-confirm', function() {
            $('#priceAlertModal').modal('hide');
            $('#add-space-details').submit();
        });
    });
});

function priceBox() {
    if ($('.check-days').is(':checked')) {
        if ($('#start-date').val() != '') {
            if ($('#end-date').val() != '') {
                if ($('.24-hours-open').is(':checked') || ($('#start-time').val() != '' && $('#end-time').val() != '')) {
                    $('.price').addClass('show');
                    $('.price').removeClass('hide');
                    showPriceOnDaysBasis();
                    return true;
                }
            }
        }
    }
    $('.price').removeClass('show');
    $('.price').addClass('hide');
    return true;
}
 (function() {
        function DateDiff(date1, date2) {
            this.days = null;
            this.hours = null;
            this.minutes = null;
            this.seconds = null;
            this.date1 = date1;
            this.date2 = date2;
            this.init();
        }
        DateDiff.prototype.init = function() {
            var data = new DateMeasure(this.date1 - this.date2);
            this.days = data.days;
            this.hours = data.hours;
            this.minutes = data.minutes;
            this.seconds = data.seconds;
        };
        function DateMeasure(ms) {
            var d, h, m, s;
            s = Math.floor(ms / 1000);
            m = Math.floor(s / 60);
            s = s % 60;
            h = Math.floor(m / 60);
            m = m % 60;
            d = Math.floor(h / 24);
            h = h % 24;
            this.days = d;
            this.hours = h;
            this.minutes = m;
            this.seconds = s;
        };
        Date.diff = function(date1, date2) {
            return new DateDiff(date1, date2);
        };
        Date.prototype.diff = function(date2) {
            return new DateDiff(this, date2);
        };
    })();
function showPriceOnDaysBasis() {
    var start = new Date($('#start-date').val());
    var end = new Date($('#end-date').val()); 
    var obj = Date.diff(end,start);
    if ($('.check-days:checked').length < 7 && $('#property_type_id').val() != 2) {
        $('.price').find('#yearly, #monthly, #weekely').parent().removeClass('show');
        $('.price').find('#yearly, #monthly, #weekely').parent().addClass('hide');
        $('.price').find('#yearly, #monthly, #weekely').val('');
        $('.price-basis').html('hourly and daily');

    } else if ($('.check-days:checked').length == 7 && $('#property_type_id').val() != 2) {
       //console.log(obj.days);
        if (obj.days >= 365 || obj.days >= 366) {
            $('.price').find('#weekely, #monthly, #yearly').parent().removeClass('hide');
            $('#yearly').addClass('show');
            $('.price-basis').html('hourly, daily, weekly, monthly and yearly');
           
        } else {

            $('.price').find('#weekely, #monthly').parent().removeClass('hide');
            $('#yearly').addClass('hide');
            $('.price-basis').html('hourly, daily, weekly, monthly');
        }     
    }
    $('.price-single').html('hourly');
    $('.price-single-cap').html('Hourly');
    if ($('#property_type_id').val() == 2) {
        $('.price').find('#monthly').removeClass('hide');
        /*if ($('.check-days:checked').length < 7) {
            $('.price').removeClass('show');
            $('.price').addClass('hide');
            $('.price').find('input').val('');
            
        } else if ($('.check-days:checked').length == 7) {
            $('.price').removeClass('hide');
        }*/
        calculateYears();
    }
}

function calculateYears() {
    var start = new Date($('#start-date').val());
    var end = new Date($('#end-date').val());
    
    var startYear = start.getFullYear();
    var endYear = end.getFullYear();
    var diffYear = endYear - startYear;

    /*var usrYear, usrMonth = start.getMonth()+1;
    var curYear, curMonth = end.getMonth()+1;
    if((usrYear=start.getFullYear()) < (curYear=end.getFullYear())){
        curMonth += (curYear - usrYear) * 12;
    }
    var diffMonths = curMonth - usrMonth;
    if(start.getDate() > end.getDate()) diffMonths--;*/
    


    if (diffYear > 0) {
        $('.price').find('#yearly').parent().removeClass('hide');
        $('#yearly').removeClass('hide');
        $('.price-basis').html('monthly and yearly');
        $('.price-single').html('monthly');
        $('.price-single-cap').html('Monthly');
    } /*else if (diffMonths > 0) {
        $('.price').find('#monthly').parent().removeClass('hide');
    }*/ else {
        $('.price').find('#yearly').parent().addClass('hide');
        $('.price').find('#yearly').val('');
        $('.price').removeClass('show');
        $('.price-basis').html('monthly');
        $('.price-single').html('monthly');
        $('.price-single-cap').html('Monthly');
        //$('.price').addClass('hide');
    }
    return true;
};

