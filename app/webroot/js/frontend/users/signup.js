$(function () {
	var signupValidate = function(ele){
		$(ele).validate({
			rules: {
				"data[User][first_name]": {
					required: true,
                },
                "data[User][last_name]": {
					required: true,
                },
                "data[User][username]": {
					required: true,
                },
                "data[User][email]": {
					required: true,
					email: true,
                },
                "data[User][password]": {
					required: true,
					minlength: 6,
                },
                "data[User][confirm_password]": {
					required: true,
					minlength: 6,
					equalTo : "#password",
                },
                "data[User][mobile]": {
					required: true,
					mobileNumber: true
                },
			},

			messages: {
            "data[User][first_name]": "Please enter your first name",
            "data[User][last_name]": "Please enter your last name",
            "data[User][username]": "Please enter your username",
            "data[User][email]": "Please enter valid email address",
            "data[User][password]": {
	                minlength: "Your password must be at least 6 characters long"
	            },
            "data[User][confirm_password]": {
	                minlength: "Your password must be at least 6 characters long",

	            },
        	},
		});

		jQuery.validator.addMethod("mobileNumber", function() {
		   jQuery("#moblie").mask("~99 999 999 9999");
		   return true;
		}, "Please specify the correct phone number");

	};

	// Add
 	$(document).ready(function(){
		signupValidate('#user-signup');
	});

});