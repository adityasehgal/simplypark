(function () {

    var formGroup = ".form-group",
        hasSuccess = "has-success",
        hasError = "has-error",
        includeClass = "valid",
        updateProfileFormId = '#update-profile';

    /* ****************************************************************
      It is to validate update profile form
    ******************************************************************/
    var validateUpdateForm = function (formId){
        $(formId).validate({
            rules: {
                'data[UserProfile][first_name]': {
                    required: true,
                    letterswithbasicpunc: true
                },
                'data[UserProfile][last_name]': {  
                    required: true,
                    letterswithbasicpunc: true
                },
                'data[UserProfile][profile_pic]': {  
                    required: false
                },
                'data[UserProfile][mobile]': {  
                    required: true,
                    minlength:10,
                    maxlength:10,
                    number: true
                },
               
                'data[UserProfile][company]': {  
                    required: false
                },
                'data[UserProfile][tan_number]': {  
                    number: true
                },
                'data[UserAddress][1][flat_apartment_number]': {  
                    required: true
                },
                'data[UserAddress][1][address]': {  
                    required: true
                },
                'data[UserAddress][1][state_id]': {  
                    required: true
                },
                'data[UserAddress][1][city_id]': {  
                    required: true
                },
                'data[UserAddress][1][post_code]': {  
                    required: true
                },
                'data[UserAddress][2][flat_apartment_number]': {  
                    required: '#home-address:unchecked'
                },
                'data[UserAddress][2][address]': {  
                    required: '#home-address:unchecked'
                },
                'data[UserAddress][2][state_id]': {  
                    required: '#home-address:unchecked'
                },
                'data[UserAddress][2][city_id]': {  
                    required: '#home-address:unchecked'
                },
                'data[UserAddress][2][post_code]': {  
                    required: '#home-address:unchecked'
                },
                'data[UserCar][0][registeration_number]': {
                    required: false
                }
            },
            highlight : function(element){
                highlightFunc(element);
            },
            success : function(element) {
                successFunc(element);
            }
        });

    };

    var highlightFunc = function(ele){
        $(ele).closest(formGroup).removeClass(hasSuccess).addClass(hasError);
    };

    var successFunc = function(ele){
        $(ele).addClass(includeClass).closest(formGroup).removeClass(hasError).addClass(hasSuccess);
    };

    var verifyContact = function(code,mobile) { 
        var url = base_url+'users/verifyContact/'+code+'/'+mobile;
        $.getJSON(url, function(data) {
            $('.verification-message').find('.text-info').html(data.message);
            if (data.code == 200) { 
                location.reload(true);
            } 
            
        });    
    };
    var sendOtp = function(mobile) { 
        var url = base_url+'users/sendOtpOnPhone/'+mobile;
        $.getJSON(url, function(data) {
            $('.response').find('.text-info').html(data.message);
            if (data.code == 100 || data.code == 101 || data.code == 102) { 
                $('#verifyOTP').removeClass('hide').addClass('show');    
            } 
        });    
    };

    $(document).ready(function(){
        
        var max_fields      = 10; //maximum input boxes allowed
        var wrapper         = $(".input-wrapper"); //Fields wrapper
        var add_button      = ".plus-btn"; //Add button ID
        var minus_button    = ".minus-btn"; //Add button ID
        var x = 1; //initlal text box count
        var car_number_form_index = 0;
        
        $('#home-address').click(function() {
            if ($(this).attr('checked') == 'checked') {
                $('#show-home-address').removeClass('hide');
                $('#show-home-address').addClass('show');
                $('#show-home-address').prepend('<input type="hidden" name="data[UserAddress][2][type]" class="home-address-type" value="2" />');
                $(this).attr('checked',false); 
            } else {                
                $('#show-home-address').removeClass('show');
                $('#show-home-address').addClass('hide');
                $(this).attr('checked',true);
                $('.home-address-type').remove();
            }
        });
        
        $('.verify').on('click',function() { 
            var code = $('.code').val();
            var mobile = $('.phone').val();
            verifyContact(code,mobile);
        });
        $('.sendOtp').on('click',function() { 
           
            var mobile = $('.mobile').val();
            $('.code').val('');
            $('.phone').val(mobile);
            sendOtp(mobile);
        });

        $(document).on("click",add_button,(function(e){ //on add input button click

            if (wrapper.siblings('.form-group').length > 0) {
                var id_attr = wrapper.siblings(":last").attr('id');
                if (typeof id_attr !== typeof undefined && id_attr !== false) {
                    car_index = parseInt(wrapper.siblings(":last").attr('id'));
                    car_number_form_index = car_index+1;                    
                } else {
                    car_number_form_index++;
                }
            } else {
                car_number_form_index++;
            }

            //e.preventDefault();
            wrapper.parent().append('<div class="form-group">'+
                    '<div class="col-sm-4 col-sm-offset-3">'+
                        '<input name="data[UserCar]['+car_number_form_index+'][registeration_number]" id="addcar" class="form-control" placeholder="Car Number Plate" type="text">'+
                    '</div>'+
                    '<div class="col-sm-4">'+
                        '<select name="data[UserCar]['+car_number_form_index+'][car_type]" class="form-control">'+
                            '<option value="1">Hatchback</option>'+
                            '<option value="2">SUV</option>'+
                            '<option value="3">Minivans/MPV</option>'+
                        '</select>'+
                    '</div>'+
                    '<div class="col-sm-1">'+
                        '<a href="#" class="minus-btn text-danger">'+
                            '<span class="fa fa-minus-circle fa-2x"></span>'+
                        '</a>'+
                    '</div>'+
                '</div>'
            ); //add input box
        }));
        
        $(document).on("click",minus_button, function(e){ //user click on remove text
            e.preventDefault();

            $(this).parents('.form-group').remove();
        })

        validateUpdateForm(updateProfileFormId);
        
        
    });
})();
