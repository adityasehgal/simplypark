$(function () {
    var forgotPasswordValidate = function(ele){
		$(ele).validate({
			rules: {
				"data[User][email]": {
                    required: true
                }
			}
		});
	};
	$(document).ready(function(){
		forgotPasswordValidate('#forgot-password');
	});

});
