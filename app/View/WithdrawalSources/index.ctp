<?php
    echo $this->Html->script('frontend/withdrawal-sources', array('inline' => false));
?>
<section class="col-xs-12 col-md-9 right-panel">
    
    <div class="heading clearfix col-xs-12">
        <span class="pull-left title">Withdrawal Sources</span>
        <?php
            echo $this->Html->link('Add Withdrawal Sources', 'addWithdrawalSource', array(
            'class' => 'pull-right btn btn-info text-uppercase pull-right'));
        ?>
    </div>

    <div class="col-xs-12 parking-space">
		<?php if(isset($data['no_data_exists'])) { ?>
			<div class="alert alert-info text-center" role="alert">
				<?php echo $data['no_data_exists']; ?>
				<?php unset($data['no_data_exists']); ?>
		   </div>
		<?php } ?>

		<?php if(isset($data['data_msg'])) { ?>
			<div class="alert alert-warning text-center" role="alert">
            	<?php echo $data['data_msg']; ?>        
            </div>
		<?php unset($data['data_msg']); } ?>

		<?php if(isset($data['data'])) { ?>
			<?php foreach ($data['data'] as $data) { ?>
				<div class="well white-bg clearfix">
					<div class="row">
						<div class="col-md-12">
							<div class="radio">
								<h4 class="text-info">
									  <label>
									  	<?php 
									  		$checked = '';
									  		if($data['WithdrawalSources']['is_activated'] == 1) {
									  			$checked = 'checked';
									  		}
									  	?>
									    <input type="radio" name="data[WithdrawalSources][is_activated]" 
									    	data-url = "<?php echo Configure::read('ROOTURL').'WithdrawalSources/activateWithdrawalSources' ?>"
									     	value="<?php echo $data['WithdrawalSources']['id']; ?>" <?php echo $checked; ?> >

									    <?php echo $data['WithdrawalSources']['account_first_name'].' '.$data['WithdrawalSources']['account_middle_name'].' '.$data['WithdrawalSources']['account_last_name']; ?>
									  </label>
								</h4>
							</div>
							<div class="col-sm-12">
								<p>
									<span>
										<small>
											<b>Address :</b> <?php echo $data['WithdrawalSources']['address']. ' ' .$data['City']['name']. ' '.$data['State']['name'].' '.$data['WithdrawalSources']['postal_code']; ?>
										</small>
									</span>
								</p>
								<ul class="list-inline account-detail">
									<li>
										<b>IFC Code :</b> <?php echo $data['WithdrawalSources']['ifsc_code']; ?>
									</li>
									<li>
										<b>Bank Accont :</b> <?php echo $data['WithdrawalSources']['account_number']; ?>
									</li>
								</ul>
								<ul class="list-inline action">
									<li>
				                    	<?php
								            echo $this->Html->link('Edit',
								            	Configure::read('ROOTURL').'WithdrawalSources/addWithdrawalSource/'.base64_encode($data['WithdrawalSources']['id']), 
								            	array(
								            	'class' => 'text-primary'));
								        ?>
				                    </li>
				                    <li>
				                    	<?php
								            echo $this->Html->link('Delete',
								            	'#',
								            	array(
								            		'data-toggle' => 'modal',
			                                        'data-target' => '#deleteConfirmModal',
			                                        'escape' => false,
			                                        'data-url' => Configure::read('ROOTURL').'WithdrawalSources/deleteAccount/'.base64_encode($data['WithdrawalSources']['id']),
								            		'class' => 'text-danger delete-popup',
								            	)
								            );
								        ?>
				                    </li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		<?php } ?>
	</div>
</section>
