<?php
	echo $this->Html->script('frontend/withdrawal-sources', array('inline' => false));
?>
<section class="col-xs-12 col-md-9 right-panel">
	
	<div class="heading clearfix col-xs-12 border-bottom">
	    <span class="pull-left title">Add Withdrawal Sources</span>
	</div>

	<?php
		echo $this->Form->create(
				'WithdrawalSources',
				array(
                    'url' => 'addWithdrawalSource',
					'method' => 'post',
					'class' => 'form-horizontal form-divide-group',
					'id' => 'add-withdrawal-source',
					'enctype' => 'multipart/form-data',
					'novalidate' => false
				)
			);

		if(isset($data['WithdrawalSources'])) {
            echo $this->Form->input('id', array(
                                'div' => false,
                                'label' => false,
                                'type' => 'hidden',
                                'value' => $data['WithdrawalSources']['id']
                                )
                            );
        }
	?>
		<div class="col-xs-12 body">
			<div class="form-group">
				<label for="accountHolderName" class="col-sm-3 control-label">Account Holder Name<span class='error'> *</span></label>
				<div class="col-sm-3">
					<?php
                		echo $this->Form->input('account_first_name', array(
                			'div' => false,
                			'label' => false,
                			'class' => 'form-control',
                			'placeholder' => 'First Name',
                			'value' => (isset($data['WithdrawalSources'])) ? $data['WithdrawalSources']['account_first_name'] : ''
                			)
                		);
                	?>
            	</div>
            	<div class="col-sm-3">
					<?php
                		echo $this->Form->input('account_middle_name', array(
                			'div' => false,
                			'label' => false,
                			'class' => 'form-control',
                			'placeholder' => 'Middle Name',
                			'value' => (isset($data['WithdrawalSources'])) ? $data['WithdrawalSources']['account_middle_name'] : ''
                			)
                		);
                	?>
            	</div>
            	<div class="col-sm-3">
					<?php
                		echo $this->Form->input('account_last_name', array(
                			'div' => false,
                			'label' => false,
                			'class' => 'form-control',
                			'placeholder' => 'Last Name',
                			'value' => (isset($data['WithdrawalSources'])) ? $data['WithdrawalSources']['account_last_name'] : ''
                			)
                		);
                	?>
            	</div>
			</div>

			<div class="form-group">
				<label for="accountNumber" class="col-sm-3 control-label">Account Number<span class='error'> *</span></label>
				<div class="col-sm-6">
					<?php
                		echo $this->Form->input('account_number', array(
                			'div' => false,
                			'label' => false,
                			'class' => 'form-control',
                			'placeholder' => 'Account Number',
                			'value' => (isset($data['WithdrawalSources'])) ? $data['WithdrawalSources']['account_number'] : ''
                			)
                		);
                	?>
				</div>
			</div>

			<!-- <div class="form-group">
				<label for="accountBranchCode" class="col-sm-3 control-label">Account Branch Code<span class='error'> *</span></label>
				<div class="col-sm-6">
					<?php
                		echo $this->Form->input('branch_code', array(
                			'div' => false,
                			'label' => false,
                			'class' => 'form-control',
                			'placeholder' => 'Account Branch Code',
                			'value' => (isset($data['WithdrawalSources'])) ? $data['WithdrawalSources']['branch_code'] : ''
                			)
                		);
                	?>
				</div>
			</div> -->

			<div id="ifscCode" class="form-group ">
				<label for="accountIfscCode" class="col-sm-3 control-label">Account IFSC Code<span class='error'> *</span></label>
				<div class="col-sm-6">
					<?php
                		echo $this->Form->input('ifsc_code', array(
                			'div' => false,
                			'label' => false,
                			'id' => 'ifsc-code',
                			'class' => 'form-control',
                			'placeholder' => 'Account IFSC Code',
                			'value' => (isset($data['WithdrawalSources'])) ? $data['WithdrawalSources']['ifsc_code'] : ''
                			)
                		);
                	?>
				</div>
			</div>

			<div class="form-group">
				<label for="bankName" class="col-sm-3 control-label">Bank Name<span class='error'> *</span></label>
				<div class="col-sm-6">
					<?php
                		echo $this->Form->input('bank_name', array(
                			'div' => false,
                			'label' => false,
                			'id' => 'bank-name',
                			'class' => 'form-control',
                			'placeholder' => 'Name',
                			'value' => (isset($data['WithdrawalSources'])) ? $data['WithdrawalSources']['bank_name'] : ''
                			)
                		);
                	?>
				</div>
			</div> 
			<div class="form-group">
				<label for="bankAddress" class="col-sm-3 control-label">Bank Address<span class='error'> *</span></label>
				<div class="col-sm-9">
					<?php
                		echo $this->Form->input('address', array(
                			'div' => false,
                			'label' => false,
                			'class' => 'form-control',
                			'placeholder' => 'Bank Address',
                			'value' => (isset($data['WithdrawalSources'])) ? $data['WithdrawalSources']['address'] : ''
                			)
                		);
                	?>
				</div>
			</div>
	<!-- 		
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-9">
					<div class="row">
						<div class="col-sm-6">
							<?php
		                		echo $this->Form->input('state_id', array(
		                			'div' => false,
		                			'label' => false,
		                			'options' => $data['state'],
		                			'id' => 'state',
		                			'class' => 'form-control selectpicker',
		                			'empty' => 'State',
		                			'data-url' => Configure::read('ROOTURL').'users/getCities',
		                			'selected' => (isset($data['WithdrawalSources'])) ? $data['WithdrawalSources']['state_id'] : ''
		                			)
		                		);
		                	?>
						</div>
						<div class="col-sm-6" id= "list-city">
							<?php
	                            if(isset($data['WithdrawalSources'])) {
	                            	if(isset($data['city_list'])) {
	                                    echo $this->Form->input('city_id', array(
	                                        'div' => false,
	                                        'label' => false,
	                                        'options' => $data['city_list'],
	                                        'id' => 'city',
	                                        'class' => 'form-control',
	                                        'empty' => 'City',
	                                        'selected' => $data['WithdrawalSources']['city_id']
	                                        )
	                                    );
                                	}
	                            }
	                        ?>
						</div>
					</div>
				</div>
			</div> -->
		</div>
		<div class="footer clearfix col-xs-12 border-top">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10 text-right">
					<?php
                        echo $this->Html->link('Cancel', 'index', array(
                        	'class' => 'btn btn-default'));
                    ?>
                    <?php
                        echo $this->Form->button(__('Submit'), array(
                                    'class' => 'btn btn-info',
                                    'type' => 'submit'
                                    ));
                    ?>
				</div>
			</div>
		</div>
	<?php echo $this->Form->end(); ?>

</section>

<script type="text/html" id="addCity">
	<select class='form-control' id = "city" name =data[WithdrawalSources][city_id]>
	    <option value="">City</option>
	    <% _.each(data, function(value, key){ %>
	        <option value="<%= key %>"><%= value %></option>
	    <% }); %>
    </select>
</script>
