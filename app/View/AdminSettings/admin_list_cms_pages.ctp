﻿<?php echo $this->Html->script('backend/commonmagnificpopup',array('inline' => false)); ?>
<div class="crumbs">
    <ul id="breadcrumbs" class="breadcrumb">
        <li class="active">
            <i class="icon-wrench"></i>
            <?php
                echo $this->Html->link(
                        __('Static CMS Pages'),
                        '/admin/admin_settings/listCmsPages',
                        array(
                                'class' => 'active'
                            )
                    ); 
            ?>
        </li>
    </ul>
</div>
<div class="panel1" id="inline-popups">
    <span class="clearfix"></span>
    <div class="panel panel-default">
        <table class="table table-bordered table-striped table-bordered table-hover new-addministartor-table">
            <thead>
                <tr>
                    <th><?php echo __('Meta Title'); ?></th>
                    <th><?php echo __('Meta Keywords'); ?></th>
                    <th><?php echo __('Page Name'); ?></th>
                    <th><?php echo __('Status'); ?></th>
                    <th><?php echo __('Actions'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(!empty($getAllCmsPages)) { 
                        foreach ($getAllCmsPages as $showAllCmsPages) {
                ?>
                            <tr>
                                <td>
                                    <?php echo $showAllCmsPages['CmsPage']['meta_title']; ?>
                                </td>
                                <td>
                                    <?php echo $showAllCmsPages['CmsPage']['meta_keyword']; ?>
                                </td>
                                <td>
                                    <?php echo $showAllCmsPages['CmsPage']['page_name']; ?>
                                </td>
                                <td>
                                    <?php if ($showAllCmsPages['CmsPage']['is_activated'] == 0) { ?>
                                        <a class="btn btn-danger btn-xs btn-grad" href="#">Inactive</a>
                                    <?php } else { ?>
                                        <a class="btn btn-success btn-xs btn-grad" href="#">Active</a>
                                    <?php } ?>
                                </td>
                                <td id="inline-popups">
                                    <?php 
                                        $activeInactiveAttr = $this->Admin->activeInactiveIconValues($showAllCmsPages['CmsPage']['is_activated']);
                                        echo $this->Html->link(
                                            '<i class="glyphicon glyphicon-'.$activeInactiveAttr['Class'].'"></i>',
                                            $activeInactiveAttr['PopupID'],
                                            array(
                                                'class' => 'btn btn-ok activeinactivemodalbutton magniPopup',
                                                'title' => $activeInactiveAttr['Title'],
                                                'data-effect' => 'mfp-zoom-in',
                                                'escape' => false,
                                                'data-url' => Configure::read('ROOTURL').'admin/admins/changeRecordStatus/'.base64_encode($showAllCmsPages['CmsPage']['id']).'/CmsPage/Cms Page'
                                            )
                                        );
                                        echo $this->Html->link(
                                                '<i class="glyphicon glyphicon-pencil"></i>',
                                                Configure::read('ROOTURL').'admin/admin_Settings/editCms/'.base64_encode($showAllCmsPages['CmsPage']['id']),
                                                array(
                                                        'class' => 'btn btn-green',
                                                        'title' => 'Edit',
                                                        'escape' => false
                                                    )
                                            );
                                    ?>
                                </td>
                            </tr>
                <?php } } else { ?>
                    <tr>
                        <td colspan="5">
                            No Record Found
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<?php
    echo $this->element('backend/active_popup');
    echo $this->element('backend/inactive_popup');
?>