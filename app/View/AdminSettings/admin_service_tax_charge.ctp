<?php
    echo $this->Html->script('backend/AdminSettings/service_tax_charge', array('inline' => false));
?>
<div class="crumbs">
    <ul id="breadcrumbs" class="breadcrumb">
        <li class="active">
            <i class="icon-wrench"></i>
            <?php
                echo $this->Html->link(
                        __('Service Tax/Commission'),
                        '/admin/AdminSettings/serviceTaxCharge',
                        array(
                                'class' => 'active'
                            )
                    ); 
            ?>
        </li>
    </ul>
</div>
<div class="col-xs-12">
    <?php
        echo $this->Form->create('ServiceTaxCharges', array(
                        'url' => '/admin/AdminSettings/serviceTaxCharge',
                        'method' => 'post',
                        'class' => 'form-horizontal charges-form',
                        'id' => 'add-service-rax-charges',
                        'novalidate' => false
                        ));

        if(isset($data['service_tax_charges_data'])) {
            echo $this->Form->input('id', array(
                            'div' => false,
                            'label' => false,
                            'type' => 'hidden',
                            'value' => $data['service_tax_charges_data']['id']
                            )
                        );
        }
    ?>
        <div class="well">
            <div class="col-sm-6">
                <div class="checkbox">
                    <label>
                        <label class=""><?php echo __('SimplyPark Commision'); ?></label>
                    </label>
                </div>

                <div class="form-group">
                    <label class="col-xs-6 control-label"><?php echo __('Set Commission (in %)'); ?></label>
                    <div class="col-xs-3">
                        <?php 
                            echo $this->Form->input('commission_percentage', array(
                                            'div' => false,
                                            'label' => false,
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'value' => (isset($data['service_tax_charges_data'])) ? $data['service_tax_charges_data']['commission_percentage'] : ''
                                            )
                                        );
                        ?>
                    </div>                
                    <div class="col-xs-1 control-label left-row-none text-left">%</div>
                </div>
                <div class="form-group">
                    <label class="col-xs-6 control-label"><?php echo __('Commission Paid by:'); ?></label>
                    <div class="col-xs-6 control-checkbox">
                        <?php 
                            $options = array('1' => 'Driver', '2' => 'Owner');
                            $attributes = array('legend' => false,
                                                'value' => $data['service_tax_charges_data']['commission_paid_by'],'separator' => '  ',
                                                'class' => 'service-popup',
                                                );
                            echo $this->Form->radio('commission_paid_by', $options, $attributes);
                        ?>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="checkbox">
                    <label>
                        <label class=""><?php echo __('Service Tax'); ?></label>
                    </label>
                </div>

                <div class="form-group">
                    <label class="col-xs-6 control-label"><?php echo __('Set Service Tax (in %)'); ?></label>
                    <div class="col-xs-3">
                        <?php 
                            echo $this->Form->input('service_tax_percentage', array(
                                            'div' => false,
                                            'label' => false,
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'value' => (isset($data['service_tax_charges_data'])) ? $data['service_tax_charges_data']['service_tax_percentage'] : ''
                                            )
                                        );
                        ?>
                    </div>
                    <div class="col-xs-1 control-label left-row-none">%</div>
                </div>
                <div class="form-group">
                    <label class="col-xs-6 control-label"><?php echo __('Service Tax payable by SimplyPark:'); ?></label>
                    <div class="col-xs-3  control-checkbox">
                        <?php $checked = $data['service_tax_charges_data']['service_tax_paid_by_simplypark'];
                            echo $this->Form->checkbox('service_tax_paid_by_simplypark', array('checked' => $checked));
                        ?>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <?php
                        echo $this->Form->button(__('Save'), array(
                                    'class' => 'btn btn-primary',
                                    'type' => 'submit'
                                    ));
                    ?>
            </div>
        </div>
    <?php echo $this->Form->end(); ?>
<?php echo $this->element('backend/AdminSettings/servicePopup'); ?>
</div>