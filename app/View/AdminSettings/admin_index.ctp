<div class="crumbs">
    <ul id="breadcrumbs" class="breadcrumb">
        <li class="active">
            <i class="icon-wrench"></i>
            <?php
                echo $this->Html->link(
                        __('Spaces'),
                        '/admin/AdminSettings/',
                        array(
                                'class' => 'active'
                            )
                    ); 
            ?>
        </li>
        <div class="pull-right">
            <?php
                echo $this->Html->link('<i class="icon-plus icon-spacing"></i>'.__('Add New Space Type'),
                        '#',
                        array(
                            'class' => 'btn btn-primary btn-grad btn-bottom',
                            'data-toggle' => 'modal',
                            'data-target' => '#addSpace',
                            'escape' => false
                        ));
            ?>
        </div>
    </ul>
</div>
<div class="panel1">
    <span class="clearfix"></span>
    <div class="panel panel-default" id="inline-popups">
        <div class="panel-heading clearfix">
            <div class="right-inner-addon pull-right row">
                <?php
                    echo $this->Form->create('Space', array(
                                    'url' => '/admin/AdminSettings/',
                                    'type' => 'get',
                                    'novalidate' => true
                                    ));
                ?>
                        <div class="input-group col-xs-3 pull-right">
                            <?php
                                echo $this->Form->input(
                                    'search',
                                    array(
                                        'div' => false,
                                        'label' => false,
                                        'class' => 'form-control',
                                        'placeholder' => 'search',
                                        'escape' => false,
                                        'default' => (isset($this->request->query['search']) && !empty($this->request->query['search'])) ? $this->request->query['search'] : ''
                                        )
                                );
                            ?>
                            <span class="input-group-btn">
                                <?php
                                    echo $this->Form->button(
                                        '<i class="icon-search"></i>',
                                        array(
                                            'type' => 'submit',
                                            'class'=> 'btn btn-default',
                                            'escape' => false
                                            )
                                    );
                                ?>
                            </span>
                        </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
        <div class="table-responsive" id="innercontent">
            <?php echo $this->element('backend/AdminSettings/listing'); ?>
        </div>
    </div>
    <?php
        echo $this->Html->script(array(
            'backend/AdminSettings/add_space',
            'backend/commonmagnificpopup',
            )
        );
    ?>
</div>
<?php
	echo $this->element('backend/AdminSettings/add_space');
    echo $this->element('backend/AdminSettings/edit_space');
    echo $this->element('backend/active_popup');
    echo $this->element('backend/inactive_popup');
?>
