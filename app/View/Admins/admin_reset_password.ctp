<?php echo $this->Html->script('reset_password', array('inline' => false)); ?>
<div class="text-center">
   <?php echo $this->Html->image('simplyparklogo.png'); ?>
</div>
<div class="tab-content">
   <div id="login" class="tab-pane active">
   	<div>
			<h3 class="text-center"><?php echo __('Reset Password'); ?></h3>
		</div>
      <?php  echo $this->Form->create('User', array(
              'type' => 'post',
              'url' => '/admin/admins/resetPassword',
              'class' => 'form-signin',
              'id' => 'reset-password-validate',
              'novalidate' => true
            ));
            echo $this->Form->hidden('id', array('value' => $userID));
      ?>
         <div class="form-group">
            <?php
                if ($this->Session->check('Message.flash')) {
                  echo $this->element('flash_msg');
                }
            ?>
         </div>
         <div class="form-group">
            <label><?php echo __('New Password'); ?></label>
            <?php echo $this->Form->input('password', array(
                         'placeholder' => 'New Password',
                         'class' => 'form-control',
                         'div' => false,
                         'label' => false,
                         'id' => 'new-password'
                       )
                     );
            ?>
         </div>
         <div class="form-group">
           <label><?php echo __('Confirm Password'); ?></label>
           <?php echo $this->Form->input('confirm_password', array(
           						'type' => 'password',
                           'placeholder' => 'Confirm Password',
                           'class' => 'form-control',
                           'label' => false,
                           'div' => false
                        )
                     );
            ?>
         </div>
         <div class="clearfix">
           <div class="pull-right">
               <button class="btn btn-primary" type="submit"><?php echo __('Save'); ?></button>
           </div>
         </div>
      <?php echo $this->Form->end(); ?>
   </div>
</div>