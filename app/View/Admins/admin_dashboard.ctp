<div class="crumbs">
    <ul id="breadcrumbs" class="breadcrumb">
        <li class="active">
            <i class="icon-home"></i>
            <?php
                echo $this->Html->link(
                        __('Dashboard'),
                        '/admin/admins/dashboard',
                        array(
                                'class' => 'active'
                            )
                    ); 
            ?>
        </li>
    </ul>
</div>
<div class="panel1">
    <span class="clearfix"></span>
    <div class="summary-box clearfix text-center">
            <div class="col-xs-3">
                <?php echo $this->Html->link(
                                    '<div class="red-bg"><i class="icon-user"></i>
                                        <span>'.$activeUsers.'</span>
                                        <div>No. of Users</div>
                                    </div>',
                                    array(
                                            'controller' => 'admins',
                                            'action' => 'userListing',
                                            'admin' => true
                                    ),array(
                                            'escape' => false,
                                            'class' => 'remove-underline',
                                            'title' => 'New User Added'
                                    ));
                ?>
                
            </div>

            <div class="col-xs-3">
                <div class="view-all">
                    <?php echo $this->Html->link(
                                    ' <div class="green-bg">
                                         <i class="icon-map-marker"></i>
                                         <span>'.$parkingSpaces.'</span>
                                         <div>Active Parking Spaces</div>
                                     </div>',
                                    array(
                                            'controller' => 'spaces',
                                            'admin' => true
                                    ),array(
                                            'escape' => false,
                                            'class' => 'remove-underline',
                                            'title' => 'Active Parking Spaces'
                                    ));
                    ?>
                </div>
            </div>
            
            <div class="col-xs-3">
                <div class="yellow-bg">
                    <i class="icon-shopping-cart"></i>
                    <span><?php echo $bookings; ?><!--<span class="percentage"></span>--></span>
                    <div>Bookings done</div>
                </div>
                
            </div>
            <div class="col-xs-3">
                <div class="purple-bg">
                    <i class="indian-rupee">&#8377;</i>
                    <span><?php echo number_format($amount,2); ?></span>
                    <div><?php echo __('Simply Park Income'); ?></div>
                </div>
                <!-- <div class="view-all">
                    <a href="#"></a>
                </div> -->
            </div>
            <div class="col-xs-3">
                <div class="blue-bg">
                    <i class="indian-rupee">&#8377;</i>
                    <span><?php echo number_format($serviceTaxTotalAmount,2); ?></span>
                    <div><?php echo __('Service Tax'); ?></div>
                </div>
                <!-- <div class="view-all">
                    <a href="#"></a>
                </div> -->
            </div>
        </div>
</div>
