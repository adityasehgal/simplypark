<?php echo $this->Html->script('forgot_password', array('inline' => false)); ?>
<div class="text-center">
   <?php echo $this->Html->image('simplyparklogo.png'); ?>
</div>
<div class="tab-content">
   <div id="login" class="tab-pane active">
      <?php  echo $this->Form->create('User', array(
              'method' => 'post',
              'class' => 'form-signin',
              'id' => 'login-validate',
              'novalidate' => true
            ));
      ?>
         <div class="form-group">
            <?php
                if ($this->Session->check('Message.flash')) {
                  echo $this->element('flash_msg');
                }
            ?>
         </div>
         <div class="form-group">
            <label><?php echo __('Username') ?></label>
            <?php echo $this->Form->input('username', array(
                         'placeholder' => 'username or email',
                         'class' => 'form-control',
                         'div' => false,
                         'label' => false
                       )
                     );
            ?>
         </div>
         <div class="form-group">
           <label><?php echo __('Password') ?></label>
           <?php echo $this->Form->input('password', array(
                           'placeholder' => 'password',
                           'class' => 'form-control',
                           'label' => false,
                           'div' => false
                        )
                     );
            ?>
         </div>
         <div class="clearfix">
           <div class="pull-left">
              <div class="checkbox remember-me">
                 <label>
                      <?php
                        echo $this->Form->checkbox('keep_me_logged_in', array(
                             'label' => false,
                             'div' => false
                            )
                          );
                         echo __('Keep me logged in');
                      ?>                        
                 </label>
               </div>
           </div>
           <div class="pull-right">
               <button class="btn btn-primary" type="submit"><?php echo __('Sign in') ?></button>
           </div>
         </div>
      <?php echo $this->Form->end(); ?>
   </div>
   <div id="forgot" class="tab-pane">
      <?php
        echo $this->Form->create('User',
          array(
            'url' => '/admin/admins/forgotPassword',
            'id' => 'forgot-password',
            'type' => 'post',
            'class' => 'form-signin',
            'novalidate' => true
            )
          );
      ?>
          <h4 class="text-muted text-center btn-rect"><?php echo __('Enter your valid e-mail or username') ?></h4>
          <div class="form-group">
          <?php
            echo $this->Form->input('username', array('div' => false, 'label' => false, 'placeholder' => 'Your E-mail or username', 'class' => 'form-control'));
          ?>
         </div>                 
         <?php
            echo $this->Form->button('Recover Password', array('div' => false, 'label' => false, 'type' => 'submit', 'placeholder' => 'Your E-mail', 'class' => 'btn text-muted text-center btn-success'));
          ?>
      <?php echo $this->Form->end(); ?>
   </div>
</div>
<div class="text-center footer">
   <ul class="list-inline">
      <li><a class="text-muted" href="#login" data-toggle="tab" id="adminlogin"><?php echo __('Login') ?></a></li>
      <li><a class="text-muted" href="#forgot" data-toggle="tab" id="forgorpassword"><?php echo __('Forgot Password?') ?></a></li>
   </ul>
</div>
