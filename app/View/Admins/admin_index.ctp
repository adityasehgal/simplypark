<div class="crumbs">
    <ul id="breadcrumbs" class="breadcrumb">
        <li class="active">
            <i class="icon-user"></i>
            <?php
                echo $this->Html->link(
                        __('Manage Administrators'),
                        '/admin/admins/',
                        array(
                                'class' => 'active'
                            )
                    ); 
            ?>
        </li>
        <?php if ($this->Session->read('Auth.User.user_type_id') == configure::read('UserTypes.Admin')) { ?>
            <div class="pull-right">
                <?php
                    echo $this->Html->link('<i class="icon-plus icon-spacing"></i>'.__('Add New Sub Admin'),
                            '#',
                            array(
                                'class' => 'btn btn-primary btn-grad btn-bottom',
                                'data-toggle' => 'modal',
                                'data-target' => '#addSubAdminModal',
                                'escape' => false
                            ));
                ?>
            </div>
        <?php } ?>
    </ul>
</div>
<div class="panel1">
    <span class="clearfix"></span>
    <div class="panel panel-default" id="inline-popups">
        <div class="panel-heading clearfix">
            <div class="right-inner-addon pull-right row">
                <?php
                    echo $this->Form->create('User', array(
                                    'url' => '/admin/admins/',
                                    'type' => 'get',
                                    'novalidate' => true
                                    ));
                ?>
                        <div class="input-group col-xs-3 pull-right">
                            <?php
                                echo $this->Form->input(
                                    'search',
                                    array(
                                        'div' => false,
                                        'label' => false,
                                        'class' => 'form-control',
                                        'placeholder' => 'search',
                                        'escape' => false,
                                        'default' => (isset($this->request->query['search']) && !empty($this->request->query['search'])) ? $this->request->query['search'] : ''
                                        )
                                );
                            ?>
                            <span class="input-group-btn">
                                <?php
                                    echo $this->Form->button(
                                        '<i class="icon-search"></i>',
                                        array(
                                            'type' => 'submit',
                                            'class'=> 'btn btn-default',
                                            'escape' => false
                                            )
                                    );
                                ?>
                            </span>
                        </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
        <div class="table-responsive" id="innercontent">
            <?php echo $this->element('backend/Admin/listing'); ?>
        </div>
    </div>
    <?php
        echo $this->Html->script(array(
            'backend/Admin/add_subadmin',
            'backend/Admin/edit_admin_profile',
            'backend/commonmagnificpopup',
            )
        );
    ?>
</div>
<?php
	echo $this->element('backend/Admin/add_subadmin');
    echo $this->element('backend/delete_popup');
    echo $this->element('backend/restore_popup');
    echo $this->element('backend/active_popup');
    echo $this->element('backend/inactive_popup');
?>
