<div class="crumbs">
    <ul id="breadcrumbs" class="breadcrumb">
        <li class="active">
            <i class="icon-envelope"></i>
            <?php
                echo $this->Html->link(
                        'Email Templates',
                        '/admin/email_templates/',
                        array(
                                'class' => 'active'
                            )
                    ); 
            ?>
        </li>
    </ul>
</div>
<div class="panel1">
    <span class="clearfix"></span>
    <div class="panel panel-default">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-bordered table-hover new-addministartor-table">
                <thead>
                    <tr>
                        <th>
                            <?php echo $this->Paginator->sort('EmailTemplate.template_used_for', 'Template Used For', array('class' => 'tableinherit')); ?>
                        </th>
                        <th>
                            <?php echo $this->Paginator->sort('EmailTemplate.subject', 'Subject', array('class' => 'tableinherit')); ?>
                        </th>
                        <th>
                            <?php echo $this->Paginator->sort('EmailTemplate.modified', 'Last Modified', array('class' => 'tableinherit')); ?>
                        </th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if(!empty($emailTemplates)) { 
                            foreach ($emailTemplates as $showEmailTemplates) {
                    ?>
                                <tr>
                                    <td>
                                        <?php echo $showEmailTemplates['EmailTemplate']['template_used_for']; ?>
                                    </td>
                                    <td>
                                        <?php echo $showEmailTemplates['EmailTemplate']['subject']; ?>
                                    </td>
                                    <td>
                                        <?php echo date('Y-m-d', strtotime($showEmailTemplates['EmailTemplate']['modified'])); ?>
                                    </td>
                                    <td>
                                        <?php
                                            echo $this->Html->link(
                                                    '<i class="glyphicon glyphicon-pencil"></i>',
                                                    Configure::read('ROOTURL').'admin/email_templates/editTemplate/'.base64_encode($showEmailTemplates['EmailTemplate']['id']),
                                                    array(
                                                            'class' => 'btn btn-green',
                                                            'title' => 'Edit',
                                                            'escape' => false
                                                        )
                                                );
                                        ?>
                                    </td>
                                </tr>
                    <?php } } else { ?>
                        <tr>
                            <td colspan="6">
                                No Record Found
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php echo $this->element('backend/pagination'); ?>
</div>