<div id="background">
    <h1>404 Page not found, sorry!</h1>
    <p>The page you were looking for appears to have been moved, deleted or does not<br> exist. You could go back to where you were or head straight to our <a href="<?php echo Configure::read('ROOTURL'); ?>">home page</a></p>
</div>