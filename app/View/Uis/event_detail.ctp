<div class="main-container">
	<div class="event-banner">
		<div class="container">
			<section class="clearfix">			
				<div class="col-sm-3 img-block">
					<?php 
						echo $this->Html->image('frontend/city.png',
							array(
								    'alt' => 'IPL Cricket',
								    'class' => 'img-responsive'
								)
						);
					?>
				</div>
				<div class="col-sm-9 ">
					<h2>IPL Cricket</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
				</div>		
			</section>
		</div>
	</div>
	<div class="container">
		<section class="clearfix">
			<div class="col-md-8">
				<div class="row heading">
					<div class="col-xs-12 col-sm-2 text-center">
						<h3>Events</h3>
					</div>
					<div class="col-xs-12 col-sm-8 col-sm-offset-2">
						<form class="form-horizontal row">
							<div class="col-sm-6">
								<div class="form-group">
									<label class="col-sm-4 control-label" for="exampleInputEmail1">City</label>
									<div class="col-sm-8">
										<?php
											echo $this->Form->input('field', array(
											    'options' => array(1, 2, 3, 4, 5),
											    'empty' => '(choose one)',
											    'class' => 'form-control',
											    'label'	=> false
											));
										?>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="col-sm-4 control-label" for="exampleInputEmail1">Month</label>
									<div class="col-sm-8">
										<?php
											echo $this->Form->input('field', array(
											    'options' => array(1, 2, 3, 4, 5),
											    'empty' => '(choose one)',
											    'class' => 'form-control',
											    'label'	=> false
											));
										?>
									</div>
								</div>
							</div>						
						</form>
					</div>
				</div>
				<div class="body">
					<div class="well gray-bg">
						
						<div class="row">
							<div class="col-xs-12 col-md-9">
								<div class="row">
									<div class="col-sm-4">
										<h3 class="place-name">
											10 June
										</h3>
									</div>
									<div class="col-sm-8 place-address">
										PCA Stadium - Mohali
									</div>
								</div>
								
							</div>

							<div class="col-xs-12 col-md-3 text-right">
								<?php
									echo $this->Form->button('Find Parking', 
										array(
												'class' => 'btn btn-info text-uppercase'
											)
									);
								?>
							</div>
						</div>

					</div>
					<div class="well gray-bg">
						
						<div class="row">
							<div class="col-xs-12 col-md-9">
								<div class="row">
									<div class="col-sm-4">
										<h3 class="place-name">
											10 June
										</h3>
									</div>
									<div class="col-sm-8 place-address">
										PCA Stadium - Mohali
									</div>
								</div>
								
							</div>

							<div class="col-xs-12 col-md-3 text-right">
								<?php
									echo $this->Form->button('Find Parking', 
										array(
												'class' => 'btn btn-info text-uppercase'
											)
									);
								?>
							</div>
						</div>

					</div>
					<div class="well gray-bg">
						
						<div class="row">
							<div class="col-xs-12 col-md-9">
								<div class="row">
									<div class="col-sm-4">
										<h3 class="place-name">
											10 June
										</h3>
									</div>
									<div class="col-sm-8 place-address">
										PCA Stadium - Mohali
									</div>
								</div>
								
							</div>

							<div class="col-xs-12 col-md-3 text-right">
								<?php
									echo $this->Form->button('Find Parking', 
										array(
												'class' => 'btn btn-info text-uppercase'
											)
									);
								?>
							</div>
						</div>

					</div>
				</div>
			</div>
			<div class="col-md-4">
				<h3>Other events</h3>
				<div class="row">
					<div class="col-xs-12 form-group item text-center img-block">
				
						<?php 
							echo $this->Html->image('frontend/img1.png',
								array(
									    'alt' => '',
									    'class' => 'img-responsive'
									)
							);
						?>
						<div class="box">
							<h4>IPL- Mohali</h4>
							<small>3 June, 2015</small>
						</div>
						
					</div>

					<div class="col-xs-12 form-group item text-center img-block">
				
						<?php 
							echo $this->Html->image('frontend/img2.png',
								array(
									    'alt' => '',
									    'class' => 'img-responsive'
									)
							);
						?>
						<div class="box">
							<h4>IPL- Mohali</h4>
							<small>3 June, 2015</small>
						</div>
						
					</div>
				</div>
				<div>
					<!--<p>Your SimplyPark.in passes will be valid for one hour before and one hour after the event.</p>
					<p>Your SimplyPark.in passes will be valid for one hour before and one hour after the event.</p>-->
				</div>
			</div>
		</section>

	</div>
</div>
