<div class="main-container">
    <div class="contact-bg">
        <div class="container">
            <section class="clearfix">
                <div class="row">
                    <div class="col-sm-12 account-info">
                        <div class="col-sm-2">
                            <?php
                                echo $this->Html->image("frontend/city.png", array(
                                    "alt" => "IPL Cricket",
                                    "class" => "img-responsive",
                                    'url' => array(
                                                'controller' => 'recipes', 'action' => 'view', 6
                                            )
                                ));
                            ?>
                        </div>
                        <div class="col-sm-3">
                            <span class="parking-place">DLF Club V at Gurgoan</span><br>
                            <span class="parking-dates"> 1st April - 2nd April, 2015</span><br>
                            <span class="parking-price"><i class="fa fa-inr"></i>150</span>
                        </div>
                        <div class="col-sm-3">
                            <address class="parking-address">
                                <p> 2620 Stoneybrook Road,</p>
                                <p>  Palm Bay, FL 32905</p>
                                <p>  +44 - 7700 900075 </p>
                            </address>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 account-info-section">



                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        1. Account Information
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body ac-info">
                                    <form class="form-horizontal" >
                                        <div class="form-group">
                                            <label for="name " class="col-sm-3 control-label">Name</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="name" placeholder="First Name">
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control " id="lastname" placeholder="Last Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="phone-no" class="col-sm-3 control-label">Contact Details</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="phone-no" placeholder="Phone No">
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="email" class="form-control " id="email" placeholder="Email ">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="car" class="col-sm-3 control-label" >Choose a Car</label>
                                            <div class="col-sm-3">
                                                <select class="form-control" id="sel1">
                                                    <option>MH 12 4537</option>
                                                    <option>MH 12 4536</option>
                                                    <option>PB 12 4537</option>
                                                    <option>CH 12 4537</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-9 col-sm-offset-3">
                                                <button type="submit" class="btn btn-info btn-lg">Next</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        2. Review
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body review-section">
                                    <div class="col-sm-10 col-sm-offset-2">
                                        <dl class="dl-horizontal">
                                            <dt>Parking at</dt>
                                            <dd>DLF Club V, Gurgoan</dd>
                                            <dt>Duraation</dt>
                                            <dd>1st April, 2015 to 1st May, 015</dd>
                                            <dt>Price  </dt>
                                            <dd> 1500 INR</dd>
                                        </dl>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-2 review-text">
                                        <small>Once you book, Club V, Gurgoan will approve your booking. Your credit card will be charged only once approved.</small>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-2 igree-text">
                                        <label class="checkbox-inline"><input type="checkbox" value=""><span class="text-muted">I agree to SimplyPark.in </span><span class="text-primary">Terms & Conditions</span></label>
                                    </div>
                                    <div class="col-xs-10 col-sm-offset-2">
                                        <button type="submit" class="btn btn-info btn-lg">Next</button>
                                        <button type="submit" class="btn btn-default btn-lg">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        3. Payment Information
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body payment-info">
                                    <form class="form-horizontal col-sm-9 col-center" >
                                        <div class="form-group">
                                            <div class="promocode-section">
                                                <div class="col-sm-8">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-lg" id="name" placeholder="Enter Freefund / Promocode">
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-lg btn-gray" type="button">Apply</button>
                                                        </span>
                                                    </div><!-- /input-group -->                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <?php
                                                    echo $this->Html->image("frontend/Flat-Credit-Card-Icons-Set.png", array(
                                                        "alt" => "credit card",
                                                        "class" => "img-responsive credit-card-icon",
                                                        'url' => array('controller' => 'recipes', 'action' => 'view', 6)
                                                    ));
                                                ?>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control" id="name" placeholder="Please enter your name as it appears on your card">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="name" placeholder="Credit Card Number">
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="name" placeholder="Security Code">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="name" placeholder="Expiry Month">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="name" placeholder="Expiry Year">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-info btn-lg">Submit</button>
                                                <button type="submit" class="btn btn-default btn-lg">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>                        
                    </div>

                </div>

            </section>

        </div>
    </div>
</div>