<div class="header clearfix">
	<div class="pull-left">
		<h3>E-Ticket</h3>
		<p>
			<small>
				<span>Booking ID: NF2203545391983</span><br>
				<span>Booking Date: Sat, 23 May 2015</span>
			</small>
		</p>
	</div>
	<div class="pull-right logo">
		<!-- ======= LOGO ========-->
		<a class="navbar-brand" href="#">
			<?php
				echo $this->Html->link(
                    $this->Html->image(
                            'frontend/logo-simply-park.png',
                            array(
                                'class' => 'site-logo img-responsive'
                                )
                        ),
                        '/',
                        array(
                            'class' => 'navbar-brand scrollto',
                            'escape' => false
                    )
            	);
			?>
		</a>
	</div>
</div>
<div class="cloud-bg col-xs-12 clearfix">
	<h3>Parking Booking Details</h3>
	<div class="clearfix">
		<div class="well clearfix well-opacity">
			<div class="col-xs-4">
				<h4>DLF Club V at Gurgoan</h4>
				<p>Mohali</p>
				<address>
					2620 Stoney Raod,<br>
					Palm Bay, Mohali<br>
					+91 - 7700900075
				</address>
			</div>
			<div class="col-xs-8 left-border">
				<table class="booking-detail">
					<tbody>
						<tr>
							<td>Name</td>
							<td>Raju Patil</td>
						</tr>	
						<tr>
							<td>Car registration Number</td>
							<td>MH12 7645</td>
						</tr>	
						<tr>
							<td>Transaction ID</td>
							<td>xxxxxxxxxwvv</td>
						</tr>	
						<tr>
							<td>Booking with</td>
							<td>Raju2043@gmail.com</td>
						</tr>	
						<tr>
							<td>Parking date &amp; time</td>
							<td>12 July, 12:30 PM - 13 July, 2015 12:30</td>
						</tr>	
						<tr>
							<td>Duration</td>
							<td>24 Hr</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<p>
		<b>Transaction ID: xxxxxxxxxwvv</b>
	</p>
		<?php echo $this->element('backend/Booking/print_ticket_content'); ?>
	
</div>
<script type="text/javascript">
	$(document.ready(function () {
		window.print();		
	});
</script>
