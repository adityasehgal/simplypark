<?php
	echo $this->Html->script('frontend/users/profile', array('inline' => false));
?>
<section class="col-xs-12 col-md-9 right-panel">
	
	<div class="heading clearfix col-xs-12 border-bottom">
		<span class="pull-left title">Profile</span>
	</div>

	<?php
		echo $this->Form->create(
				'Space',
				array(
                    'url' => 'addSpacePolicy',
					'method' => 'post',
					'class' => 'form-horizontal form-divide-group',
					'id' => 'add-space',
					'enctype' => 'multipart/form-data',
					'novalidate' => false
					)
			);
	?>
		<div class="col-xs-12 body">
            <article>
    			<div class="form-group">
    				<label for="username" class="col-sm-3 control-label">Name <span class='name'>*</span></label>
    				<div class="col-sm-4">
    					<?php
                    		echo $this->Form->input('first name', array(
                    			'div' => false,
                    			'label' => false,
                    			'id' => 'firstname',
                    			'class' => 'form-control',
                    			'placeholder' => 'First Name'
                    			)
                    		);
                    	?>
    				</div>
    				<div class="col-sm-4">
    					<?php
                    		echo $this->Form->input('last name', array(
                    			'div' => false,
                    			'label' => false,
                    			'id' => 'lastname',
                    			'class' => 'form-control',
                    			'placeholder' => 'Last Name'
                    			)
                    		);
                    	?>
    				</div>
    			</div>

    			<section>
	    			<div class="form-group input-wrapper">
	    				<label for="parkingSpaceName" class="col-sm-3 control-label">Add Car</label>
	    				<div class="col-sm-4">
	    					<?php
	                    		echo $this->Form->input('addcar', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'id' => 'addcar',
	                    			'class' => 'form-control',
	                    			'placeholder' => 'Car Number Plate',
	                    			)
	                    		);
	                    	?>
	                    </div>
	                    <div class="col-sm-1">
	                    	<a href="#" class="plus-btn">
	                    		<span class="fa fa-plus-circle fa-2x"></span>
	                    	</a>
	    				</div>
	    			</div>
	    		</section>

                <div class="form-group">
                    <label for="address" class="col-sm-3 control-label">Profile Picture</label>
                    <div class="col-sm-4">
                        <?php
                            echo $this->Form->input('place_pic', array(
                                'div' => false,
                                'type' => 'file',
                                'label' => false,
                                'class' => 'place_pic form-control',
                                )
                            );
                        ?>
                    </div>
                </div>
            
    			<div class="form-group">
    				<label for="address" class="col-sm-3 control-label">Billing Address</label>
    				<div class="col-sm-8">
    					<?php
                    		echo $this->Form->input('flat_apartment_number', array(
                    			'div' => false,
                    			'label' => false,
                    			'id' => 'address1',
                    			'class' => 'form-control',
                    			'placeholder' => 'Flat/ Apartment Number'
                    			)
                    		);
                    	?>
    				</div>
    			</div>

    			<div class="form-group">
    				<div class="col-sm-8 col-sm-offset-3">
    					<?php
                    		echo $this->Form->input('address', array(
                    			'div' => false,
                    			'label' => false,
                    			'id' => 'address2',
                    			'class' => 'form-control',
                    			'placeholder' => 'Address'
                    			)
                    		);
                    	?>
    				</div>
    			</div>

    			<div class="form-group">
    				<div class="col-sm-4 col-sm-offset-3">
    					<?php
                    		echo $this->Form->input('state', array(
                    			'div' => false,
                    			'label' => false,
                    			'options' => '',
                    			'id' => 'state',
                    			'class' => 'form-control',
                    			'empty' => 'State',
                    			'data-url' => ''
                    			)
                    		);
                    	?>
    				</div>
    				<div class="col-sm-4" id= "list-city">
    					<?php
                    		echo $this->Form->input('city', array(
                    			'div' => false,
                    			'label' => false,
                    			'options' => '',
                    			'id' => 'city',
                    			'class' => 'form-control',
                    			'empty' => 'City',
                    			'data-url' => ''
                    			)
                    		);
                    	?>
    				</div>
    			</div>

    			<div class="form-group">
    				<div class="col-sm-4 col-sm-offset-3">
    					<?php
                    		echo $this->Form->input('post_code', array(
                    			'div' => false,
                    			'label' => false,
                    			'id' => 'pinCode',
                    			'class' => 'form-control',
                    			'placeholder' => 'Pin Code',
                                'type' => 'text'
                    			)
                    		);
                    	?>
    				</div>
    			</div>

    			<div class="form-group">
	    			<div class="checkbox col-sm-offset-3 col-sm-8">
						<label>
							<?php
								echo $this->Form->input('home_address', array(
								    'type' => 'checkbox',
								    'after' => __('Billing address is my home address'),
								    'div' => false,
								    'label' => false,
								    'checked' => 'checked',
								    'id' => 'home-address',
								    'hiddenField' => false // added for non-first elements
								));
							?>							
						</label>
					</div>
				</div>

				<section id="show-home-address" class="hide">
					<div class="form-group">
	    				<label for="address" class="col-sm-3 control-label">Home Address</label>
	    				<div class="col-sm-8">
	    					<?php
	                    		echo $this->Form->input('flat_apartment_number', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'id' => 'address1',
	                    			'class' => 'form-control',
	                    			'placeholder' => 'Flat/ Apartment Number'
	                    			)
	                    		);
	                    	?>
	    				</div>
	    			</div>

	    			<div class="form-group">
	    				<div class="col-sm-8 col-sm-offset-3">
	    					<?php
	                    		echo $this->Form->input('address', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'id' => 'address2',
	                    			'class' => 'form-control',
	                    			'placeholder' => 'Address'
	                    			)
	                    		);
	                    	?>
	    				</div>
	    			</div>

	    			<div class="form-group">
	    				<div class="col-sm-4 col-sm-offset-3">
	    					<?php
	                    		echo $this->Form->input('state', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'options' => '',
	                    			'id' => 'state',
	                    			'class' => 'form-control',
	                    			'empty' => 'State',
	                    			'data-url' => ''
	                    			)
	                    		);
	                    	?>
	    				</div>
	    				<div class="col-sm-4" id= "list-city">
	    					<?php
	                    		echo $this->Form->input('city', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'options' => '',
	                    			'id' => 'city',
	                    			'class' => 'form-control',
	                    			'empty' => 'City',
	                    			'data-url' => ''
	                    			)
	                    		);
	                    	?>
	    				</div>
	    			</div>

	    			<div class="form-group">
	    				<div class="col-sm-4 col-sm-offset-3">
	    					<?php
	                    		echo $this->Form->input('post_code', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'id' => 'pinCode',
	                    			'class' => 'form-control',
	                    			'placeholder' => 'Pin Code',
	                                'type' => 'text'
	                    			)
	                    		);
	                    	?>
	    				</div>
	    			</div>
	    		</section>			
            </article>

            <article>
				<div class="form-group">
					<label for="address" class="col-sm-3 control-label">Password</label>
					<div class="col-sm-4">
						<?php
	                		echo $this->Html->link('Change Password', array
	                			(
		                			'controller' => '',
							        'action' => '',
							        'full_base' => true
		                		),
		                		array(
		                			'class' => 'change-pass', 
		                		)
	                		);
	                	?>
					</div>
				</div>

				<div class="form-group">
					<label for="address" class="col-sm-3 control-label">Mobile Number</label>
					<div class="col-sm-4">
						<?php
	                		echo $this->Form->input('Change Password', array
	                			(
		                			'div' => false,
							        'label' => false,
							        'class' => 'form-control',
							        'id' => '',
							        'placeholder' => ''
		                		)
	                		);
	                	?>
					</div>
					<div class="help-block col-sm-offset-3 col-xs-8 text-info">
						<?php
	                		echo $this->Html->link('Verify Mobile Number', array
	                			(
		                			'controller' => '',
							        'action' => '',
							        'full_base' => true
		                		)
	                		);
	                	?>
					</div>
				</div>
			</article>

			<div class="form-group">
				<label for="address" class="col-sm-3 control-label">Company Name</label>
				<div class="col-sm-4">
                    <?php
                        echo $this->Form->input('company_name', array(
                            'div' => false,
                            'label' => false,
                            'type' => 'text',
                            'class' => 'form-control',
                            'placeholder' => 'If you are company'
                            )
                        );
                    ?>
                </div>
			</div>
            <div class="form-group" id="tanNumber">
                <label for="address" class="col-sm-3 control-label">TAN Number</label>
                <div class="col-sm-4">
                    <?php
                        echo $this->Form->input('tanNumber', array(
                            'div' => false,
                            'label' => false,
                            'class' => 'form-control',
                            'placeholder' => 'If you are company'
                            )
                        );
                    ?>
                </div>
            </div>

		</div>
		<div class="footer clearfix col-xs-12 border-top">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10 text-right">
					<?php
                        echo $this->Html->link('Cancel', 'parkingSpace', array(
                        	'class' => 'btn btn-default'));
                    ?>
                    <?php
                        echo $this->Form->button(__('Next'), array(
                                    'class' => 'btn btn-info',
                                    'type' => 'submit'
                                    ));
                    ?>
				</div>
			</div>
		</div>
	<?php echo $this->Form->end(); ?>

</section>

<?php 
	echo $this->Html->script(
		array(
			'backend/Event/latlong',
		),
		array('inline' => false)
	);
?>

<script type="text/html" id="addCity">
	<select class='form-control' id = "city" name =data[Space][city_id]>
	    <option value="">City</option>
	    <% _.each(data, function(value, key){ %>
	        <option value="<%= key %>"><%= value %></option>
	    <% }); %>
    </select>
</script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
