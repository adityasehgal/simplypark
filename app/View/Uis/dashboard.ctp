<section class="col-xs-12 col-md-9 right-panel">
					
	<div class="heading clearfix">
		<span class="pull-left title">Summary</span>
	</div>
	<div class="summary-box clearfix text-center">
		<div class="col-xs-3 red-bg">
			<i class="fa fa-bookmark-o"></i>
			<span>0</span>
			<div>New Booking</div>
		</div>
		<div class="col-xs-3 green-bg">
			<i class="fa fa-car"></i>
			<span>12</span>
			<div>Active Parking Space</div>
		</div>
		<div class="col-xs-3 purple-bg">
			<i class="fa fa-money"></i>
			<span>5</span>
			<div>Total Confirmed &amp; Paid</div>
		</div>
		<div class="col-xs-3 yellow-bg">
			<i class="fa fa-user"></i>
			<span>78<span class="percentage">%</span></span>
			<div>Profile Completed</div>
		</div>
	</div>

	<div class="heading clearfix primary">
		<span class="title-sm">Summary</span>
	</div>
	<div class="table-responsive">
		<table class="table table-bordered summary-tb">
			<thead>
				<tr>
					<th>Booking ID</th>
					<th>Parking Details</th>
					<th>Booking Dated</th>
					<th colspan="2">Amount</th>
				</tr>
			</thead>
			<tbody>
				<?php
					for ($i=1; $i < 5; $i++) { 
				?>
						<tr>
							<td>BIZK0001105323</td>
							<td>
								<span>Imperial Parking</span>
								<br>
								12 Jan 12:30 AM - 3:00 PM
							</td>
							<td>11 Jan, 2015</td>
							<td>120</td>
							<td><a href="#">Print</a></td>
						</tr>
				<?php } ?>
						<tr>
							<td colspan="5" class="text-center">
								<a href="#">Show All</a>
							</td>
						</tr>
			</tbody>
		</table>
	</div>

	<div class="heading clearfix primary">
		<span class="title-sm">Current Status of my spaces</span>
	</div>
	<div class="table-responsive">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Parking Details</th>
					<th>Total Booking</th>
					<th>Total Empty Slot</th>
				</tr>
			</thead>
			<tbody>
				<?php
					for ($i=1; $i < 2; $i++) { 
				?>
						<tr>
							<td>Imperial Parking<br>12 Jan 12:30 AM - 3:00 PM</td>
							<td>8</td>
							<td>12</td>
						</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>

	<article class="custom-msg clearfix">
		<div class="col-sm-8">
			Have your own Parking place.<br>
			<small>Learn how to make money from it.</small>
		</div>
		<div class="col-sm-4">
			<a href="#" class="pull-right btn btn-info text-uppercase">add space</a>
		</div>
	</article>

	<div class="satisfaction-box clearfix">
		<div class="col-sm-4">
			<?php 
				echo $this->Html->image('satisfaction.png',
					array(
						    'alt' => 'satisfaction',
						    'class' => 'img-responsive'
						)
				);
			?>
		</div>
		<div class="col-sm-8 satisfaction-icon">
			<h2>Simply Park</h2>
			<p>
				All parking are covered under SimplyPark Promise.<br>
				Click here to understand how you covered.
			</p>
		</div>
	</div>
