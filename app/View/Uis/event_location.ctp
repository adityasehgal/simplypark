<?php
echo $this->Html->css('backend/bootstrap/css/bootstrap-datetimepicker.min', array('inline' => false));
echo $this->Html->script(array('backend/bootstrap/bootstrap-datetimepicker.min', 'http://maps.googleapis.com/maps/api/js'), array('inline' => false));
?>
<div class="main-container">
	<!-- <div class="event-banner">
		<div class="container">
			<section class="clearfix">			
				<div class="col-sm-3 img-block">
					<?php 
						echo $this->Html->image('frontend/city.png',
							array(
								    'alt' => 'IPL Cricket',
								    'class' => 'img-responsive'
								)
						);
					?>
				</div>
				<div class="col-sm-9 ">
					<h2>PCA Stadium - Mohali</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
				</div>		
			</section>
		</div>
	</div> -->

	<div class="contact-bg">
		<div class="container">
			<div class="col-md-12 parking-details">
				<h3>Parking near <strong>PCA Stadium - Mohali </strong></h3>
			</div>
		</div>
		<div class="container">
			<section class="clearfix">
				
				<div class="col-md-5">

					<div class="well timeframe clearfix">
						<h4>
							<span class="glyphicon glyphicon-calendar"></span>
							Your Parking Timeframe
						</h4>
						<small>Change the start and end times below to when you'll need parking.</small>
						<form>
							<div class="row">
								<div class="custom-padding clearfix">
									<div class="col-sm-6">
										<div class="form-group">
											<label for="start_date" class="text-primary">Start Date & Time</label>
											<div class='input-group date' id='date1' data-date-format="mm/dd/yyyy hh:ii">
												<?php
							                		echo $this->Form->input('start_date', array(
							                			'div' => false,
							                			'label' => false,
							                			'id' => 'end-date',
							                			'type' => 'text',
							                			'class' => 'form-control',
							                			'placeholder' => 'Date',
							                			'value' => null
							                			)
							                		);
							                	?>
							                	<span class="input-group-addon">
							                        <span class="glyphicon glyphicon-calendar"></span>
							                    </span>
		                					</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label for="end_date" class="text-primary">End Date & Time</label>
											<div class='input-group date' id='date2' data-date-format="mm/dd/yyyy hh:ii">
												<?php
							                		echo $this->Form->input('end_date', array(
							                			'div' => false,
							                			'label' => false,
							                			'id' => 'start-date',
							                			'type' => 'text',
							                			'class' => 'form-control',
							                			'placeholder' => 'Date',
							                			'value' => null
							                			)
							                		);
							                	?>
							                	<span class="input-group-addon">
							                        <span class="glyphicon glyphicon-calendar"></span>
							                    </span>
		                					</div>
										</div>
									</div>
								</div>

								<div class="form-group form-inline">
									<div class="col-sm-12">
										<label for="parking_rates">Parking Rates</label>
										<?php
					                		echo $this->Form->input('space_type_id', array(
					                			'div' => false,
					                			'label' => false,
					                			'id' => 'parking_rates',
					                			'options' => '',
					                			'class' => 'form-control',
					                			'empty' => 'In hourly',
					                            'selected' => ''
					                			)
					                		);
					                	?>
									</div>
								</div>

							</div>
						</form>

					</div>

					<div class="parkingspace-left-section">
						<div class="location-name active clearfix">
							<div class="col-md-12">
								<h4><span class="park-map-marker"></span>
								<span class="parking-name">Parking Space on Sbm Colony, </span></h4>
							</div>

							<div class="col-xs-12">
								<dl class="dl-horizontal">
									<dt>Price:</dt>
									<dd>$50/hr</dd>
									<dt>Distance:</dt>
									<dd>0.0 miles</dd>
									<dt>Available slots:</dt>
									<dd>450</dd>
								</dl>
							</div>
							
							<div class="col-xs-6">
								<a class="view-details" href="#" data-toggle="modal" data-target="#myModal">
									View Details
								</a>
							</div>
							<div class="col-xs-6 text-right">
								<button class="btn btn-info"> BOOK NOW</button>
							</div>				
						</div>

						<div class="location-name clearfix">
							<div class="col-md-12">
								<h4 class="active"><span class="map-marker-icon"></span>
								<span class="parking-name">PCA Stadium Parking </span></h4>
							</div>

							<div class="col-xs-12">
								<dl class="dl-horizontal">
									<dt>Price:</dt>
									<dd>$50/hr</dd>
									<dt>Distance:</dt>
									<dd>0.0 miles</dd>
									<dt>Available slots:</dt>
									<dd>450</dd>
								</dl>
							</div>
						
							<div class="col-xs-6">
								<a class="view-details" href="#">View Details</a>
							</div>
							<div class="col-xs-6 text-right">
								<button class="btn btn-info"> BOOK NOW</button>
							</div>				
						</div>
						<div class="location-name clearfix">
							<div class="col-md-12">
								<h4 class="active"><span class="map-marker-icon"></span>
								<span class="parking-name">PCA Stadium Parking </span> </h4>
							</div>

							<div class="col-xs-12">
								<dl class="dl-horizontal">
									<dt>Price:</dt>
									<dd>$50/hr</dd>
									<dt>Distance:</dt>
									<dd>0.0 miles</dd>
									<dt>Available slots:</dt>
									<dd>450</dd>
								</dl>
							</div>
						
							<div class="col-xs-6">
								<a class="view-details" href="#">View Details</a>
							</div>
							<div class="col-xs-6 text-right">
								<button class="btn btn-info"> BOOK NOW</button>
							</div>
						</div>

						<div class="location-name clearfix">
							<div class="col-md-12">
								<h4 class="active"><span class="map-marker-icon"></span>
								<span class="parking-name">PCA Stadium Parking </span></h4>
							</div>

							<div class="col-xs-12">
								<dl class="dl-horizontal">
									<dt>Price:</dt>
									<dd>$50/hr</dd>
									<dt>Distance:</dt>
									<dd>0.0 miles</dd>
									<dt>Available slots:</dt>
									<dd>450</dd>
								</dl>
							</div>				
					
							<div class="col-xs-6">
								<a class="view-details" href="#">View Details</a>
							</div>
							<div class="col-xs-6 text-right">
								<button class="btn btn-info"> BOOK NOW</button>
							</div>						
						</div>


						<div class="location-name clearfix">
							<div class="col-md-12">
								<h4 class="active"><span class="map-marker-icon"></span>
								<span class="parking-name">PCA Stadium Parking </span></h4>
							</div>

							<div class="col-xs-12">
								<dl class="dl-horizontal">
									<dt>Price:</dt>
									<dd>$50/hr</dd>
									<dt>Distance:</dt>
									<dd>0.0 miles</dd>
									<dt>Available slots:</dt>
									<dd>450</dd>
								</dl>
							</div>							
							
							<div class="col-xs-6">
								<a class="view-details" href="#">View Details</a>
							</div>
							<div class="col-xs-6 text-right">
								<button class="btn btn-info"> BOOK NOW</button>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-7">
					<?php 
						echo $this->Html->image('frontend/event-location-map.png',
							array(
								    'alt' => 'IPL Cricket',
								    'class' => 'img-responsive'
								)
						);
					?>			
				</div>

			</section>

		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade location-details-popup" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title view-details-title" id="myModalLabel">Quick Park</h4>
			</div>
			<div class="modal-body clearfix">
				<article class="clearfix">
					<div class="col-xs-12">
						<div class="row">
							<div class="col-xs-4">
								<?php 
									echo $this->Html->image('frontend/location-details.png',
										array(
											    'alt' => 'IPL Cricket',
											    'class' => 'img-responsive'
											)
									);
								?>
							</div>
							<div class="col-xs-8 quick-park">
								<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae elit consequat, molestie nibh sit amet, aliquam diam. Vestibulum et cursus odio. Donec ac nisl nibh.
								</p>
							</div>
						</div>
					</div>
				</article>

				<article class="clearfix">
					<div class="col-xs-12">

						<table class="table">
							<thead>
								<tr>
									<th>Hourly</th>
									<th>Daily</th>
									<th>Weekly</th>
									<th>Monthly</th>
									<th>Yearly</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><i class="fa fa-inr"></i>8</td>
									<td><i class="fa fa-inr"></i>100</td>
									<td><i class="fa fa-inr"></i>500</td>
									<td><i class="fa fa-inr"></i>800</td>
									<td><i class="fa fa-inr"></i>1000</td>
								</tr>
							</tbody>
						</table>
					</div>
				</article>

				<article class="clearfix">
					<div class="col-xs-12 ">
						<div class="row">
							<div class="col-xs-6">
								<address>
									<span>ADDRESS</span><br>
									<p>2620 Stoneybrook Road,</p>
									<p>Palm Bay, FL 32905</p>
									<p>+44 - 7700 900075</p>
								</address>
							</div>

							<div class="col-xs-6 extra-services">
								<span>AMENTIES</span>
								<p>Secure Parking</p>
								<p>ATM onsite</p>
								<p>24 X 7 Open</p>
							</div>
						</div>
					</div>
				</article>

				<article class="clearfix">
					<div class="col-xs-12 ">
						<div class="row">
							<div class="col-xs-6">
								<span>AVAILABLE SPACES</span><br>
								250
							</div>
							<div class="col-xs-6">
								<span>AVAILABLE SMALL CAR SPACES</span><br>
								210
							</div>
						</div>
					</div>
				</article>

				<article class="clearfix">
					<div class="col-xs-12 ">
						<span>CANCELLATION  POLICY</span>
						<p> Full Refund minimum 2 days prior to the booking</p>
					</div>
				</article>
			</div>
		</div>
	</div>
</div>

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModa2">
  demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="myModa2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
		  <div class="form-group">
		    <label for="inputEmail3" class="col-sm-2 control-label">text</label>
		    <div class="col-sm-8">
		      <input type="text" class="form-control" id="inputEmail3" placeholder="text">
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputEmail3" class="col-sm-2 control-label">Address</label>
		    <div class="col-sm-8">
		      <p>
		      	#315-316, Tricity Plaza,<br>
		      	Peer muchalla,<br>
		      	zirkpur,<br>
		      	Punjab<br>
		      	140603
		      </p>
		    </div>
		  </div>
		  <div id="googleMap" style="width:auto;height:380px;"></div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(function () {
		$(document).ready(function(){

			var from_date = new Date();

			$('#date1').datetimepicker({
	            startDate: from_date,
	            minuteStep: 30,
	            autoclose: true,
	            todayHighlight: true,
		    })

		    $('#date2').datetimepicker({
	            startDate: from_date,
	            minuteStep: 30,
	            autoclose: true,
	            todayHighlight: true,
		    })
		});

		function initialize() {
		  var mapProp = {
		    center:new google.maps.LatLng(51.508742,-0.120850),
		    zoom:5,
		    mapTypeId:google.maps.MapTypeId.ROADMAP
		  };
		  var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
		}
		google.maps.event.addDomListener(window, 'load', initialize);
	});
</script>
