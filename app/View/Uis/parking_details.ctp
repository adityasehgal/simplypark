<div class="main-container">
	<div class="event-banner">
		<div class="container">
			<section class="clearfix">			
				<div class="col-sm-3 img-block">
					<?php 
						echo $this->Html->image('frontend/city.png',
							array(
								    'alt' => 'IPL Cricket',
								    'class' => 'img-responsive'
								)
						);
					?>
				</div>
				<div class="col-sm-9 quick-parking-details">
					<h2>Quick Park</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
				</div>		
			</section>
		</div>
	</div>
	
	<div class="container">
		<div class="parking-details clearfix">
			<div class="col-sm-5">
				<div class="well timeframe clearfix">
					<h4>
						<span class="glyphicon glyphicon-calendar"></span>
						Your Parking Timeframe
					</h4>
					<small>Change the start and end times below to when you'll need parking.</small>
					<form>
						<div class="row">
							<div class="clearfix">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="start_date" class="text-primary">Start Date & Time</label>
										<div class='input-group date' id='date1' data-date-format="mm/dd/yyyy hh:ii">
											<?php
						                		echo $this->Form->input('start_date', array(
						                			'div' => false,
						                			'label' => false,
						                			'id' => 'start-date',
						                			'type' => 'text',
						                			'class' => 'form-control',
						                			'placeholder' => 'Date',
						                			'value' => null
						                			)
						                		);
						                	?>
						                	<span class="input-group-addon">
						                        <span class="glyphicon glyphicon-calendar"></span>
						                    </span>
	                					</div>
									</div>									
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="end_date" class="text-primary">End Date & Time</label>
										<div class='input-group date' id='date2' data-date-format="mm/dd/yyyy hh:ii">
											<?php
						                		echo $this->Form->input('end_date', array(
						                			'div' => false,
						                			'label' => false,
						                			'id' => 'end-date',
						                			'type' => 'text',
						                			'class' => 'form-control',
						                			'placeholder' => 'Date',
						                			'value' => null
						                			)
						                		);
						                	?>
						                	<span class="input-group-addon">
						                        <span class="glyphicon glyphicon-calendar"></span>
						                    </span>
	                					</div>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<button class="btn btn-info btn-block" type="submit">Book Now</button>
									</div>
								</div>
							</div>

						</div>
					</form>

				</div>
				<article>
					<table class="table">
						<thead>
							<tr>
								<th>Hourly</th>
								<th>Daily</th>
								<th>Weekly</th>
								<th>Monthly</th>
								<th>Yearly</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><i class="fa fa-inr"></i>8</td>
								<td><i class="fa fa-inr"></i>100</td>
								<td><i class="fa fa-inr"></i>500</td>
								<td><i class="fa fa-inr"></i>800</td>
								<td><i class="fa fa-inr"></i>1000</td>
							</tr>
						</tbody>
					</table>
				</article>

				<article>
					<span>AMENTIES</span>
			        <dl class="dl-horizontal">
						<dt>Secure Parking</dt>
						<dd>Security</dd>
						<dt>ATM onsite</dt>
						<dd>CCTV</dd>  								
					</dl>
				</article>

				<article>
					<address>
						<span>ADDRESS</span><br>
						<p>2620 Stoneybrook Road,</p>
						<p>Palm Bay, FL 32905</p>
						<p>+44 - 7700 900075</p>
					</address>
				</article>

				<article>
					<ul>
						<li><i class="fa fa-check right-check"></i> Monday</li>
						<li><i class="fa fa-check right-check"></i> Tuesday</li>
						<li><i class="fa fa-check right-check"></i> Wednesday</li>
						<li><i class="fa fa-check right-check"></i> Thursday</li>
						<li><i class="fa fa-check right-check"></i> Friday</li>
						<li><i class="fa fa-times cross-icon"></i> Saturday</li>
						<li><i class="fa fa-times cross-icon"></i> Sunday</li>
						
					</ul>
				</article>

				<article class="clearfix">
					<div class="row">
						<div class="col-xs-6">
							<span>AVAILABLE SPACES</span><br>
							250
						</div>
						<div class="col-xs-6">
							<span>AVAILABLE SMALL CAR SPACES</span><br>
							210

						</div>
					</div>
				
				</article>

				<article class="clearfix cancel-policy">
				  	<span>CANCELLATION  POLICY</span>
					<p> Full Refund minimum 2 days prior to the booking</p>
				</article>
			</div>

			<div class="col-sm-7">	
				<?php 
					echo $this->Html->image('frontend/event-location-map.png',
						array(
							    'alt' => 'IPL Cricket',
							    'class' => 'img-responsive'
							)
					);
				?>	
			</div>
		</div>
	</div>


<script type="text/javascript">
$(function () {
	$(document).ready(function(){


		Date.prototype.addDays = function(days) {
		    this.setDate(this.getDate() + parseInt(days));
		    return this;
		};

		var startDate = new Date();
		var nextDate = startDate.addDays(1);

		$('#date1').datetimepicker({
            startDate: nextDate,
            minuteStep: 30,
            autoclose: true,
            todayHighlight: true,
	    }).on("changeDate", function(e) {
	    	var stDate = new Date($('#start-date').val());
	    	var edDate = stDate.addDays(1);
	    	$('#date2').datetimepicker({
	            startDate: edDate,
	            minuteStep: 30,
	            autoclose: true,
	            todayHighlight: true
		    });
	    });

	});
});
</script>