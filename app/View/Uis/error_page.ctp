<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>404 | Error page: Simply Park</title>

    <!-- Bootstrap -->
    <?php
        echo $this->Html->css(
            array(
                    'frontend/style'
                )
        );
        echo $this->fetch('meta');
        echo $this->fetch('css');
    ?>
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
        <div id="background">
            <h1>404 Page not found, sorry!</h1>
            <p>The page you were looking for appears to have been moved, deleted or does not<br> exist. You could go back to where you were or head straight to our <a href="#">home page</a></p>
        </div>
  </body>
</html>
