<section class="col-xs-12 col-md-9 right-panel">
					
	<div class="heading clearfix col-xs-12">
		<span class="pull-left title">Bookings</span>
	</div>

	<div class="booking-receive" data-example-id="togglable-tabs">
	    <ul id="myTabs" class="nav nav-tabs nav-justified" role="tablist">
	      	<li role="presentation" class="active"><a href="#bookingMade" id="booking-made-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Bookings Made</a></li>
	      	<li role="presentation" class=""><a href="#bookingReceived" role="tab" id="booking-received-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false">Bookings Received</a></li>	   
	    </ul>
	    <div id="myTabContent" class="tab-content">
	      	<div role="tabpanel" class="tab-pane fade active in" id="bookingMade" aria-labelledby="booking-made-tab">
		      	<form class="form-horizontal clearfix">				      	
					<div class="form-group">
						<label class="col-sm-2 control-label" for="address">Search</label>
						<div class="col-sm-4">
							<div class='input-group date' id='date1' data-date-format="mm/dd/yyyy hh:ii">
								<?php
			                		echo $this->Form->input('start_date', array(
			                			'div' => false,
			                			'label' => false,
			                			'id' => 'start-date',
			                			'type' => 'text',
			                			'class' => 'form-control',
			                			'placeholder' => 'Date',
			                			'value' => null
			                			)
			                		);
			                	?>
			                	<span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
        					</div>
						</div>
						<div class="col-sm-4">
							<div class='input-group date' id='date2' data-date-format="mm/dd/yyyy hh:ii">
								<?php
			                		echo $this->Form->input('end_date', array(
			                			'div' => false,
			                			'label' => false,
			                			'id' => 'end-date',
			                			'type' => 'text',
			                			'class' => 'form-control',
			                			'placeholder' => 'Date',
			                			'value' => null
			                			)
			                		);
			                	?>
			                	<span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
        					</div>
						</div>
						<div class="col-sm-2">
							<button type="submit" class="btn btn-info">Show</button>
						</div>					
					</div>							
				</form>
				<div class="table-responsive">
					<table class="table">

						<thead>
							<tr>
								<th>BOOKING ID</th>
								<th>PARKING DETAILS</th>
								<th>PARKING DATE & TIME</th>
								<th>BOOKING AMOUNT</th>
								<th>STATUS</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td scope="row">43ED65</td>
								<td>
									<small>Booking Date: 11 june</small>
									<div>Imperial Parking</div>	
								</td>
								<td>
									<small>
										15 June, 2015<br>
										11:30 AM - 2:00PM
									</small>
								</td>
								<td>57</td>
								<td>
									<small>
										<div class="waiting"></div>
										 &nbsp;   Wait for booking approval from<br>
										space owner<br>

										<a data-toggle="modal" data-target="#myModalcancel" href="#" class="cancel-request">Request for cancellation</a>
									</small>
								</td>
							</tr>
							<tr>
								<td scope="row">43ED65</td>
								<td>
									<small>Booking Date: 11 june</small>
									<div>Imperial Parking</div>	
								</td>
								<td>
									<small>
										15 June, 2015<br>
										11:30 AM - 2:00PM
									</small>
								</td>
								<td>58</td>
								<td>
									<small>
										<div class="approved"></div>&nbsp;	Approved Parking<br>
										Starts on 3 June<br>
										<a class="booking-print" href="#">Print</a>&nbsp;&nbsp;   <span class="cancel-request">Request for cancellation</span>
									</small>
								</td>
							</tr>
							<tr>
								<td scope="row">43ED65</td>
								<td>
									<small>Booking Date: 11 june</small>
									<div>Imperial Parking</div>	
								</td>
								<td>
									<small>
										15 June, 2015<br>
										11:30 AM - 2:00PM
									</small>
								</td>
								<td>60</td>
								<td>
									<small>
										<div class="rejected"></div>&nbsp;	Your booking request rejected
									</small>
								</td>
							</tr>
							<tr class="expired-text">
								<td scope="row">43ED65</td>
								<td>
									<small>Booking Date: 11 june</small>
									<div>Imperial Parking</div>	
								</td>
								<td>
									<small>
										15 June, 2015<br>
										11:30 AM - 2:00PM
									</small>
								</td>
								<td>60</td>
								<td>
									<small>
										<i class="fa fa-times expired-booking"></i> &nbsp;  Expired booking
									</small>
								</td>
							</tr>
						</tbody>
					</table>
				</div>

			</div>


	      	<div role="tabpanel" class="tab-pane fade" id="bookingReceived" aria-labelledby="booking-received-tab">
	         	<form class="form-horizontal clearfix">
				      	
					<div class="form-group">
						<label class="col-sm-2 control-label" for="address">Search</label>
						<div class="col-sm-4">
							<div class='input-group date' id='date3' data-date-format="mm/dd/yyyy hh:ii">
								<?php
			                		echo $this->Form->input('start_date', array(
			                			'div' => false,
			                			'label' => false,
			                			'id' => 'start-date',
			                			'type' => 'text',
			                			'class' => 'form-control',
			                			'placeholder' => 'Date',
			                			'value' => null
			                			)
			                		);
			                	?>
			                	<span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
        					</div>
						</div>
						<div class="col-sm-4">
							<div class='input-group date' id='date4' data-date-format="mm/dd/yyyy hh:ii">
								<?php
			                		echo $this->Form->input('end_date', array(
			                			'div' => false,
			                			'label' => false,
			                			'id' => 'end-date',
			                			'type' => 'text',
			                			'class' => 'form-control',
			                			'placeholder' => 'Date',
			                			'value' => null
			                			)
			                		);
			                	?>
			                	<span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
        					</div>
						</div>
						<div class="col-sm-2">
							<button type="submit" class="btn btn-info">Show</button>
						</div>	
					</div>
							
				</form>

				<div class="table-responsive">
			      	<table class="table">      
	  					<thead>
					        <tr>
								<th>BOOKING ID</th>
								<th>PARKING DETAILS</th>
								<th>PARKING DATE & TIME</th>
								<th>BOOKING AMOUNT</th>
								<th>STATUS</th>
					        </tr>
	  					</thead>
				      	<tbody>
					        <tr>
								<td scope="row">43ED65</td>
								<td>
									<small>Booking Date: 11 june</small>
									<div>Imperial Parking</div>	
								</td>
								<td>
									<small>
										15 June, 2015<br>
										11:30 AM - 2:00PM
									</small>
								</td>
								<td>57</td>
								<td>
									<small>
										<div class="waiting"></div> &nbsp;   Wait for booking approval<br>
										<a data-toggle="modal" data-target="#myModalapprove" href="#"class="aprove-text">Approve</a> &nbsp; &nbsp;  <a data-toggle="modal" data-target="#myModaldisapprove" href="#" class="disapprove-text">Disapprove</a><br>
									</small>
								</td>
					        </tr>
					        <tr>
								<td scope="row">43ED65</td>
								<td>
									<small>Booking Date: 11 june</small>
									<div>Imperial Parking</div>	
								</td>
								<td>
									<small>
										15 June, 2015<br>
										11:30 AM - 2:00PM
									</small>
								</td>
								<td>58</td>
								<td>
									<small>
										<div class="approved"></div>&nbsp;	Approved Boking<br>
										Starts on 3 June<br>
										<a data-toggle="modal" data-target="#myModalcancelbooking" href="#" class="cancel-booking">Cancel Booking</a>
									</small>
								</td>
							</tr>
							<tr>
								<td scope="row">43ED65</td>
								<td>
									<small>Booking Date: 11 june</small>
									<div>Imperial Parking</div>	
								</td>
								<td>
									<small>
										15 June, 2015<br>
										11:30 AM - 2:00PM
									</small>
								</td>
								<td>60</td>
								<td>
									<small>
										<div class="rejected"></div>&nbsp;	 Booking request rejected by you
									</small>
								</td>
							</tr>
							<tr>
								<td scope="row">43ED65</td>
								<td>
									<small>Booking Date: 11 june</small>
									<div>Imperial Parking</div>	
								</td>
								<td>
									<small>
										15 June, 2015<br>
										11:30 AM - 2:00PM
									</small>
								</td>
								<td>60</td>
								<td>
									<small>
										<i class="fa fa-times expired-booking"></i> &nbsp;  Expired booking
									</small>
								</td>
							</tr>
							<tr>
								<td scope="row">43ED65</td>
								<td>
									<small>Booking Date: 11 june</small>
									<div>Imperial Parking</div>	
								</td>
								<td>
									<small>
										15 June, 2015<br>
										11:30 AM - 2:00PM
									</small>
								</td>
								<td>60</td>
								<td>
									<small>
										<i class="fa fa-times expired-booking"></i> &nbsp;  Parking Canceled from user
									</small>
								</td>
							</tr>
				     	</tbody>
					</table>
				</div>
	 		</div>		   
				   
	    </div>
	</div>
</section>

<!-- Modal -->
		<div class="modal fade" id="myModalcancel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog ">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title view-details-title" id="myModalLabel">Request for Cancellation</h4>
					</div>
				<div class="modal-body clearfix">
				<span class-="model-text">	Are you sure you want cancel your Parking Space!</span>

					</div>
				<div class="modal-footer">
				<button type="button" class="btn btn-default">Cancel</button>
				<button type="button" class="btn btn-info" data-dismiss="modal">Yes</button>
				
				</div>
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="myModalapprove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title view-details-title" id="myModalLabel">Approval</h4>
					</div>
				<div class="modal-body clearfix">
			<span class-="model-text">	Are you sure you want Approve your Parking Space!</span>

					</div>
				<div class="modal-footer">
				<button type="button" class="btn btn-default">Cancel</button>
				<button type="button" class="btn btn-info" data-dismiss="modal">Yes</button>
				
				</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="myModaldisapprove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title view-details-title" id="myModalLabel">Disapprove</h4>
					</div>
				<div class="modal-body clearfix">
			<span class-="model-text">	Are you sure you want Disapprove your Parking Space!</span>

					</div>
				<div class="modal-footer">
				<button type="button" class="btn btn-default">Cancel</button>
				<button type="button" class="btn btn-info" data-dismiss="modal">Yes</button>
				
				</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="myModalcancelbooking" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title view-details-title" id="myModalLabel">Cancel Booking</h4>
					</div>
				<div class="modal-body clearfix">
					<span class-="model-text">	Are you sure you want cancel customer parking space!</span>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default">Cancel</button>
					<button type="button" class="btn btn-info" data-dismiss="modal">Yes</button>				
				</div>
				</div>
			</div>
		</div>
