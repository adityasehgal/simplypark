<div class="main-container">
   <div class="contact-bg">
      <div class="container">
         <section class="clearfix contact-main">
            <div class="col-sm-7">
               <h1>Contact us</h1>
               <p>
                  We are here to answer any questions you may have about our SimplyPark. Reach out to us and we'll respond as soon as we can.
               </p>
               <p>Even if there is something you have always wanted to experience and can't find it on SimplyPark, let us know and we promise we'll do our best to find it for you and send you there.
               </p>
               <form>
                  <div class="row">
                     <div class="form-group col-sm-6">
                        <label for="name">NAME <span>*</span></label>
                        <input type="text" name="name" class="form-control" id="name" >
                     </div>
                     <div class="form-group col-sm-6">
                        <label for="Email">EMAIL <span>*</span></label>
                        <input type="email" name="Email" class="form-control" id="email">
                     </div>
                     <div class="col-sm-12 form-group">
                        <label for="bookingId">BOOKING ID (if its available)</label>
                        <input type="text" class="form-control" name="bookingID" id="bookingId">
                     </div>
                     <div class="col-sm-12 form-group">
                        <label for="message">MESSAGE</label>
                        <textarea class="form-control" rows="5"></textarea>
                     </div>
                     <div class="col-sm-12 captcha">
                        <div class="row">
                           <div class="col-sm-6">
                              <?php 
                                 echo $this->Html->image('frontend/recaptcha-example.gif',
                                    array(
                                           'alt' => 'recaptcha',
                                           'class' => 'img-responsive'
                                       )
                                 );
                              ?>
                           </div>
                           <div class="col-sm-6 text-right">
                              <?php
                                 echo $this->Form->button('SEND', array(
                                     'type' => 'submit',
                                     'class' => 'btn btn-info btn-lg contact-btn-send',
                                     'escape' => true
                                 ));
                              ?>
                              <!-- <button class="btn btn-info btn-lg contact-btn-send" type="submit">SEND</button> -->
                           </div>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-4 contact-right">
               <div class="row">
                  <div class="col-sm-8 col-center">
                     <p><strong>EMAIL</strong><br>
                        <span>customercare@simplypark.com</span>
                     </p>
                     <p><i class="fa fa-twitter fa-fw"><br>
                        <span>Simplypark</span>
                     </p>
                     <p class="contact-address"><strong>ADDRESS</strong>
                     <p>simplypark<br>
                        1 kings Avenue<br>
                        Delhi<br>
                        india
                     </p>
                     </p> 
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
