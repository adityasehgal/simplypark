<?php
	echo $this->Html->script('frontend/users/profile', array('inline' => false));
?>
<section class="col-xs-12 col-md-9 right-panel">
	
	<div class="heading clearfix col-xs-12 border-bottom">
		<span class="pull-left title"><?php echo __('Update Profile'); ?></span>

	</div>
	<div class="col-xs-12 parking-space">
							<div role="alert" class="alert alert-info text-center">
				Once you are done, please Press Update to save your changes
		   </div>
			</div>
	<?php
	   
		echo $this->Form->create(
				'User',
				array(
					'url' => array(
								'controller' => 'users',
								'action' => 'updateProfileSave'
							),
					'type' => 'post',
					'class' => 'form-horizontal form-divide-group',
					'id' => 'update-profile',
					'enctype' => 'multipart/form-data',
					// 'novalidate' => true
					)
			);
			echo $this->Form->hidden('id', array('value' => $this->Session->read('Auth.User.id')));
			echo $this->Form->hidden('UserProfile.id', array('value' => $getUserProfile['UserProfile']['id']));
	?>
		<div class="col-xs-12 body">
            <article>
    			<div class="form-group">
    				<label for="username" class="col-sm-3 control-label"><?php echo __('Name'); ?> <span class='name'>*</span></label>
    				<div class="col-sm-4">
    					<?php
                    		echo $this->Form->input('UserProfile.first_name', array(
                    			'div' => false,
                    			'label' => false,
                    			'id' => 'firstname',
                    			'class' => 'form-control',
                    			'placeholder' => __('First Name'),
                    			'value' => $userData['UserProfile']['first_name']
                    			)
                    		);
                    	?>
    				</div>
    				<div class="col-sm-4">
    					<?php
                    		echo $this->Form->input('UserProfile.last_name', array(
                    			'div' => false,
                    			'label' => false,
                    			'id' => 'lastname',
                    			'class' => 'form-control',
                    			'placeholder' => __('Last Name'),
                    			'value' => $userData['UserProfile']['last_name']
                    			)
                    		);
                    	?>
    				</div>
    			</div>

    			<div class="form-group">
                    <label for="username" class="col-sm-3 control-label"><?php echo __('Username'); ?></label>
                    <div class="col-sm-4">
                        <?php
                            echo $this->Form->input('User.username', array(
                                'div' => false,
                                'label' => false,
                                'class' => 'place_pic form-control',
                                'value' => $userData['User']['username'],
                                'disabled' => true
                                )
                            );
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label"><?php echo __('Email'); ?></label>
                    <div class="col-sm-4">
                        <?php
                            echo $this->Form->input('User.email', array(
                                'div' => false,
                                'label' => false,
                                'class' => 'place_pic form-control',
                                'value' => $userData['User']['email'],
                                'disabled' => true
                                )
                            );
                        ?>
                    </div>
                </div>

    			<section>
	    			<div class="form-group input-wrapper">
	    				<label for="parkingSpaceName" class="col-sm-3 control-label"><?php echo __('Please enter your Car Registration Number'); ?><span class='addcar'></span></label>
	    				<div class="col-sm-4">
	    					<?php
	                    		echo $this->Form->input('UserCar.0.registeration_number', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'id' => 'addcar',
	                    			'class' => 'form-control',
	                    			'placeholder' => __('Car Number Plate'),
	                    			'value' => !empty($userData['UserCar']) ? $userData['UserCar'][0]['registeration_number'] : ''
	                    			)
	                    		);
	                    	?>
	                    </div>
	                    <div class="col-sm-4">
	    					<?php
	                    		echo $this->Form->input('UserCar.0.car_type', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'options' => Configure::read('CarTypes'),
	                    			'class' => 'form-control',
	                    			'default' => !empty($userData['UserCar']) ? $userData['UserCar'][0]['car_type'] : ''
	                    			)
	                    		);
	                    		if (!empty($userData['UserCar'])) {
	                    			echo $this->Form->hidden('UserCar.0.id', array('value' => $userData['UserCar'][0]['id']));
	                    			unset($userData['UserCar'][0]);
	                    		}
	                    	?>
	                    </div>
	    			</div>
	    			<?php if (!empty($userData['UserCar'])) {
	    					$i = 1;
	    					foreach ($userData['UserCar'] as $showCars) {
	    			?>
				    			<div class="form-group" id="<?php echo $i; ?>">
			                        <div class="col-sm-4 col-sm-offset-3">
			                        	<?php
			                        		echo $this->Form->hidden('UserCar.'.$i.'.id', array('value' => $showCars['id']));
			                        		echo $this->Form->input(
			                        				'UserCar.'.$i.'.registeration_number',
			                        				array(
			                        						'class' => 'form-control',
			                        						'label' => false,
			                        						'div' => false,
			                        						'value' => $showCars['registeration_number']
			                        					)
			                        			);
			                        	?>
			                        </div>
			                        <div class="col-sm-4">
				    					<?php
				                    		echo $this->Form->input('UserCar.'.$i.'.car_type', array(
				                    			'div' => false,
				                    			'label' => false,
				                    			'options' => Configure::read('CarTypes'),
				                    			'class' => 'form-control',
				                    			'default' => $showCars['car_type']
				                    			)
				                    		);
				                    	?>
				                    </div>
			                        <div class="col-sm-1">
			                        	<?php echo $this->Html->link(
	                    								'<span class="fa fa-minus-circle fa-2x"></span>',
	                    								'#',
	                    								array(
	                    										'class' => 'minus-btn text-danger',
	                    										'escape' => false
	                    									)
	                    							);
	                    				?>
			                        </div>
			                    </div>
	    			<?php $i++; } } ?>
	    		</section>

	    		<div class="form-group">
		    		<div class="custom-plus">
	                	<?php echo $this->Html->link(
	                								'<span class="fa fa-plus-circle fa-2x"></span>',
	                								'#',
	                								array(
	                										'class' => 'plus-btn',
	                										'escape' => false
	                									)
	                							);
	                	?>
					</div>
				</div>

                <div class="form-group">
                    <label for="address" class="col-sm-3 control-label"><?php echo __('Profile Picture'); ?></label>
                    <div class="col-sm-4">
                        <?php
                            echo $this->Form->input('UserProfile.profile_pic', array(
                                'div' => false,
                                'type' => 'file',
                                'label' => false,
                                'class' => 'place_pic form-control',
                                )
                            );
                        ?>
	                    <div class="help-info">
							<small class="text-info">
								<?php echo __('Please upload image with minimum upload size of 150*160.'); ?>
							</small>
						</div>
                    </div>
                </div>
            
    			<div class="form-group">
					<label for="address" class="col-sm-3 control-label"><?php echo __('Billing Address'); ?><span class='falt_apartment_number'>*</span></label>
					<?php
						echo $this->Form->hidden('UserAddress.1.type', array('value' => Configure::read('UserAddressType.Billing')));
						if (isset($userData['UserAddress'][0]['id']) && !empty($userData['UserAddress'][0]['id'])) {
							echo $this->Form->hidden('UserAddress.1.id', array('value' => $userData['UserAddress'][0]['id']));
						}
					?>
					<div class="col-sm-8">
						<?php
				    		echo $this->Form->input('UserAddress.1.flat_apartment_number', array(
				    			'div' => false,
				    			'label' => false,
				    			'class' => 'form-control',
				    			'placeholder' => __('Flat/ Apartment Number'),
				    			'value' => (isset($userData['UserAddress'][0]['flat_apartment_number'])) ? $userData['UserAddress'][0]['flat_apartment_number'] : ''
				    			)
				    		);
				    	?>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-8 col-sm-offset-3">
						<?php
				    		echo $this->Form->input('UserAddress.1.address', array(
				    			'div' => false,
				    			'label' => false,
				    			'class' => 'form-control',
				    			'placeholder' => __('Address'),
				    			'value' => (isset($userData['UserAddress'][0]['address'])) ? $userData['UserAddress'][0]['address'] : ''
				    			)
				    		);
				    	?>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-4 col-sm-offset-3">
						<?php
				    		echo $this->Form->input('UserAddress.1.state_id', array(
				    			'div' => false,
				    			'label' => false,
				    			'options' => $states,
				    			'id' => 'state-list',
				    			'class' => 'form-control',
				    			'empty' => __('State'),
				    			'default' => (isset($userData['UserAddress'][0]['state_id'])) ? $userData['UserAddress'][0]['state_id'] : ''
				    			)
				    		);
				    	?>
					</div>
					<div class="col-sm-4" id="cityContainer">
						<?php
				    		echo $this->Form->input('UserAddress.1.city_id', array(
				    			'div' => false,
				    			'label' => false,
				    			'options' => (isset($billingCities)) ? $billingCities : '',
				    			'id' => 'city',
				    			'class' => 'form-control',
				    			'empty' => __('City'),
				    			'default' => (isset($userData['UserAddress'][0]['city_id'])) ? $userData['UserAddress'][0]['city_id'] : ''
				    			)
				    		);
				    	?>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-4 col-sm-offset-3">
						<?php
				    		echo $this->Form->input('UserAddress.1.pincode', array(
				    			'div' => false,
				    			'label' => false,
				    			'class' => 'form-control',
				    			'placeholder' => __('Pin Code'),
				                'type' => 'text',
				    			'value' => (isset($userData['UserAddress'][0]['pincode'])) ? $userData['UserAddress'][0]['pincode'] : ''
				    			)
				    		);
				    	?>
					</div>
				</div>

    			<div class="form-group">
	    			<div class="checkbox col-sm-offset-3 col-sm-8">
						<label>
							<?php
								echo $this->Form->input('home_address', array(
								    'type' => 'checkbox',
								    'after' => __('Billing address is my home address'),
								    'div' => false,
								    'label' => false,
								    'checked' => (!empty($userData['UserAddress'])) ? false : true,
								    'id' => 'home-address',
								    'hiddenField' => false // added for non-first elements
								));
							?>							
						</label>
					</div>
				</div>

				<section id="show-home-address" class="<?php echo (empty($userData['UserAddress'])) ? 'hide' : ''; ?>">
					<div class="form-group">
						<label for="address" class="col-sm-3 control-label"><?php echo __('Home Address'); ?><span class='falt_apartment_number'>*</span></label>
						<div class="col-sm-8">
							<?php
								if (isset($userData['UserAddress'][1]['id']) && !empty($userData['UserAddress'][1]['id'])) {
									echo $this->Form->hidden('UserAddress.2.id', array('value' => $userData['UserAddress'][1]['id']));
								}
					    		echo $this->Form->input('UserAddress.2.flat_apartment_number', array(
					    			'div' => false,
					    			'label' => false,
					    			'class' => 'form-control',
					    			'placeholder' => __('Flat/ Apartment Number'),
				    				'value' => (isset($userData['UserAddress'][1]['flat_apartment_number'])) ? $userData['UserAddress'][1]['flat_apartment_number'] : ''
					    			)
					    		);
					    	?>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-8 col-sm-offset-3">
							<?php
					    		echo $this->Form->input('UserAddress.2.address', array(
					    			'div' => false,
					    			'label' => false,
					    			'class' => 'form-control',
					    			'placeholder' => __('Address'),
				    				'value' => (isset($userData['UserAddress'][1]['address'])) ? $userData['UserAddress'][1]['address'] : ''
					    			)
					    		);
					    	?>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-3">
							<?php
					    		echo $this->Form->input('UserAddress.2.state_id', array(
					    			'div' => false,
					    			'label' => false,
					    			'options' => $states,
					    			'id' => 'state-list-home',
					    			'class' => 'form-control',
					    			'empty' => __('State'),
				    				'default' => (isset($userData['UserAddress'][1]['state_id'])) ? $userData['UserAddress'][1]['state_id'] : ''
					    			)
					    		);
					    	?>
						</div>
						<div class="col-sm-4" id="cityContainerHome">
							<?php
					    		echo $this->Form->input('UserAddress.2.city_id', array(
					    			'div' => false,
					    			'label' => false,
					    			'options' => (isset($homeCities)) ? $homeCities : '',
					    			'id' => 'city-list-home',
					    			'class' => 'form-control',
					    			'empty' => __('City'),
				    				'default' => (isset($userData['UserAddress'][1]['city_id'])) ? $userData['UserAddress'][1]['city_id'] : ''
					    			)
					    		);
					    	?>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-3">
							<?php
					    		echo $this->Form->input('UserAddress.2.pincode', array(
					    			'div' => false,
					    			'label' => false,
					    			'class' => 'form-control',
					    			'placeholder' => __('Pin Code'),
					                'type' => 'text',
				    				'value' => (isset($userData['UserAddress'][1]['pincode'])) ? $userData['UserAddress'][1]['pincode'] : ''
					    			)
					    		);
					    	?>
						</div>
					</div>
	    		</section>			
            </article>

            <article>
            <?php if (!$this->Session->read('Auth.User.social_network_user')) { ?>	
				<div class="form-group">
					<label for="address" class="col-sm-3 control-label"><?php echo __('Password'); ?></label>
					<div class="col-sm-4">
						<?php
	                		echo $this->Html->link('Change Password', array
	                			(
		                			'controller' => 'users',
							        'action' => 'changePassword',
							        'full_base' => true
		                		),
		                		array(
		                			'class' => 'change-pass', 
		                		)
	                		);
	                	?>
					</div>
				</div>
			<?php } ?>
				<?php
					$class1 = $class2 = $class3 = ''; 
					if($userData['UserProfile']['is_mobile_verified'] == Configure::read('Bollean.True')) { 
						$class1 = 'has-success has-feedback'; 
						$class2 = 'glyphicon glyphicon-ok form-control-feedback';
						$class3 = 'col-left-zero';
 					}
				?>
				<div class="form-group main <?php echo $class1; ?>">
					<label for="address" class="col-sm-3 control-label"><?php echo __('Mobile Number'); ?><span class='mobile_number'>*</span></label>
					<div class="col-sm-4">
						<?php
	                		echo $this->Form->input('UserProfile.mobile', array
	                			(
		                			'div' => false,
							        'label' => false,
							        'class' => 'form-control mobile',
                    				'value' => $userData['UserProfile']['mobile']
		                		)
	                		);
	                	?>	                	
					</div>
					<div class="col-sm-1 <?php echo $class3; ?> text-left">
						<span class="<?php echo $class2 ?>" aria-hidden="true"></span>
					</div>
					<?php //if($userData['User']['is_verified'] != Configure::read('Bollean.True')) { ?> 

					<? //} ?>
					<div class="help-info col-sm-offset-3 col-xs-8 text-info response">
						<small class="text-info">
							
						</small>
					</div>
					<?php
					    
						if ($userData['UserProfile']['mobile'] != null) {
							if($userData['UserProfile']['is_mobile_verified'] != Configure::read('Bollean.True')) { ?> 
								<div class="help-block col-sm-offset-3 col-xs-8 text-info">
									<?php
				                		echo $this->Html->link(
				                				'Send OTP on phone to verify mobile number',
				                				'#;',
				                				array
					                			(
					                				'class' => 'sendOtp',
					                				'escape' => false
						                		)
				                		);
				                		
				                	?>
								</div>
					<?php }} ?>
				</div>
				<div id="verifyOTP" class='hide' >
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-3">
							 Verify OTP received on phone
						</div>
					</div>
					<div class="form-group">
						<label for="address" class="col-sm-3 control-label">Code</label>
						<div class="col-sm-4">
							<?php
		                		echo $this->Form->input('code', array
		                			(
			                			'div' => false,
								        'label' => false,
								        'class' => 'form-control code',
	                    				
			                		)
		                		);
		                	?>
						</div>
					</div>

					<div class="form-group">
						<label for="address" class="col-sm-3 control-label">Phone</label>
						<div class="col-sm-4">
							<?php
		                		 echo $this->Form->input('phone', array(
		                        	'type' => 'text',
		                            'div' => false,
		                            'label' => false,
		                            'class' => 'form-control phone',
		                            
		                    		
                            	)
                        		);
		                	?>
						</div>
						<div class="help-info col-sm-offset-3 col-xs-8 text-info verification-message">
							<small class="text-info">
								
							</small>
						</div>
						<div class="help-block col-sm-offset-3 col-xs-8 text-info">
							<?php
		                		echo $this->Html->link(
		                				'Verify OTP received on phone',
		                				'#;',
		                				array
			                			(
			                				'class' => 'verify',
			                				'escape' => false
				                		)
		                		);
		                	?>
						</div>
					</div>
					
				</div>

			</article>

			<div class="form-group">
				<label for="address" class="col-sm-3 control-label"><?php echo __('Company Name'); ?></label>
				<div class="col-sm-4">
                    <?php
                        echo $this->Form->input('UserProfile.company', array(
                            'div' => false,
                            'label' => false,
                            'class' => 'form-control',
                            'placeholder' => __('If you are company'),
                    		'value' => $userData['UserProfile']['company']
                            )
                        );
                    ?>
                </div>
			</div>
            <div class="form-group" id="tanNumber">
                <label for="address" class="col-sm-3 control-label"><?php echo __('TAN Number'); ?></label>
                <div class="col-sm-4">
                    <?php
                        echo $this->Form->input('UserProfile.tan_number', array(
                        	'type' => 'text',
                            'div' => false,
                            'label' => false,
                            'class' => 'form-control',
                            'placeholder' => __('If you are company'),
                    		'value' => $userData['UserProfile']['tan_number']
                            )
                        );
                    ?>
                </div>
            </div>
		</div>
		<div class="footer clearfix col-xs-12 border-top">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10 text-right">
					<?php
                        echo $this->Html->link(__('Cancel'), 'parkingSpace', array(
                        	'class' => 'btn btn-default'));
                    ?>
                    <?php
                        echo $this->Form->button(__('Update'), array(
                                    'class' => 'btn btn-info',
                                    'type' => 'submit'
                                    ));
                    ?>
				</div>
			</div>
		</div>
	<?php echo $this->Form->end(); ?>

</section>
<script type="text/html" id="cityData">
    <select name="data[UserAddress][1][city_id]" class="form-control">
        <option value="">Select</option>
        <% _.each(data, function(value, key){ %>
            <option value="<%= key %>"><%= value %></option>
        <% }); %>
    </select>
</script>
<script type="text/html" id="cityDataHome">
    <select name="data[UserAddress][2][city_id]" class="form-control">
        <option value="">Select</option>
        <% _.each(data, function(value, key){ %>
            <option value="<%= key %>"><%= value %></option>
        <% }); %>
    </select>
</script>

