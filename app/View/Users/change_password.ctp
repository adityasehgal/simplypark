<!-- ==============================================
Right panel
=============================================== -->
<section class="col-xs-12 col-md-9 right-panel">
	<div class="heading clearfix col-xs-12 border-bottom">
		<span class="pull-left title"><?php echo __("Change Password"); ?></span>
	</div>
	<?php
		echo $this->Form->create(
				'User',
				array(
						'url' => '/users/saveChangePassword',
						'method' => 'post',
						'id' => 'changePasswordForm',
						'class' => 'form-horizontal',
						'inputDefaults' => array(
								'div' => false,
								'label' => false
							),
						'novalidate' => true
					)
			);
			echo $this->Form->hidden(
					'id',
					array(
							'value' => $this->Session->read('Auth.User.id')
						)
				);
	?>
		<div class="col-xs-12 body">
			<div class="form-group">
				<label for="oldPassword" class="col-sm-3 control-label"><?php echo __('Old Password'); ?></label>
				<div class="col-sm-8">
					<?php
						echo $this->Form->input(
								'old_password',
								array(
										'class' => 'form-control',
										'type' => 'password',
										'placeholder' => __('Old Password')
									)
							);
					?>
				</div>
			</div>
			<div class="form-group">
				<label for="newPassword" class="col-sm-3 control-label"><?php echo __('New Password'); ?></label>
				<div class="col-sm-8">
					<?php
						echo $this->Form->input(
								'password',
								array(
										'class' => 'form-control',
										'id' => 'new-password',
										'placeholder' => __('New Password'),
                                        'data-display' => 'mainPassword'
									)
							);
					?>
					<div class="left passwordStrength" id="mainPassword"></div>
				</div>
			</div>
			<div class="form-group">
				<label for="confirmPassword" class="col-sm-3 control-label"><?php echo __('Confirm Password'); ?></label>
				<div class="col-sm-8">
					<?php
						echo $this->Form->input(
								'confirm_password',
								array(
										'class' => 'form-control',
										'type' => 'password',
										'placeholder' => __('Confirm password')
									)
							);
					?>
				</div>
			</div>
		</div>
		<div class="footer clearfix col-xs-12 border-top">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10 text-right">
					<?php
							echo $this->Form->button(
									__('Update'),
									array(
											'class' => 'btn btn-info',
											'type' => 'submit',
											'escape' => false
										)
								);
					?>
				</div>
			</div>
		</div>
	<?php echo $this->Form->end(); ?>
</section>