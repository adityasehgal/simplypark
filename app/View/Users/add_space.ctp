<?php
	echo $this->Html->script('frontend/users/addSpace', array('inline' => false));
?>
<section class="col-xs-12 col-md-9 right-panel">
	
	<div class="heading clearfix col-xs-12 border-bottom">
    <?php 
        $title = 'Add Space';
        $readonly = '';
        $disabled = '';
        if(isset($data['space_data']['Space'])) {
            if($data['space_data']['Space']['is_completed'] == 1 && $data['space_data']['Space']['is_approved'] == 1){
    		  $title = 'Review Space';
                  $readonly = 'readonly';
                  $disabled = 'disabled';
            }
            else if($data['space_data']['Space']['is_completed'] == 1){
    		  $title = 'Edit Space';
            }
         }
    ?>
    <?php echo $this->Session->flash('reviewSpaceMessage'); ?>
    <span class="pull-left title"><?php echo $title;?>
    </span>
	</div>

	<?php
		echo $this->Form->create(
				'Space',
				array(
                    'url' => 'addSpacePolicySave',
					'method' => 'post',
					'class' => 'form-horizontal form-divide-group',
					'id' => 'add-space',
					'enctype' => 'multipart/form-data',
					'novalidate' => false
					)
			);
        if(isset($data['space_data']['Space'])) {
            echo $this->Form->input('id', array(
                                'div' => false,
                                'label' => false,
                                'type' => 'hidden',
                                'value' => $data['space_data']['Space']['id']
                                )
                            );
            echo $this->Form->input('space_pricing', array(
                                'div' => false,
                                'label' => false,
                                'type' => 'hidden',
                                'value' => isset($data['space_data']['SpacePricing']['id'])?$data['space_data']['SpacePricing']['id']:null
                                )
                            );
            echo $this->Form->input('small_car_space', array(
                                'div' => false,
                                'label' => false,
                                'type' => 'hidden',
                                'value' => $data['space_data']['SmallCarSpace']['id']
                                )
                            );
        }
	?>
		<div class="col-xs-12 body">
            <article>
    			<div class="form-group">
    				<label for="parkingSpaceName" class="col-sm-3 control-label">Parking Space Name<span class='error'> *</span></label>
    				<div class="col-sm-8">
    					<?php
                    		echo $this->Form->input('name', array(
                    			'div' => false,
                    			'label' => false,
                    			'id' => 'parkingSpaceName',
                    			'class' => 'form-control',
                    			'placeholder' => 'Parking space name',
                                'value' => (isset($data['space_data']['Space'])) ? $data['space_data']['Space']['name'] : ''
                    			)
                    		);
                    	?>
    				</div>
    			</div>

    			<div class="form-group">
    				<label for="parkingSpaceName" class="col-sm-3 control-label">Description</label>
    				<div class="col-sm-8">
    					<?php
                    		echo $this->Form->textarea('description', array(
                    			'div' => false,
                    			'label' => false,
                    			'id' => 'description',
                    			'class' => 'form-control',
                    			'placeholder' => 'Parking description',
                                'value' => (isset($data['space_data']['Space'])) ? $data['space_data']['Space']['description'] : ''
                    			)
                    		);
                    	?>
    				</div>
    			</div>

                <div class="form-group">
                    <label for="address" class="col-sm-3 control-label">Space Picture</label>
                    <div class="col-sm-8 form-inline">
                        <?php if(isset($data['space_data']['Space'])) { ?>
                            <?php if(!empty($data['space_data']['Space']['place_pic'])) { ?>
                                <img src="<?php echo $data['path'].'files/SpacePic/place_pic.'.$data['space_data']['Space']['place_pic']; ?>" alt="Space Image">
                            <?php } ?>
                        <?php } ?>
                        <?php
                            echo $this->Form->input('place_pic', array(
                                'div' => false,
                                'type' => 'file',
                                'label' => false,
                                'class' => 'place_pic',
                                )
                            );
                        ?>
                    </div>
                </div>
            </article>

            <article>
                <div class="form-group">
                    <label for="address" class="col-sm-3 control-label">Space Information</label>
                    <div class="col-sm-4">
                        <?php
                            echo $this->Form->input('space_type_id', array(
                                'div' => false,
                                'label' => false,
                                'options' => $data['space_type'],
                                'class' => 'form-control',
                                'empty' => 'Type of space',
                                'readonly' => $readonly,
                                'disabled' => $disabled,
                                'selected' => (isset($data['space_data']['Space'])) ? $data['space_data']['Space']['space_type_id'] : ''
                                )
                            );
                           //disabled and readonly for select will not post the data. therefore, we add a hidden input value with the 
                           //same name
                           if (!empty($disabled)) {
                            echo $this->Form->input('space_type_id', array(
                                'div' => false,
                                'label' => false,
                                'class' => 'form-control',
                                'empty' => 'Type of space',
                                'type' => 'hidden',
                                'value' => (isset($data['space_data']['Space'])) ? $data['space_data']['Space']['space_type_id'] : ''
                                )
                            );
                           }
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                            echo $this->Form->input('property_type_id', array(
                                'div' => false,
                                'label' => false,
                                'options' => $data['property_type'],
                                'class' => 'form-control',
                                'id' => 'property_type_id',
                                'empty' => 'Type of property',
                                 'readonly' => $readonly,
                                 'disabled' => $disabled,
                                'selected' => (isset($data['space_data']['Space'])) ? $data['space_data']['Space']['property_type_id'] : ''
                                )
                            );
                           //disabled and readonly for select will not post the data. therefore, we add a hidden input value with the 
                           //same name
                           if (!empty($disabled)) {
                            echo $this->Form->input('property_type_id', array(
                                'div' => false,
                                'label' => false,
                                'class' => 'form-control',
                                'id' => 'property_type_id',
                                'type' => 'hidden',
                                'value' => (isset($data['space_data']['Space'])) ? $data['space_data']['Space']['property_type_id'] : ''
                                )
                            );
                          }
                        ?>
                    </div>
                </div>
    			<div class="form-group">
    				<label for="address" class="col-sm-3 control-label">Address <span class='error'> *</span></label>
    				<div class="col-sm-8">
    					<?php
                    		echo $this->Form->input('flat_apartment_number', array(
                    			'div' => false,
                    			'label' => false,
                    			'id' => 'address-flat-no',
                    			'class' => 'form-control',
                    			'placeholder' => 'Flat/ Apartment Number',
                                         'readonly' => $readonly,
                                         'disabled' => $disabled,
                                'value' => (isset($data['space_data']['Space'])) ? $data['space_data']['Space']['flat_apartment_number'] : '',
                                )
                    		);
                    	?>
    				</div>
    			</div>

    			<div class="form-group">
    				<div class="col-sm-8 col-sm-offset-3">
    					<?php
                    		echo $this->Form->input('address1', array(
                    			'div' => false,
                    			'label' => false,
                    			'id' => 'address1',
                    			'class' => 'form-control',
                    			'placeholder' => 'Address Line 1',
                                        'readonly' => $readonly,
                                        'disabled' => $disabled,
                                'value' => (isset($data['space_data']['Space'])) ? $data['space_data']['Space']['address1'] : ''
                    			)
                    		);
                    	?>
    				</div>
    			</div>

                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                        <?php
                            echo $this->Form->input('address2', array(
                                'div' => false,
                                'label' => false,
                                'id' => 'address2',
                                'class' => 'form-control',
                                'placeholder' => 'Address Line 2',
                                'readonly' => $readonly,
                                'disabled' => $disabled,
                                'value' => (isset($data['space_data']['Space'])) ? $data['space_data']['Space']['address2'] : ''
                                )
                            );
                        ?>
                    </div>
                </div>

    			<div class="form-group">
    				<div class="col-sm-4 col-sm-offset-3">
    					<?php
                    		echo $this->Form->input('state_id', array(
                    			'div' => false,
                    			'label' => false,
                    			'options' => $data['state'],
                    			'id' => 'state',
                    			'class' => 'form-control',
                                        'readonly' => $readonly,
                                        'disabled' => $disabled,
                    			'empty' => 'State',
                    			'data-url' => $data['path'].'users/getCities',
                                'selected' => (isset($data['space_data']['Space'])) ? $data['space_data']['Space']['state_id'] : ''
                    			)
                    		);
                    	?>
    				</div>
    				<div class="col-sm-4" id= "list-city">
                        <?php
                            if(isset($data['space_data']['Space'])) {
                                if(!empty($data['space_data']['Space']['city_id'])) {
                                    echo $this->Form->input('city_id', array(
                                        'div' => false,
                                        'label' => false,
                                        'options' => (isset($data['city'])) ? $data['city'] : 'City',
                                        'id' => 'city',
                                        'class' => 'form-control',
                                        'empty' => 'City',
                                        'readonly' => $readonly,
                                        'disabled' => $disabled,
                                        'selected' => $data['space_data']['Space']['city_id']
                                        )
                                    );
                                }
                            }
                        ?>
    				</div>
    			</div>

    			<div class="form-group">
    				<div class="col-sm-4 col-sm-offset-3">
    					<?php
                    		echo $this->Form->input('post_code', array(
                    			'div' => false,
                    			'label' => false,
                    			'id' => 'pinCode',
                    			'class' => 'form-control',
                                        'readonly' => $readonly,
                                        'disabled' => $disabled,
                    			'placeholder' => 'Pin Code',
                                'type' => 'text',
                                'value' => (isset($data['space_data']['Space'])) ? $data['space_data']['Space']['post_code'] : ''
                    			)
                    		);
                    	?>
    				</div>
    			</div>
                <div class="form-group hide gated-tower">
                    <label for="address" class="col-sm-3 control-label"><?php echo __('Tower Number'); ?><span class='error'> *</span></label>
                    <div class="col-sm-4">
                        <?php
                            echo $this->Form->input('tower_number', array(
                                'div' => false,
                                'label' => false,
                                'class' => 'form-control',
                                'placeholder' => 'Gated Community Tower Number',
                                'readonly' => $readonly,
                                'disabled' => $disabled,
                                'value' => (isset($data['space_data']['Space'])) ? $data['space_data']['Space']['tower_number'] : ''
                                )
                            );
                        ?>
                    </div>
                </div>
				<?php
            		echo $this->Form->input('lat', array(
            			'div' => false,
            			'label' => false,
            			'id' => 'lat',
                        'type' => 'hidden',
            			'class' => 'form-control',
            			'placeholder' => 'Latitude',
                        'value' => (isset($data['space_data']['Space'])) ? $data['space_data']['Space']['lat'] : ''
            			)
            		);

            		echo $this->Form->input('lng', array(
            			'div' => false,
            			'label' => false,
            			'id' => 'long',
                        'type' => 'hidden',
            			'class' => 'form-control',
            			'placeholder' => 'Longitude',
                        'value' => (isset($data['space_data']['Space'])) ? $data['space_data']['Space']['lng'] : ''
            			)
            		);
            	?>
            </article>
            <?php /*
            <div class="form-group" id="deposit-price-input">
                <label for="deposit-price" class="col-sm-3 control-label">Select Months</label>
                <div class="col-sm-4">
                    <?php
                        echo $this->Form->input('deposit_for_months', array(
                            'div' => false,
                            'label' => false,
                            'options' => Configure::read('deposit_months'),
                            'id' => 'deposit-price',
                            'class' => 'form-control',
                            'empty' => 'Select Number of Months',
                            'selected' => (isset($data['space_data']['Space'])) ? $data['space_data']['Space']['deposit_for_months'] : ''
                            )
                        );
                    ?>
                </div>
                <div class="help-block col-sm-offset-3 col-xs-8">
                        <small class="text-info">
                            <i class="fa fa-exclamation-circle"></i>
                            <b>Select total month for advance deposite amount.</b>
                        </small>
                </div>
            </div>
            */ ?>
			<div class="form-group check-box-inline">
				<label for="amenties" class="col-sm-3 control-label">Amenties</label>
				<div class="col-sm-8">
                    <?php 
                        $facility_id_array = array();
                        if(isset($data['space_data']['SpaceFacility'])) {
                            foreach($data['space_data']['SpaceFacility'] as $facility_data) {
                                $facility_id_array[] = $facility_data['facility_id'];
                            }
                        }
                    ?>
                    <?php $i = 0; ?>
					<?php foreach($data['facilities'] as $key => $value) { ?>
						<div class="checkbox">
							<label>
								<?php 
                                    $checked = false;
                                    if(!empty($facility_id_array)) {
                                        if (in_array($key, $facility_id_array)) {
                                            $checked = true;
                                        }
                                    }

									echo $this->Form->checkbox('facility_id', array(
                                        'hiddenField' => false,
                                        'value' => $key,
                                        'name' => 'data[Space][facility_id]['.$i .']',
                                        'checked' => $checked
                                        )
                                    );
									echo $value;
                                    $i++;
								?>
							</label>
						</div>
					<?php } ?>
				</div>
			</div>

			<div class="form-group">
				<label for="address" class="col-sm-3 control-label">Available spaces<span class='error'> *</span></label>
				<div class="col-sm-8 form-inline input-margin adjust-checkbox">
                    <?php
                        echo $this->Form->input('number_slots', array(
                            'div' => false,
                            'label' => false,
                            'type' => 'text',
                            'class' => 'form-control',
                            'placeholder' => 'Number of slots',
                            'id' => 'total-slots',
                            'readonly' => $readonly,
                            'disabled' => $disabled,
                            'value' => (isset($data['space_data']['Space'])) ? $data['space_data']['Space']['number_slots'] : ''
                            )
                        );
                    ?>
                    <!-- <div class="checkbox">
                        <label>
                            <?php
                                /*$checked = true;
                                if(isset($data['space_data']['Space'])) {
                                    if(empty($data['space_data']['Space']['is_same_slots'])) {
                                        $checked = false;
                                    }
                                }
                                echo $this->Form->checkbox('is_same_slots', array(
                                    'hiddenField' => false,
                                    'id' => 'small-car-slot',
                                    'checked' => $checked
                                    )
                                );
                                echo 'Can Accommodate any kind of car (like hatchback, sedan, or SUV)';*/
                            ?>
                        </label>
                    </div> -->
                </div>
			</div>
            <!-- <div class="form-group" id="small-car-slot-input">
                <label for="address" class="col-sm-3 control-label">Available small car spaces</label>
                <div class="col-sm-8 input-margin form-inline">
                    <?php
                        /*echo $this->Form->input('small_car_slot', array(
                            'div' => false,
                            'label' => false,
                            'class' => 'form-control',
                            'placeholder' => 'Small car slots',
                            'id' => 'small-cars',
                            'value' => (isset($data['space_data']['SmallCarSpace'])) ? $data['space_data']['SmallCarSpace']['small_car_slot'] : ''
                            )
                        );*/
                    ?>
                    (for small cars only)
                </div>
            </div> -->

            <section class="gated-park hide">
                <div class="form-group">
                    <label for="address" class="col-sm-3 control-label"><?php echo __('Car Park Number'); ?><span class='error'> *</span></label>
                    <div class="col-sm-4">
                        <?php
                            echo $this->Form->input('SpacePark.0.park_number', array(
                                'div' => false,
                                'label' => false,
                                'class' => 'form-control',
                                'placeholder' => 'Gated Community Park Number',
                                'readonly' => $readonly,
                                'disabled' => $disabled,
                                'value' => !empty($data['space_data']['SpacePark']) ? $data['space_data']['SpacePark'][0]['park_number'] : ''
                                )
                            );
                            if (!empty($data['space_data']['SpacePark'])) {
                                echo $this->Form->hidden('SpacePark.0.id', array('value' => $data['space_data']['SpacePark'][0]['id']));
				if(!empty($disabled)) {
					echo $this->Form->input('SpacePark.0.park_number', array(
								'placeholder' => 'Gated Community Park Number',
                                                                'type'=> 'hidden',
								'value' => !empty($data['space_data']['SpacePark']) ? $data['space_data']['SpacePark'][0]['park_number'] : ''
								)
							);

				}
                
                                unset($data['space_data']['SpacePark'][0]);
                            }
                        ?>
                    </div>
                </div>
                <?php
                    if (!empty($data['space_data']['SpacePark'])) { 
                        $i = 1;
                        foreach($data['space_data']['SpacePark'] as $gatedSpaceParks) {
                ?>
                    <div class="form-group" id="<?php echo $i; ?>">
                        <div class="col-sm-4 col-sm-offset-3">
                            <?php
                                echo $this->Form->hidden('SpacePark.'.$i.'.id', array('value' => $gatedSpaceParks['id']));
                                echo $this->Form->input(
                                        'SpacePark.'.$i.'.park_number',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('Allocated Parking Number'),
                                                'div' => false,
                                                'label' => false,
                                                'readonly' => $readonly,
                                                'disabled' => $disabled,
                                                'value' => $gatedSpaceParks['park_number']
                                            )
                                    );
                                 if(!empty($disabled))
				 {
					 echo $this->Form->input(
							 'SpacePark.'.$i.'.park_number',
							 array(
                                                                 'type' => 'hidden',
								 'value' => $gatedSpaceParks['park_number']
							      )
							 );
				 }
        
                            ?>
                        </div>
                    </div>
                <?php $i++;
                        }
                    }
                ?>
            </section>

		</div>
		<div class="footer clearfix col-xs-12 border-top">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10 text-right">
					<?php
                        echo $this->Html->link('Cancel', 'parkingSpace', array(
                        	'class' => 'btn btn-default'));
                    ?>
                    <?php
                        echo $this->Form->button(__('Next'), array(
                                    'class' => 'btn btn-info',
                                    'id' => 'get-lat-long',
                                    'type' => 'submit'
                                    ));
                    ?>
				</div>
			</div>
		</div>
	<?php echo $this->Form->end(); ?>

</section>

<?php 
	echo $this->Html->script(
		array(
			'backend/Event/latlong',
		),
		array('inline' => false)
	);
?>

<script type="text/html" id="addCity">
	<select class='form-control' id = "city" name =data[Space][city_id]>
	    <option value="">City</option>
	    <% _.each(data, function(value, key){ %>
	        <option value="<%= key %>"><%= value %></option>
	    <% }); %>
    </select>
</script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
