<?php
if($data['space_data']['Space']['is_completed'] == 1 && $data['space_data']['Space']['is_approved'] == 1){
   echo "<script>
          var isEditAfterApproval = true;
        </script>";
} else {
   echo "<script>
          var isEditAfterApproval = false;
        </script>";
}

echo $this->Html->script(array('frontend/users/addSpace', 'frontend/Space/add_space_datetimepicker'), array('inline' => false));
?>
<section class="col-xs-12 col-md-9 right-panel">

	<div class="heading clearfix col-xs-12 border-bottom">
    <?php 
        $title = 'Add Space';
        $isGatedCommunity = 0;
        $readonly = '';
        $disabled = '';
        if(isset($data['space_data']['Space'])) {
            if($data['space_data']['Space']['is_completed'] == 1 && $data['space_data']['Space']['is_approved'] == 1){
    		  $title = 'Review Space';
                  $readonly = 'readonly';
                  $disabled = 'disabled';
            }
            else if($data['space_data']['Space']['is_completed'] == 1){
    		  $title = 'Edit Space';
            }

           if($data['space_data']['Space']['property_type_id'] == 2){
              $isGatedCommunity = 1;
           }
        }
    ?>
    <?php echo $this->Session->flash('reviewSpaceMessage'); ?>
    <span class="pull-left title"><?php echo $title; ?></span>
	</div>

	<?php
		echo $this->Form->create(
				'Space',
				array(
                    'url' => 'saveSpaceData',
					'method' => 'post',
					'class' => 'form-horizontal form-divide-group horizontal-form',
					'id' => 'add-space-details',
					'enctype' => 'multipart/form-data',
					'novalidate' => false
					)
			);
	?>
		<div class="col-xs-12 body">
			<article>
				
				<div class="form-group week-box-inline">
					<h4 class="col-sm-offset-3 col-sm-8"><?php echo __('Which days is your parking space available?'); ?></h4>
					<label for="amenties" class="col-sm-3 control-label"><?php echo __('Select Days'); ?></label>
					<div class="col-sm-8">
						<div class="checkbox">
							<label>
								<?php
				            		echo $this->Form->input('id', array(
				            			'div' => false,
				            			'label' => false,
				            			'class' => 'form-control',
				            			'type' => 'hidden',
				            			'value' => $data['space_id'],
				            			'id' => 'space-id'
				            			)
				            		);

				            		echo $this->Form->input('is_completed', array(
				            			'div' => false,
				            			'label' => false,
				            			'class' => 'form-control',
				            			'type' => 'hidden',
				            			'value' => Configure::read('Bollean.True')
				            			)
				            		);
 

				            		if(isset($data['space_data']['SpaceTimeDay']) && !empty($data['space_data']['SpaceTimeDay'])) {
					            		echo $this->Form->input('SpaceTimeDay.id', array(
			                                'div' => false,
			                                'label' => false,
			                                'type' => 'hidden',
			                                'value' => $data['space_data']['SpaceTimeDay']['id']
			                                )
			                            );
				            		}

				            		if(isset($data['space_data']['SpacePricing'][0]) && !empty($data['space_data']['SpacePricing'][0])) {
					            		echo $this->Form->input('SpacePricing.id', array(
			                                'div' => false,
			                                'label' => false,
			                                'type' => 'hidden',
			                                'value' => $data['space_data']['SpacePricing'][0]['id']
			                                )
			                            );
				            		}

				            		echo $this->Form->hidden(
				            				'property_type',
				            				array(
				            						'value' => $data['space_data']['Space']['property_type_id'],
				            						'id' => 'property_type_id'
				            					)
				            			);

				            		                $checked = false;
                                                                        $value = 0;
									if(isset($data['space_data']['SpaceTimeDay'])) {
										if($data['space_data']['SpaceTimeDay']['mon'] == 1) {
											$checked = true;
										}
									}

									echo $this->Form->checkbox('SpaceTimeDay.mon', array(
															'hiddenField' => false,
															'checked'=> $checked,
															'class' => 'wdays check-days',
                                                                                                                        'disabled' => $disabled
																));

                                                                        if ($isGatedCommunity) {
                                                                           $value = 1;
                                                                           echo $this->Form->input('SpaceTimeDay.mon', array(
                                                                                                                         'type' => 'hidden',
                                                                                                                          'value' => $value
                                                                                                                           )
                                                                                                  );
                                                                        } else if (!empty($disabled)) {
                                                                           echo $this->Form->input('SpaceTimeDay.mon', array(
                                                                                                                          'type' => 'hidden',
                                                                                                                           'value' => $checked
                                                                                                                            )
                                                                                                   );
                                                                        }
									echo __('Monday');
								?>
								
							</label>
						</div>
						<div class="checkbox">
							<label>
								<?php 
									$checked = false;
                                                                        $value = 0;
									if(isset($data['space_data']['SpaceTimeDay'])) {
										if($data['space_data']['SpaceTimeDay']['tue'] == 1) {
											$checked = true;
										}
									}

									echo $this->Form->checkbox('SpaceTimeDay.tue', array(
															'hiddenField' => false,
															'checked'=> $checked,
														         'class' => 'wdays check-days',
                                                                                                                        'disabled'=>$disabled
																));
                                                                        if ($isGatedCommunity) {
                                                                           $value = 1;
                                                                           echo $this->Form->input('SpaceTimeDay.tue', array(
                                                                                                                         'type' => 'hidden',
                                                                                                                          'value' => $value
                                                                                                                           )
                                                                                                   );
                                                                        } else if (!empty($disabled)) {
                                                                           echo $this->Form->input('SpaceTimeDay.tue', array(
                                                                                                                          'type' => 'hidden',
                                                                                                                           'value' => $checked
                                                                                                                            )
                                                                                                   );
                                                                        }
									echo __('Tuesday');
								?>
							</label>
						</div>
						<div class="checkbox">
							<label>
								<?php
									$checked = false;
                                                                        $value = 0;
									if(isset($data['space_data']['SpaceTimeDay'])) {
										if($data['space_data']['SpaceTimeDay']['wed'] == 1) {
											$checked = true;
										}
									}

									echo $this->Form->checkbox('SpaceTimeDay.wed', array(
															'hiddenField' => false,
															'checked'=> $checked,
															'class' => 'wdays check-days',
                                                                                                                        'disabled'=>$disabled
																));
                                                                        if ($isGatedCommunity) {
                                                                           $value = 1;
                                                                           echo $this->Form->input('SpaceTimeDay.wed', array(
                                                                                                                         'type' => 'hidden',
                                                                                                                          'value' => $value
                                                                                                                           )
                                                                                                   );
                                                                        } else if (!empty($disabled)) {
                                                                           echo $this->Form->input('SpaceTimeDay.wed', array(
                                                                                                                          'type' => 'hidden',
                                                                                                                           'value' => $checked
                                                                                                                            )
                                                                                                   );
                                                                        }
									echo __('Wednesday');
								?>
							</label>
						</div>
						<div class="checkbox">
							<label>
								<?php
									$checked = false;
                                                                        $value = 0;
									if(isset($data['space_data']['SpaceTimeDay'])) {
										if($data['space_data']['SpaceTimeDay']['thu'] == 1) {
											$checked = true;
										}
									}

									echo $this->Form->checkbox('SpaceTimeDay.thu', array(
															'hiddenField' => false,
															'checked'=> $checked,
															'class' => 'wdays check-days',
                                                                                                                        'disabled'=>$disabled
                                                                                                                         		
																));
                                                                        if ($isGatedCommunity) {
                                                                           $value = 1;
                                                                           echo $this->Form->input('SpaceTimeDay.thu', array(
                                                                                                                         'type' => 'hidden',
                                                                                                                          'value' => $value
                                                                                                                           )
                                                                                                   );
                                                                        } else if (!empty($disabled)) {
                                                                           echo $this->Form->input('SpaceTimeDay.thu', array(
                                                                                                                          'type' => 'hidden',
                                                                                                                           'value' => $checked
                                                                                                                            )
                                                                                                   );
                                                                        }
									echo __('Thursday');
								?>
							</label>
						</div>
						<div class="checkbox">
							<label>
								<?php
									$checked = false;
                                                                        $value = 0;
									if(isset($data['space_data']['SpaceTimeDay'])) {
										if($data['space_data']['SpaceTimeDay']['fri'] == 1) {
											$checked = true;
										}
									}

									echo $this->Form->checkbox('SpaceTimeDay.fri', array(
															'hiddenField' => false,
														        'checked'=> $checked,
															'class' => 'wdays check-days',
                                                                                                                        'disabled'=>$disabled
																));
                                                                        if ($isGatedCommunity) {
                                                                           $value = 1;
                                                                           echo $this->Form->input('SpaceTimeDay.fri', array(
                                                                                                                          'type' => 'hidden',
                                                                                                                           'value' => $value
                                                                                                                            )
                                                                                                   );
                                                                        } else if (!empty($disabled)) {
                                                                           echo $this->Form->input('SpaceTimeDay.fri', array(
                                                                                                                          'type' => 'hidden',
                                                                                                                           'value' => $checked
                                                                                                                            )
                                                                                                   );
                                                                        }
									echo __('Friday');
								?>
							</label>
						</div>
						<div class="checkbox">
							<label>
								<?php
									$checked = false;
                                                                        $value = 0;
									if(isset($data['space_data']['SpaceTimeDay'])) {
										if($data['space_data']['SpaceTimeDay']['sat'] == 1) {
											$checked = true;
										}
									}

									echo $this->Form->checkbox('SpaceTimeDay.sat', array(
															'hiddenField' => false,
															'checked'=> $checked,
															'class' => 'wend check-days',
                                                                                                                        'disabled'=>$disabled
																));
                                                                        if ($isGatedCommunity) {
                                                                           $value = 1;
                                                                           echo $this->Form->input('SpaceTimeDay.sat', array(
                                                                                                                          'type' => 'hidden',
                                                                                                                           'value' => $value
                                                                                                                            )
                                                                                                   );
                                                                        } else if (!empty($disabled)) {
                                                                           echo $this->Form->input('SpaceTimeDay.sat', array(
                                                                                                                          'type' => 'hidden',
                                                                                                                           'value' => $checked
                                                                                                                            )
                                                                                                   );
                                                                        }
									echo __('Saturday');
								?>
							</label>
						</div>
						<div class="checkbox">
							<label>
								<?php
									$checked = false;
                                                                        $value = 0;
									if(isset($data['space_data']['SpaceTimeDay'])) {
										if($data['space_data']['SpaceTimeDay']['sun'] == 1) {
											$checked = true;
										}
									}

									echo $this->Form->checkbox('SpaceTimeDay.sun', array(
															'hiddenField' => false,
															'checked'=> $checked,
														        'class' => 'wend check-days',
                                                                                                                        'disabled'=>$disabled
																));
                                                                        if ($isGatedCommunity) {
                                                                           $value = 1;
                                                                           echo $this->Form->input('SpaceTimeDay.sun', array(
                                                                                                                          'type' => 'hidden',
                                                                                                                           'value' => $value
                                                                                                                            )
                                                                                                   );
                                                                        }
                                                                        else if (!empty($disabled)) {
                                                                           echo $this->Form->input('SpaceTimeDay.sun', array(
                                                                                                                          'type' => 'hidden',
                                                                                                                           'value' => $checked
                                                                                                                            )
                                                                                                   );
                                                                        }
									echo __('Sunday');
								?>
							</label>
						</div>
						<div class="checkbox">
							<label>
								<?php 
									echo $this->Form->checkbox('SpaceTimeDay.weekdays', array(
																'hiddenField' => false,
																'checked'=> false,
																'class' => 'weekdays',
                                                                                                                                'disabled'=>$disabled
																));
									echo __('Only weekdays');
								?>
							</label>
						</div>
						<div class="checkbox">
							<label>
								<?php 
									echo $this->Form->checkbox('SpaceTimeDay.weekends', array(
															       'hiddenField' => false,
															       'checked'=> false,
															       'class' => 'weekends',
                                                                                                                                'disabled'=>$disabled
																));
									echo __('Only weekends');
								?>
							</label>
						</div>
						<div class="checkbox">
							<label>
								<?php 
									echo $this->Form->checkbox('SpaceTimeDay.whole_week', array(
																'hiddenField' => false,
																'checked'=> false,
																'class' => 'whole-week',
                                                                                                                                'disabled'=>$disabled
																));
									echo __('Whole week');
								?>
							</label>
						</div>
					</div>
				</div>
			</article>
			<div class="form-group">
				<h4 class="col-sm-offset-3 col-sm-8"><?php echo __('When is your parking available?'); ?></h4>
				<label for="amenties" class="col-sm-3 control-label"><?php echo __('From'); ?></label>
				<div class="col-sm-8">
					<div class="form-group">
		                <div class='input-group date' id='date1' data-date-format="mm/dd/yyyy hh:ii">
							<?php
		                		echo $this->Form->input('SpaceTimeDay.from_date', array(
		                			'div' => false,
		                			'label' => false,
		                			'id' => 'start-date',
		                			'type' => 'text',
		                			'class' => 'form-control',
		                			'placeholder' => 'Date',
                                                        'disabled' => $disabled,
		                			'value' => (isset($data['space_data']['SpaceTimeDay']) && !empty($data['space_data']['SpaceTimeDay']['from_date'])) ? date('Y-m-d',strtotime($data['space_data']['SpaceTimeDay']['from_date'])) : ''
		                			)
		                		);
                                               if (!empty($disabled)) {
		                		echo $this->Form->input('SpaceTimeDay.from_date', array(
		                			'id' => 'start-date',
		                			'type' => 'hidden',
		                			'value' => (isset($data['space_data']['SpaceTimeDay']) && !empty($data['space_data']['SpaceTimeDay']['from_date'])) ? date('Y-m-d',strtotime($data['space_data']['SpaceTimeDay']['from_date'])) : ''
		                			)
		                		);
                                              }
		                	?>
		                	<span class="input-group-addon">
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>
		            </div>
				</div>
			</div>
			<div class="form-group">
				<label for="amenties" class="col-sm-3 control-label"><?php echo __('Until'); ?></label>
				<div class="col-sm-8">
					<div class="form-group">
		                <div class='input-group date' id='date2' data-date-format="mm/dd/yyyy hh:ii">
							<?php
		                		echo $this->Form->input('SpaceTimeDay.to_date', array(
		                			'div' => false,
		                			'label' => false,
		                			'id' => 'end-date',
		                			'class' => 'form-control',
		                			'type' => 'text',
		                			'placeholder' => 'Date',
                                                        'disabled' => $disabled,
		                			'value' => (isset($data['space_data']['SpaceTimeDay']) && !empty($data['space_data']['SpaceTimeDay']['to_date'])) ? date('Y-m-d',strtotime($data['space_data']['SpaceTimeDay']['to_date'])) : ''
		                			)
		                		);

                                              if (!empty($disabled)) {
		                		echo $this->Form->input('SpaceTimeDay.to_date', array(
		                			'id' => 'end-date',
		                			'type' => 'hidden',
		                			'value' => (isset($data['space_data']['SpaceTimeDay']) && !empty($data['space_data']['SpaceTimeDay']['to_date'])) ? date('Y-m-d',strtotime($data['space_data']['SpaceTimeDay']['to_date'])) : ''
		                			)
		                		);
  
                                              }
		                	?>
		                    <span class="input-group-addon">
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>
		            </div>
				</div>
				<h5 class="col-sm-offset-3 col-sm-8 dates-error text-danger hide"></h5>
                                <h5 class="col-sm-offset-3 col-sm-9 end-date-info text-info hide"></h5>
			</div>

			<div class="form-group">
				<h4 class="col-sm-offset-3 col-sm-8"><?php echo __('Please Choose your operating hours :'); ?></h4>
				<label for="amenties" class="col-sm-3 control-label"></label>
				<div class="col-sm-8">
					<div class="checkbox">
						<label>
		                	<?php
								$allHours = false;
                                                                $value = 0;
								if($isGatedCommunity /*&& !$data['space_data']['Space']['is_completed']*/) {
									$allHours = true;
								} elseif($data['space_data']['SpaceTimeDay']['open_all_hours']) {
									$allHours = true;
								}

								echo $this->Form->checkbox('SpaceTimeDay.open_all_hours', array(
																'hiddenField' => false,
																'checked'=> $allHours,
																'class' => '24-hours-open',
                                                                                                                                'disabled'=>$disabled
															));

                                                                if ($isGatedCommunity) {
                                                                     $value = 1;
                                                                     echo $this->Form->input('SpaceTimeDay.open_all_hours', array(
                                                                                                                           'type' => 'hidden',
                                                                                                                           'value' => $value
                                                                                                                           )
                                                                                                   );
                                                                 } else if (!empty($disabled)) {
								    echo $this->Form->input('SpaceTimeDay.open_all_hours', array(
																'value'=> $allHours,
                                                                                                                                'type' => 'hidden'
															));
                                                                }
                                                                
								echo __('Open 24 Hours');
							?>
						</label>
		            </div>
				</div>
			</div>

			<div class="form-group">
				<label for="amenties" class="col-sm-3 control-label"><?php echo __('From'); ?></label>
				<div class="col-sm-8">
					<div class="form-group">
		                <div class='input-group date' id='time1' data-date-format="mm/dd/yyyy hh:ii">
							<?php
		                		echo $this->Form->input('SpaceTimeDay.from_time', array(
		                			'div' => false,
		                			'label' => false,
		                			'id' => 'start-time',
		                			'type' => 'text',
		                			'class' => 'form-control',
		                			'placeholder' => 'Time',
                                                        'disabled' => $disabled,
		                			'value' => (isset($data['space_data']['SpaceTimeDay']) && !empty($data['space_data']['SpaceTimeDay']['from_date'])) ? date('H:i', strtotime($data['space_data']['SpaceTimeDay']['from_date'])) : null,
		                			)
		                		);

                                               if(!empty($disabled)) {
		                		echo $this->Form->input('SpaceTimeDay.from_time', array(
		                			'id' => 'start-time',
		                			'type' => 'hidden',
		                			'value' => (isset($data['space_data']['SpaceTimeDay']) && !empty($data['space_data']['SpaceTimeDay']['from_date'])) ? date('H:i', strtotime($data['space_data']['SpaceTimeDay']['from_date'])) : null,
		                			)
		                		     );
                                               }
		                	?>
		                	<span class="input-group-addon">
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>
		            </div>
				</div>
			</div>
			<div class="form-group">
				<label for="amenties" class="col-sm-3 control-label"><?php echo __('To'); ?></label>
				<div class="col-sm-8">
					<div class="form-group">
		                <div class='input-group date' id='time2' data-date-format="mm/dd/yyyy hh:ii">
							<?php
		                		echo $this->Form->input('SpaceTimeDay.to_time', array(
		                			'div' => false,
		                			'label' => false,
		                			'id' => 'end-time',
		                			'class' => 'form-control',
		                			'type' => 'text',
		                			'placeholder' => 'Time',
                                                        'disabled'=>$disabled,
		                			'value' => (isset($data['space_data']['SpaceTimeDay']) && !empty($data['space_data']['SpaceTimeDay']['to_date'])) ? date('H:i', strtotime($data['space_data']['SpaceTimeDay']['to_date'])) : null,
		                			)
		                		);

                                               if(!empty($disabled))
                                               {
		                		   echo $this->Form->input('SpaceTimeDay.to_time', array(
		                			                   'id' => 'end-time',
		                			                   'type' => 'hidden',
		                			                   'value' => (isset($data['space_data']['SpaceTimeDay']) && !empty($data['space_data']['SpaceTimeDay']['to_date'])) ? date('H:i', strtotime($data['space_data']['SpaceTimeDay']['to_date'])) : null,
		                			)
		                		    );
                                              }
		                	?>
		                    <span class="input-group-addon">
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>
		            </div>
				</div>
			</div>

			<?php
				$priceClass = !$data['space_data']['Space']['is_completed'] ? 'hide' : 'show';
			?>

			<div class="form-group price <?php echo $priceClass; ?>">
                         <?php
                            if($data['space_data']['Space']['property_type_id'] != Configure::read('PropertyType.PrivateResidence') && 
                               $data['space_data']['Space']['property_type_id'] != Configure::read('PropertyType.GatedCommunity'))
                             {
			         echo "<div class=\"col-sm-offset-3 col-xs-8\" id=\"tierPricingGroup\">";
                                 
                                 $isChecked = false;
                                 $displayState = 'hide';

                                 if(isset($data['space_data']['Space']['tier_pricing_support']) && $data['space_data']['Space']['tier_pricing_support'] == 1){
                                    $isChecked = true;
                                    $displayState = 'show';
                                 }
 
                                 echo $this->Form->checkbox('tier_pricing_support',array( 
                                                                                'checked' => $isChecked
                                                                                ));
                                                                             
                                 echo  __(' Hourly pricing is Tiered (example Up to 4 Hrs - Rs. 10, 4-8 hours - Rs 20 etc)');
                                 echo "</div>";
			         
                                 echo "<div class=\"col-sm-offset-3 col-xs-8 $displayState\" id=\"showTierPricingTextArea\">";
                                    echo $this->Form->input('tier_desc', array(
                                                                  'div' => false,
                                                                  'id' => 'tier_desc',
                                                                  'label' => false,
                                                                  'type' => 'textarea',
                                                                  'class' => 'form-control',
                                                                  'placeholder' => 'Please enter your hourly pricing. Example Upto 4 hours : Rs 10, 4-8 hours: Rs. 20 and so on...',
                                                                  'value' => (isset($data['space_data']['Space']['tier_desc']))? $data['space_data']['Space']['tier_desc']:''

                                                                  )
                                                            );
                                 echo "<br /></div>";

                             }
                         ?>
			<label for="address" class="col-sm-3 control-label">Price <span class='error'> *</span></label>
                <div class="col-sm-2 hourly-input">
                    <?php
                        echo $this->Form->input('SpacePricing.hourly', array(
                        	'type' => 'text',
                            'div' => false,
                            'label' => false,
                            'id' => 'hourly',
                            'class' => 'form-control',
                            'placeholder' => 'Hourly',
                            'value' => (isset($data['space_data']['SpacePricing'][0])) ? $data['space_data']['SpacePricing'][0]['hourly'] : ''
                            )
                        );
                    ?>
                </div>
				<div class="col-sm-2 daily-input">
					<?php
                		echo $this->Form->input('SpacePricing.daily', array(
                			'type' => 'text',
                			'div' => false,
                			'label' => false,
                			'id' => 'daily',
                			'class' => 'form-control',
                			'placeholder' => 'Daily',
                            'value' => (isset($data['space_data']['SpacePricing'][0])) ? $data['space_data']['SpacePricing'][0]['daily'] : ''
                			)
                		);
                	?>
				</div>
                <div class="col-sm-2 weekly-input">
                    <?php
                        echo $this->Form->input('SpacePricing.weekely', array(
                        	'type' => 'text',
                            'div' => false,
                            'label' => false,
                            'id' => 'weekely',
                            'class' => 'form-control',
                            'placeholder' => 'Weekely',
                            'value' => (isset($data['space_data']['SpacePricing'][0])) ? $data['space_data']['SpacePricing'][0]['weekely'] : ''
                            )
                        );
                    ?>
                </div>
				<div class="col-sm-2">
					<?php
                		echo $this->Form->input('SpacePricing.monthly', array(
                			'type' => 'text',
                			'div' => false,
                			'label' => false,
                			'id' => 'monthly',
                			'class' => 'form-control',
                			'placeholder' => 'Monthly',
                            'value' => (isset($data['space_data']['SpacePricing'][0])) ? $data['space_data']['SpacePricing'][0]['monthly'] : ''
                			)
                		);
                	?>
				</div>
				<div class="col-sm-2">
					<?php
                		echo $this->Form->input('SpacePricing.yearly', array(
                			'type' => 'text',
                			'div' => false,
                			'label' => false,
                			'id' => 'yearly',
                			'class' => 'form-control',
                			'placeholder' => 'Yearly',
                            'value' => (isset($data['space_data']['SpacePricing'][0])) ? $data['space_data']['SpacePricing'][0]['yearly'] : ''
                			)
                		);
                	?>
				</div>
				<div class="help-block col-sm-offset-3 col-xs-8" id="price-text">
                    <small class="text-info price-text-size">
                        <i class="fa fa-exclamation-circle"></i>
                        <b>Please choose the <span class="price-basis"></span> price (atleast one) that you wish to charge for using your parking space. Please specify atleast one price.  However, You can leave a price field blank. For example, leave <span class="price-single-cap"></span> field blank if you do not want to rent out your space on a <span class="price-single"></span> basis</b>
                    </small>
                </div>
            </div>

                          <?php
			$checked = true;
		        ?>
		<div class="col-sm-3 text-center">
			<?php 
        		echo $this->Form->input('Space.is_activated', array(
                                                                    'label' => false,
                                                                    'div' => false,
                                                                    'type' => 'hidden',
                                                                    'value' => $checked
                                                                  )
                                       );
                                       
			?>

		</div>

		
		<div class="footer clearfix col-xs-12 border-top">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10 text-right">
					<?php
                        echo $this->Html->link('Cancel', 'parkingSpace', array(
                        		'class' => 'btn btn-default'
                        	));
                    ?>
                    <?php
                        echo $this->Form->button(__('Next'), array(
                                    'class' => 'btn btn-info',
                                    'id' => 'submit-space-details',
                                    'type' => 'submit'
                                ));
                    ?>
				</div>
			</div>
		</div>							
		
	<?php echo $this->Form->end(); ?>

</section>
<?php
	echo $this->element('frontend/Space/price_alert');
?>
