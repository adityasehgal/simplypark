<?php
	echo $this->Html->script('frontend/users/addSpace', array('inline' => false));
	if($data['space_data']['Space']['is_completed'] == 1 && $data['space_data']['Space']['is_approved'] == 1){
              echo "<script>
                      var isMapDraggable = false;
                    </script>";
        }else {
              echo "<script>
                      var isMapDraggable = true;
                    </script>";
        } 
?>
<section class="col-xs-12 col-md-9 right-panel">
	
	<div class="heading clearfix col-xs-12 border-bottom">
    <?php 
        $title = 'Add Space';
        $readonly = '';
        $disabled = '';
        if(isset($data['space_data']['Space'])) {
            if($data['space_data']['Space']['is_completed'] == 1 && $data['space_data']['Space']['is_approved'] == 1){
    		  $title = 'Review Space';
                  $readonly = 'readonly';
                  $disabled = 'disabled';
            }
            else if($data['space_data']['Space']['is_completed'] == 1){
    		  $title = 'Edit Space';
            }
        }
    ?>
    <span class="pull-left title"><?php echo $title; ?></span>
	</div>
	<?php
		echo $this->Form->create(
				'Space',
				array(
                    'url' => 'addSpaceDetailsSave',
					'method' => 'post',
					'class' => 'form-horizontal',
					'id' => 'add-space-policy',
					'enctype' => 'multipart/form-data',
					'novalidate' => false
					)
			);
	?>
		<div class="col-xs-12 body">
			<!-- <div class="help-block">
				<small class="text-info">
					<i class="fa fa-exclamation-circle"></i>
					<b>Price that you to charge for parking</b>
				</small>
			</div> -->
			<p class="help-block">
				<span class="text-info">
					<?php if(empty($readonly)) {echo __('If the Pin is not at its correct place in the map, please help us and drag the pin to the correct location. 
  Don\'t sweat if you cannot find the correct location to pin to. Our super friendly support team will fix the pin automatically for you.');}
                                              else {
                                                      echo __('If you feel the Pin is not at its correct place in the map, get in touch with us customercare@simplypark.in');
                                                }

                                        ?>
                                              
				</span>
			</p>
                        <?php 
		        echo "<div class=map id=location style=height:340px;>";
		        echo "</div>";
                       
                        ?> 
			<div class="form-group">
				<?php
            		echo $this->Form->input('id', array(
            			'div' => false,
            			'label' => false,
            			'class' => 'form-control',
            			'type' => 'hidden',
            			'value' => $data['space_id']
            			)
            		);

            		echo $this->Form->input('lat', array(
            			'div' => false,
            			'label' => false,
            			'id' => 'latitude',
            			'class' => 'form-control',
            			'type' => 'hidden',
            			'value' => $data['lat']
            			)
            		);

            		echo $this->Form->input('long', array(
            			'div' => false,
            			'label' => false,
            			'id' => 'longitude',
            			'class' => 'form-control',
            			'type' => 'hidden',
            			'value' => $data['long']
            			)
            		);
            	?>
			</div>

			<div class="form-group check-box-inline">
				<label for="amenties" class="col-sm-3 control-label"><?php echo __('Cancellation Policy'); ?></label>
				<div class="col-sm-8 match-label-radio">
					<div class="radio">
						<?php
							$first_key = key($data['policies']);

							if(isset($data['space_data']['Space'])) {
								if(!empty($data['space_data']['Space']['cancellation_policy_id'])) {
									$first_key = $data['space_data']['Space']['cancellation_policy_id'];
								}
							}

							$attributes = array(
									'legend' => false,
									'default' => $first_key,
								);
							//echo $this->Form->radio('cancellation_policy_id', $data['policies'], $attributes);
							echo $this->form->input('cancellation_policy_id', array(
							    'separator' => '<br>',
							    'options' => $data['policies'],
							    'type' => 'radio',
							    'legend' => false,
								'default' => $first_key
							));
						?>
					</div>
					<?php echo __('For more information about how we process cancellations and how they offer additional protection to both drivers and property owners, please review our ');
                                              echo $this->Html->link('Cancellation Policy', 
                                                                     'https://www.simplypark.in/homes/cms/OA%3D%3D',
                                                                      array('target' => '_blank')
                                                                     );
                                        ?>

				</div>
			</div>

			<div class="form-group check-box-inline">
				<label for="amenties" class="col-sm-3 control-label"><?php echo __('Booking Confirmation'); ?></label>
				<div class="col-sm-8 match-label">
					<?php echo __('How will you like to accept the bookings?'); ?>
					<div>
						<?php
							$first_key = 1;
							
							if (isset($data['space_data']['Space']) && !is_null($data['space_data']['Space']['booking_confirmation']) && $data['space_data']['Space']['booking_confirmation'] != 1) {
								$first_key = 0;
							}

							$attributes = array(
									'legend' => false,
									'default' => $first_key
								);
							echo $this->form->input('Space.booking_confirmation', array(
							    'separator' => '<br>',
							    'options' => $data['booking_confirmation'],
							    'type' => 'radio',
							    'legend' => false,
								'default' => $first_key,
								'class' => 'how-confirm'
							));
						?>
					</div>
					<div class="<?php echo $first_key == 1 ? 'hide' : ''; ?> manually-confirm-note">
						<?php echo __('For every booking request received, you will have to confirm the booking within 24 hours.'); ?>
					</div>
				</div>
			</div>

			<div class="form-group border-top">
				<div class="col-sm-11">
					<div class="checkbox">
						<label>
							<?php 
								$checked = false;
								if(isset($data['space_data']['Space'])) {
									if($data['space_data']['Space']['sign_agreement'] == 1){
										$checked = true;
									}
								}
								echo $this->Form->checkbox('sign_agreement', array(
																'hiddenField' => false,
																'checked'=> $checked
															)
								); 
							?>
							<?php echo __('For yearly booking, sign a parking rental agreement with parking user.'); ?>
						</label>
					</div>
					<div class="checkbox term-condition">
						<label>
							<?php echo $this->Form->checkbox('TandC', array('hiddenField' => false, 'checked'=>true)); ?>
							<?php echo __('I agree to SimplyPark\'s terms and conditions and confirm that I have the legal right to list the parking'); ?>
						</label>
					</div>									
				</div>
			</div>
		</div>
		<div class="footer clearfix col-xs-12 border-top">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10 text-right">
					<?php
                        echo $this->Html->link('Cancel', 'parkingSpace', array(
                        	'class' => 'btn btn-default'));
                    ?>
                    <?php
                        echo $this->Form->button(__('Next'), array(
                                    'class' => 'btn btn-info',
                                    'type' => 'submit'
                                    ));
                    ?>
				</div>
			</div>
		</div>
	<?php echo $this->Form->end(); ?>
</section>
<?php 
	echo $this->Html->script(
		array(
			'frontend/users/Map',
		),
		array('inline' => false)
	);
?>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
