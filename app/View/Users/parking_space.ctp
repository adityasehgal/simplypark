<?php
	echo $this->Html->script('frontend/users/addSpace', array('inline' => false));
?>
<section class="col-xs-12 col-md-9 right-panel">
	
	<div class="heading clearfix col-xs-12">
		<span class="pull-left title">Parking Space</span>
		<?php
            echo $this->Html->link('Add space', 'addSpace', array(
        	'class' => 'pull-right btn btn-info text-uppercase pull-right'));
        ?>
	</div>

	<div class="col-xs-12 parking-space">
		<?php //echo $this->Session->flash('spaceMessage'); ?>
		<?php if(empty($data)) { ?>
			<div class="alert alert-info text-center" role="alert">
				You currently have no parking spaces added. 
				Click on `add space` to list your space on SimplyPark
		   </div>
		<?php } else { ?>
			<?php 
			$path = $data['path']; unset($data['path']);
			foreach($data as $space_data) { 
			?>
				<div class="well white-bg clearfix">
			
					<div class="row">
						<div class="col-xs-12 col-md-9">
							<h4>
								<?php echo $space_data['Space']['name']; ?>
								<small>(<?php echo $space_data['Space']['number_slots'].' slots'; ?>)</small>
								<?php if(empty($space_data['Space']['is_approved']) || 
											$space_data['Space']['is_approved'] == 0) { ?>
									<span class="fa fa-clock-o"></span>
									<small>under verification by SimplyPark</small>
								<?php } ?>

								<?php if($space_data['Space']['is_approved'] == 1) { ?>
									<span class="fa fa fa-check-circle text-info"></span>
									<small>Verified by SimplyPark</small>
								<?php } ?>

								<?php if($space_data['Space']['is_approved'] == 2) { ?>
									<span class="fa fa-times-circle text-danger"></span>
										<small>Parking space not approved</small>
								<?php } ?>
							</h4>

							<?php if(!empty($space_data['SpaceDisapprovalReason']['reason']) && 
									$space_data['Space']['is_approved'] == 2) { ?>
								<div class="alert alert-danger alert-sm" role="alert">
									<strong>Reason: </strong>
									<?php echo $space_data['SpaceDisapprovalReason']['reason']; ?>
								</div>
							<?php } ?>

							<p>
								<span class="col-table">
									<a href="#">
										<i class="fa fa-map-marker fa-fw"></i>
									</a>
									<?php
					            		// echo $this->Form->input('space_id', array(
					            		// 	'div' => false,
					            		// 	'label' => false,
					            		// 	'class' => 'form-control',
					            		// 	'type' => 'hidden',
					            		// 	'value' => $data['id'],
					            		// 	'data-serverUrl' = $path
					            		// 	)
					            		// );
				            		?>
									<small class="col-cell">
										<?php 
											$address = $space_data['Space']['flat_apartment_number'].', ';
											$address .= $space_data['Space']['address1'].', '.$space_data['Space']['address2'].', '.$space_data['City']['name'].' ';
											$address .= $space_data['Space']['post_code'].', '.$space_data['State']['name'];

											echo $address;
										?>
									</small>
								</span>
							</p>
							<ul class="list-inline action">
			                    <li>
			                    	<?php
			                    		if($space_data['Space']['is_approved'] != 1) {
								            echo $this->Html->link('Edit',
								            	$path.'users/addSpace/'.base64_encode($space_data['Space']['id']), 
								            	array(
								            	'class' => 'text-primary'));
							 }else if($space_data['Space']['is_approved'] == 1) {
								            echo $this->Html->link('Review',
								            	$path.'users/addSpace/'.base64_encode($space_data['Space']['id']), 
								            	array(
								            	'class' => 'text-primary'));
                                                         }
							        ?>
			                    </li>
			                    <li>
			                    	<?php
			                    		if($space_data['Space']['is_activated'] != 1) {
								            echo $this->Html->link('Delete',
								            	'#',
								            	array(
								            		'data-toggle' => 'modal',
			                                        'data-target' => '#deleteConfirmModal',
			                                        'escape' => false,
			                                        'data-url' => $path.'users/setIsDeleted/'.base64_encode($space_data['Space']['id']).'/Space',
								            		'class' => 'text-danger delete-popup',
								            	)
								            );
							        	}
							        ?>
			                    </li>
			                </ul>
						</div>

						<div class="col-xs-12 col-md-3 text-center activate-space">
							<span>Parking space activated</span>
							<?php $checked = ($space_data['Space']['is_activated']) ? true: false;?>
							<?php echo $this->Form->checkbox('is_activated', array(
														'hiddenField' => false,
														'checked'=> $checked,
														'data-on-color' => 'success',
														'class' => 'activate-checkbox',
														'data-serverUrl' => Configure::read('ROOTURL').'users/setIsActivated/'.base64_encode($space_data['Space']['id']).'/Space'
													));
							?>
						</div>
					</div>

				</div>
			<?php } ?>
		<?php } ?>
	</div>

</section>
