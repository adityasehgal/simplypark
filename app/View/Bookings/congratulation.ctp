<div class="main-container">
    <div class="contact-bg">
        <div class="container">
            <section class="congratulation-section">
                <h2><?php echo __('Congratulations'); ?></h2>
                <?php if (!$getBookingRecord['Booking'][Configure::read('BookingTableStatusField')]) { ?>
                        <p>
                            <?php echo __('Your booking is pending approval. Once confirmed, an email would be sent to'); ?><strong> <?php echo $getBookingRecord['Booking']['email']; ?></strong>
                            <small><?php echo __('<br />(Your credit/debit card is charged but we will automatically refund you in case the owner disapproves your booking request)');?></small>
                        </p>
                <?php } elseif($getBookingRecord['Booking'][Configure::read('BookingTableStatusField')] == Configure::read('SaveBookingStatus.Approved')) { ?>
                        <p> <?php echo __('Your booking is approved. Please carry a printout of the confirmation email and upon arrival give the simplypark parking slip to the attendant.'); 
                            ?>
                        <br /> <?php echo __('You can also print your booking pass from the Booking Section.'); 
                            ?>
                        </p>
                <?php } ?>
                <p>
                    <?php 
                    if (!empty($getBookingRecord['Transaction']['payment_id'])) {
                    echo __('Please write to us quoting your transaction id ('.str_replace('pay', 'SP', $getBookingRecord['Transaction']['payment_id']).') at'); ?> <?php echo __('<strong>customercare@simplypark.in</strong> and we will get back to you.'); 
                    }
                    ?>
                </p>
                <div class="congratulation-address clearfix">
                    <div class="col-sm-4">
                        <span class="parking-place"><?php echo $getBookingRecord['Space']['name'] . ' at ' . $getBookingRecord['Space']['City']['name']; ?></span><br>
                        <span class="parking-dates"><?php echo date('dS F H:i',strtotime($getBookingRecord['Booking']['start_date'])) . ' - ' . date('dS F, Y  H:i',strtotime($getBookingRecord['Booking']['end_date'])); ?></span><br>
                        <?php if (!empty($getBookingRecord['Transaction']['payment_id'])){  ?>
                        <span class="parking-dates"> <?php echo __('Transaction ID'); ?>: <?php echo str_replace('pay','SP',$getBookingRecord['Transaction']['payment_id']); ?></span><br>
                        <?php } ?>
                        <span class="parking-price"><i class="fa fa-inr"></i><?php echo $getBookingRecord['Booking']['booking_price'] + $getBookingRecord['Booking']['refundable_deposit']; ?></span>
                    </div>
                <div class="col-sm-4">

                    <address class="parking-address">
                        <p class="address-congo"><?php echo __('Address'); ?>:-</p>
                        <?php
                            $show_mobile_no = true;
                            $show_space_park_no = true;
                            if ($getBookingRecord['Space']['property_type_id'] == Configure::read('gated_community') && $getBookingRecord['Space']['booking_confirmation'] != Configure::read('Bollean.True')) {
                             
                                $show_mobile_no = false;
                                $show_space_park_no = false;
                            }else if ($getBookingRecord['Booking']['num_slots'] > 0){
                                $show_space_park_no = false;
                            }
                        ?>

                         
                        <?php 
                            if ($show_space_park_no) { ?> 
                                <p> <?php echo $getBookingRecord['SpacePark']['park_number']; ?></p>
                        <?php } ?>
                        <?php if ($getBookingRecord['Space']['property_type_id'] == Configure::read('gated_community')) { ?>

                            <p> <?php echo !empty($getBookingRecord['Space']['tower_number']) ? 'Tower Number : '.$getBookingRecord['Space']['tower_number'] : 'Tower Number : N/A' ; ?>
                            </p>
                        <?php } else { ?>
                            <p> <?php echo $getBookingRecord['Space']['flat_apartment_number']; ?></p>
                        <?php } ?>
                        <p> <?php echo $getBookingRecord['Space']['address1']; ?></p>
                        <p> <?php echo $getBookingRecord['Space']['address2'] . ', '.$getBookingRecord['Space']['State']['name'].', '.$getBookingRecord['Space']['City']['name'].', '. $getBookingRecord['Space']['post_code']; ?></p>
                        <?php  if ($show_mobile_no) { ?> 
                            <p> <?php echo $getBookingRecord['Space']['User']['UserProfile']['mobile'];  ?></p>
                        <?php } ?>
                    </address>
                </div>
                <div class="col-sm-4 text-right">
               <?php
	                 CakeLog::write('debug','GetBookingRecord ' . print_r($getBookingRecord,true));			     
                         $fileName = "";
			 if($getBookingRecord['Booking']['num_slots'] > 1){
                             $fileName = Configure::Read('QrImagePath').$getBookingRecord['Space']['id'].'/'.$getBookingRecord['Booking']['qrcodefilename'];
			 }else{
		             $fileName = Configure::Read('QrImagePath').$getBookingRecord['Booking']['qrcodefilename'];		 
             	         }
                         if (file_exists($fileName)){
                             echo $this->Html->image(
                                            '/'.$fileName,
                                                array(
                                                    'class' => 'img-responsive'
                                                    )
                                                );
                          }
                       ?>
                </div> 
                </div>
            </section>
        </div>
    </div>
</div>

