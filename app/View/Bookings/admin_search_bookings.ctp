<div class="crumbs">
    <ul id="breadcrumbs" class="breadcrumb">
        <li class="active">
            <i class="icon-user"></i>
            <?php
                echo $this->Html->link(
                        __('Search Bookings'),
                        '#',
                        array(
                                'class' => 'active'
                            )
                    ); 
                
            ?>
        </li>
    </ul>
</div>
<div class="panel1">
    <span class="clearfix"></span>
    <div class="panel panel-default" id="inline-popups">
        <div class="panel-heading clearfix">
            <h2 class="pull-left zero-margin">
                Search Booking
            </h2>
            <?php
                        echo $this->Form->create('User', array(
                                        'url' => '/admin/bookings/search_bookings',
                                        'type' => 'get',
                                        'novalidate' => true
                                        ));
                    ?>
                        <div class="row">

                            <div class="input-group col-xs-3 pull-right">

                                <?php
                                    echo $this->Form->input(
                                        'search',
                                        array(
                                            'div' => false,
                                            'label' => false,
                                            'class' => 'form-control',
                                            'placeholder' => 'Search (Booking Id)',
                                            'escape' => false,
                                            'default' => (isset($this->request->query['search']) && !empty($this->request->query['search'])) ? $this->request->query['search'] : ''
                                            )
                                    );
                                ?>
                                <span class="input-group-btn">
                                    <?php
                                        echo $this->Form->button(
                                            '<i class="icon-search"></i>',
                                            array(
                                                'type' => 'submit',
                                                'class'=> 'btn btn-default search-icon',
                                                'escape' => false
                                                )
                                        );
                                    ?>
                                </span>
                            </div>
                        </div>
                    <?php echo $this->Form->end(); ?>
        </div>
        <div class="table-responsive col-xs-6 col-center billing-detail" id="innercontent">
            <table class="table">
                <thead>
                     <tr class="active">
                       
                            <th colspan="2" class="text-center"><?php echo __('Booking Detail'); ?></th>
                           
            
              
                     </tr>
                    </thead>
                
                 <tbody>
                 <?php if(isset($bookingData) && !empty($bookingData)) { ?>

               
               
                        <tr>
                            <td><strong><?php echo $bookingType; ?></strong></td>

                        </tr>
                           
                    
                        <tr>
                            <td><?php echo __('Space Name') ?></td>
                             <td><?php echo $bookingData['Space']['name']; ?></td>
                        </tr>
                        <tr>
                            <td><?php echo __('Name') ?></td>
                           
                            <td><?php echo $bookingData['Booking']['first_name'].' '.$bookingData['Booking']['last_name']; ?></td>
                           
                        </tr>
                        <tr>
                            <td><?php echo __('Email') ?></td>
                            <td><?php echo $bookingData['Booking']['email'] ?></td>
                            
                        </tr>
                        <tr>
                             <td><?php echo __('Start Date') ?></td>
                            <td><?php echo $bookingData['Booking']['start_date'] ?></td>
                            
                        </tr>
                        <tr>
                            <td><?php echo __(' End Date ') ?></td>
                            <td><?php echo $bookingData['Booking']['end_date'] ?></td>
                            
                        </tr>
                        <tr>
                            <td><?php echo __(' Booking Type ') ?></td>
                            <td><?php if ($bookingData['Booking']['booking_type'] == 1) { 
                                    echo 'Hourly';
                                } else if ($bookingData['Booking']['booking_type'] == 2) {
                                    echo 'Daily';
                                } else if ($bookingData['Booking']['booking_type'] == 3) {
                                    echo 'Weekly';
                                } else if ($bookingData['Booking']['booking_type'] == 4) {
                                    echo 'Monthly';
                                 } else if ($bookingData['Booking']['booking_type'] == 5) { 
                                    echo 'Yearly';
                                } else {
                                    echo 'Invalid';
                                } ?>
                            </td>
                            
                        </tr>
                        <tr>
                            <td><?php echo __(' Booking Price ') ?></td>
                            <td><?php echo $bookingData['Booking']['booking_price']; ?></td>
                            
                        </tr>
                        <tr>
                            <td><?php echo __(' Discount ') ?></td>
                            <td><?php echo $bookingData['Booking']['discount_amount']; ?></td>
                            
                        </tr>
                        <tr>
                            <td><?php echo __(' Commission ') ?></td>
                            <td><?php echo $bookingData['Booking']['commission']; ?></td>
                            
                        </tr>
                        <tr>
                            <td><?php echo __(' Owner Income ') ?></td>
                            <td><?php echo $bookingData['Booking']['owner_income']; ?></td>
                            
                        </tr>
                        <tr>
                            <td><?php echo __(' Simply Park Income ') ?></td>
                            <td><?php echo $bookingData['Booking']['simplypark_income']; ?></td>
                            
                        </tr>
                         <tr>
                            <td><?php echo __(' Simply Park Service Tax ') ?></td>
                            <td><?php echo $bookingData['Booking']['simplypark_service_tax_amount']; ?></td>
                            
                        </tr>
                        
                        <?php if (!empty($bookingData['BookingInstallment'])) { ?>
                        <tr>

                            <td><?php echo __('  Booking Installments  ') ?></td>
                            <td>
                                <?php foreach ($bookingData['BookingInstallment'] as $key => $value) { ?>
                                      <ol>
                                            <strong>Installment <?php echo $key+1; ?></strong> 
                                            <li>Amount : <?php echo $value['amount']; ?></li>
                                            <li>Pay Date : <?php echo $value['date_pay']; ?></li>
                                            <li>End Date : <?php echo $value['end_date']; ?></li>
                                            <li>Commission : <?php echo $value['commission']; ?></li>
                                            <li>Service Tax : <?php echo $value['service_tax']; ?></li>
                                            <li>Paid to owner : <?php echo $value['paid_to_owner'] == 1 ? 'Paid':'Not Paid'; ?></li>
                                           
                                            <?php if ($value['paid_to_owner'] == 1) { ?>
                                                    <li>Reference Code : <?php echo $value['bank_reference_code']; ?></li>
                                                    <li> Amount Paid : 
                                                            <?php 
                                                                if ($value['service_tax_by_owner'] == 1 ) {
                                                                    $value['space_price'] = $value['space_price'] + $value['owner_service_tax_amount'];
                                                                }

                                                                echo  number_format($value['space_price'],2);
                                                            ?>
                                                    </li>
                                            <?php } ?> 
                                      </ol>  
                                
                                <?php } ?>
                            </td>
                            
                        </tr>

                        <?php } ?>
                        <?php if ($bookingData['Booking']['paid_to_owner'] == 1) { ?>
                            <tr>
                                <td><?php echo __(' Bank Reference Code ') ?></td>
                                <td><?php echo $bookingData['Booking']['bank_reference_code']; ?></td>                                
                            </tr>
                            <tr>

                                <td><?php echo __(' Amount Paid ') ?></td>

                                <td>
                                    <?php 
                                        if ($bookingData['Booking']['service_tax_by_owner'] == 1 ) {
                                            $bookingData['Booking']['space_price'] = $bookingData['Booking']['space_price'] + $bookingData['Booking']['owner_service_tax_amount'];
                                        }

                                        echo  number_format($bookingData['Booking']['space_price'],2);
                                    ?>
                                </td>
                                
                            </tr>
                        <?php } ?> 
                  <?php } else { ?>
                        <tr>
                            <td> No Booking Found.</td>
                        </tr>
                 <?php } ?>
                   </tbody>
            </table>
           
        </div>
    </div>
    <?php
        echo $this->Html->script(array(
            'backend/Space/index',
            'backend/commonmagnificpopup',
            'backend/Map',
            )
            //array('inline' => false)
        );
    ?>
</div>

