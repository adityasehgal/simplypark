<?php echo $this->Html->script(
                array(
                        'frontend/BookingSpace/index'
                    ),
                array('inline' => false)); ?>
<div class="main-container">
    <div class="contact-bg">
        <div class="container">
            <section class="clearfix">
                <div class="row">
                    <div class="col-sm-12 account-info">
                        <div class="col-sm-2">
                            <?php 
                                if (!empty($spaceDetail['Space']['place_pic'])) {
                                    if (file_exists(Configure::Read('SpaceImagePath').'place_pic.'.$spaceDetail['Space']['place_pic'])) {
                                        echo $this->Html->image(
                                            '/'.Configure::Read('SpaceImagePath').'place_pic.'.$spaceDetail['Space']['place_pic'],
                                            array(
                                                    'class' => 'img-responsive'
                                                )
                                        );
                                    } else {
                                        echo $this->Html->image('Not_available.jpg',
                                            array(
                                                    'class' => 'img-responsive'
                                                )
                                        );
                                    }
                                } else {
                                    echo $this->Html->image('Not_available.jpg',
                                        array(
                                                'class' => 'img-responsive'
                                            )
                                    );
                                }
                            ?>
                        </div>
                        <div class="col-sm-3">
                            <span class="parking-place"><?php echo $spaceDetail['Space']['name'] . ' at ' . $spaceDetail['City']['name']; ?></span><br>
                            <span class="parking-dates"> 
                                Entry Time : <?php echo date('jS M \'y', strtotime($startDate)) .' ' .'<b>' . date('H:i',strtotime($startDate)) . '</b>' ?><br />
                                Exit Time  : <?php echo date('jS M \'y',strtotime($endDate)) . ' ' . '<b>' . date('H:i',strtotime($endDate)) . '</b>' ?><br />
                            </span>
                            <div class="parking-price">
                                <i class="fa fa-inr"></i>
                                      <?php 
                                         if ( $monthlyInstallment )
                                         {
                                            echo "$monthlyInstallment/month <br />";
                                         }
                                        else
                                         {
                                            echo "<span class=st-parking-price-strikethrough>$totalPrice</span>";
                                         }
                                       ?>
                              </div>
                            <div class="lt-parking-price-info hide">
                            </div>
                            <div class="parking-price hide" id="after-discount-price">
                            </div>
                         
                        </div>
                        <div class="col-sm-3">
                            <address class="parking-address">
                                <p>
                                    <?php
                                        if ($spaceDetail['Space']['property_type_id'] == Configure::read('gated_community')) {
                                            echo 'Tower '.$spaceDetail['Space']['tower_number'].',';
                                        } else {
                                            echo $spaceDetail['Space']['flat_apartment_number'].',';
                                        }
                                    ?>
                                </p>
                                <p> <?php echo $spaceDetail['Space']['address1'] .', '. $spaceDetail['Space']['address2']; ?></p>
                                <p> <?php echo $spaceDetail['Space']['post_code']; ?></p>
                            </address>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 account-info-section">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#" aria-expanded="true" aria-controls="collapseOne">
                                        <?php echo __('1. Account Information'); ?>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body ac-info">
                                    <?php
                                        echo $this->Form->create(
                                                'Booking',
                                                array(
                                                        'class' => 'form-horizontal',
                                                        'id' => 'account-information'
                                                    )
                                            );
                                    ?>
                                        <div class="form-group">
                                            <label for="car" class="col-sm-3 control-label" ></label>
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                   
                                                    <span class="isBulk hide"><?php echo $isBulk; ?></span>
                                                    <span class="numSlotsForBulkBooking hide"><?php echo $numSlots; ?></span>
                                                    <span class="propertyTypeId hide"><?php echo $spaceDetail['Space']['property_type_id']; ?></span>
                                                    <?php
                                                        echo $this->Form->input(
                                                                'discount_code',
                                                                array(
                                                                        'class' => 'form-control input-lg coupon-input',
                                                                        'placeholder' => 'Enter PromoCode or Discount Voucher',
                                                                        'div' => false,
                                                                        'label' => false
                                                                    )
                                                            );
                                                    ?>
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-lg btn-gray apply-coupon" data-loading-text="Applying..." type="button"><?php echo __('Apply'); ?></button>
                                                    </span>
                                                </div><!-- /input-group -->
                                                <div class="help-info">
                                                    <small class="text-success" id="show-error">
                                                      
                                                    </small>
                                                </div>
                                            </div>
                                        </div>
                                             <div class="float-group clearfix">
                                            <label for="name " class="col-sm-3 control-label"><?php echo __('Name'); ?><span class='firstname'>*</span></label>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <?php
                                                        echo $this->Form->input(
                                                                'first_name',
                                                                array(
                                                                        'class' => 'form-control',
                                                                        'placeholder' => 'First Name',
                                                                        'value' => $getUserProfile['UserProfile']['first_name'],
                                                                        'div' => false,
                                                                        'label' => false,
                                                                        'id' => 'first_name'
                                                                    )
                                                            );
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <?php
                                                        echo $this->Form->input(
                                                                'last_name',
                                                                array(
                                                                        'class' => 'form-control',
                                                                        'placeholder' => 'Last Name',
                                                                        'value' => $getUserProfile['UserProfile']['last_name'],
                                                                        'div' => false,
                                                                        'label' => false,
                                                                        'id' => 'last_name'
                                                                    )
                                                            );
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="float-group clearfix">
                                            <label for="phone-no" class="col-sm-3 control-label"><?php echo __('Contact Details'); ?><span class='contactdetails'>*</span></label>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <?php
                                                        echo $this->Form->input(
                                                                'mobile',
                                                                array(
                                                                        'class' => 'form-control',
                                                                        'placeholder' => 'Phone No',
                                                                        'value' => $getUserProfile['UserProfile']['mobile'],
                                                                        'div' => false,
                                                                        'label' => false,
                                                                        'id' => 'mobile'
                                                                    )
                                                            );
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <?php
                                                        echo $this->Form->input(
                                                                'email',
                                                                array(
                                                                        'class' => 'form-control',
                                                                        'placeholder' => 'Email',
                                                                        'value' => $this->Session->read('Auth.User.email'),
                                                                        'div' => false,
                                                                        'label' => false,
                                                                        'id' => 'email'
                                                                    )
                                                            );
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <?php
                                                    echo $this->Form->input('user_car',
                                                        array(
                                                                'value' => $bulkUserCarId,
                                                                'id' => 'user-cars',
                                                                'type' => 'hidden'
                                                            )
                                                        );
                                                ?>
                                            </div>
                                        <div class="form-group">
                                            <div class="col-sm-9 col-sm-offset-3">
                                                <?php
                                                    echo $this->Form->submit(
                                                                    __('Next'),
                                                                    array(
                                                                            'class' => 'btn btn-info btn-lg',
                                                                            'escape' => false
                                                                        )
                                                                );
                                                ?>
                                            </div>
                                        </div>
                                    <?php echo $this->Form->end(); ?>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#" aria-expanded="false" aria-controls="collapseTwo">
                                        <?php echo __('2. Review & Payment Information'); ?>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body review-section">
                                    <div class="col-sm-10 col-sm-offset-2">
                                        <dl class="dl-horizontal">
                                            <dt><?php echo __('Parking at'); ?></dt>
                                            <dd><?php echo $spaceDetail['Space']['name'] . ', ' . $spaceDetail['City']['name']; ?></dd>
                                            <dt><?php echo __('Duration'); ?></dt>
                                            <dd><?php echo date('jS M \'y', strtotime($startDate))  . ' ' . date('H:i',strtotime($startDate)). ' to ' .  
                                                           date('jS M \'y',strtotime($endDate)) . ' '. date('H:i',strtotime($endDate)) ?></span><br>
                                            <?php 
                                               if ( $monthlyInstallment )
                                               {
                                                  echo "<dt>Rate</dt>";
                                                  echo "<dd class=total-amount-pay> " . "Rs.  $monthlyInstallment  per month</dd>";
                                                  echo "<div id=lt-discount-info class=hide></div>";
                                                  echo "<dt>Number of Slots </dt>";
                                                  echo "<dd class=total-amount-pay> " . "$numSlots</dd>";
                                                  if($refundableDeposit > 0){
                                                  echo "<dt id=rdeposit>Refundable Deposit </dt>";
                                                  echo "<dd id=rdepositVal class=total-amount-pay> " . "Rs.  $refundableDeposit &nbsp;&nbsp;<small><a href=https://www.simplypark.in/faq/index.php?solution_id=1018 target=_blank>Why we collect a deposit?</a></small></dd>";
                                                  }
                                                  echo "<dt>Payable Now</dt>";
                                                  $payNow = ($monthlyInstallment) + $refundableDeposit;
                                                  echo "<dd class=total-amount-pay id=lt-final-pay>Rs.&nbsp; $payNow</dd>";

                                               }
                                              else
                                               {
                                                  echo "<dt>Price</dt>";
                                                  echo "<dd class=total-amount-pay> " . "Rs. <span class=st-parking-price-display-2>$totalPrice</span> <span class=st-handling-charge-display-1>(includes a handling charge of Rs. $handlingCharges)</span></dd>";
                                               }
                               
                                            ?>
                                        </dl>
                                    </div>
                                          <?php 
                                                if ( $numberOfInstallments )
                                                {
                                               	    $offset = "col-sm-offset-2";
                                                    $colLen = "col-sm-10";
                                                    if ( $numberOfInstallments < 6 )
                                                    {
                                                       $offset = "col-sm-offset-4";
                                                       $colLen = "col-sm-8";
                                                    }  
                                                    else if ( $numberOfInstallments > 6 && $numberOfInstallments < 11)
                                                    {
                                                       $offset = "col-sm-offset-3";
                                                       $colLen = "col-sm-9";
                                                    }  
                                                     echo "<div class=\"$colLen $offset\">"; 
                                                      if ( $monthlyInstallment )
                                                      {
                                                         $payNow = ($monthlyInstallment) + $refundableDeposit;
                                                         echo "Your payment schedule";
                                                         echo "<table id=priceschedule border=1 cellspacing=2>";
                                                         echo "<th id=paynow>Now&nbsp;&nbsp;</th>";
                                                         $startDateTimeStamp = strtotime($startDate);
                                                         for($itr=0; $itr<$numberOfInstallments-1; $itr++)
                                                         {
                                                            $timestamp = strtotime("+1 month",$startDateTimeStamp);
                                                            $month     = date('jS M',$timestamp);
                                                            echo "<th id=installment>$month</th>";
                                                            $startDateTimeStamp = $timestamp;
                                                         }
                                                         $displayAmount = $payNow; 
                                                         echo "<tr>";
                                                         echo "<td id=paynowAmount align=center>$displayAmount";
                                                         if($refundableDeposit > 0){
                                                         echo "*";
                                                         }
                                                         echo "</td>";
                                                         for($itr=1; $itr<$numberOfInstallments-1; $itr++)
                                                         {
                                                            echo "<td id=installment".$itr." align=center>$monthlyInstallment</td>";
                                                         }
                                                         echo "<td id=lastInstallment align=center>$lastInstallmentAmount</td>";
                                                         echo "</tr>";
                                                         echo "</table>";
                                                         if($refundableDeposit > 0){
                                                         echo "<small>*Includes refundable deposit</small>";
                                                         }
                                                         echo "<br /><br /></div>";
                                                      }
                                                    }
                                             ?>
                                    <?php
                                        if (!$spaceDetail['Space']['booking_confirmation']) {
                                    ?>
                                            <!--<div class="col-sm-10 col-sm-offset-2 review-text">-->
                                            <div class="col-sm-10 col-sm-offset-2 review-text">
                                                <?php echo 'Once you book, '.$spaceDetail['Space']['name'].', '.$spaceDetail['City']['name'].' will approve your booking. Your credit/debit card will be charged but the amount will be automatically refunded in case the Owner disapproves your booking request'; ?>
                                            </div>
                                    <?php
                                        }
                                        echo $this->Form->create(
                                                'AgreeBookingTerms',
                                                array(
                                                        'class' => 'form-horizontal',
                                                        'id' => 'agree-booking-terms'
                                                    )
                                            );
                                    ?>
                                        <div class="col-sm-10 col-sm-offset-2 igree-text">
                                            <label class="checkbox-inline">
                                                <?php
                                                    echo $this->Form->checkbox('agree_booking_terms_conditions');
                                                ?>
                                                <span class="text-muted"><?php echo __('I agree to SimplyPark.in'); ?> </span>

                                                <span class="text-primary">
                                                  <?php
                                                       echo $this->Html->link(
                                                             __('Terms & Conditions'),
                                                             'https://www.simplypark.in/homes/cms/MTA%3D',
                                                             array(
                                                                   'target' => '_blank'
                                                                  )
                                                              );
                                                  ?>


                                             </span>
                                            </label>
                                            <div class="help-block has-error">
                                            </div>
                                        </div>
                                        <div class="col-xs-10 col-sm-offset-2">
                                            <?php
                                                echo $this->Form->button(
                                                        __('Pay'),
                                                        array(
                                                                'class' => 'btn btn-info btn-lg',
                                                                'type' => 'submit'
                                                            )
                                                    );
                                            ?>
                                            <?php
                                                echo $this->Html->link(
                                                        __('Cancel'),
                                                        '/',
                                                        array(
                                                                'class' => 'btn btn-default btn-lg',
                                                                'escape' => false
                                                            )
                                                    );
                                            ?>
                                        </div>
                                    <?php echo $this->Form->end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
            </section>
        </div>
    </div>
</div>
<div class="hide">
    <?php
        echo $this->Form->create(
                'Booking',
                array(
                        'controller' => 'bookings',
                        'action' => 'saveBooking',
                        'type' => 'post',
                        'id' => 'booking-space-save',
                        'inputDefaults' => array(
                                'div' => false,
                                'label' => false
                            )
                    )
            );

            echo $this->Form->input('space_id', array('value' => $spaceID, 'type' => 'text'));
            echo $this->Form->input('num_slots', array('value' => $numSlots, 'type' => 'text'));
            echo $this->Form->input('space_user_id', array('value' => $spaceDetail['User']['id'], 'type' => 'text'));
            if ($spaceDetail['Space']['booking_confirmation']) 
            {
                echo $this->Form->input(Configure::read('BookingTableStatusField'), 
                                         array('value' => $spaceDetail['Space']['booking_confirmation'], 'type' => 'text'));
                $amountToCapture = ($monthlyInstallment > 0)?$monthlyInstallment+$refundableDeposit:$totalPrice;
                //amountToCapture  = 1st installment + deposit 
                echo $this->Form->input('amount_to_capture', array('value' => $amountToCapture, 'type' => 'text', 'class' => 'amount_to_capture'));
                echo $this->Form->input('status', array('value' => Configure::read('Bollean.True'), 'type' => 'text'));
            }
            echo $this->Form->input('user_id', array('value' => $this->Session->read('Auth.User.id'), 'type' => 'text'));
            echo $this->Form->input('start_date', array('value' => date('Y-m-d H:i:s', strtotime($startDate)), 'type' => 'text'));
            echo $this->Form->input('end_date', array('value' => date('Y-m-d H:i:s', strtotime($endDate)), 'type' => 'text'));

            echo $this->Form->input('space_park_id', array('value' => $space_park_id, 'type' => 'text'));

            echo $this->Form->input('commission', array('value' => $commissionPercentage));
            echo $this->Form->input('commission_paid_by', array('value' => $commissionPaidBy));
            echo $this->Form->input('service_tax', array('value' => $serviceTaxPercentage));

            if ($ownerServiceTax > 0) 
            {
                echo $this->Form->input('service_tax_by_owner', array('value' => 1, 'type' => 'text'));
            }
            
           if ($simplyParkServiceTax > 0) 
            {
                echo $this->Form->input('service_tax_by_simplypark', array('value' => 1, 'type' => 'text'));
            }


            //space price - before handling charge
            if ( $monthlyInstallment )
            {
               $spacePrice = $monthlyInstallment - $monthlyHandlingCharge;
	       echo $this->Form->input('space_price', array('value' => $spacePrice));
	       //booking price- with handling charges
	       echo $this->Form->input('booking_price', array('value' => $monthlyInstallment, 'id' => 'booking-price'));
               //owner service tax
               echo $this->Form->input('owner_service_tax_amount', array('value' => $ownerServiceTaxPerInstallment));
               //monthly handling charge
               echo $this->Form->input('internet_handling_charges', array('value' => $monthlyHandlingCharge, 
                                                                          'id' => 'internet-handling-charges'));
               //sp service tax
               echo $this->Form->input('simplypark_service_tax_amount', array('value' => $simplyParkServiceTaxPerInstallment, 
                                                                              'id' => 'simplypark-tax-amount'));
               //owner income
               echo $this->Form->input('owner_income', array('value' => $ownerIncomePerInstallment));
               //sp income
               echo $this->Form->input('simplypark_income', array('value' => $simplyParkIncomePerInstallment, 'id' => 'simplypark-income'));
            }
           else
            {
	       echo $this->Form->input('space_price', array('value' => $bookingPriceBeforeHandlingCharges));
	       //booking price- with handling charges
	       echo $this->Form->input('booking_price', array('value' => $totalPrice, 'id' => 'booking-price'));
               //owner service tax
               echo $this->Form->input('owner_service_tax_amount', array('value' => $ownerServiceTax));
               //handling charge
               echo $this->Form->input('internet_handling_charges', array('value' => $internetHandlingCharges, 
                                                                          'id' => 'internet-handling-charges'));
               //sp service tax
               echo $this->Form->input('simplypark_service_tax_amount', array('value' => $simplyParkServiceTax, 
                                                                              'id' => 'simplypark-tax-amount'));
               //owner income
               echo $this->Form->input('owner_income', array('value' => $ownerIncome));
               //sp income
               echo $this->Form->input('simplypark_income', array('value' => $simplyParkServiceIncome, 'id' => 'simplypark-income'));
            }

            if ($monthlyInstallment) 
            {
	            echo $this->Form->input('refundable_deposit', array('value' => $refundableDeposit, 'class' => 'refundable_amount'));
	            $totalInstallment = $numberOfInstallments;

	            $installmentDate = date('Y-m-d');
	            $monthsToAdd = 1;

	            for($itr=0 ; $itr < $numberOfInstallments; $itr++)
	            {
		            $nextInstallmentDate = date('Y-m-d', strtotime($startDate. ' +'.$monthsToAdd. ' month')); 
		            $monthsToAdd++;

		            //date_pay
		            echo $this->Form->input('BookingInstallment.'.$itr.'.date_pay', 
			                  array('value' => date('Y-m-d',strtotime($installmentDate)), 
					        'type' => 'text'));


		            //status
		            if ( $itr == 0 )
			    {
				    echo $this->Form->input('BookingInstallment.'.$itr.'.status', 
						    array('value' => Configure::read('Bollean.True'), 
							    'type' => 'text'));
			    } 
		          else 
			   {
				  echo $this->Form->input('BookingInstallment.'.$itr.'.status', 
						  array('value' => Configure::read('Bollean.False'), 
							  'type' => 'text'));
			   }
		         //commission percentage 
		         echo $this->Form->input('BookingInstallment.'.$itr.'.commission', 
			                  	array('value' => $commissionPercentage, 'type' => 'text'));
		         //commission paid by 
		         echo $this->Form->input('BookingInstallment.'.$itr.'.commission_paid_by', 
			                  	array('value' => $commissionPaidBy, 'type' => 'text'));
		         //serviceTaxPercentage 
		         echo $this->Form->input('BookingInstallment.'.$itr.'.service_tax', 
			                  	array('value' => $serviceTaxPercentage, 
					         'type' => 'text'));
		         //owner service tax payable 
		         if ($ownerServiceTax > 0) 
		         {
			         echo $this->Form->input('BookingInstallment.'.$itr.'.service_tax_by_owner', 
			         	         	array('value' => 1, 
				         		'type' => 'text'));
		         }
		         
                         //simplypark service tax payable 
		         if ($simplyParkServiceTax > 0) 
		         {
			         echo $this->Form->input('BookingInstallment.'.$itr.'.service_tax_by_simplypark', 
				                  	array('value' => 1, 
					         	'type' => 'text'));
		         }
                         
                         if ( $itr != ($numberOfInstallments - 1) )
			 { 
		                 //end_date
		                 echo $this->Form->input('BookingInstallment.'.$itr.'.end_date', 
				               array('value' => date('Y-m-d',strtotime($nextInstallmentDate)), 
					            'type' => 'text'));
		                 //amount
		                 echo $this->Form->input('BookingInstallment.'.$itr.'.amount', 
			        	         array('value' => $monthlyInstallment, 
			        		'class' => 'installment_pay'.$itr));
				 //owner service tax amount
				 echo $this->Form->input('BookingInstallment.'.$itr.'.owner_service_tax_amount', 
						 array('value' => $ownerServiceTaxPerInstallment, 
							 'type' => 'text'));
				 //simplypark service tax amount
				 echo $this->Form->input('BookingInstallment.'.$itr.'.simplypark_service_tax_amount', 
						 array('value' => $simplyParkServiceTaxPerInstallment, 
							 'type' => 'text', 
							 'class' => 'installment_sp_tax_amount'.$itr));
				 //handling charges
				 echo $this->Form->input('BookingInstallment.'.$itr.'.internet_handling_charges', 
						 array('value' => $monthlyHandlingCharge, 
							 'type' => 'text', 
							 'class' => 'installment_handling_charges'.$itr));
				 //owner income
				 echo $this->Form->input('BookingInstallment.'.$itr.'.owner_income', 
						 array('value' => $ownerIncomePerInstallment, 
							 'type' => 'text'));
				 //simplypark income
				 echo $this->Form->input('BookingInstallment.'.$itr.'.simplypark_income', 
						 array('value' => $simplyParkIncomePerInstallment, 
							 'type' => 'text', 
							 'class' => 'installment_sp_income'.$itr));
			 }
                        else
                         {
		                 //end_date
		                 echo $this->Form->input('BookingInstallment.'.$itr.'.end_date', 
				               array('value' => date('Y-m-d',strtotime($endDate)), 
					            'type' => 'text'));
		                 //amount
		                 echo $this->Form->input('BookingInstallment.'.$itr.'.amount', 
			        	         array('value' => $lastInstallmentAmount, 
			        		'class' => 'installment_pay'.$itr));
				 //owner service tax amount
				 echo $this->Form->input('BookingInstallment.'.$itr.'.owner_service_tax_amount', 
						 array('value' => $ownerServiceTaxForLastInstallment, 
							 'type' => 'text'));
				 //simplypark service tax amount
				 echo $this->Form->input('BookingInstallment.'.$itr.'.simplypark_service_tax_amount', 
						 array('value' => $simplyParkServiceTaxForLastInstallment, 
							 'type' => 'text', 
							 'class' => 'installment_sp_tax_amount'.$itr));
				 //handling charges
				 echo $this->Form->input('BookingInstallment.'.$itr.'.internet_handling_charges', 
						 array('value' => $lastMonthHandlingCharge, 
							 'type' => 'text', 
							 'class' => 'installment_handling_charges'.$itr));
				 //owner income
				 echo $this->Form->input('BookingInstallment.'.$itr.'.owner_income', 
						 array('value' => $ownerIncomeForLastInstallment, 
							 'type' => 'text'));
				 //simplypark income
				 echo $this->Form->input('BookingInstallment.'.$itr.'.simplypark_income', 
						 array('value' => $simplyParkIncomeForLastInstallment, 
							 'type' => 'text', 
							 'class' => 'installment_sp_income'.$itr));
                         }
	        	    $installmentDate = $nextInstallmentDate;

                        
                          




	            }



            }
                

        echo $this->Form->end();
    ?>
</div>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<?php
    if ( $monthlyInstallment )
    {
       $razorPayAmount = $monthlyInstallment + $refundableDeposit;
    }
   else
    {
       $razorPayAmount = $totalPrice;
    }
    echo '<script>
            var razorPayKeyID = ' . json_encode(Configure::read('RazorPayKeys')) . ';
            var totalAmount = ' . $razorPayAmount*100 . ';
            var spaceDetail = ' . json_encode($spaceDetail) . ';
            var taxPercentage = ' . $serviceTaxPercentage . ';
            var spaceID = ' . $spaceID . ';
            var startDate = ' . json_encode($startDate) . ';
            var endDate = ' . json_encode($endDate) . ';
        </script>';
    echo $this->element('frontend/BookingSpace/add_car');
?>
