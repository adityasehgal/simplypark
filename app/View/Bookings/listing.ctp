<?php echo $this->Html->script('frontend/BookingSpace/listing', array('inline' => false)); ?>

	
<section class="col-xs-12 col-md-9 right-panel">
					
	<div class="heading clearfix col-xs-12">
		<span class="pull-left title"><?php echo __('Bookings'); ?></span>
	</div>

	<div class="booking-receive" data-example-id="togglable-tabs">
	    <ul id="myTabs" class="nav nav-tabs nav-justified" role="tablist">
	    	<?php 
	    		if (isset($_GET['booking-tab']) && !empty($_GET['booking-tab'])) {
	    			if($_GET['booking-tab'] == 1) {
	    				$class1 = 'active';
		    			$class2 = '';
		    			$ariaExpanded1 = 'true';
		    			$ariaExpanded2 = 'false';
		    			$class3 = 'active in';
		    			$class4 = '';

	    			} else {
	    				$class1 = '';
		    			$class2 = 'active';
		    			$ariaExpanded1 = 'false';
		    			$ariaExpanded2 = 'true';
		    			$class3 = '';
		    			$class4 = 'active in';
	    			}
		    			
	    		} else {
	    			$class1 = 'active';
	    			$class2 = '';
	    			$ariaExpanded1 = 'true';
	    			$ariaExpanded2 = 'false';
	    			$class3 = 'active in';
	    			$class4 = '';

	    		}	

	    	?> 
	      	<li role="presentation" class="<?php echo $class1; ?>"><a href="#bookingMade" id="booking-made-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="?php echo $ariaExpanded1; ?>"><?php echo __('Bookings Made'); ?></a></li>
	      	<li role="presentation" class="<?php echo $class2; ?>"><a href="#bookingReceived" role="tab" id="booking-received-tab" data-toggle="tab" aria-controls="profile" aria-expanded="<?php echo $ariaExpanded2; ?>"><?php echo __('Bookings Received'); ?></a></li>	   
	    </ul>
	    <div id="myTabContent" class="tab-content">
	    	<div class="loader">
		    	<?php
	                echo $this->Html->image(
	                    'loader.gif',
	                    array('id' => 'busy-indicator')
	                );
	            ?>
		    </div>
		    <div role="tabpanel" class="tab-pane fade <?php echo $class3; ?>" id="bookingMade" aria-labelledby="booking-made-tab">
		    	<?php
		    		echo $this->element('frontend/BookingSpace/booking_made');
		    	?>
		    </div>
		    <div role="tabpanel" class="tab-pane fade <?php echo $class4; ?>" id="bookingReceived" aria-labelledby="booking-received-tab">
		    	<?php
		    		echo $this->element('frontend/BookingSpace/booking_received');
		    	?>
		    </div>
	    </div>
	</div>
</section>

<!-- Modal -->
		<div class="modal fade" id="myModalcancel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog ">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title view-details-title" id="myModalLabel"><?php echo __('Request for Cancellation'); ?></h4>
					</div>
					<div class="modal-body clearfix">
						<span class-="model-text"><?php echo __('Are you sure you want cancel your booking!'); ?></span>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default noButton" data-dismiss="modal"><?php echo __('No'); ?></button>
						<!--<button type="button" class="btn btn-info">-->
                                                <?php
				                   echo $this->Html->link(
						                          __('Yes'),
						                             '#',
						                       array(
						                             'class' => 'btn btn-info',
						                             'escape' => false,
						                             'id' => 'cancel-request-link'
						                             )
						                          );
				                ?>
                                                             

                                               <!-- </button>-->
					</div>
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="myModalapprove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title view-details-title" id="myModalLabel"><?php echo __('Approval'); ?></h4>
					</div>
					<div class="modal-body clearfix">
						<span class-="model-text">	<?php echo __('Are you sure you want Approve your booking!'); ?></span>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('No'); ?></button>
						<button type="button" class="btn btn-info access-approval"><?php echo __('Yes'); ?></button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="myModaldisapprove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title view-details-title" id="myModalLabel"><?php echo __('Disapprove'); ?></h4>
					</div>
					<div class="modal-body clearfix">
						<span class-="model-text"><?php echo __('Are you sure you want Disapprove your booking!'); ?></span>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('No'); ?></button>
						<button type="button" class="btn btn-info access-disapproval"><?php echo __('Yes'); ?></button>
						
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="myModalcancelbooking" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title view-details-title" id="myModalLabel"><?php echo __('Cancel Booking'); ?></h4>
					</div>
				<div class="modal-body clearfix">
					<span class-="model-text"><?php echo __('Are you sure you want cancel customer parking space!'); ?></span>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default noButtonOwner" data-dismiss="modal"><?php echo __('No'); ?></button>
					<!--<button type="button" class="btn btn-info" data-dismiss="modal">></button> -->	
                                                <?php
				                   echo $this->Html->link(
						                          __('Yes'),
						                             '#',
						                       array(
						                             'class' => 'btn btn-info',
						                             'escape' => false,
						                             'id' => 'cancel-request-by-owner-link'
						                             )
						                          );
				                ?>
				</div>
				</div>
			</div>
		</div>
