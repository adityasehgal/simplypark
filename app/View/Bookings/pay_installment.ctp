<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<?php
   echo $this->Html->script('frontend/BookingSpace/payinstallment', array('inline' => false));
?>
<div class="main-container">
   <div class="contact-bg">
      <div class="container">
         <section class="clearfix contact-main">
            <div class="col-sm-7">
               <h1><?php echo __('Pay Installment'); ?></h1>
                  <p>
                     <?php echo __("Below are the details of your installment : -"); ?>
                  </p>
                  <div class="row">
                     <div class="form-group col-sm-6">
                        <label for="name"><?php echo __('Space Name: '); ?></label>
                        <?php echo $installmentInfo['Booking']['Space']['name']; ?>
                     </div>
                     <div class="form-group col-sm-6">
                        <label for="name"><?php echo __('Amount To Pay: '); ?></label>
                        <?php echo $installmentInfo['BookingInstallment']['amount']; ?>
		     </div>
                   </div>
                   <div class="row">
                     <div class="form-group col-sm-6">
                        <label for="Email"><?php echo __('Booking Start Date: '); ?></label>
                        <?php echo date('Y-m-d',strtotime($installmentInfo['BookingInstallment']['date_pay'])); ?>
                     </div>
                     <div class="form-group col-sm-6">
                        <label for="Email"><?php echo __('Booking End Date: '); ?></label>
                        <?php echo date('Y-m-d',strtotime($installmentInfo['BookingInstallment']['end_date'])); ?>
		     </div>
                    </div>
                     <div class="col-sm-12">
                        <div class="row">
                           <div class="col-sm-6 text-right">
                              <?php
                                 echo $this->Html->link(
                                          __('Pay'),
                                          '#',
                                          array(
                                                'escape' => false,
                                                'class' => 'btn btn-info btn-lg pay_intallment'
                                             )
                                       );
                              ?>
                           </div>
                        </div>
                     </div>
                  </div>
            </div>
         </section>
      </div>
   </div>
</div>
<div class="hide">
    <?php
         echo $this->Form->create(
                'Booking',
                array(
                        'url' => '/bookings/installmentSave',
                        'type' => 'post',
                        'id' => 'booking-installment-pay',
                        'inputDefaults' => array(
                                'div' => false,
                                'label' => false
                            )
                    )
            );

            echo $this->Form->input('BookingInstallment.0.id', array('value' => $installmentInfo['BookingInstallment']['id'], 'type' => 'text'));
            echo $this->Form->input('BookingInstallment.0.status', array('value' => Configure::read('Bollean.True'), 'type' => 'text'));
            echo $this->Form->input('id', array('value' => $installmentInfo['BookingInstallment']['booking_id'], 'type' => 'text'));
            echo $this->Form->input('user_id', array('value' => $installmentInfo['Booking']['user_id'], 'type' => 'text'));
            echo $this->Form->input('space_user_id', array('value' => $installmentInfo['Booking']['space_user_id'], 'type' => 'text'));
            echo $this->Form->input('id', array('value' => $installmentInfo['BookingInstallment']['booking_id'], 'type' => 'text'));
            echo $this->Form->input('booking_price', array('value' => $installmentInfo['BookingInstallment']['amount'], 'type' => 'text'));
            echo $this->Form->input('commission', array('value' => $installmentInfo['Booking']['commission']+$installmentInfo['BookingInstallment']['commission'], 'type' => 'text'));

            echo $this->Form->input('service_tax', array('value' => $installmentInfo['Booking']['service_tax']+$installmentInfo['BookingInstallment']['service_tax'], 'type' => 'text'));

            if (!empty($installmentInfo['BookingInstallment']['owner_service_tax_amount'])) {
              echo $this->Form->input('owner_service_tax_amount', array('value' => $installmentInfo['Booking']['owner_service_tax_amount']+$installmentInfo['BookingInstallment']['owner_service_tax_amount'], 'type' => 'text'));
            }

            echo $this->Form->input('simplypark_service_tax_amount', array('value' => $installmentInfo['Booking']['simplypark_service_tax_amount']+$installmentInfo['BookingInstallment']['simplypark_service_tax_amount'], 'type' => 'text'));

            echo $this->Form->input('owner_income', array('value' => $installmentInfo['Booking']['owner_income']+$installmentInfo['BookingInstallment']['owner_income'], 'type' => 'text'));

            echo $this->Form->input('simplypark_income', array('value' => $installmentInfo['Booking']['simplypark_income']+$installmentInfo['BookingInstallment']['simplypark_income'], 'type' => 'text'));

            
         echo $this->Form->end();
    ?>
</div>
<?php
    echo '<script>
            var razorPayKeyID = ' . json_encode(Configure::read('RazorPayKeys')) . ';
            var BookingDetail = ' . json_encode($installmentInfo) . ';
        </script>';
?>
