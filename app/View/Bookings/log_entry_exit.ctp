<div class="header clearfix">
	
        <div class="pull-left logo">
		<!-- ======= LOGO ========-->
		<a class="navbar-brand" href="#">
			<?php
				echo $this->Html->link(
                    $this->Html->image(
                            'frontend/logo-simply-park.png',
                            array(
                                'class' => 'site-logo img-responsive'
                                )
                        ),
                        '/',
                        array(
                            'class' => 'navbar-brand scrollto',
                            'escape' => false
                    )
            	);
			?>
		</a>
	</div>
</div>
	
<div class="cloud-bg col-xs-12 clearfix">
	<h3>
        <?php if($entryOrExit == Configure::read('BookingEntryOrExit.entry') || 
                 $entryOrExit == Configure::read('BookingEntryOrExit.reEntry')) {
                 echo __(' ENTRY');
              }else if($entryOrExit == Configure::read('BookingEntryOrExit.exit')) {
                 echo __(' EXIT');
              }
         ?>
        </h3>
	<div class="clearfix">
		<div class="well clearfix well-opacity">
                       <table class="validateBookingTable" border="0" width="100%">
                       <tbody>
                          <tr>
                            <td style="font-size:36px;">
                              <?php 
                                 if($result == Configure::read('BookingValidationStatus.validBooking')) { 
                                    echo "<span class=\"label label-success label-as-badge\">";
                                    echo __('&nbsp;&nbsp;');
                                    echo "</span>";
                                 }else if($result == Configure::read('BookingValidationStatus.paymentDue')){
                                    echo "<span class=\"label label-warning label-as-badge\">";
                                    echo __('&nbsp;&nbsp;');
                                    echo "</span>";
                                 }else if($result == Configure::read('BookingValidationStatus.tooEarly')){
                                    //echo "<span class=\"label label-warning label-as-badge\">";
                                    echo "<span>";
                                    echo $this->Form->create(
                                                      null,
                                                       array(
                                                             'url' => '/bookings/log_entry_exit/'.$lang.'/'.$spaceId.'/'.$bookingId.'/'.$transId.'/1',
                                                             'type' => 'post',
                                                             )
                                                       );
                                    echo $this->Form->submit(
                                                          __('  '),
                                                          array(
                                                                 'class' => 'btn button-early-entry'
                                                               ) 
                                                            );
                                    echo $this->Form->end();
                                   
                                    echo "</span>";
                                    echo "<span style=\"font-size:12px\" class=\"label label-info label-as-badge\">";
                                    echo __('Press the circle to allow Entry');
                                    echo "</span>";
                                 }else {
                                    echo "<span class=\"label label-danger label-as-badge\">";
                                    echo __('&nbsp;&nbsp;');
                                    echo "</span>";
                                 }
                               ?>
                          </td>
                         </tr>
                         <tr>
                           <td>&nbsp;</td>
                         </tr>
                         <tr>
                           <td style="font-weight:bold;">
                               <?php if($result == Configure::read('BookingValidationStatus.validBooking')) {
                                           if($entryOrExit == Configure::read('BookingEntryOrExit.entry')){ 
                                              if($offlineUser){
                                                 echo __('Entry Recorded');
                                              }else{
                                                 echo __('Entry Allowed');
                                                 echo "<br />";
                                                 echo __('Car Number '. $carReg);
                                                 echo "<br />";
                                                 echo __('Entry Time '. $time);
                                                 if(!$isHourlyBooking){
                                                    echo "<br />";
                                                    echo __($longBookingDisplayString);
                                                 }
                                              }
                                           }else if ($entryOrExit == Configure::read('BookingEntryOrExit.exit')){
                                              echo __('Exit Recorded'); 
                                              echo __('<br />Exit Time '. $time);
                                              if(!$offlineUser){
                                                echo "<br /><span class=\"label label-success label-as-badge\">";
                                                echo __('No payments due');
                                                echo "</span>";
                                              }
                                           }else if($entryOrExit == Configure::read('BookingEntryOrExit.reEntry')){ 
                                                 echo __('Entry Allowed');
                                                 echo "<br />";
                                                 echo __('Car Number '. $carReg);
                                                 echo "<br />";
                                                 echo __('Entry Time '. $time);
                                                 echo "<br />";
                                                 echo __($longBookingDisplayString);
                                           }
                                     }else {
                                       if($result == Configure::read('BookingValidationStatus.noSuchBooking')) {
                                          echo __('No Such Booking');
                                       }else if ($result == Configure::read('BookingValidationStatus.bookingExpired')) {
                                          echo __('Booking is Expired');
                                       }else if ($result == Configure::read('BookingValidationStatus.bookingCancelled')) {
                                          echo __('Booking was Cancelled');
                                       }else if ($result == Configure::read('BookingValidationStatus.tooEarly')) {
                                          echo __('User has arrived early.<br /> Entry not allowed till ' .$preEntryTime);
                                       }else if ($result == Configure::read('BookingValidationStatus.tooLate')) {
                                          echo __('User arrived too Late. Entry was allowed till ' . $postEntryTime);
                                       }else if ($result == Configure::read('BookingValidationStatus.exitWithoutEntry')) {
                                          if($offlineUser) {
                                             echo __('Exit BarCode scanned but NO entry recorded');
                                          }
                                       }else if ($result == Configure::read('BookingValidationStatus.paymentDue')) {
                                             echo "<span class=\"label label-danger label-as-badge\">";
                                             echo __('Payment Due of Rs. ' . $paymentDue);
                                             echo "</span>";
                                             echo "<br /><br />";
                                             echo __($overstayString);
                                       }else if ($result == Configure::read('BookingValidationStatus.exitAlreadyRecorded')) {
                                             echo __('<u>Booking Pass has already been used</u>');
                                             echo "<br />";
                                             echo __('Last Used on  : ' . $exitTime);
                                             echo "<br />";
                                             echo __('with Car Number ' . $carReg);
                                       }
                                       else {
                                          echo __('Unapproved Booking');
                                       }
                                     }
                               ?>
                            </td>
                          </tr>
                        </tbody>
                        </table>
                </div>
        </div>
</div>
<div class="col-xs-12 clearfix">
  <div class="clearfix">

    <?php
     if($offlineUser) { 
        echo __("Need Help? Call SimplyPark at 7289-828463"); 
     }else{
        echo __("Need Help? Call SimplyPark at 7289-828463 and quote transaction ID <b>$transId</b>"); 
     }
    ?> 
  </div>
</div>
