<div class="crumbs">
    <ul id="breadcrumbs" class="breadcrumb">
        <li class="active">
            <i class="icon-user"></i>
            <?php
                echo $this->Html->link(
                        __('Manage Bookings'),
                        '#',
                        array(
                                'class' => 'active'
                            )
                    ); 
                
            ?>
        </li>
    </ul>
</div>
<div class="panel1">
    <span class="clearfix"></span>
    <div class="panel panel-default" id="inline-popups">
        <div class="panel-heading clearfix">
            <?php
                        echo $this->Form->create('User', array(
                                        'url' => '/admin/bookings/search_bookings',
                                        'type' => 'get',
                                        'novalidate' => true
                                        ));
                    ?>
                        <div class="row">
                            <div class="input-group col-xs-3 pull-right">
                                <?php
                                    echo $this->Form->input(
                                        'search',
                                        array(
                                            'div' => false,
                                            'label' => false,
                                            'class' => 'form-control',
                                            'placeholder' => 'search (Email,First Name,Last Name)',
                                            'escape' => false,
                                            'default' => (isset($this->request->query['search']) && !empty($this->request->query['search'])) ? $this->request->query['search'] : ''
                                            )
                                    );
                                ?>
                                <span class="input-group-btn">
                                    <?php
                                        echo $this->Form->button(
                                            '<i class="icon-search"></i>',
                                            array(
                                                'type' => 'submit',
                                                'class'=> 'btn btn-default',
                                                'escape' => false
                                                )
                                        );
                                    ?>
                                </span>
                            </div>
                        </div>
                    <?php echo $this->Form->end(); ?>
        </div>
        <div class="table-responsive" id="innercontent">
            <?php echo $this->element('backend/Booking/approved_bookings_list'); ?>
        </div>
    </div>
    <?php
        echo $this->Html->script(array(
            'backend/Space/index',
            'backend/commonmagnificpopup',
            'backend/Map',
            )
            //array('inline' => false)
        );
    ?>
</div>
<?php
    echo $this->element('backend/Space/view_space');
    echo $this->element('backend/Space/edit_space');
    echo $this->element('backend/approvespace_popup');
    echo $this->element('backend/disapprovespace_popup');
    echo $this->element('backend/setservicetaxcharges_popup');
?>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
