<div class="main-container">
   <div class="contact-bg">
      <div class="container">
         <section class="clearfix contact-main">
            <div class="col-sm-7">
               <h1><?php echo __('Pay Installment'); ?></h1>
                  <p>
                     <?php echo __("Below are the details of your installment : -"); ?>
                  </p>
                  <div class="row">
                     <div class="form-group col-sm-6">
                        <label for="name"><?php echo __('Space Name'); ?><span> *</span></label>
                        <?php echo $installmentInfo['Booking']['Space']['name']; ?>
                     </div>
                     <div class="form-group col-sm-6">
                        <label for="name"><?php echo __('Amount To Pay'); ?><span> *</span></label>
                        <?php echo $installmentInfo['BookingInstallment']['amount']; ?>
                     </div>
                     <div class="form-group col-sm-6">
                        <label for="Email"><?php echo __('Start Time'); ?><span> *</span></label>
                        <?php echo date('Y-m-d',strtotime($installmentInfo['BookingInstallment']['date_pay'])); ?>
                     </div>
                     <div class="form-group col-sm-6">
                        <label for="Email"><?php echo __('End Time'); ?><span> *</span></label>
                        <?php echo date('Y-m-d',strtotime($installmentInfo['BookingInstallment']['end_date'])); ?>
                     </div>
                     <div class="col-sm-12">
                        <div class="row">
                           <div class="col-sm-6 text-right">
                              <?php
                                 echo $this->Html->link(
                                          __('Pay'),
                                          '#',
                                          array(
                                                'escape' => false,
                                                'class' => 'btn btn-info btn-lg contact-btn-send'
                                             )
                                       );
                              ?>
                           </div>
                        </div>
                     </div>
                  </div>
            </div>
         </section>
      </div>
   </div>
</div>
