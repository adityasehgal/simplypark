<?php
   if ($this->Session->check('Message') && $this->Session->read('Message.flash.params') == 'success') {
      $url = array( 'controller' => 'homes', 'action' => 'index' );
      header('Refresh: 5; URL='.Router::url( $url ));
   }
   echo $this->Html->script('frontend/contactus/contactus', array('inline' => false));
?>
<div class="main-container">
   <div class="contact-bg">
      <div class="container">
         <section class="clearfix contact-main">
            <div class="col-sm-7">
               <h1><?php echo __('Contact Us'); ?></h1>
               <p>
                  <?php echo __("We are here to answer any questions you may have about our SimplyPark. Reach out to us and we'll respond as soon as we can."); ?>
               </p>
               <p>
                  <?php echo __("Even if there is something you have always wanted to experience and can't find it on SimplyPark, let us know and we promise we'll do our best to find it for you and send you there."); ?>
               </p>
               <?php
                  echo $this->Form->create(
                                       'ContactUs',
                                       array(
                                             'url' => '/homes/contactUs',
                                             'id' => 'contact-us',
                                             'inputDefaults' => array(
                                                   'div' => false,
                                                   'label' => false
                                                )
                                          )
                                    );
               ?>
                  <div class="row">
                     <div class="form-group col-sm-6">
                        <label for="name"><?php echo __('NAME'); ?><span> *</span></label>
                        <?php
                           echo $this->Form->input(
                                    'name',
                                    array(
                                          'class' => 'form-control',
                                          'value' => (isset($data['ContactUs'])) ? $data['ContactUs']['name'] : ''
                                       )
                                 );
                        ?>
                     </div>
                     <div class="form-group col-sm-6">
                        <label for="Email"><?php echo __('EMAIL'); ?><span> *</span></label>
                        <?php
                           echo $this->Form->input(
                                    'email',
                                    array(
                                          'class' => 'form-control',
                                          'value' => (isset($data['ContactUs'])) ? $data['ContactUs']['email'] : ''
                                       )
                                 );
                        ?>
                     </div>
                     <div class="col-sm-12 form-group">
                        <label for="exampleInputPassword1"><?php echo __('MESSAGE'); ?><span> *</span></label>
                        <?php
                           echo $this->Form->input('message',
                                    array(
                                          'class' => 'form-control',
                                          'type' => 'textarea',
                                          'rows' => 3,
                                          'maxlength' => 1000,
                                          'onkeyup' => 'countChar(this)',
                                          'value' => (isset($data['ContactUs'])) ? $data['ContactUs']['message'] : ''
                                       )
                                 );
                        ?>
                        <div class="help-info pull-right">
                           <small class="text-info" id="charNum">
                              
                           </small>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="g-recaptcha" data-sitekey="<?php echo Configure::read('GoogleSiteSecretKey.SiteKey'); ?>"></div>
                           </div>
                           <div class="col-sm-6 text-right">
                              <?php
                                 echo $this->Form->button('SEND', array(
                                     'type' => 'submit',
                                     'class' => 'btn btn-info btn-lg contact-btn-send',
                                     'escape' => true
                                 ));
                              ?>
                           </div>
                        </div>
                     </div>
                  </div>
               <?php echo $this->Form->end(); ?>
            </div>
            <div class="col-sm-4 col-sm-offset-1 contact-right">
               <div class="row">
                  <div class="col-sm-8 col-center">
                     <p><strong><?php echo __('EMAIL'); ?></strong><br>
                        <span><?php echo __('customercare@simplypark.in'); ?></span>
                     </p>
                     <p><strong><?php echo __('CONNECT WITH US'); ?></strong><br>
                       <a href="https://www.twitter.com/simplyparkin"><i class="fa fa-twitter fa-fw"></i></a>
                        &nbsp;&nbsp;
                        <a href="https://www.facebook.com/simplypark.in"><i class="fa fa-facebook fa-fw"></i></a><br />
                     </p>
                     <p class="contact-address">
                        <strong><?php echo __('JUST DROP IN'); ?></strong>
                     </p>
                     <p>SimplyPark<br/>
                        C-460, LGF, C.R. Park<br/>
                        New Delhi-110019<br />
                        India
                     </p>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<script src="https://www.google.com/recaptcha/api.js"></script>
