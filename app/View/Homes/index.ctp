<!-- ==============================================
HEADER
=============================================== -->
<?php
    echo $this->Html->script('frontend/homes/index', array('inline' => false));
?>
<?php echo $this->element('frontend/Home/search'); ?>         

<!-- ==============================================
How it Work
=============================================== -->
<?php echo $this->element('frontend/Home/how_it_works'); ?>

<!-- ==============================================
Popular venues
=============================================== -->
<?php
	if (!empty($getPopularEvents)) {
		echo $this->element('frontend/Home/popular_venues');
	}
?>

<!-- ==============================================
How Your Own Parking Place
=============================================== -->
<?php echo $this->element('frontend/Home/parking_place'); ?>

<!-- ==============================================
Happy Customers
=============================================== -->
<?php echo $this->element('frontend/Home/happy_customers'); ?>  

<!-- ==============================================
Our Partner
=============================================== -->
<?php
	if (!empty($getOurPartners)) {
		echo $this->element('frontend/Home/our_partners');
	}
?>