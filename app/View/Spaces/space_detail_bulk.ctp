<?php echo $this->Html->script(array('frontend/Space/space_location','frontend/Space/bulkdatetimepicker'), array('inline' => false)); ?>
<div class="main-container">
	<div class="event-banner">
		<div class="container">
			<section class="clearfix">			
				<div class="col-sm-3 img-block show-more-info hide">
					<?php 

						if (!empty($getSpaceDetail['Space']['place_pic'])) {
							if (file_exists(Configure::Read('SpaceImagePath').'place_pic.'.$getSpaceDetail['Space']['place_pic'])) {
								echo $this->Html->image(
									'/'.Configure::Read('SpaceImagePath').'place_pic.'.$getSpaceDetail['Space']['place_pic'],
									array(
										    'class' => 'img-responsive'
										)
								);
							} else {
								echo $this->Html->image('Not_available.jpg',
									array(
										    'class' => 'img-responsive'
										)
								);
							}
						} else {
							echo $this->Html->image('Not_available.jpg',
								array(
									    'class' => 'img-responsive'
									)
							);
						}
					?>
				</div>
				<div class="col-sm-9 quick-parking-details">
					<h2>
						<?php echo $getSpaceDetail['Space']['name']; ?>
						<a class="success" href="#"><small class="more-info">More</small></a>
						<small class="hide show-more-info pill pill-<?php echo $getSpaceDetail['Space']['booking_confirmation'] ? 'success' : 'danger'; ?>"> 
							<?php echo $getSpaceDetail['Space']['booking_confirmation'] ? 'Immediately confirmed' : 'This booking requires a confirmation from the Owner'; ?>

						</small>
					</h2>
					<p class="hide show-more-info"><?php echo nl2br($getSpaceDetail['Space']['description']); ?></p>
				</div>		
			</section>
		</div>
	</div>
	
	<div class="container">
		<div class="parking-details clearfix">
			<div class="col-sm-6">
				<div class="well timeframe clearfix">
					<h4>
						<span class="glyphicon glyphicon-calendar"></span>
						<?php echo __('Your Parking Timeframe'); ?>
					</h4>
					<span class="clearfix operating-hours">
						<?php
							$operatingHours = '24 hours';
                                                        if (!$getSpaceDetail['SpaceTimeDay']['open_all_hours']) {
                                                                $fromHours = date('H:i a',strtotime($getSpaceDetail['SpaceTimeDay']['from_date']));
								$toHours = date('H:i a',strtotime($getSpaceDetail['SpaceTimeDay']['to_date']));
								$fromHoursInt = substr($fromHours,0,2);
								$toHoursInt = substr($toHours,0,2);
								$operatingHours = $fromHours . ' to ' . $toHours;
								if($toHoursInt < $fromHoursInt){
									$operatingHours .= " (next day)";
								}
							}
							echo 'Operating Hours : '.$operatingHours;
						?>
					</span>
					<small><?php echo __('Change the number of slots, start and end times below to when you\'ll need parking.'); ?></small>
					<?php
						echo $this->Form->create(
								'Space',
								array(
										'url' => array('controller' => 'bookings',
                                                                                               'action' => 'bulkIndex/0/1'),
										'type' => 'post',
										'id' => 'book-space'
									)
							);
							echo $this->Form->hidden(
									'space_id',
									array(
											'value' => $getSpaceDetail['Space']['id'],
											'class' => 'hidden-space-id'
										)
								);
							echo $this->Form->hidden(
									'is_Event',
									array(
											'value' => 0,
											'class' => 'hidden-is-event'
										)
								);
					?>
						<div class="row">
							<div class="clearfix">
								<div class="col-sm-4">
									<div class="form-group">
										<label for="num_slots" class="text-primary"><?php echo __('Slots'); ?></label> 
										<div class='input-group' id='numSlots'>
											<?php
						                		echo $this->Form->input('num_slots', array(
						                			'div' => false,
						                			'label' => false,
						                			'id' => 'numSlots',
						                			'class' => 'form-control',
						                			'placeholder' => 'Slots to Book',
						                			'options' => $slotOptions,
                                                                                        'default' => $maxSlots/2,
						                			)
						                		);
						                	?>
	                					              </div>
									</div>									
								</div>
                                                            </div>
                                                   </div>
						<div class="row">
							<div class="clearfix">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="start_date" class="text-primary"><?php echo __('Start Date & Time'); ?></label>
										<div class='input-group date' id='date1' data-date-format="mm/dd/yyyy hh:ii">
											<?php
												$start_date = null;
												if(isset($this->request->query['stdate'])) {
													$start_date = urldecode(base64_decode($this->request->query['stdate']));
												}
						                		echo $this->Form->input('start_date', array(
						                			'div' => false,
						                			'label' => false,
						                			'id' => 'start-date',
						                			'type' => 'text',
						                			'class' => 'form-control',
						                			'placeholder' => 'Date',
						                			'value' => $start_date
						                			)
						                		);
						                	?>
						                	<span class="input-group-addon">
						                        <span class="glyphicon glyphicon-calendar"></span>
						                    </span>
	                					</div>
									</div>									
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="end_date" class="text-primary"><?php echo __('End Date & Time'); ?></label>
										<div class='input-group date' id='date2' data-date-format="mm/dd/yyyy hh:ii">
											<?php
												$end_date = null;
												if(isset($this->request->query['edate'])) {
													$end_date = urldecode(base64_decode($this->request->query['edate']));
												}
						                		echo $this->Form->input('end_date', array(
						                			'div' => false,
						                			'label' => false,
						                			'id' => 'end-date',
						                			'type' => 'text',
						                			'class' => 'form-control',
						                			'placeholder' => 'Date',
						                			'value' => $end_date,
						                			'readonly' => true
						                			)
						                		);
						                	?>
						                	<span class="input-group-addon">
						                        <span class="glyphicon glyphicon-calendar"></span>
						                    </span>
	                					</div>
									</div>
								</div>
								<div class="col-xs-12 text-center">
									<h1 class="fa fa-inr space-price hide"></h1>
									<h5 class="hide deposit-content"></h5>
									<h5 class="hide booking-tip"></h5>
                                                                        <h5 class="hide sp-commission"></h5>
									<div class="help-info">
										<small class="text-danger" id="show-error">
											
										</small>
									</div>
									<div class="form-group">
										<?php
											echo $this->Form->submit(
													__('Book Now'),
													array(
															'class' => 'btn btn-info btn-block book-now-button',
															'escape' => false,
															'data-user' => $this->Session->check('Auth.User') ? true : false
														)
												);
										?>
									</div>
								</div>
							</div>
						</div>
					<?php echo $this->Form->end(); ?>
				</div>
				<article>
					<table class="table">
						<thead>
							<tr>
								<?php if ($bulkBookingSettings['BulkBookingSetting']['hourly_price'] != Configure::read('SpaceUnavailabelPrice')) { ?>
									<th><?php echo __('Hourly'); ?></th>
								<?php } if ($bulkBookingSettings['BulkBookingSetting']['daily_price'] != Configure::read('SpaceUnavailabelPrice')) { ?>
									<th><?php echo __('Daily'); ?></th>
								<?php } if ($bulkBookingSettings['BulkBookingSetting']['weekly_price'] != Configure::read('SpaceUnavailabelPrice')) { ?>
									<th><?php echo __('Weekly'); ?></th>
								<?php } if ($bulkBookingSettings['BulkBookingSetting']['monthly_price'] != Configure::read('SpaceUnavailabelPrice')) { ?>
									<th><?php echo __('Monthly'); ?></th>
								<?php } if ($bulkBookingSettings['BulkBookingSetting']['yearly_price'] != Configure::read('SpaceUnavailabelPrice')) { ?>
									<th><?php echo __('Yearly'); ?></th>
								<?php } ?>
							</tr>
						</thead>
						<tbody>
							<tr>
								<?php if ($bulkBookingSettings['BulkBookingSetting']['hourly_price'] != Configure::read('SpaceUnavailabelPrice')) { ?>
									<td><i class="fa <?php echo !empty($bulkBookingSettings['BulkBookingSetting']['hourly_price']) ? 'fa-inr' : ''; ?>"></i><?php echo !empty($bulkBookingSettings['BulkBookingSetting']['hourly_price']) ? $bulkBookingSettings['BulkBookingSetting']['hourly_price'] : 'N/A'; ?></td>
								<?php } if ($bulkBookingSettings['BulkBookingSetting']['daily_price'] != Configure::read('SpaceUnavailabelPrice')) { ?>
									<td><i class="fa <?php echo !empty($bulkBookingSettings['BulkBookingSetting']['daily_price']) ? 'fa-inr' : ''; ?>"></i><?php echo !empty($bulkBookingSettings['BulkBookingSetting']['daily_price']) ? $bulkBookingSettings['BulkBookingSetting']['daily_price'] : 'N/A'; ?></td>
								<?php } if ($bulkBookingSettings['BulkBookingSetting']['weekly_price'] != Configure::read('SpaceUnavailabelPrice')) { ?>
									<td><i class="fa <?php echo !empty($bulkBookingSettings['BulkBookingSetting']['weekly_price']) ? 'fa-inr' : ''; ?>"></i><?php echo !empty($bulkBookingSettings['BulkBookingSetting']['weekly_price']) ? $bulkBookingSettings['BulkBookingSetting']['weekly_price'] : 'N/A'; ?></td>
								<?php } if ($bulkBookingSettings['BulkBookingSetting']['monthly_price'] != Configure::read('SpaceUnavailabelPrice')) { ?>
									<td><i class="fa <?php echo !empty($bulkBookingSettings['BulkBookingSetting']['monthly_price']) ? 'fa-inr' : ''; ?>"></i><?php echo !empty($bulkBookingSettings['BulkBookingSetting']['monthly_price']) ? $bulkBookingSettings['BulkBookingSetting']['monthly_price'] : 'N/A'; ?></td>
								<?php } if ($bulkBookingSettings['BulkBookingSetting']['yearly_price'] != Configure::read('SpaceUnavailabelPrice')) { ?>
									<td><i class="fa <?php echo !empty($bulkBookingSettings['BulkBookingSetting']['yearly_price']) ? 'fa-inr' : ''; ?>"></i><?php echo !empty($bulkBookingSettings['BulkBookingSetting']['yearly_price']) ? $bulkBookingSettings['BulkBookingSetting']['yearly_price'] : 'N/A'; ?></td>
								<?php } ?>
							</tr>
						</tbody>
					</table>
				</article>
				<?php
					if ($getSpaceDetail['SpaceFacility']) {
				?>
					<article class="clearfix">
						<span><?php echo __('AMENTIES'); ?></span>
				        <dl class="dl-horizontal">
				        	<?php $i = 0;
								foreach ($getSpaceDetail['SpaceFacility'] as $amenities) {
									if ($i%2 == 0) { 
							?>
										<dt><?php echo $amenities['Facility']['name']; ?></dt>
							<?php
									} else {
							?>
										<dd><?php echo $amenities['Facility']['name']; ?></dd>
							<?php
									}
							 	$i++;
							 	}
							?>
						</dl>
					</article>
				<?php } ?>
				<?php 
            		if($getSpaceDetail['PropertyType']['id'] != Configure::read('gated_community')) {
				?>
				<article>
					<address>
						<span><?php echo __('ADDRESS'); ?></span><br>
						<p><?php echo $getSpaceDetail['Space']['flat_apartment_number'] . ', ' . $getSpaceDetail['Space']['address1']; ?>,</p>
						<p><?php echo $getSpaceDetail['Space']['address2']; ?></p>
						<p><?php echo $getSpaceDetail['Space']['post_code']; ?></p>
					</address>
				</article>
				<?php } ?>

				<article>
					<ul>
						<li><i class="fa <?php echo $getSpaceDetail['SpaceTimeDay']['mon'] == Configure::read('Bollean.True') ? 'fa-check right-check' : 'fa-times cross-icon'; ?>"></i> <?php echo __('Monday'); ?></li>
						<li><i class="fa <?php echo $getSpaceDetail['SpaceTimeDay']['tue'] == Configure::read('Bollean.True') ? 'fa-check right-check' : 'fa-times cross-icon'; ?>"></i> <?php echo __('Tuesday'); ?></li>
						<li><i class="fa <?php echo $getSpaceDetail['SpaceTimeDay']['wed'] == Configure::read('Bollean.True') ? 'fa-check right-check' : 'fa-times cross-icon'; ?>"></i> <?php echo __('Wednesday'); ?></li>
						<li><i class="fa <?php echo $getSpaceDetail['SpaceTimeDay']['thu'] == Configure::read('Bollean.True') ? 'fa-check right-check' : 'fa-times cross-icon'; ?>"></i> <?php echo __('Thursday'); ?></li>
						<li><i class="fa <?php echo $getSpaceDetail['SpaceTimeDay']['fri'] == Configure::read('Bollean.True') ? 'fa-check right-check' : 'fa-times cross-icon'; ?>"></i> <?php echo __('Friday'); ?></li>
						<li><i class="fa <?php echo $getSpaceDetail['SpaceTimeDay']['sat'] == Configure::read('Bollean.True') ? 'fa-check right-check' : 'fa-times cross-icon'; ?>"></i> <?php echo __('Saturday'); ?></li>
						<li><i class="fa <?php echo $getSpaceDetail['SpaceTimeDay']['sun'] == Configure::read('Bollean.True') ? 'fa-check right-check' : 'fa-times cross-icon'; ?>"></i> <?php echo __('Sunday'); ?></li>
					</ul>
				</article>

				<article class="clearfix">
					<div class="row">
						<div class="col-xs-6">
							<span><?php echo __('AVAILABLE SPACES'); ?></span><br>
							<?php echo !empty($getSpaceDetail['Space']['number_slots']) ? $getSpaceDetail['Space']['number_slots'] : 0; ?>
						</div>
						<!-- <div class="col-xs-6">
							<span><?php echo __('AVAILABLE SMALL CAR SPACES'); ?></span><br>
							<?php echo !empty($getSpaceDetail['SmallCarSpace']['small_car_slot']) ? $getSpaceDetail['SmallCarSpace']['small_car_slot'] : 0; ?>
						</div> -->
					</div>
				
				</article>

				<?php if ($getSpaceDetail['Space']['property_type_id'] == 2) { ?>
					<article class="clearfix">
						<div class="row">
							<div class="col-xs-6">
								<span><?php echo __('PROPERTY TYPE'); ?></span><br>
								<?php echo 'Gated Community' ;?>
							</div>
							<div class="col-xs-6">
								<span><?php echo __('TOWER NUMBER'); ?></span><br>
								<?php echo $getSpaceDetail['Space']['tower_number']; ?>
							</div>
						</div>
					</article>
				<?php } ?>

				<?php if (!empty($getSpaceDetail['Space']['calcellation_policy_id'])) { ?>
					<article class="clearfix cancel-policy">
					  	<span><?php echo __('CANCELLATION  POLICY'); ?></span>
						<p><?php echo $getSpaceDetail['CancellationPolicy']['policy']; ?></p>
					</article>
				<?php } ?>
			</div>

			<div class="col-sm-6" id="space-on-map">
				
			</div>
		</div>
	</div>
</div>
<?php
	if (!empty($getSpaceDetail['Space']['admin_description'])) {
		echo $this->element('frontend/Space/space_description');
	}
	echo '<script>
			var spacesForMap = ' . json_encode($getSpaceDetail).';
			var bulkBookingSettings = ' . json_encode($bulkBookingSettings).';
			var serverName = ' . json_encode($_SERVER['SERVER_NAME']).';
			var currentAction = '.json_encode($this->here).';
			var spaceAvailableFromDate = '.json_encode(date('Y-m-d', strtotime($getSpaceDetail['SpaceTimeDay']['from_date']))).';
			var spaceAvailableToDate = '.json_encode(date('Y-m-d', strtotime($getSpaceDetail['SpaceTimeDay']['to_date']))).';
			var spaceAvailableFromTime = '.json_encode(date('h:i', strtotime($getSpaceDetail['SpaceTimeDay']['from_date']))).';
			var spaceAvailableToTime = '.json_encode(date('h:i', strtotime($getSpaceDetail['SpaceTimeDay']['to_date']))).';
                        var eventTiming = \'\';
		        </script>';
?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
