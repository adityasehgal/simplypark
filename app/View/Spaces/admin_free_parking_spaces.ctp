<div class="crumbs">
    <ul id="breadcrumbs" class="breadcrumb">
        <li class="active">
            <i class="icon-user"></i>
            <?php
                echo $this->Html->link(
                        __('Manage Spaces'),
                        '/admin/spaces/free_parking_spaces',
                        array(
                                'class' => 'active'
                            )
                    ); 
            ?>
        </li>
        <div class="pull-right">
            <?php
                echo $this->Html->link('<i class="icon-plus icon-spacing"></i>'.__('Add New Parking Space'),
                        '#',
                        array(
                            'class' => 'btn btn-primary btn-grad btn-bottom',
                            'data-toggle' => 'modal',
                            'data-target' => '#addFreeParkingSpace',
                            'escape' => false
                        ));
            ?>
        </div>

    </ul>
</div>
<div class="panel1">
    <span class="clearfix"></span>
    <div class="panel panel-default" id="inline-popups">
        <div class="panel-heading clearfix">
            <div class="row">
                <div class="col-xs-6">
                    <ul class="list-inline abbrevation">
                    </ul>
                </div>
                <div class="col-xs-6 right-inner-addon row">
                    <?php
                        echo $this->Form->create('User', array(
                                        'url' => '/admin/spaces/free_parking_spaces',
                                        'type' => 'get',
                                        'novalidate' => true
                                        ));
                    ?>
                        <div class="row">
                            <div class="input-group col-xs-6 pull-right">
                                <?php
                                    echo $this->Form->input(
                                        'search',
                                        array(
                                            'div' => false,
                                            'label' => false,
                                            'class' => 'form-control',
                                            'placeholder' => 'search',
                                            'escape' => false,
                                            'default' => (isset($this->request->query['search']) && !empty($this->request->query['search'])) ? $this->request->query['search'] : ''
                                            )
                                    );
                                ?>
                                <span class="input-group-btn">
                                    <?php
                                        echo $this->Form->button(
                                            '<i class="icon-search"></i>',
                                            array(
                                                'type' => 'submit',
                                                'class'=> 'btn btn-default',
                                                'escape' => false
                                                )
                                        );
                                    ?>
                                </span>
                            </div>
                        </div>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
        <div class="table-responsive" id="innercontent">
            <?php echo $this->element('backend/Space/free_space_listing'); ?>
        </div>
    </div>
    <?php
        echo $this->Html->script(array(
            'backend/Space/index',
            'backend/commonmagnificpopup',
            'backend/Map',
            )
            //array('inline' => false)
        );
    ?>
</div>
<?php
    echo $this->element('backend/Space/view_space');
    echo $this->element('backend/Space/edit_space');
    echo $this->element('backend/approvespace_popup');
    echo $this->element('backend/disapprovespace_popup');
    echo $this->element('backend/AdminSettings/add_free_space');
?>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
