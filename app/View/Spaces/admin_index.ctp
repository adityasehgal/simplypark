<div class="crumbs">
    <ul id="breadcrumbs" class="breadcrumb">
        <li class="active">
            <i class="icon-user"></i>
            <?php
                echo $this->Html->link(
                        __('Manage Spaces'),
                        '/admin/spaces/',
                        array(
                                'class' => 'active'
                            )
                    ); 
            ?>
        </li>
    </ul>
</div>
<div class="panel1">
    <span class="clearfix"></span>
    <div class="panel panel-default" id="inline-popups">
        <div class="panel-heading clearfix">
            <div class="row">
                <div class="col-xs-6">
                    <ul class="list-inline abbrevation">
                        <li>
                            <span class="color-box space-row-color-approve"></span> <?php echo __('Approved'); ?>
                        </li>
                        <li>
                            <span class="color-box space-row-color-not-approve"></span> <?php echo __('Disapproved'); ?>
                        </li>
                        <li>
                            <span class="color-box btn-default"></span> <?php echo __('Decision Pending'); ?>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-6 right-inner-addon row">
                    <?php
                        echo $this->Form->create('User', array(
                                        'url' => '/admin/spaces/',
                                        'type' => 'get',
                                        'novalidate' => true
                                        ));
                    ?>
                        <div class="row">
                            <div class="input-group col-xs-3 pull-right">
                                <?php
                                    echo $this->Form->input(
                                        'search',
                                        array(
                                            'div' => false,
                                            'label' => false,
                                            'class' => 'form-control',
                                            'placeholder' => 'search',
                                            'escape' => false,
                                            'default' => (isset($this->request->query['search']) && !empty($this->request->query['search'])) ? $this->request->query['search'] : ''
                                            )
                                    );
                                ?>
                                <span class="input-group-btn">
                                    <?php
                                        echo $this->Form->button(
                                            '<i class="icon-search"></i>',
                                            array(
                                                'type' => 'submit',
                                                'class'=> 'btn btn-default',
                                                'escape' => false
                                                )
                                        );
                                    ?>
                                </span>
                            </div>
                        </div>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
        <div class="table-responsive" id="innercontent">
            <?php echo $this->element('backend/Space/listing'); ?>
        </div>
    </div>
    <?php
        echo $this->Html->script(array(
            'backend/Space/index',
            'backend/commonmagnificpopup',
            'backend/Map',
            )
            //array('inline' => false)
        );
    ?>
</div>
<?php
    echo $this->element('backend/Space/view_space');
    echo $this->element('backend/Space/edit_space');
    echo $this->element('backend/approvespace_popup');
    echo $this->element('backend/disapprovespace_popup');
    echo $this->element('backend/setservicetaxcharges_popup');
?>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>