<?php echo $this->Html->script(array('frontend/event/event_locations', 'frontend/event/all_spaces_map'), array('inline' => false)); ?>
<div class="main-container">
	<div class="event-banner">
		<div class="container">
			<section class="clearfix">			
				<div class="col-sm-3 img-block"> 
					<?php 
						// //echo $this->Html->image(
						// 	'/' . Configure::read('EventImagePath') . $getEventData['Event']['event_pic'],
						// 	array(
						// 		    'alt' => 'IPL Cricket',
						// 		    'class' => 'img-responsive'
						// 		)
						// );
					?>
				</div>
				<div class="col-sm-9 ">
					<h2><?php //echo $getEventData['Event']['event_name'] . '-' . $getEventData['EventAddress']['City']['name']; ?></h2>
					<p><?php //echo nl2br($getEventData['Event']['description']); ?></p>
				</div>		
			</section>
		</div>
	</div>

	<div class="container">
		<div class="col-md-12 parking-details">
			<h3><?php  echo __('Showing results for'); ?> 
				<strong>
					<?php 
						if(!empty($_GET['search_string'])) {
							echo $_GET['search_string'];
						} else {
							echo '<i>Nothing</i>';
						}
			 		?> 
			 	</strong>
		 	</h3>
		 	<div id="resultStats">
		 		<?php 
		 			if(count($response) > 0) {
		 				echo '<strong>'.count($response).'</strong> results';
		 			} else {
		 				echo 'Your search did not match any parking space.';


		 			}
		 		?>
		 	</div>
		</div>
	</div>
		<div class="container">
			<section class="clearfix">
				<?php if (!empty($response)) {?>

					<div class="col-md-4 parkingspace-left-section">
						<?php foreach ($response as $value) { ?>
								
								<div class="location-name clearfix" onclick="showSpace(<?php echo $value['Space']['id']; ?>);" id="SPACEID-<?php echo $value['Space']['id']; ?>">
									<div class="col-md-12">
										<h4><span class="map-marker-icon space-title"></span>
										<span class="parking-name"><?php echo $value['Space']['name']; ?> </span></h4>
									</div>

									<div class="col-xs-12">
										<dl class="dl-horizontal">
											<dt><?php echo __('Price:'); ?></dt>
											
											<dd>
												<?php if (!empty($value['SpacePricing']['hourly']) && $value['SpacePricing']['hourly'] > 0) {
        											echo '&#8377 '.$value['SpacePricing']['hourly'].' /hr';
											    } else if (!empty($value['SpacePricing']['daily']) && $value['SpacePricing']['daily'] > 0) {
											        echo '&#8377 '.$value['SpacePricing']['daily'].' /day';
											    } else if (!empty($value['SpacePricing']['weekely']) && $value['SpacePricing']['weekely'] > 0) {
											        echo '&#8377 '.$value['SpacePricing']['weekely'].' /week';
											    } else if (!empty($value['SpacePricing']['monthly']) && $value['SpacePricing']['monthly'] > 0) {
											        echo '&#8377 '.$value['SpacePricing']['monthly'].' /month';
											    } else if (!empty($value['SpacePricing']['yearly']) && $value['SpacePricing']['yearly'] > 0) {
											        echo '&#8377 '.$value['SpacePricing']['yearly'].' /annum';
											    } else {
											        echo 'N/A';
											    } ?>
											</dd>
											<!-- <dt><?php echo __('Distance:'); ?></dt>
											<dd><?php// echo round($value['distance']).' km(s)'; ?></dd> -->
											<dt><?php echo __('Available slots:'); ?></dt>
											<dd><?php echo $value['Space']['number_slots'] != '' ? $value['Space']['number_slots']: 'N/A' ; ?></dd>
										</dl>
									</div>
									
									<div class="col-xs-6">
										<?php
											echo $this->Html->link(
													__('View Details'),
													'#',
													array(
															'class' => 'view-details space-details',
															'data-id' => urlencode(base64_encode($value['Space']['id']))
														)
												);
										?>
									</div>
									<div class="col-xs-6 text-right">
										<?php
											echo $this->Html->link(
														__('BOOK NOW'),
														'/spaces/spaceDetail/'.urlencode(base64_encode($value['Space']['id'])),
														array(
																'escape' => false,
																'class' => 'btn btn-info'
															)
													);
										?>
									</div>
								</div>
						<?php } ?>
					</div>
					<div class="col-md-8">
						<div id="map_wrapper">
						    <div id="map_canvas" class="mapping"></div>
						</div>
					</div>
				<?php } else { ?>
					<div class="col-md-12 text-center gray-bg">
						<strong><?php echo __('No Record Found.'); ?></strong>
					</div>
				<?php } ?>
			</section>
		</div>
</div>
<?php echo $this->element('frontend/Space/space_info'); 
	echo '<script>var nearestSpacesForMap = ' . json_encode($response) . ';</script>';
?>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
