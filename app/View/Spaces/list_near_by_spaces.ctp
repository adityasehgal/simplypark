<?php echo $this->Html->script(array('frontend/event/event_locations', 'frontend/Space/list_near_by_space_map','frontend/Space/datetimepickernearby'), array('inline' => false)); ?>
<div class="map-space-container">
 <div class="mapSpaceView container-fluid">
  <section class="clearfix">
   <div class="col-md-12 mapView">
    <div id="map_wrapper" class="map-height">
       <div id="map_canvas" class="mapping"></div>
     </div>
   </div><!--col-md-12 mapView-->

   <!--the space display section starts here -->
          <div class="col-xs-4 spaceOverMaps">
           <div id="space-over-maps-header">
	    <span class="space-over-maps-header-heading">
               <strong><?php echo $getSpaceDetail['Space']['name'];  ?></strong></span>
	   </div><!--space-over-maps-header-->
           
           <!--From & TO form -->
	   <div class="well well-sm timeframe clearfix" style="margin-bottom:0px;">
	     <div class="row row1">
              <div class="col-xs-12">
                 <h4>
	            <span class="glyphicon glyphicon-calendar"></span>
	            <?php echo __('When would you like to park?'); ?>
	       </h4>
              </div>
             </div><!--row1--> 
	     <div class="row row2">
              <div class="col-xs-12">
                 <small><?php echo __('Choose start and end times from below.'); ?></small>
              </div>
             </div><!--row2--> 
	     <div class="row row3">
	        <?php
	       echo $this->Form->create(
	   	  'Space',
	           array(
		   'url' => 'listNearBySpaces',
		   'type' => 'post',
		   'id' => 'select-nearby-space',
		   )
		);
	     ?>
	      <div class="col-xs-6">
	       <div class="form-group">
	         <?php echo $this->Form->hidden(
					'filter_results',
					array(
						'value' => 1,
						'class' => 'hidden-filter-results'
		 			    )
				      );
                 ?>
	        <?php echo $this->Form->hidden(
					'spaceID',
					array(
						'value' => $encodedSpaceID,
						'class' => 'hidden-space-id'
		 			    )
				      );
                ?>
	        <label for="start_date" class="text-primary"><?php echo __('Start Date & Time') ?></label>
                <div class='input-group date' id='date1' date-date-format="mm/dd/yyyy hh:ii">
	         <?php	
                  $start_date = null;
	       	  echo $this->Form->input(
                     'start_date', 
                      array(
	    	        'div' => false,
			'label' => false,
			'id' => 'start-date',
                        'type' => 'text',
		        'empty' => 'Please Select',
			'class' => 'form-control',
                        'placeholder' => 'Date',
		        )
		   );
		 ?>
		  <span class="input-group-addon">
		   <span class="glyphicon glyphicon-calendar"></span>
	          </div><!--date1-->
		 </div><!--form-group-->
                </div><!--col-xs-6-->
		<!--TO date-->
                <div class="col-xs-6">
	         <div class="form-group">
		  <label for="end_date" class="text-primary"><?php echo __('End Date & Time') ?></label>
                  <div class='input-group date' id='date2' date-date-format="mm/dd/yyyy hh:ii">
		   <?php	
                     $end_date = null;
		     echo $this->Form->input(
                          'end_date', 
                           array(
				'div' => false,
				'label' => false,
				'id' => 'end-date',
                                'type' => 'text',
			        'empty' => 'Please Select',
				'class' => 'form-control',
                                'placeholder' => 'Date',
                                'readonly' => true
				)
			);
		     ?>
		     <span class="input-group-addon">
		     <span class="glyphicon glyphicon-calendar"></span>
	            </div><!--date2-->
	         </div><!--form-group-->
		</div><!--col-xs-6-->
	     </div><!--row3-->
            <!--search button -->
            <div class="row row4">
 	      <div class="col-xs-12">
	        <div class="form-group">
		   <?php
		    echo $this->Form->submit(
		           __('Search'),
			   array(
			   'class' => 'btn btn-info btn-block',
			   'escape' => false,
			   'data-user' => $this->Session->check('Auth.User') ? true : false
			    )
		        );
	           ?>
		</div><!--form-group-->
	       </div><!--col-xs-12-->
            </div><!--row4-->
	    <?php echo $this->Form->end(); ?>
	   </div><!--well well-sm timeframe clearfix-->

	  <!--parking space description starts here -->
	   <div class="parkingspace-left-section">
         <?php if(!empty($listNearBySpaces)) 
           { $itr = 0;?>
             <?php foreach($listNearBySpaces as $showNearestSpaces)
                   { ?>
		     <div class="parkingspace-details">
	                <div class="location-name clearfix container-fluid" 
                             onclick="showSelectedSpace(<?php echo $showNearestSpaces['Space']['id'].",$itr"; ?>);" 
			     id="SPACEID-<?php echo $showNearestSpaces['Space']['id']; ?>">
			  <div class="row NameRow">
                               <!-- PARK ICON + NAME -->
                               <div class="col-xs-1 nopadding">
                                   <span class="map-marker-icon-mod space-title"></span>  
                                </div>
                                <div class="col-xs-11 nopadding">
                                    <span class="text-primary text-left"><?php echo $showNearestSpaces['Space']['name']; ?></span>
			        </div>
                             </div><!--row NameRow-->
                            
                               <!-- First Line of Address -->
			       <div class="row spaceDetailPriceRow">
				  <div class="col-xs-6">
				    <div class="row row1 text-success padding-left">
				     <?php //echo $showNearestSpaces['Space']['flat_apartment_number'] ?>
				     <?php echo $showNearestSpaces['Space']['address1'];?>
                                     </div><!--row row1 text-success-->
                                     <!-- Distance -->
			             <div class="row row2 distance text-left padding-left">
				         <?php echo round($showNearestSpaces[0]['distance']) . " km(s) away";?>
			            </div><!-- row2 -->
			            <!-- Facilities -->
                      <?php if(0 && !empty($showNearestSpaces['SpaceFacility']))
		            { ?>
			          <div class="row row3">
                                     <?php foreach($showNearestSpaces['SpaceFacility'] as $facility)
				            {
                                              $iconName = "/simplypark/files/FacilityIcon/icon.".$facility['Facility']['icon'];
                                              $title =  $facility['Facility']['name'];
                                              echo "<img src=$iconName title=$title alt=$title>"; 
                                              echo "&nbsp&nbsp;";
                                           }
                                     ?>
			          </div><!--row3-->
                      <?php
		            } 
                       ?>
                             <!-- View Details -->
                             <div class="row row4 padding-left">
				<?php
				   echo $this->Html->link(
				  	  __('View Details'),
					     '#',
				           array(
						'class' => 'view-details space-details',
						'data-id' => urlencode(base64_encode($showNearestSpaces['Space']['id']))
					         )
				         );
				?>
                              </div><!--row4-->
			     </div><!--col-xs-6-->

                             <div class="col-xs-6">
                                <!--PRICE DISPLAY-->
                                <div class="row row1 text-right padding-right-price"> 
                                <?php if(!$showNearestSpaces['Space']['is_parking_free'])
				{
					if(!empty($showNearestSpaces['SpacePricing'][0]['hourly']) &&
						$showNearestSpaces['SpacePricing'][0]['hourly'] > 0)
					{
						echo '<span class="pricing-display">';
						echo '&#8377 '.$showNearestSpaces['SpacePricing'][0]['hourly'].' /hr';
						echo '</span>';
					}elseif($showNearestSpaces['Space']['tier_pricing_support']) 
					{
						echo $showNearestSpaces['Space']['tier_formatted_desc'];
					}elseif(!empty($showNearestSpaces['SpacePricing'][0]['daily']) 
						&& $showNearestSpaces['SpacePricing'][0]['daily'] > 0) 
					{
						echo '&#8377 '.$showNearestSpaces['SpacePricing'][0]['daily'].' /day';
					}elseif(!empty($showNearestSpaces['SpacePricing'][0]['weekely']) 
						&& $showNearestSpaces['SpacePricing'][0]['weekely'] > 0) 
					{
						echo '&#8377 '.$showNearestSpaces['SpacePricing'][0]['weekely'].' /week';
					}elseif(!empty($showNearestSpaces['SpacePricing'][0]['monthly']) 
						&& $showNearestSpaces['SpacePricing'][0]['monthly'] > 0) 
					{
						echo '&#8377 '.$showNearestSpaces['SpacePricing'][0]['monthly'].' /month';
					}elseif(!empty($showNearestSpaces['SpacePricing'][0]['yearly']) 
						&& $showNearestSpaces['SpacePricing'][0]['yearly'] > 0) 
					{
						echo '&#8377 '.$showNearestSpaces['SpacePricing'][0]['yearly'].' /annum';
					}else{
						echo 'N/A';
					} 
                                      echo "</b>";

				     }		
                                   else{
                                       echo "<div class=\"col-xs-offset-3 -offcol-xs-9\">";
                                       echo "<span><img src=\"/files/FacilityIcon/free.png\" class=\"center-block\"></span>";
                                       echo "</div>";
                                    }
                                   ?>     
                                  </div><!--row1-->
                             </div><!--col-xs-6-->
                            </div><!-- row spaceDetailPriceRow -->
                          <div class="row bookNowRow">
                              <div class="col-xs-6 col-xs-offset-6 text-right">
				<?php
                                   if($showNearestSpaces['Space']['booking_confirmation'] != 2)
				   {
				      if($filteredResults)
				      {		
					//CakeLog::write('debug','in filter results....' . $stdate . ' ' . $edate);    
					$buttonText = "&#8377;".$showNearestSpaces['booking_price'] . ' Book Now';
				        echo $this->Html->link(
					        $buttonText,
						'/spaces/spaceDetail/'.urlencode(base64_encode($showNearestSpaces['Space']['id'])).'?'.
						                       'stdate='.urlencode(base64_encode($stdate)).'&&'.
								       'edate='.urlencode(base64_encode($edate)),
					       array(
					         'escape' => false,
						 'class' => 'btn btn-success'
						 )
					  );
				      }else{
				        echo $this->Html->link(
					       __('Book Now'),
					       '/spaces/spaceDetail/'.urlencode(base64_encode($showNearestSpaces['Space']['id'])),
					       array(
					         'escape' => false,
						 'class' => 'btn btn-success'
						 )
					  );
			             }
                                   }else{
					    //echo $showNearestSpaces['Space']['mapsURL']; 
					$formID = "sms-location-form".$itr;
					$formName = "SMSLocation".$itr;     
					echo $this->Form->create(
						$formName,
                                                array(
                                                  'url' => '/spaces',
                                                  'type' => 'post', 
						  'class' => 'smsForm'
                                                 )
					 );
				        $shortUrlID = "shortURL".$itr;
					echo $this->Form->hidden(
						'short-url',
				                 array(
				                'value' => $showNearestSpaces['Space']['mapsURL'],
						'class' => 'hidden-space-id',
				                      )
					);
				       $spaceID = "space-id".$itr;	 
			               echo $this->Form->hidden(
			                        'space-id',
			                        array(
			                         'value' => $showNearestSpaces['Space']['id'],
			                         'class' => 'hidden-space-id'
				                )
					);
				       $userID = "user_id".$itr;
                                       echo $this->Form->hidden(
				                'user-id',
				                 array(
				                   'value' => isset($userId)?$userId:1,
				                   'class' => 'hidden-space-id'
				                  )
                                        );
				      echo "<div class=\"form-group hide\" id=\"mobile$itr\">";
				      $mobileInputStr = 'mobileInput'.$itr; 
			              echo $this->Form->input('mobile', array(
				                                    'div' => false,
				                                    'label' => false,
						                    'id' => $mobileInputStr,
						                    'type' => 'number',
						                    'class' => 'form-control',
						                    'placeholder' => 'Enter your Number',
						                    'value' => isset($userMobile)?$userMobile:'' 
						               )
						             );
                                         echo "</div>";
                                         echo "<div id=\"smsSendStatus$itr\" class=\"hide\"></div>";   
                                         echo "<div class=\"btn btn-warning smsButton\" id=\"$itr\">";
					 echo "SMS Location";
                                         echo "</div>";
					 echo "<div class=\"nopadding text-danger text-right driving\">Online Booking coming soon</div>";
					 $itr++;
	                                 echo $this->Form->end();
                                      }
			           ?>

                              </div><!--col-xs-6-->

                          </div><!--bookNowRow-->
                       </div><!--location-name clearfix container-fluid -->
                       
                     </div><!--parkingspace-details-->


             <?php }?> <!--foreach-->

	  <?php }else{ ?><!--if somespaces have to be shown -->
		     <div class="parkingspace-details noresults">
			<div class="well location-name clearfix container-fluid">
                               <div class="row">&nbsp;</div>
			       <div class="row">
                                   <div class="col-xs-12 text-center">
				     <span class="text-primary">No Space available during the choosen time</span>
                                   </div>
			       </div>
                               <div class="row">&nbsp;</div>
                              <div class="row">
				  <div class="col-xs-12 text-center">
                                    <?php echo $this->Html->link(
					        "See All Results",
					       '/spaces/listNearBySpaces/'.$encodedSpaceID,
					       array(
					         'escape' => false,
						 'class' => 'btn btn-info'
						 )
					  );
                                    ?>
                                  </div>
                              </div>
                             <div class="row">&nbsp;</div>
			     <div class="row">
                                <div class="col-xs-12 text-center">
				   <span class="text-success">or Search with different dates and time</span>
                                </div>
                             </div>
                        </div> 
                     </div><!--parkingspace-details-noresults-->
	     <?php }?>
           </div><!--parkingspace-left-section-->
          
          </div><!--col-xs-4 spaceOverMaps-->
     

  </section><!--clearfix-->
 </div><!--mapSpaceView container-fluid-->
</div><!--map-space-container-->
<?php echo $this->element('frontend/Space/space_info'); 
	echo '<script>var nearestSpacesForMap = ' . json_encode($listNearBySpaces) . ';</script>';
?>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
