<?php echo $this->Html->script(array('frontend/event/event_locations', 'frontend/Space/list_near_by_space_map'), array('inline' => false)); ?>
<div class="main-container">

	<div class="event-banner">
	<div class="container">
		<div class="col-md-12"><br />
			<h3><font color="white"><?php echo __('Parking near'); ?> <strong><?php echo $getSpaceDetail['Space']['name'];  ?></strong></font></h3>
		</div>
	</div>
	</div>
		<div class="container">
			<section class="clearfix">
				<?php if (!empty($listNearBySpaces)) { 	?>

					<div class="col-md-5">
						
						<div class="well timeframe clearfix">
							<h4>
								<span class="glyphicon glyphicon-calendar"></span>
								<?php echo __('Search Parking Space'); ?>
							</h4>
							<small><?php echo __('Change the price and distance below to search space.'); ?></small>
							<div class="row">
								<?php
									echo $this->Form->create(
												'Space',
												array(
														'url' => 'listNearBySpaces/'.urlencode(base64_encode($getSpaceDetail['Space']['id'])),
														'type' => 'GET',
														'id' => 'book-space',
														
													)
											);
											
										?>
								<div class="col-xs-6">
									<div class="form-group">
										
										<label for="price">Price (&#8377;)</label>
										<?php
					                		echo $this->Form->input('price', array(
					                			'options' => array('hourly'=>'Hourly',
					                								'daily'=>'Daily',
					                								'weekely' => 'Weekly',
					                								'monthly' => 'Monthly',
					                								'yearly' => 'Yearly'
					                								),
					                			
					                			'div' => false,
					                			'label' => false,
					                			'id' => 'price',
					                			'empty' => 'Please Select',
					                			'class' => 'form-control',
					                			'default' => $price
					                			)
					                		);
						                	?>
										
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label for="distance">Distance (Km)</label>
										<?php
					                		echo $this->Form->input('distance', array(
					                			'options' => array(1 => 1,
					                							2 => 2,
					                							3 => 3 ,
					                							4 => 4,
					                							5 => 5),
					                			'div' => false,
					                			'label' => false,
					                			'id' => 'distance',
					                			'empty' => 'Please Select',
					                			'class' => 'form-control',
					                			'default' => $distance
					                			)
					                		);
						                	?>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<div class="checkbox">
											<label>
												<?php 
													if (isset($this->request->query['type']) && !empty($this->request->query['type'])) {
														$checked = 'checked';
													} else {
														$checked = '';
													}
												?>
												<input <?php echo $checked; ?> name="type" type="checkbox"> Immediately available
											</label>
										</div>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<?php
											echo $this->Form->submit(
												__('Search'),
												array(
														'class' => 'btn btn-info btn-block',
														'escape' => false,
														'data-user' => $this->Session->check('Auth.User') ? true : false
													)
											);
										?>
									</div>
								</div>
							</div>
							<?php echo $this->Form->end(); ?>
						</div>
						<div class=" parkingspace-left-section">
						<?php foreach ($listNearBySpaces as $showNearestSpaces) { ?> 
								
								<div class="location-name clearfix" onclick="showSelectedSpace(<?php echo $showNearestSpaces['Space']['id']; ?>);" id="SPACEID-<?php echo $showNearestSpaces['Space']['id']; ?>">
									<div class="col-md-12">
										<h4>
											<span class="map-marker-icon space-title"></span>
											<span class="parking-name-mod"><?php echo $showNearestSpaces['Space']['name']; ?> </span>
                                                                                        <span class="parking-icons">
                                                                                          <?php if($showNearestSpaces['Space']['is_parking_free']){
                                                                                                  echo "<span><img src=\"/files/FacilityIcon/free.png\" width=\"40\" ></span>";
                                                                                                }
                                                                                          ?>
                                                                                     
                                                                                        </span>
                                                                                        <span class="parking-icons">
                                                                                          <?php if($showNearestSpaces['SpaceTimeDay']['open_all_hours']){
                                                                                                  echo "<span><img src=\"/files/FacilityIcon/2441.png\" width=\"42\" ></span>";
                                                                                                }
                                                                                          ?>
                                                                                     
                                                                                        </span>
											<?php if($showNearestSpaces['Space']['property_type_id'] == 2) { ?>
												<small class="pill"> 
													<?php echo '<b>Restricted to only fellow residents </b>'; ?>
												</small>
											<?php } ?>
                                                                                        
										</h4>
                                                                                 <?php if($showNearestSpaces['Space']['booking_confirmation'] == 0){                                                                                                     echo "<small class=\"pill pill-danger\">Owner confirmation required"; 
                                                                                       }elseif($showNearestSpaces['Space']['booking_confirmation'] == 1){ 
                                                                                         echo "<small class=\"pill pill-success\">Immediately Available";
                                                                                       }else{
                                                                                         echo "<small class=\"pill pill-info\">Coming soon to SimplyPark";
                                                                                       }  
										       echo "</small>";
                                                                                  ?>
										
									</div>

									<div class="col-xs-12">
										<dl class="dl-horizontal">
                                                                                <?php if(!$showNearestSpaces['Space']['is_parking_free']){ ?>
											<dt><?php echo __('Price:'); ?></dt>
											
											<dd>
											<?php if (isset($this->request->query) && !empty($this->request->query))                                                                                                                                                              { 
													switch ($price) {
														case 'hourly':
															$symbol = ' /hr';
															break;
														case 'daily':
															$symbol = ' /day';
															break;
														case 'weekely':
															$symbol = ' /week';
															break;
														case 'monthly':
															$symbol = ' /month';
															break;
														case 'yearly':
															$symbol = ' /annum';
															break;

													}
												}


												if(!empty($price)) { 
                                                                                                        if($showNearestSpaces['Space']['tier_pricing_support'])                                                                                                          {
                                                                                                            echo $showNearestSpaces['Space']['tier_formatted_desc'];              
                                                                                                         }else{
													     echo '&#8377 '.$showNearestSpaces['SpacePricing'][$price].$symbol;
                                                                                                         }

												} else { 

											 
													if (!empty($showNearestSpaces['SpacePricing']['hourly']) && $showNearestSpaces['SpacePricing']['hourly'] > 0) {
	        											echo '&#8377 '.$showNearestSpaces['SpacePricing']['hourly'].' /hr';
												    } else if ($showNearestSpaces['Space']['tier_pricing_support']) {
	        											echo $showNearestSpaces['Space']['tier_formatted_desc'];

                                                                                                    } else if (!empty($showNearestSpaces['SpacePricing']['daily']) && $showNearestSpaces['SpacePricing']['daily'] > 0) {
												        echo '&#8377 '.$showNearestSpaces['SpacePricing']['daily'].' /day';
												    } else if (!empty($showNearestSpaces['SpacePricing']['weekely']) && $showNearestSpaces['SpacePricing']['weekely'] > 0) {
												        echo '&#8377 '.$showNearestSpaces['SpacePricing']['weekely'].' /week';
												    } else if (!empty($showNearestSpaces['SpacePricing']['monthly']) && $showNearestSpaces['SpacePricing']['monthly'] > 0) {
												        echo '&#8377 '.$showNearestSpaces['SpacePricing']['monthly'].' /month';
												    } else if (!empty($showNearestSpaces['SpacePricing']['yearly']) && $showNearestSpaces['SpacePricing']['yearly'] > 0) {
												        echo '&#8377 '.$showNearestSpaces['SpacePricing']['yearly'].' /annum';
												    } else {
												        echo 'N/A';
												    } 
											} ?>
											</dd>
                                                                                       <?php } ?>
											<dt><?php echo __('Distance:'); ?></dt>
											<dd><?php echo round($showNearestSpaces[0]['distance']).' km(s)'; ?></dd>
											<dt><?php echo __('Available slots:'); ?></dt>
											<dd><?php echo $showNearestSpaces['Space']['number_slots'] != '' ? $showNearestSpaces['Space']['number_slots']: 'N/A' ; ?></dd>
                                                                                        <?php if(!empty($showNearestSpaces['SpaceFacility'])){ ?>

											        <dt><?php echo __('Facilities:'); ?></dt>
											        <dd>
                                                                                                   <?php 
                                                                                                    foreach($showNearestSpaces['SpaceFacility'] as $facility){
                                                                                                        $iconName = "/files/FacilityIcon/icon.".$facility['Facility']['icon'];
                                                                                                        $title =  $facility['Facility']['name'];
                                                                                                       echo "<img src=$iconName title=$title alt=$title>"; 
                                                                                                        echo "&nbsp&nbsp;";
                                                                                                     }


                                                                                                   ?>

                                                                                               </dd>
                                                                                        <?php } ?>
										</dl>
									</div>
									
									<div class="col-xs-6">
										<?php
											echo $this->Html->link(
													__('View Details'),
													'#',
													array(
															'class' => 'view-details space-details',
															'data-id' => urlencode(base64_encode($showNearestSpaces['Space']['id']))
														)
												);
										?>
									</div>
									<div class="col-xs-6 text-right">
										<?php
                                                                                        if($showNearestSpaces['Space']['booking_confirmation'] != 2){
											   echo $this->Html->link(
														__('BOOK NOW'),
														'/spaces/spaceDetail/'.urlencode(base64_encode($showNearestSpaces['Space']['id'])),
														array(
																'escape' => false,
																'class' => 'btn btn-info'
															)
													);
                                                                                        }else{
                                                                                           //echo $showNearestSpaces['Space']['mapsURL']; 
                                                                                           
                                                                                           echo $this->Form->create(
                                                                                                                'SMSLocation',
                                                                                                                 array(
                                                                                                                       'url' => '/spaces',
                                                                                                                       'type' => 'post', 
                                                                                                                       'id' => 'sms-location-form'
                                                                                                                      )
                                                                                                                   );
							                                    echo $this->Form->hidden(
									                                    'short_URL',
									                                    array(
											                                    'value' => $showNearestSpaces['Space']['mapsURL'],
											                                    'class' => 'hidden-space-id'
										                                    )
                                                                                                               );
							                                    echo $this->Form->hidden(
									                                    'space_id',
									                                    array(
											                                    'value' => $showNearestSpaces['Space']['id'],
											                                    'class' => 'hidden-space-id'
										                                    )
                                                                                                               );
							                                    
                                                                                               echo $this->Form->hidden(
									                                    'user_id',
									                                    array(
											                                    'value' => isset($userId)?$userId:1,
											                                    'class' => 'hidden-space-id'
										                                    )
                                                                                                               );
									                                                echo "<div class=\"form-group hide\" id=\"mobile\">";
						                		                                           echo $this->Form->input('mobile', array(
						                			                                           'div' => false,
						                			                                           'label' => false,
						                			                                           'id' => 'mobileInput',
						                			                                           'type' => 'number',
						                			                                           'class' => 'form-control',
						                			                                           'placeholder' => 'Enter your Number',
						                			                                           'value' => isset($userMobile)?$userMobile:'' 
						                			                                           )
						                		                                           );
                                                                                                                        echo "</div>";
                                                                                           echo "<div id=\"smsSendStatus\" class=\"hide\"></div>";   
                                                                                           echo "<div class=\"btn btn-info\" id=\"smsLocation\">";
                                                                                              echo "SMS Location";
                                                                                           echo "</div>";
                                       
    
                                                                                        }
										?>
									</div>
								</div>
						<?php } ?>
						</div>
					</div>
					<div class="col-md-7">
						<div id="map_wrapper" class="map-height">
						    <div id="map_canvas" class="mapping"></div>
						</div>
					</div>
				<?php } else { ?>
					<div class="col-md-12 text-center gray-bg">
						<strong><?php echo __('No Record Found.'); ?></strong>
					</div>
				<?php } ?>
			</section>
		</div>
</div>
<?php echo $this->element('frontend/Space/space_info'); 
	echo '<script>var nearestSpacesForMap = ' . json_encode($listNearBySpaces) . ';</script>';
?>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
