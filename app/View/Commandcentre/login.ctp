  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
          <?php  echo $this->Form->create('User', array('method' => 'post',
                                                        'class' => 'form-signin',
                                                        'id' => 'login-validate',
                                                        'novalidate' => true
                                                      ));
          ?>
               <div class="form-group">
                  <?php
                      if ($this->Session->check('Message.flash')) {
                        echo $this->element('flash_msg');
                      }
                  ?>
              </div>
              <h1>Login Form</h1>
              <div class="form-group">
                 <label><?php echo __('Username') ?></label>
                        <?php echo $this->Form->input('username', array(
                                                     'placeholder' => 'username or email',
                                                     'class' => 'form-control',
                                                     'div' => false,
                                                     'label' => false
                                                    )
                                                   );
                      ?>
              </div>
              <div class="form-group">
                  <label><?php echo __('Password') ?></label>
                         <?php echo $this->Form->input('password', array(
                                                       'placeholder' => 'password',
                                                       'class' => 'form-control',
                                                       'label' => false,
                                                       'div' => false
                                                    )
                                                 );
                                        ?>
              </div>
              <div class="clearfix">
                <div class="pull-left">
                   <div class="checkbox remember-me">
                      <label>
                      <?php
                        echo $this->Form->checkbox('keep_me_logged_in', array(
                             'label' => false,
                             'div' => false
                            )
                          );
                         echo __('Keep me logged in');
                      ?>                        
                     </label>
                  </div>
               </div>
              <div class="pull-right">
                <button class="btn btn-primary" type="submit"><?php echo __('Sign in') ?></button>
              </div>
              <?php echo $this->Form->end(); ?>

              <div class="clearfix"></div>

              <div class="separator">
                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="map-marker-icon-mod"></i> SimplyPark.in</h1>
                  <p>Parking. Simplified. <br />©2016 All Rights Reserved.</p>
                </div>
              </div>
            </form>
          </section>
        </div>

        <div id="register" class="animate form registration_form">
          <section class="login_content">
            <form>
              <h1>Create Account</h1>
              <div>
                <input type="text" class="form-control" placeholder="Username" required="" />
              </div>
              <div>
                <input type="email" class="form-control" placeholder="Email" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <a class="btn btn-default submit" href="index.html">Submit</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Already a member ?
                  <a href="#signin" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> Gentelella Alela!</h1>
                  <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
</body>
</html>
