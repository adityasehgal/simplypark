<!DOCTYPE html>
	<!-- BEGIN HEAD -->
	<head>
	    <meta charset="UTF-8" />
	    <title></title>
	    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />

		<!-- ==============================================
		Favicons
		=============================================== -->

		<link rel="shortcut icon" href="<?php echo configure::read('ROOTURL'); ?>img/favicons/favicon-16x16.ico">
		<link rel="apple-touch-icon" href="<?php echo configure::read('ROOTURL'); ?>img/favicons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo configure::read('ROOTURL'); ?>img/favicons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href=".<?php echo configure::read('ROOTURL'); ?>img/favicons/apple-icon-114x114.png">
	    
	    <!-- GLOBAL STYLES -->
	    <?php
	    	echo $this->Html->css(
	    			array(
	    				'backend/bootstrap/css/bootstrap',
	    				'backend/login/login',
	    				'backend/login/magic',
	    				'backend/uniform/uniform.default'
	    			),
                    null,array('inline' => false)
	    		);
	    	echo $this->fetch('meta');
          	echo $this->fetch('css');
	    ?>
	</head>
	<body class="padTop53">
		<div class="login-page">
    		<div class="container">
    			<?php echo $this->fetch('content'); ?>
    		</div>
    	</div>
	    <?php echo $this->Html->script(
	    			array(
	    				'backend/jquery1.11.0-min',
	    				'backend/bootstrap/bootstrap.min',
	    				'common',
	    				'validationengine/jquery.validate',
	    				'validationengine/additional-methods',
	    				'backend/login/login'
	    			)
	    	); 
	    	echo $this->fetch('script');
	    ?>
	</body>
</html>