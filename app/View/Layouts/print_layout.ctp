<!DOCTYPE html>
<html lang="en-US">
	<!-- ==============================================
		Title and Meta Tags
		=============================================== -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>SimplyPark</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<?php
			if (isset($getCms['CmsPage']['meta_keyword'])) { 
				echo $this->Html->meta(
				    'keywords',
				    $getCms['CmsPage']['meta_keyword']
				);
			}
			if (isset($getCms['CmsPage']['meta_description'])) {
				echo $this->Html->meta(
				    'description',
				    $getCms['CmsPage']['meta_description']
				);
			}
		?>
		<meta name="author" content="">
		<!-- <meta http-equiv="refresh" content="30"> -->

		<!-- ==============================================
		Favicons
		=============================================== -->

		<link rel="shortcut icon" href="<?php echo configure::read('ROOTURL'); ?>img/favicons/favicon-16x16.ico">
		<link rel="apple-touch-icon" href="<?php echo configure::read('ROOTURL'); ?>img/favicons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo configure::read('ROOTURL'); ?>img/favicons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href=".<?php echo configure::read('ROOTURL'); ?>img/favicons/apple-icon-114x114.png">

		<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
		
		<!-- ==============================================
		CSS
		=============================================== -->
		<?php
			echo $this->Html->css(
					array(
							'frontend/bootstrap/bootstrap.min',
							'frontend/bootstrap/font-awesome',
							'frontend/style',
							'print-layout'
						)
				);
			echo $this->fetch('meta');
          	echo $this->fetch('css');

          	echo $this->Html->script(
          			'frontend/jquery.min'
          		);
		?>
		
		<!--[if lt IE 9]>
			<script src="js/html5shiv.min.js"></script>
			<script src="js/respond.min.js"></script>
		<![endif]-->		
		
	</head>
	<body>
		<?php 
			if ($this->Session->check('Message.flash') && !isset($this->params['url']['errorlogin']) && !isset($this->params['url']['errorsignup']) && !isset($this->params['url']['forgotpassword']) && !isset($this->params['url']['resetpassword'])) {
	        	echo $this->element('flash_msg');
	        }
			echo $this->fetch('content');
		?>
		
		<!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php
			echo $this->Html->script(
					array(
							'jquery-ui.min',
							'frontend/bootstrap/bootstrap.min'
						)
				);
			echo $this->fetch('script');
		?>
	</body>
</html>
