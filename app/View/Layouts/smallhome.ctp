<!DOCTYPE html>
<html lang="en-US">
	<!-- ==============================================
		Title and Meta Tags
		=============================================== -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>SimplyPark - Search Book List parking space in Delhi,delhi/new delhi/old delhi/ south delhi east delhi west delhi north delhi central delhi Gurgaon,DLF sushant Lok,all sectors in Noida Ghaziabad Faridabad Indirapuram Vaishali Vasundhra Bangalore Chennai Hyderabad Park Place Regency PSN Prestige Shantiniketan Whitefield Karnataka</title>
                <meta name="keywords" content="Search,Book,Park,SimplyPark.in,parking,DLF,PSN,Prestige,Shantiniketan,Bangalore,delhi,new delhi,old delhi,south delhi,east delhi, west delhi, north delhi, centra delhi, Gurgaon, Whitefield, Bangalore Urban, Karnataka,Pune, Maharashtra, Apsara Cinema, Events,online parking spot, online parking system, parking in delhi, parking in PSN,car parking, car parking DLF" />
 
                <meta name="description" content="SimplyPark lets you search, book, and pay for a guaranteed Parking Spot. Pre-book, pre-pay and get a guranteed parking spot. Parking available in DLF, Regency, Crossing Republic, PSN, Prestige Shantiniketan, Whitefield, Apsara Cinema, Pune, Maharashtra" /> 

                <meta itemprop="name" content="Welcome to SimplyPark.in  : Let's Simplyify Parking" />
                <meta itemprop="description" content="SimplyPark lets you search, book, and pay for a guaranteed Parking Spot. Pre-book, pre-pay and get a guranteed parking spot. Parking available in DLF, Regency, Crossing Republic, PSN, Prestige Shantiniketan, Whitefield, Apsara Cinema, Pune, Maharashtra" />

                <meta itemprop="image" content="https://www.simplypark.in/img/frontend/logo-simply-park.png?1442395156" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<?php
			if (isset($getCms['CmsPage']['meta_keyword'])) { 
				echo $this->Html->meta(
				    'keywords',
				    $getCms['CmsPage']['meta_keyword']
				);
			}
			if (isset($getCms['CmsPage']['meta_description'])) {
				echo $this->Html->meta(
				    'description',
				    $getCms['CmsPage']['meta_description']
				);
			}
		?>
		<meta name="author" content="">
		<!-- <meta http-equiv="refresh" content="30"> -->

		<!-- ==============================================
		Favicons
		=============================================== -->

		<link rel="shortcut icon" href="<?php echo configure::read('ROOTURL'); ?>img/favicons/favicon-16x16.ico">
		<link rel="apple-touch-icon" href="<?php echo configure::read('ROOTURL'); ?>img/favicons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo configure::read('ROOTURL'); ?>img/favicons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href=".<?php echo configure::read('ROOTURL'); ?>img/favicons/apple-icon-114x114.png">

		<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
		
		<!-- ==============================================
		CSS
		=============================================== -->
		<?php
			echo $this->Html->css(
					array(
							'frontend/bootstrap/bootstrap.min',
							'frontend/bootstrap/font-awesome',
							'frontend/style',
							'change_style',
							'frontend/alert-msg',
							/*'backend/bootstrap/css/bootstrap-datepicker3',
							'backend/bootstrap/css/bootstrap-datetimepicker.min',*/
							'frontend/jquery.datetimepicker',
							'frontend/sticky-footer'
						)
				);
			echo $this->fetch('meta');
          	echo $this->fetch('css');

          	echo $this->Html->script(
          			'frontend/jquery.min'
          		);
		?>
		
		<!--[if lt IE 9]>
			<script src="js/html5shiv.min.js"></script>
			<script src="js/respond.min.js"></script>
		<![endif]-->		
		
	</head>
	<body>
	<div id="overlay"></div>
		<!-- ==============================================
		Coupon code
		=============================================== -->
		<?php echo $this->element('frontend/Home/discount_coupon'); ?>

		<!-- ==============================================
		MAIN NAV
		=============================================== -->
		<?php 
			echo $this->element('frontend/Home/headermod');
			if ($this->Session->check('Message.flash') && !isset($this->params['url']['errorlogin']) && !isset($this->params['url']['errorsignup']) && !isset($this->params['url']['forgotpassword']) && !isset($this->params['url']['resetpassword'])) {
	        	echo $this->element('flash_msg');
	        }
			echo $this->fetch('content');
		?>

		<!-- ==============================================
		Login Modal
		=============================================== -->	
		<!-- Modal -->
		<?php echo $this->element('frontend/Home/login'); ?>

		<!-- ==============================================
		Signup Modal
		=============================================== -->	
		<!-- Modal -->
		<?php echo $this->element('frontend/Home/signup'); ?>

		<!-- ==============================================
		Forgot Password Modal
		=============================================== -->	
		<!-- Modal -->
		<?php echo $this->element('frontend/Home/forgot_password'); ?>

		<!-- ==============================================
		Search tag not matched Modal
		=============================================== -->	
		<!-- Modal -->
		<?php echo $this->element('frontend/Home/search_tags'); ?>

		<!-- ==============================================
		Reset Password Modal
		=============================================== -->	
		<!-- Modal -->
		<?php echo $this->element('frontend/Home/reset_password'); ?>
		
		<?php echo $this->element('frontend/Home/search_gated_space'); ?>
		<!-- ==============================================
		FOOTER
		=============================================== -->
		<?php echo $this->element('frontend/footer'); ?>
		
		<!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php
			echo $this->Html->script(
					array(
							'jquery-ui.min',
							'frontend/bootstrap/bootstrap.min',
							'underscore-min',
							'validationengine/jquery.validate',
	    					'validationengine/additional-methods',
	    					'pStrength.jquery',
	    					'frontend/custom-script',
	    					'frontend/common',
	    					'frontend/Search/search',
	    					'backend/bootstrap/bootstrap-datepicker',
							'backend/bootstrap/bootstrap-datetimepicker.min',
							'frontend/jquery.datetimepicker',
						)
				);
			echo '<script>
					var domainName = ' .json_encode($_SERVER['SERVER_NAME']).';
				</script>';
			echo $this->fetch('script');
		?>
		<script type="text/javascript">
			$(document).ready(function () { 
				var domain_string = "; domain=" + domainName;
				//console.log(window.history);
				if (window.history && window.history.pushState) {    
					$(window).on('popstate', function() {
						var hashLocation = location.hash;
						var hashSplit = hashLocation.split("#");

						var hashName = hashSplit[1];      
						if (hashName !== '') {
							var hash = window.location.hash;
							if (hash === '') {

								document.cookie = 'start-date' + "=; max-age=0; path=/" + domain_string;
								document.cookie = 'end-date' + "=; max-age=0; path=/" + domain_string;
								document.cookie = 'space-id' + "=; max-age=0; path=/" + domain_string;
								document.cookie = 'space-park-id' + "=; max-age=0; path=/" + domain_string;	 
								document.cookie = 'current-action' + "=; max-age=0; path=/" + domain_string;	 	
							}
				     	}
			   		});    
					window.history.pushState('forward', null, '');
				}
			}); 
		</script>
	</body>
</html>
