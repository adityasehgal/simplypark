<!DOCTYPE html>
	<!-- BEGIN HEAD -->
	<head>
	    <meta charset="UTF-8" />
	    <title></title>
	    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />

		<!-- ==============================================
		Favicons
		=============================================== -->
		<link rel="shortcut icon" href="<?php echo configure::read('ROOTURL'); ?>img/favicons/favicon-16x16.ico">
		<link rel="apple-touch-icon" href="<?php echo configure::read('ROOTURL'); ?>img/favicons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo configure::read('ROOTURL'); ?>img/favicons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href=".<?php echo configure::read('ROOTURL'); ?>img/favicons/apple-icon-114x114.png">
	    
	    <!-- GLOBAL STYLES -->
	    <?php
	    	echo $this->Html->css(
	    			array(
	    				'backend/bootstrap/css/bootstrap',
	    				'backend/bootstrap/css/font-awesome',
	    				'backend/magnific-popup',
	    				'backend/common',
	    				'backend/main',
	    				'change_style'
	    			),
                    null,array('inline' => false)
	    		);
	    	echo $this->fetch('meta');
          	echo $this->fetch('css');

          	echo $this->Html->Script('backend/jquery1.11.0-min');
	    ?>
	</head>
	<body class="padTop53 " >
		<div id="wrap" >
	        <?php echo $this->element('backend/header');
	        	echo $this->element('backend/navigation');
	        ?>
	        <!--PAGE CONTENT -->
	        <div id="content" class="clearfix">
	        	<div class="inner" style="min-height: 600px;">
	        		<?php 
	        			if ($this->Session->check('Message.flash')) {
				        	echo $this->element('flash_msg');
				        }
	        			echo $this->fetch('content');
	        		?>
	        	</div>
	        </div>
	        <!--END PAGE CONTENT -->
	    </div>
	    <div id="footer">
	        <p><?php echo configure::read('SITE_SETTINGS.Copyright'); ?></p>
	    </div>
	    <div class="loader">
	    	<?php
                echo $this->Html->image(
                    'loader.gif',
                    array('id' => 'busy-indicator')
                );
            ?>
	    </div>
	    <?php echo $this->Html->script(
	    			array(
	    				'jquery-ui.min',
	    				'underscore-min',
	    				'backend/bootstrap/bootstrap.min',
	    				'pStrength.jquery',
	    				'backend/jquery.magnific-popup',
	    				'backend/common',
	    				'validationengine/jquery.validate',
	    				'validationengine/additional-methods',
	    				'change_password',
	    				'backend/Admin/edit_admin_profile'
	    			)
	    	); 
	    	echo $this->fetch('script');
	    ?>
	</body>
</html>