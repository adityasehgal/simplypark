<!DOCTYPE html>
<html lang="en-US">
	<!-- ==============================================
		Title and Meta Tags
		=============================================== -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>SimplyPark</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="author" content="">
		<!-- <meta http-equiv="refresh" content="30"> -->

		<!-- ==============================================
		Favicons
		=============================================== -->

		<link rel="shortcut icon" href="<?php echo configure::read('ROOTURL'); ?>img/favicons/favicon-16x16.ico">
		<link rel="apple-touch-icon" href="<?php echo configure::read('ROOTURL'); ?>img/favicons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo configure::read('ROOTURL'); ?>img/favicons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href=".<?php echo configure::read('ROOTURL'); ?>img/favicons/apple-icon-114x114.png">

		<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
		
		<!-- ==============================================
		CSS
		=============================================== -->
		<?php
			echo $this->Html->css(
					array(
							'frontend/bootstrap/bootstrap.min',
							'frontend/bootstrap/font-awesome',
							'frontend/style',
							'change_style',
							'frontend/bootstrap/bootstrap-switch.min',
							/*'backend/bootstrap/css/bootstrap-datepicker3',
							'backend/bootstrap/css/bootstrap-datetimepicker.min',*/
							'frontend/jquery.datetimepicker',
							'frontend/sticky-footer'
						)
				);
			echo $this->fetch('meta');
          	echo $this->fetch('css');
		?>
		
		<!--[if lt IE 9]>
			<script src="js/html5shiv.min.js"></script>
			<script src="js/respond.min.js"></script>
		<![endif]-->		
		
	</head>
	<body>
		<!-- ==============================================
		Flash Messages
		=============================================== -->
		<?php
			echo $this->element('frontend/deletePopup');
			if ($this->Session->check('Message.flash')) {
	        	echo $this->element('flash_msg');
	        }
		?>
		<!-- ==============================================
		MAIN NAV
		=============================================== -->
		<?php
			echo $this->element('frontend/User/header');
		?>

		<div class="container main-content">
			<div class="row">
				<!-- ==============================================
				left panel
				=============================================== -->
				<?php
					echo $this->element('frontend/User/left_nav');
				?>

				<!-- ==============================================
				MAIN NAV
				=============================================== -->
				<?php
					echo $this->fetch('content');
				?>
			</div>
		</div>

		<?php echo $this->element('frontend/Home/search_tags'); ?>
		<?php echo $this->element('frontend/Home/search_gated_space'); ?>
		
		<!-- ==============================================
		FOOTER
		=============================================== -->
		<?php echo $this->element('frontend/footer'); ?>
		
		<!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php
			echo $this->Html->script(
					array(
							'frontend/jquery.min',
							'jquery-ui.min',
							'frontend/pop-up',
							'underscore-min',
							'frontend/bootstrap/bootstrap.min',
							'validationengine/jquery.validate',
	    					'validationengine/additional-methods',
	    					'pStrength.jquery',
	    					'frontend/custom-script',
	    					'frontend/common',
	    					'bootstrap-switch.min',
							'moment.min',
							/*'backend/bootstrap/bootstrap-datepicker',
							'backend/bootstrap/bootstrap-datetimepicker.min',*/
							'frontend/jquery.datetimepicker',
	    					'frontend/Search/search'
						)
				);
			echo $this->fetch('script');
		?>

		
		<script type="text/javascript">
		    $(document).ready(function() {
		        $("#homeAddress").click(function() {
		            if($('#homeAddress:checked').length) {
		                $("#showHomeAddress").show();
		            } else {
		                $("#showHomeAddress").hide();
		            }
		        })
		    });
		</script>
	</body>
</html>
