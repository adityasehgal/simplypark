<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>404 | Error page: Simply Park</title>

    <!-- <meta http-equiv="refresh" content="30"> -->

        <!-- ==============================================
        Favicons
        =============================================== -->
        <link rel="shortcut icon" href="<?php echo configure::read('ROOTURL'); ?>img/favicons/favicon-16x16.ico">
        <link rel="apple-touch-icon" href="<?php echo configure::read('ROOTURL'); ?>img/favicons/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo configure::read('ROOTURL'); ?>img/favicons/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href=".<?php echo configure::read('ROOTURL'); ?>img/favicons/apple-icon-114x114.png">
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
        
        <!-- ==============================================
        CSS
        =============================================== -->
        <?php
            echo $this->Html->css(
                    array(
                            'frontend/bootstrap/bootstrap.min',
                            'frontend/bootstrap/font-awesome',
                            'frontend/style',
                            'change_style',
                            'frontend/alert-msg',
                            /*'backend/bootstrap/css/bootstrap-datepicker3',
                            'backend/bootstrap/css/bootstrap-datetimepicker.min',*/
                            'frontend/jquery.datetimepicker',
                            'frontend/sticky-footer'
                        )
                );
            echo $this->fetch('meta');
            echo $this->fetch('css');

            echo $this->Html->script(
                    'frontend/jquery.min'
                );
        ?>
        
        <!--[if lt IE 9]>
            <script src="js/html5shiv.min.js"></script>
            <script src="js/respond.min.js"></script>
        <![endif]-->    

    <!-- Bootstrap -->
   
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
  </head>
  <body >
        <?php //echo $this->element('frontend/Home/header'); ?>
        <div>
            <?php echo $content_for_layout; ?>
        </div>
        <!-- ==============================================
        FOOTER
        =============================================== -->
        <?php //echo $this->element('frontend/footer'); ?>
        
        <!-- ==============================================
        SCRIPTS
        =============================================== --> 
        <?php
            echo $this->Html->script(
                    array(
                            'jquery-ui.min', 
                            'frontend/bootstrap/bootstrap.min',
                            'underscore-min',
                            'validationengine/jquery.validate',
                            'validationengine/additional-methods',
                            'pStrength.jquery',
                            'frontend/custom-script',
                            'frontend/common',
                            'frontend/Search/search',
                            /*'backend/bootstrap/bootstrap-datepicker',
                            'backend/bootstrap/bootstrap-datetimepicker.min'*/
                            'frontend/jquery.datetimepicker',
                        )
                );
            echo $this->fetch('script');
        ?>
  </body>
</html>
