<div id="deleteConfirm" class="white-popup mfp-with-anim mfp-hide">
    <h3 class="text-center popup-heading">Are you sure you paid this amount to owner ?</h3>
        <?php echo $this->Form->create('Bookings',array('type'=>'post','action' => 'changeBookingPaidStatus','admin'=> true)); ?>
            <input type="hidden" class="booking-id" name='data[Booking][id]' >
            <input type="hidden" value="1" name='data[Booking][paid_to_owner]' >
            <div class="form-group">
                <label for="exampleInputEmail1">Reference Code</label>
                <input type="text" class="form-control" name='data[Booking][bank_reference_code]' >
            </div>
            <div class="text-right">
                <button class="btn btn-default close-btn" data-dismiss="modal" type="button">No</button>
                <button class="btn btn-primary" type="submit">Save</button>
            </div>
          
        <?php echo $this->Form->end(); ?>
    </div> 
</div>
