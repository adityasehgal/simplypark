<div id="left" >
	<ul id="menu" class="collapse">
		
        <li class="panel active">
            <?php
                echo $this->Html->link(
                    '<i class="icon-home"></i> '.__('Dashboard'),
                    '/admin/admins/dashboard',
                    array('escape' => FALSE)
                );
            ?>
        </li>
        <?php if ($this->Session->read('Auth.User.user_type_id') == configure::read('UserTypes.Admin')) { ?>
            <li class="panel active">
                <?php
                    echo $this->Html->link(
                            '<i class="icon-user"></i> '.__('Manage Administrators'),
                            '/admin/admins/',
                            array(
                                    'escape' => false
                                )
                        );
                ?>
            </li>
        <?php } ?>
        <li class="panel active">
            <?php
                echo $this->Html->link(
                        '<i class="icon-user"></i> '.__('Manage Users'),
                        '/admin/admins/userListing',
                        array(
                                'escape' => false
                            )
                    );
            ?>
        </li>
		<li class="panel active">
            <?php
                echo $this->Html->link(
                        '<i class="icon-envelope"></i> '.__('Manage Email Templates'),
                        '/admin/email_templates/',
                        array(
                                'escape' => false
                            )
                    );
            ?>
        </li>
        <li class="panel">
            <?php
                echo $this->Html->link(
                        '<i class="icon-wrench"></i> '.__('Manage Settings').'
                        <span class="pull-right">
                          <i class="icon-angle-right"></i>
                        </span>
                        ',
                        '#',
                        array(
                            'data-parent' => '#menu',
                            'data-toggle' => 'collapse',
                            'class' => 'accordion-toggle',
                            'data-target' => '#component-nav',
                            'escape' => false
                            )
                    );
                $classCollapseIn = $this->Admin->manageSettingsTabCollapsing();
            ?>
            <ul class="<?php echo $classCollapseIn; ?>" id="component-nav">
                <li>
                    <?php
                        $tabIconClass = $this->Admin->manageSettingsInnerTabCollapsing('admin_index');
                        echo $this->Html->link('<i class="icon-angle-right '.$tabIconClass.'"></i> '.__('List Space Types'),
                            '/admin/AdminSettings/',
                            array(
                                'escape' => false
                                )
                        );
                    ?>
                </li>
                <li>
                    <?php
                        $tabIconClass = $this->Admin->manageSettingsInnerTabCollapsing('admin_listProperty');
                        echo $this->Html->link('<i class="icon-angle-right '.$tabIconClass.'"></i> '.__('List Property Types'),
                            '/admin/AdminSettings/listProperty',
                        array(
                                'escape' => false
                                )
                        );
                    ?>
                </li>
                <li>
                    <?php
                        $tabIconClass = $this->Admin->manageSettingsInnerTabCollapsing('admin_listFacilities');
                        echo $this->Html->link('<i class="icon-angle-right '.$tabIconClass.'"></i> '.__('List Facilities Types'),
                            '/admin/AdminSettings/listFacilities',
                            array(
                                'escape' => false
                                )
                        );
                    ?>
                </li>
                <li>
                    <?php
                        $tabIconClass = $this->Admin->manageSettingsInnerTabCollapsing('admin_listCmsPages');
                        echo $this->Html->link('<i class="icon-angle-right '.$tabIconClass.'"></i> '.__('List CMS pages'),
                            '/admin/AdminSettings/listCmsPages',
                            array(
                                'escape' => false
                                )
                        );
                    ?>
                </li>
                <li>
                    <?php
                        $tabIconClass = $this->Admin->manageSettingsInnerTabCollapsing('admin_listDiscountCoupons');
                        echo $this->Html->link('<i class="icon-angle-right '.$tabIconClass.'"></i> '.__('Discount Coupons'),
                            '/admin/AdminSettings/listDiscountCoupons',
                            array(
                                'escape' => false
                                )
                        );
                    ?>
                </li>
                <li>
                    <?php
                        $tabIconClass = $this->Admin->manageSettingsInnerTabCollapsing('admin_listPartners');
                        echo $this->Html->link('<i class="icon-angle-right '.$tabIconClass.'"></i> '.__('Tied Up Partners'),
                            '/admin/AdminSettings/listPartners',
                            array(
                                'escape' => false
                                )
                        );
                    ?>
                </li>
                <li>
                    <?php
                        $tabIconClass = $this->Admin->manageSettingsInnerTabCollapsing('admin_serviceTaxCharge');
                        echo $this->Html->link('<i class="icon-angle-right '.$tabIconClass.'"></i> '.__('Service Tax/Commission'),
                            '/admin/AdminSettings/serviceTaxCharge',
                            array(
                                'escape' => false
                                )
                        );
                    ?>
                </li>
            </ul>
        </li>
        <li class="panel active">
            <?php
                echo $this->Html->link(
                        '<i class="icon-building"></i> '.__('Manage Events'),

                        '/admin/events/',
                        array(
                                'escape' => false
                            )
                    );
            ?>
        </li>
        <li class="panel active">
            <?php

                echo $this->Html->link(
                        '<i class="icon-building"></i> '.__('Manage Bookings'). '<span class="pull-right">
                          <i class="icon-angle-right"></i>
                        </span>',
                        '#',
                        array(
                            'data-parent' => '#menu2',
                            'data-toggle' => 'collapse',
                            'class' => 'accordion-toggle',
                            'data-target' => '#component-nav1',
                            'escape' => false
                            )
                    );
                $classCollapseIn = $this->Admin->manageSettingsTabCollapsing();
            ?>
                <ul class="<?php echo $classCollapseIn; ?>" id="component-nav1">
                <li>
                    <?php
                        $tabIconClass = $this->Admin->manageSettingsInnerTabCollapsing('admin_search_bookings');
                        echo $this->Html->link('<i class="icon-angle-right '.$tabIconClass.'"></i> '.__('Search Bookings'),
                            '/admin/Bookings/search_bookings',
                            array(
                                'escape' => false
                                )
                        );
                    ?>
                </li>
                <li>
                    <?php
                        $tabIconClass = $this->Admin->manageSettingsInnerTabCollapsing('admin_required_approval_bookings');
                        echo $this->Html->link('<i class="icon-angle-right '.$tabIconClass.'"></i> '.__('Required Approval Bookings'),
                            '/admin/Bookings/required_approval_bookings',
                            array(
                                'escape' => false
                                )
                        );
                    ?>
                </li>
                <li>
                    <?php
                        $tabIconClass = $this->Admin->manageSettingsInnerTabCollapsing('admin_disapproved_bookings');
                        echo $this->Html->link('<i class="icon-angle-right '.$tabIconClass.'"></i> '.__('Disapproved Bookings'),
                            '/admin/Bookings/disapproved_bookings',
                            array(
                                'escape' => false
                                )
                        );
                    ?>
                </li>
                <li>
                    <?php
                        $tabIconClass = $this->Admin->manageSettingsInnerTabCollapsing('admin_approved_bookings');
                        echo $this->Html->link('<i class="icon-angle-right '.$tabIconClass.'"></i> '.__('Approved Bookings'),
                            '/admin/Bookings/approved_bookings',
                            array(
                                'escape' => false
                                )
                        );
                    ?>
                </li>
                <li>
                    <?php
                        $tabIconClass = $this->Admin->manageSettingsInnerTabCollapsing('admin_cancelled_bookings');
                        echo $this->Html->link('<i class="icon-angle-right '.$tabIconClass.'"></i> '.__('Cancelled Bookings'),
                            '/admin/Bookings/cancelled_bookings',
                            array(
                                'escape' => false
                                )
                        );
                    ?>
                </li>
            </ul>
               
        </li>
        <li class="panel active">
            <?php
                echo $this->Html->link(
                        '<i class="icon-building"></i> '.__('Manage Spaces').'
                        <span class="pull-right">
                          <i class="icon-angle-right"></i>
                        </span>',
                        '#',
                        array(
                            'data-parent' => '#menu3',
                            'data-toggle' => 'collapse',
                            'class' => 'accordion-toggle',
                            'data-target' => '#component-nav2',
                            'escape' => false
                            )
                    );
                $classCollapseIn = $this->Admin->manageSettingsTabCollapsing();
            ?>
            <ul class="<?php echo $classCollapseIn; ?>" id="component-nav2">
                <li>
                    <?php
                        $tabIconClass = $this->Admin->manageSettingsInnerTabCollapsing('admin_simplypark_spaces');
                        echo $this->Html->link('<i class="icon-angle-right '. $tabIconClass .'"></i> '.__('Manage SimplyPark Spaces'),
                                '/admin/spaces/',
                              array(
                                  'escape' => false
                               )
                        );
                    ?>
                </li>
                <li>
                    <?php
                        $tabIconClass = $this->Admin->manageSettingsInnerTabCollapsing('admin_parking_spaces');
                        echo $this->Html->link('<i class="icon-angle-right '. $tabIconClass .'"></i> '.__('Manage Parking Spaces'),
                                '/admin/Spaces/free_parking_spaces',
                              array(
                                  'escape' => false
                               )
                        );
                    ?>
                </li>
            </ul>

        <li class="panel active">
            <?php
                echo $this->Html->link(
                        '<i>&#8377;</i> '.__('Pay To Owner'),
                        '/admin/Bookings/pay_to_owner',
                        array(
                                'escape' => false
                            )
                    );
            ?>
        </li>
        
    </ul>
</div>
