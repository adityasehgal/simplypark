<div class="modal fade" id="addDiscountCoupon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class="button b-close class="close" data-dismiss="modal" aria-label="Close"">
                     <span>X</span>
                </span>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Add Discount Coupon'); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create('DiscountCoupon', array(
                                'url' => '/admin/admin_settings/addupdateDiscountCoupon',
                                'method' => 'post',
                                'id' => 'add-discount-coupon',
                                'novalidate' => true,
                                'onload' => "setDiscountBox()"
                                ));
                ?>
                    <div class="form-group">
                        <label class="control-label"><?php echo __('Coupon Name'); ?><span>*</span></label>
                        <?php
                            echo $this->Form->input('DiscountCoupon.name', array(
                                        'class' => 'form-control',
                                        'label' => false,
                                        'div' => false,
                                        'placeholder' => 'Enter Discount Coupon Name'
                                        ));
                        ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __('Coupon Description'); ?><span>*</span></label>
                        <?php
                            echo $this->Form->textarea('DiscountCoupon.description', array(
                                        'class' => 'form-control',
                                        'label' => false,
                                        'div' => false,
                                        'placeholder' => 'Enter Discount Coupon Description'
                                        ));
                        ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __('Coupon Code'); ?><span>*</span></label>
                        <?php
                            echo $this->Form->input('DiscountCoupon.code', array(
                                        'class' => 'form-control',
                                        'label' => false,
                                        'div' => false,
                                        'placeholder' => 'Enter Discount Coupon Code'
                                        ));
                        ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __('User Type'); ?><span>*</span></label>
                        <?php
                            echo $this->Form->input('for_user_type', array(
                                'div' => false,
                                'label' => false,
                                'options' => Configure::read('coupon_users'),
                                'class' => 'form-control',
                                'empty' => 'Select User Type',
                                )
                            );
                        ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __('Discount Type'); ?><span>*</span></label><br>
                        <?php
                            $first_key = key(Configure::read('discount_type'));
                            echo $this->Form->input('DiscountCoupon.discount_type', array(
                                'separator' => '<br>',
                                'options' => Configure::read('discount_type'),
                                'type' => 'radio',
                                'legend' => false,
                                'class' => 'custom-style',
                                'label' => false,
                                'div' => false,
                                'default' => $first_key
                            ));
                        ?>
                    </div>
                    <div class="form-group flat-discount">
                        <label class="control-label"><?php echo __('Flat Discount Rate'); ?><span>*</span></label>
                        <?php
                            echo $this->Form->input('flat_amount', array(
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false,
                                        'div' => false,
                                        'placeholder' => 'Enter Flat Discount Price',
                                        // 'id' => 'discount-amount'
                                        ));
                        ?>
                    </div>
                    <div class = "discount-per">
                        <div class="form-group">
                            <label class="control-label"><?php echo __('Discount Percentage'); ?><span>*</span></label>
                            <?php
                                echo $this->Form->input('discount_percentage', array(
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false,
                                            'div' => false,
                                            'placeholder' => 'Enter Discount Percentage',
                                            'id' => 'discount-amount'
                                            ));
                            ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label"><?php echo __('Max Amount Discount'); ?></label>
                            <?php
                                echo $this->Form->input('max_amount_discount', array(
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'label' => false,
                                            'div' => false,
                                            'placeholder' => 'Enter Max Amount Discount'
                                            ));
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <?php echo $this->Form->checkbox('is_one_time_use', array('class' => 'custom-style')); ?>
                        <label class="control-label"><?php echo __('Check If coupon is one time use'); ?></label>
                    </div>	
                    <div class="form-group">
                        <?php echo $this->Form->checkbox('is_for_bulk_booking', array('class' => 'custom-style')); ?>
                        <label class="control-label"><?php echo __('Check If coupon is ONLY for bulk bookings'); ?></label>
                    </div>	
                    <div class="form-group">
                        <label class="control-label"><?php echo __('Coupon applies to Property Type'); ?></label>
                        <?php echo $this->Form->input('property_type_id', 
                                                     array(
                                                            'class' => 'form-control',
                                                             'label' => false,
                                                             'id' => 'property_type_id',
                                                            'options' => $property_type_list,
                                                            'default' => 0
                                                          )
                                                     ); ?>
                    </div>	
                    <div class="form-group">
                        <div class="clearfix">                               
                            <div class="pull-right">
                                <?php
                                    echo $this->Form->button(__('Close'), array(
                                                'class' => 'btn btn-default',
                                                'data-dismiss' => 'modal'
                                                ));
                                    echo $this->Form->button(__('Save'), array(
                                                'class' => 'btn btn-primary btn-grad btn-left',
                                                'type' => 'submit'
                                                ));
                                ?>
                            </div>
                        </div> 
                    </div>     
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
