<?php
    echo $this->element('backend/pagination_options');
?>
<table class="table table-bordered table-striped table-bordered table-hover new-addministartor-table">
    <thead>
        <tr>
            <th>
                <?php echo $this->Paginator->sort('DiscountCoupon.name', 'Discount Coupon Name', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('DiscountCoupon.code', 'Code', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('DiscountCoupon.discount_percentage', 'Discount Percentage', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('DiscountCoupon.max_amount_discount', 'Max Amount Discount', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('DiscountCoupon.is_one_time_use', 'Is One Time Use', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('DiscountCoupon.is_for_bulk_booking', 'For Bulk Booking', array('class' => 'tableinherit')); ?>
            </th>
            <th><?php echo __('Show on Frontend'); ?></th>
            <th><?php echo __('Status'); ?></th>
            <th><?php echo __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php  $discount_coupons_path = $discount_coupons['path']; unset($discount_coupons['path']);
            if(!empty($discount_coupons)) { 
                foreach ($discount_coupons as $discount_coupon) {
        ?>
                    <tr>
                        <td>
                            <?php echo $discount_coupon['DiscountCoupon']['name']; ?>
                        </td>
                        <td>
                            <?php echo $discount_coupon['DiscountCoupon']['code']; ?>
                        </td>
                        <td>
                            <?php echo $discount_coupon['DiscountCoupon']['discount_percentage']; ?>
                        </td>
                        <td>
                            <?php echo $discount_coupon['DiscountCoupon']['max_amount_discount']; ?>
                        </td>
                        <td>
                            <?php echo ($discount_coupon['DiscountCoupon']['is_one_time_use'] == 1) ? 'Yes' : 'No'; ?>
                        </td>
                        <td>
                            <?php echo ($discount_coupon['DiscountCoupon']['is_for_bulk_booking'] == 1) ? 'Yes' : 'No'; ?>
                        </td>
                        <td>
                            <?php
                                $checked = ($discount_coupon['DiscountCoupon']['is_frontend'] == 1) ? 'checked' : '';
                            ?>
                            <input class="is-frontend" id="<?php echo 'showfront'.$discount_coupon['DiscountCoupon']['id']; ?>" type="radio" name="is_frontend" value="<?php echo $discount_coupon['DiscountCoupon']['id']; ?>" <?php echo $checked; ?> id >
                            <input type="hidden" class="discount-path" data-discountUrl = "<?php echo $discount_coupons_path?>admin/admin_settings/setDiscountFront">
                        </td>
                        <td>
                        	<?php if ($discount_coupon['DiscountCoupon']['is_active'] == 0) { ?>
                                <a class="btn btn-danger btn-xs btn-grad" href="#"><?php echo __('Inactive'); ?></a>
                            <?php } else { ?>
                            	<a class="btn btn-success btn-xs btn-grad" href="#"><?php echo __('Active'); ?></a>
                            <?php } ?>
                        </td>
                        <td id="inline-popups">
                            <?php
								$activeInactiveAttr = $this->Admin->activeInactiveIconValues($discount_coupon['DiscountCoupon']['is_active']);
								echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-'.$activeInactiveAttr['Class'].'"></i>',
                                        $activeInactiveAttr['PopupID'],
                                        array(
                                            'class' => 'btn btn-ok activeinactivemodalbutton magniPopup',
                                            'title' => $activeInactiveAttr['Title'],
                                            'data-effect' => 'mfp-zoom-in',
                                            'escape' => false,
                                            'data-url' => Configure::read('ROOTURL').'admin/admins/changeRecordStatusByActive/'.base64_encode($discount_coupon['DiscountCoupon']['id']).'/DiscountCoupon'
                                        )
                                    );
                            ?>
                            <?php
                                if ($discount_coupon['DiscountCoupon']['is_active'] == 0) {
                                    echo $this->Html->link(
                                            '<i class="glyphicon glyphicon-pencil"></i>',
                                            '#',
                                            array(
                                                'class' => 'btn btn-green edit_discountcoupon_data',
                                                'data-effect' => 'mfp-zoom-in',
                                                'data-toggle' => 'modal',
                                                'data-target' => '#editDiscountCouponModal',
                                                'title' => 'Edit',
                                                'data-url' => Configure::read('ROOTURL').'admin/admin_settings/ajaxGetDiscountCouponData/'.base64_encode($discount_coupon['DiscountCoupon']['id']).'.json',
                                                'escape' => false
                                            )
                                        );
                                }
                            ?>
                        </td>
                    </tr>
        <?php } } else { ?>
            <tr>
                <td colspan="8">
                    <?php echo __('No Record Found'); ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php
    echo $this->element('backend/pagination');
    echo $this->Js->writeBuffer();
?>
