<div class="modal fade" id="editDiscountCouponModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class="button b-close class="close" data-dismiss="modal" aria-label="Close"">
                     <span>X</span>
                </span>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Edit Discount Coupon'); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create('User', array(
                                'url' => '/admin/admin_settings/addupdateDiscountCoupon',
                                'method' => 'post',
                                'id' => 'edit-discountcoupon'
                                ));
                ?>
                    <div id="discountCouponDataContainer">
                    
                    </div>
                    <div class="form-group">
                        <div class="clearfix">                               
                            <div class="pull-right">
                                <?php
                                    echo $this->Form->button('Close', array(
                                                'class' => 'btn btn-default',
                                                'data-dismiss' => 'modal'
                                                ));
                                    echo $this->Form->button('Save', array(
                                                'class' => 'btn btn-primary btn-grad btn-left',
                                                'type' => 'submit'
                                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/html" id="editDiscountCouponField">
	<input type='hidden' name='data[DiscountCoupon][id]' value="<%= data.DiscountCoupon.id %>" />
	<div class="form-group">
		<label class="control-label">Discount Coupon Name<span>*</span></label>
    	<input type='text' class='form-control' placeholder='Enter Discount Coupon Name' value="<%= data.DiscountCoupon.name %>" name='data[DiscountCoupon][name]' />
    </div>
    <div class="form-group">
        <label class="control-label">Coupon Description<span>*</span></label>
        <textarea class='form-control' placeholder='Enter Coupon Description' name='data[DiscountCoupon][description]'><%= data.DiscountCoupon.description %></textarea>
    </div>
    <div class="form-group">
        <label class="control-label">Discount Coupon Code<span>*</span></label>
        <input type='text' class='form-control' placeholder='Enter Discount Coupon Code' value="<%= data.DiscountCoupon.code %>" name='data[DiscountCoupon][code]' />
    </div>
    <div class="form-group">
        <label class="control-label">User Type<span>*</span></label>
        <select class='form-control' name="data[DiscountCoupon][for_user_type]">
            <option value="">(Select User Type)</option>
            <% _.each(data.user_type, function(value, key){
                var selected = '';
                if(data.DiscountCoupon.for_user_type == false) {
                    selected = '';
                }
                if(key == data.DiscountCoupon.for_user_type){
                    selected = 'selected';
                }
            %> 
                <option value="<%= key %>" <%= selected %> ><%= value %></option>
            <% }); %>
        </select>
    </div>
    <div class="form-group">
        <label class="control-label">Discount Type<span>*</span></label>
        <% _.each(data.discount_type, function(value, key){
            var checked = '';
            if(data.DiscountCoupon.discount_type == false) {
                checked = '';
            }
            if(key == data.DiscountCoupon.discount_type){
                checked = 'checked';
            }
        %>
            <br />
            <input type="radio" class="custom-style" name="data[DiscountCoupon][discount_type]" <%= checked %> value="<%= key %>"><%= value %>
        <% }); %>
    </div>
    <div class="form-group edit-flat-discount">
        <label class="control-label">Flat Discount Rate<span>*</span></label>
        <input type='text' class='form-control' placeholder='Enter Flat Discount Price' value="<%= data.DiscountCoupon.flat_amount %>" name='data[DiscountCoupon][flat_amount]' />
    </div>
    <div class = "edit-discount-per">
        <div class="form-group">
            <label class="control-label">Discount Percentage<span>*</span></label>
            <input type='text' class='form-control' placeholder='Enter Discount Coupon Discount Percentage' value="<%= data.DiscountCoupon.discount_percentage %>" name='data[DiscountCoupon][discount_percentage]' id='discount-amount-edit' />
        </div>
        <div class="form-group">
            <label class="control-label">Max Amount Discount</label>
            <input type='text' class='form-control' placeholder='Enter Discount Coupon Max Amount Discount' value="<%= data.DiscountCoupon.max_amount_discount %>" name='data[DiscountCoupon][max_amount_discount]' />
        </div>
    </div>
    <div class="form-group">
        <input type="checkbox" name="data[DiscountCoupon][is_one_time_use]" class = "custom-style" value="<%= data.DiscountCoupon.is_one_time_use %>" <%= (data.DiscountCoupon.is_one_time_use == true) ? 'checked' : '' %> >
        <label class="control-label">Check If coupon is one time use</label>
    </div>
    <div class="form-group">
        <input type="checkbox" name="data[DiscountCoupon][is_for_bulk_booking]" class = "custom-style" value="<%= data.DiscountCoupon.is_for_bulk_booking %>" <%= (data.DiscountCoupon.is_for_bulk_booking == true) ? 'checked' : '' %> >
        <label class="control-label">Check If coupon is for Bulk Bookings</label>
    </div>
    <div class="form-group">
      <label class="control-label"><?php echo __('Coupon applies to Property Type'); ?></label>
      <?php echo $this->Form->input('property_type_id', 
                                         array(
                                               'class' => 'form-control',
                                               'label' => false,
                                               'id' => 'property_type_id',
                                                'options' => $data['property_type_list'],
                                               'default' => $data['DiscountCoupon']['property_type_id']
                                               )
                                              ); ?>
    </div>	
</script>
