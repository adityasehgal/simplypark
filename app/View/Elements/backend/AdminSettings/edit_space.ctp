<div class="modal fade" id="editSpaceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class="button b-close class="close" data-dismiss="modal" aria-label="Close"">
                     <span>X</span>
                </span>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Edit Space Type'); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create('SpaceType', array(
                                'url' => '/admin/admin_settings/addUpdateSpace',
                                'method' => 'post',
                                'id' => 'edit-space'
                                //'novalidate' => true
                                ));
                ?>
                    <div id="spaceDataContainer">
                    
                    </div>
                    <div class="form-group">
                        <div class="clearfix">                               
                            <div class="pull-right">
                                <?php
                                    echo $this->Form->button('Close', array(
                                                'class' => 'btn btn-default',
                                                'data-dismiss' => 'modal'
                                                ));
                                    echo $this->Form->button('Save', array(
                                                'class' => 'btn btn-primary btn-grad btn-left',
                                                'type' => 'submit'
                                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/html" id="editSpaceField">
	<input type='hidden' name='data[SpaceType][id]' value="<%= data.SpaceType.id %>" />
	<div class="form-group">
		<label class="control-label">Space Type<span>*</span></label>
    	<input type='text' class='form-control' placeholder='Enter SpaceType Name' value="<%= data.SpaceType.name %>" name='data[SpaceType][name]' />
	</div>
</script>