<div class="modal fade custom-modal del-modal" id="serviceConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class = "modal-padding-lg">
          <p class="margin-zero"><b>Are you sure you want to change who pays SimplyPark Commission ? <br>This change will reflect only on new transactions</b></p>
        </div>
      </div>
      <div class="modal-footer btn-center">
        <?php
            echo $this->Html->link('No',
            	'#', 
            	array(
                'data-dismiss' => 'modal',
            		'class' => 'btn btn-default no-service',
            	)
            );
        ?>
        <?php
            echo $this->Html->link('Yes',
            	'#',
            	array(
                'data-dismiss' => 'modal',
            		'class' => 'btn btn-info yes-service',
            	)
            );
        ?>
      </div>
    </div>
  </div>
</div>
