<div class="modal fade" id="editPartnerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class="button b-close class="close" data-dismiss="modal" aria-label="Close"">
                     <span>X</span>
                </span>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Edit Partner'); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create('User', array(
                                'url' => '/admin/admin_settings/addupdatePartner',
                                'method' => 'post',
                                'id' => 'edit-partner',
                                'novalidate' => true,
                                'type' => 'file',
                                'enctype'=>'multipart/form-data'
                                ));
                ?>
                    <div id="partnerDataContainer">
                    
                    </div>
                    <div class="form-group">
                        <div class="clearfix">                               
                            <div class="pull-right">
                                <?php
                                    echo $this->Form->button('Close', array(
                                                'class' => 'btn btn-default',
                                                'data-dismiss' => 'modal'
                                                ));
                                    echo $this->Form->button('Save', array(
                                                'class' => 'btn btn-primary btn-grad btn-left',
                                                'type' => 'submit'
                                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/html" id="editPartnerField">
	<input type='hidden' name='data[Partner][id]' value="<%= data.Partner.id %>" />
	<div class="form-group">
		<label class="control-label">Partner Name<span>*</span></label>
    	<input type='text' class='form-control' placeholder='Enter Partner Name' value="<%= data.Partner.name %>" name='data[Partner][name]' />
    </div>
    <label class="control-label text-">Partner Logo<span>*</span></label>
    <img src="<%= data.path+''+data.Partner.logo %>" alt="Event Image"> 
    <div class="form-group">
        <input type='file' class='edit-upload-logo' value ="<%= data.Partner.logo %>" name='data[Partner][logo]' />
        <div class="text-info">
            <small><?php echo __('Please upload the recomended size of 263*180 for better user experience.'); ?></small>
        </div>
	</div>
</script>