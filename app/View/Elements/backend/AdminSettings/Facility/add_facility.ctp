<div class="modal fade" id="addFacility" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class="button b-close class="close" data-dismiss="modal" aria-label="Close"">
                     <span>X</span>
                </span>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Add Facility'); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create('Facility', array(
                                'url' => '/admin/admin_settings/addUpdateFacility',
                                'method' => 'post',
                                'id' => 'add-facility',
                                'novalidate' => true,
                                'type' => 'file',
                                'enctype'=>'multipart/form-data'
                                ));
                ?>
                    <div class="form-group">
                        <label class="control-label"><?php echo __('Facility'); ?><span>*</span></label>
                        <?php
                            echo $this->Form->input('name', array(
                                        'class' => 'form-control',
                                        'label' => false,
                                        'div' => false,
                                        'placeholder' => 'Enter Facility Name'
                                        ));
                        ?>
                        <label class="control-label"><?php echo __('Upload Icon'); ?></label>
                        <?php
                            echo $this->Form->input('icon', array(
                                        'label' => false,
                                        'div' => false,
                                        'type' => 'file',
                                        'class' => 'upload-icon'
                                        ));
                        ?>
                    </div>	
                    <div class="form-group">
                        <div class="clearfix">                               
                            <div class="pull-right">
                                <?php
                                    echo $this->Form->button(__('Close'), array(
                                                'class' => 'btn btn-default',
                                                'data-dismiss' => 'modal'
                                                ));
                                    echo $this->Form->button(__('Save'), array(
                                                'class' => 'btn btn-primary btn-grad btn-left',
                                                'type' => 'submit'
                                                ));
                                ?>
                            </div>
                        </div> 
                    </div>     
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
