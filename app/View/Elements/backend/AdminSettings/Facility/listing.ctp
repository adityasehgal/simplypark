<?php
    echo $this->element('backend/pagination_options');
?>
<table class="table table-bordered table-striped table-bordered table-hover new-addministartor-table">
    <thead>
        <tr>
            <th>
                <?php echo $this->Paginator->sort('Facility.name', 'Facility Name', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('Facility.modified', 'Last Modified', array('class' => 'tableinherit')); ?>
            </th>
            <th><?php echo __('Status'); ?></th>
            <th><?php echo __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(!empty($getFacilities)) { 
                foreach ($getFacilities as $showFacilities) {
        ?>
                    <tr>
                        <td>
                            <?php echo $showFacilities['Facility']['name']; ?>
                        </td>
                        <td>
                            <?php echo date('Y-m-d', strtotime($showFacilities['Facility']['modified'])); ?>
                        </td>
                        <td>
                        	<?php if ($showFacilities['Facility']['is_activated'] == 0) { ?>
                                <a class="btn btn-danger btn-xs btn-grad" href="#"><?php echo __('Inactive'); ?></a>
                            <?php } else { ?>
                            	<a class="btn btn-success btn-xs btn-grad" href="#"><?php echo __('Active'); ?></a>
                            <?php } ?>
                        </td>
                        <td id="inline-popups">
                            <?php
								$activeInactiveAttr = $this->Admin->activeInactiveIconValues($showFacilities['Facility']['is_activated']);
								echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-'.$activeInactiveAttr['Class'].'"></i>',
                                        $activeInactiveAttr['PopupID'],
                                        array(
                                            'class' => 'btn btn-ok activeinactivemodalbutton magniPopup',
                                            'title' => $activeInactiveAttr['Title'],
                                            'data-effect' => 'mfp-zoom-in',
                                            'escape' => false,
                                            'data-url' => Configure::read('ROOTURL').'admin/admins/changeRecordStatus/'.base64_encode($showFacilities['Facility']['id']).'/Facility'
                                        )
                                    );
                            ?>
                            <?php
                                echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-pencil"></i>',
                                        '#',
                                        array(
                                            'class' => 'btn btn-green edit_facility_data',
                                            'data-effect' => 'mfp-zoom-in',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#editFacilityModal',
                                            'title' => 'Edit',
                                            'data-url' => Configure::read('ROOTURL').'admin/admin_settings/ajaxGetFacilityData/'.base64_encode($showFacilities['Facility']['id']).'.json',
                                            'escape' => false
                                        )
                                    );
                            ?>
                        </td>
                    </tr>
        <?php } } else { ?>
            <tr>
                <td colspan="6">
                    <?php echo __('No Record Found'); ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php
    echo $this->element('backend/pagination');
    echo $this->Js->writeBuffer();
?>