<div class="modal fade" id="editFacilityModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class="button b-close class="close" data-dismiss="modal" aria-label="Close"">
                     <span>X</span>
                </span>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Edit Facility'); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create('User', array(
                                'url' => '/admin/admin_settings/addUpdateFacility',
                                'method' => 'post',
                                'id' => 'edit-facility',
                                'novalidate' => true,
                                'type' => 'file',
                                'enctype'=>'multipart/form-data'
                                ));
                ?>
                    <div id="facilityDataContainer">
                    
                    </div>
                    <div class="form-group">
                        <div class="clearfix">                               
                            <div class="pull-right">
                                <?php
                                    echo $this->Form->button('Close', array(
                                                'class' => 'btn btn-default',
                                                'data-dismiss' => 'modal'
                                                ));
                                    echo $this->Form->button('Save', array(
                                                'class' => 'btn btn-primary btn-grad btn-left',
                                                'type' => 'submit'
                                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/html" id="editFacilityField">
	<input type='hidden' name='data[Facility][id]' value="<%= data.Facility.id %>" />
	<div class="form-group">
		<label class="control-label">Facility<span>*</span></label>
    	<input type='text' class='form-control' placeholder='Enter facility Name' value="<%= data.Facility.name %>" name='data[Facility][name]' />

        <label class="control-label">Facility Icon</label>
        <% if(!_.isEmpty(data.Facility.icon)) { %>
            <img class= "icon" src="<%= data.path+'icon.'+data.Facility.icon %>" alt="Facility Icon"> 
        <% } %>
        <input type='file' class='edit-upload-icon' value ="<%= data.Facility.icon %>" name='data[Facility][icon]' />
	</div>
</script>