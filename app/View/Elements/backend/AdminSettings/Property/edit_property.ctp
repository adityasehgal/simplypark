<div class="modal fade" id="editPropertyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class="button b-close class="close" data-dismiss="modal" aria-label="Close"">
                     <span>X</span>
                </span>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Edit Property Type'); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create('PropertyType', array(
                                'url' => '/admin/admin_settings/addUpdateProperty',
                                'method' => 'post',
                                'id' => 'edit-property'
                                //'novalidate' => true
                                ));
                ?>
                    <div id="propertyDataContainer">
                    
                    </div>
                    <div class="form-group">
                        <div class="clearfix">                               
                            <div class="pull-right">
                                <?php
                                    echo $this->Form->button('Close', array(
                                                'class' => 'btn btn-default',
                                                'data-dismiss' => 'modal'
                                                ));
                                    echo $this->Form->button('Save', array(
                                                'class' => 'btn btn-primary btn-grad btn-left',
                                                'type' => 'submit'
                                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/html" id="editPropertyField">
	<input type='hidden' name='data[PropertyType][id]' value="<%= data.PropertyType.id %>" />
	<div class="form-group">
		<label class="control-label">Property Type<span>*</span></label>
    	<input type='text' class='form-control' placeholder='Enter property type' value="<%= data.PropertyType.name %>" name='data[PropertyType][name]' />
	</div>
</script>