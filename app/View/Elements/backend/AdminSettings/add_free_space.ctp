<div class="modal fade" id="addFreeParkingSpace" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> <!--1 -->
    <div class="modal-dialog"><!-- 2 -->
        <div class="modal-content"><!-- 3 -->
            <div class="modal-header"><!-- 4 -->
                <span class="button b-close class="close" data-dismiss="modal" aria-label="Close"">
                     <span>X</span>
                </span>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Add Informational Space'); ?></h4>
            </div><!-- E4 -->
            <div class="modal-body"><!-- 5 -->
                <?php echo $this->Form->create('Space', array(
                                'url' => '/admin/Spaces/addFreeSpace',
                                'method' => 'post',
                                'id' => 'add-free-space',
                                'novalidate' => true,
                                'class' => "form-horizontal",
                                ));
                ?>
          
                  <div class="form-group"><!-- 6 -->
    				<label for="parkingSpaceName" class="control-label col-sm-3">Space Name</label>
                                        <div class="col-sm-9">
    					<?php
                    		           echo $this->Form->input('name', array(
                    			                           'div' => false,
                    			                           'label' => false,
                    			                           'id' => 'parkingSpaceName',
                    			                           'class' => 'form-control',
                    			                           'placeholder' => 'Parking space name',
                    			                           )
                    		                                 );
                    	                ?>
                                       </div>
                </div><!--form-group--><!-- E6 -->
    		<div class="form-group"><!-- 7 -->
    				<label for="description" class="control-label col-sm-3">Description</label>
                                <div class="col-sm-9">
    				<?php
                    		    echo $this->Form->textarea('description', array(
                    		    	                       'div' => false,
                    			                       'label' => false,
                    			                       'id' => 'description',
                    			                       'class' => 'form-control',
                    			                       'placeholder' => 'Parking description',
                                                               'value' => ''
                    			                      )
                    		                      );
                    	       ?>
                               </div>
                </div><!--form-group--><!-- E7 -->
                <div class="form-group"><!-- 9 -->
                             <label for="address" class="col-sm-3 control-label">Space Type</label>
                             <div class="col-sm-9">
                            <?php
                              echo $this->Form->input('space_type_id', array(
                                                     'div' => false,
                                                     'label' => false,
                                                     'options' => $spaceTypeArr,
                                                     'class' => 'form-control',
                                                     'empty' => 'Type of space',
                                                      )
                                                    );
                             ?>
                             </div>
                 </div><!--form-group--><!--E9-->
                 <div class="form-group"><!-- 10 -->
                             <label for="address" class="col-sm-3 control-label">Property Type</label>
                             <div class="col-sm-9">
                             <?php
                              echo $this->Form->input('property_type_id', array(
                                                    'div' => false,
                                                    'label' => false,
                                                    'options' => $propertyTypeArr,
                                                    'id' => 'property_type_id',
                                                     'class' => 'form-control',
                                                    'empty' => 'Type of property',
                                                    )
                                                  );
                           ?>
                           </div>
                </div><!--form-group--><!--E10-->
                <div class="form-group"><!--11 -->
                   <label for="address" class="col-sm-3 control-label">Address </label>
    		   <div class="col-sm-9">
    		     <?php
                    	echo $this->Form->input('flat_apartment_number', array(
                    			        'div' => false,
                    			        'label' => false,
                    			        'id' => 'address-flat-no',
                    			        'class' => 'form-control',
                    			        'placeholder' => 'Flat/ Apartment Number',
                                               )
                    		              );
                    	?>
   		 </div>
                </div><!--form-group--><!--E11-->
                <div class="form-group"><!--12 -->
                 <div class="col-sm-9 col-sm-offset-3">
    		    <?php
                    	echo $this->Form->input('address1', array(
                    			        'div' => false,
                    			        'label' => false,
                    			        'id' => 'address1',
                    			        'class' => 'form-control',
                    			        'placeholder' => 'Address Line 1',
                    			       )
                    		             );
                    	?>
                 </div>

                </div><!--form-group--><!--E12-->
                
                <div class="form-group"><!--13 -->
                 <div class="col-sm-9 col-sm-offset-3">
    		    <?php
                    	echo $this->Form->input('address2', array(
                    			        'div' => false,
                    			        'label' => false,
                    			        'id' => 'address2',
                    			        'class' => 'form-control',
                    			        'placeholder' => 'Address Line 2',
                    			       )
                    		             );
                    	?>
                 </div>
                </div><!--form-group--><!--E13-->
    		<div class="form-group"><!--E14-->
    			<div class="col-sm-4 col-sm-offset-3">
    			<?php
                   		echo $this->Form->input('state_id', array(
                    			                'div' => false,
                    			                'label' => false,
                    			                'options' => $statesArr,
                    			                'id' => 'state',
                    			                'class' => 'form-control',
                    			                )
                    		                      );
                    ?>
    		    </div>
    		    <div class="col-sm-4" id= "list-city">
                        <?php
                                    echo $this->Form->input('city_id', array(
                                        'div' => false,
                                        'label' => false,
                                        'options' => $citiesArr,
                                        'id' => 'city',
                                        'class' => 'form-control',
                                        'empty' => 'City',
                                        )
                                    );
                        ?>
    		   </div>
    	        </div><!--form-group--><!--E14-->
    		<div class="form-group"><!--E15-->
    		  <div class="col-sm-4 col-sm-offset-3">
    			<?php
                      	   echo $this->Form->input('post_code', array(
                    			            'div' => false,
                    			            'label' => false,
                    			            'id' => 'pinCode',
                    			            'class' => 'form-control',
                    			            'placeholder' => 'Pin Code',
                                                    'type' => 'text',
                    			          )
                    		                );
                    	?>
    		 </div>
    		</div><!--form-group--><!--E15-->
    		<div class="form-group"><!--16-->
                   <label for="lat/long" class="col-sm-3 control-label">Lat/Long</label>
                   <div class="col-sm-4"> 
			<?php
            		echo $this->Form->input('lat', array(
            			'div' => false,
            			'label' => false,
            			'id' => 'lat',
                                'type' => 'text',
            			'class' => 'form-control',
            			'placeholder' => 'Latitude',
            			)
            		);
                       ?>
                    </div> 
                   <div class="col-sm-4">
                     <?php
            		echo $this->Form->input('lng', array(
            			'div' => false,
            			'label' => false,
            			'id' => 'long',
                                 'type' => 'text',
            			'class' => 'form-control',
            			'placeholder' => 'Longitude',
            			)
            		);
            	     ?>
                   </div>

                </div><!--form-group--><!--E16-->
		<div class="form-group check-box-inline"><!--17-->
		   <label for="amenties" class="col-sm-3 control-label">Amenties</label>
		   <div class="col-sm-9">
                    <?php $i = 0; ?>
			<?php foreach($facilities as $key => $value) { ?>
				<div class="checkbox">
				<label>
				 <?php 
                                    $checked = false;

				   echo $this->Form->checkbox('SpaceFacility.facility_id', array(
                                                              'hiddenField' => false,
                                                              'value' => $key,
                                                              'name' => 'data[Space][facility_id]['.$i .']',
                                                              'checked' => $checked
                                                              )
                                                          );
				  echo $value;
                                  $i++;
				 ?>
				 </label>
				 </div>
			 <?php } ?>
                   </div>
                </div><!--form-group--><!--E17-->
		<div class="form-group"><!--18-->
		   <label for="address" class="col-sm-3 control-label">Available spaces</label>
		   <div class="col-sm-9 form-inline input-margin adjust-checkbox">
                    <?php
                        echo $this->Form->input('number_slots', array(
                                                'div' => false,
                                                'label' => false,
                                                'type' => 'text',
                                                'class' => 'form-control',
                                                'placeholder' => 'Number of slots',
                                                'id' => 'total-slots',
                                                )
                                            );
                    ?>
                   </div>
                 </div><!--E18-->

                 <div class="form-group week-box-inline"><!--19-->
		   <label for="parkingAvailbility" class="col-sm-3 control-label">Parking Available On</label>
		   <div class="col-sm-9">
                      <?php
			echo $this->Form->checkbox('SpaceTimeDay.mon', array(
									     'hiddenField' => false,
									     'class' => 'wdays check-days',
                                                                             'checked' => true,
					                                     )
                                                   );	
					echo __('Mon&nbsp;&nbsp;');
                      ?>
                      <?php
			echo $this->Form->checkbox('SpaceTimeDay.tue', array(
									     'hiddenField' => false,
									     'class' => 'wdays check-days',
                                                                             'checked' => true,
					                                     )
                                                   );	
					echo __('Tue&nbsp;&nbsp;');
                      ?>
                      <?php
			echo $this->Form->checkbox('SpaceTimeDay.wed', array(
									     'hiddenField' => false,
									     'class' => 'wdays check-days',
                                                                             'checked' => true,
					                                     )
                                                   );	
					echo __('Wed&nbsp;&nbsp;');
                      ?>
                      <?php
			echo $this->Form->checkbox('SpaceTimeDay.thu', array(
									     'hiddenField' => false,
									     'class' => 'wdays check-days',
                                                                             'checked' => true,
					                                     )
                                                   );	
					echo __('Thurs&nbsp;&nbsp;');
                      ?>
                      <?php
			echo $this->Form->checkbox('SpaceTimeDay.fri', array(
									     'hiddenField' => false,
									     'class' => 'wdays check-days',
                                                                             'checked' => true,
					                                     )
                                                   );	
					echo __('Fri&nbsp;&nbsp;');
                      ?>
                      <?php
			echo $this->Form->checkbox('SpaceTimeDay.sat', array(
									     'hiddenField' => false,
									     'class' => 'wdays check-days',
                                                                             'checked' => true,
					                                     )
                                                   );	
					echo __('Sat&nbsp;&nbsp;');
                      ?>
                      <?php
			echo $this->Form->checkbox('SpaceTimeDay.sun', array(
									     'hiddenField' => false,
									     'class' => 'wdays check-days',
                                                                             'checked' => true,
					                                     )
                                                   );	
					echo __('Sun&nbsp;&nbsp;');
                      ?>
                   </div>
                  </div><!--form-group--><!--E19-->
                  <div class="form-group"><!--20-->
			<label for="openingTime" class="col-sm-3 control-label"><?php echo __('Operating Hours'); ?></label>
                        <div class="col-sm-8">
                          <div class="checkbox">
                          <?php
				echo $this->Form->checkbox('SpaceTimeDay.open_all_hours', array(
												'hiddenField' => false,
												'checked'=> false, 
												'class' => '24-hours-open',
												) 
                                                          );
                                echo "Open 24 Hours";
                           ?>
                          </div>
                        </div>
                 </div><!--form-group--><!--E20-->
                 <div class="form-group"><!--21-->
		   <label for="From Time" class="col-sm-3 control-label"><?php echo __('From'); ?></label>
                   <div class="col-sm-9">
			<?php
		           echo $this->Form->input('SpaceTimeDay.from_time', array(
		                			               'div' => false,
		                			               'label' => false,
		                			               'id' => 'start-time',
		                			               'type' => 'text',
		                			               'class' => 'form-control',
		                			               'placeholder' => 'Opening Time(24 hour format)',
		                			              )
		                		  );
                         ?>
                         &nbsp;&nbsp;
			<?php
		           echo $this->Form->input('SpaceTimeDay.to_time', array(
		                			               'div' => false,
		                			               'label' => false,
		                			               'id' => 'end-time',
		                			               'type' => 'text',
		                			               'class' => 'form-control',
		                			               'placeholder' => 'Closing Time(24 hour format)',
		                			              )
		                		  );
                         ?>
                   </div>
                 </div><!--form-group--><!--E21-->

                 <div class="form-group price"> <!--32-->
		   <label for="Price" class="col-sm-3 control-label">Price</label>
                   <div class="col-sm-4">
                    <?php
                        echo $this->Form->checkbox('is_parking_free',array( 
                                                                            'checked' => false 
                                                                           ) 
                                                  );
                        echo "&nbsp;Is Parking Free";
                     ?>
                    </div>
                  </div>
                 <div class="form-group price"> <!--22-->
		   <label for="Price" class="col-sm-3 control-label">Price</label>
                   <div class="col-sm-4">
                    <?php
                        echo $this->Form->checkbox('tier_pricing_support',array( 
                                                                            'checked' => false 
                                                                           ) 
                                                  );
                        echo "&nbsp;Tier Pricing Support";
                     ?>
                    </div>
                   <div class="col-sm-5">
                   <?php
                        echo $this->Form->input('tier_desc', array(
                                                                  'div' => false,
                                                                  'id' => 'tier_desc',
                                                                  'label' => false,
                                                                  'type' => 'textarea',
                                                                  'class' => 'form-control',
                                                                  'placeholder' => 'Please enter your hourly pricing. Example Upto 4 hours : Rs 10, 4-8 hours: Rs. 20 and so on...',
                                                                  )
                                                            );
                    ?>
                   </div>

                 </div><!--form-group--><!--E22-->

                 <div class="form-group price"> <!--23 -->
		    <label for="price" class="col-sm-2 control-label">Price</label>
                    <div class="col-sm-2 hourly-input">
                        <?php
                            echo $this->Form->input('SpacePricing.hourly', array(
                                	'type' => 'text',
                                        'div' => false,
                                        'label' => false,
                                        'id' => 'hourly',
                                        'class' => 'form-control',
                                        'placeholder' => 'Hourly',
                                        )
                                      );
                       ?>
                    </div>
		    <div class="col-sm-2 daily-input">
			<?php
                	   echo $this->Form->input('SpacePricing.daily', array(
                			'type' => 'text',
                			'div' => false,
                			'label' => false,
                			'id' => 'daily',
                			'class' => 'form-control',
                			'placeholder' => 'Daily',
                			)
                		);
                	?>
		    </div>
                    <div class="col-sm-2 weekly-input">
                      <?php
                        echo $this->Form->input('SpacePricing.weekely', array(
                        	'type' => 'text',
                                'div' => false,
                                'label' => false,
                                'id' => 'weekely',
                                'class' => 'form-control',
                                'placeholder' => 'Weekely',
                               )
                        );
                    ?>
                   </div>
		   <div class="col-sm-2">
		     <?php
                	echo $this->Form->input('SpacePricing.monthly', array(
                			'type' => 'text',
                			'div' => false,
                			'label' => false,
                			'id' => 'monthly',
                			'class' => 'form-control',
                			'placeholder' => 'Monthly',
                			)
                		);
               	      ?>
		   </div>
		   <div class="col-sm-2">
			<?php
                        echo $this->Form->input('SpacePricing.yearly', array(
                		'type' => 'text',
                		'div' => false,
                		'label' => false,
                		'id' => 'yearly',
                		'class' => 'form-control',
                		'placeholder' => 'Yearly',
                		)
                	);
               	       ?>
		  </div>


                 </div><!--form-group--><!--E23-->




		<div class="footer clearfix col-xs-12 border-top">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10 text-right">
					<?php
                        echo $this->Form->button('Cancel', array(
                        	                          'class' => 'btn btn-default',
                                                          'data-dismiss' => 'modal'));
                    ?>
                    <?php
                        echo $this->Form->button(__('Add Space'), array(
                                    'class' => 'btn btn-info',
                                    'id' => 'get-lat-long',
                                    'type' => 'submit'
                                    ));
                    ?>
				</div>
			</div>
		</div>
	<?php echo $this->Form->end(); ?>
             
              </div><!--E5-->
            </div><!--E3-->
        </div><!--E2-->
      </div><!--E1-->
