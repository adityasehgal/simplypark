<?php
    echo $this->Html->script('backend/Booking/booking.js',array('inline' => false)); 
    echo $this->element('backend/pagination_options');
?>
<table class="table table-bordered new-addministartor-table">
    <thead>
        <tr>
            <th>
                <?php echo $this->Paginator->sort('Booking.first_name', 'Name', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('Booking.space_id', 'Space', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('Booking.start_date', 'Start Date', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('Booking.end_date', 'End Date', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('Booking.booking_type', 'Booking Type', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('Booking.booking_price', 'Booking Price', array('class' => 'tableinherit')); ?>
            </th>
           
            <th><?php echo __('Email'); ?></th>
            <th><?php echo __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(!empty($bookingsData)) { 
                
                foreach ($bookingsData as $bookingData) {
                   
        ?>
                    <tr class="">
                        <td>
                            <?php echo $bookingData['Booking']['first_name'].' '.$bookingData['Booking']['last_name']; ?>
                        </td>
                        <td>
                            <?php echo $bookingData['Space']['name']; ?>
                        </td>
                        <td>
                            <?php echo $bookingData['Booking']['start_date']; ?>
                        </td>
                        <td>
                            <?php echo $bookingData['Booking']['end_date']; ?>
                        </td>
                        <td>
                            <?php 
                                if ($bookingData['Booking']['booking_type'] == 1) {
                                        echo 'Hourly';
                                } elseif ($bookingData['Booking']['booking_type'] == 2) {
                                        echo 'Daily';    
                                } elseif ($bookingData['Booking']['booking_type'] == 3) {
                                        echo 'Weekly'; 
                                } elseif ($bookingData['Booking']['booking_type'] == 4) {
                                        echo 'Monthly';
                                } elseif ($bookingData['Booking']['booking_type'] == 5) {
                                        echo 'Yearly';
                                } else {
                                    echo 'Invalid';
                                }
                            ?>
                        </td>
                        <td>
                            <?php echo $bookingData['Booking']['booking_price']; ?>
                        </td>
                        <td>
                            <?php echo $bookingData['Booking']['email']; ?>
                        </td>
                        
                        <td>
                           <?php 
                                    echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-search"></i>',
                                        '#',
                                        array(
                                            'class' => 'btn btn-green view_booking_data',
                                            'data-effect' => 'mfp-zoom-in',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#viewUserModal',
                                            'title' => 'View Details',
                                            'data-booking-id' => base64_encode($bookingData['Booking']['id']),
                                            //'data-url' => Configure::read('ROOTURL').'admin/Bookings/ajaxGetFrontUserData/'.base64_encode($bookingData['Booking']['id']).'.json',
                                            'escape' => false
                                        )
                                    );
                                    
                               
                            ?>
                            
                        </td>
                    </tr>
        <?php } } else { ?>
            <tr>
                <td colspan="10">
                    <?php echo __('No Record Found'); ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php
    echo $this->element('backend/Booking/bookingDetailsModal');
    echo $this->element('backend/pagination');
    echo $this->Js->writeBuffer();
?>
