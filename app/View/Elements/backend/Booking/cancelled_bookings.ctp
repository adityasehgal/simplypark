<?php
    echo $this->Html->script('backend/Booking/booking.js',array('inline' => false)); 
    echo $this->element('backend/pagination_options');
?>
<?php
    echo $this->Form->create('', array(
                    'url' => '/admin/bookings/change_booking_refund_status',
                    'type' => 'post',
                    'novalidate' => true,
                    'class' => 'form-horizontal'
                    ));
?>
<table class="table table-bordered new-addministartor-table">
    <thead>
        <tr>
            <th></th>
            <th>
                <?php echo $this->Paginator->sort('Booking.id', 'Booking Id', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('Booking.first_name', 'Name', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('Booking.space_id', 'Space', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('Booking.start_date', 'Start Date', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('Booking.end_date', 'End Date', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('Booking.booking_type', 'Booking Type', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('Booking.booking_price', 'Booking Price', array('class' => 'tableinherit')); ?>
            </th>
           
            <th><?php echo __('Email'); ?></th>

            <th>
                <?php echo $this->Paginator->sort('Booking.cancellation_fees,', 'Cancellation Fees', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('Booking.total_refund_paid ,', 'Total Refund Paid', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('Booking.first_name', 'Refund to', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('Booking.refundable_deposit', 'Refundable Deposit', array('class' => 'tableinherit')); ?>
            </th>
        
            <th><?php echo __('View Details'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(!empty($bookingsData)) { 
                
                foreach ($bookingsData as $bookingData) {
                    
                    if($bookingData['Booking']['status'] == Configure::read('BookingStatus.Cancelled')) {
                       
                        $class = 'alert-danger';

                    } else {
                        
                        $class = 'alert-success';
                    }
                   
        ?>
                    <tr class="<?php echo $class; ?>">
                        <td>
                            <?php
                                if($bookingData['Booking']['refundable_deposit'] > 0) {
                                    echo $this->Form->input(
                                            'Booking.id.',
                                
                                            array(
                                                'type' => 'checkbox',
                                                'div' => false,
                                                'label' => false,
                                                'escape' => false,
                                                'value' => $bookingData['Booking']['id'],
                                                'hiddenField' => false                
                                        ));
                                    
                                } ?>
                            
                           

                        </td>
                        <td>
                            <?php echo str_replace('pay_', 'SP_', $bookingData['Transaction']['payment_id']); ?>
                        </td>
                        <td>
                            <?php echo $bookingData['Booking']['first_name'].' '.$bookingData['Booking']['last_name']; ?>
                        </td>
                        <td>
                            <?php echo $bookingData['Space']['name']; ?>
                        </td>
                        <td>
                            <?php echo $bookingData['Booking']['start_date']; ?>
                        </td>
                        <td>
                            <?php echo $bookingData['Booking']['end_date']; ?>
                        </td>
                        <td>
                            <?php 
                                if ($bookingData['Booking']['booking_type'] == 1) {
                                        echo 'Hourly';
                                } elseif ($bookingData['Booking']['booking_type'] == 2) {
                                        echo 'Daily';    
                                } elseif ($bookingData['Booking']['booking_type'] == 3) {
                                        echo 'Weekly'; 
                                } elseif ($bookingData['Booking']['booking_type'] == 4) {
                                        echo 'Monthly';
                                } elseif ($bookingData['Booking']['booking_type'] == 5) {
                                        echo 'Yearly';
                                } else {
                                    echo 'Invalid';
                                }
                            ?>
                        </td>
                        <td>
                            <?php echo $bookingData['Booking']['booking_price']; ?>
                        </td>
                        <td>
                            <?php echo $bookingData['Booking']['email']; ?>
                        </td>
                       
                        <td>
                            <?php echo $bookingData['Booking']['cancellation_fees']; ?>
                        </td>
                        <td>
                            <?php echo $bookingData['Booking']['total_refund_paid']; ?>
                        </td>
                        <td>
                            <?php echo $bookingData['Booking']['first_name'].' '.$bookingData['Booking']['last_name']; ?>
                        </td>

                        <td>
                            <?php echo $bookingData['Booking']['refundable_deposit']; ?>
                        </td>
                        <td>
                           <?php 
                                    echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-search"></i>',
                                        '#',
                                        array(
                                            'class' => 'btn btn-green view_booking_data',
                                            'data-effect' => 'mfp-zoom-in',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#viewUserModal',
                                            'title' => 'View Details',
                                            'data-booking-id' => base64_encode($bookingData['Booking']['id']),
                                            //'data-url' => Configure::read('ROOTURL').'admin/Bookings/ajaxGetFrontUserData/'.base64_encode($bookingData['Booking']['id']).'.json',
                                            'escape' => false
                                        )
                                    );
                                    
                               
                            ?>
                            
                        </td>
                    </tr>
        <?php } } else { ?>
            <tr>
                <td colspan="10">
                    <?php echo __('No Record Found'); ?>
                </td>
            </tr>
        <?php } ?>


    </tbody>


</table>
<?php if(!empty($bookingsData)) { ?>
    <div class="col-xs-4 col-center">
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-5 control-label">Bank Refrence Code</label>
        <div class="col-sm-7">
          <?php
                echo $this->Form->input(
                    'Booking.bank_reference_code_refundable_deposit',
                    array(
                        'type' => 'text',
                        'div' => false,
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'escape' => false,
                        //'value' => $refundableBookingData['Booking']['id']                
                ));
            ?>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-5 col-sm-7">
            <?php
                echo $this->Form->button(
                    'Refund Deposit',
                    array(
                        'type' => 'submit',
                        'class'=> 'btn btn-primary',
                        'escape' => false
                        )
                );
            ?>
        </div>
      </div>
    </div>
<?php } ?>
    
<?php echo $this->Form->end(); ?>

    
<?php
    echo $this->element('backend/Booking/bookingDetailsModal');
    echo $this->element('backend/pagination');
    echo $this->Js->writeBuffer();
?>
