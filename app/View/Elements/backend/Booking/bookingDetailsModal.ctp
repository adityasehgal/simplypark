<!-- Modal -->
 <div class="modal fade location-details-popup" id="viewBookingDetailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                 <span class="button b-close class="close" data-dismiss="modal" aria-label="Close"">
                     <span>X</span>
                </span>
            <h4 class="modal-title" id="myModalLabel">View Details</h4>
            </div>
            <div class="modal-body clearfix" id='bookingTemplate'>
                
            </div>
        </div>
    </div>
</div> 
<script type="text/html" id="bookingInfoContainer">
    <div class="form-group">
        <div class="col-xs-12 clearfix">
            <div class="col-xs-4">
                <label class="control-label">Space Name</label>
            </div>

            <div class="col-xs-8 clearfix">
                <%= booking_data.Space.name %>
           </div>
        </div>
        <div class="col-xs-12 clearfix">
            <div class="col-xs-4">
                <label class="control-label">Name</label>
            </div>

            <div class="col-xs-8 clearfix">
                <%= booking_data.Booking.first_name  %> <%= booking_data.Booking.last_name  %> 
           </div>
        </div>
        <div class="col-xs-12 clearfix">
            <div class="col-xs-4">
                <label class="control-label">Driver Email</label>
            </div>

            <div class="col-xs-8 clearfix">
                <%= booking_data.Booking.email  %> 
           </div>
        </div>
         <div class="col-xs-12 clearfix">
            <div class="col-xs-4">
                <label class="control-label">Mobile</label>
            </div>

            <div class="col-xs-8 clearfix">
                <%= booking_data.Booking.mobile  %> 
           </div>
        </div>
        <div class="col-xs-12 clearfix">
            <div class="col-xs-4">
                <label class="control-label">Start Date</label>
            </div>

            <div class="col-xs-8 clearfix">
                <%= booking_data.Booking.start_date %>
           </div>
        </div>
        <div class="col-xs-12 clearfix">
            <div class="col-xs-4">
                <label class="control-label">End Date</label>
            </div>

            <div class="col-xs-8 clearfix">
                <%= booking_data.Booking.end_date %>
           </div>
           </div>
        </div>

         <div class="col-xs-12 clearfix">
            <div class="col-xs-4">
                <label class="control-label">Booking Id</label>
            </div>

            <div class="col-xs-8 clearfix">
               <% str = booking_data.Transaction.payment_id %>
               <%= str.replace('pay_','SP_') %>
           </div>
        </div>
        <div class="col-xs-12 clearfix"> 
            <div class="col-xs-4">
                <label class="control-label">Booking Type</label>
            </div>

            <div class="col-xs-8 clearfix">
                
                <% if (booking_data.Booking.booking_type == 1) { %>
                    Hourly
                <% } else if (booking_data.Booking.booking_type == 2) { %>
                    Daily
                <% } else if (booking_data.Booking.booking_type == 3) { %>
                    Weekly
                <% } else if (booking_data.Booking.booking_type == 4) { %>
                    Monthly
                 <% } else if (booking_data.Booking.booking_type == 5) { %>
                    Yearly
                <% } else { %>
                    Invalid
                <% } %>
           </div>
        </div>
        <div class="col-xs-12 clearfix">
            <div class="col-xs-4">
                <label class="control-label">Booking Price</label>
            </div>

            <div class="col-xs-8 clearfix">
               <%= booking_data.Booking.booking_price %>
           </div>
        </div>
        <div class="col-xs-12 clearfix">
            <div class="col-xs-4">
                <label class="control-label">Discount</label>
            </div>

            <div class="col-xs-8 clearfix">
               <%= booking_data.Booking.discount_amount %>
           </div>
        </div>
        <div class="col-xs-12 clearfix">
            <div class="col-xs-4">
                <label class="control-label">Commission</label>
            </div>

            <div class="col-xs-8 clearfix">
                <%= booking_data.Booking.commission %>
           </div>
        </div>
        <div class="col-xs-12 clearfix">
            <div class="col-xs-4">
                <label class="control-label">Owner Income</label>
            </div>

            <div class="col-xs-8 clearfix">
                <%= booking_data.Booking.owner_income %>
           </div>
        </div>
        <div class="col-xs-12 clearfix">
            <div class="col-xs-4">
                <label class="control-label">Owner Income After Cancellation</label>
            </div>

            <div class="col-xs-8 clearfix">
                <%= booking_data.Booking.owner_income_after_cancellation %>
           </div>
        </div>
        <div class="col-xs-12 clearfix">
            <div class="col-xs-4">
                <label class="control-label">Owner Service Tax</label>
            </div>

            <div class="col-xs-8 clearfix">
                <%= booking_data.Booking.owner_service_tax_amount %>
           </div>
        </div>
        <div class="col-xs-12 clearfix">
            <div class="col-xs-4">
                <label class="control-label">Simply Park Income</label>
            </div>

            <div class="col-xs-8 clearfix">
                <%= booking_data.Booking.simplypark_income %>
           </div>
        </div>
        <div class="col-xs-12 clearfix">
            <div class="col-xs-4">
                <label class="control-label">Simply Park Service Tax</label>
            </div>

            <div class="col-xs-8 clearfix">
                <%= booking_data.Booking.simplypark_service_tax_amount %>
           </div>
        </div>
         <div class="col-xs-12 clearfix">
            <div class="col-xs-4">
                <label class="control-label">Driver Withdrawal Source Account</label>
            </div>

            <div class="col-xs-8 clearfix">
                <% if (_.isEmpty(booking_data.User.WithdrawalSources)) { %>
                     No Source Added   
                <% } else { %>
                    <% var accountNmuber = _.map(booking_data.User.WithdrawalSources, function(field){ return field.account_number ; }) %>
                    <%= accountNmuber %>
               <% } %>
           </div>
        </div>
        
         <% if (_.isEmpty(booking_data.BookingInstallment) == false) { %>

            <div class="col-xs-12 clearfix">
                <div class="col-xs-4">
                    <label class="control-label">Booking Installments</label>
                </div>

                <div class="col-xs-8 clearfix">
                    <table class="table table-bordered installmentTable">
                    <th>Income</th>
                    <th>Bank Reference Code</th>
                    <th>Action</th>
                    <% _.each(booking_data.BookingInstallment, function(bookingInstallment,key) { %>
                            
                                <tr class="InstallmentRow">
                                   
                                    <td> <% if (bookingInstallment.owner_income != null) {  %> 
                                                <%= '&#8377; '+bookingInstallment.owner_income %>
                                        <% } else { %>
                                                N/A
                                        <% } %>
                                    </td>
                                     <% if (bookingInstallment.paid_to_owner != true) { %>
                                        <td>
                                            <form class="payForm<%= key %>">
                                                <input type="text" class="form-control" name="branch" placeholder="Enter Bank Code"></input> 
                                            </form>
                                         </td>
                                        <td> <a data-id="<%= bookingInstallment.id %>"  class="btn btn-sm btn-info pay">Pay</a></td>

                                    <% } else { %>
                                    <td><%= bookingInstallment.bank_reference_code %> </td>
                                       <td>
                                            Paid
                                         </td>
                                        
                                   <% } %>
                                </tr>
                            </form>
                        </
                    <% }); %>
                    </table>
                </div>
            </div>
        <% } %>
    </div>
    
</script>
