<?php
    echo $this->Html->script('backend/Booking/booking.js',array('inline' => false)); 
    echo $this->element('backend/pagination_options');
?>
<table class="table table-bordered new-addministartor-table" style="font-size:13px">
    <thead>
        <tr>
            <th>
                <?php echo $this->Paginator->sort('Booking.id', 'Booking Id', array('class' => 'tableinherit')); ?>
            </th>
             <th>
                <?php echo $this->Paginator->sort('Booking.first_name', 'User Name', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('Booking.status', 'Booking Status', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('Booking.start_date', 'Date', array('class' => 'tableinherit')); ?>
            </th>
            <th> <?php echo $this->Paginator->sort('', 'Owner Email', array('class' => 'tableinherit')); ?> </th>
             <th> <?php echo $this->Paginator->sort('', 'Owner Ot. Withdrawal Sources', array('class' => 'tableinherit')); ?> </th>
            <th>
                <?php echo $this->Paginator->sort('Booking.owner_income', 'Owner Income', array('class' => 'tableinherit')); ?>
            </th>
             <th>
                <?php echo __('Owner Activated Account'); ?>
            </th>
            
            <th><?php echo __('Actions'); ?></th>
            <th><?php echo __('Status'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(!empty($bookingsData)) { 
                
           // pr($bookingsData);die;
                foreach ($bookingsData as $bookingData) {

                    if(empty($bookingData['BookingInstallment'])) {

                        $class = 'alert-success';
                    
                    } else {

                        $class = 'alert-danger';

                    }
                   
        ?>
                    <tr class="<?php echo $class; ?>">
                         <td>
                            <?php echo str_replace('pay_', 'SP_', $bookingData['Transaction']['payment_id']); ?>
                        </td>
                         <td>
                            <?php echo $bookingData['Booking']['first_name'].' '.$bookingData['Booking']['last_name'];?>
                        </td>
                         <td>
                            <?php echo $action = $this->UserData->bookingActions($bookingData['Booking']['end_date'], $bookingData['Booking'][Configure::read('BookingTableStatusField')]);?>
                        </td>
                        <td>
                            <?php echo date('d M,y H:i A', strtotime($bookingData['Booking']['start_date'])) . ' - ' . date('d M,y H:i A', strtotime($bookingData['Booking']['end_date'])); ?>
                        </td>
                        <td>
                            <?php echo $bookingData['OwnerId']['email'] ; ?>
                        </td>
                        <td>
                            <?php
                            $bankAccount = $this->Front->getAccounts($bookingData['Booking']['space_user_id'],0,'all');
                            
                            foreach ($bankAccount as $value) {
                                $bankAccountNumber = !empty($bankAccount) ? $value['WithdrawalSources']['account_number'] : 'N/A';
                                $ifsc_code = !empty($bankAccount) ? $value['WithdrawalSources']['ifsc_code'] : 'N/A';
                                
                                echo 'Bank Account No. : '.$bankAccountNumber.'<br>';
                                echo 'IFSC Code : '.$ifsc_code.'<br><br>'; 
                            }
                            
                                
                            ?> 
                        </td>
                        <td>
                            <?php 
                                $ownerIncome = $bookingData['Booking']['owner_income'];
                                if($bookingData['Booking'][Configure::read('BookingTableStatusField')] == Configure::read('BookingStatus.Cancelled') || $bookingData['Booking'][Configure::read('BookingTableStatusField')] == Configure::read('BookingStatus.CancellationRequested')) {

                                    $ownerIncome = $bookingData['Booking']['owner_income_after_cancellation'];
                                
                                } 


                                echo number_format($ownerIncome,2);
                            ?>
                        </td>

                            
                        
                        <td>
                            <?php
                            $bankAccount = $this->Front->getAccounts($bookingData['Booking']['space_user_id'],Configure::read('Bollean.True'),'first');
                            
                            $ownerDetails = $this->Front->getOwnerDetails($bookingData['Booking']['space_user_id']);
                            $bankAccountNumber = !empty($bankAccount) ? $bankAccount['WithdrawalSources']['account_number'] : 'N/A';
                            $ifsc_code = !empty($bankAccount) ? $bankAccount['WithdrawalSources']['ifsc_code'] : 'N/A';
                                echo 'Owner Name : '.$bookingData['OwnerDetail']['first_name'].' '.$bookingData['OwnerDetail']['last_name'].'<br>' ;
                                echo 'Bank Account No. : '.$bankAccountNumber.'<br>';
                                echo 'IFSC Code : '.$ifsc_code.'<br>'; 
                                echo 'Contact No. : '.$bookingData['OwnerDetail']['mobile'].'<br>'; 
                            ?> 
                        </td>
                        
                        <td>
                           <?php 
                                    echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-search"></i>',
                                        '#',
                                        array(
                                            'class' => 'btn btn-green view_booking_data',
                                            'data-effect' => 'mfp-zoom-in',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#viewUserModal',
                                            'title' => 'View Details',
                                            'data-booking-id' => base64_encode($bookingData['Booking']['id']),
                                            'escape' => false
                                        )
                                    );
                                    
                               
                            ?>
                            <?php
                                if(empty($bookingData['BookingInstallment'])) {
                                    
                                    if($bookingData['Booking']['paid_to_owner'] != true) {
                                        echo $this->Html->link(
                                            '<i class="glyphicon glyphicon-ok"></i>',
                                            '#deleteConfirm',
                                            array(
                                                'class' => 'btn btn-ok activeinactivemodalbutton magniPopup',
                                                'title' => 'Change Paid',
                                                'data-effect' => 'mfp-zoom-in',
                                               
                                                'escape' => false,
                                                'data-id' => base64_encode($bookingData['Booking']['id'])
                                            )
                                        );
                                    } 
                                    //else {
                                    //     echo 'Reference Code : '.$bookingData['Booking']['bank_reference_code'];
                                    // }
                                     
                                }
                                
                            ?>
                            
                        </td>
                        <td>
                            <?php if(empty($bookingData['BookingInstallment'])) {
                             $refCode = !empty($bookingData['Booking']['bank_reference_code']) ? $bookingData['Booking']['bank_reference_code'] : 'N/A'; 
                                echo 'Reference Code : '.$refCode;
                             } else {
                                foreach ($bookingData['BookingInstallment'] as $key => $value) {
                                    $refCode = !empty($value['bank_reference_code']) ? $value['bank_reference_code'] : 'N/A';
                                    echo 'Reference Code : '.$refCode;
                                }
                             } 
                            ?>
                        </td>
                    </tr>
        <?php } } else { ?>
            <tr>
                <td colspan="10">
                    <?php echo __('No Record Found'); ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php
    echo $this->element('backend/Booking/bookingDetailsModal');
    echo $this->element('backend/pagination');
    echo $this->Js->writeBuffer();
?>
