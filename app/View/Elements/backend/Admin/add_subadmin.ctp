<div class="modal fade" id="addSubAdminModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class="button b-close class="close" data-dismiss="modal" aria-label="Close"">
                     <span>X</span>
                </span>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Add New Sub-Admin'); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create('User', array(
                                'url' => '/admin/admins/addSubadmin',
                                'method' => 'post',
                                'id' => 'add-sub-admin',
                                'novalidate' => true
                                ));
                ?>
                    <div class="form-group">
                        <label class="control-label"><?php echo __('First Name'); ?><span>*</span></label>
                        <?php
                            echo $this->Form->input('UserProfile.first_name', array(
                                        'class' => 'form-control',
                                        'label' => false,
                                        'div' => false,
                                        'placeholder' => 'First Name'
                                        ));
                        ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __('Last Name'); ?><span>*</span></label>
                        <?php
                            echo $this->Form->input('UserProfile.last_name', array(
                                        'class' => 'form-control',
                                        'label' => false,
                                        'div' => false,
                                        'placeholder' => 'Last Name'
                                        ));
                        ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __('Username'); ?><span>*</span></label>
                        <?php
                            echo $this->Form->input('username', array(
                                        'class' => 'form-control',
                                        'label' => false,
                                        'div' => false,
                                        'placeholder' => 'Username'
                                        ));
                        ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __('Email'); ?><span>*</span></label>
                        <?php
                            echo $this->Form->input('email', array(
                                        'class' => 'form-control',
                                        'label' => false,
                                        'div' => false,
                                        'placeholder' => 'Email'
                                        ));
                        ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __('New Password'); ?><span>*</span></label>
                        <?php
                            echo $this->Form->input('password', array(
                                        'class' => 'form-control newpassword',
                                        'type' => 'password',
                                        'label' => false,
                                        'div' => false,
                                        'placeholder' => 'New Password',
                                        'id' => 'subadmin-new-password',
                                        'data-display' => 'subAdminPassword'
                                        ));
                        ?>
                        <div class="left passwordStrength" id="subAdminPassword"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __('Confirm Password'); ?><span>*</span></label>
                        <?php
                            echo $this->Form->input('confirm_password', array(
                                        'class' => 'form-control',
                                        'type' => 'password',
                                        'label' => false,
                                        'div' => false,
                                        'placeholder' => 'Confirm Password'
                                        ));
                        ?>
                    	</div>
							<div class="form-group">
                        <label class="control-label"><?php echo __('Mobile'); ?><span>*</span></label>
                        <?php
                            echo $this->Form->input('UserProfile.mobile', array(
                                        'class' => 'form-control',
                                        'label' => false,
                                        'div' => false,
                                        'placeholder' => 'Mobile',
                                        'type' => 'text'
                                        ));
                        ?>
                    </div>
                    	<div class="form-group">
                        <div class="clearfix">                               
                            <div class="pull-right">
                                <?php
                                    echo $this->Form->button(__('Close'), array(
                                                'class' => 'btn btn-default',
                                                'data-dismiss' => 'modal'
                                                ));
                                    echo $this->Form->button(__('Save'), array(
                                                'class' => 'btn btn-primary btn-grad btn-left',
                                                'type' => 'submit'
                                                ));
                                ?>
                            </div>
                        </div> 
                    </div>     
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
