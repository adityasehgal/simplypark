<?php
    echo $this->element('backend/pagination_options');
?>
<table class="table table-bordered table-striped table-bordered table-hover new-addministartor-table">
    <thead>
        <tr>
            <th>
                <?php echo $this->Paginator->sort('UserProfile.first_name', 'First Name', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('UserProfile.last_name', 'Last Name', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('User.username', 'Username', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('User.email', 'Email', array('class' => 'tableinherit')); ?>
            </th>
            <th><?php echo __('Mobile'); ?></th>
            <th><?php echo __('Status'); ?></th>
            <th><?php echo __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(!empty($subAdmins)) { 
                foreach ($subAdmins as $showSubAdmins) {
        ?>
                    <tr>
                        <td>
                            <?php echo $showSubAdmins['UserProfile']['first_name']; ?>
                        </td>
                        <td>
                            <?php echo $showSubAdmins['UserProfile']['last_name']; ?>
                        </td>
                        <td>
                            <?php echo $showSubAdmins['User']['username']; ?>
                        </td>
                        <td>
                            <?php echo $showSubAdmins['User']['email']; ?>
                        </td>
                        <td>
                            <?php echo ($showSubAdmins['UserProfile']['mobile']) ? $showSubAdmins['UserProfile']['mobile'] : 'N/A'; ?>
                        </td>
                        <td>
                        	<?php if ($showSubAdmins['User']['is_activated'] == 0) { ?>
                                <a class="btn btn-danger btn-xs btn-grad" href="#"><?php echo __('Inactive'); ?></a>
                            <?php } else { ?>
                            	<a class="btn btn-success btn-xs btn-grad" href="#"><?php echo __('Active'); ?></a>
                            <?php } ?>
                        </td>
                        <td id="inline-popups">
                            <?php
								$activeInactiveAttr = $this->Admin->activeInactiveIconValues($showSubAdmins['User']['is_activated'], $showSubAdmins['User']['is_deleted']);
								echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-'.$activeInactiveAttr['Class'].'"></i>',
                                        $activeInactiveAttr['PopupID'],
                                        array(
                                            'class' => 'btn btn-ok activeinactivemodalbutton magniPopup',
                                            'title' => $activeInactiveAttr['Title'],
                                            'data-effect' => 'mfp-zoom-in',
                                            'escape' => false,
                                            'data-url' => Configure::read('ROOTURL').'admin/admins/changeRecordStatus/'.base64_encode($showSubAdmins['User']['id']).'/User'
                                        )
                                    );
                            ?>
                            <?php
                                echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-pencil"></i>',
                                        '#',
                                        array(
                                            'class' => 'btn btn-green edit_profile_data',
                                            'data-effect' => 'mfp-zoom-in',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#editProfileModal',
                                            'title' => 'Edit',
                                            'data-url' => Configure::read('ROOTURL').'admin/admins/ajaxGetUserData/'.base64_encode($showSubAdmins['User']['id']).'.json',
                                            'escape' => false
                                        )
                                    );
                            ?>
                            <?php
                                $deleteRestoreAttr = $this->Admin->deleteRestoreIconValues($showSubAdmins['User']['is_deleted']);
                                echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-'.$deleteRestoreAttr['Class'].'"></i>',
                                        $deleteRestoreAttr['PopupID'],
                                        array(
                                            'class' => 'btn btn-red deleteModalButton magniPopup',
                                            'title' => $deleteRestoreAttr['Title'],
                                            'data-effect' => 'mfp-zoom-in',
                                            'data-url' => Configure::read('ROOTURL').'admin/admins/delete/'.base64_encode($showSubAdmins['User']['id']).'/User',
                                            'escape' => false
                                        )
                                    );
                            ?>
                        </td>
                    </tr>
        <?php } } else { ?>
            <tr>
                <td colspan="8">
                    <?php echo __('No Record Found'); ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php
    echo $this->element('backend/pagination');
    echo $this->Js->writeBuffer();
?>