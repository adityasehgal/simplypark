<div class="modal fade" id="editProfileModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class="button b-close class="close" data-dismiss="modal" aria-label="Close"">
                     <span>X</span>
                </span>
                <h4 class="modal-title" id="myModalLabel">Edit Profile</h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create('User', array(
                                'url' => '/admin/admins/edit_profile',
                                'method' => 'post',
                                'id' => 'edit-admin-profile'
                                //'novalidate' => true
                                ));
                ?>
                    <div id="profileDataContainer">
                    
                    </div>
                    <div class="form-group">
                        <div class="clearfix">                               
                            <div class="pull-right">
                                <?php
                                    echo $this->Form->button('Close', array(
                                                'class' => 'btn btn-default',
                                                'data-dismiss' => 'modal'
                                                ));
                                    echo $this->Form->button('Save', array(
                                                'class' => 'btn btn-primary btn-grad btn-left',
                                                'type' => 'submit'
                                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/html" id="editProfileData">
	<input type='hidden' name='data[User][id]' value="<%= data.User.id %>" />
	<input type='hidden' name='data[UserProfile][id]' value="<%= data.UserProfile.id %>" />
	<div class="form-group">
		<label class="control-label">First Name<span>*</span></label>
    	<input type='text' class='form-control' placeholder='First Name' value="<%= data.UserProfile.first_name %>" name='data[UserProfile][first_name]' />
	</div>
	<div class="form-group">
		<label class="control-label">Last Name<span>*</span></label>
    	<input type='text' class='form-control' placeholder='Last Name' value="<%= data.UserProfile.last_name %>" name='data[UserProfile][last_name]' />
	</div>
	<div class="form-group">
		<label class="control-label">Email<span>*</span></label>
    	<input type='text' class='form-control' placeholder='Email' value="<%= data.User.email %>" name='data[User][email]' />
	</div>
	<div class="form-group">
		<label class="control-label">Mobile<span>*</span></label>
    	<input type='text' class='form-control' placeholder='Mobile' value="<%= data.UserProfile.mobile %>" name='data[UserProfile][mobile]' />
	</div>
</script>