<?php
    echo $this->element('backend/pagination_options');
?>
<table class="table table-bordered table-striped table-bordered table-hover new-addministartor-table">
    <thead>
        <tr>
            <th>
                <?php echo $this->Paginator->sort('UserProfile.first_name', 'First Name', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('UserProfile.last_name', 'Last Name', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('User.email', 'Email', array('class' => 'tableinherit')); ?>
            </th>
            <th><?php echo __('Network Type'); ?></th>
            <th><?php echo __('Status'); ?></th>
            <th><?php echo __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(!empty($usersDatas)) { 
                foreach ($usersDatas as $usersData) {
        ?>
                    <tr>
                        <td>
                            <?php echo $usersData['UserProfile']['first_name']; ?>
                        </td>
                        <td>
                            <?php echo $usersData['UserProfile']['last_name']; ?>
                        </td>
                        <td>
                            <?php echo $usersData['User']['email']; ?>
                        </td>
                        <td>
                            <?php 
                                if($usersData['User']['social_network_user'] == 1) {
                                 echo '<span class="icon-facebook-sign icon-large"></span>';
                                    //echo 'Facebook Signup';
                            ?>
                            <?php
                                } else {
                                    echo '<span class="website-icon"></span>';
                                }
                            ?>
                        </td>
                        <td>
                        	<?php if ($usersData['User']['is_activated'] == 0) { ?>
                                <a class="btn btn-danger btn-xs btn-grad" href="#"><?php echo __('Inactive'); ?></a>
                            <?php } else { ?>
                            	<a class="btn btn-success btn-xs btn-grad" href="#"><?php echo __('Active'); ?></a>
                            <?php } ?>
                        </td>
                        <td id="inline-popups">
                            <?php
								$activeInactiveAttr = $this->Admin->activeInactiveIconValues($usersData['User']['is_activated'], $usersData['User']['is_deleted']);
								echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-'.$activeInactiveAttr['Class'].'"></i>',
                                        $activeInactiveAttr['PopupID'],
                                        array(
                                            'class' => 'btn btn-ok activeinactivemodalbutton magniPopup',
                                            'title' => $activeInactiveAttr['Title'],
                                            'data-effect' => 'mfp-zoom-in',
                                            'escape' => false,
                                            'data-url' => Configure::read('ROOTURL').'admin/admins/changeRecordStatus/'.base64_encode($usersData['User']['id']).'/User'
                                        )
                                    );
                            ?>
                            <?php
                                echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-search"></i>',
                                        '#',
                                        array(
                                            'class' => 'btn btn-green view_user_data',
                                            'data-effect' => 'mfp-zoom-in',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#viewUserModal',
                                            'title' => 'View',
                                            'data-url' => Configure::read('ROOTURL').'admin/admins/ajaxGetFrontUserData/'.base64_encode($usersData['User']['id']).'.json',
                                            'escape' => false
                                        )
                                    );
                            ?>
                            <?php
                                $deleteRestoreAttr = $this->Admin->deleteRestoreIconValues($usersData['User']['is_deleted']);
                                echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-'.$deleteRestoreAttr['Class'].'"></i>',
                                        $deleteRestoreAttr['PopupID'],
                                        array(
                                            'class' => 'btn btn-red deleteModalButton magniPopup',
                                            'title' => $deleteRestoreAttr['Title'],
                                            'data-effect' => 'mfp-zoom-in',
                                            'data-url' => Configure::read('ROOTURL').'admin/admins/delete/'.base64_encode($usersData['User']['id']).'/User',
                                            'escape' => false
                                        )
                                    );
                            ?>
                        </td>
                    </tr>
        <?php } } else { ?>
            <tr>
                <td colspan="6">
                    <?php echo __('No Record Found'); ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php
    echo $this->element('backend/pagination');
    echo $this->Js->writeBuffer();
?>