<div class="modal fade" id="viewUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog user-view-popup">
        <div class="modal-content">
            <div class="modal-header">
                <span class="button b-close class="close" data-dismiss="modal" aria-label="Close"">
                     <span>X</span>
                </span>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('View/Edit User Details'); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create('Admins', array(
                                'action' => 'saveDetails',
                                'method' => 'post',
                                'admin' => true,
                                'id' => 'view-user',
                                'enctype' => "multipart/form-data"
                                ));
                ?>
                    <div id="userDataContainer">
                    </div>
                    <div class="form-group">
                        <div class="clearfix">                               
                            <div class="pull-right">
                                <?php
                                     echo $this->Form->button('Save', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-primary',
                                                
                                                ));?>
                                <?php
                                    echo $this->Form->button('Close', array(
                                                'class' => 'btn btn-default',
                                                'data-dismiss' => 'modal'
                                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/html" id="viewUserField">
<div class="form-group">
        <div class="col-xs-12 form-group clearfix">
            <div class="col-xs-4">
                <label class="control-label">First Name</label>
            </div>

            <div class="col-xs-8 clearfix">
            <input type="hidden"  class="form-control"  name="data[UserProfile][id]" value="<%= data.UserProfile.id %>"> 
            <input type="hidden"  class="form-control"  name="data[UserProfile][user_id]" value="<%= data.UserProfile.user_id %>"> 
                <%= data.UserProfile.first_name %>
           </div>
        </div>
        <div class="col-xs-12 form-group clearfix">
            <div class="col-xs-4">
                <label class="control-label">Last Name</label>
            </div>

            <div class="col-xs-8 clearfix">
                <%= data.UserProfile.last_name %>
           </div>
        </div>
        <div class="col-xs-12 form-group clearfix">
            <div class="col-xs-4">
                <label class="control-label">UserName</label>
            </div>

            <div class="col-xs-8 clearfix">
                <%= data.User.username %>
           </div>
        </div>
        <div class="col-xs-12 form-group clearfix">
            <div class="col-xs-4">
                <label class="control-label">Billing Addresses</label>
            </div>

            <div class="col-xs-8 form-group clearfix">

                <% if(_.isEmpty(data.UserAddress)) { %>
                    No Billing Address Added
                <% } else { %>
                    <% _.each(data.UserAddress, function(address) { %>
                         <input type="text"  class="form-control user-details"  required="required"  name="data[UserAddress][address][]" value="<%= address.flat_apartment_number %>,<%= address.address %>,<%= address.City.name %>,<%= address.State.name %>,<%= address.pincode %>">
                    <% }); %>
                <% } %>
           </div>
        </div>
        <div class="col-xs-12 form-group clearfix">
            <div class="col-xs-4">
                <label class="control-label">Email</label>
            </div>

            <div class="col-xs-8 clearfix">
                <%= data.User.email %>
           </div>
        </div>
        <div class="col-xs-12 form-group clearfix">
            <div class="col-xs-4">
                <label class="control-label">Profile Pic</label>
            </div>

            <div class="col-xs-8 clearfix">
                 <%
                    if (data.UserProfile.profile_pic != null) {
                        var imgPath = base_url+data.UserProfile.profile_pic;
                        if (fileExists(imgPath)) {
                %>
                            <img class="img-responsive" src=<%= imgPath %> />
                             <a href=<%= base_url+'admin/Admins/removeUserImage/'+data.UserProfile.user_id %>    class="btn btn-sm btn-danger">Remove</a>
                            
                <%
                        } else {
                %>
                            <img class="img-responsive" src=<%= base_url+'img/Not_available.jpg' %> />
                <%
                        }
                    } else {
                %>
                        <img class="img-responsive" src=<%= base_url+'img/Not_available.jpg' %> />
                <% } %>
           </div> 
        </div>
        <div class="col-xs-12 form-group clearfix">
            <div class="col-xs-4">
                <label class="control-label">Update Profile Pic</label>
            </div>

            <div class="col-xs-8 clearfix">
                 <input type="file"  class="form-control" required="required"  name="data[UserProfile][profile_pic]" >
           </div>
        </div>
  
        <div class="col-xs-12 form-group clearfix">
            <div class="col-xs-4">
                <label class="control-label">Mobile</label>
            </div>

            <div class="col-xs-8 clearfix">
                <input type="text"  class="form-control" required="required"  name="data[UserProfile][mobile]" value="<%= data.UserProfile.mobile %>"> 
           </div>
        </div>
        <div class="col-xs-12 form-group clearfix">
            <div class="col-xs-4">
                <label class="control-label">Company</label>
            </div>

            <div class="col-xs-8 clearfix">
                <%= data.UserProfile.company %>
           </div>
        </div>
        <div class="col-xs-12 form-group clearfix">
            <div class="col-xs-4">
                 <label class="control-label">Cars</label>
                
               
            </div>

            <div class="col-xs-8 clearfix">
               <% _.each(data.UserCar, function(car) { %>
                     <input type="text"  class="form-group"  required="required"  name="data[UserCar][registeration_number][]" value=" <%= car.registeration_number %>">
            <% }); %>
           </div>
        </div>
        <div class="col-xs-12 form-group clearfix">
            <div class="col-xs-4">
                <label class="control-label">Tan Number</label>
            </div>

            <div class="col-xs-8 clearfix">
                 <input type="text"  class="form-control" required="required" name="data[UserProfile][tan_number]" value=" <%= data.UserProfile.tan_number %>"> 
               
           </div>
        </div>
        <div class="col-xs-12 form-group clearfix">
            <div class="col-xs-4">
                <label class="control-label">Withdrawl Sources</label>
            </div>

            <div class="col-xs-8 clearfix">

                 <% if(_.isEmpty(data.WithdrawalSources)) { %>
                    No Withdrawal Source Added
                <% } else { %>
                    <% _.each(data.WithdrawalSources, function(WithdrawlSource) { %>
                        <ol>  
                            <li><strong>Account Number</strong> : <%= WithdrawlSource.account_number %></li>
                            <li><strong>Branch Code</strong> :  <%= WithdrawlSource.branch_code %></li>
                            <li><strong>IfSC Code</strong> : <%= WithdrawlSource.ifsc_code %></li>
                            <li><strong>SWIFT Code</strong> : <%= WithdrawlSource.swift_code %></li>
                            <li><strong>Postal Code</strong> : <%= WithdrawlSource.postal_code %></li>
                          

                        </ol>
                    <% }); %>
               <% } %>
           </div>
        </div>
        <div class="col-xs-12 form-group clearfix">
            <div class="col-xs-4">
                <label class="control-label">Space Id</label>
            </div>

            <div class="col-xs-8 clearfix">
                <% _.each(data.Space, function(space) { %>
                    <% if(space.is_deleted != 1) { %> 
                        <ul>  
                            <li><a href=<%= base_url + 'admin/spaces/?search=' + data.User.email %>><%= space.name %></a></li>
                        </ul>
                    <% } %>
                    
                <% }); %>
               
           </div>
        </div>

    </div>
</script>
<?php
        echo $this->Html->script(array(
            'backend/Space/index'
            ));
?>