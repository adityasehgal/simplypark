<div class="modal fade" id="disApproveSpace" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class="button b-close class="close" data-dismiss="modal" aria-label="Close"">
                     <span>X</span>
                </span>
                <h4 class="modal-title" id="myModalLabel">Disapprove Reason</h4>
            </div>
            <?php echo $this->Form->create('SpaceDisapprovalReason', array(
                            'url' => array(
                                        'controller' => 'spaces',
                                        'action' => 'disApproveSpace',
                                        'prefix' => 'admin'
                                    ),
                            'method' => 'post',
                            'id' => 'disapprove-space'
                    ));
                    echo $this->Form->hidden('space_id', array('id' => 'spaceID'));
                    echo $this->Form->hidden('user_id', array('id' => 'userID'));
            ?>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label"><?php echo __('Reason'); ?><span>*</span></label>
                        <?php
                            echo $this->Form->input(
                                    'reason',
                                    array(
                                            'type' => 'textarea',
                                            'class' => 'form-control',
                                            'rows' => 3,
                                            'label' => false,
                                            'div' => false,
                                            'maxlength' => 1000,
                                            'onkeyup' => 'countChar(this)'
                                        )
                                );
                        ?>
                        <div class="help-info pull-right">
                           <small class="text-info" id="charNum">
                              
                           </small>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <?php
                        echo $this->Form->button('Close', array(
                                    'class' => 'btn btn-default',
                                    'data-dismiss' => 'modal'
                                    ));
                    ?>
                    <?php
                        echo $this->Form->button('Save', array(
                                    'class' => 'btn btn-primary btn-grad btn-left',
                                    'type' => 'submit'
                                    ));
                    ?>
                </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>