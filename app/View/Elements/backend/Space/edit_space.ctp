<div class="modal fade" id="editAdminSpaceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php echo $this->Form->create('Space', array(
                            'url' => '/admin/Spaces/saveUpdateAdminSpaceData',
                            'method' => 'post',
                            'id' => 'edit-space',
                            'class' => 'form-horizontal',
                            'enctype' => 'multipart/form-data'
                            //'novalidate' => true
                            ));
            ?>
            <div class="modal-header">
                <span class="button b-close class="close" data-dismiss="modal" aria-label="Close"">
                     <span>X</span>
                </span>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Edit Space'); ?></h4>
            </div>
            <div class="modal-body custom-form-setting">
                    <div id="spaceAdminDataContainer">
                    
                    </div>
                    <!-- <div class="form-group">
                        <div class="clearfix">                               
                            <div class="pull-right">
                                <?php
                                    echo $this->Form->button('Close', array(
                                                'class' => 'btn btn-default',
                                                'data-dismiss' => 'modal'
                                                ));
                                    echo $this->Form->button('Save', array(
                                                'class' => 'btn btn-primary btn-grad btn-left',
                                                'type' => 'submit'
                                                ));
                                ?>
                            </div>
                        </div>
                    </div> -->
            </div>
            <div class="modal-footer">
                <?php
                    echo $this->Form->button('Close', array(
                                'class' => 'btn btn-default',
                                'data-dismiss' => 'modal'
                                ));
                ?>
                <?php 
                    echo $this->Form->button('Save', array(
                                'class' => 'btn btn-primary btn-grad btn-left',
                                'type' => 'submit'
                                ));
                ?>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>

<script type="text/html" id="editAdminSpaceField">
    <input type='hidden' name='data[Space][id]' value="<%= data.Space.id %>" />
    <input type='hidden' name='data[Space][lat]' id='latitude' value="<%= data.Space.lat %>" />
    <input type='hidden' name='data[Space][lng]' id='longitude' value="<%= data.Space.lng %>" />
    
    <div class="form-group">
        <label class="control-label col-sm-4">Space Name</label>
        <div class="col-sm-7">
            <input type='text' class='form-control' placeholder='Enter Space name' value="<%= data.Space.name %>" name='data[Space][name]' />
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">Space Address</label>
        <div class="col-sm-7">
            <%= data.Space.flat_apartment_number+' '+data.Space.address1+' '+data.Space.address2+' '+data.City.name+' '+data.State.name+' '+data.Space.post_code %>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">Space Picture</label>
        <div class="col-sm-7">
                 <%
                    if (data.Space.place_pic != null) {
                        var imgPath = base_url+'files/SpacePic/place_pic.'+data.Space.place_pic;
                        if (fileExists(imgPath)) {
                %>
                            <img class="img-responsive" src=<%= base_url+'files/SpacePic/place_pic.'+data.Space.place_pic %> />
                            <a href=<%= base_url+'admin/Admins/removeSpaceImage/'+data.Space.id %> class="btn btn-sm btn-danger">Remove Image</a>
                <%
                        } else {
                %>
                            <img class="img-responsive" src=<%= base_url+'img/Not_available.jpg' %> />
                <%
                        }
                    } else {
                %>
                        <img class="img-responsive" src=<%= base_url+'img/Not_available.jpg' %> />
                <% } %>
        </div>
    </div>
    
    <div class="form-group">
        <label class="control-label col-sm-4">Update Space Picture</label>
        <div class="col-sm-7">
            <input type='file' name="data[Space][place_pic]" />
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-4">Space Description</label>
        <div class="col-sm-7">
            <textarea class='form-control' placeholder='Enter Space description' name='data[Space][description]'><%= data.Space.description %></textarea> 
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">Space Search Tags</label>
        <div class="col-sm-7">
            <input type='text' class='form-control' placeholder='Enter Space Search Tags' value="<%= data.Space.search_tags %>" name='data[Space][search_tags]' />
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">Space Search String</label>
        <div class="col-sm-7">
            <input type='text' class='form-control' placeholder='Enter Space String' value="<%= data.Space.search_string %>" name='data[Space][search_string]' />
        </div>
    </div>
     <div class="form-group">
        <label class="control-label col-sm-4">Space Tower No.</label>
        <div class="col-sm-7">
            <input type="text" class='form-control' placeholder='Enter Space Tower Number' name='data[Space][tower_number]' value="<%= data.Space.tower_number %>">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-4">Space Admin Description</label>
        <div class="col-sm-7">
            <textarea class='form-control' placeholder='Enter Space Admin Description' name='data[Space][admin_description]'><%= data.Space.admin_description %></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">Enter Lat Long</label>
        <div class="col-sm-5">
            <input type='text' class='form-control custom-lat-long' placeholder='Enter lat long comma separated' />
        </div>
        <a href="#" class="btn btn-primary btn-grad location-on-map">Set</a>
    </div>
    <div class="map" id="location" style= "height:340px">
    </div>
</script>
