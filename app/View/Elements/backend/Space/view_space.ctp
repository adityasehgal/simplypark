<!-- Modal -->
<div class="modal fade location-details-popup" id="spaceInfoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="spaceTemplate">

            </div>
        </div>
    </div>
</div>
<script type="text/html" id="spaceInfoContainer">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title view-details-title" id="myModalLabel"><%= data.Space.name %></h4>
    </div>
    <div class="modal-body clearfix">
        <article class="clearfix">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-4">
                        <%
                            if (data.Space.place_pic != null) {
                                var imgPath = base_url+'files/SpacePic/place_pic.'+data.Space.place_pic;
                                if (fileExists(imgPath)) {
                        %>
                                    <img class="img-responsive" src=<%= base_url+'files/SpacePic/place_pic.'+data.Space.place_pic %> />
                                    
                        <%
                                } else {
                        %>
                                    <img class="img-responsive" src=<%= base_url+'img/Not_available.jpg' %> />
                        <%
                                }
                            } else {
                        %>
                                <img class="img-responsive" src=<%= base_url+'img/Not_available.jpg' %> />
                        <% } %>
                    </div>
                    <div class="col-xs-8 quick-park">
                        <p>
                        <%= data.Space.description %>
                        </p>
                    </div>
                </div>
            </div>
        </article>

        <article class="clearfix">
            <div class="col-xs-12">

                <table class="table remove-bg">
                    <thead>
                        <tr>
                            <th>Hourly</th>
                            <th>Daily</th>
                            <th>Weekly</th>
                            <th>Monthly</th>
                            <th>Yearly</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <% if (data.SpacePricing[0].hourly != null) { %>
                            <td><i class="fa icon-inr"></i><%= data.SpacePricing[0].hourly %></td>
                            <% } else if (data.Space.tier_pricing_support > 0) { %>
                                <td><%= data.Space.tier_formatted_desc %></td>
                            <% } else { %>
                            <td><i class="fa"></i>N/A</td>
                            <% } if (data.SpacePricing[0].daily != null) { %>
                                <td><i class="fa icon-inr"></i><%= data.SpacePricing[0].daily %></td>
                            <% } else { %>
                                <td><i class="fa"></i>N/A</td>
                            <% } if (data.SpacePricing[0].weekely != null) { %>
                                <td><i class="fa icon-inr"></i><%= data.SpacePricing[0].weekely %></td>
                            <% } else { %>
                                <td><i class="fa"></i>N/A</td>
                            <% } if (data.SpacePricing[0].monthly != null) { %>
                                <td><i class="fa icon-inr"></i><%= data.SpacePricing[0].monthly %></td>
                            <% } else { %>
                                <td><i class="fa"></i>N/A</td>
                            <% } if (data.SpacePricing[0].yearly != null) { %>
                                <td><i class="fa icon-inr"></i><%= data.SpacePricing[0].yearly %></td>
                            <% } else { %>
                                <td><i class="fa"></i>N/A</td>
                            <% } %>
                        </tr>
                    </tbody>
                </table>
            </div>
        </article>

        <article class="clearfix">
            <div class="col-xs-12 ">
                <div class="row">
                    <div class="col-xs-6">
                        <address>
                            <span>ADDRESS</span><br>
                            <p><%= data.Space.flat_apartment_number %></p>
                            <p><%= data.Space.address1 %></p>
                            <p><%= data.Space.address2 %></p>
                            <p><%= data.Space.post_code %></p>
                        </address>
                    </div>

                    <div class="col-xs-6 extra-services">
                        <span>AMENTIES</span>
                        <%  if (data.SpaceFacility.length > 0) {
                                _.each(data.SpaceFacility, function(value, key){ %>
                                    <p><%= value.Facility.name %></p>
                        <%      
                                });
                            } else { %>
                                <p>N/A</p>
                        <% } %>
                    </div>
                </div>
            </div>
        </article>

        <article class="clearfix">
            <div class="col-xs-12 ">
                <div class="row">
                    <div class="col-xs-6">
                        <span>TOTAL AVAILABLE SPACES</span><br>
                        <% if (data.Space.number_slots != null) { %>
                            <%= data.Space.number_slots %>
                        <% } else { %>
                            <%= 0 %>
                        <% } %>
                    </div>
                    <div class="col-xs-6">
                        <span>AVAILABLE SMALL CAR SPACES</span><br>
                        <% if (data.SmallCarSpace.small_car_slot != null) { %>
                            <%= data.SmallCarSpace.small_car_slot %>
                        <% } else { %>
                            <%= 0 %>
                        <% } %>
                    </div>
                </div>
            </div>
        </article>
        <% if (data.CancellationPolicy.policy != null) { %>
            <article class="clearfix">
                <div class="col-xs-12 ">
                    <span>CANCELLATION  POLICY</span>
                    <p> <%= data.CancellationPolicy.policy %></p>
                </div>
            </article>
        <% } %>
        <% if (data.Space.sign_agreement) { %>
            <article class="clearfix">
                <div class="col-xs-12 ">
                    <span>POLICY AGREEMENT</span>
                    <p>For yearly booking, sign a parking rental agreement with parking user.</p>
                </div>
            </article>
        <% } %>
    </div>
</script>
