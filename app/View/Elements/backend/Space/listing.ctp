<?php
    echo $this->element('backend/pagination_options');
?>
<table class="table table-bordered new-addministartor-table">
    <thead>
        <tr>
            <th>
                <?php echo $this->Paginator->sort('Space.id', 'Id', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('User.UserProfile.first_name', 'Name', array('class' => 'tableinherit')); ?>
            </th>
            <!--<th>
                <?php echo $this->Paginator->sort('User.email', 'Email', array('class' => 'tableinherit')); ?>
            </th>-->
            <th>
                <?php echo $this->Paginator->sort('Space.name', 'Space Name', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('SpaceType.name', 'Space Type', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('PropertyType.name', 'Property Type', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo 'Tower & Park No.'; ?>
            </th>
            <th> 
                <?php echo $this->Paginator->sort('State.name', 'State', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('City.name', 'City', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('', 'Operating hours', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('SpaceTimeDay.from_date', 'From', array('class' => 'tableinherit')); ?>
            </th>
             <th>
                <?php echo $this->Paginator->sort('SpaceTimeDay.to_date', 'To', array('class' => 'tableinherit')); ?>
            </th>
            <th><?php echo __('Space Status'); ?></th>
            <th><?php echo __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(!empty($spaces)) { 
                //pr($spaces);die;
		    foreach ($spaces as $showSpaces) {
                    $tableRowClass = $this->Admin->manageSpaceApproveDisapproveColor(intval($showSpaces['Space']['is_approved']));
        ?>
                    <tr class="<?php echo $tableRowClass; ?>">
                        <td>
                            <?php echo $showSpaces['Space']['id']; ?>
                        </td>
                        <td>
                            <?php echo $showSpaces['UserProfile']['first_name'] . ' ' . $showSpaces['UserProfile']['last_name']; ?><br />
                            <?php echo "(".$showSpaces['User']['email'].")"; ?>
                        </td>
                        <td>
                            <?php
                                echo $this->Html->link(
                                        $showSpaces['Space']['name'],
                                        '#',
                                        array(
                                                'class' => 'space-details',
                                                'data-space-id' => urlencode(base64_encode($showSpaces['Space']['id'])),
                                                'escape' => false
                                            )
                                    );
                            ?>
                        </td>
                        <td>
                            <?php echo $showSpaces['SpaceType']['name'] != '' ? $showSpaces['SpaceType']['name'] : 'N/A'; ?>
                        </td>
                        <td>
                            <?php echo $showSpaces['PropertyType']['name'] != '' ? $showSpaces['PropertyType']['name'] : 'N/A'; ?>
                        </td>
                         <td>
                            <?php if ($showSpaces['Space']['property_type_id'] == Configure::read('gated_community')) { 
                                    echo !empty($showSpaces['Space']['tower_number']) ? '<strong>'.$showSpaces['Space']['tower_number'].'</strong>' : '--';
                                    foreach($showSpaces['SpacePark'] as $parkNumber){
                                       echo !empty($parkNumber['park_number']) ? '<br />('.$parkNumber['park_number'].')':'';
                                     }
                                } else {
                                    echo 'N/A';    
                                }
                            ?>
                        </td>
                        <td>
                            <?php echo $showSpaces['State']['name']; ?>
                        </td>
                        <td>
                            <?php echo $showSpaces['City']['name'] != '' ? $showSpaces['City']['name'] : 'N/A'; ?>
                        </td>
                        <td><?php
                            $operatingHours = '24 hours';
                            if (!$showSpaces['SpaceTimeDay']['open_all_hours']) {
                                $operatingHours = date('h:i a', strtotime($showSpaces['SpaceTimeDay']['from_date'])) . ' to ' . date('h:i a', strtotime($showSpaces['SpaceTimeDay']['to_date']));
                            }
                            echo $operatingHours;
                        ?></td>
                        <td>
                            <?php echo $showSpaces['SpaceTimeDay']['from_date']; ?>
                        </td>
                        <td>
                            <?php echo $showSpaces['SpaceTimeDay']['to_date']; ?>
                        </td>
                        <td>
                            <?php if ($showSpaces['Space']['is_activated'] == Configure::read('Bollean.False')) { ?>
                                <a class="btn btn-danger btn-xs btn-grad" href="#"><?php echo __('Inactive'); ?></a>
                            <?php } else { ?>
                                <a class="btn btn-success btn-xs btn-grad" href="#"><?php echo __('Active'); ?></a>
                            <?php } ?>
                        </td>
                        <td>
                            <?php if (intval($showSpaces['Space']['is_approved']) == Configure::read('SpaceStatus.NotApproved') || intval($showSpaces['Space']['is_approved']) == Configure::read('SpaceStatus.None')) {

                                    echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-ok"></i>',
                                        '#',
                                        array(
                                            'class' => 'btn btn-ok makeusertrusted magniPopup',
                                            'title' => 'Approve Space',
                                            'escape' => false,
                                            'data-space-id' => base64_encode($showSpaces['Space']['id']),
                                            'data-user-id' => base64_encode($showSpaces['User']['id']),
                                            'data-url' => Configure::read('ROOTURL').'admin/spaces/makeUserTrusted/'. base64_encode($showSpaces['User']['id'])

                                        )
                                    );
                                }
                            ?>
                            <?php
                                if (intval($showSpaces['Space']['is_approved']) == Configure::read('SpaceStatus.Approved') || intval($showSpaces['Space']['is_approved']) == Configure::read('SpaceStatus.None')) {

                                    echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-remove"></i>',
                                        '#',
                                        array(
                                            'class' => 'btn btn-ok disapprovalspace',
                                            'title' => 'Disapprove Space',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#disApproveSpace',
                                            'data-space-id' => $showSpaces['Space']['id'],
                                            'data-user-id' => $showSpaces['User']['id'],
                                            'escape' => false
                                        )
                                    );
                                }
                            ?>
                            <?php
                                echo $this->Html->link(
                                        '<i class="icon-briefcase"></i>',
                                        '#',
                                        array(
                                            'class' => 'space-charges-settings',
                                            'title' => 'Edit Tax/Charges',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#setChargesSpace',
                                            'data-space-id' => $showSpaces['Space']['id'],
                                            'data-user-id' => $showSpaces['User']['id'],
                                            'data-url' => Configure::read('ROOTURL').'admin/spaces/ajaxSpaceChargeData/'.base64_encode($showSpaces['Space']['id']).'.json',
                                            'escape' => false
                                        )
                                    );
                            ?>
                            <?php
                                echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-pencil"></i>',
                                        '#',
                                        array(
                                            'class' => 'btn btn-green edit_admin_space_data',
                                            'data-effect' => 'mfp-zoom-in',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#editAdminSpaceModal',
                                            'title' => 'Edit',
                                            'data-url' => Configure::read('ROOTURL').'admin/Spaces/ajaxGetAdminSpaceData/'.base64_encode($showSpaces['Space']['id']).'.json',
                                            'escape' => false
                                        )
                                    );
                            ?>
                        </td>
                    </tr>
        <?php } } else { ?>
            <tr>
                <td colspan="10">
                    <?php echo __('No Record Found'); ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php
    echo $this->element('backend/pagination');
    echo $this->Js->writeBuffer();
?>
