<?php
    echo $this->element('backend/pagination_options');
?>
<table class="table table-bordered table-striped table-bordered table-hover new-addministartor-table">
    <thead>
        <tr>
            <th>
                <?php echo $this->Paginator->sort('Event.event_name', 'Event', array('class' => 'tableinherit')); ?>
            </th>
            
            <th>
                <?php echo $this->Paginator->sort('Event.website', 'Website', array('class' => 'tableinherit')); ?>
            </th>
            
            <th>
                <?php echo $this->Paginator->sort('Event.tags', 'Tags', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('Event.website', 'Popular', array('class' => 'tableinherit')); ?>
            </th>
            
            <th><?php echo __('Status'); ?></th>
            <th><?php echo __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php $event_path = $events['path']; unset($events['path']);
            if(!empty($events)) { 
                foreach ($events as $showEvents) {
        ?>
                    <tr>
                        <td>
                            <?php echo $showEvents['Event']['event_name']; ?>
                        </td>
                        <td>
                            <?php echo $showEvents['Event']['website']; ?>
                        </td>
                        <td>
                            <?php echo $showEvents['Event']['tags']; ?>
                        </td>
                        <td>
                            <?php $checked = ''; $value = 0; 
                                if($showEvents['Event']['is_popular']) { $checked = 'checked'; $value = $showEvents['Event']['is_popular'];} 
                            ?>
                            <input class = "is-popular" type="checkbox" value = "<?php echo $value; ?>" name="data['Event']['is_popular']" <?php echo $checked; ?> 
                            data-eventId = "<?php echo $showEvents['Event']['id']; ?>" data-evenUrl = "<?php echo $event_path?>admin/Events/updateIsPopular">
                        </td>
                        <td>
                        	<?php if ($showEvents['Event']['is_activated'] == 0) { ?>
                                <a class="btn btn-danger btn-xs btn-grad" href="#"><?php echo __('Inactive'); ?></a>
                            <?php } else { ?>
                            	<a class="btn btn-success btn-xs btn-grad" href="#"><?php echo __('Active'); ?></a>
                            <?php } ?>
                        </td>
                        <td id="inline-popups">
                            <?php
                                if (!empty($showEvents['EventAddress'])) {
    								$activeInactiveAttr = $this->Admin->activeInactiveIconValues($showEvents['Event']['is_activated']);
    								echo $this->Html->link(
                                            '<i class="glyphicon glyphicon-'.$activeInactiveAttr['Class'].'"></i>',
                                            $activeInactiveAttr['PopupID'],
                                            array(
                                                'class' => 'btn btn-ok activeinactivemodalbutton magniPopup',
                                                'title' => $activeInactiveAttr['Title'],
                                                'data-effect' => 'mfp-zoom-in',
                                                'escape' => false,
                                                'data-url' => Configure::read('ROOTURL').'admin/admins/changeRecordStatus/'.base64_encode($showEvents['Event']['id']).'/Event'
                                            )
                                        );
                                }
                            ?>
                            <?php
                                echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-pencil"></i>',
                                        '/admin/events/editEvent/'.urlencode(base64_encode($showEvents['Event']['id'])),
                                        array(
                                            'class' => 'btn btn-green',
                                            'title' => 'Edit',
                                            'escape' => false
                                        )
                                    );
                            ?>
                            <?php
                                $deleteRestoreAttr = $this->Admin->deleteRestoreIconValues($showEvents['Event']['is_deleted']);
                                echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-'.$deleteRestoreAttr['Class'].'"></i>',
                                        $deleteRestoreAttr['PopupID'],
                                        array(
                                            'class' => 'btn btn-red deleteModalButton magniPopup',
                                            'title' => $deleteRestoreAttr['Title'],
                                            'data-effect' => 'mfp-zoom-in',
                                            'data-url' => Configure::read('ROOTURL').'admin/admins/delete/'.base64_encode($showEvents['Event']['id']).'/Event',
                                            'escape' => false
                                        )
                                    );
                            ?>
                            <?php
                                echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-search"></i>',
                                        '#',
                                        array(
                                            'class' => 'btn btn-green view_event_data',
                                            'data-effect' => 'mfp-zoom-in',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#viewEventModal',
                                            'title' => 'View',
                                            'data-url' => Configure::read('ROOTURL').'admin/events/ajaxGetEventData/'.base64_encode($showEvents['Event']['id']).'.json',
                                            'escape' => false
                                        )
                                    );
                            ?>
                            <?php
                                echo $this->Html->link(
                                        'Add/Edit Event Address',
                                        '#',
                                        array(
                                            'class' => 'btn btn-blue add_address_data edit-btn',
                                            'data-effect' => 'mfp-zoom-in',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#addEventAddressModal',
                                            'title' => 'Add/Edit Event Address',
                                            'data-url' => Configure::read('ROOTURL').'admin/events/addEventAddress/'.base64_encode($showEvents['Event']['id']).'.json',
                                            'escape' => false
                                        )
                                    );
                            ?>
                        </td>
                    </tr>
        <?php } } else { ?>
            <tr>
                <td colspan="8">
                    <?php echo __('No Record Found'); ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php
    echo $this->element('backend/pagination');
    echo $this->Js->writeBuffer();
?>
<script type="text/javascript">
    $('.is-popular').change(function() {
        var value = 0;
        var current_this = this;
        if($(this).prop('checked') == true){
            value = 1;
        }
        var event_id = $(this).attr("data-eventId");
        var evenUrl = $(this).attr("data-evenUrl");

        var request ={'event_id': event_id,'is_popular':value};
        $.ajax({
            url: evenUrl,
            type: 'POST',
            dataType: "json",
            data: {result: request},
            success:function( data ) {
                if(data.status == 'error'){
                    $(current_this).attr('checked', false);
                }
                alert(data.msg);
            }
        });
    });
</script>
<style type="text/css">
    .red {
        color: #FF0000;
    }
    .green {
        color: #009900;
    }
    .blink_me {
        -webkit-animation-name: blinker;
        -webkit-animation-duration: 1s;
        -webkit-animation-timing-function: linear;
        -webkit-animation-iteration-count: infinite;
        
        -moz-animation-name: blinker;
        -moz-animation-duration: 1s;
        -moz-animation-timing-function: linear;
        -moz-animation-iteration-count: infinite;
        
        animation-name: blinker;
        animation-duration: 1s;
        animation-timing-function: linear;
        animation-iteration-count: infinite;
    }

    @-moz-keyframes blinker {  
        0% { opacity: 1.0; }
        50% { opacity: 0.0; }
        100% { opacity: 1.0; }
    }

    @-webkit-keyframes blinker {  
        0% { opacity: 1.0; }
        50% { opacity: 0.0; }
        100% { opacity: 1.0; }
    }

    @keyframes blinker {  
        0% { opacity: 1.0; }
        50% { opacity: 0.0; }
        100% { opacity: 1.0; }
    }
</style>