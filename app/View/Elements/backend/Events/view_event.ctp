<div class="modal fade" id="viewEventModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog event-address-popup">
        <div class="modal-content">
            <div class="modal-header">
                <span class="button b-close class="close" data-dismiss="modal" aria-label="Close"">
                     <span>X</span>
                </span>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('View Event'); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create('EventType', array(
                                'url' => '/admin/events/updateEvent',
                                'method' => 'post',
                                'id' => 'view-event'
                                ));
                ?>
                    <div id="eventDataContainer">
                    </div>
                    <div class="form-group">
                        <div class="clearfix">                               
                            <div class="pull-right">
                                <?php
                                    echo $this->Form->button('Close', array(
                                                'class' => 'btn btn-default',
                                                'data-dismiss' => 'modal'
                                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/html" id="viewEventField">
	<div class="form-group">
        <div class="col-xs-12 clearfix">
            <div class="col-xs-4">
                <label class="control-label">Event Name</label>
            </div>

            <div class="col-xs-8 clearfix">
                <%= data.Event.event_name %>
           </div>
        </div>

        <div class="col-xs-12 clearfix">
            <div class="col-xs-4">
                <label class="control-label">Image</label>
            </div>

            <div class="col-xs-8">
                 <img src="<%= data.path+'event.'+data.Event.event_pic %>" alt="Event Image"> 
           </div>
        </div>

        <div class="col-xs-12 clearfix">
            <div class="col-xs-4">
                <label class="control-label">Website</label>
            </div>

            <div class="col-xs-8">
                <%= data.Event.website %>
            </div>
        </div>

        <div class="col-xs-12 clearfix">
            <div class="col-xs-4">
                <label class="control-label">Description</label>
            </div>

            <div class="col-xs-8">
                <%= data.Event.description %>
            </div>
        </div>
        
        <div class="col-xs-12 clearfix">
            <div class="col-xs-4">
                <label class="control-label">Tags</label>
            </div>

            <div class="col-xs-8">
                <%= data.Event.tags %>
            </div>
        </div>
        <div class="col-xs-12">
            <h4> Event Addresses : </h4>
        </div>
        <div class="col-xs-12">
            <table class="table table-bordered new-addministartor-table">
                <thead>
                    <tr>
                        <th>
                            <a class="tableinherit">Address</a>
                        </th>
                        <th>
                            <a class="tableinherit">State</a>
                        </th>
                        <th>
                            <a class="tableinherit">City</a>
                        </th>
                        <th>
                            <a class="tableinherit">Postal Code</a>
                        </th>
                        <th>
                            <a class="tableinherit">Latitude</a>
                        </th>
                        <th>
                            <a class="tableinherit">Longitude</a>
                        </th>
                       
                    </tr>
                </thead>
                <tbody>
                    <% if (_.isArray(data.EventAddress) && _.isEmpty(data.EventAddress)) { %>
                        <tr>
                            <td colspan="9">
                                <?php echo __('No Record Found'); ?>
                            </td>
                        </tr>
                    <% } else { %>
                        <% _.each(data.EventAddress, function(value, key){ %>
                                <tr>
                                    <td>
                                        <%= value.address %>
                                    </td>
                                    <td>
                                        <%= value.State.name %>
                                    </td>
                                    <td>
                                        <%= value.City.name %>
                                    </td>
                                    <td>
                                        <%= value.post_code %>
                                    </td>
                                    <td>
                                        <%= value.lat %>
                                    </td>
                                    <td>
                                        <%= value.long %>
                                    </td>
                                   
                                </tr>
                                <tr>
                                    <td colspan="7">
                                        <% _.each(value.EventTiming, function(eventValue, eventKey){ %>
                                            <label>Event Timings : </label> <%= eventValue.to %> - <%= eventValue.from %><br>
                                        <% }); %>
                                    </td>
                                </tr>
                        <% }); %>
                    <% } %>
                </tbody>
            </table>
        </div>
    </div>
</script>