<?php
	echo $this->Html->script('backend/Event/add_event_address', array('inline' => false));
?>
<div class="modal fade" id="addEventAddressModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog event-address-popup">
        <div class="modal-content">
            <div class="modal-header">
                <span class="button b-close class="close" data-dismiss="modal" aria-label="Close"">
                     <span>X</span>
                </span>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('View Address Details'); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create('EventType', array(
                                'url' => '/admin/events/addEventAddressData',
                                'method' => 'post',
                                'id' => 'add-event-address',
                                'class' => 'form-horizontal custom-form-setting'
                                ));
                ?>
                    <div class="eventAddressDataContainer">
                    </div>
                    <div class="form-group">
                        <div class="clearfix">                               
                            <div class="pull-right">
                                <?php
                                    echo $this->Form->button('Close', array(
                                                'class' => 'btn btn-default',
                                                'data-dismiss' => 'modal'
                                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/html" id="addEventAddress">

<div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Event Names</label>
        </div>

        <div class="col-xs-9 event-name">
        	 <%= data.event_name %>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Address<span class='red'>*</span></label>
        </div>

        <div class="col-xs-9">
        	<input class="form-control" id="address" type="text" name="data[EventAddress][address]">
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">State<span class='red'>*</span></label>
        </div>

        <div class="col-xs-9">
        	<select class='form-control' name="data[EventAddress][state_id]" id="state-list" data-url= "<%= data.path %>admin/events/getCities">
        		 <option value="">(Select State)</option>
	                <% _.each(data.state, function(value, key){ %>
		                <option value="<%= key %>"><%= value %></option>
	            	<% }); %>
        	</select>
        </div>
    </div>

    <div id = "list-city" class="form-group">
  	</div>

    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Postal Code<span class='red'>*</span></label>
        </div>

        <div class="col-xs-9">
        	<input class="form-control" id= "pin-code" type="text" name="data[EventAddress][post_code]">
         	<div class="pull-left">
	    		<a href="#" class='latlong'>Get lat long</a>
	    	</div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Latitude<span class='red'>*</span></label>
        </div>

        <div class="col-xs-9">
        	<input class="form-control" id="latitude" type="text" name="data[EventAddress][lat]">
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Longitude<span class='red'>*</span></label>
        </div>

        <div class="col-xs-9">
        	<input class="form-control" id="longitude" type="text" name="data[EventAddress][long]">
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Event Date And Timings</label>
        </div>
		<div class="col-xs-4">
			<div class='input-group date' id='start_date_0'>	
				<input type="text" class="form-control" name="data[EventAddress][time][0][from]" data-type='0'>
				<span class="input-group-addon date-pick">
					<span class="glyphicon glyphicon-calendar"></span>
				</span>
			</div>
		</div>
		<div class="col-xs-4 left-row-none">
			<div class='input-group date' id='end_date_1'>
				<input type="text" class="form-control"  name="data[EventAddress][time][0][to]" data-type='1'>
				<span class="input-group-addon date-pick">
					<span class="glyphicon glyphicon-calendar"></span>
				</span>
			</div>
		</div>
		<div class="col-xs-1 left-row-none">
			<input type="hidden" value="0" id="count">
			<button type="submit" id="time-slots" class="btn btn-primary" url="#">
				<span class="glyphicon glyphicon-plus"></span>
			</button>
		</div>
	</div>
	<div class="time-slots"></div>
    <input type="hidden" name="data[EventAddress][event_id]" value="<%= data.event_id %>">

    <div class="form-group">
        <div class="clearfix">                               
               <button class="btn btn-primary btn-grad btn-left save-event-address" type="submit">Save</button>
        </div>
    </div>
    <hr>
    <div class="form-group">
        <div class="col-xs-12">
            <h4>Already Added Addresses : </h4>
        </div>
        <div class="col-xs-12" id = "event-addresses-table">
	        <table class="table table-bordered new-addministartor-table">
			    <thead>
			        <tr>
			            <th>
			            	<a class="tableinherit">Address</a>
			            </th>
			            <th>
			            	<a class="tableinherit">State</a>
			            </th>
			            <th>
			            	<a class="tableinherit">City</a>
			            </th>
			            <th>
			            	<a class="tableinherit">Postal Code</a>
			            </th>
			            <th>
			            	<a class="tableinherit">Latitude</a>
			            </th>
			            <th>
			            	<a class="tableinherit">Longitude</a>
			            </th>
			            <th>
			            	<a class="tableinherit">Action</a>
			            </th>
			        </tr>
			    </thead>
			    <tbody>
			    	<% if (_.isArray(data.event_address_data) && _.isEmpty(data.event_address_data)) { %>
		    			<tr>
			                <td colspan="8">
			                    <label> No Record Found </label>
			                </td>
			            </tr>
		    		<% } else { %>
				    	<% _.each(data.event_address_data, function(value, key){ %>
						    	<tr>
						        	<td>
						        		<%= value.EventAddress.address %>
						            </td>
						            <td>
						        		<%= value.State.name %>
						            </td>
						            <td>
						        		<%= value.City.name %>
						            </td>
						            <td>
						        		<%= value.EventAddress.post_code %>
						            </td>
						            <td>
						        		<%= value.EventAddress.lat %>
						            </td>
						            <td>
						        		<%= value.EventAddress.long %>
						            </td>
						            <td rowspan="2">
						            	<a href ="#" data-url="<%= data.path %>admin/events/eventDetailsDelete/<%= value.EventAddress.id %>/<%= data.event_id %>" title="Delete" class="btn btn-red delete-address">
						            		<i class="glyphicon glyphicon-trash"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
							        <td colspan="6">
								        <% _.each(value.EventTimings, function(eventValue, eventKey){ %>
								        	<% if(eventValue.from == '' && eventValue.to == '') { %>
								        		<label> No Event Timings Exists </label>
								        	<% } else { %>
								        		<label>Event Timings : </label> <%= eventValue.to %> TO <%= eventValue.from %><br>
								        	<% } %>
								        <% }); %>
							        </td>
						        </tr>
		            	<% }); %>
			    	<% } %>
			    </tbody>
		    </table>
	    </div>
    </div>
</script>
<?php 
	echo $this->Html->css(
		array(
			'backend/bootstrap/css/datetimepicker-bootstrap3.css',
			// 'backend/bootstrap/css/bootstrap-datetimepicker.min'
		),
        null,array('inline' => false)
);

	echo $this->Html->script(
		array(
			'moment.min',
			'backend/bootstrap/bootstrap-datetimepicker-3.min',
			'backend/Event/latlong',
			//'backend/bootstrap/bootstrap-datetimepicker'
		),
		array('inline' => false)
	);
?>

<script type="text/html" id="addCity">
	<div class="col-xs-3">
		<label class="control-label">City</label>
	</div>
    <div class="col-xs-9">
        <select class='form-control' id = "city-list" name =data[EventAddress][city_id]>
	        <option value="">(Select City)</option>
	        <% _.each(data, function(value, key){ %>
	            <option value="<%= key %>"><%= value %></option>
	        <% }); %>
	        </select>
    </div>
</script>

<script type="text/html" id="addTimeSlots">
	<div class='added-time-slots form-group'>
		<div class="col-xs-3">
			<label class="control-label">Event Date And Timings</label>
		</div>
		<div class="col-xs-4 input-group date" id='start_date_<%= count %>'>
			<input type="text" class="form-control" type="text"  name="data[EventAddress][time][<%= count %>][from]" data-type='0'>
			<span class="input-group-addon date-pick">
	            <span class="glyphicon glyphicon-calendar date-pick"></span>
	        </span>
        </div>
        <div class="col-xs-4 input-group date left-row-none" id="end_date_<%= count + 1 %>">
			<input type="text" class="form-control"  name="data[EventAddress][time][<%= count %>][to]" data-type='1'>
			<span class="input-group-addon date-pick">
		        <span class="glyphicon glyphicon-calendar date-pick"></span>
		    </span>
	    </div>
    	<div class="col-xs-1 left-row-none">
		    <button class='btn btn-primary remove-button' type="button">
		    	<span class="glyphicon glyphicon-minus"></span>
		    </button>
		</div>
    </div>
</script>

<script type="text/html" id="deleteEventAddresses">
    <table class="table table-bordered new-addministartor-table">
	    <thead>
	        <tr>
	            <th>
	            	<a class="tableinherit">Address</a>
	            </th>
	            <th>
	            	<a class="tableinherit">State</a>
	            </th>
	            <th>
	            	<a class="tableinherit">City</a>
	            </th>
	            <th>
	            	<a class="tableinherit">Postal Code</a>
	            </th>
	            <th>
	            	<a class="tableinherit">Latitude</a>
	            </th>
	            <th>
	            	<a class="tableinherit">Longitude</a>
	            </th>	          
	            <th>
	            	<a class="tableinherit">Action</a>
	            </th>
	        </tr>
	    </thead>
	    <tbody>
	    	<% if (_.isArray(data.event_address_data) && _.isEmpty(data.event_address_data)) { %>
    			<tr>
	                <td colspan="8">
	                    <label> No Record Found </label>
	                </td>
	            </tr>
    		<% } else { %>
		    	<% _.each(data.event_address_data, function(value, key){ %>
				    	<tr>
				        	<td>
				        		<%= value.EventAddress.address %>
				            </td>
				            <td>
				        		<%= value.State.name %>
				            </td>
				            <td>
				        		<%= value.City.name %>
				            </td>
				            <td>
				        		<%= value.EventAddress.post_code %>
				            </td>
				            <td>
				        		<%= value.EventAddress.lat %>
				            </td>
				            <td>
				        		<%= value.EventAddress.long %>
				            </td>
				            <td rowspan="2">
				            	<a href ="#" data-url="<%= data.path %>admin/events/eventDetailsDelete/<%= value.EventAddress.id %>/<%= data.event_id %>" title="Delete" class="btn btn-red delete-address">
				            		<i class="glyphicon glyphicon-trash"></i>
				            	</a>
				            </td>
				        </tr>
				        <tr>
					        <td colspan="6">
						        <% _.each(value.EventTimings, function(eventValue, eventKey){ %>
						        	<% if(eventValue.from == '' && eventValue.to == '') { %>
						        		<label> No Event Timings Exists </label>
						        	<% } else { %>
						        		<label>Event Timings : </label> <%= eventValue.from %> TO <%= eventValue.to %><br>
						        	<% } %>
						        <% }); %>
					        </td>
				        </tr>
            	<% }); %>
	    	<% } %>
	    </tbody>
    </table>
</script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
