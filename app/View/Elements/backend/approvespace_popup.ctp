<div id="approveSpace" class="white-popup mfp-with-anim mfp-hide">
    <h3 class="text-center popup-heading"><?php echo __('Do you want to make customer trusted?'); ?></h3>
    <div CLASS="text-center"> 
        <button class="btn btn-default close-btn" data-dismiss="modal" type="button"><?php echo __('No'); ?></button>
        <?php
            echo $this->Html->link(
                    'Yes',
                    '#',
                    array(
                        'class' => 'btn btn-primary btn-grad usertrustedsure',
                        'escape' => false
                        )
                );
        ?>
    </div> 
</div>