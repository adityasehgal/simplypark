<div id="top">
    <nav class="navbar navbar-inverse navbar-fixed-top " style="padding-top: 5px;">
        <a data-original-title="Show/Hide Menu" data-placement="bottom" data-tooltip="tooltip" class="accordion-toggle btn btn-primary btn-sm visible-xs" data-toggle="collapse" href="#menu" id="menu-toggle">
            <i class="icon-align-justify"></i>
        </a>
        <header class="navbar-header">
            <?php
                echo $this->Html->link(
                        $this->Html->image('simplyparklogo_small.png', array('alt' => 'logo')),
                        '/admin/admins/',
                        array(
                            'class' => 'navbar-brand',
                            'escape' => false
                            )
                    );
            ?>
        </header>
        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle text-white" data-toggle="dropdown" href="#">
                    <span class="text-white admin-text">Welcome <?php echo $getAdminUserName['UserProfile']['first_name']; ?></span>
                    <i class="icon-user "></i>&nbsp; <i class="icon-chevron-down "></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li>
                        <?php
                            echo $this->Html->link(
                                    '<i class="icon-user"></i> Edit Profile',
                                    '#',
                                    array(
                                            'class' => 'edit_profile_data',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#editProfileModal',
                                            'data-url' => Configure::read('ROOTURL').'admin/admins/ajaxGetUserData/'.base64_encode($this->Session->read('Auth.User.id')).'.json',
                                            'escape' => false
                                        )
                                );
                        ?>
                    </li>
                    <li>
                        <?php
                            echo $this->Html->link(
                                    '<i class="icon-gear"></i> Change Password',
                                    '#',
                                    array(
                                            'data-toggle' => 'modal',
                                            'data-target' => '#changePasswordModal',
                                            'escape' => false
                                        )
                                );
                        ?>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <?php
                            echo $this->Html->link(
                                '<i class="icon-signout"></i> Logout',
                                '/admin/admins/logout',
                                array('escape' => FALSE)
                            );
                        ?>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</div>
<?php
    echo $this->element('backend/Admin/change_password'); 
    echo $this->element('backend/Admin/edit_profile'); 
?>
