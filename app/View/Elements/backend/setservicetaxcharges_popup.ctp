<div class="modal fade" id="setChargesSpace" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class="button b-close class="close" data-dismiss="modal" aria-label="Close"">
                     <span>X</span>
                </span>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Set Tax and Commission for Space.'); ?></h4>
            </div>
            <?php echo $this->Form->create('SpaceChargeSetting', array(
                        'url' => array(
                                    'controller' => 'spaces',
                                    'action' => 'setChargesSpace',
                                    'prefix' => 'admin'
                                ),
                        'method' => 'post',
                        'id' => 'set-space-charges'
                    ));
                    echo $this->Form->hidden('space_id', array('id' => 'space-id'));
                    echo $this->Form->hidden('user_id', array('id' => 'user-id'));
            ?>
                <div id="chargesSpaceDataContainer">
                    
                </div>
                
                <div class="modal-footer">
                    <?php
                        echo $this->Form->button('Close', array(
                                    'class' => 'btn btn-default',
                                    'data-dismiss' => 'modal'
                                    ));
                    ?>
                    <?php
                        echo $this->Form->button('Save', array(
                                    'class' => 'btn btn-primary btn-grad btn-left',
                                    'type' => 'submit'
                                    ));
                    ?>
                </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
<script type="text/html" id="editChargesSpaceField">
    <div class="modal-body">
        <div class="form-group">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="data[SpaceChargeSetting][charge_service_tax_to_owner]" <% if (data.charge_service_tax_to_owner == 1) { %> checked='checked' <% } %> />
                    Service Tax payable by Owner
                </label>
            </div>
        </div>
        <div class="form-group">
                
            <label>Commission Paid by: </label>
            <div class="form-inline">
                <input name="data[SpaceChargeSetting][commission_paid_by]" id="ServiceTaxChargesCommissionPaidBy1" type="radio" <% if (data.commission_paid_by == 1) { %> checked='checked' <% } %> value="1" />
                <label for="ServiceTaxChargesCommissionPaidBy1">Driver</label>
                <input name="data[SpaceChargeSetting][commission_paid_by]" id="ServiceTaxChargesCommissionPaidBy2" type="radio" <% if (data.commission_paid_by == 2) { %> checked='checked' <% } %> value="2" />
                <label for="ServiceTaxChargesCommissionPaidBy2">Owner</label>
            </div>
        </div>
        <div class="form-group">
            <label for="commission-value">Commission Rate in Perecntage</label>

                <input type="text" name="data[SpaceChargeSetting][commission_percentage]" placeholder = 'Add Commission'
                    class ='form-control' id = 'commission-value' value ="<%= (typeof(data.commission_percentage) !== 'undefined') ? data.commission_percentage : '' %>">

                <input type="checkbox" onclick="javascript:hideShow();" name="data[SpaceChargeSetting][is_one_time_charge]" id="oneTime" val = "1" <% if(data.is_one_time_charge) { %> checked <% } %>/> Charge commission as one off charge
                <div id="maxCommissionDiv">
                   <label for="max-commission-value">Cap Commission to </label>
                   <input type="text" name="data[SpaceChargeSetting][max_commission_value]" class ='form-control' id = 'max-commission-value' value ="<%=(typeof(data.max_commission_value) !== 'undefined')?data.max_commission_value:'' %>"/>
                </div> 
            <input type="hidden" name="data[SpaceChargeSetting][id]"
                value ="<%= (typeof(data.id) !== 'undefined') ? data.id : '' %>">
        </div>
    </div>
</script>
<script type="text/javascript">

function hideShow() {
    if (document.getElementById('oneTime').checked) {
        document.getElementById('maxCommissionDiv').style.display = 'block';
    } else {
        document.getElementById('maxCommissionDiv').style.display = 'none';
    }
}

</script>
