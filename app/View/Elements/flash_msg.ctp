<?php
	$resultClassName = array('success' => 'success', 'error' => 'danger');
	$iconClassName = array('success' => 'ok', 'error' => 'remove');
?>
<div class="alert alert-<?php echo @$resultClassName[$this->Session->read('Message.flash.params')]; ?> alert-dismissible fade in" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <?php echo $this->Session->flash(); ?>
</div>
