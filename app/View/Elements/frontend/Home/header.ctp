<?php echo $this->Html->script(array('frontend/misc'), array('inline' => false)); ?>
<div id="main-nav" class="navbar navbar-fixed-top">
    <div class="container">
    <?php //pr($this->params->params); ?>
        <div class="navbar-header">
            <?php
                echo $this->Html->link(
                        '<span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>',
                        '#',
                        array(
                                'class' => 'navbar-toggle',
                                'type' => 'button',
                                'data-toggle' => 'collapse',
                                'data-target' => '#site-nav',
                                'escape' => false
                            )
                    );
                echo $this->Html->link(
                        $this->Html->image(
                                'frontend/logo-simply-park.png',
                                array(
                                    'class' => 'site-logo img-responsive'
                                    )
                            ),
                            '/',
                            array(
                                'class' => 'navbar-brand scrollto',
                                'escape' => false
                        )
                );
            ?>
        </div>
        
        <div id="site-nav" class="navbar-collapse collapse">

            <?php if($this->params->params['controller'] == 'homes' && $this->params->params['action'] == 'index') { 
                } else {
            ?>
                <form class="navbar-form navbar-left search" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for parking places or events" id="search_new">
                        <span class="input-group-btn">
                            <a id="search-btn" class="btn btn-orange text-uppercase" ><?php echo __('Search'); ?></a>
                        </span>
                    </div><!-- /input-group-->
                </form> 
            <?php } ?>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <?php
                        echo $this->Html->link(
                            __('List Your Space'),
                            '/users/addSpace',
                            array(
                                    'escape' => false
                                )
                        );
                    ?>
                </li>
                <?php if (!$this->Session->read('Auth.User')) { ?>
                    <li class="login">
                        <?php
                            echo $this->Html->link(
                                __('Login'),
                                '#',
                                array(
                                        'data-toggle' => 'modal',
                                        'data-target' => '#loginModal',
                                        'escape' => false
                                    )
                            );
                        ?>
                    </li>
                    <li class="hidden-xs li-divider">
                        <?php
                            echo $this->Html->link(
                                '',
                                '#',
                                array(
                                        'escape' => false
                                    )
                            );
                        ?>
                        <a href="#">/</a>
                    </li>
                    <li>
                        <?php
                            echo $this->Html->link(
                                __('Create Account'),
                                '#',
                                array(
                                        'data-toggle' => 'modal',
                                        'data-target' => '#signupModal',
                                        'escape' => false
                                    )
                            );
                        ?>
                    </li>
                <?php } else { ?>
                    <li>
                        <?php
                            echo $this->Html->link(
                                __('My Account'),
                                '/dashboards/dashboard',
                                array(
                                        'escape' => false
                                    )
                            );
                        ?>
                    </li>
                <?php } ?>
                <li id="helpId" class="dropdown">
                    <?php
                        echo $this->Html->link(
                            __('HELP').'<span class="caret"></span>',
                            '#',
                            array(
	                          'data-toggle' => 'dropdown',
	                          'class' => 'dropdown-toggle',
	                          'role' => 'button',
	                          'aria-expanded' => 'false',
	                          'escape' => false
                                )
                           );
                    ?>
	  	    <ul class="dropdown-menu" role="menu">
			<li>
		 	<?php
			    echo $this->Html->link(
						__('What is SimplyPark Booking Pass?'),
						'https://www.simplypark.in/faq/index.php?action=artikel&cat=2&id=13&artlang=en',
						array(
							'escape' => false,
                                                        'target' => '_blank',
                       				     )
						   );
			?>
			</li>
			<li>
			<?php
			    echo $this->Html->link(
						__('How do I list a space?'),
					        'https://www.simplypark.in/faq/index.php?action=artikel&cat=4&id=17&artlang=en',
						array(
							'escape' => false,
                                                        'target' => '_blank'
						     )
						);
			?>
			</li>
			<li>
			    <?php
				echo $this->Html->link(
						__('More Questions?'),
						'https://www.simplypark.in/faq',
						array(
							'escape' => false,
                                                        'target' => '_blank'
			 			     )
				);
			     ?>
			</li>
			<li>
			   <?php
				echo $this->Html->link(
						__('Contact Us'),
						'https://www.simplypark.in/homes/contactUs',
						array(
							'escape' => false,
                                                        'target' => '_blank'
			 		             )
						 );
			    ?>
			</li>
                     </ul>
                </li>
            </ul>
        </div><!--End navbar-collapse -->
        
    </div><!--End container -->
    
</div><!--End main-nav -->
