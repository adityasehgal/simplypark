<div class="modal fade custom-modal" id="searchGatedSpace" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog gated-space">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3>Gated Space List  <span class="badge"></span></h3>
			</div>
			<div class="modal-body scroll">
				<?php
					echo $this->Form->create(
							'ListGatedSpace',
							array(
									'url' => 'javascript:void(0);',
									'method' => 'post',
									'id' => 'showGatedSpace'
								)
						);
				?>
					<div id="showGatedSpaceContainer">
	       			</div>
       		</div>
			<div class="modal-footer">
       			<div class="col-xs-3 pull-right text-right">
					<?php
						echo $this->Form->button('Close', array(
                                                'class' => 'btn btn-info text-uppercase',
                                                'data-dismiss' => 'modal'
                                                ));						
					?>
				</div> 
				
			</div>
		</div>
	</div>
</div>

<script type="text/html" id="showGatedSpaceField">
<div id="gatedSpacesDiv">
Sort Results by&nbsp;
<button class="sort" data-sort="tower-name">
    Tower 
</button>
<button class="sort" data-sort="monthly-price">
    Monthly Price 
</button>
<button class="sort" data-sort="yearly-price">
     Yearly Price 
</button> 
<table class="table gated-space-table"> 
	<thead>
		<tr>
			<th>Image</th>
			<th>Title</th>
			<th>Tower Number</th>
			<th>Monthly Price</th>
                        <th>Yearly Price</th>
		</tr>
	</thead>
	<tbody class="list">
		<% _.each(data.gatedSpace, function(value, key){ %>
			<tr>
				<td>
					<% if (value.Space.place_pic == null) { %>
						<img src = "<%= base_url+'img/no_image.png' %>" alt="parking images">
					<% }else{ %>
						<img src = "<%= base_url +'files/SpacePic/place_pic.'+value.Space.place_pic %>" alt="parking images">
					<% } %>
				</td>
				<td>
					<a href="<%= base_url+"spaces/spaceDetail/"+encodeURIComponent(Base64.encode(value.Space.id)) %>" title="<%= value.Space.name %>">
						<%= value.Space.name %>
					</a>
				</td>
				<td class="tower-name">
					<% if (value.Space.tower_number == null) { %>
					       N&#47;A	
					<% }else{ %>
						<%= value.Space.tower_number %>
					<% } %>
				</td>
				<td class="monthly-price">
					<% if (value.SpacePricing[0].monthly == null) { %>
					       N&#47;A	
					<% }else{ %>
						&#8377; <%= value.SpacePricing[0].monthly %>
					<% } %>
				</td>
				<td class="yearly-price">
					<% if (value.SpacePricing[0].yearly == null) { %>
						--
					<% }else{ %>
						&#8377; <%= value.SpacePricing[0].yearly %>
					<% } %>
				</td>
			</tr>
		<% }); %>
	</tbody>
</table>
</div>
<?php echo $this->Html->script('frontend/list.min', array('inline' => false)); ?>
<script type="text/javascript">
var options = {
  valueNames: [ 'tower-name', 'monthly-price', 'yearly-price' ]
};

var userList = new List('gatedSpacesDiv', options);


</script>
</script>
