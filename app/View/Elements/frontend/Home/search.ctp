<header id="home" class="jumbotron clearfix">
    
    <div id="sequence-theme" class="container">
    
        <div id="sequence" class="img-block">

            <!-- <img class="img-responsive" src="./img/SimplyParkBannerBackGround.jpg" alt="simple parking"> -->
            <div class="col-md-12 clearfix">
                <div class="col-xs-8 search-spot">
                    <div class="text-default"><h2><?php echo __('Find &amp; book your parking spot'); ?></h2></div>
                    <div class="input-group custom-input ">
                        <input type="text" class="form-control input-lg search-box" placeholder="Search for parking places or events" id="search_new">
                        <span class="input-group-btn">
                            <a id="search-btn" class="btn btn-orange btn-lg text-uppercase" ><?php echo __('Search'); ?></a>
                        </span>
                    </div><!-- /input-group -->
                </div>
	    </div>
            <!--
	    <div class="col-md-12">
             <div class="row">&nbsp;</div>
             <div class="row">
	         <div class="text-default col-md-offset-2 text-primary"><h2><u>Book Delhi Metro Monthly Passes</u></h2></div>
             </div>
	     <div class="row">
               <div class="col-md-4 col-md-offset-1">
                  <?php echo $this->Html->link($this->Html->image('ghitroni.png',
		                                                   array('class' => 'img-responsive',
								   'alt' => 'guru',
							   )),"https://www.simplypark.in/spaces/spaceDetail/NDY%3D",
							   array('class'=> 'btn zIndex','escape' => false));
                  ?> 
               </div>
               <div class="col-md-4">
                  <?php echo $this->Html->link($this->Html->image('guru.png',
		                                                  array('class' => 'img-responsive',
					                          'alt' => 'guru'
							  )),"https://www.simplypark.in/spaces/spaceDetail/OTk%3D",
							  array('class'=> 'btn zIndex','escape' => false));
                  ?> 
               </div>
             </div>
	    </div>
            -->

            <div class="col-md-12 parking-car">
                <?php
                    echo $this->Html->image(
                            'frontend/car-bg.png',
                            array(
                                    'class' => 'img-responsive',
                                    'alt' => 'parking car'
                                )
                        );
                ?>
            </div>

        </div>
        
    </div>
    
</header><!--End header -->
