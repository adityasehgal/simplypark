<div class="modal fade custom-modal" id="forgotModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div class="modal-padding-lg">
					<?php
						if (isset($this->params['url']['forgotpassword']) && $this->Session->check('Message.flash')) {
							echo $this->element('flash_msg');
						}
					?>
					<h3><?php echo __("Can't login in? Forget your password?"); ?></h3>
					<p><?php echo __("Enter your email address or username below and we'll send you password reset instructions."); ?></p>
					<?php
						echo $this->Form->create(
								'User',
								array(
										'url' => '/homes/forgotPassword',
										'method' => 'post',
										'id' => 'forgotPass'
									)
							);
					?>
						<div class="form-group">
							<?php
								echo $this->Form->input(
										'username',
										array(
												'class' => 'form-control input-lg',
												'placeholder' => __('Your E-mail or username')
											)
									);
							?>
						</div>
						<?php
							echo $this->Form->submit(
									__('Send Email'),
									array(
											'class' => 'btn btn-info btn-lg btn-block text-uppercase',
											'escape' => false
										)
								);
					echo $this->Form->end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	if (isset($this->params['url']['forgotpassword'])) {
?>
	<script>
		$(document).ready(function(){
			$("#forgotModal").modal("show");
		});
	</script>
<?php
	}
?>