<div class="modal fade custom-modal" id="searchTagEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div class="modal-padding-lg">
					<h3><?php echo __("Didn't get the desired parking?"); ?></h3>
					<p><?php echo __("Please provide us your email id and enter the name of the space and/or location that you desire. We will intimate you when we do find a parking at your desired space! See you soon!"); ?></p>
					<?php
						echo $this->Form->create(
								'Search',
								array(
										'url' => '/searches/sendTagsEmail',
										'method' => 'post',
										'id' => 'searchTags'
									)
							);
						if(!empty($getUserProfile)) {
							echo $this->Form->hidden('mobile', array('value' => $getUserProfile['UserProfile']['mobile']));
							echo $this->Form->hidden('name', array('value' => $getUserProfile['UserProfile']['first_name'].' '.$getUserProfile['UserProfile']['last_name']));
						}
							
					?>
						<div class="form-group">
							<?php
								echo $this->Form->input(
										'email',
										array(
												'class' => 'form-control input-lg',
												'placeholder' => __('Your E-mail'),
												'div' => false,
												'label' => false,
												'value' => $this->Session->check('Auth.User') ? $this->Session->read('Auth.User.email') : ''
											)
									);
							?>
						</div>
						<div class="form-group">
							<?php
								echo $this->Form->input(
										'keyword',
										array(
												'class' => 'form-control input-lg searched_keyword',
												'placeholder' => __('Enter place(s)'),
												'div' => false,
												'label' => false
											)
									);
							?>
						</div>
						<?php
							echo $this->Form->submit(
									__('Submit'),
									array(
											'class' => 'btn btn-info btn-lg btn-block text-uppercase',
											'escape' => false
										)
								);
					echo $this->Form->end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
