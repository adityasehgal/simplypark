<div class="modal fade custom-modal" id="signupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div class="modal-padding-lg">
					<?php
						if (isset($this->params['url']['errorsignup']) && $this->Session->check('Message.flash')) {
							echo $this->element('flash_msg');
						}
					?>
					<h3><?php echo __('Log in with your SimplyPark account'); ?></h3>
					<div class="form-group">
						<?php
							echo $this->Html->link(
									'<i class="fa fa-facebook-official fa-fw"></i>'.__('Sign up with facebook'),
									'/auth/facebook',
									array(
											'class' => 'btn btn-facebook btn-lg btn-block text-uppercase',
											'escape' => false,
										)
								);
						?>
					</div>
					<?php
						echo $this->Form->create(
								'User',
								array(
										'url' => '/homes/signup',
										'method' => 'post',
										'id' => 'signupForm',
										'novalidate' => true,
										'inputDefaults' => array(
												'div' => false,
												'label' => false
											)
									)
							);
					?>
						<div class="text-center divider-box">
							<hr>
							<span class="divider-or"><?php echo __('or'); ?></span>
						</div>
						<div class="form-group">
							<?php
								echo $this->Form->input(
										'email',
										array(
												'class' => 'form-control input-lg',
												'placeholder' => __('Enter your email')
											)
									);
							?>
						</div>
						<div class="form-group">
							<?php
								echo $this->Form->input(
										'UserProfile.first_name',
										array(
												'class' => 'form-control input-lg',
												'placeholder' => __('Enter your first name')
											)
									);
							?>
						</div>
						<div class="form-group">
							<?php
								echo $this->Form->input(
										'UserProfile.last_name',
										array(
												'class' => 'form-control input-lg',
												'placeholder' => __('Enter your last name')
											)
									);
							?>
						</div>
						<div class="form-group">
							<?php
								echo $this->Form->input(
										'username',
										array(
												'class' => 'form-control input-lg',
												'placeholder' => __('Enter your username')
											)
									);
							?>
						</div>
						<div class="form-group">
							<?php
								echo $this->Form->input(
										'UserProfile.mobile',
										array(
												'class' => 'form-control input-lg',
												'type' => 'text',
												'placeholder' => __('Enter your mobile number')
											)
									);
							?>
						</div>
						<div class="form-group">
							<?php
								echo $this->Form->input(
										'password',
										array(
												'class' => 'form-control input-lg',
												'type' => 'password',
												'placeholder' => __('Enter your password')
											)
									);
							?>
						</div>
						<?php
							echo $this->Form->submit(
									__('Sign up'),
									array(
										 'class' => 'btn btn-info btn-lg btn-block text-uppercase',
										 'escape' => false
										)
								);
						?>
					<?php echo $this->Form->end(); ?>
					<p class="text-center create-acc"><?php echo __('Already have a SimplyPark account?'); ?> <a href="#" data-toggle="modal" data-target="#loginModal" id="callLoginModal"><?php echo __('Click here') ?></a> <?php echo __('to login'); ?></p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	if (isset($this->params['url']['errorsignup'])) {
?>
	<script>
		$(document).ready(function(){
			$("#signupModal").modal("show");
		});
	</script>
<?php
	}
?>
