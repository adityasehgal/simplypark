<section id="our-partner" class="white-bg padding-top-bottom">
    <div class="container padding-top-bottom border-top">
        <h1 class="section-title"><?php echo __('Our Partners'); ?></h1>
        <p class="section-description">
            <?php echo __("We 've worked with many serious brands in the industry."); ?>
        </p>
        <div class="row our-partner">

            <ul class="list-inline text-center">
                <?php
                     $gridClass = count($getOurPartners);
                    foreach ($getOurPartners as $id => $logo) {
                ?>
                    <li>
                        <?php
                            echo $this->Html->image(
                                    '/' . Configure::read('PartnerLogoPath') .$logo,
                                    array(
                                            'class' => 'img-responsive',
                                            'alt' => 'apl'
                                        )
                                );
                        ?>
                    </li>
            <?php
                }
            ?>
            </ul>
        </div>              
    </div>
</section>