<div class="modal fade custom-modal" id="resetPasswordModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div class="modal-padding-lg">
					<?php
						if (isset($this->params['url']['resetpassword']) && $this->Session->check('Message.flash')) {
							echo $this->element('flash_msg');
						}
					?>
					<h3><?php echo __("Reset your password"); ?></h3>
					<?php
						echo $this->Form->create(
								'User',
								array(
										'url' => '/homes/resetPassword',
										'type' => 'post',
										'id' => 'reset-password'
									)
							);
						if (isset($this->params['url']['persistcode'])) {
						
							echo $this->Form->hidden(
									'persist_code',
									array(
											'value' => urldecode(base64_decode($this->params['url']['persistcode']))
										)
								);
						} else {

							echo $this->Form->hidden(
									'persist_code',
									array(
											'value' => ''
										)
							);
						}
					?>
						<div class="form-group">
							<?php
								echo $this->Form->input(
										'password',
										array(
												'class' => 'form-control input-lg',
												'placeholder' => __('Enter new password'),
												'id' => 'new-password',
                                        		'data-display' => 'mainPassword'
											)
									);
							?>
							<div class="left passwordStrength" id="mainPassword"></div>
						</div>
						<div class="form-group">
							<?php
								echo $this->Form->input(
										'confirm_password',
										array(
												'type' => 'password',
												'class' => 'form-control input-lg',
												'placeholder' => __('Enter confirm password')
											)
									);
							?>
						</div>
						<?php
							echo $this->Form->submit(
									__('save'),
									array(
											'class' => 'btn btn-info btn-lg btn-block text-uppercase',
											'escape' => false
										)
								);
					echo $this->Form->end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	if (isset($this->params['url']['resetpassword'])) {
?>
	<script>
		$(document).ready(function(){
			$("#resetPasswordModal").modal("show");
		});
	</script>
<?php
	}
?>