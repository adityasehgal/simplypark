<?php if(!empty($getDiscountCoupon) && isset($getDiscountCoupon)) { ?>
	<div class="alert alert-warning alert-dismissible fade in coupon-code navbar-fixed-bottom" role="alert">
	      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">×</span>
	      </button>
	      <?php echo $getDiscountCoupon['DiscountCoupon']['description']; ?>
	</div>
<?php } ?>