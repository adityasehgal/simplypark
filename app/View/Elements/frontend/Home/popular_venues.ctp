<section id="popular-venues" class="white-bg padding-top-bottom">
    <div class="container">
        <h1 class="section-title"><?php echo __('Popular venues location'); ?></h1>
        <p class="section-description">
            <?php echo __('Find parking for the most popular events in your city'); ?>
        </p>
        <div class="row popular-venues">
            <ul class="list-inline text-center">
                <?php
                    $gridClass = count($getPopularEvents);
                    foreach ($getPopularEvents as $showPopularEvents) {
                ?>
                    <li>
                        <?php
                            if (!empty($showPopularEvents['Event']['event_pic'])) {
                                if (file_exists(Configure::Read('EventImagePath').$showPopularEvents['Event']['event_pic'])) {
                                    echo $this->Html->link(
                                        $this->Html->image(
                                                '/' . Configure::read('EventImagePath') . $showPopularEvents['Event']['event_pic'],
                                                array(
                                                    'class' => 'img-responsive'
                                                    )
                                            ),
                                        '/events/eventDetail/' . urlencode(base64_encode($showPopularEvents['Event']['id'])),
                                        array(
                                            'escape' => false
                                        )
                                    );
                                } else {
                                    echo $this->Html->link(
                                        $this->Html->image('event-no-image.png',
                                            array(
                                                    'class' => 'img-responsive'
                                                )
                                        ),    
                                        '/events/eventDetail/' . urlencode(base64_encode($showPopularEvents['Event']['id'])),
                                        array(
                                            'escape' => false
                                        )   
                                    );
                                    
                                }
                            } else {
                                echo $this->Html->link(
                                    $this->Html->image('event-no-image.png',
                                        array(
                                                'class' => 'img-responsive pic-size'
                                            )
                                    ),    
                                    '/events/eventDetail/' . urlencode(base64_encode($showPopularEvents['Event']['id'])),
                                    array(
                                        'escape' => false
                                    )
                                );
                            }   
                            
                       

                        ?>
                        <div class="item text-center img-block">
                            <div class="box">
                                <h4><?php echo $showPopularEvents['Event']['event_name']; ?></h4>
                                <?php if($showPopularEvents['EventTiming']) { ?>
                                    <small><?php echo date('d M, Y', strtotime($showPopularEvents['EventTiming'][0]['from'])) ?></small>
                                <?php } ?>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</section>