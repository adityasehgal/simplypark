<section id="parking-place" class="parking padding-top-bottom">
    <div class="container">
        <h1 class="section-title"><?php echo __('Have your own parking space?'); ?></h1>
        <p class="section-description">
            <?php echo __('Learn how to make money from it.'); ?>
        </p>
        <p class="text-center">
            <?php
                echo $this->Html->link(
                            __('List your space'),
                            '/users/addSpace',
                            array(
                                    'class' => 'btn btn-custom scrollto text-uppercase',
                                    'escape' => false
                                )
                        );
            ?>
        </p>
    </div>
</section>
