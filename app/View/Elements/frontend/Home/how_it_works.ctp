<section id="how-it-work" class="white-bg">

    <div class="container padding-top-bottom border-bottom">
        
        <h1 class="section-title">How it Works</h1>
        <p class="section-description">
            SimplyPark lets you search, book, and pay for a guaranteed Parking Spot
        </p>
        
        <div class="row how-it-work">
        
            <div class="col-sm-4 item text-center">
        
                <div class="icon"><i class="fa fa-search-new"></i></div>
                <h3>FIND YOUR SPOT</h3>
                <p>Compare price &amp; amenities.</p>
                
            </div>
            
            <div class="col-sm-4 item text-center">
        
                <div class="icon"><i class="fa fa-space"></i></div>
                <h3>BOOK YOUR PARKING SPACE</h3>
                <p>Never get turned away again</p>
                
            </div>
            
            <div class="col-sm-4 item text-center">
        
                <div class="icon"><i class="fa fa-car-new"></i></div>
                <h3>PARK &amp; GO</h3>
                <p>Move on to what matters.</p>
                
            </div>
            
        </div>              
        
    </div>
    
</section>
