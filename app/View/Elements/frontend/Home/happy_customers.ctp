<section id="happy-customer" class="white-bg">

    <div class="container padding-top-bottom">
        
        <h1 class="section-title">Happy Customers</h1>
        <p class="section-description">What others are saying about us</p>
        
        <div class="row happy-customer">
        
            <div class="col-sm-4 item text-center">
        
                <div class="icon">
                    <?php
                        echo $this->Html->image(
                                'frontend/twitter-1.png',
                                array(
                                        'alt' => 'person'
                                    )
                            );
                    ?>
                </div>
                <h3>Adnan Alam<br>@ahalam</h3>
                <p>
                    Fantastic service. No idea finding parking was this easy. Keep it up @simplypark
                </p>
                
            </div>
            
            <div class="col-sm-4 item text-center">
        
                <div class="icon">
                    <?php
                        echo $this->Html->image(
                                'frontend/twitter-avatar-small.png',
                                array(
                                        'alt' => 'person'
                                    )
                            );
                    ?>
                </div>
                <h3>Kiran<br>@bahugunakiran</h3>
                <p>
                    Woha. Has anyone tried booking through simplypark. I need review people
                </p>
                
            </div>
            
            <div class="col-sm-4 item text-center">
        
                <div class="icon">
                    <?php
                        echo $this->Html->image(
                                'frontend/twitter-avatar-3.png',
                                array(
                                        'alt' => 'person'
                                    )
                            );
                    ?>
                </div>
                <h3>Sameer Hussaini<br>@sameer1010</h3>
                <p>
                    Booked a parking for my second car through simplypark. It was a breeze.
                </p>
                
            </div>
            
        </div>              
        
    </div>
    
</section>
