<div class="modal fade custom-modal" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div class="modal-padding-lg">
					<?php
						if (isset($this->params['url']['errorlogin']) && $this->Session->check('Message.flash')) {
							echo $this->element('flash_msg');
						}
					?>
					<h3><?php echo __('Log in with your SimplyPark account'); ?></h3>
					<div class="form-group">
						<?php
							echo $this->Html->link(
									'<i class="fa fa-facebook-official fa-fw"></i>'.__('login with facebook'),
									'/auth/facebook',
									array(
											'class' => 'btn btn-facebook btn-lg btn-block text-uppercase',
											'escape' => false,
										)
								);
						?>
					</div>
					<?php
						echo $this->Form->create(
								'User',
								array(
										'url' => '/homes/login',
										'method' => 'post',
										'id' => 'loginForm',
										'inputDefaults' => array(
												'div' => false,
												'label' => false
											)
									)
							);
							if (isset($this->params['url']['listSpaceRedirect']) && $this->params['url']['listSpaceRedirect'] == Configure::read('Bollean.True')) {
								echo $this->Form->hidden('listSpaceRedirect', array('value' => Configure::read('Bollean.True')));
							}
							
                                                       if (isset($this->params['url']['bulkBookingRedirect']) 
                                                           && $this->params['url']['bulkBookingRedirect'] == Configure::read('Bollean.True')) 
                                                       {
							   echo $this->Form->hidden('bulkBookingRedirect', 
                                                                                     array('value' => Configure::read('Bollean.True')));
							   echo $this->Form->hidden('spaceID', array('value' => $this->params['url']['spaceId']));
							}
							
                                                       if (isset($this->params['url']['isBookingApprovalRedirect']) && $this->params['url']['isBookingApprovalRedirect'] == Configure::read('Bollean.True')) {
								echo $this->Form->hidden('isBookingApprovalRedirect', array('value' => Configure::read('Bollean.True')));
							}
							if (isset($this->params['url']['errorlogin']) == Configure::read('Bollean.True') && isset($this->params['url']['stdate'])) {
									echo $this->Form->input(
										'User.booking_start_date',
										array(
												'type' => 'hidden',
												'value' => urldecode(base64_decode($this->params['url']['stdate']))
											)
									);
									echo $this->Form->input(
										'User.booking_end_date',
										array(
												'type' => 'hidden',
												'value' =>  urldecode(base64_decode($this->params['url']['edate']))
											)
									);
									echo $this->Form->input(
										'User.space_id',
										array(
												'type' => 'hidden',
												'value' =>  urldecode(base64_decode($this->params['url']['spaceId']))
											)
									);
									echo $this->Form->input(
										'User.space_park_id',
										array(
												'type' => 'hidden',
												'value' =>  urldecode(base64_decode($this->params['url']['space_park_id']))
											)
									);
							}

							
							// if (isset($this->params['url']['errorlogin']) && $this->Session->check('Message.flash')) {
							// 	echo $this->Form->input(
							// 			'username',
							// 			array(
							// 					'class' => 'form-control input-lg',
							// 					'placeholder' => __('username or email')
							// 				)
							// 		);
							// }
					?>
						<div class="text-center divider-box">
							<hr>
							<span class="divider-or"><?php echo __('or'); ?></span>
						</div>
						<div class="form-group">
							<?php
								echo $this->Form->input(
										'username',
										array(
												'class' => 'form-control input-lg',
												'placeholder' => __('username or email')
											)
									);
							?>
						</div>
						<div class="form-group">
							<?php
								echo $this->Form->input(
										'password',
										array(
												'class' => 'form-control input-lg',
												'placeholder' => __('Password')
											)
									);
							?>
						</div>
						<div class="form-group text-left">
							<?php
								echo $this->Form->checkbox(
										'keep_me_logged_in',
										array(
												'div' => false,
												'label' => false,
												'escape' => false
											)
									);
							?>
							<span><?php echo __('Keep me logged in'); ?></span>
						</div>
						<?php
							echo $this->Form->submit(
									__('log in'),
									array(
											'class' => 'btn btn-info btn-lg btn-block text-uppercase',
											'escape' => false
										)
								);
					echo $this->Form->end(); ?>
					<div class="form-group text-right forgot-pass">
						<?php
							echo $this->Html->link(
									__('Forgot your password?'),
									'#',
									array(
											'data-toggle' => 'modal',
											'data-target' => '#forgotModal',
											'id' => 'callForgotPass',
											'escape' => false
										)
								);
						?>
					</div>
					<p class="text-center create-acc"><?php echo __("Don't have a SimplyPark account?"); ?> <a href="#" data-toggle="modal" data-target="#signupModal" id="callSignupModal"><?php echo __('Click here'); ?></a> <?php echo __('to create one'); ?></p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	if (isset($this->params['url']['errorlogin'])) {
?>
	<script>
		$(document).ready(function(){
			$("#loginModal").modal("show");
		});
	</script>
<?php
	}
?>
