<div class="modal fade custom-modal del-modal" id="deleteConfirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class = "modal-padding-lg">
          <p class="margin-zero"><b>Are you sure you want to delete</b></p>
        </div>
      </div>
      <div class="modal-footer btn-center">
        <?php
            echo $this->Html->link('No',
            	Configure::read('ROOTURL').'WithdrawalSources/',
            	array(
                    'data-dismiss' => 'modal',
            		'class' => 'btn btn-default',
            	)
            );
        ?>
        <?php
            echo $this->Html->link('Yes',
            	'#',
            	array(
            		'class' => 'btn btn-info yes-confirm',
            	)
            );
        ?>
      </div>
    </div>
  </div>
</div>
