<?php
	if(!empty($eventDetail['EventAddress'])) {
		//pr($eventDetail);
		foreach ($eventDetail['EventAddress'] as $k => $v) {
			$stateName =  $v['State']['name'];
			$cityName =  $v['City']['name'];
			$eventAddress = $v['address'];
			foreach ($v['EventTiming'] as $key => $value) { ?>
				<div class="well gray-bg">
					<div class="row">
						<div class="col-xs-12 col-md-9">
							<div class="row">
								<div class="col-sm-4"> 
                                                                        <u>From</u>
									<h3 class="place-name">
										<?php echo date('dS M', strtotime($value['from']));?>
									</h3>
                                                                        <?php echo date('H:i', strtotime($value['from'])); ?> 
								</div>
                                                                 
								<div class="col-sm-8 place-address">
									<?php 
										
									$locationName = !empty($cityName) ? $cityName : $stateName;
										 echo $eventAddress . ' - ' . $locationName;
									?>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-3 text-right">
							<?php
								echo $this->Html->link(
									'Find Parking', 
									'/events/eventParkingLocations/'. urlencode(base64_encode($value['event_id'])) . '/'. urlencode(base64_encode($value['event_address_id'])) . '/'. urlencode(base64_encode($value['id'])),
									array(
											'class' => 'btn btn-info text-uppercase'
										)
									);
							?>
						</div>
					</div>
				</div>
			<?php } 
		}

	} else {?>
		<div class="well gray-bg">
		 				<div class="row text-center">
		 					 No Record Found.
		 				</div>
		 			</div> 	
	<?php } ?>
		

			

