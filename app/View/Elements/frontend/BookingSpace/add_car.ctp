<div class="modal fade custom-modal" id="addCarModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div class="modal-padding-lg">
					<h3><?php echo __("Add New Car"); ?></h3>
					<div class="help-info error-box text-danger hide">
						<?php echo __('Add car request not completed due to the following reasons:'); ?>
						<small id="show-car-error">
							
						</small>
					</div>
					<?php
						echo $this->Form->create(
								'UserCar',
								array(
										'url' => '/bookings/addCar',
										'type' => 'post',
										'id' => 'addCar'
									)
							);
					?>
						<div class="form-group">
							<?php
								echo $this->Form->input(
										'registeration_number',
										array(
												'class' => 'form-control input-lg',
												'placeholder' => __('Enter car registeration number')
											)
									);
							?>
						</div>
						<div class="form-group">
							<?php
								echo $this->Form->input('car_type', array(
				                    			'div' => false,
				                    			'label' => false,
				                    			'options' => Configure::read('CarTypes'),
				                    			'class' => 'form-control'
				                    			)
				                    		);
							?>
						</div>
						<?php
							echo $this->Form->submit(
									__('Add'),
									array(
											'class' => 'btn btn-info btn-lg btn-block text-uppercase',
											'escape' => false
										)
								);
					echo $this->Form->end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>