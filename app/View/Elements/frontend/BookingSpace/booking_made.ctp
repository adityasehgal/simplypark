<?php 
	echo $this->Form->create('Bookings',
			array(
					'type' => 'GET',
					'action' => 'ajaxBookingsMade',
					'class' => 'form-horizontal clearfix',
					'id' => 'filter-bookings-made'
			)
		);
?>
  	
<div class="form-group">
	<?php
		echo $this->Form->hidden(
				'bookingType',
				array(
						'value' => 1
					)
			);
	?>
	<label class="col-sm-2 control-label" for="address"><?php echo __('Search'); ?></label>
	
	<div class="col-sm-4">
		<?php
			echo $this->Form->input(
						'booking-start-date-made',
						array(
								'class' => 'form-control start-date',
								'placeholder' => 'From Date',
								'value' => isset($bookingStartDateMade) ? $bookingStartDateMade : "",
								'label' => false,
								'div' => false
							)
					);
		?>
	</div>
	<div class="col-sm-4">
		<?php
			echo $this->Form->input(
						'booking-end-date-made',
						array(
								'class' => 'form-control end-date',
								'placeholder' => 'Until Date',
								'value' => isset($bookingEndDateMade) ? $bookingEndDateMade : "",
								'label' => false,
								'div' => false
							)
					);
		?>
	</div>
	<div class="col-sm-2">
		<?php
			echo $this->Form->submit(
						__('Show'),
						array(
								'class' => 'btn btn-info'
							)
					);
		?>
	</div>					
</div>			
			
<?php echo $this->Form->end(); ?>

<div class="table-responsive">
	<table class="table booking-made">

		<thead> 
			<tr>
				<th><?php echo __('BOOKING ID'); ?></th>
				<th><?php echo __('PARKING DETAILS'); ?></th>
				<!-- <th><?php //echo __('PARKING DATE & TIME'); ?></th> -->
				<th><?php echo __('BOOKING AMOUNT'); ?></th>
				<th><?php echo __('STATUS'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php $i = 0;
			if (!empty($bookingMadeReceived)) {
				foreach($bookingMadeReceived as $showMadeBookings) {
					if ($showMadeBookings['Booking']['user_id'] != $this->Session->read('Auth.User.id')) {
						continue;
					}
			?>
						<tr>
							<td scope="row"><?php echo str_replace('pay', 'SP', $showMadeBookings['Transaction']['payment_id']); ?></td>
							<td>
							<!--<small><?php echo __('Booking Date') . ' : ' . date('d F', strtotime($showMadeBookings['Booking']['start_date'])) ?></small>-->
							<div><?php echo $showMadeBookings['Space']['name']; ?>

							</div>
							<small>
									<?php echo date('d M,y H:i A', strtotime($showMadeBookings['Booking']['start_date'])) . ' - ' . date('d M,y H:i A', strtotime($showMadeBookings['Booking']['end_date'])); ?>
								</small>	
							</td>
							<!-- <td>
							<small>
							<?php echo date('d F, Y', strtotime($showMadeBookings['Booking']['start_date'])); ?><br>
							<?php echo date('H:i A', strtotime($showMadeBookings['Booking']['start_date'])) . ' - ' . date('H:i A', strtotime($showMadeBookings['Booking']['end_date'])); ?>
							</small>
							</td> -->
							<td>
								<?php echo $showMadeBookings[0]['booking_made_amount']; ?>
							</td>
							<?php $action = $this->UserData->bookingActions($showMadeBookings['Booking']['end_date'], $showMadeBookings['Booking'][Configure::read('BookingTableStatusField')]); ?>
							<td>

							<?php
							switch ($action) {
								case 'Approved':
									?>
										<small>
										<div class="approved"></div>&nbsp;	<?php echo __('Approved Parking'); ?>&nbsp;<br />
										<?php 
										echo $this->Html->link(
												__('(Print Booking Pass)'),
												'print_ticket/'.urlencode(base64_encode($showMadeBookings['Booking']['id'])),
												array(
													'class' => 'booking-print',
													'escape' => false
												     )
												);
									?><br />
										<?php echo $showMadeBookings['booking_status']; ?><br />
									<?php 
                                        if ( $showMadeBookings['is_cancel_allowed'] ) {
										    echo $this->Html->link(
												   	__('Cancel Booking'),
                                                    '#',
												    array(
														'data-toggle' => 'modal',
													    'data-target' => '#myModalcancel',
													    'class' => 'cancel-request',
													    'escape' => false,
													    'title' => $showMadeBookings['hover_text'],
                                                        'data-link' => 'cancelBooking/'.urlencode(base64_encode($showMadeBookings['Booking']['id'])).'/1'
													    )
												    );
								       	} else {
                                        	$displayText = $showMadeBookings['hover_text'];
                                            echo "$displayText";  
                                        }
									?>
										</small>
										<?php
										break;
								case 'DisApproved':
									?>
										<small>
										<div class="rejected"></div>&nbsp;	<?php echo __('Your booking request rejected'); ?>
										</small>
										<?php
										break;
								case 'Waiting':
									?>
										<small>
										<div class="waiting"></div>
										&nbsp;   <?php echo __('Waiting for approval'); ?><br>
										<?php 
	                                        if ( $showMadeBookings['is_cancel_allowed'] ) {
												echo $this->Html->link(
														__('Cancel Booking'),
	                                                    '#',
														array(
															 	'data-toggle' => 'modal',
															 	'data-target' => '#myModalcancel',
															 	'class' => 'cancel-request',
															 	'escape' => false,
															 	'title' => $showMadeBookings['hover_text'],
	                                                         	'data-link' => 'cancelBooking/'.urlencode(base64_encode($showMadeBookings['Booking']['id'])).'/1',
														    )
														);
											 } else {
	                                           	$displayText = $showMadeBookings['hover_text'];
	                                            echo "$displayText";  
											}
									?>
										</small>
										<?php
										break;
								case 'Cancelled(in notice period)':
									?>
										<small>
										<div class="notice-period-ongoing"></div>&nbsp;	<?php echo __('Booking Cancelled<br/>(Notice Period Ongoing)'); ?>
										</small>
										<?php
										break;
								case 'Cancelled':
									?>
										<small>
										<div class="rejected"></div>&nbsp;	<?php echo __('Booking Cancelled'); ?>
										</small>
										<?php
										break;
								default:
									?>
										<small>
										<i class="fa fa-times expired-booking"></i> &nbsp;  <?php echo __('Expired booking'); ?>
										</small>
										<?php
										break;
							} ?>
							</td>
						</tr>
						<?php
						$i++;
				}
			} if($i == Configure::read('Bollean.False')) {
			?>
				<tr>
					<td scope="row" colspan="5" class="text-center"><?php echo __('No Records Found.'); ?></td>
				</tr>
			<?php  } ?>
		</tbody>
	</table>
</div>
