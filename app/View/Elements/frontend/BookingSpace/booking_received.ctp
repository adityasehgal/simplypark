<?php 
	echo $this->Form->create('Bookings',
			array(
					'type' => 'GET',
					'action' => 'ajaxBookingsReceived',
					'class' => 'form-horizontal clearfix',
					'id' => 'filter-bookings-received'
			)
		);
?>
  	
<div class="form-group">
	<?php
		echo $this->Form->hidden(
				'bookingType',
				array(
						'value' => 2
					)
			);
	?>
	<label class="col-sm-2 control-label" for="address"><?php echo __('Search'); ?></label>
	
	<div class="col-sm-4">
		<?php
			echo $this->Form->input(
						'booking-start-date-recieved',
						array(
								'class' => 'form-control start-date-receive',
								'placeholder' => 'From Date',
								'value' => isset($bookingStartDateReceived) ? $bookingStartDateReceived : "",
								'label' => false,
								'div' => false
							)
					);
		?>
	</div>
	<div class="col-sm-4">
		<?php
			echo $this->Form->input(
						'booking-end-date-recieved',
						array(
								'class' => 'form-control end-date-receive',
								'placeholder' => 'Until Date',
								'value' => isset($bookingEndDateReceived) ? $bookingEndDateReceived : "",
								'label' => false,
								'div' => false
							)
					);
		?>

	</div>
	<div class="col-sm-2">
		<?php
			echo $this->Form->submit(
						__('Show'),
						array(
								'class' => 'btn btn-info'
							)
					);
		?>
	</div>					
</div>				
		
<?php echo $this->Form->end(); ?>


<div class="table-responsive">
  	<table class="table booking-made">      
		<thead>
	        <tr>
				<th><?php echo __('BOOKING ID'); ?></th>
				<th><?php echo __('PARKING DETAILS'); ?></th>
				<!-- <th><?php echo __('PARKING DATE & TIME'); ?></th> -->
				<th><?php echo __('BOOKING AMOUNT'); ?></th>
				<th><?php echo __('STATUS'); ?></th>
			</tr>
		</thead>
      	<tbody>
	        <?php $j = 0;
				if (!empty($bookingMadeReceived)) {
					foreach($bookingMadeReceived as $showMadeBookings) {
						if ($showMadeBookings['Booking']['space_user_id'] != $this->Session->read('Auth.User.id')) {
							continue;
						}
			?>
						<tr>
							<td scope="row"><?php echo str_replace('pay', 'SP', $showMadeBookings['Transaction']['payment_id']); ?></td>
							<td>
							<!--	<small><?php echo __('Booking Date') . ' : ' . date('d F', strtotime($showMadeBookings['Booking']['start_date'])) ?></small>-->
								<div><?php echo $showMadeBookings['Space']['name']; ?>

								</div>	
								<small>
									<?php echo date('d M,y H:i A', strtotime($showMadeBookings['Booking']['start_date'])) . ' - ' . date('d M,y H:i A', strtotime($showMadeBookings['Booking']['end_date'])); ?>
								</small>
							</td>
							<!-- <td>
								<small>
									<?php echo date('d F, Y', strtotime($showMadeBookings['Booking']['start_date'])) ?><br>
									<?php echo date('H:i A', strtotime($showMadeBookings['Booking']['start_date'])) . ' - ' . date('H:i A', strtotime($showMadeBookings['Booking']['end_date'])); ?>
								</small>
							</td> -->
							<td><?php echo $showMadeBookings[0]['booking_received_amount']; ?></td>
							<?php $action = $this->UserData->bookingActions($showMadeBookings['Booking']['end_date'], $showMadeBookings['Booking'][Configure::read('BookingTableStatusField')]); ?>
							<td>
							<?php
								switch ($action) {
									case 'Approved':
										?>
											<small>
											<div class="approved"></div>&nbsp;	<?php echo __('Approved Booking'); ?><br>
										        <?php echo $showMadeBookings['booking_status']; ?><br />
											<?php 
											if ( $showMadeBookings['is_cancel_allowed'] )
											{
												echo $this->Html->link(
														__('Cancel Booking'),
                                                        '#',
														array(
															'data-toggle' => 'modal',
															'data-target' => '#myModalcancelbooking',
															'class' => 'cancel-request-by-owner',
															'escape' => false,
															'title' => $showMadeBookings['hover_text'],
                                                           	'data-link' => 'cancelBooking/'.urlencode(base64_encode($showMadeBookings['Booking']['id'])).'/2'
														     )
														);
											}
											else {
												$displayText = $showMadeBookings['hover_text'];
												echo "$displayText";  
											}
										?>
											</small>
											<?php
											break;
									case 'DisApproved':
										?>
											<small>
											<div class="rejected"></div>&nbsp;	 <?php echo __('Booking request rejected by you'); ?>
											</small>
											<?php
											break;
									case 'Waiting':
										?>
											<small>
											<div class="waiting"></div> &nbsp;   <?php echo __('Please approve/disapprove the request'); ?><br>
											<?php 
											echo $this->Html->link(
													__('Approve'),
													'#',
													array(
														'data-toggle' => 'modal',
														'data-target' => '#myModalapprove',
														'class' => 'aprove-text approve-booking',
														'escape' => false,
														'data-id' => base64_encode(urlencode($showMadeBookings['Booking']['id'])),
														'data-payment-id' => $showMadeBookings['Transaction']['payment_id'],
														'data-payment-amount' => $showMadeBookings[0]['amount_to_capture']
													     )
													);
										?> &nbsp; &nbsp; 
										<?php 
											echo $this->Html->link(
													__('Disapprove'),
													'#',
													array(
														'data-toggle' => 'modal',
														'class' => 'disapprove-text disapprove-booking',
														'escape' => false,
														'data-id' => base64_encode(urlencode($showMadeBookings['Booking']['id'])),
														'data-payment-id' => $showMadeBookings['Transaction']['payment_id'],
													     )
													);
										?><br>
											</small>
											<?php
											break;
                                                                        case 'Cancelled(in notice period)':
                                                                                 ?>
											<small>
											<div class="notice-period-ongoing"></div>&nbsp; <?php echo __('Booking cancelled<br />(Notice Period ongoing)'); ?>
											</small>
											<?php
											break;
									case 'Cancelled':
										?>
											<small>
											<div class="rejected"></div>&nbsp; <?php echo __('Booking cancelled'); ?>
											</small>
											<?php
											break;
										?>
									<?php
									default:
									?>
											<small>
											<i class="fa fa-times expired-booking"></i> &nbsp;  <?php echo __('Expired booking'); ?>
											</small>
											<?php
											break;
								}
								?>
							</td>
						</tr>
			<?php
						$j++;
					}
				} if ($j == Configure::read('Bollean.False'))  {
			?>
				<tr>
					<td scope="row" colspan="5" class="text-center"><?php echo __('No Records Found.'); ?></td>
				</tr>
			<?php  } ?>
 	</tbody>
</table>
</div>
<script type="text/html" id="approveSpace">
	<small>
		<div class="approved"></div>&nbsp;	Approved Booking<br>
		Starts on <%= data.StartDate %><br>
		<% if (data.is_cancel_allowed) { %>
			<a href="#" class="cancel-request-by-owner" data-toggle="modal" data-target="#myModalcancelbooking" data-link=<%= 'bookings/cancelBooking/'+Base64.encode(data.BookingID)+'/2' %> title="<%= data.hover_text %>">
				Cancel Booking
			</a>
		<% } else { %>
			<%= data.hover_text %>
		<% } %>
	</small>
</script>

<script type="text/html" id="disapproveSpace">
	<small>
		<div class="rejected"></div>&nbsp;	 Booking request rejected by you
	</small>
</script>
