<footer class="sticky-footer">
	<div class="container img-block">
		<?php
	        echo $this->Html->image(
	                'frontend/footer-bg.png',
	                array(
	                		'class' => 'img-responsive',
	                        'alt' => 'footer background'
	                    )
	            );
	    ?>
	</div>
	<div id="main-footer" class="color-bg light-typo">
		<div class="container-fluid text-center">
			<ul class="social-links">
				<li>
					<a target="_blank" href="https://twitter.com/simplyparkin">
						<i class="fa fa-twitter fa-fw"></i>
					</a>
				</li>
				<li>
					<a target="_blank" href="https://www.facebook.com/simplypark.in">
						<i class="fa fa-facebook fa-fw"></i>
					</a>
				</li>
			</ul>
			<ul class="list-inline footer-nav">
				<?php
					foreach ($cmsPages as $id => $pageTitle) {
				?>
					<li>
						<?php
							echo $this->Html->link(
									$pageTitle,
									'/homes/cms/'.urlencode(base64_encode($id)),
									array(
											'escape' => false,
										)
								);
						?>
					</li>

				<?php
					}
				?>
				<li>
					<?php
						echo $this->Html->link(
								'Contact Us',
								'/homes/contactUs',
								array(
										'escape' => false,
									)
							);
					?>
				</li>
                                 <li>
					<?php
						echo $this->Html->link(
								'Blog',
								'https://www.simplypark.in/blog',
								array(
										'escape' => false,
									)
							);
					?>
                               </li>
			</ul>
			<p><?php echo __('Copyright (c) SimplyPark 2015'); ?></p>
                        <div><font size="1">Some Icons are made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a>             is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0">CC BY 3.0</a></font></div>
	</div>
</footer>
