 
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SimplyPark.in - Command Centre</title>

    <?php echo $this->Html->css(array('frontend/bootstrap/bootstrap.min.css',
                                      'frontend/bootstrap/font-awesome',
				      'frontend/commandcentre/animate.min.css',
				      'frontend/commandcentre/nprogress.css',
				      'frontend/commandcentre/green.css',
				      'frontend/commandcentre/bootstrap-progressbar-3.3.4.min.css',
				      'frontend/commandcentre/jqvmap.min.css',
				      'frontend/commandcentre/daterangepicker.css',
				      'frontend/commandcentre/custom.min.css',

			             )
			     );?>



  </head>

