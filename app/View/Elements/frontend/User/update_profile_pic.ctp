<div class="modal fade custom-modal" id="updateProfilePic" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div class="modal-padding-lg">
					<h3><?php echo __("Update Profile Picture"); ?></h3>
					<?php
						echo $this->Form->create(
								'UserProfile',
								array(
										'url' => '/users/udpateProfilePic',
										'method' => 'post',
										'id' => 'updateProfilePicForm',
										'enctype' => 'multipart/form-data'
									)
							);
							echo $this->Form->hidden(
									'id',
									array(
											'value' => $getUserProfile['UserProfile']['id']
										)
								);
					?>
						<div class="radio">
							<label>
								<?php
									echo $this->Form->input('upload_remove_profile_pic', array(
									    'type' => 'radio',
									    'options' => array(1 => 'Upload Profile Pic',),
									    'class' => 'testClass uploadpic',
									    'div' => false,
									    'label' => false,
									    'hiddenField' => false, // added for non-first elements
									    'checked' => true
									));
								?>								 
							</label>
						</div>
						<div class="form-group col-sm-offset-1">
							<?php
								echo $this->Form->input(
										'profile_pic',
										array(
												'class' => 'form-control input-lg',
												'type' => 'file',
												'label' => false,
												'placeholder' => __('Select Profile Picture')
											)
									);
							?>
							<div class="help-info">
								<small class="text-info">
									<?php echo __('Please upload image with minimum upload size of 150*160.'); ?>
								</small>
							</div>
						</div>
						<div class="radio">
							<label>
							<?php
									echo $this->Form->input('upload_remove_profile_pic', array(
									    'type' => 'radio',
									    'options' => array(0 => 'Remove Current Pic'),
									    'class' => 'testClass removepic',
									    'div' => false,
									    'label' => false,
									    'hiddenField' => false // added for non-first elements
									));
								?>
							</label>
						</div>
						<?php
							echo $this->Form->submit(
									__('update'),
									array(
											'class' => 'btn btn-info btn-lg btn-block text-uppercase',
											'escape' => false
										)
								);
					echo $this->Form->end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>