<?php echo $this->Html->script(array('frontend/misc'), array('inline' => false)); ?>
<?php echo $this->Session->flash('spaceMessage'); ?>
<div id="main-nav" class="navbar">

	<div class="container">
	   
		<div class="navbar-header">
		
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#site-nav">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			
			<!-- ======= LOGO ========-->
			<a class="navbar-brand scrollto" href="#">
				<?php
					echo $this->Html->link(
                        $this->Html->image(
                                'frontend/logo-simply-park.png',
                                array(
                                    'class' => 'site-logo img-responsive'
                                    )
                            ),
                            '/',
                            array(
                                'class' => 'navbar-brand scrollto',
                                'escape' => false
                        )
                	);
				?>
			</a>
			
		</div>
		
		<div id="site-nav" class="navbar-collapse collapse">
			<form class="navbar-form navbar-left search" role="search">
				<div class="input-group">
					<input type="text" class="form-control search-box" placeholder="Search for parking places or events"  id="search_new" />
					<span class="input-group-btn">
						<a class="btn btn-orange text-uppercase" id="search-btn" ><?php echo __('Search'); ?></a>
					</span>
				</div><!-- /input-group -->
			</form>
			<div class="search-spot hide">
				<div class="input-group ">
					<input type="text" class="form-control input-lg" placeholder="Search for parking places or events">
					<span class="input-group-btn">
						<button class="btn btn-orange btn-lg text-uppercase" type="button"><?php echo __('Searh'); ?></button>
					</span>
				</div><!-- /input-group -->
			</div>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
                                      <?php
						echo $this->Html->link(
									__('Help&nbsp;').'<span class="caret"></span>',
									'#',
									array(
	                                                                       'class' => 'dropdown-toggle',
	                                                                        'data-toggle' => 'dropdown',
	                                                                        'role' => 'button',
	                                                                        'aria-expanded' => 'false',
	                                                                        'escape' => false
									     )
								       );
							?>
					<ul class="dropdown-menu" role="menu">
						<li>
							<?php
								echo $this->Html->link(
										__('What is SimplyPark Booking Pass?'),
										'https://www.simplypark.in/faq/index.php?action=artikel&cat=2&id=13&artlang=en',
										array(
												'escape' => false,
                                                                                                'target' => '_blank'
                      
											)
									);
							?>
						</li>
						<li>
							<?php
								echo $this->Html->link(
										__('How do I list a space?'),
										'https://www.simplypark.in/faq/index.php?action=artikel&cat=4&id=17&artlang=en',
										array(
												'escape' => false,
                                                                                                'target' => '_blank'
											)
									);
							?>
						</li>
						<li>
							<?php
								echo $this->Html->link(
										__('More Questions?'),
										'https://www.simplypark.in/faq',
										array(
												'escape' => false,
                                                                                                'target' => '_blank'
											)
									);
							?>
						</li>
						<li>
							<?php
								echo $this->Html->link(
										__('Contact Us'),
										'https://www.simplypark.in/homes/contactUs',
										array(
												'escape' => false,
                                                                                                'target' => '_blank'
											)
									);
							?>
						</li>
                                        </ul>
				</li>
				<li class="dropdown">
					<?php
						// Get profile picture path for user
						$profilePic = $this->UserData->userProfilePicturePath($getUserProfile['UserProfile']['profile_pic']);
						echo $this->Html->link(
	                        $this->Html->image(
	                                $profilePic,
	                                array(
		                                    'class' => 'img-circle',
		                                    'alt' => 'Profile Picture',
		                                    'width' => '40px'
	                                    )
	                            ).'<span class="caret"></span>',
	                            '#',
	                            array(
	                                'class' => 'dropdown-toggle',
	                                'data-toggle' => 'dropdown',
	                                'role' => 'button',
	                                'aria-expanded' => 'false',
	                                'escape' => false
		                        )
		                	);
					?>
					<ul class="dropdown-menu" role="menu">
						<li>
							<?php
								echo $this->Html->link(
										__('Update Profile'),
										'/users/updateProfile',
										array(
												'escape' => false
											)
									);
							?>
						</li>
						<li>
							<?php
								if (!$this->Session->read('Auth.User.social_network_user')) {
									echo $this->Html->link(
											__('Change Password'),
											'/users/changePassword',
											array(
													'escape' => false
												)
										);
								}
							?>
						</li>
						<li>
							<?php
								echo $this->Html->link(
										__('Log Out'),
										'/users/logout',
										array(
												'escape' => false
											)
									);
							?>
						</li>
					</ul>
				</li>
			</ul>
		</div><!--End navbar-collapse -->
	</div><!--End container -->
</div><!--End main-nav -->
<?php
	echo $this->element('frontend/User/update_profile_pic');
?>
