<aside class="col-xs-12 col-md-3">
	<div class="row">
		<div class="user-image text-center">
			<?php
				// Get profile picture path for user
				$profilePic = $this->UserData->userProfilePicturePath($getUserProfile['UserProfile']['profile_pic']);
				echo $this->Html->image(
	                            $profilePic,
	                            array(
	                                    'class' => 'img-circle',
	                                    'alt' => 'Profile Picture'
	                                )
	                        );
			?>
			<div class="btn-group edit">
			  	<?php
					echo $this->Html->link(
							'<span class="fa-stack fa-lg">
							  	<i class="fa fa-circle fa-stack-2x"></i>
							  	<i class="fa fa-camera fa-stack-1x fa-inverse"></i>
							</span>',
							'#',
							array(
									'data-toggle' => 'modal',
                                    'data-target' => '#updateProfilePic',
									'escape' => false
								)
						);
				?>
			</div>
			<h3><?php echo $getUserProfile['UserProfile']['first_name'] . ' ' . $getUserProfile['UserProfile']['last_name']; ?></h3>
			<span><?php echo $this->Session->read('Auth.User.email'); ?></span>
		</div>
	</div>

	<div class="list-group row">
		<?php $summaryActiveOrNot = $this->UserData->leftNavTabActive('dashboards','dashboard');
			echo $this->Html->link(
					'<i class="fa fa-bar-chart fa-fw"></i><span>'. __("Summary").'</span>',
					'/dashboards/dashboard',
					array(
							'class' => 'list-group-item '.$summaryActiveOrNot,
							'escape' => false
						)
				);
		?>
		<?php $bookingActiveOrNot = $this->UserData->leftNavTabActive('bookings','listing');
			echo $this->Html->link(
					'<i class="fa fa-bookmark-o fa-fw"></i><span>'. __("Booking").'</span>',
					'/bookings/listing',
					array(
							'class' => 'list-group-item '.$bookingActiveOrNot,
							'escape' => false
						)
				);
		?>
		<?php $activeOrNot = $this->UserData->leftNavTabActive('users','parkingSpace');
			echo $this->Html->link(
					'<i class="fa fa-car fa-fw"></i><span>'.__('My Spaces').'</span>',
					'/users/parkingSpace',
					array(
							'class' => 'list-group-item '.$activeOrNot,
							'escape' => false
						)
				);
			$profileActiveOrNot = $this->UserData->leftNavTabActive('users','updateProfile');
			echo $this->Html->link(
					'<i class="fa fa-user fa-fw"></i>
					<span>'.__('Profile').'</span>',
					'/users/updateProfile',
					array(
							'class' => 'list-group-item '.$profileActiveOrNot,
							'escape' => false
						)
				);
			$withdrawalSourceActiveOrNot = $this->UserData->leftNavTabActive('WithdrawalSources','index');
			echo $this->Html->link(
					'<i class="fa fa-university fa-fw"></i><span>'.__('Withdrawal Source').'</span>',
					'/WithdrawalSources',
					array(
							'class' => 'list-group-item '.$withdrawalSourceActiveOrNot,
							'escape' => false
						)
				);

			if (!$this->Session->read('Auth.User.social_network_user')) {
				$activeOrNot = $this->UserData->leftNavTabActive('users','changePassword');
				echo $this->Html->link(
						'<i class="fa fa-key fa-fw"></i>'.__('Change Password'),
						'/users/changePassword',
						array(
								'class' => 'list-group-item '.$activeOrNot,
								'escape' => false
							)
					);
			}
			echo $this->Html->link(
					'<i class="fa fa-sign-out fa-fw"></i><span>'.__('Log Out').'</span>',
					'/users/logout',
					array(
							'class' => 'list-group-item',
							'escape' => false
						)
				);
		?>
	</div>		
</aside>
