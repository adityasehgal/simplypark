<!-- Modal -->
<div class="modal fade location-details-popup" id="spaceInfoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div id="spaceTemplate">

			</div>
		</div>
	</div>
</div>
<script type="text/html" id="spaceInfoContainer">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<h4 class="modal-title view-details-title" id="myModalLabel"><%= data.Space.name %></h4>
	</div>
	<div class="modal-body clearfix">
		<article class="clearfix">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-4">
						<%
                            if (data.Space.place_pic != null) {
                                var imgPath = base_url+'files/SpacePic/place_pic.'+data.Space.place_pic;
                                if (fileExists(imgPath)) {
                        %>
                                    <img class="img-responsive" src=<%= base_url+'files/SpacePic/place_pic.'+data.Space.place_pic %> />
                        <%
                                } else {
                        %>
                                    <img class="img-responsive" src=<%= base_url+'img/Not_available.jpg' %> />
                        <%
                                }
                            } else {
                        %>
                                <img class="img-responsive" src=<%= base_url+'img/Not_available.jpg' %> />
                        <% } %>
					</div>
					<div class="col-xs-8 quick-park">
						<p>
						<%= data.Space.description %>
						</p>
					</div>
				</div>
			</div>
		</article>
                <%if(!data.Space.is_parking_free) {%> 
		<article class="clearfix">
			<div class="col-xs-12">
 
                         <table class="table">
					<thead>
						<tr>
							<% if (data.SpacePricing.hourly > 0.00 || data.Space.tier_pricing_support > 0) { %>
								<th>Hourly</th>
							<% } if (data.SpacePricing.daily > 0.00) { %>
								<th>Daily</th>
							<% } if (data.SpacePricing.weekely > 0.00) { %>
								<th>Weekly</th>
							<% } if (data.SpacePricing.monthly > 0.00) { %>
								<th>Monthly</th>
							<% } if (data.SpacePricing.yearly > 0.00) { %>
								<th>Yearly</th>
							<% } %>
						</tr>
					</thead>
					<tbody>
						<tr>
							<% if (data.SpacePricing.hourly > 0.00 ) { %>
							<td><i class="fa fa-inr"></i><%= data.SpacePricing.hourly %></td>
							<% } else if (data.Space.tier_pricing_support > 0) { %>
								<td><%= data.Space.tier_formatted_desc %></td>
							<% } if (data.SpacePricing.daily > 0.00) { %>
								<td><i class="fa fa-inr"></i><%= data.SpacePricing.daily %></td>
							<% } if (data.SpacePricing.weekely > 0.00) { %>
								<td><i class="fa fa-inr"></i><%= data.SpacePricing.weekely %></td>
							<% } if (data.SpacePricing.monthly > 0.00) { %>
								<td><i class="fa fa-inr"></i><%= data.SpacePricing.monthly %></td>
							<% } if (data.SpacePricing.yearly > 0.00) { %>
								<td><i class="fa fa-inr"></i><%= data.SpacePricing.yearly %></td>
							<% } %>
						</tr>
					</tbody>
				</table>
                          <% } %> 
			</div>
		</article>

		<article class="clearfix">
			<div class="col-xs-12 ">
				<div class="row">
					<div class="col-xs-6">
						<address>
							<span>ADDRESS</span><br>
							<% if(data.Space.property_type_id != 2) { %>
								<p>	<%= data.Space.flat_apartment_number %></p>
								<p><%= data.Space.address1 %></p>
	                            <p><%= data.Space.address2 %></p>
	                            <p><%= data.Space.post_code %></p>
                                                               <% if (data.Space.is_available_with_simplypark) { %> 
								<p><i class="fa fa-phone">  </i>
									<% if (data.User.UserProfile) { %>
										<%= data.User.UserProfile.mobile %>
									<% } else { %>
										 N/A 
									<% } %>
								</p>
                                                               <% } %>
							<% } else { %>
									<% if (data.Space.tower_number != null) { %>
										<p>	Tower No. : <%= data.Space.tower_number %> </p>
									<% } else { %>
										 <p> Tower No. : N/A  </p>
									<% } %>
									
							<% } %>
						</address>
					</div>

					<div class="col-xs-6 extra-services">
						<span>AMENTIES</span>
						<%	if (data.SpaceFacility.length > 0) {
								_.each(data.SpaceFacility, function(value, key){ %>
									<p><%= value.Facility.name %></p>
						<% 		
								});
							} else { %>
								<p></p>
						<% } %>
					</div>
				</div>
			</div>
		</article>

		<article class="clearfix">
			<div class="col-xs-12 ">
				<div class="row">
					<div class="col-xs-6">
						<span>AVAILABLE SPACES</span><br>
						<% if (data.Space.number_slots != null) { %>
                            <%= data.Space.number_slots %>
                        <% } else { %>
                            <%= 0 %>
                        <% } %>
					</div>
					
				</div>
			</div>
		</article>
		<% if (data.CancellationPolicy.policy != null) { %>
			<article class="clearfix">
				<div class="col-xs-12 ">
					<span>CANCELLATION  POLICY</span>
					<p> <%= data.CancellationPolicy.policy %></p>
				</div>
			</article>
		<% } %>
	</div>
</script>
