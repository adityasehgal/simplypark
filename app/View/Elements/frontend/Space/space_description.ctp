<!-- Modal -->
<div class="modal fade location-details-popup" id="spaceDescModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body clearfix">
				<?php echo $getSpaceDetail['Space']['admin_description']; ?>
			</div>
			<div class="modal-footer btn-center">
		        <?php
		            echo $this->Html->link('Ok',
		            	'#',
		            	array(
		            		'class' => 'btn btn-info booking-ok',
		            	)
		            );
		        ?>
		      </div>
		</div>
	</div>
</div>
