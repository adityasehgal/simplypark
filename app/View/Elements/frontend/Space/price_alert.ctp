<!-- Modal -->
<div class="modal fade location-details-popup" id="priceAlertModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body clearfix">
				You have left out certain Price fields. This will restrict your space availability and be less attractive to potential drivers.<br>
				We recommend you review these one final time (Just Press No)<br>
				Are you sure you want to continue?
			</div>
			<div class="modal-footer btn-center">

				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('No'); ?></button>
				<?php
                   echo $this->Html->link(
		                          __('Yes'),
		                             '#',
		                       array(
		                             'class' => 'btn btn-info',
		                             'escape' => false,
		                             'id' => 'price-confirm'
		                             )
		                          );
                ?>
		    </div>
		</div>
	</div>
</div>
