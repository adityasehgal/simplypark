<?php //pr($getBookingData);?>
<div class="header clearfix">
	<div class="pull-left">
		<h3>E-Ticket</h3>
		<p>
			<small>
				<span>Booking Date: <?php echo $this->Time->nice(DATE($getBookingData['Booking']['created'])); ?><!--  Sat, 23 May 2015 --></span>
			</small>
		</p>
	</div>
	<div class="pull-right logo">
		<!-- ======= LOGO ========-->
		<a class="navbar-brand" href="#">
			<?php
				echo $this->Html->link(
                    $this->Html->image(
                            'frontend/logo-simply-park.png',
                            array(
                                'class' => 'site-logo img-responsive'
                                )
                        ),
                        '/',
                        array(
                            'class' => 'navbar-brand scrollto',
                            'escape' => false
                    )
            	);
			?>
		</a>
	</div>
</div>
<div class="cloud-bg col-xs-12 clearfix">
	<h3>Parking Booking Details</h3>
	<div class="clearfix">
		<div class="well clearfix well-opacity">
			<div class="col-xs-4">
				<h4><?php echo $getBookingData['Space']['name']; ?></h4>
				<p><?php echo $getBookingData['Space']['City']['name'];  ?></p>
				<address>
					<?php echo $getBookingData['Space']['address1']; ?>,<br />
					<?php echo $getBookingData['Space']['address2']; ?><br />
					<?php echo $getBookingData['Space']['City']['name'] . ',' . $getBookingData['Space']['State']['name']; ?><br>
					<?php echo "Post Code : " . $getBookingData['Space']['post_code']; ?><br />
					+91 - <?php echo $getBookingData['Booking']['mobile']; ?>
				</address>
			</div>
			<div class="col-xs-6 left-border">
				<table class="booking-detail">
					<tbody>
						<tr>
							<td>Name</td>
							<td><?php echo $getBookingData['Booking']['first_name'].' '.$getBookingData['Booking']['last_name']; ?></td>
						</tr>	
						<tr>
							<td>Car registration Number</td>
							<td><?php echo $getBookingData['UserCar']['registeration_number']; ?></td>
						</tr>	
						<tr>
							<td>Transaction ID</td>
							<td><?php echo 'SP_'.substr($getBookingData['Transaction']['payment_id'],4); ?></td>
						</tr>	
						<tr>
							<td>Booking with</td>
							<td><?php echo $getBookingData['Booking']['email']; ?></td>
						</tr>	
						<tr>
							<td>Parking date &amp; time</td>
							<td><?php echo $this->Time->format($getBookingData['Booking']['start_date'],'%e %b, %Y %H:%M %p') .' - '. $this->Time->format($getBookingData['Booking']['end_date'],'%e %b, %Y %H:%M %p'); ?></td>
						</tr>	
						<tr>
							<td>Duration</td>
							<td><?php 
								echo $dateDiff = $this->Front->dateDiff($getBookingData['Booking']['start_date'],$getBookingData['Booking']['end_date']);
								
							?>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-xs-2 right-border">
                               <?php
                                 if (file_exists(Configure::Read('QrImagePath').$getBookingData['Booking']['qrcodefilename'])){
                                     echo $this->Html->image(
                                                            '/'.Configure::Read('QrImagePath').$getBookingData['Booking']['qrcodefilename'],
                                                             array(
                                                                'class' => 'img-responsive'
                                                             )
                                                          );
                                 }
                       ?>

                        </div>
		</div>
	</div>

	<p>
		<b>Transaction ID: <?php echo 'SP_'.substr($getBookingData['Transaction']['payment_id'],4); ?></b>
	</p>
		<?php echo $this->element('backend/Booking/print_ticket_content'); ?>
	
</div>
<script type="text/javascript">
	$(document).ready(function () {
		window.print();		
	});
</script>
