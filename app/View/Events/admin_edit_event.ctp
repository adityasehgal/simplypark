<?php
	echo $this->Html->script('backend/Event/add_event', array('inline' => false));
?>
<div class="crumbs">
    <ul id="breadcrumbs" class="breadcrumb">
        <li class="active">
            <i class="icon-building"></i>
            <?php
                echo $this->Html->link(
                        'Events',
                        '/admin/events/',
                        array(
                                'class' => 'active',
                            )
                    ); 
            ?>
        </li>
        <li><?php echo __('Edit Event'); ?></li>
    </ul>
</div>
<div class="panel1">    
    <div class="row">
	    <div class="col-lg-12">
	    	<?php
	    		echo $this->Form->create(
	    				'Event',
	    				array(
	    					'url' => '/admin/events/updateEvent',
	    					'method' => 'post',
	    					'class' => 'form-horizontal',
	    					'id' => 'update-event',
	    					'enctype' => 'multipart/form-data',
	    					'novalidate' => false
	    					)
	    			);
	    	?>
	            <div class="row">
	                <div class="col-md-8">
	                    <div class="form-group">
	                    <label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Event Name'); ?><span class='red'>*</span></label>
	                    <div class="col-sm-9">
	                    	<?php
	                    		echo $this->Form->input('event_name', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'class' => 'form-control',
	                    			'value' => $events['Event']['event_name']
	                    			)
	                    		);
	                    	?>
	                    	<?php
	                    		echo $this->Form->input('id', array(
	                    			'type' => 'hidden',
	                    			'value' => $events['Event']['id']
	                    			)
	                    		);
	                    	?>
	                    </div>
	                  </div>
	                </div>
	                <div class="col-md-8">
	                    <div class="form-group">
	                    <label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Logo/Image of the event'); ?></label>
	                    <div class="col-sm-9">
	                    	<img src="<?php echo $events['path'].'event.'.$events['Event']['event_pic']; ?>" alt="Event Image"> 

	                    	<?php
	                    		echo $this->Form->input('event_pic', array(
	                    			'div' => false,
	                    			'type' => 'file',
	                    			'label' => false,
	                    			'class' => 'upload-pic form-control',
	                    			'value' => $events['Event']['event_pic']
	                    			)
	                    		);
	                    	?>
	                    	<span id="helpBlock" class="help-block"><small>Min Upload Size :  290px*189px</small></span>
	                    	<input type="hidden" id="edit-upload-pic" value="1">
	                    </div>
	                  </div>
	                </div>
	                <div class="col-md-8">
	                    <div class="form-group">
	                    <label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Website'); ?></label>
	                    <div class="col-sm-9">
		                    <?php
	                    		echo $this->Form->input('website', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'class' => 'form-control',
	                    			'value' => $events['Event']['website']
	                    			)
	                    		);
	                    	?>
	                    </div>
	                  </div>
	                </div>
	                <div class="col-md-8">
	                    <div class="form-group">
	                    <label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Description'); ?></label>
	                    <div class="col-sm-9">
		                    <?php
	                    		echo $this->Form->input('description', array(
	                    			'div' => false,
	                    			'type' => 'textarea',
	                    			'label' => false,
	                    			'class' => 'form-control',
	                    			'value' => $events['Event']['description']
	                    			)
	                    		);
	                    	?>
	                    </div>
	                  </div>
	                </div>
	                
	                <div class="col-md-8">
	                    <div class="form-group">
	                    <label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Search Tags'); ?><span class='red'>*</span></label>
	                    <div class="col-sm-9">
		                    <?php
	                    		echo $this->Form->input('tags', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'class' => 'form-control',
	                    			'value' => $events['Event']['tags']
	                    			)
	                    		);
	                    	?>
	                    </div>
	                  </div>
	                </div>
	                <div class="col-md-8">
	                	<div class="form-group pull-right">
			                <?php
		                        echo $this->Form->button(__('Update'), array(
		                                    'class' => 'btn btn-primary btn-lg',
		                                    'type' => 'submit'
		                                    ));
		                        echo $this->Html->link('Cancel', '/admin/events', array(
		                        	'class' => 'btn btn-default btn-lg'));
		                    ?>
			            </div>
	                </div>
	            </div>
		    <?php echo $this->Form->end(); ?>
	    </div>
	</div>
	<?php 
		echo $this->Html->script(
			array(
				'moment.min',
				'backend/bootstrap/bootstrap-datepicker',
				'backend/bootstrap/bootstrap-datetimepicker',
			),
			array('inline' => false)
    	);

    	echo $this->Html->css(
			array(
				'backend/bootstrap/css/bootstrap-datepicker3',
				'backend/bootstrap/css/bootstrap-datetimepicker.min'
			),
            null,array('inline' => false)
		);
	?>

	<script type="text/html" id="addCity">
		<label for="inputEmail3" class="col-sm-3 control-label">City</label>
        <div class="col-sm-9">
	        <select class='form-control' id = "city-list" name =data[Event][city_id]>
		        <option  value="0">(Select City)</option>
		        <% _.each(data, function(value, key){ %>
		            <option value="<%= key %>"><%= value %></option>
		        <% }); %>
		        </select>
        </div>
	</script>

	<script type="text/html" id="addTimeSlots">
		<div class='added-time-slots'  style="margin-top:10px;">
			<label for="inputEmail3" class="col-sm-3 control-label">Event Timings</label>
			<div class="col-sm-3 input-group date">
				<input id="EventStartTime" class="form-control date-pick" type="text" name="data[Event][time][<%= count %>][to]">
				<span class="input-group-addon">
		            <span class="glyphicon glyphicon-time"></span>
		        </span>
	        </div>
	        <div class="col-sm-3 input-group date">
				<input id="EventEndTime" class="form-control date-pick" type="text" name="data[Event][time][<%= count %>][from]">
				<span class="input-group-addon">
			        <span class="glyphicon glyphicon-time"></span>
			    </span>
		    </div>
		    <button class='btn btn-primary remove-button' type="button"><span class="glyphicon glyphicon-minus"></span></button>
	    </div>
	</script>
	<style type="text/css">
		.red {
			color: #FF0000;
		}
	</style>
</div>