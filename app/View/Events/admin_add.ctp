<?php
	echo $this->Html->script('backend/Event/add_event', array('inline' => false));
?>
<div class="crumbs">
    <ul id="breadcrumbs" class="breadcrumb">
        <li class="active">
            <i class="icon-building"></i>
            <?php
                echo $this->Html->link(
                        'Events',
                        '/admin/events/',
                        array(
                                'class' => 'active',
                            )
                    ); 
            ?>
        </li>
        <li><?php echo __('Add New Event'); ?></li>
    </ul>
</div>
<div class="panel1">    
    <div class="row">
	    <div class="col-lg-12">
	    	<?php
	    		echo $this->Form->create(
	    				'Event',
	    				array(
	    					'url' => '/admin/events/addEvent',
	    					'method' => 'post',
	    					'class' => 'form-horizontal',
	    					'id' => 'add-event',
	    					'enctype' => 'multipart/form-data',
	    					'novalidate' => false
	    					)
	    			);
	    	?>
	            <div class="row">
	                <div class="col-md-8">
	                    <div class="form-group">
	                    <label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Event Name'); ?><span class='red'>*</span></label>
	                    <div class="col-sm-9">
	                    	<?php
	                    		echo $this->Form->input('event_name', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'class' => 'form-control'
	                    			)
	                    		);
	                    	?>
	                    </div>
	                  </div>
	                </div>
	                <div class="col-md-8">
	                    <div class="form-group">
	                    <label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Logo/Image of the event'); ?><span class='red'>*</span></label>
	                    <div class="col-sm-9">
	                    	<?php
	                    		echo $this->Form->input('event_pic', array(
	                    			'div' => false,
	                    			'type' => 'file',
	                    			'label' => false,
	                    			'class' => 'upload-pic form-control'
	                    			)
	                    		);
	                    	?>
	                    	<span id="helpBlock" class="help-block"><small>Min Upload Size :  290px*189px</small></span>
	                    </div>
	                  </div>
	                </div>
	                <div class="col-md-8">
	                    <div class="form-group">
	                    <label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Website'); ?></label>
	                    <div class="col-sm-9">
		                    <?php
	                    		echo $this->Form->input('website', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'class' => 'form-control'
	                    			)
	                    		);
	                    	?>
	                    </div>
	                  </div>
	                </div>
	                <div class="col-md-8">
	                    <div class="form-group">
	                    <label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Description'); ?></label>
	                    <div class="col-sm-9">
		                    <?php
	                    		echo $this->Form->input('description', array(
	                    			'div' => false,
	                    			'type' => 'textarea',
	                    			'label' => false,
	                    			'class' => 'form-control'
	                    			)
	                    		);
	                    	?>
	                    </div>
	                  </div>
	                </div>
	                <div class="col-md-8">
	                    <div class="form-group">
	                    <label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Search Tags'); ?><span class='red'>*</span></label>
	                    <div class="col-sm-9">
		                    <?php
	                    		echo $this->Form->input('tags', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'class' => 'form-control'
	                    			)
	                    		);
	                    	?>
	                    </div>
	                  </div>
	                </div>
	                <div class="col-md-8">
	                	<div class="form-group pull-right">
			                <?php
		                        echo $this->Form->button(__('Save'), array(
		                                    'class' => 'btn btn-primary btn-lg',
		                                    'type' => 'submit'
		                                    ));
		                        echo $this->Html->link('Cancel', '/admin/events', array(
		                        	'class' => 'btn btn-default btn-lg'));
		                    ?>
		                </div>
		            </div>
	            </div>
	            
		    <?php echo $this->Form->end(); ?>
	    </div>
	</div>
	<?php 
		echo $this->Html->script(
			array(
				'moment.min',
				'backend/bootstrap/bootstrap-datepicker',
				'backend/bootstrap/bootstrap-datetimepicker',
			),
			array('inline' => false)
    	);

    	echo $this->Html->css(
			array(
				'backend/bootstrap/css/bootstrap-datepicker3',
				'backend/bootstrap/css/bootstrap-datetimepicker.min'
			),
            null,array('inline' => false)
		);
	?>
</div>