<?php echo $this->Html->script(array('frontend/event/event_locations', 'frontend/event/spaces_map'), array('inline' => false)); ?>
<div class="main-container">
	<div class="event-banner">
		<div class="container">
			<section class="clearfix">			
				<div class="col-sm-3 img-block">
					<?php 
						echo $this->Html->image(
							'/' . Configure::read('EventImagePath') . $getEventData['Event']['event_pic'],
							array(
								    'alt' => 'IPL Cricket',
								    'class' => 'img-responsive'
								)
						);
					?>
				</div>
				<div class="col-sm-9 ">
					<h2><?php echo $getEventData['Event']['event_name'] . '-' . $getEventData['EventAddress']['City']['name']; ?></h2>
					<p><?php echo nl2br($getEventData['Event']['description']); ?></p>
				</div>		
			</section>
		</div>
	</div>

	<div class="container">
		<div class="col-md-12 parking-details">
			<h3><?php echo __('Parking near'); ?> <strong><?php echo $getEventData['Event']['event_name'] . '-' . $getEventData['EventAddress']['City']['name']; ?> </strong></h3>
		</div>
	</div>
		<div class="container">
			<section class="clearfix">
				<?php if (!empty($nearestSpaces)) { ?>
					<div class="col-md-6 parkingspace-left-section">
						<?php foreach ($nearestSpaces as $showNearestSpaces) { ?>
								
								<div class="location-name clearfix" onclick="showSelectedSpace(<?php echo $showNearestSpaces['Space']['id']; ?>);" id="SPACEID-<?php echo $showNearestSpaces['Space']['id']; ?>">
									<div class="col-md-12">
										<h4><span class="map-marker-icon space-title"></span>
										<span class="parking-name"><?php echo $showNearestSpaces['Space']['name']; ?> </span></h4>
									</div>

									<div class="col-xs-12">
										<dl class="dl-horizontal">
											<dt><?php echo __('Price:'); ?></dt>
											
											<dd>
											   <?php if (!empty($showNearestSpaces['SpacePricing']['hourly']) && $showNearestSpaces['SpacePricing']['hourly'] > 0) {
        										          echo '&#8377 '.$showNearestSpaces['SpacePricing']['hourly'].' /hr';
											   } else if($showNearestSpaces['Space']['tier_pricing_support']){ 
												   echo $showNearestSpaces['Space']['tier_formatted_desc'];
                                                                                           } else if (!empty($showNearestSpaces['SpacePricing']['daily']) && $showNearestSpaces['SpacePricing']['daily'] > 0) {
											        echo '&#8377 '.$showNearestSpaces['SpacePricing']['daily'].' /day';
											    } else if (!empty($showNearestSpaces['SpacePricing']['weekely']) && $showNearestSpaces['SpacePricing']['weekely'] > 0) {
											        echo '&#8377 '.$showNearestSpaces['SpacePricing']['weekely'].' /week';
											    } else if (!empty($showNearestSpaces['SpacePricing']['monthly']) && $showNearestSpaces['SpacePricing']['monthly'] > 0) {
											        echo '&#8377 '.$showNearestSpaces['SpacePricing']['monthly'].' /month';
											    } else if (!empty($showNearestSpaces['SpacePricing']['yearly']) && $showNearestSpaces['SpacePricing']['yearly'] > 0) {
											        echo '&#8377 '.$showNearestSpaces['SpacePricing']['yearly'].' /annum';
											    } else {
											        echo 'N/A';
											    } ?>
											</dd>
											<dt><?php echo __('Distance:'); ?></dt>
											<dd><?php echo round($showNearestSpaces[0]['distance']).' km(s)'; ?></dd>
											<dt><?php echo __('Available slots:'); ?></dt>
											<dd><?php echo $showNearestSpaces['Space']['number_slots'] != '' ? $showNearestSpaces['Space']['number_slots']: 'N/A' ; ?></dd>
										</dl>
									</div>
									
									<div class="col-xs-6">
										<?php
											echo $this->Html->link(
													__('View Details'),
													'#',
													array(
															'class' => 'view-details space-details',
															'data-id' => urlencode(base64_encode($showNearestSpaces['Space']['id']))
														)
												);
										?>
									</div>
									<div class="col-xs-6 text-right">
										<?php
											echo $this->Html->link(
														__('BOOK NOW'),
														'/spaces/spaceDetail/'.urlencode(base64_encode($showNearestSpaces['Space']['id'])) . '/' .urlencode(base64_encode($eventTimingID)),
														array(
																'escape' => false,
																'class' => 'btn btn-info'
															)
													);
										?>
									</div>
								</div>
						<?php } ?>
					</div>
					<div class="col-md-8">
						<div id="map_wrapper">
						    <div id="map_canvas" class="mapping"></div>
						</div>
					</div>
				<?php } else { ?>
					<div class="col-md-12 text-center gray-bg">
						<strong><?php echo __('No Record Found.'); ?></strong>
					</div>
				<?php } ?>
			</section>
		</div>
</div>
<?php echo $this->element('frontend/Space/space_info'); 
	echo '<script>
			var nearestSpacesForMap = ' . json_encode($nearestSpaces) . ';
			var eventAddresForMap = ' . json_encode($getEventData) . '
		</script>';
?>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
