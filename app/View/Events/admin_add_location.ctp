<?php
	echo $this->Html->script('backend/Event/add_event', array('inline' => false));
?>
<div class="crumbs">
    <ul id="breadcrumbs" class="breadcrumb">
        <li class="active">
            <i class="icon-building"></i>
            <?php
                echo $this->Html->link(
                        'Events',
                        '/admin/events/',
                        array(
                                'class' => 'active',
                            )
                    ); 
            ?>
        </li>
        <li><?php echo __('Add New Event'); ?></li>
    </ul>
</div>
<div class="panel1">    
    <div class="row">
	    <div class="col-lg-12">
	    	<?php
	    		echo $this->Form->create(
	    				'Event',
	    				array(
	    					'url' => '/admin/events/addLocation',
	    					'method' => 'post',
	    					'class' => 'form-horizontal',
	    					'id' => 'add-event',
	    					'novalidate' => false
	    					)
	    			);
	    	?>
	            <div class="row">
	                <div class="col-md-8">
	                    <div class="form-group">
		                    <label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Enter Address/Place/Location/City'); ?></label>
		                    <div class="col-sm-9">
		                    	<?php
		                    		echo $this->Form->input('full_address', array(
		                    			'div' => false,
		                    			'label' => false,
		                    			'class' => 'form-control'
		                    			)
		                    		);
		                    	?>
		                    </div>
	                  	</div>
	                </div>
	                <div class="col-md-8">
	                    <div class="form-group">
		                    <label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Flat/Apartment Number'); ?></label>
		                    <div class="col-sm-9">
		                    	<?php
		                    		echo $this->Form->input('flat_apartment_number', array(
		                    			'div' => false,
		                    			'label' => false,
		                    			'class' => 'form-control'
		                    			)
		                    		);
		                    	?>
		                    </div>
	                  	</div>
	                </div>
	                <div class="col-md-8">
	                    <div class="form-group">
		                    <label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Address'); ?></label>
		                    <div class="col-sm-9">
			                    <?php
		                    		echo $this->Form->input('address', array(
		                    			'div' => false,
		                    			'label' => false,
		                    			'class' => 'form-control'
		                    			)
		                    		);
		                    	?>
		                    </div>
	                  	</div>
	                </div>
	                <div class="col-md-8">
	                    <div class="form-group">
		                    <label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('State'); ?></label>
		                    <div class="col-sm-9">
			                    <?php
		                    		echo $this->Form->input('state', array(
		                    			'div' => false,
		                    			'label' => false,
		                    			'class' => 'form-control'
		                    			)
		                    		);
		                    	?>
		                    </div>
		                </div>
	                </div>
	            </div>
	            <div class="form-actions text-center">
	                <?php
                        echo $this->Form->button(__('Next - Add Location'), array(
                                    'class' => 'btn btn-primary btn-lg',
                                    'type' => 'submit'
                                    ));
                        echo $this->Form->button(__('Close'), array(
                                    'class' => 'btn btn-default btn-lg',
                                    'url' => '/admin/events'
                                    ));
                    ?>
	            </div>
		    <?php echo $this->Form->end(); ?>
	    </div>
	</div>
</div>