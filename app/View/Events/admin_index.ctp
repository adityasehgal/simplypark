<div class="crumbs">
    <ul id="breadcrumbs" class="breadcrumb">
        <li class="active">
            <i class="icon-building"></i>
            <?php
                echo $this->Html->link(
                        __('Events'),
                        '/admin/Events/',
                        array(
                                'class' => 'active'
                            )
                    ); 
            ?>
        </li>
        <div class="pull-right">
            <?php
                echo $this->Html->link('<i class="icon-plus icon-spacing"></i>'.__('Add New Event'),
                        '/admin/Events/add',
                        array(
                            'class' => 'btn btn-primary btn-grad btn-bottom',
                            'escape' => false
                        ));
            ?>
        </div>
    </ul>
</div>
<div class="panel1">
    <span class="clearfix"></span>
    <div class="panel panel-default" id="inline-popups">
        <div class="panel-heading clearfix">
            <div class="right-inner-addon pull-right row">
                <?php
                    echo $this->Form->create('Event', array(
                                    'url' => '/admin/events/index',
                                    'type' => 'get',
                                    'novalidate' => true
                                    ));
                ?>
                        <div class="input-group col-xs-9 pull-right">
                            <?php
                                echo $this->Form->input(
                                    'search',
                                    array(
                                        'div' => false,
                                        'label' => false,
                                        'class' => 'form-control',
                                        'placeholder' => 'search',
                                        'escape' => false,
                                        'default' => (isset($this->request->query['search']) && !empty($this->request->query['search'])) ? $this->request->query['search'] : ''
                                        )
                                );
                            ?>
                            <span class="input-group-btn">
                                <?php
                                    echo $this->Form->button(
                                        '<i class="icon-search"></i>',
                                        array(
                                            'type' => 'submit',
                                            'class'=> 'btn btn-default',
                                            'escape' => false
                                            )
                                    );
                                ?>
                            </span>
                        </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
        <div class="table-responsive" id="innercontent">
            <?php echo $this->element('backend/Events/listing'); ?>
        </div>
    </div>
    <?php
        echo $this->Html->script(array(
            'backend/Event/event',
            'backend/commonmagnificpopup',
            ),
            array('inline' => false)
        );
    ?>
</div>
<?php
    echo $this->element('backend/Events/view_event');
    echo $this->element('backend/Events/add_address');
    echo $this->element('backend/active_popup');
    echo $this->element('backend/inactive_popup');
    echo $this->element('backend/delete_popup');
?>