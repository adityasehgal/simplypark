<?php
	echo $this->Html->script('frontend/event/event_detail', array('inline' => false));
?>
<div class="main-container">
	<div class="event-banner">
		<div class="container">
			<section class="clearfix">			
				<div class="col-sm-3 img-block">

					<?php 
						if (!empty($eventDetail['Event']['event_pic'])) {
							if (file_exists(Configure::Read('EventImagePath').$eventDetail['Event']['event_pic'])) {
								echo $this->Html->image(
									'/'.Configure::Read('EventImagePath').$eventDetail['Event']['event_pic'],
									array(
										    'class' => 'img-responsive'
										)
								);
							} else {
								echo $this->Html->image('Not_available.jpg',
									array(
										    'class' => 'img-responsive'
										)
								);
							}
						} else {
							echo $this->Html->image('Not_available.jpg',
								array(
									    'class' => 'img-responsive'
									)
							);
						}
					?>
				</div>
				<div class="col-sm-9 ">
					<h2><?php echo $eventDetail['Event']['event_name']; ?></h2>
					<p><?php echo nl2br($eventDetail['Event']['description']); ?></p>
				</div>		
			</section>
		</div>
	</div>
	<div class="container">
		<section class="clearfix">
			<div class="col-md-8">
				<div class="row heading">
					<div class="col-xs-12 col-sm-2 text-center">
						<h3><?php echo __('Events'); ?></h3>
					</div>
					<div class="col-xs-12 col-sm-10">
						<?php
							echo $this->Form->create(
									'EventAddress',
									array(
											'url' => '/events/ajaxSerachEventAddresses',
											'type' => 'get',
											'class' => 'form-horizontal row',
											'id' => 'search_address_form'
										)
								);
								echo $this->Form->hidden('event_id', array('value' => $eventDetail['Event']['id'],'id' => 'event-id'));
						?>
							<div class="col-sm-4">
								<div class="form-group">
									<label class="col-sm-4 control-label" for="exampleInputEmail1"><?php echo __('State'); ?></label>
									<div class="col-sm-8">
										<?php
											echo $this->Form->input('state', array(
											    'options' => $states,
											    'empty' => 'Select',
											    'class' => 'form-control search_address',
											    'label'	=> false,
											    'id' => 'state-event-list'
											));
										?>
									</div>
								</div>
							</div>
							<div id="cityContainer">
								<div class="col-sm-4 hide" id="empty-city">
									<div class="form-group">
										<label class="col-sm-4 control-label" for="exampleInputEmail1"><?php echo __('City'); ?></label>
										<div class="col-sm-8" id="city-list-all">
											<?php
												echo $this->Form->input('city', array(
													'options' => '',
												    'empty' => 'Select',
												    'class' => 'form-control search_address',
												    'label'	=> false,
												    'id' => 'city-list'
												));
											?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label class="col-sm-4 control-label" for="exampleInputEmail1"><?php echo __('Month'); ?></label>
									<div class="col-sm-8">
										<?php
											echo $this->Form->input('mob', 
												array(
														'options' => $eventMonths,
														'empty' => 'Select',
														'monthNames' => true,
														'class' => 'form-control search_address',
														'label'	=> false
													)
												);
										?>
									</div>
								</div>
							</div>						
						<?php echo $this->Form->end(); ?>
					</div>
				</div>
				<div class="loader">
			    	<?php
		                echo $this->Html->image(
		                    'loader.gif',
		                    array('id' => 'busy-indicator')
		                );
		            ?>
			    </div>
				<div class="body" id="event-addresses">
					<?php echo $this->element('frontend/Event/event_addresses'); ?>
				</div>
			</div>
			<div class="col-md-4">
				<h3><?php echo __('Other events'); ?></h3>
				<div class="row">
					<?php
						if(isset($getOtherEvents) && !empty($getOtherEvents)) {
							//pr($getOtherEvents);
							foreach ($getOtherEvents as $key => $value) {
								
								if(!empty($value['EventTiming'])) {
									foreach ($value['EventTiming'] as $showOtherEvents) {  ?> 

									<div class="col-xs-12 form-group item text-center img-block">
										<?php
										$image_path = Configure::read('EventImagePath') .$value['Event']['event_pic'];
									 	$image_src = file_exists($image_path) ? '/'.Configure::read('EventImagePath') . $value['Event']['event_pic'] :
									 		'event-no-image.png';
									
			                            echo $this->Html->link(
				                                $this->Html->image(
				                                        $image_src,
				                                        array(
				                                            'class' => 'img-responsive' 
				                                            )
				                                    ),
				                                    '/events/eventDetail/' . urlencode(base64_encode($showOtherEvents['event_id'])),
				                                    array(
				                                        'escape' => false
				                                )
				                        );

				                        ?>
				                        <div class="col-xs-12">
											<div class="box">
												<h4><?php echo $value['Event']['event_name']; ?></h4>
												<small><?php echo date('d F, Y', strtotime($showOtherEvents['from'])) ?></small>
											</div>
										</div>
									</div>
					<?php } } } } ?>
				</div>
			</div>
		</section>

	</div>
</div>

<script type="text/html" id="cityDataEvent">
    <select name="city" class="form-control search_address" id="valid-cities">
        <option value="">Select</option>
        <% _.each(data, function(value, key){ %>
            <option value="<%= value.id %>"><%= value.name %></option>
        <% }); %>
    </select>
</script>
