<?php

App::uses('AppHelper', 'View/Helper');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class FrontHelper extends AppHelper {

/**
 * Method activeInactive to get the active or inactive class
 *
 * @param $isActivated int is_actiavted field value
 * @return array $activeInactive contains the class and title to return
 */
    public function getEventAddressDetails ($eventId = null, $eventAddressId = null) {
        App::import('Model', 'EventAddress');
        $eventAddress = new EventAddress();
        $data = $eventAddress->find('first', 
                                            array(
                                                'fields' => array(
                                                            'state_id',
                                                            'city_id',
                                                            'address'
                                                    ),
                                                'conditions' => 
                                                            array(
                                                                'EventAddress.event_id' => $eventId,
                                                                'EventAddress.id' => $eventAddressId
                                                            )

                                                )
                                    );
        return $data;
    }

    public function getAccounts ($user_id = null,$is_activated = null,$param = null) {

        App::import('Model', 'WithdrawalSources');
        $withdrawalSources = new WithdrawalSources();
        $data = $withdrawalSources->find($param, 
                                            array(
                                                'fields' => array(
                                                            'account_number',
                                                            'ifsc_code',
                                                            
                                                    ),
                                                'conditions' => 
                                                            array(
                                                                
                                                                'WithdrawalSources.user_id' => $user_id,
                                                                'WithdrawalSources.is_activated' => $is_activated
                                                            )

                                                )
                                    );
        
        return $data;
    }

    public function getStateName ($state_id = null) {
        App::import('Model', 'State');
        $stateName = new State();
        $data = $stateName->find('first', 
                                            array(
                                                'fields' => array(
                                                            'name',
                                                            
                                                    ),
                                                'conditions' => 
                                                            array(
                                                                
                                                                'State.id' => $state_id
                                                            )

                                                )
                                    );
        return $data;
    }

    public function getCityName ($city_id = null) {
        App::import('Model', 'City');
        $cityName = new City();
        $data = $cityName->find('first', 
                                            array(
                                                'fields' => array(
                                                            'name',
                                                            
                                                    ),
                                                'conditions' => 
                                                            array(
                                                                
                                                                'City.id' => $city_id
                                                            )

                                                )
                                    );
        return $data;
    }

    public function getOwnerDetails ($space_user_id = null) {
        App::import('Model', 'UserProfile');
        $user = new UserProfile();
        $data = $user->find('first', 
                                    array(
                                    
                                        'conditions' => 
                                                    array(
                                                        
                                                        'UserProfile.user_id' => $space_user_id
                                                    )

                                        )
                            );
        return $data;
    }

    

    public function getLastInstallmentAmoutTax($amount = null, $paidBy = null, $commissionPercentage = null, $taxPercentage, $taxBySimplyPark = null, $taxByowner = null) {

        $spaceCommissionAmount = round(($commissionPercentage / 100) * $amount,2);

        $spaceTaxAmount = round(($taxPercentage / 100) * $amount,2);

        $internetHandlingCharges = '';
        $serviceTaxByOwner = '';
        $serviceTaxBySimplyPark = '';
        $ownerIncome = '';

        if ($paidBy == Configure::read('CommissionPaidBy.Driver')) {
            //$amount += $spaceCommissionAmount;
            $ownerIncome = $amount;

            $internetHandlingCharges += $spaceCommissionAmount;

            if ($taxByowner) {
                $serviceTaxByOwner = $spaceTaxAmount;
                $internetHandlingCharges += $serviceTaxByOwner;
            }

            if ($taxBySimplyPark) {
                $serviceTaxBySimplyPark = round(($taxPercentage / 100) * $spaceCommissionAmount,2);
                $internetHandlingCharges += $serviceTaxBySimplyPark;
            }

        } else { // in case of owner pays the commission
            $ownerIncome = $amount - $spaceCommissionAmount; //decrease space commssion amount
            $ownerIncome = $ownerIncome - $spaceTaxAmount; //decrease space tax amount

            if ($taxByowner) {
                $serviceTaxByOwner = $spaceTaxAmount;
                //$amount += $serviceTaxByOwner;

                $internetHandlingCharges += $serviceTaxByOwner;
            }

        }

        $amount += $internetHandlingCharges;

        $simplyParkIncome = $spaceCommissionAmount;
        
        $retrnArr = array(
                        'amount' => $amount,
                        'commissionAmount' => $spaceCommissionAmount,
                        'taxAmount' => $spaceTaxAmount,
                        'handlingCharges' => $internetHandlingCharges,
                        'taxByOwner' => $serviceTaxByOwner,
                        'taxBySimplyPark' => $serviceTaxBySimplyPark,
                        'ownerIncome' => $ownerIncome,
                        'simplyParkIncome' => $simplyParkIncome
                    );
        return $retrnArr;
    }

    public function dateDiff($startDate,$endDate) {

        $start_date = new DateTime($startDate);
        $end_date = new DateTime($endDate);   
        $interval = $start_date->diff($end_date);
        $dateString = '';
        $doPlural = function($nb,$str){return $nb>1?$str.'s':$str;}; 
        if ($interval->format('%y') != 0) {
            
            $dateString = $dateString.$interval->format('%y').$doPlural($interval->format('%y'), "year");

        } 
        if ($interval->format('%m') != 0) {
            
           $dateString = $dateString.' '.$interval->format('%m').$doPlural($interval->format('%m'), "month");
        }
        if ($interval->format('%d') != 0) {
           
           $dateString = $dateString.' '.$interval->format('%d').$doPlural($interval->format('%d'), "day"); 
            
        }
        if ($interval->format('%h') != 0) {

           $dateString = $dateString.' '.$interval->format('%h').$doPlural($interval->format('%h'), "hour");
            
        }
        if ($interval->format('%i') != 0) {

           $dateString = $dateString.$interval->format('%i').$doPlural($interval->format('%i'), "minute");
            
        }
        return $dateString;

    }
}
