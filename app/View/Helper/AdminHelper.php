<?php

App::uses('AppHelper', 'View/Helper');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class AdminHelper extends AppHelper {

/**
 * Method activeInactive to get the active or inactive class
 *
 * @param $isActivated int is_actiavted field value
 * @return array $activeInactive contains the class and title to return
 */
    public function activeInactiveIconValues($isActivated = null) {

        $activeInactive['Class'] = $isActivated == Configure::read('Bollean.False') ? 'ok' : 'remove';
        $activeInactive['Title'] = $isActivated == Configure::read('Bollean.False') ? 'Activate' : 'Inactivate';
        $activeInactive['PopupID'] = $isActivated == Configure::read('Bollean.False') ? '#activeConfirm' : '#inactiveConfirm';
        return $activeInactive;
    }

/**
 * Method delete to get the active or inactive class
 *
 * @param $isDeleted int is_deleted field value
 * @return array $deleteRestoreArray contains the class and title to return
 */
    public function deleteRestoreIconValues($isDeleted = null) {

        $deleteRestoreArray['Class'] = $isDeleted == Configure::read('Bollean.False') ? 'trash' : 'refresh';
        $deleteRestoreArray['Title'] = $isDeleted == Configure::read('Bollean.False') ? 'Delete' : 'Restore';
        $deleteRestoreArray['PopupID'] = $isDeleted == Configure::read('Bollean.False') ? '#deleteConfirm' : '#restoreConfirm';
        return $deleteRestoreArray;
    }

/**
 * Method manageSettingsCollapse to check that the one of the manage settings tab is open or not.
 *
 * @return string $className name fo the bootstrap class which keep the tab open
 */
    public function manageSettingsTabCollapsing() {
        $controllerName = $this->params->params['controller'];
        $className = $controllerName == 'AdminSettings' ? 'in' : 'collapse';
        return $className;
    }

/**
 * Method manageSettingsCollapse to check that the one of the manage settings tab is open or not.
 *
 * $action string name of the action
 * @return string $className name fo the bootstrap class which keep the inner tab icon open
 */
    public function manageSettingsInnerTabCollapsing($action = null) {
        $controllerName = $this->params->params['controller'];
        $actionName = $this->params->params['action'];
        $className = ($controllerName == 'AdminSettings' && $actionName == $action) ? '' : 'icon-angle-down';
        return $className;
    }

/**
 * Method manageSpaceApproveDisapproveColor to check the space is approved or disapprove
 *
 * $isApproved int the is approved space status of admin
 * @return string $class for approve or disapprove rows
 */
    public function manageSpaceApproveDisapproveColor($isApproved = null) {
        $class = null;
        if ($isApproved == Configure::read('SpaceStatus.NotApproved')) {
            $class = 'space-row-color-not-approve';
        } elseif ($isApproved == Configure::read('SpaceStatus.Approved')) {
            $class = 'space-row-color-approve';
        }
        return $class;
    }
}
