<section class="col-xs-12 col-md-9 right-panel">
					
	<div class="heading clearfix">
		<span class="pull-left title"><?php echo __('Summary'); ?></span>
	</div>
	<div class="summary-box text-center">
		<div class="detail-box">
			<div class="red-bg">
				<i class="fa fa-bookmark-o fa-fw"></i>
				<span><?php echo $getNewBookings; ?></span>
				<div><?php echo __('New Booking'); ?></div>
			</div>
		</div>
		<div class="detail-box">
			<div class="green-bg">
				<i class="fa fa-car fa-fw"></i>
				<span><?php echo $getActiveParkings; ?></span>
				<div><?php echo __('Active Parking Space'); ?></div>
			</div>
		</div>
		<div class="detail-box">
			<div class="yellow-bg">
				<i class="fa fa-user fa-fw"></i>
				<span><?php echo $profilePercentage; ?><span class="percentage">%</span></span>
				<div><?php echo __('Profile Completed'); ?></div>
			</div>
		</div>
		<div class="detail-box">
			<div class="purple-bg">
				<!-- <i class="fa fa-check fa-fw"></i> -->
				<i class="fa indian-rupee">&#8377;</i>
				<span><?php echo number_format($confirmedAndPaidAmount,2); ?></span>
				<div><?php echo __('Total Confirmed &amp; Paid'); ?></div>
			</div>
		</div>
		
		<div class="detail-box">
			<div class="pink-bg">
				<i class="fa indian-rupee">&#8377;</i>
				<span><?php echo number_format($getOwnerIncome,2); ?></span>
				<div><?php echo __('Owner Income'); ?></div>
			</div>
		</div>
		</div>

		<div class="heading clearfix primary">
			<span class="title-sm"><?php echo __('Summary'); ?></span>
			<div class="pull-right">
			<?php 
				echo $this->Html->link(
												'Under Approval Bookings <span class="badge">'.$getUnderApprovalBookings.'</span>',
												'/bookings/listing?booking-tab=2',
												array(
														'escape' => false
													)
											);
			?>
				

		</div>
	</div>
	<div class="table-responsive">
		<table class="table summary-tb">
			<thead>
				<tr>
					<th><?php echo __('Booking ID'); ?></th>
					<th><?php echo __('Parking Details'); ?></th>
					<th><?php echo __(''); ?></th>
					<th><?php echo __('Booking Status'); ?></th>
					<th colspan="2"><?php echo __('Amount'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach ($getBookingsSummary as $showBookedSummary) {
							
						
				?>
						<tr>
							<td><?php echo str_replace('pay_', 'SP_', $showBookedSummary['Transaction']['payment_id']); ?></td>
							<td>
								<span><?php echo $showBookedSummary['Space']['name']; ?></span>
								<br>
								<?php echo date('d M,y H:i A', strtotime($showBookedSummary['Booking']['start_date'])) . ' - ' . date('d M,y H:i A', strtotime($showBookedSummary['Booking']['end_date'])); ?>
							</td>
							<td>
								<?php 
									echo $showBookedSummary['Booking']['user_id'] == CakeSession::read('Auth.User.id') ? 'Booking Made':'Booking Received'; 
								?>
							</td>
							<td>
								<?php
									switch ($showBookedSummary['Booking']['status']) {
										case Configure::read('BookingStatus.Waiting'):
											echo 'Waiting';
											break;
										
										case Configure::read('BookingStatus.Approved'):
											echo 'Approved';
											break;

										case Configure::read('BookingStatus.DisApproved'):
											echo 'Disapproved';
											break;

										case Configure::read('BookingStatus.CancellationRequested'):
											echo 'Requested for cancellation';
											break;

										case Configure::read('BookingStatus.Cancelled'):
											echo 'Cancelled';
											break;

										case Configure::read('BookingStatus.Expired'):
											echo 'Expired';
											break;
										case Configure::read('BookingStatus.CancelledThroughPaymentDefault'):
											echo 'Cancellation through payement.';
											break;	
							
									}
								 ?>
							</td>
							<td>
								<?php 
									echo $showBookedSummary['Booking']['user_id'] == CakeSession::read('Auth.User.id') ? 
										$showBookedSummary[0]['booking_made_amount'] : 
										$showBookedSummary[0]['booking_received_amount']; 
								?>
							</td>
							<td>
								
									<?php
										$currDate = date('Y-m-d H:i:s');
										// if( ( ($showBookedSummary['Booking']['status'] != Configure::read('BookingStatus.Cancelled') || $showBookedSummary['Booking']['status'] != Configure::read('BookingStatus.DisApproved')) && $showBookedSummary['Booking']['start_date'] >= $currDate ) || (($showBookedSummary['Booking']['status'] != Configure::read('BookingStatus.Cancelled') || $showBookedSummary['Booking']['status'] != Configure::read('BookingStatus.DisApproved')) && $showBookedSummary['Booking']['start_date'] <= $currDate && $showBookedSummary['Booking']['end_date'] >= $currDate) ) {

										if($showBookedSummary['Booking']['status'] == Configure::read('BookingStatus.Approved') && ($showBookedSummary['Booking']['start_date'] >= $currDate || ($showBookedSummary['Booking']['start_date'] <= $currDate && $showBookedSummary['Booking']['end_date'] >= $currDate ))) {
 											
											echo $this->Html->link(
													__('Print'),
													'print_ticket/'.urlencode(base64_encode($showBookedSummary['Booking']['id'])),
													array(
															'escape' => false
														)
												);
										}
									?>
								
							</td>
						</tr>
				<?php } ?>
				<?php if (!empty($getBookingsSummary)) { ?>
						<tr>
							<td colspan="6" class="text-center">
								<?php
									echo $this->Html->link(
											__('Show All'),
											'/bookings/listing',
											array(
													'escape' => false
												)
										);
								?>
							</td>
						</tr>
				<?php } else { ?>
						<tr>
							<td colspan="6" class="text-center">
								<?php
									echo __('Currently, there are no bookings to show.');
								?>
							</td>
						</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<?php if (!empty($getCurrentSpaceStatus)) {?>
		<div class="heading clearfix primary">
			<span class="title-sm"><?php echo __('Current Status of my spaces'); ?></span>
		</div>
		<div class="table-responsive">
			<table class="table summary-tb">
				<thead>
					<tr>
						<th><?php echo __('Space Name'); ?></th>
						<th><?php echo __('Slots'); ?></th>
						<th><?php echo __('Status'); ?></th>
						<th><?php echo __('Availability Dates'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
						
							foreach ($getCurrentSpaceStatus as $showCurrentBookedSpaces) {
					?>
							<tr>

								<td><?php echo $showCurrentBookedSpaces['Space']['name']; ?></td>
								<td><?php echo $showCurrentBookedSpaces['Space']['number_slots']; ?></td>
								<td><?php echo $status = $showCurrentBookedSpaces['Space']['is_activated'] == 1 ? 'Active' : 'Not Active'; ?></td>
								<td><?php echo date('d M, Y H:i A', strtotime($showCurrentBookedSpaces['SpaceTimeDay']['from_date'])) . ' - ' . date('d M, Y H:i A', strtotime($showCurrentBookedSpaces['SpaceTimeDay']['to_date'])); ?></td> 
							</tr>
					<?php }
						
					?>
						
					
				</tbody>
			</table>
		</div>
	<?php } ?>
	<article class="custom-msg clearfix">
		<div class="col-sm-8">
			<?php echo __('Have your own Parking place.'); ?><br>
			<small><?php echo __('Learn how to make money from it.'); ?></small>
		</div>
		<div class="col-sm-4">
			<?php
				echo $this->Html->link(
						__('add space'),
						'/users/addSpace',
						array(
								'class' => 'pull-right btn btn-info text-uppercase',
								'escape' => false
							)
					);
			?>
		</div>
	</article>

	<div class="satisfaction-box clearfix">
		<div class="col-sm-4">
			<?php 
				echo $this->Html->image('satisfaction.png',
					array(
						    'alt' => 'satisfaction',
						    'class' => 'img-responsive'
						)
				);
			?>
		</div>
		<div class="col-sm-8 satisfaction-icon">
			<h2><?php echo __('Simply Park'); ?></h2>
			<p>
				<?php echo __('All parking are covered under SimplyPark Promise.'); ?><br>
				<?php echo __('Click here to understand how you covered.'); ?>
			</p>
		</div>
	</div>
</section>
