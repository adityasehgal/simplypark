// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

  var latLngStr = null;
  var allMarkers = [];
  var countryRestrict = {'country': 'IN'};
  var currentPosition;
  var currentLatitude;
  var currentLongitude;
  var destinationLatitude;
  var destinationLongitude;
  
var app = angular.module('Simplypark-User', ['ionic','LocalStorageModule','ngCordova','ksSwiper'])
.run(function($ionicPlatform,GoogleMaps, $ionicPopup) {
  $ionicPlatform.ready(function() {
  
   if(window.Connection) {
                if(navigator.connection.type == Connection.NONE) {
                    $ionicPopup.alert({
                        title: "Internet Disconnected",
                        content: "The internet is disconnected on your device, please connect to internet and relaunch the app."
                    })
                    .then(function(result) {
                        
                            ionic.Platform.exitApp();
                        
                    });
				}
			}
  
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
	
	cordova.plugins.locationAccuracy.canRequest(function(canRequest){
    if(canRequest){
        cordova.plugins.locationAccuracy.request(function (success){
            console.log("Successfully requested accuracy: "+success.message);
			//alert("Successfully requested accuracy:"+JSON.stringify(success));
			if(success.code==1){
				document.location.href = "index.html";
			}
			//$window.location.reload(true);
			}, function (error){
//				alert("Accuracy request failed: error code="+error.code+"; error message="+JSON.stringify(error));
				if(error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED){
					
					if(window.confirm("Failed to automatically set Location Mode to 'High Accuracy'. Would you like to switch to the Location Settings page and do this manually?")){
						cordova.plugins.diagnostic.switchToLocationSettings();
					}
				}else{
					  $ionicPopup.alert({
                        title: "Location Disabled",
                        content: "The Location accuracy setting is disabled on your device."
                    })
                    .then(function(result) {
                        if(!result) {
                            ionic.Platform.exitApp();
                        }else{ionic.Platform.exitApp();}
                    });
					}
			}, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);
		}
	});
	
	
	GoogleMaps.init();
  });
})

app.config(function (localStorageServiceProvider) {
    localStorageServiceProvider
      .setPrefix('scotch-todo');
  });

app.config(function($stateProvider, $urlRouterProvider) {
 
  $stateProvider
  .state('map', {
    url: '/',
    templateUrl: 'templates/map.html',
    controller: 'MapCtrl'
  })
  .state('Events', {
    url: '/Events',
    templateUrl: 'templates/event.html',
	controller: 'EventCtrl'
  })
    .state('Spacedetails', {
    url: '/Spacedetails',
    templateUrl: 'templates/SpaceDetails.html',
	controller:'SpaceCtrl'
  })

      .state('Profile', {
    url: '/Profile',
    templateUrl: 'templates/UserProfile.html',
	controller:'UserProfileCtrl'
  })

    .state('Offers', {
    url: '/Offers',
    templateUrl: 'templates/offers.html'
  });
  
  
  $urlRouterProvider.otherwise("/");
})




app.directive('testDirective', ['$compile', function($compile) {
        return {
          restrict: 'E',
          template: "{{innerHtml}}",
          link: function (scope, element, attr) {
            element.append($compile("<span ng-bind=\"location.name\" id=\"location\" ></span>")(scope));
          }
        }
      }]);

app.directive('disableTap', function($timeout) {
  return {
    link: function() {

      $timeout(function() {
        document.querySelector('.pac-container').setAttribute('data-tap-disabled', 'true')
      },500);
    }
  };
});

function eventFire(el, etype){
 
  if (el.fireEvent) {
  
    el.fireEvent('on' + etype);
  } else {
    var evObj = document.createEvent('MouseEvents');
	
    evObj.initMouseEvent('click', true, false, window, 0, 0, 0, 80, 20, false, false, false, false, 0, null);
	
    el.dispatchEvent(evObj);
  }
}

