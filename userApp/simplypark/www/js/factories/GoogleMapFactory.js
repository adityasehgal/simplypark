/**/
app.factory('GoogleMaps', function($cordovaGeolocation, Markers, $ionicLoading){
 
  var apiKey = false;
  var map = null;
  function initMap(){

    var options = {timeout: 10000, enableHighAccuracy: true};
 
    $cordovaGeolocation.getCurrentPosition(options).then(function(position){
 
      var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
	
	  currentPosition = position;
	  latLngStr = position.coords.latitude+","+ position.coords.longitude;
	  currentLatitude =position.coords.latitude;
	  currentLongitude = position.coords.longitude;
	  targetLatitude =position.coords.latitude;
	  targetLongitude = position.coords.longitude;
      var mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
//	console.log("map_1 defined");
    map_1 = new google.maps.Map(document.getElementById("map"), mapOptions);
	var geocoder = new google.maps.Geocoder();
	document.getElementById('CurSubmit').addEventListener('click', function() {
          focusCurrentLocation(geocoder,map_1);
        });
		document.getElementById('Submit').addEventListener('click', function() {
          geocodeAddress(geocoder, map_1);

        });
		  FOOTER = document.getElementById("footer");
		  
		location.name="Select Your parking..";
		FOOTER.innerHTML = "<span id=\"location\">"+location.name+"</span>";
		GoogleMapServiceObj.data.spaceDetails.footerDescription = location.name;
		
      //Wait until the map is loaded
      google.maps.event.addListenerOnce(map_1, 'idle', function(){
 	  $ionicLoading.hide();
		  focusCurrentLocation(geocoder,map_1); 
      });
 
    }, function(error){
      console.log("Could not get location");
 
        //Load the markers
        loadMarkers();
    });
 

  }
 
 // When the user selects a city, get the place details for the city and
      // zoom the map in on the city.
      function onPlaceChanged() {
			
        var place = autocomplete.getPlace();
        if (place.geometry) {
          map_1.panTo(place.geometry.location);
          map_1.setZoom(15);
		var geocoder = new google.maps.Geocoder();
		geocodeAddress(geocoder, map_1);          
		 // search();
			GoogleMapServiceObj.data.IsVisible=false;
			GoogleMapServiceObj.clearValues();
		location.name="Select Your parking..";
		FOOTER.innerHTML = "<span id=\"location\">"+location.name+"</span>";
		GoogleMapServiceObj.data.spaceDetails.footerDescription = location.name;
		document.getElementById("footer_BTN").style.display="none";

        } else {
          document.getElementById('address').placeholder = 'Enter a Place';
        }
      }
 
  function loadMarkers(){
 
      //Get all of the markers from our Markers factory
      Markers.getMarkers(targetLatitude,targetLongitude).then(function(markers){
		//alert(markersResult);
		//var markers = markersResult.results;
		
        console.log("Markers: ", JSON.stringify(markers));
		var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
 
 
        for (var i = 0; i < markers.length; i++) {
			
          var record = markers[i];   
		  
  
          var markerPos = new google.maps.LatLng(record.lat, record.lng);
		var markerIcon = {
						  url: 'img/unselected_map_marker.png',
						  scaledSize: new google.maps.Size(45, 50),
						  origin: new google.maps.Point(0, 0),
						  anchor: new google.maps.Point(32,65),
						  labelOrigin: new google.maps.Point(25,20)
						};
			lableString = i+'';
          // Add the markerto the map
          var marker = new google.maps.Marker({
              map: map_1,
			  label: {
						text: lableString, 
						fontFamily: 'Roboto, Arial, sans-serif, custom-label-'+labels[i % labels.length], 
						fontSize: '10', 
						color: '#eb3a44', 
						fontWeight: 'bold',
						align: 'top'
						},
              animation: google.maps.Animation.DROP,
              position: markerPos,
			  icon:markerIcon
          });
		  allMarkers.push(marker);
          var infoWindowContent = "<h4>" + record.name + "</h4><h5>Available Slots: "+record.availableSlots+"</h5>";          
		  location.name=record.name;
          addInfoWindow(marker, infoWindowContent, record);
 
        }
 
      },function(error){
    // exceptions in transformData, or saveToIndexDB
    // will result in this error callback being called.
	
  }); 
 
  }
 var prev_infowindow =false; 
 
  function addInfoWindow(marker, message, record) {
		
      var infoWindow = new google.maps.InfoWindow({
          content: message
      });
 
      google.maps.event.addListener(marker, 'click', function () {
		if( prev_infowindow ) {
           prev_infowindow.close();
        }
		  prev_infowindow = infoWindow;
          infoWindow.open(map, marker);
		 destinationLatitude =record.lat;
		 destinationLongitude =record.lng;
		  FOOTER = document.getElementById("footer");
		  GoogleMapServiceObj.bindValues(record);
		  FOOTER.innerHTML = "<span id=\"location\">"+record.name+ ", Available Slots "+record.availableSlots+"</span><ion-item class=\"item-icon-right\"> </ion-item>";
		  	  document.getElementById("footer_BTN").style.display="block";
			  if(record.encoded_id==''||typeof  record.encoded_id=='undefined'||!record.encoded_id){
				document.getElementById("book_button").style.display="none";
			  }else{
				document.getElementById("book_button").style.display="";
			  }

      });
 
  }
		function focusCurrentLocation(geocoder, resultsMap){
		  deleteMarkers();
		  latLngStr=currentLatitude+","+currentLongitude;
		  var currentLatLng = new google.maps.LatLng(currentLatitude,currentLongitude);
	      resultsMap.setCenter(currentLatLng);
 	      var address = geocodeLatLng(geocoder, resultsMap, currentLatLng);
	  
		  if (address == null){
			address = "You";
		  }
		  var marker = new google.maps.Marker({
			  map: resultsMap,
			  animation: google.maps.Animation.DROP,
			  position: currentLatLng,
			  title:address
		  });
			allMarkers.push(marker);
			var infoWindowContent = "<h4>"+address+"</h4>";          
			addInfoWindow(marker, infoWindowContent, null);
			//Load the markers
			loadMarkers();
  
		}
        function geocodeAddress(geocoder, resultsMap) {
        var address = document.getElementById('address').value;
		
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
		    deleteMarkers();
			resultsMap.setCenter(results[0].geometry.location);
			var newLocation=results[0].geometry.location.lat()+","+results[0].geometry.location.lng();
			latLngStr=newLocation;
			targetLatitude =  results[0].geometry.location.lat();
			targetLongitude = results[0].geometry.location.lng();
            var marker = new google.maps.Marker({
              map: resultsMap,
              position: results[0].geometry.location
            });
			allMarkers.push(marker);
			var infoWindowContent = "<h4>"+address+"</h4>";          
			
			addInfoWindow(marker, infoWindowContent, results[0]);
			
			loadMarkers();
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
	  /*This function is used to get the address of a location based on it's Latitude and Longitude.*/
	  function geocodeLatLng(geocoder, map, latlng) {
		var address = null;
		 var inputAddress = document.getElementById('address');
		geocoder.geocode({ 'location': latlng }, function (results, status) {
			if (status === google.maps.GeocoderStatus.OK) {
				if (results[0]) {
		/*			map.setZoom(11);
					var marker = new google.maps.Marker({
						position: latlng,
						map: map
					});
					infowindow.setContent(results[0].formatted_address);
					infowindow.open(map, marker);
					*/
					address = results[0].formatted_address;
					address_components = results[0].address_components;
					
					for(var i =0; i < address_components.length; i++){
						  types = address_components[i].types;
					  	  for (var typeCount = 0;typeCount <types.length;typeCount++){
							if (types[typeCount] == "country"){
								countryRestrict = {'country': address_components[i].short_name};
								 autocomplete = new google.maps.places.Autocomplete(
											/** @type {!HTMLInputElement} */ (
											
												document.getElementById('address')), {
											  types: ['address']
											  ,componentRestrictions: countryRestrict
											});
								//        places = new google.maps.places.PlacesService(map_1);

										autocomplete.addListener('place_changed', onPlaceChanged);
 								
						  }
						}
					}
					
					inputAddress.value= address;
					//GoogleMapServiceObj.data.searchAddress= address;
				}else{
					alert(result[0]);
				}				
			}else{
					alert(status);
				} 
			
		});
		return address;
		}

 
	  // Sets the map on all markers in the array.
      function setMapOnAll(map) {
        for (var i = 0; i < allMarkers.length; i++) {
          allMarkers[i].setMap(map);
        }
      }

      // Removes the markers from the map, but keeps them in the array.
      function clearMarkers() {
        setMapOnAll(null);
      }

      // Shows any markers currently in the array.
      function showMarkers() {
        setMapOnAll(map_1);
      }

      // Deletes all markers in the array by removing references to them.
      function deleteMarkers() {
        clearMarkers();
        allMarkers = [];
      }
	

	
	GoogleMapServiceObj ={
    init: function(){
      initMap();
    },

	
	getGeocodeAddress : function(geocode,map){
		geocodeAddress(geocode,map);
	},
	 setTrue : function() {
      GoogleMapServiceObj.data.IsVisible = true;
    },

	data:{IsVisible: false,searchAddress:"",spaceDetails: {isSpaceSelected:false,SpacedetailsPage:'', spaceName:'', spaceLatitude:'', spaceLongitude:'',destinationLatitude:'',destinationLongitude:'',currentLatitude:'',currentLongitude:'',spaceicon:'',footerDescription:'',availableSlots:'',charges:'',encoded_id:''}},

	bindValues : function(record){
		  GoogleMapServiceObj.data.spaceDetails.SpacedetailsPage='Spacedetails';
		  GoogleMapServiceObj.data.spaceDetails.isSpaceSelected=true;
		  GoogleMapServiceObj.data.spaceDetails.spaceName = record.name;
		  GoogleMapServiceObj.data.spaceDetails.spaceLatitude = record.lat;
		  GoogleMapServiceObj.data.spaceDetails.spaceLongitude = record.lng;
		  GoogleMapServiceObj.data.spaceDetails.spaceicon = record.spaceicon;
		  GoogleMapServiceObj.data.spaceDetails.destinationLatitude =record.lat;
		  GoogleMapServiceObj.data.spaceDetails.destinationLongitude =record.lng;	
		  GoogleMapServiceObj.data.spaceDetails.currentLatitude= currentLatitude;
		  GoogleMapServiceObj.data.spaceDetails.currentLongitude =currentLongitude;
		  GoogleMapServiceObj.data.spaceDetails.footerDescription = record.name;
		  GoogleMapServiceObj.data.spaceDetails.availableSlots = record.availableSlots;
		  GoogleMapServiceObj.data.spaceDetails.charges = record.charges;
		  GoogleMapServiceObj.data.spaceDetails.encoded_id = record.encoded_id;
		},
		
		clearValues: function(){
		  GoogleMapServiceObj.data.spaceDetails.SpacedetailsPage='';
		  GoogleMapServiceObj.data.spaceDetails.isSpaceSelected=false;
		  GoogleMapServiceObj.data.spaceDetails.spaceName = '';
		  GoogleMapServiceObj.data.spaceDetails.spaceLatitude = '';  
		  GoogleMapServiceObj.data.spaceDetails.spaceLongitude = '';  
		  GoogleMapServiceObj.data.spaceDetails.destinationLatitude='';
		  GoogleMapServiceObj.data.spaceDetails.destinationLongitude='';
		  GoogleMapServiceObj.data.spaceDetails.currentLatitude='';
		  GoogleMapServiceObj.data.spaceDetails.currentLongitude='';
		  GoogleMapServiceObj.data.spaceDetails.spaceicon = '';  
		  GoogleMapServiceObj.data.spaceDetails.footerDescription='Select Your Parking';
		  GoogleMapServiceObj.data.spaceDetails.availableSlots = '';
		  GoogleMapServiceObj.data.spaceDetails.charges = '';
		  GoogleMapServiceObj.data.spaceDetails.encoded_id = '';
		}

	
  }
 
  return GoogleMapServiceObj;
 
})
