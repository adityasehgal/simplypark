app.factory('Space', function() {
 
  space = {};
  space.data = {};
  space.data.spaceName = '';
  space.data.spaceLatitude = '';  
  space.data.spaceLongitude = '';  
  space.data.destinationLatitude='';
  space.data.destinationLongitude='';
  space.data.currentLatitude='';
  space.data.currentLongitude='';
  space.data.spaceicon = '';  
  space.data.footerDescription
  space.data.availableSlots = '';
  space.data.charges = '';
  return space;
});