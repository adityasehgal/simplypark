app.factory('Markers', function($http,$q) {
 
 var markers=[];
 //var base_path='http://meddealsindia.com/api/?service=1&subject=parking&loc='+latLngStr;
 //var base_path='http://127.0.0.1:81/simplypark/spaces/listNearBySpacesByLatLong/';
 var base_path='https://simplypark.in/spaces/listNearBySpacesByLatLong/';
  return {
	
	getDistance: function(){
		return $http.get(base_path).then(function(response){
		//the response from the server is now contained in 'response'
		//markers=response;
		  locations = response.data.results;
		  console.log(JSON.stringify(locations));
		  var jsonArr = [];
		  for (i in locations) {
				 latStr = locations[i].geometry.location.lat;
				 lngStr = locations[i].geometry.location.lng;				 
				 jsonArr.push({
					lat: latStr,
					lng: lngStr,
					name:locations[i].vicinity,
					spaceicon:locations[i].icon
				});
			}
			markersFinal = {mark:jsonArr}; 
			
			return markersFinal.mark;

			}, function(error){
				//there was an error fetching from the server
				$q.reject(response.data);	
			});
	},
  
    getMarkers: function(lat,lng){
	return $http.get(base_path+lat+"/"+lng+".json").then(function(response){
    //the response from the server is now contained in 'response'
	 //markers=response;
	  locations = response.data.listNearBySpaces;
	  var jsonArr = [];
	  for (i in locations) {
			 latStr = locations[i].Space.lat;
			 lngStr = locations[i].Space.lng;
			 for(price in locations[i].SpacePricing){
				
			 }
			 jsonArr.push({
				lat: latStr,
				lng: lngStr,
				name:locations[i].Space.name,
				spaceicon:locations[i].Space.icon,
				address:locations[i].Space.address1,
				availableSlots:locations[i].Space.number_slots,
				charges:locations[i].SpacePricing,
				encoded_id:locations[i].Space.encoded_id
				
			});
		}
		markersFinal = {mark:jsonArr}; 
		
		return markersFinal.mark;

		}, function(error){
			//there was an error fetching from the server
			$q.reject(response.data);	
		});
    }
  }
})