app.controller('SpaceCtrl', function($scope,  $cordovaLaunchNavigator, GoogleMaps) {
  $scope.input = GoogleMaps.data.spaceDetails;
   $scope.images = [];

  this.init = function(){
	for(var i = 0; i < 10; i++) {
		
            $scope.images.push({id: i, src: $scope.input.spaceicon});
        }
	};
	this.init();
	 $scope.swiper = {};
 
    $scope.onReadySwiper = function (swiper) {
 
        swiper.on('slideChangeStart', function () {
            console.log('slide start');
        });
         
        swiper.on('onSlideChangeEnd', function () {
            console.log('slide end');
        });     
    };
$scope.launchNavigator = function() {
	        $scope.counter++;
	var destination = [$scope.InputControl.data.destinationLatitude, $scope.input.destinationLongitude];
	var start = [$scope.input.currentLatitude,$scope.input.currentLongitude];
	/*
	$cordovaLaunchNavigator.isAppAvailable($cordovaLaunchNavigator.APP.GOOGLE_MAPS, function(isAvailable){
	var appV;
    if(isAvailable){
		console.log("isAvailable"+isAvailable);
        appV = $cordovaLaunchNavigator.APP.GOOGLE_MAPS;
    }else{
        console.warn("Google Maps not available - falling back to user selection");
        appV = $cordovaLaunchNavigator.APP.USER_SELECT;
    }
	alert(appV);
    
	});
	*/
	$cordovaLaunchNavigator.navigate(destination, start).then(function() {
      console.log("Navigator launched");
    }, function (err) {
      console.error(err);
    }); 

  };  
});