package in.simplypark.simplypark_ee;

import android.app.IntentService;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;


import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.NetworkInfo;

import android.os.Bundle;



import android.widget.RemoteViews;
import android.content.Intent;
import android.app.PendingIntent;
//import android.util.Log;
import android.net.ConnectivityManager;

import com.google.android.gms.gcm.GcmListenerService;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


import com.google.android.gms.iid.InstanceIDListenerService;

import static android.appwidget.AppWidgetManager.EXTRA_APPWIDGET_ID;
import static android.appwidget.AppWidgetManager.EXTRA_CUSTOM_EXTRAS;
import static android.appwidget.AppWidgetManager.INVALID_APPWIDGET_ID;


/**
 * Implementation of App Widget functionality.
 * App Widget Configuration implemented in {@link EntryExitConfigureActivity EntryExitConfigureActivity}
 */
public class EntryExit extends AppWidgetProvider {


    private static final String ACTION_WIDGET_CONFIGURE = "CONFIGURE";
    private static final String ACTION_WIDGET_ENTRY = "ENTRY";
    private static final String ACTION_WIDGET_EXIT = "EXIT";
    private static final String ACTION_WIDGET_DELETE = "DELETE";
    private static final String ACTION_WIDGET_REFRESH_TOKEN = "REFRESH";
    private static final String ACTION_WIDGET_NOACTION = "NOACTION";
    private static final String INTENT_HANDLE_CONNECTIVITY_CHANGE = "CONNECTIVITYNOTIFICATION";

    private static final String offlineEntry = "OFFLINEENTRY";
    private static final String offlineExit  = "OFFLINEEXIT";
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static final String baseUrl = "https://www.simplypark.in/spaces/";
    //private static final String baseUrl = "http://172.17.20.152/simplypark/spaces/";
    private static final String updateController = "updateSlotOccupancy";
    private static final String registerController = "registerWidget";
    private static final String deleteController = "deRegisterWidget";
    private static final String refreshTokenController = "refreshToken";


    private static final String PREFS_NAME = "in.simplypark.simplypark_ee.EntryExit";
    private static final String PREF_PREFIX_KEY = "appwidget_";
    private static final String PREF_PREFIX_TOKEN_KEY = "_token";
    private static final String PREF_PREFIX_SPACEID_KEY = "_space";
    private static final String PREF_PREFIX_WIDGET_KEY ="_widget";
    private static final String PREF_PREFIX_SERVER_WIDGET_KEY ="_server";
    private static final String PREFS_NOT_FOUND = "";

    private static final String EXTRA_ACTION = "action";

    public static enum EntryExitAction {
        DEFAULT, //dont change DEFAULT's position
        ENTRY,
        EXIT,
        CONFIGURE,
        UPDATE,
        REGISTER,
        DELETE,
        REFRESH_TOKEN;

        private static EntryExitAction[] cachedValues = null;
        public static EntryExitAction fromInt(int i){
            if(EntryExitAction.cachedValues == null){
                EntryExitAction.cachedValues = EntryExitAction.values();
            }
            return EntryExitAction.cachedValues[i];
        }

    }



    /* v3 update : Based on user feedback, the widget update is quiet sluggish.
     * We are therefore going to increment the count ourselves locally and not wait
     * for the server response. When the server response does come, we will only update
     * the view if updateCount is 0
     */
    private static int     updateRequestCounter      = 0;
    private static int     currentSlotCounter = 0;
    private static boolean lastUpdateFailed   = false;


    static public void putSpaceIdPerWidgetId(Context context, int widgetId,String spaceId){
        putPersistentKeyValue(context,PREF_PREFIX_SPACEID_KEY+widgetId,spaceId);
    }

    static public String getSpaceId(Context context, int widgetId){
        return(getPersistentKeyValue(context,PREF_PREFIX_SPACEID_KEY+widgetId));
    }

    static public void putTokenPerWidgetId(Context context, int widgetId,String token){
        putPersistentKeyValue(context, PREF_PREFIX_TOKEN_KEY+widgetId,token);
    }

    static public String getToken(Context context, int widgetId){
        return(getPersistentKeyValue(context,PREF_PREFIX_TOKEN_KEY+widgetId));
    }
    
    static public void putServerWidgetIdPerWidgetId(Context context, int serverWidgetId,
                                                    int widgetId){
        putPersistentKeyValue(context, PREF_PREFIX_SERVER_WIDGET_KEY+widgetId,
                              Integer.toString(serverWidgetId));
    }

    static public String getServerWidgetIdPerWidgetId(Context context, int widgetId){
        return(getPersistentKeyValue(context,PREF_PREFIX_SERVER_WIDGET_KEY+widgetId));
    }

    static public void putIsConfiguredPerWidgetId(Context context, int widgetId,Boolean val){
        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        String key = PREF_PREFIX_WIDGET_KEY+widgetId;
        prefs.putBoolean(key, val);
        prefs.apply();
    }

    static public Boolean getIsWidgetConfigured(Context context, int widgetId){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        String key = PREF_PREFIX_WIDGET_KEY+widgetId;
        return (prefs.getBoolean(key, false));
    }


    static public void deletePreference(Context context, int widgetId){
        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.remove(PREF_PREFIX_TOKEN_KEY+widgetId);
        prefs.remove(PREF_PREFIX_SPACEID_KEY+widgetId);
        prefs.remove(PREF_PREFIX_WIDGET_KEY+widgetId);
        prefs.remove(PREF_PREFIX_SERVER_WIDGET_KEY+widgetId);
        prefs.apply();
    }

    // Write the prefix to the SharedPreferences object for this widget
    static public void putPersistentKeyValue(Context context, String key, String value) {
        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.putString(key, value);
        prefs.apply();
    }

    static public String getPersistentKeyValue(Context context, String key){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        return (prefs.getString(key, PREFS_NOT_FOUND));
    }

     static void deleteAppWidget(Context context, AppWidgetManager appWidgetManager,
                                  int appWidgetId) {

        //CharSequence widgetText = EntryExitConfigureActivity.loadTitlePref(context, appWidgetId);
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.entry_exit);
        //views.setTextViewText(R.id.spaceID, widgetText);

        //Log.v("updateAppWidget","spaceId ::" + spaceIdPerWidgetId.get(appWidgetId));


        // Build the intent to call the service
        Intent intent = new Intent(context.getApplicationContext(),
                LogEntryExitIntentService.class);

        intent.putExtra(EXTRA_APPWIDGET_ID, appWidgetId);
        intent.setAction(ACTION_WIDGET_DELETE);
        //Log.v("onUPdate", "about to start service");
        // Update the widgets via the service
        context.startService(intent);

        appWidgetManager.updateAppWidget(appWidgetId, views);


    }





    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        //CharSequence widgetText = EntryExitConfigureActivity.loadTitlePref(context, appWidgetId);
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.entry_exit);
        //views.setTextViewText(R.id.spaceID, widgetText);

        //Log.i("updateAppWidget","spaceId ::" + spaceIdPerWidgetId.get(appWidgetId));


         // Build the intent to call the service
        Intent intent = new Intent(context.getApplicationContext(),
                LogEntryExitIntentService.class);

        intent.putExtra(EXTRA_APPWIDGET_ID, appWidgetId);
        intent.setAction(ACTION_WIDGET_NOACTION);
        //Log.v("onUPdate","about to start service");
        // Update the widgets via the service
        context.startService(intent);



        appWidgetManager.updateAppWidget(appWidgetId, views);


    }





    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        //Log.i("PREFS","Inside onUpdate");
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            //Log.i("PREFS","Calling updateAppWidget for "+appWidgetId);
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }




    }




    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        // When the user deletes the widget, delete the preference associated with it.
        for (int appWidgetId : appWidgetIds) {
            EntryExitConfigureActivity.deleteTitlePref(context, appWidgetId);
            deleteAppWidget(context,appWidgetManager,appWidgetId);

        }
    }

    @Override
    public void onEnabled(Context context) {
        //Log.i("PREFS","Inside onEnabled");
        // There may be multiple widgets active, so update all of them
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        int appWidgetIds[] = appWidgetManager.getAppWidgetIds(new ComponentName(context,
                                                                        EntryExit.class));
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
        // Enter relevant functionality for when the first widget is created

    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    @Override
    public void onReceive(Context context,Intent intent) {
        // Enter relevant functionality for when the last widget is recieved
        final String action = intent.getAction();
        //Log.d(EntryExit.class.getSimpleName(), "action: " + intent.getAction());

        if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            //Log.v("onReceived....","CONNECTIVITY_ACTION");
            // save the connected state to get in onUpdate
            // update all widgets
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            int appWidgetIds[] = appWidgetManager.getAppWidgetIds(new ComponentName(context,
                                                                  EntryExit.class));
            onUpdate(context, appWidgetManager, appWidgetIds);
        } else {
            super.onReceive(context, intent);
        }

    }

    public static boolean isConnected(Context ctx){

        ConnectivityManager connMgr = (ConnectivityManager)ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

   public static void setViewsOnError(RemoteViews views,String errorString)
    {
            views.setFloat(R.id.count, "setTextSize", 15);
            //views.setTextColor(R.id.count,getResources().getColor(R.color.colorDanger));
            views.setTextViewText(R.id.count, errorString);
            views.setTextViewText(R.id.poweredBy, "");

            //set currentRequestCounter to 0 to force the view to update based on server
            //value next time
            updateRequestCounter = 0;
            lastUpdateFailed     = true;

    }

    public static void setViewsOnSuccess(RemoteViews views)
    {
            views.setFloat(R.id.count, "setTextSize", 54);
            views.setTextViewText(R.id.poweredBy, "powered by SimplyPark.in");
    }


    /*******************************************************
             Inner LogEntryExitIntent Service Class       *
    ********************************************************/
    public static class LogEntryExitIntentService extends IntentService {


        public LogEntryExitIntentService() {
            super("LogEntryExitIntentService");
        }

        @Override
        protected void onHandleIntent(Intent intent) {
            //Log.i("onHandleIntent", "inside onHandleINtent");


            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);

                //Log.v("onHandleIntent", "isConnected == TRUE");
             
               int incomingAppWidgetId = intent.getIntExtra(EXTRA_APPWIDGET_ID,INVALID_APPWIDGET_ID);

               String token = intent.getStringExtra(EXTRA_CUSTOM_EXTRAS);

               if(incomingAppWidgetId != INVALID_APPWIDGET_ID){
                  if(EntryExit.isConnected(this.getApplicationContext())){
                     //Log.i("LogEntryEx", intent.getAction());
                     updateOneAppWidget(appWidgetManager, incomingAppWidgetId, token,
                                        intent.getAction());
                  }else {
                      //Log.v("onHandleIntent", "isConnected == FALSE");
                      RemoteViews views = new RemoteViews(this.getPackageName(),
                                                          R.layout.entry_exit);

                      setViewsOnError(views," No network connection");
                      appWidgetManager.updateAppWidget(incomingAppWidgetId, views);
                  }
               }

        }

        private void updateOneAppWidget(AppWidgetManager appWidgetManager,
                                        int appWidgetId, String oldToken, String whichButton) {

            if(!getIsWidgetConfigured(getApplicationContext(),appWidgetId)){
                return;
            }

            RemoteViews views = new RemoteViews(this.getPackageName(),
                    R.layout.entry_exit);

            buttonIntent(views, appWidgetId, ACTION_WIDGET_ENTRY, R.id.entryButton);
            buttonIntent(views, appWidgetId, ACTION_WIDGET_EXIT, R.id.exitButton);

            disableButtons(views);
            appWidgetManager.partiallyUpdateAppWidget(appWidgetId, views);

            EntryExitAction action = EntryExitAction.DEFAULT;


            //Log.i("PERFS","Action is"+whichButton);
            //updateRequestCounter is ONLY incremented when Entry Exit buttons are pressed.
            //All other times updateRequestCounter would be 0

            if(whichButton.equals(ACTION_WIDGET_ENTRY)) {

                if(!lastUpdateFailed){
                    updateRequestCounter++;

                    setViewsOnSuccess(views);
                    views.setTextViewText(R.id.count, Integer.toString(++currentSlotCounter));
                    appWidgetManager.partiallyUpdateAppWidget(appWidgetId, views);

                }
                action = EntryExitAction.ENTRY;

            }else if(whichButton.equals(ACTION_WIDGET_EXIT)){


                if(!lastUpdateFailed){
                    updateRequestCounter++;
                    setViewsOnSuccess(views);
                    if(currentSlotCounter > 0) {
                        currentSlotCounter--;
                    }

                    views.setTextViewText(R.id.count, Integer.toString(currentSlotCounter));
                    appWidgetManager.partiallyUpdateAppWidget(appWidgetId, views);
                }
                action = EntryExitAction.EXIT;

            }else if(whichButton.equals(ACTION_WIDGET_DELETE)){
                //Log.v("deletion","delete");

                action = EntryExitAction.DELETE;
            }else if(whichButton.equals(ACTION_WIDGET_REFRESH_TOKEN)){
 

              InstanceID instanceId = InstanceID.getInstance(this);

              try {
                    putTokenPerWidgetId(getApplicationContext(), appWidgetId,
                            instanceId.getToken(getString(R.string.
                                            gcm_defaultSenderId),
                                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null));

              } catch (IOException ie) {
                  setViewsOnError(views, " Could not register with Google. Please re-install");
                  enableButtons(views);
                  appWidgetManager.updateAppWidget(appWidgetId,views);
                  return;

              }
                action = EntryExitAction.REFRESH_TOKEN;


            }
            else {


                  if(getToken(getApplicationContext(), appWidgetId).
                          equals(PREFS_NOT_FOUND)){

                    action = EntryExitAction.REGISTER;
                    InstanceID instanceId = InstanceID.getInstance(this);

                    try {
                        putTokenPerWidgetId(getApplicationContext(), appWidgetId,
                                instanceId.getToken(getString(R.string.
                                                gcm_defaultSenderId),
                                        GoogleCloudMessaging.INSTANCE_ID_SCOPE, null));
                        //Log.v("RegisterationToken", registerToken);
                    } catch (IOException ie) {
                        //Log.v("RegisterationToken", "Failed");
                        setViewsOnError(views, " Could not register with Google. Please re-install");
                        enableButtons(views);
                        appWidgetManager.updateAppWidget(appWidgetId,views);
                        return;

                    }
                }

            }
             enableButtons(views);

             /* v3: start the updateServer Service and to update the server in the background
              *     and locally update the views
              */
             Context context = getApplicationContext();

             Intent intent = new Intent(context,
                                      UpdateServerIntentService.class);

             intent.putExtra(EXTRA_APPWIDGET_ID,appWidgetId);
             intent.putExtra(EXTRA_ACTION, action.ordinal());
             intent.putExtra(EXTRA_CUSTOM_EXTRAS, oldToken);

             context.startService(intent);



            appWidgetManager.updateAppWidget(appWidgetId,views);
        }

        private void buttonIntent(RemoteViews views, int appWidgetId, String action, int viewId) {
            Intent buttonIntent = new Intent(this, this.getClass());
            buttonIntent.putExtra(EXTRA_APPWIDGET_ID, appWidgetId);
            buttonIntent.setAction(action);

            PendingIntent buttonPendingIntent = PendingIntent.getService(this,
                    0, buttonIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            views.setOnClickPendingIntent(viewId, buttonPendingIntent);
        }
        
        public void disableButtons(RemoteViews views)
        {
            views.setBoolean(R.id.entryButton, "setEnabled", false);
            views.setBoolean(R.id.exitButton, "setEnabled", false);
        }

        public void enableButtons(RemoteViews views)
        {
            views.setBoolean(R.id.entryButton ,"setEnabled", true);
            views.setBoolean(R.id.exitButton ,"setEnabled", true);
        }

    }

    /********************************************************************
     *    Inner Intent Service Class to update Server                   *
     *******************************************************************/
    public static class UpdateServerIntentService extends IntentService {
	    OkHttpClient client = new OkHttpClient();

	    public UpdateServerIntentService(){
		    super("UpdateServerIntentSerivce");
	    }

	    @Override
		    protected void onHandleIntent(Intent intent) {
			    //0 as default value corresponds to DEFAULT
			    final int    action      = intent.getIntExtra(EXTRA_ACTION,
                                                              EntryExitAction.DEFAULT.ordinal());

			    final int    appWidgetId = intent.getIntExtra(EXTRA_APPWIDGET_ID,INVALID_APPWIDGET_ID);
			    final String token       = intent.getStringExtra(EXTRA_CUSTOM_EXTRAS);

                RemoteViews views = new RemoteViews(getApplicationContext().getPackageName(),
                                                        R.layout.entry_exit);

                updateServer(views, EntryExitAction.fromInt(action), appWidgetId, token);

		    }

	    private void updateServer(RemoteViews views,EntryExitAction action, int appWidgetId,
                                          String oldToken)
	    {
            try {

                 Request request;

                switch(action)
                {
                    case REGISTER:
                        {
                            request = buildRegisterRequest(appWidgetId);
                        }
                        break;

                    case DELETE:
                        {
                           request = buildDeleteRequest(appWidgetId);
                        }
                        break;

                    case REFRESH_TOKEN:
                        {
                           request = buildRefreshTokenRequest(appWidgetId,oldToken);
                        }
                        break;
                    case CONFIGURE:
                    case UPDATE:
                    case DEFAULT:
                    default:
                       {
                           request = buildUpdateRequest(appWidgetId,EntryExitAction.DEFAULT);
                       }
                    break;

                    case ENTRY:
                    case EXIT:
                        {
                         request = buildUpdateRequest(appWidgetId,action);
                        }
                    break;
                }

                Response response = client.newCall(request).execute();

                if(response.isSuccessful())
                {


                    if(action == EntryExitAction.DELETE){
                        //delete the token
                        InstanceID.getInstance(getApplicationContext()).
                                deleteToken(getString(R.string.gcm_defaultSenderId),
                                        GoogleCloudMessaging.INSTANCE_ID_SCOPE);

                        //delete the perferences files
                        deletePreference(getApplicationContext(),appWidgetId);
                    }else if(action != EntryExitAction.REFRESH_TOKEN){
                        String responseData = response.body().string();
                        JSONObject json = new JSONObject(responseData);

                        //Log.v("ResponseData", responseData);
                        Integer occupied = json.getInt("numOccupied");
                        if(action == EntryExitAction.REGISTER){
                           putServerWidgetIdPerWidgetId(getApplicationContext(),
                                                        json.getInt("id"), //this is the ID simplypark server has given to this widget
                                                        appWidgetId);
                        }


                        if (occupied >= 0) {
                            if(updateRequestCounter == 0) {
                                setViewsOnSuccess(views);
                                currentSlotCounter = occupied;
                                views.setTextViewText(R.id.count,
                                        Integer.toString(currentSlotCounter));
                            }else{
                                updateRequestCounter--;
                            }
                        } else {
                            setViewsOnError(views, "Invalid ID. Please re-install");
                        }
                        lastUpdateFailed = false;

                    }
                }else{
                    setViewsOnError(views,"Failed. Please try after sometime");
                }
                response.body().close();

            }catch(IOException | JSONException ioe){
			    setViewsOnError(views, " Failed.Please try after sometime");
		    }

            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(getApplicationContext());

            appWidgetManager.updateAppWidget(appWidgetId,views);
	    }

	    private Request buildRegisterRequest(int appWidgetId) throws JSONException
	    {

                String url = baseUrl + registerController + "/" +
                        getSpaceId(getApplicationContext(), appWidgetId)
                        + ".json";

                JSONObject jsonDataObj = new JSONObject();
                jsonDataObj.put("token", getToken(getApplicationContext(), appWidgetId));


                RequestBody body = RequestBody.create(JSON, jsonDataObj.toString());
                return (new Request.Builder()
                        .url(url)
                        .post(body)
                        .build());


		}

	    private Request buildDeleteRequest(int appWidgetId) throws JSONException
	    {
		    String url = baseUrl + deleteController +".json";
            Context context = getApplicationContext();

		    JSONObject jsonDataObj = new JSONObject();
		    jsonDataObj.put("token", getToken(context,appWidgetId));
            jsonDataObj.put("id",getServerWidgetIdPerWidgetId(context,appWidgetId));

		    RequestBody body = RequestBody.create(JSON, jsonDataObj.toString());
            return(new Request.Builder()
			    .url(url)
			    .post(body)
			    .build());


	    } 

	    private Request buildRefreshTokenRequest(int appWidgetId,String oldToken) throws JSONException
	    {
		    String url = baseUrl + refreshTokenController + ".json";
            Context context = getApplicationContext();

		    JSONObject jsonDataObj = new JSONObject();
		    jsonDataObj.put("oldToken",oldToken);
		    jsonDataObj.put("newToken",getToken(context,appWidgetId));
            jsonDataObj.put("id",getServerWidgetIdPerWidgetId(context,appWidgetId));

            RequestBody body = RequestBody.create(JSON, jsonDataObj.toString());
            return(new Request.Builder()
			    .url(url)
			    .post(body)
			    .build());

	    }

	    private Request buildUpdateRequest(int appWidgetId, EntryExitAction action)
	    {
            String id  = getServerWidgetIdPerWidgetId(getApplicationContext(),appWidgetId);
		    String url = baseUrl + updateController +  "/" +
			    getSpaceId(getApplicationContext(),appWidgetId) + "/" + action.ordinal() + "/"+ id + ".json";


            return (new Request.Builder()
			    .url(url)
			    .build());


	    }

    } 

    //Inner class to handle occupancy update
    public static class WidgetGCMListenerService extends GcmListenerService{
        @Override
        public void onMessageReceived(String from, Bundle data){

            //Log.e("onMessageReceived",from);

            String numOccupied = data.getString("numOccupied");
            String spaceId = data.getString("spaceId");
            //Log.e("onMessageReceived","numOccupied is::" + numOccupied);
            //Log.e("onMessageReceived","spaceid is::" + spaceId);
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(getApplicationContext());
            RemoteViews views = new RemoteViews(this.getPackageName(),
                                       R.layout.entry_exit);


            views.setFloat(R.id.count, "setTextSize", 54);
            views.setTextViewText(R.id.poweredBy, "powered by SimplyPark.in");
            views.setTextViewText(R.id.count, numOccupied);

            int appWidgetIds[] = appWidgetManager.getAppWidgetIds(new ComponentName(getApplicationContext(),
                                         EntryExit.class));
            for (int appWidgetId : appWidgetIds) {
                //Log.e("onMessageReceived","spaceid123 is::" + spaceIdPerWidgetId.get(appWidgetId));

                if(!getSpaceId(getApplicationContext(),appWidgetId).equals(PREFS_NOT_FOUND)) {
                    appWidgetManager.updateAppWidget(appWidgetId, views);
                }
            }

        }
    }
    public static class WidgetInstanceIDListenerService extends InstanceIDListenerService {

        @Override
        public void onTokenRefresh() {
            // Fetch updated Instance ID token and notify our app's server of any changes
            // (if applicable).
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(getApplicationContext());
            int appWidgetIds[] = appWidgetManager.getAppWidgetIds(new ComponentName(getApplicationContext(),
                                                              EntryExit.class));


            //CharSequence widgetText = EntryExitConfigureActivity.loadTitlePref(context, appWidgetId);
            // Construct the RemoteViews object
            Context context = getApplicationContext();

            for (int appWidgetId : appWidgetIds) {
                String token = getToken(getApplicationContext(),appWidgetId);
                if(!token.equals(PREFS_NOT_FOUND)) {
                    RemoteViews views = new RemoteViews(context.getPackageName(),
                            R.layout.entry_exit);

                    // Build the intent to call the service
                    Intent intent = new Intent(context.getApplicationContext(),
                            LogEntryExitIntentService.class);

                    intent.putExtra(EXTRA_APPWIDGET_ID, appWidgetId);
                    intent.putExtra(EXTRA_CUSTOM_EXTRAS,token);
                    intent.setAction(ACTION_WIDGET_REFRESH_TOKEN);
                    context.startService(intent);

                    appWidgetManager.updateAppWidget(appWidgetId, views);
                    break; //currently all widget instances on the same mobile have the same token,
                           //so we break when we find the first widgetId with a token

                }
            }





        }

    }

}

